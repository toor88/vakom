<?php
session_start();

include 'lib/db.oracle.class.php';
include 'lib/db.oracle.ascore_garanties.class.php';
include 'lib/html.class.php';
include 'lib/fonctions.php';

class db_ascore_garanties {
    // Propriétés par défaut
    private $login = "VAKOM_TEST";
    private $passw = "VAKOM_TEST01";
    private $machine = "81.93.1.32";
    private $port = "1521";
    private $sid = "CLGB";

    // Constructeur
    public function __construct() {
        // Vous pouvez laisser le constructeur vide ou ajouter des initialisations ici
    }

    // Méthode d'initialisation pour configurer les paramètres de connexion
    public function init($login, $passw, $machine, $port, $sid) {
        $this->login = $login;
        $this->passw = $passw;
        $this->machine = $machine;
        $this->port = $port;
        $this->sid = $sid;
    }

    // Méthode de connexion
    public function connect() {
        // Code de connexion ici en utilisant les propriétés configurées
    }

    // Autres méthodes de la classe
}

$host = "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=".$db->machine.")(PORT=".$db->port."))(CONNECT_DATA=(SERVICE_NAME=".$db->sid.")))";
$db = new db_ascore_garanties();
$conn = ocilogon($db->login,$db->passw,$db->host);
while (!$conn) {
	$conn    = ocilogon($login,$pwd,$host);
}

if ($_POST['set_pass'])
{
	$utilisateur = $db->getUtilisateur_by_login($_POST['login']);
	if ($utilisateur[0]['login'] != '' && $_POST['mdp1'] == $_POST['mdp2'] && $_POST['old_mdp'] == $utilisateur[0]['mdp']) {
		$db->setUtilisateur($utilisateur[0]['id'], $_POST['mdp1']);
		$_SESSION['login'] = $utilisateur[0]['login']; 
	} else {

	}
}

if ($_POST['connexion'])
{
	$utilisateur = $db->getUtilisateur_by_login($_POST['login']);
	
	if ($utilisateur[0]['login'] != '' && $_POST['mdp'] == $utilisateur[0]['mdp']) {
		if ($utilisateur[0]['date_conn']) {
			$_SESSION['login'] = $utilisateur[0]['login'];
		} else {
			header('Location: login.php?valid_mdp=1');
			break;
		}
	} else {
		$_SESSION['login'] = '';
	}
}

if ($_GET['deconnexion'])
{
	$_SESSION['login'] = '';
}

if ( $_SESSION['login'] == '' ) {
	header('Location: login.php');
}

if ($_POST['select_id_garantie']) {
	$garantie = $db->getGaranties($_POST['select_id_garantie']);
	
	$db->setGarantieSuivi($garantie['0']['id'], $_SESSION['login']);
	
	$societe = $db->getSocietes($garantie['0']['id_societe']);
	$_SESSION['id_garantie']	= $garantie['0']['id'];
	$_SESSION['label_garantie']	= $garantie['0']['label'];
	$_SESSION['societe']		= $societe['0']['label'];
	$_SESSION['college']		= $garantie['0']['college'];
	$_SESSION['publication']	= $garantie['0']['publication'];
	unset($_POST);
}

if ($_POST['id_societe']) {
	if ($db->getGaranties_soc($_POST['id_societe'], $_POST['college'])) {
		echo "<SCRIPT LANGUAGE='JavaScript'>document.location.href='index.php?dupli=1'</SCRIPT>"
;	} else {
		$id_garantie = $db->setGarantie($id_garantie='', $_POST['id_societe'], $_POST['id_societe'], $_POST['college']);

		$garantie = $db->getGaranties($id_garantie);
		
		$db->setGarantieSuivi($garantie['0']['id'], $_SESSION['login']);
		
		$societe = $db->getSocietes($garantie['0']['id_societe']);
	
		$_SESSION['id_garantie']	= $garantie['0']['id'];
		$_SESSION['label_garantie']	= $garantie['0']['label'];
		$_SESSION['societe']		= $societe['0']['label'];
		$_SESSION['college']		= $garantie['0']['college'];
		$_SESSION['publication']	= $garantie['0']['publication'];
		if ($_POST['select_copie']!=""){
			include 'dupli_garantie.php';
		}else{
			unset($_POST);
		}
	}
}
?>