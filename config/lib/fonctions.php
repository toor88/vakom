<?php

function concat_array($array, $separator = "##")
{	
	$result = "";
	if(is_array($array)) {
		foreach($array as $element) {
			$result .= $element;
			$result .= $separator;
		}
	} else {
		$result = "0";
	}
	$result = rtrim($result, "##");
	return $result;
}

function assemble_formule($taux, $tranche)
{
	$nb_lignes = count($taux);
	for ($i = 0; $i < $nb_lignes; $i++) {
		echo $i;
	}
}

function getNextRow($table)
{
		global $conn;
		$query="SELECT id FROM ".$table." ORDER BY id DESC";
		$sql = @ociparse($conn,$query);
		ociexecute($sql);
		ocifetch($sql);
		$next_id = ociresult($sql,"ID") + 1;
		return $next_id;
}

function dump_array($arrayname,$tab="&nbsp;&nbsp;&nbsp;&nbsp;",$indent=0)
{
	while(list($key, $value) = each($arrayname))
	{
		for($i=0; $i<$indent; $i++) $currenttab .= $tab;
		if (is_array($value))
		{
			$retval .= "$currenttab$key : Array: <BR>$currenttab{<BR>";
			$retval .= dump_array($value,$tab,$indent+1)."$currenttab}<BR>";
		}
		else $retval .= "$currenttab$key => $value<BR>";
		$currenttab = NULL;
	}
	echo $retval;
}

function syncFormuleMum($tab_formule, $tab_minimaxi)
{
	$i=0;
	foreach ($tab_formule AS $formule) {
		foreach ($tab_minimaxi AS $minimaxi) {
			if ($formule['id'] == $minimaxi['id_formule']) {
				$result[$i]['id']    				= $minimaxi['id'];
				$result[$i]['id_ligne_garantie']    = $minimaxi['id_ligne_garantie'];
				$result[$i]['taux'] 				= $minimaxi['taux'];
				$result[$i]['id_tranche'] 			= $minimaxi['id_tranche'];
				$result[$i]['id_formule'] 			= $minimaxi['id_formule'];
			} else {
				$result[$i]['id']    				= "";
				$result[$i]['id_ligne_garantie']    = "";
				$result[$i]['taux'] 				= "";
				$result[$i]['id_tranche'] 			= "";
				$result[$i]['id_formule'] 			= "";
			}
		}
		$i++;
	}
	return $result;
}



?>