<?php

function makeSelect($nom, $options, $selected='', $isMultiple='', $ligne_a_vide='', $style='', $js='')
{
	if (!empty($selected)) $selected_ids = explode('##', $selected);
	$result  = "<select name=".$nom." id=".$nom;
	if (!empty($isMultiple)) $result = "<select name=".$nom."[] id=".$nom."[] size=5 multiple";
	if (!empty($style)) $result .= " style=\"".$style."\"";
	if (!empty($js) && $js==1) $result .= " onclick=\"copie_select_vers_input()\"";
	if (!empty($js) && $js==2) $result .= " onclick=\"copie_select_vers_parametrage()\"";
	$result .= " >";
	$result .= "<option value=\"0\">&nbsp;</option>";
	foreach ($options as $res) {
			$result .= "<option value='".$res['id']."'";
			if (!empty($selected) && in_array($res['id'], $selected_ids)) $result .= " selected";
			$result .= ">".$res['label']."</option>";
	}
	$result .= "</select>";
	return $result;
}

function makeTableHeader($nom, $options, $taille='100%', $border = '0', $bgcolor = '#E9E9E9', $class = 'ArialNoir')
{
	$result  = "<table name=".$nom." id=".$nom." size=".$taille." border=".$border.">";
	$i=0;
	foreach ($options as $res) {
		$result .= "<tr><td>".$res[$i]."</td></tr>";
		$i++;
	}
	$result .= "</table>";
}



?>
