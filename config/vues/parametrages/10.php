<?phpPHP

$id_ligne_garantie 	= $_POST['id_ligne_garantie'];
$label 				= $_POST['libelle_garantie'];
$label_formule 		= $_POST['libelle_formule'];
$id_garantie 		= $_SESSION['id_garantie'];
$id_parametrage		= $_GET['p'];
$id_option 			= $_GET['o'];
$statut 			= concat_array($_POST['statut']);
$situation_famille 	= concat_array($_POST['situation_famille']);
$nb_enfant 			= $_POST['nb_enfant'];
$nb_charge 			= $_POST['nb_charge'];
$ordre 				= $_POST['ordre'];

if ($_POST['ajout']) {

	if ($statut == "0") $statut = "1";
	if ($situation_famille == "0") $situation_famille = "1";
	
	$ret_ligne_garantie =  $db->setLigneGarantie(
		$id_ligne_garantie,
		$label,
		$label_formule,
		$id_garantie,
		$id_parametrage,
		$option,
		$statut,
		$situation_famille,
		$nb_enfant,
		$nb_charge,
		$ordre
	);

	$db->delFormule_By_id_ligne_garantie($ret_ligne_garantie);
	$db->delMinimum_By_id_ligne_garantie($ret_ligne_garantie);
	$db->delMaximum_By_id_ligne_garantie($ret_ligne_garantie);
	
	for ($i = 0; $i < count($_POST['taux_formule']); $i++) {
		if (!empty($_POST['taux_formule'][$i])) {
			$id_formule = $db->setFormule('', $ret_ligne_garantie, $_POST['taux_formule'][$i], $_POST['tranche_formule'][$i], $condition);
			if (!empty($_POST['taux_minimum'][$i])) {
				$db->setMinimum('', $ret_ligne_garantie, $_POST['taux_minimum'][$i], $_POST['tranche_minimum'][$i], $id_formule);
			}
			if (!empty($_POST['taux_maximum'][$i])) {
				$db->setMaximum('', $ret_ligne_garantie, $_POST['taux_maximum'][$i], $_POST['tranche_maximum'][$i], $id_formule);
			}
		}
	}
}

if ($_GET['edit']) {
	$result = $db->getLigneGarantie($_GET['edit']);
	$ligne = $result['ligne']['0'];
	$formule = $db->getFormule($_GET['edit']);
	
	if ($formule) {
		$i = 0;
		foreach ($formule as $ligne_formule) {
			$result_min = $db->getMinimumMat($_GET['edit'], $ligne_formule['id']);
			if ($result_min[0]['id_formule'] == $ligne_formule['id']) $minimum[$i] = $result_min[0];
			$result_max = $db->getMaximumMat($_GET['edit'], $ligne_formule['id']);
			if ($result_max[0]['id_formule'] == $ligne_formule['id']) $maximum[$i] = $result_max[0];
			$i++;
		}
	}
}
?>


<form id="form_<?php echo$p?>" name="form_<?php echo$p?>" method="post" action="index.php?c=<?php echo$c?>&p=<?php echo$p?>&o=<?php echo$o?>">
<?php echomakeSelect('option', $db->getOption(), $ligne['id_option_parametrage']); ?>
<input name="id_ligne_garantie" type="hidden" id="id_ligne_garantie" value="<?php echo$_GET['edit']?>" />
<table border="0" cellspacing="1" cellpadding="2" class="ArialNoir" bgcolor="#000000" width="100%">
  <tr>
    <td bgcolor="#E9E9E9" class="ArialNoir" align="center" valign="middle">Libell&eacute; de la garantie</td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF" align="center">
	<textarea name="libelle_garantie" id="libelle_garantie" class="formulaireTexteLibre" rows="10" style="width:100%"><?php echo$ligne['label']?></textarea>
    </td>

  </tr>
</table>
<span class="ArialNoir12Normal">
<input type="submit" name="ajout" value="Enregister" class="bnOkrouge">
</span>
</form>
