<?phpPHP

$id_ligne_garantie 	= $_POST['id_ligne_garantie'];
$label 				= $_POST['libelle_garantie'];
$label_formule 		= $_POST['libelle_formule'];
$id_garantie 		= $_SESSION['id_garantie'];
$id_parametrage		= $_GET['p'];
$id_option 			= $_GET['o'];
$statut 			= concat_array($_POST['statut']);
$situation_famille 	= concat_array($_POST['situation_famille']);
$nb_enfant 			= $_POST['nb_enfant'];
$nb_charge 			= $_POST['nb_charge'];
$ordre 				= $_POST['ordre'];

if ($_POST['ajout']) {

	if ($statut == "0") $statut = "1";
	if ($situation_famille == "0") $situation_famille = "1";
	
	$ret_ligne_garantie =  $db->setLigneGarantie(
		$id_ligne_garantie,
		$label,
		$label_formule,
		$id_garantie,
		$id_parametrage,
		$option,
		$statut,
		$situation_famille,
		$nb_enfant,
		$nb_charge,
		$ordre
	);

	$db->delFormule_By_id_ligne_garantie($ret_ligne_garantie);
	$db->delMinimum_By_id_ligne_garantie($ret_ligne_garantie);
	$db->delMaximum_By_id_ligne_garantie($ret_ligne_garantie);
	
	for ($i = 0; $i < count($_POST['taux_formule']); $i++) {
		if (!empty($_POST['taux_formule'][$i])) {
			$id_formule = $db->setFormule('', $ret_ligne_garantie, $_POST['taux_formule'][$i], $_POST['tranche_formule'][$i], $_POST['condition'][$i]);
			if (!empty($_POST['taux_minimum'][$i])) {
				$db->setMinimum('', $ret_ligne_garantie, $_POST['taux_minimum'][$i], $_POST['tranche_minimum'][$i], $id_formule);
			}
			if (!empty($_POST['taux_maximum'][$i])) {
				$db->setMaximum('', $ret_ligne_garantie, $_POST['taux_maximum'][$i], $_POST['tranche_maximum'][$i], $id_formule);
			}
		}
	}
}


if ($_GET['edit']) {
	$result = $db->getLigneGarantie($_GET['edit']);
	$ligne = $result['ligne']['0'];
	$formule = $db->getFormule($_GET['edit']);
	
	if ($formule) {
		$i = 0;
		foreach ($formule as $ligne_formule) {
			$result_min = $db->getMinimumMat($_GET['edit'], $ligne_formule['id']);
			if ($result_min[0]['id_formule'] == $ligne_formule['id']) $minimum[$i] = $result_min[0];
			$result_max = $db->getMaximumMat($_GET['edit'], $ligne_formule['id']);
			if ($result_max[0]['id_formule'] == $ligne_formule['id']) $maximum[$i] = $result_max[0];
			$i++;
		}
	}
}
?>


<form id="form_<?php echo$p?>" name="form_<?php echo$p?>" method="post" action="index.php?c=<?php echo$c?>&p=<?php echo$p?>&o=<?php echo$o?>">
<?php echomakeSelect('option', $db->getOption(), $ligne['id_option_parametrage']); ?>
<input name="id_ligne_garantie" type="hidden" id="id_ligne_garantie" value="<?php echo$_GET['edit']?>" />
<table border="0" cellspacing="1" cellpadding="2" class="ArialNoir" bgcolor="#000000" width="100%">
  <tr>
    <td bgcolor="#E9E9E9" class="ArialNoir" align="center" valign="middle">Ordre</td>
	<td bgcolor="#E9E9E9" class="ArialNoir" align="center" valign="middle">Statut</td>
    <td bgcolor="#E9E9E9" class="ArialNoir" align="center" valign="middle">&nbsp;Nbre <br> d'enfants&nbsp;</td>
    <td bgcolor="#E9E9E9" class="ArialNoir" align="center" valign="middle">Nbre de personnes<br> &agrave; charge</td>
    <td bgcolor="#E9E9E9" class="ArialNoir" align="center" valign="middle">Libell&eacute; de la garantie</td>
    <td bgcolor="#E9E9E9" class="ArialNoir" align="center" valign="middle">Formule en libell&eacute;</td>
  </tr>
  <tr>
	<td valign="top" bgcolor="#FFFFFF" align="center">
		<?phpPHP
		$max_ordre = $db->getMaxOrdre($id_garantie, $id_parametrage);
		if(empty($ligne['ordre'])) $ligne['ordre'] = $max_ordre['ordre'] + 1;
		?>
		<input name="ordre" type="text" class="formulaireCadreNoir" id="ordre" size="3" value="<?php echo$ligne['ordre']?>">
	</td>
    <td valign="top" bgcolor="#FFFFFF" align="center">
	<?php echomakeSelect('statut', $db->getStatut(), $ligne['id_statut'], 1); ?>
    </td>
    <td valign="top" bgcolor="#FFFFFF" align="center"><input type="text" name="nb_enfant" size="3" class="formulaireCadreNoir" value="<?php echo$ligne['nb_enfant']?>">
    </td>
    <td valign="top" bgcolor="#FFFFFF" align="center"><input type="text" name="nb_charge" size="3" class="formulaireCadreNoir" value="<?php echo$ligne['nb_charge']?>">
    </td>
    <td valign="top" bgcolor="#FFFFFF" align="center">
    <?php echomakeSelect('select_libelle', $db->getAllLignes(), '', '', '', "width:400px;", '1'); ?><br>
    <textarea name="libelle_garantie" id="libelle_garantie" class="formulaireTexteLibre" rows="5" style="width:400px"><?php echo$ligne['label']?></textarea>
    </td>
	<td valign="top" bgcolor="#FFFFFF" align="center"><input type="text" name="libelle_formule" class="formulaireCadreNoir"  value="<?php echo$ligne['label_formule']?>" size="25">
    </td>
	</tr></table><br><table border="0" cellspacing="1" cellpadding="2" class="ArialNoir" bgcolor="#000000" width="100%"><tr>
		<td bgcolor="#E9E9E9" class="ArialNoir" align="center" valign="middle">Formule en calcul</td>
		<td bgcolor="#E9E9E9" class="ArialNoir" align="center" valign="middle">Minimum</td>
		<td bgcolor="#E9E9E9" class="ArialNoir" align="center" valign="middle">Maximum</td>
	</tr>
    <td valign="top" bgcolor="#FFFFFF" align="center">
      <input name="id_formule[]" type="hidden" id="id_formule[]" value="<?php echo$formule['0']['id']?>" />
	  <input name="taux_formule[]" type="text" class="formulaireCadreNoir" id="taux_formule[]" size="3" value="<?php echo$formule['0']['taux']?>">
	  %<?php echomakeSelect('tranche_formule[]', $db->getTranches(), $formule['0']['id_tranche']); ?>
	  <input name="condition[0]" id="condition[0]" type="checkbox" <?php if ($formule['0']['condition']) echo "checked"; ?>> moins SS ?
	  </br>
	  </br>
	  <input name="id_formule[]" type="hidden" id="id_formule[]" value="<?php echo$formule['1']['id']?>" />
	  <input name="taux_formule[]" type="text" class="formulaireCadreNoir" id="taux_formule[]" size="3" value="<?php echo$formule['1']['taux']?>">
	  %<?php echomakeSelect('tranche_formule[]', $db->getTranches(), $formule['1']['id_tranche']); ?>
	  <input name="condition[1]" id="condition[1]" type="checkbox" <?php if ($formule['1']['condition']) echo "checked"; ?>> moins SS ?
	  </br>
	  </br>
	  <input name="id_formule[]" type="hidden" id="id_formule[]" value="<?php echo$formule['2']['id']?>" />
	  <input name="taux_formule[]" type="text" class="formulaireCadreNoir" id="taux_formule[]" size="3" value="<?php echo$formule['2']['taux']?>">
	  %<?php echomakeSelect('tranche_formule[]', $db->getTranches(), $formule['2']['id_tranche']); ?>
	  <input name="condition[2]" id="condition[2]" type="checkbox" <?php if ($formule['2']['condition']) echo "checked"; ?>> moins SS ?
	  </br>
	  </br>
	  <input name="id_formule[]" type="hidden" id="id_formule[]" value="<?php echo$formule['3']['id']?>" />
	  <input name="taux_formule[]" type="text" class="formulaireCadreNoir" id="taux_formule[]" size="3" value="<?php echo$formule['3']['taux']?>">
	  %<?php echomakeSelect('tranche_formule[]', $db->getTranches(), $formule['3']['id_tranche']); ?>
	  <input name="condition[3]" id="condition[3]" type="checkbox" <?php if ($formule['3']['condition']) echo "checked"; ?>> moins SS ?
	  </br>
	  </br>
	  <input name="id_formule[]" type="hidden" id="id_formule[]" value="<?php echo$formule['4']['id']?>" />
	  <input name="taux_formule[]" type="text" class="formulaireCadreNoir" id="taux_formule[]" size="3" value="<?php echo$formule['4']['taux']?>">
	  %<?php echomakeSelect('tranche_formule[]', $db->getTranches(), $formule['4']['id_tranche']); ?>
	  <input name="condition[4]" id="condition[4]" type="checkbox" <?php if ($formule['4']['condition']) echo "checked"; ?>> moins SS ?
</td>
	<td valign="top" bgcolor="#FFFFFF" align="center">
      <input name="id_minimum[]" type="hidden" id="id_minimum[]" value="<?php echo$minimum['0']['id']?>" />
	  <input name="taux_minimum[]" type="text" class="formulaireCadreNoir" id="taux_minimum[]" size="3" value="<?php echo$minimum['0']['taux']?>">
	  %<?php echomakeSelect('tranche_minimum[]', $db->getTranches(), $minimum['0']['id_tranche']); ?>
	  </br>
	  </br>
	  <input name="id_minimum[]" type="hidden" id="id_minimum[]" value="<?php echo$minimum['1']['id']?>" />
	  <input name="taux_minimum[]" type="text" class="formulaireCadreNoir" id="taux_minimum[]" size="3" value="<?php echo$minimum['1']['taux']?>">
	  %<?php echomakeSelect('tranche_minimum[]', $db->getTranches(), $minimum['1']['id_tranche']); ?>
	  </br>
	  </br>
	  <input name="id_minimum[]" type="hidden" id="id_minimum[]" value="<?php echo$minimum['2']['id']?>" />
	  <input name="taux_minimum[]" type="text" class="formulaireCadreNoir" id="taux_minimum[]" size="3" value="<?php echo$minimum['2']['taux']?>">
	  %<?php echomakeSelect('tranche_minimum[]', $db->getTranches(), $minimum['2']['id_tranche']); ?>
	  </br>
	  </br>
	  <input name="id_minimum[]" type="hidden" id="id_minimum[]" value="<?php echo$minimum['3']['id']?>" />
	  <input name="taux_minimum[]" type="text" class="formulaireCadreNoir" id="taux_minimum[]" size="3" value="<?php echo$minimum['3']['taux']?>">
	  %<?php echomakeSelect('tranche_minimum[]', $db->getTranches(), $minimum['3']['id_tranche']); ?>
	  </br>
	  </br>
	  <input name="id_minimum[]" type="hidden" id="id_minimum[]" value="<?php echo$minimum['4']['id']?>" />
	  <input name="taux_minimum[]" type="text" class="formulaireCadreNoir" id="taux_minimum[]" size="3" value="<?php echo$minimum['4']['taux']?>">
	  %<?php echomakeSelect('tranche_minimum[]', $db->getTranches(), $minimum['4']['id_tranche']); ?>
</td>
	<td valign="top" bgcolor="#FFFFFF" align="center">
      <input name="id_maximum[]" type="hidden" id="id_maximum[]" value="<?php echo$maximum['0']['id']?>" />
	  <input name="taux_maximum[]" type="text" class="formulaireCadreNoir" id="taux_maximum[]" size="3" value="<?php echo$maximum['0']['taux']?>">
	  %<?php echomakeSelect('tranche_maximum[]', $db->getTranches(), $maximum['0']['id_tranche']); ?>
	  </br>
	  </br>
	  <input name="id_maximum[]" type="hidden" id="id_maximum[]" value="<?php echo$maximum['1']['id']?>" />
	  <input name="taux_maximum[]" type="text" class="formulaireCadreNoir" id="taux_maximum[]" size="3" value="<?php echo$maximum['1']['taux']?>">
	  %<?php echomakeSelect('tranche_maximum[]', $db->getTranches(), $maximum['1']['id_tranche']); ?>
	  </br>
	  </br>
	  <input name="id_maximum[]" type="hidden" id="id_maximum[]" value="<?php echo$maximum['2']['id']?>" />
	  <input name="taux_maximum[]" type="text" class="formulaireCadreNoir" id="taux_maximum[]" size="3" value="<?php echo$maximum['2']['taux']?>">
	  %<?php echomakeSelect('tranche_maximum[]', $db->getTranches(), $maximum['2']['id_tranche']); ?>
	  </br>
	  </br>
	  <input name="id_maximum[]" type="hidden" id="id_maximum[]" value="<?php echo$maximum['3']['id']?>" />
	  <input name="taux_maximum[]" type="text" class="formulaireCadreNoir" id="taux_maximum[]" size="3" value="<?php echo$maximum['3']['taux']?>">
	  %<?php echomakeSelect('tranche_maximum[]', $db->getTranches(), $maximum['3']['id_tranche']); ?>
	  </br>
	  </br>
	  <input name="id_maximum[]" type="hidden" id="id_maximum[]" value="<?php echo$maximum['4']['id']?>" />
	  <input name="taux_maximum[]" type="text" class="formulaireCadreNoir" id="taux_maximum[]" size="3" value="<?php echo$maximum['4']['taux']?>">
	  %<?php echomakeSelect('tranche_maximum[]', $db->getTranches(), $maximum['4']['id_tranche']); ?>
</td>
    
  </tr>
</table>
<span class="ArialNoir12Normal">
<input type="submit" name="ajout" value="Enregister" class="bnOkrouge">
</span>
</form>
