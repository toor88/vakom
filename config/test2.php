<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>frequency decoder ~ table sort (revisited) demo</title>
  <script type="text/javascript" src="tablesort.js"></script>
  <link href="tablesort.css"       rel="stylesheet" type="text/css" />
</head>
<body>
<?phpPHP
include 'config.php';

$liste_tables = $db->getTables();

foreach ($liste_tables as $table) {
	echo "TABLE ".strtolower($table['label']);
	//echo "<table width='100%' cellpadding='1' cellspacing='1' border='1'>";
	echo '<table id="table_'.strtolower($table['label']).'" cellpadding="0" cellspacing="0" border="0" class="sortable-onload-3r-4 rowstyle-alt no-arrow">';
	echo '<caption>The Numbers Weekly World Chart (2007-03-08)</caption>';
	echo '<thead>';
		echo "<tr>";
			$query = "SELECT * FROM ".$table['label'];
			$sql = ociparse($conn,$query);
			ociexecute($sql);
			$num_cols = ocinumcols($sql);
			for ($i = 1; $i <= $num_cols; $i++) {
				echo '<th class="sortable">';
				echo strtolower(ocicolumnname($sql, $i));
				echo '</th>';
			}
		echo "</tr>";
	echo '</thead>';
	
	echo '<tbody>';
	while (ocifetch($sql)){
		echo "<tr>";
		for ($i = 1; $i <= $num_cols; $i++) {
			echo "<td>".ociresult($sql,$i)."&nbsp;</td>";
		}
		echo "</tr>";
	}
	echo '</tbody>';
	
	echo "</table>";
	echo "<br><br>";
}

?>
</body>
</html>
