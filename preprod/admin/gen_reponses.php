<?php
session_start();
if ($_SESSION['droit']!=''){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	
	$db = new db($conn);
	
	if((isset($_GET['candid'])&&isset($_GET['opeid']))&&($_GET['candid']&&$_GET['opeid'])){
		$sql_get_reponse = "SELECT QUESTIONNAIRE.QUEST_NOM, CHOIX.CHOIX_QUESTION, 
		CHOIX.CHOIX_QUESTION_COMM,CAND_A_QUEST.REPONSE_PI, CAND_A_QUEST.REPONSE_PN
		FROM CAND_A_QUEST,QUESTIONNAIRE,CHOIX
		WHERE CAND_A_QUEST.QUEST_ID = QUESTIONNAIRE.QUEST_ID AND CAND_A_QUEST.CHOIX_ID = CHOIX.CHOIX_ID
		AND CAND_A_QUEST.CAND_ID='".$_GET['candid']."' AND CAND_A_QUEST.OPE_ID='".$_GET['opeid']."' 
		AND CHOIX.CHOIX_LANG_ID=1 order by CHOIX.CHOIX_ID";
		$qry_get_reponse = $db->query($sql_get_reponse);
		
		$filename = 'reponses_'.$_GET['candid'].'_'.$_GET['opeid'].'.csv';
		$file_path = 'csv/'.$filename;
		$csv_file = fopen($file_path, 'w+');
		
		if(is_array($qry_get_reponse)){
			$column_name = implode(';', array_keys($qry_get_reponse[0]))."\n";
			$line = '';
			
			foreach($qry_get_reponse as $data){
				$line .= '"'.implode('";"', preg_replace("/\r/", "", array_values($data))).'"'."\n";
			}
		}else{
			$line = 'Aucune données disponibles.';
		}
 		
		fwrite($csv_file, $column_name);
		fwrite($csv_file, $line);
		fclose($csv_file);
				
		header("Content-Description: File Transfer"); 
		header('Content-Transfer-Encoding: binary');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		header('Content-Length: ' . filesize($file_path));
		readfile($file_path);
		exit;
	}
}else{
	include('no_acces.php');
	exit;
}
?>
