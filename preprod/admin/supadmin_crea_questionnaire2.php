<?php
session_start();
	// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	if ($_GET['questid']!=''){
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);
			
		if ($_GET['action']=='del_lang' && $_GET['langid'] >0){

			$sql_del_lang 	= "DELETE FROM QUEST_A_LANG WHERE QUEST_ID='".txt_db($_GET['questid'])."' AND LANG_ID='".txt_db($_GET['langid'])."'";
			$qry_del_lang	= $db->query($sql_del_lang);
			
			header('location:supadmin_crea_questionnaire2.php?questid='.$_GET['questid']).'&choix_lang='.$_GET['choix_lang'];
	
		}
		if ($_GET['action']=='del_question' && $_GET['questionid'] >0){

			$sql_del_question 	= "UPDATE QUEST_A_VAL SET VAL_USER_SUPPRESSION='".$_SESSION['vak_id']."', VAL_DATE_SUPPRESSION=SYSDATE WHERE VAL_CHOIX_ID='".txt_db($_GET['questionid'])."'";
			$qry_del_question	= $db->query($sql_del_question);
			
			header('location:supadmin_crea_questionnaire2.php?questid='.$_GET['questid']).'&choix_lang='.$_GET['choix_lang'];
	
		}
		
		// Si le questionnaire est modifié
		if ($_POST['posted']=='1'){
			if (trim($_POST['nom_quest'])!=''){
				$sql_modif_quest = "UPDATE QUESTIONNAIRE SET QUEST_NOM='".txt_db($_POST['nom_quest'])."',
				QUEST_PUBLIC='".txt_db($_POST['grand_public'])."',
				QUEST_PAYANT='".txt_db($_POST['payant'])."',
				QUEST_TYPE_QUESTION_CODE_ID='".txt_db($_POST['select_quest_type'])."' 
				WHERE QUEST_ID='".txt_db($_GET['questid'])."'";
				$qry_modif_quest = $db->query($sql_modif_quest);
			}

			/* Changement de l'ordre des questions */
			if (is_array($_POST['hid_choix_id'])){
				foreach($_POST['hid_choix_id'] as $choix_id){
					unset($sql_up_ordre);
					$sql_up_ordre = "UPDATE QUEST_A_VAL SET VAL_ORDRE='".txt_db(intval($_POST["ordre_$choix_id"]))."' WHERE VAL_QUEST_ID='".txt_db(intval($_GET['questid']))."' AND VAL_CHOIX_ID='".txt_db(intval($choix_id))."'";
					$qry_up_ordre = $db->query($sql_up_ordre);
				}
			}
			
			/* Si une des cases à cocher est supprimée */
			if (is_array($_POST['che_quest'])){
				/* Pour chaque question à suprimer */
				foreach($_POST['che_quest'] as $choix){
					/* On supprime la question */
					unset($sql_del_choix);
					$sql_del_choix = "DELETE FROM CHOIX WHERE CHOIX_ID='".txt_db(intval($choix))."'";
					$qry_del_choix = $db->query($sql_del_choix);
				}
			}
			
			header('location:supadmin_crea_questionnaire2.php?questid='.$_GET['questid'].'&choix_lang='.$_GET['choix_lang']);
		}
		
	#############################################################
	########            TRAITEMENT FORM0             ############
	#############################################################
	if ($_POST['choix_action']!=''){
	
		switch($_POST['choix_action']){
			case 'selection':
				if ($_POST['select_select']!=''){
					header('location:supadmin_crea_questionnaire2.php?questid='.intval($_POST['select_select']));
				}
			break;
			case 'creation': // Création d'un nouveau questionnaire
				if (trim($_POST['txt_quest'])!=''){
					// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
					$sql_seq_num 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
					$qry_seq_num 		= $db->query($sql_seq_num);
					$seq_num			= intval($qry_seq_num[0]['seq_num']);
				
					$sql_insert_quest 	= "INSERT INTO QUESTIONNAIRE VALUES('".$seq_num."', 
					'".txt_db($_POST['txt_quest'])."', 
					'',
					'', '".txt_db(intval($_POST['select_type_question']))."', '', '')";
					$qry_insert_quest	= $db->query($sql_insert_quest);
					
					header('location:supadmin_crea_questionnaire2.php?questid='.$seq_num);
				}else{
					$error = '<?php echo $t_manque_questionnaire ?>';
				}
			break;
			case 'duplication':
				if ($_POST['select_duplic']!='' && trim($_POST['nvo_nom_quest'])!=''){
					// On sélectionne les informations pour le questionnaire à dupliquer
					$sql_select_quest_a_dupliquer	= "SELECT * FROM QUESTIONNAIRE WHERE QUEST_ID='".txt_db($_POST['select_duplic'])."'";
					$qry_qad						= $db->query($sql_select_quest_a_dupliquer);
					
					if (is_array($qry_qad)){
						// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
						$sql_seq_num 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
						$qry_seq_num 		= $db->query($sql_seq_num);
						$seq_num			= intval($qry_seq_num[0]['seq_num']);
						
						$sql_insert_qai	= "INSERT INTO QUESTIONNAIRE(QUEST_ID,QUEST_NOM,QUEST_TYPE_QUESTION_CODE_ID) VALUES('".txt_db($seq_num)."', 
						'".txt_db($_POST['nvo_nom_quest'])."', 
						'".txt_db(intval($qry_qad[0]['quest_type_question_code_id']))."')";
						$qry_insert_qai	= $db->query($sql_insert_qai);
						
						
						/* Récupération des données de la table QUEST_A_LANG pour le questionnaire sélectionné */
						$sql_recup_qal 	= "SELECT * FROM QUEST_A_LANG WHERE QUEST_ID='".$qry_qad[0]['quest_id']."'";
						$qry_recup_qal	= $db->query($sql_recup_qal);

						if (is_array($qry_recup_qal)){
							foreach($qry_recup_qal as $data_quest_a_lang){					
								/* Pour chaque langue insérée dans le questionnaire à dupliquer, on fait une insertion avec l'id du nouveau questionnaire, en récupérant les memes données */
								$sql_insert_qal = "INSERT INTO QUEST_A_LANG (QUEST_ID,LANG_ID,TITRE,COMMENTAIRE_IN,COMMENTAIRE_IN_2,COMMENTAIRE_OUT) VALUES('".txt_db($seq_num)."',
								'".txt_db($data_quest_a_lang['lang_id'])."',
								'".txt_db($data_quest_a_lang['titre'])."',
								'".substr(txt_db($data_quest_a_lang['commentaire_in']),0,4000)."',
								'".substr(txt_db($data_quest_a_lang['commentaire_in_2']),4001,4000)."',
								'".txt_db($data_quest_a_lang['commentaire_out'])."')";
								$qry_insert_qal 		= $db->query($sql_insert_qal);								
							}
						}
						
						/* Récupération des données de la table QUEST_A_CORRESP_VAR pour le questionnaire sélectionné */
						$sql_recup_cor_var 	= "SELECT * FROM QUEST_A_CORRESP_VAR WHERE COR_VAR_QUEST_ID='".$qry_qad[0]['quest_id']."'";
						$qry_recup_cor_var	= $db->query($sql_recup_cor_var);
						
						if (is_array($qry_recup_cor_var)){
							foreach($qry_recup_cor_var as $quest_a_var){					
								/* Pour chaque langue insérée dans le questionnaire à dupliquer, on fait une insertion avec l'id du nouveau questionnaire, en récupérant les memes données */
								$sql_insert_var = "INSERT INTO QUEST_A_CORRESP_VAR VALUES(SEQ_ID.NEXTVAL,
								'".txt_db($seq_num)."',
								'".txt_db($quest_a_var['cor_var_type_profil_code_id'])."',
								'".txt_db($quest_a_var['cor_var_type_variable_code_id'])."',
								'".txt_db($quest_a_var['cor_var_somme'])."',
								'".txt_db($quest_a_var['cor_var_points'])."',
								'".txt_db($quest_a_var['cor_var_user_creation'])."',
								'".txt_db($quest_a_var['cor_var_date_creation'])."',
								'".txt_db($quest_a_var['cor_var_user_modification'])."',
								'".txt_db($quest_a_var['cor_var_date_modification'])."',
								'".txt_db($quest_a_var['cor_var_user_suppression'])."',
								'".txt_db($quest_a_var['cor_var_date_suppression'])."')";
								$qry_insert_var = $db->query($sql_insert_var);	
							}
						}
						
						/* Récupération des données de la table QUEST_A_CORRESP_REGLE pour le questionnaire sélectionné */
						$sql_recup_cor_regle 	= "SELECT * FROM QUEST_A_CORRESP_REGLE WHERE COR_REGLE_QUEST_ID='".$qry_qad[0]['quest_id']."'";
						$qry_recup_cor_regle	= $db->query($sql_recup_cor_regle);
						
						if (is_array($qry_recup_cor_regle)){
							foreach($qry_recup_cor_regle as $quest_a_regle){					
								/* Pour chaque langue insérée dans le questionnaire à dupliquer, on fait une insertion avec l'id du nouveau questionnaire, en récupérant les memes données */
								$sql_insert_regle = "INSERT INTO QUEST_A_CORRESP_REGLE VALUES(SEQ_ID.NEXTVAL,
								'".txt_db($seq_num)."',
								'".txt_db($quest_a_regle['cor_regle_type_profil_code_id'])."',
								'".txt_db($quest_a_regle['cor_regle_points_inf'])."',
								'".txt_db($quest_a_regle['cor_regle_points_sup'])."',
								'".txt_db($quest_a_regle['cor_regle_segment'])."',
								'".txt_db($quest_a_regle['cor_regle_bloc'])."',
								'".txt_db($quest_a_regle['cor_regle_user_creation'])."',
								'".txt_db($quest_a_regle['cor_regle_date_creation'])."',
								'".txt_db($quest_a_regle['cor_regle_user_modification'])."',
								'".txt_db($quest_a_regle['cor_regle_date_modification'])."',
								'".txt_db($quest_a_regle['cor_regle_user_suppression'])."',
								'".txt_db($quest_a_regle['cor_regle_date_suppression'])."')";
								$qry_insert_regle = $db->query($sql_insert_regle);								
							}
						}
						
						/* Récupération de toutes les questions de toutes les langues pour le questionnaire sélectionné */
						$sql_recup_valeurs 	= "SELECT * FROM QUEST_A_VAL WHERE VAL_QUEST_ID='".$qry_qad[0]['quest_id']."' AND VAL_DATE_SUPPRESSION IS NULL";
						$qry_recup_valeurs	= $db->query($sql_recup_valeurs);
						
						if (is_array($qry_recup_valeurs)){
							foreach ($qry_recup_valeurs as $valeur){
								
								/* ON récupère les intitulés des différentes langues pour chaque valeur retrouvée */
								$sql_questions	= "SELECT * FROM CHOIX WHERE CHOIX_ID='".txt_db($valeur['val_choix_id'])."'";
								$qry_langues	= $db->query($sql_questions);
								
								// On récupère l'id de la question à insérer. (Il nous servira pour faire la redirection) 
								$sql_seq_num_question 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
								$qry_seq_num_question 		= $db->query($sql_seq_num_question);
								$seq_num_question			= intval($qry_seq_num_question[0]['seq_num']);
								
								
								$sql_insert_valeurs = "INSERT INTO QUEST_A_VAL VALUES('".txt_db($seq_num)."',
								'".txt_db($seq_num_question)."',
								'".txt_db($valeur['val_ordre'])."',										
								'".txt_db($valeur['val_pi_1'])."',										
								'".txt_db($valeur['val_pi_2'])."',										
								'".txt_db($valeur['val_pi_3'])."',										
								'".txt_db($valeur['val_pi_4'])."',		
								'".txt_db($valeur['val_pn_1'])."',										
								'".txt_db($valeur['val_pn_2'])."',										
								'".txt_db($valeur['val_pn_3'])."',										
								'".txt_db($valeur['val_pn_4'])."',	
								'".txt_db($_SESSION['vak_id'])."',
								SYSDATE,
								'',
								'',
								'',
								'')";
								//echo $sql_insert_valeurs.'<br><br>';
								$qry_insert_valeur = $db->query($sql_insert_valeurs);
								
								/* Pour chaque langue sélectionnée, on insère un nouvel id de question dans la table des intitulés, avec la langue en quesion. */
								if (is_array($qry_langues)){
									foreach ($qry_langues as $langue){
										/* Pour chaque langue trouvée, on fait une insertion des questions */
										$sql_insert_questions = "INSERT INTO CHOIX (CHOIX_ID,CHOIX_LANG_ID,CHOIX_QUESTION,CHOIX_QUESTION_COMM,CHOIX_1,CHOIX_2,CHOIX_3,
										CHOIX_4,CODE_ID) VALUES ('".txt_db($seq_num_question)."',
										'".txt_db($langue['choix_lang_id'])."',
										'".txt_db($langue['choix_question'])."',
										'".txt_db($langue['choix_question_comm'])."',
										'".txt_db($langue['choix_1'])."',
										'".txt_db($langue['choix_2'])."',
										'".txt_db($langue['choix_3'])."',
										'".txt_db($langue['choix_4'])."',
										'".txt_db($langue['code_id'])."')";
										
										//echo $sql_insert_questions.'<br>';
										$qry_insert_questions = $db->query($sql_insert_questions);
									}
								}
								
							}
						}
						header('location:supadmin_crea_questionnaire2.php?questid='.$seq_num);
					}
				}
			break;
		}		
	}
		
	if($_POST['del_questid']>0){
		#############################################################
		########      SUPPRESSION DU QUESTIONNAIRE       ############
		#############################################################
		$sql_del_quest 	= "UPDATE QUESTIONNAIRE SET QUEST_DATE_SUPPRESSION=SYSDATE, QUEST_USER_SUPPRESSION='".txt_db($_SESSION['vak_id'])."' WHERE QUEST_ID='".txt_db($_POST['del_questid'])."'";
		$qry_del_quest 	= $db->query($sql_del_quest);
		
		#############################################################
		########          SUPPRESSION DES REGLES         ############
		#############################################################
		$sql_del_regle 	= "UPDATE QUEST_A_CORRESP_REGLE SET COR_REGLE_DATE_SUPPRESSION=SYSDATE, COR_REGLE_USER_SUPPRESSION='".txt_db($_SESSION['vak_id'])."' WHERE COR_REGLE_QUEST_ID='".txt_db($_POST['del_questid'])."'";
		$qry_del_regle 	= $db->query($sql_del_regle);
		
		#############################################################
		########        SUPPRESSION DES VARIABLES        ############
		#############################################################
		$sql_del_var 	= "UPDATE QUEST_A_CORRESP_VAR SET COR_VAR_DATE_SUPPRESSION=SYSDATE, COR_VAR_USER_SUPPRESSION='".txt_db($_SESSION['vak_id'])."' WHERE COR_VAR_QUEST_ID='".txt_db($_POST['del_questid'])."'";
		$qry_del_var 	= $db->query($sql_del_var);
		
		
		header('location: supadmin_crea_questionnaire.php');
	}
	
	// Selection de la liste des types de question disponibles
	$sql_type_quest_list = "SELECT * FROM CODE WHERE CODE_TABLE='TYPE_QUESTION' ORDER BY CODE_LIBELLE ASC";
	$qry_type_quest_list = $db->query($sql_type_quest_list);
	
	// Selection de la liste des questionnaires existants
	$sql_quest_list		= "SELECT * FROM QUESTIONNAIRE WHERE QUEST_DATE_SUPPRESSION IS NULL ORDER BY QUEST_NOM ASC";
	$qry_quest_list		= $db->query($sql_quest_list);
	
	#############################################################
	########            TRAITEMENT FORM0             ############
	#############################################################
		
		// Selection de la liste des types de question disponibles
		$sql_type_quest_list = "SELECT * FROM CODE WHERE CODE_TABLE='TYPE_QUESTION' ORDER BY CODE_LIBELLE ASC";
		$qry_type_quest_list = $db->query($sql_type_quest_list);
		
		// Sélection des informations de base du questionnaire
		$sql_info_questionnaire	= "SELECT * FROM QUESTIONNAIRE WHERE QUEST_ID='".txt_db($_GET['questid'])."'";
		$qry_info_questionnaire	= $db->query($sql_info_questionnaire);
		
		// Sélection des parametres de langue des textes généraux du questionnaire
		if (is_array($qry_info_questionnaire)){ // Si le questionnaire est trouvé
			$sql_info_lang_questionnaire	= "SELECT * FROM QUEST_A_LANG, LANGUE WHERE LANGUE.LANG_ID=QUEST_A_LANG.LANG_ID AND QUEST_A_LANG.QUEST_ID='".txt_db($_GET['questid'])."'";
			$qry_info_lang_questionnaire	= $db->query($sql_info_lang_questionnaire);
			//echo $sql_info_lang_questionnaire;
			if ($_GET['choix_lang']>0){
				$choix_lang = $_GET['choix_lang'];
			}else{
				$choix_lang = 1;
			}
			$sql_question = "SELECT * FROM CHOIX, QUEST_A_VAL WHERE CHOIX.CHOIX_ID=QUEST_A_VAL.VAL_CHOIX_ID AND VAL_QUEST_ID='".txt_db($_GET['questid'])."' AND CHOIX.CHOIX_LANG_ID='".$choix_lang."' AND QUEST_A_VAL.VAL_USER_SUPPRESSION IS NULL ORDER BY QUEST_A_VAL.VAL_ORDRE ASC, choix_question ASC";
			//echo $sql_question;
			$qry_question = $db->query($sql_question);
			
			$sql_langue = "SELECT LANGUE.LANG_ID, LANGUE.LANG_LIBELLE FROM LANGUE, QUEST_A_LANG WHERE QUEST_A_LANG.LANG_ID=LANGUE.LANG_ID AND QUEST_A_LANG.QUEST_ID='".txt_db($_GET['questid'])."' AND LANGUE.LANG_ID NOT IN ('1')";
			$qry_langue = $db->query($sql_langue);
			?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">		
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<link rel="stylesheet" href="../css/style.css" type="text/css">			
			</head>

			<body bgcolor="#FFFFFF" text="#000000">
					<?php
					include("menu_top_new.php");
					?>						  
	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
					
			  
				<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;QUESTIONNAIRES</td>
				</tr>
				</table>
	<form method="post" action="#" name="form0">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14" ></td>
		  <td width="14" height="14" align="right"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr> 
		<tr> 
		  <td width="14" height="14"></td>
		  <td align="center" class="TX"> 
			<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			  <tr> 
				<td height="14"></td>
				<td height="14"></td>
			  </tr>
			  <?php
				if (is_array($qry_quest_list)){
				  ?>
				  <tr> 
					<td align="left" class="TX"> 
					  <input type="radio" name="choix_action" value="selection" checked="checked">
					  S&eacute;lectionner un questionnaire :&nbsp;&nbsp;</td>
					<td align="left" class="TX"> 
					  <select name="select_select" class="form_ediht_Tarifs">
						<?php
						foreach($qry_quest_list as $quest){
							echo '<option value="'.$quest['quest_id'].'">'.htmlentities($quest['quest_nom']).'</option>';
						}
						?>
					  </select>
					  &nbsp;&nbsp; </td>
				  </tr>
				  <?php
				}
			  ?>
			  <tr> 
				<td align="left" class="TX"> 
				  <input type="radio" name="choix_action" value="creation">
				  Cr&eacute;er un questionnaire :&nbsp; &nbsp;</td>
				<td align="left" class="TX"> 
				  <input type="text" name="txt_quest" class="form_ediht_Tarifs" size="31" maxlength="50">
				  </td>
			  </tr>
			  <?php
			  if (is_array($qry_type_quest_list)){
				  ?>
				  <tr>
					<td align="left" class="TX" style="padding-left:24px"> 
					  Type de question :</td>
					<td align="left" class="TX"> 
					  <select name="select_type_question" class="form_ediht_Tarifs">
						<?php
						foreach($qry_type_quest_list as $type_question){
							echo '<option value="'.$type_question['code_id'].'">'.$type_question['code_libelle'].'</option>';
						}
						?>
					  </select>&nbsp; </td>
				  </tr>
				  <?php
			  }
			  if (is_array($qry_quest_list)){
			  ?>
			  <tr> 
				<td align="left" class="TX"> 
				  <input type="radio" name="choix_action" value="duplication">
				  Dupliquer un questionnaire :&nbsp; &nbsp;</td>
				<td align="left" class="TX"> 
				  <select name="select_duplic" class="form_ediht_Tarifs">
					<?php
					foreach($qry_quest_list as $quest){
						echo '<option value="'.$quest['quest_id'].'">'.htmlentities($quest['quest_nom']).'</option>';
					}
					?>
				  </select>
				  &nbsp;Nouveau libellé du questionnaire : 
				  <input type="text" name="nvo_nom_quest" class="form_ediht_Tarifs" size="30" maxlength="50">
				</td>
			  </tr>
			  <?php
			  }
			  ?>
			  <tr> 
				<td height="14"></td>
				<td height="14"></td>
			  </tr>
			</table>
		  </td>
		  <td width="14" height="14" align="right"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td align="right"></td>
		  <td width="14" height="14" align="right"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	  <br>
	  <p align="center">
			<input type="submit" name="ok_duplic" value="Valider" class="BN"><br>
	  </p>
	  </form>
	  <?php
	  if(is_array($qry_info_questionnaire)){
	   ?>
			<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="14" height="14">&nbsp;</td>
			</tr>
			</table>
				<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo htmlentities($qry_info_questionnaire[0]['quest_nom'])?></td>
				</tr>
				</table>
			  <table width="961" border="0" cellspacing="0" cellpadding="0"  align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<form method="post" action="#1" name="form1">
				<input type="hidden" name="posted" value="1" class="BN">
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">Nom du questionnaire : 
					<input type="text" name="nom_quest" class="form_ediht_Tarifs" size="50" maxlength="50" value="<?php echo htmlentities($qry_info_questionnaire[0]['quest_nom'])?>">
										<?php
					if (is_array($qry_type_quest_list)){
					?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Type de question : 
						  <select name="select_quest_type" class="form_ediht_Tarifs">
							<?php
							foreach($qry_type_quest_list as $type_quest){
								if ($type_quest['code_id']==$qry_info_questionnaire[0]['quest_type_question_code_id']){
									$selected = ' selected="selected"';
								}else{
									$selected = '';
								}
								echo '<option value="'.$type_quest['code_id'].'"'.$selected.'>'.$type_quest['code_libelle'].'</option>';
							}
							?>
						  </select>
					<?php
					}
					?>
						  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX"> 
					<input type="button" name="nvo_client" value="Ajouter une langue" class="bn_ajouter" onClick="MM_openBrWindow('supadmin_crea_lang.php?questid=<?php echo $qry_info_questionnaire[0]['quest_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=950,height=800')">
				  </td>
				  <td width="14"></td>
				</tr>
				<?php
				if (is_array($qry_info_lang_questionnaire)){
					?>
					<tr> 
					  <td width="14"></td>
					  <td align="left" class="TX"> 
						<table class="tabloGeneral">
						  <tr> 
							<td class="TX_bold" width="1">Langue</td>
							<td class="TX_bold">Titre</td>
							<td class="TX_bold">Commentaires</td>
							<td class="TX_bold" align="center" width="1">Modifier</td>
							<td class="TX_bold" align="center" width="1">Supprimer</td>
						  </tr>
						  <?php 
						  foreach ($qry_info_lang_questionnaire as $lang_questionnaire){
						  echo '<tr bgcolor="#ffffff" style="font-size:13px"> 
							<td align="left">'.$lang_questionnaire['lang_libelle'].'</td>
							<td>'.$lang_questionnaire['titre'].'</td>
							<td>
								<b>IN</b>&nbsp;:&nbsp;'.$lang_questionnaire['commentaire_in'].$lang_questionnaire['commentaire_in_2'].'
								<br><b>OUT</b>&nbsp;:&nbsp;'.$lang_questionnaire['commentaire_out'].'
							</td>
							<td align="center"><a href="#" onclick="MM_openBrWindow(\'supadmin_edit_lang.php?questid='. $qry_info_questionnaire[0]['quest_id'] .'&langid='.$lang_questionnaire['lang_id'].'\',\'\',\'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=950,height=800\')"><img src="modifier.png" border="0" width="20"></a></td>
							<td align="center"><a href="#" onclick="del_langue('.$lang_questionnaire['lang_id'].');"><img src="../images/icon_supp2.gif" border="0" width="11" height="12"></a></td>
						  </tr>';
						  }
						  ?>
						</table>
					  </td>
					  <td width="14"></td>
					</tr>
					<tr> 
					  <td width="14"></td>
					  <td align="left" class="TX">&nbsp;</td>
					  <td width="14"></td>
					</tr>
					<?php
				}
				?>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX"> 
					<input type="button" name="ajout" value="Ajouter une question" class="bn_ajouter" onClick="MM_openBrWindow('creation_question.php?questid=<?php echo $_GET['questid']?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=950,height=550')">
					&nbsp;&nbsp; 
					<?php
					if (!$_GET['choix_lang'] || $_GET['choix_lang']<2){
						$checked_fr = ' checked="checked"';
					}else{
						$checked_fr = '';
					}
					?>
					<input onClick="change_langue('1');" type="radio" name="choix_langue" value="1"<?php echo $checked_fr ?>>
					Francais&nbsp;&nbsp;&nbsp; 
					<?php
					if (is_array($qry_langue)){
						foreach($qry_langue as $langue){
							if ($_GET['choix_lang'] == $langue['lang_id']){
								$checked = ' checked="checked"';
							}else{
								$checked= '';
							}
							echo '<input onclick="change_langue(\''. $langue['lang_id'] .'\');" type="radio" name="choix_langue" value="'. $langue['lang_id'] . '"'. $checked .'>'.$langue['lang_libelle'].'&nbsp;&nbsp;&nbsp;';
						}
					}
					?>
				  </td>
				  <td width="14"></td>
				</tr>
				<?php
				if (is_array($qry_question)){
				?>
					<tr> 
					  <td width="14"></td>
					  <td align="left" class="TX"> 
						<table class="tabloGeneral">
						  <?php
				 		  	if($qry_info_questionnaire[0]['quest_type_question_code_id'] != 12000){							  
						  ?>						
						  <tr align="center"> 
							<td class="TX_bold">Ordre <br>
							  d'affichage</td>
							<td class="TX_bold">Question</td>
							<td class="TX_bold">Choix 1</td>
							<td class="TX_bold">Choix 2</td>
							<td class="TX_bold">Choix 3</td>
							<td class="TX_bold">Choix 4</td>
							<td class="TX_bold" align="center">Modifier</td>
							<td class="TX_bold" align="center">Sélection<br><input type="checkbox" name="select_all" onClick="che_all(document.form1.che_quest)" value="1">Tous</td>
						  </tr>
						  <?php
				 		  } else {
						  ?>
						  <tr align="center"> 
							<td class="TX_bold">Ordre <br>
							  d'affichage</td>
							<td class="TX_bold">Item</td>
							<td class="TX_bold">Th&egrave;me</td>
							<td class="TX_bold">Sous-th&egrave;me</td>
							<td class="TX_bold">Question Manager</td>
							<td class="TX_bold" align="center">Modifier</td>
							<td class="TX_bold" align="center">Sélection<br><input type="checkbox" name="select_all" onClick="che_all(document.form1.che_quest)" value="1">Tous</td>
						  </tr>						  
						  <?php
							}
						  ?>						  
						  <?php
				 		  	if($qry_info_questionnaire[0]['quest_type_question_code_id'] != 12000){							  
						  		foreach($qry_question as $question){

						  ?>
							  <tr style="background-color:#FFF"> 
								<td align="center" class="TX"> 
								  <input type="text" name="ordre_<?php echo $question['choix_id']?>" size="1" class="form_ediht_Tarifs" value="<?php echo $question['val_ordre'] ?>">
								</td>
								<td align="center" class="TX"><?php echo nl2br($question['choix_question'].'<br>'.$question['choix_question_comm']) ?>&nbsp;</td>
								<td class="TX"><?php if (trim($question['choix_1'])!='') echo '<b>'.nl2br($question['choix_1']).'</b><br />'.$question['val_pi_1'].'+ '.$question['val_pn_1'].'-' ?>&nbsp;</td>
								<td class="TX"><?php if (trim($question['choix_2'])!='') echo '<b>'.nl2br($question['choix_2']).'</b><br />'.$question['val_pi_2'].'+ '.$question['val_pn_2'].'-' ?>&nbsp;</td>
								<td class="TX"><?php if (trim($question['choix_3'])!='') echo '<b>'.nl2br($question['choix_3']).'</b><br />'.$question['val_pi_3'].'+ '.$question['val_pn_3'].'-' ?>&nbsp;</td>
								<td class="TX"><?php if (trim($question['choix_4'])!='') echo '<b>'.nl2br($question['choix_4']).'</b><br />'.$question['val_pi_4'].'+ '.$question['val_pn_4'].'-' ?>&nbsp;</td>
								<td class="TX" align="center"><img onMouseOver="this.style.cursor='pointer'" onClick="MM_openBrWindow('edit_question.php?questid=<?php echo $_GET['questid']?>&questionid=<?php echo $question['choix_id']?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=950,height=500')" src="modifier.png" border="0" width="20"></td>
								<td class="TX" align="center"><input type="checkbox" name="che_quest[]" id="che_quest" value="<?php echo $question['choix_id']?>"><input type="hidden" name="hid_choix_id[]" value="<?php echo $question['choix_id']?>"></td>
							  </tr>
							<?php
							}} else {
								foreach($qry_question as $question){
								$sql_bpm_quest = "SELECT * FROM BPM WHERE ITHEME_ID='".txt_db($question['code_id'])."'";
								$qry_bpm_quest = $db->query($sql_bpm_quest);

								$sql_theme_quest = "SELECT CODE_LIBELLE FROM CODE WHERE CODE_ID='".txt_db($qry_bpm_quest[0]['theme_id'])."'";
								$qry_theme_quest = $db->query($sql_theme_quest);

								$sql_stheme_quest = "SELECT CODE_LIBELLE FROM CODE WHERE CODE_ID='".txt_db($qry_bpm_quest[0]['stheme_id'])."'";
								$qry_stheme_quest = $db->query($sql_stheme_quest);																								

								$sql_item_quest = "SELECT CODE_LIBELLE FROM CODE WHERE CODE_ID='".txt_db($qry_bpm_quest[0]['itheme_id'])."'";
								$qry_item_quest = $db->query($sql_item_quest);
															
							?>
							  <tr style="background-color:#FFF"> 
								<td align="center" class="TX"> 
								  <input type="text" name="ordre_<?php echo $question['choix_id']?>" size="1" class="form_ediht_Tarifs" value="<?php echo $question['val_ordre'] ?>">
								</td>					
								<td class="TX"><?php if (trim($qry_theme_quest[0]['code_libelle'])!='') echo '<b>'.nl2br($qry_theme_quest[0]['code_libelle']).'</b>' ?>&nbsp;</td>
								<td class="TX"><?php if (trim($qry_stheme_quest[0]['code_libelle'])!='') echo '<b>'.nl2br($qry_stheme_quest[0]['code_libelle']).'</b>' ?>&nbsp;</td>
								<td class="TX"><?php if (trim($qry_item_quest[0]['code_libelle'])!='') echo '<b>'.nl2br($qry_item_quest[0]['code_libelle']).'</b>' ?>&nbsp;</td>								
								<td align="center" class="TX"><?php echo nl2br($question['choix_1']) ?>&nbsp;</td>
								<td class="TX" align="center"><img onMouseOver="this.style.cursor='pointer'" onClick="MM_openBrWindow('edit_question.php?questid=<?php echo $_GET['questid']?>&questionid=<?php echo $question['choix_id']?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=950,height=500')" src="modifier.png" border="0" width="20"></td>
								<td class="TX" align="center"><input type="checkbox" name="che_quest[]" id="che_quest" value="<?php echo $question['choix_id']?>"><input type="hidden" name="hid_choix_id[]" value="<?php echo $question['choix_id']?>"></td>
							  </tr>
							  <?php
								} }							
							  ?>	
							<tr style="background-color:#FFF"><td colspan="7">&nbsp;</td><td align="center"><input type="button" class="bn_ajouter" value="Supprimer la sélection" onClick="verif_del();"></td></tr>
						</table>
					  </td>
					  <td width="14"></td>
					</tr>
				<?php
				}
				?>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
			  <br>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr><td align="center">
					<input type="button" name="Submit" onClick="document.form1.submit();" value="Valider" class="BN">
				  </td></tr>
			  </table>
			  </form>
			  <br>
			<form action="#" name="form2" method="post">
			  <input type="hidden" name="del_questid" value="<?php echo $qry_info_questionnaire[0]['quest_id'] ?>">
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr><td align="center">
					<input type="button" onClick="verifier_del_quest('<?php echo addslashes(htmlentities($qry_info_questionnaire[0]['quest_nom'])) ?>');" name="Submit" value="Supprimer" class="BN">
				  </td></tr>
			  </table>
			  </form>
			  <br>
			  <?php
			  }
			  ?>
			</p></div>	</article></div>	</div>	</div>	</div>			  
			</body>
			</html>
		<?php
		}
	}
}else{
	include('no_acces.php');
}
?>
			<script type="text/javascript">

			function verifier_del_quest(quest_nom){
				if(confirm('<?php echo $t_supprimer_question_tabeau_1 ?> ' + quest_nom + '\r\n<?php echo $t_supprimer_question_tabeau_2 ?>')){
					document.form2.submit();
				}
			}
			
			function gd_public(){
				if (document.form1.grand_public.checked==true){
					document.getElementById('options_quest').style.display='inline';
				}else{
					document.getElementById('options_quest').style.display='none';
				}
			}			function verif_del(){
				if (confirm('<?php echo $t_supprimer_questions_select ?>')){
					document.form1.submit();
				}
			}
			
			
			function del_langue(langid){
				if (confirm('<?php echo $t_supprimer_textes ?>')){
					document.location.href='supadmin_crea_questionnaire2.php?questid=<?php echo $_GET['questid']?>&choix_lang<?php echo $_GET['choix_lang']?>&action=del_lang&langid='+langid;
				}
			}
			
			function che_all(field){
				if (document.form1.select_all.checked==true){
					field.checked=true;
					for (i=0;i<field.length;i++)
					{
					field[i].checked = true;
					}
				}
				if (document.form1.select_all.checked==false){
					field.checked=false;
					for (i=0;i<field.length;i++)
					{
					field[i].checked = false;
					}
				}
			}
			
			function delete_question(questionid){
				if (confirm('<?php echo $t_supprimer_question ?>')){
					document.location.href='supadmin_crea_questionnaire2.php?questid=<?php echo $_GET['questid']?>&choix_lang<?php echo $_GET['choix_lang']?>&action=del_question&questionid='+questionid;
				}
			}
						
			function change_langue(choix_lang){
				document.location.href='supadmin_crea_questionnaire2.php?questid=<?php echo $_GET['questid']?>&choix_lang='+choix_lang;
			}
			
			function MM_openBrWindow(theURL,winName,features) { //v2.0
			  window.open(theURL,winName,features);
			}
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			</script>