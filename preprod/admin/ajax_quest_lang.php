<?php
session_start();

if ($_GET['questid']>0){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);

	/* On fait la liste des langues disponibles */
	$sql_lang = "SELECT LANGUE.LANG_ID, LANGUE.LANG_LIBELLE FROM QUEST_A_LANG, LANGUE WHERE QUEST_A_LANG.LANG_ID=LANGUE.LANG_ID AND QUEST_A_LANG.QUEST_ID='".$_GET['questid']."' ORDER BY LANGUE.LANG_LIBELLE ASC";
	$qry_lang = $db->query($sql_lang);	
			
	header('Content-type: text/html; charset=UTF-8'); 

	$contenu = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	if (is_array($qry_lang)){
		foreach($qry_lang as $lang){
			$sql_value_lib = "SELECT * FROM GRAPHE_A_LIB WHERE LANG_ID='".txt_db(intval($lang['lang_id']))."'";
			$qry_value_lib = $db->query($sql_value_lib);
			$contenu .= '<tr> 
			  <td class="TX">'. $lang['lang_libelle'] .' :</td>
			  <td class="TX">
				<input type="hidden" name="langue[]" value="'. $lang['lang_id'] .'">
				<input type="text" name="'. $lang['lang_id'] .'_lib" class="form_ediht_Tarifs" size="100">
			  </td>
			</tr>';
		}
	}
	$contenu .= '</table>__';
	echo $contenu;
}
