<?php
session_start();
ini_set('memory_limit', '256M');
// Si l'utilisateur est un super admin
if ($_SESSION['droit']>0){

	if($_SESSION['droit']>4){
		$id_utilisateur = $_SESSION['vak_id'];
	}else{
		$id_utilisateur = $_SESSION['cert_id'];
	}

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	$lib_operateurs = array(
	1=>'>',
	2=>'>=',
	3=>'=',
	4=>'<',
	5=>'<=',
	6=>'LIKE',
	7=>'LIKE',
	8=>'IN',
	9=>'<>'
	);
	
	if($_GET['req_id']>0){
		
		
		##########################################################
		// On construit le SELECT la requête spécifiée dans l'URL
		$sql_sel_champs = "SELECT * FROM REQUETE_A_CHP, LISTE_CHP WHERE REQUETE_A_CHP.INDIC_TABLE=LISTE_CHP.INDIC_TABLE AND REQUETE_A_CHP.INDIC_CHP=LISTE_CHP.INDIC_CHP AND REQUETE_A_CHP.NUM_REQUETE=".intval($_GET['req_id'])." AND REQUETE_A_CHP.AFFICHE=1 order by requete_a_chp.ordre";
		$qry_sel_champs = $db->query($sql_sel_champs);
		
		$select_finale 	= "SELECT distinct ";
		$Entre_req_partcertcand = 0;
		$Entre_req_cand_ope = 0;
		$Entre_req_part_jeton = 0;
		$Entre_req_cert_jeton = 0;
		$Entre_req_cand_a_quest = 0;
		$Entre_req_type_profil = '';
		
		if(is_array($qry_sel_champs)){
			foreach($qry_sel_champs as $select){
				if ($select['type'] == 2) {//date
					//$select_finale 	.= "to_char(".$select['nom_table'].".".$select['nom_champ'].",'DD/MM/YYYY HH24:MI') ".$select['nom_champ']." , ";
					$select_finale 	.= $select['nom_table'].".".$select['nom_champ']." , ";
				}
				else
				{
					$select_finale 	.= $select['nom_table'].".".$select['nom_champ']." , ";
				}
				if ($select['nom_table'] == 'req_partcertcand'){
					$Entre_req_partcertcand = 1;
				}
				if ($select['nom_table'] == 'req_cand_ope'){
					$Entre_req_cand_ope = 1;
				}
				if ($select['nom_table'] == 'vue_cand_a_quest'){
					$Entre_req_cand_a_quest = 1;
				}
				if ($select['nom_table'] == 'req_part_jeton'){
					$Entre_req_part_jeton = 1;
				}
				if ($select['nom_table'] == 'req_cert_jeton'){
					$Entre_req_cert_jeton = 1;
				}
				if ($select['nom_table'] == 'cand_pi'){
					$Entre_req_type_profil = 'cand_pi';
				}elseif ($select['nom_table'] == 'cand_pn'){
					$Entre_req_type_profil = 'cand_pn';
				}elseif ($select['nom_table'] == 'cand_pr'){
					$Entre_req_type_profil = 'cand_pr';
				}elseif ($select['nom_table'] == 'cand_pnpi'){
					$Entre_req_type_profil = 'cand_pnpi';
				}elseif ($select['nom_table'] == 'cand_pnpr'){
					$Entre_req_type_profil = 'cand_pnpr';
				}elseif ($select['nom_table'] == 'cand_prpi'){
					$Entre_req_type_profil = 'cand_prpi';
				}
			}
			$select_finale = substr($select_finale,0,(strlen($select_finale)-2));
		}
		##########################################################
		
		if ($Entre_req_partcertcand == 0)
		{
			$from_finale 	= "FROM req_cand_ope ";
		}
		elseif ($Entre_req_cand_ope == 0)
		{
			$from_finale 	= "FROM req_partcertcand ";
		}
		else
		{
			$from_finale 	= "FROM req_partcertcand,req_cand_ope ";

		}
		if ($Entre_req_type_profil != ''){
			$from_finale = $from_finale.",".$Entre_req_type_profil." ";
		}
		if ($Entre_req_part_jeton == 1){
			$from_finale = $from_finale.",req_part_jeton ";
		}
		if ($Entre_req_cert_jeton == 1){
			$from_finale = $from_finale.",req_cert_jeton ";
		}
		if ($Entre_req_cand_a_quest == 1){
			$from_finale = $from_finale.",vue_cand_a_quest ";
		}
		
		
		##########################################################
		// On construit le WHERE la requête spécifiée dans l'URL
		$sql_sel_req_filtre = "SELECT * FROM REQUETE_A_FILTRE, LISTE_CHP WHERE REQUETE_A_FILTRE.INDIC_TABLE=LISTE_CHP.INDIC_TABLE AND REQUETE_A_FILTRE.INDIC_CHP=LISTE_CHP.INDIC_CHP AND REQUETE_A_FILTRE.NUM_REQUETE=".intval($_GET['req_id']);
		$qry_sel_req_filtre = $db->query($sql_sel_req_filtre);
		
		if ($Entre_req_partcertcand == 0)
		{
			$where_finale 	= "WHERE 1=1 ";
		}
		elseif ($Entre_req_cand_ope == 0)
		{
			if ($id_utilisateur != 1)
				$where_finale 	= "WHERE CERT_ID=".$id_utilisateur." ";
			else
				$where_finale 	= "WHERE 1=1 ";			
		}
		else
		{
			$where_finale 	= "WHERE req_partcertcand.cand_id=req_cand_ope.cand_id ";
		}		

		if ($Entre_req_type_profil != ''){
			$where_finale = $where_finale." and req_cand_ope.cand_id=".$Entre_req_type_profil.".cand_id and req_cand_ope.ope_id=".$Entre_req_type_profil.".ope_id ";
		}
		if ($Entre_req_part_jeton == 1){
			$where_finale = $where_finale." and req_partcertcand.part_id=req_part_jeton.part_id ";
		}
		if ($Entre_req_cert_jeton == 1){
			$where_finale = $where_finale." and req_partcertcand.cert_id=req_cert_jeton.cert_jet_cert_id ";
		}
		if ($Entre_req_cand_a_quest == 1){
			$where_finale = $where_finale." and req_cand_ope.cand_id=vue_cand_a_quest.cand_id and req_cand_ope.ope_id=vue_cand_a_quest.ope_id ";
		}
		
		/* Sélection des champs à trier */
		$sql_sel_order = "SELECT * FROM REQUETE_A_CHP, LISTE_CHP WHERE REQUETE_A_CHP.TRI=1 AND REQUETE_A_CHP.INDIC_TABLE=LISTE_CHP.INDIC_TABLE AND REQUETE_A_CHP.INDIC_CHP=LISTE_CHP.INDIC_CHP AND REQUETE_A_CHP.NUM_REQUETE=".intval($_GET['req_id'])." order by requete_a_chp.ordre";
		$qry_sel_order = $db->query($sql_sel_order);
		
		$order_finale = "";
		if(is_array($qry_sel_order)){
			$order_finale = "ORDER BY ";
			foreach($qry_sel_order as $champ_order){
				//$order_finale .= $champ_order['nom_table'].'.'.$champ_order['nom_champ'].', ';
				$order_finale .= $champ_order['nom_champ'].', ';
			}
			
			$order_finale = substr($order_finale,0,(strlen($order_finale)-2));
		}
		
		if(is_array($qry_sel_req_filtre)){
			foreach($qry_sel_req_filtre as $where){
				if ($where['type'] == 1 || $where['type'] == 3) {//MAJ
					$where_finale 	.= "AND upper(".$where['nom_table'].".".$where['nom_champ'].") ";
				}
				elseif ($where['type'] == 2) {//DATE
					$where_finale 	.= "AND to_char(".$where['nom_table'].".".$where['nom_champ'].",'YYYYMMDD') ";
				}
				else
				{
					$where_finale 	.= "AND ".$where['nom_table'].".".$where['nom_champ']." ";
				}
				
				$where_finale	.= $lib_operateurs[$where['operateur']]." ";
				if ($where['type'] == 1 || $where['type'] == 3) {//MAJ
					$where_finale	.= "upper(";
				}
				// Début d'encadrement du critere
				if($where['operateur']<6 || $where['operateur']==7 || $where['operateur']==9){
					$where_finale	.= "'";
				}
				if($where['operateur']==6){
					$where_finale	.= "'%";
				}elseif($where['operateur']==8){
					$where_finale	.= "('";					
				}
				
				// Ecriture du critere
				if($where['operateur']==8){
					$where_finale	.= str_replace(",", "','",txt_db($where['critere']));
				}else{				
					if ($where['type'] == 2) {//DATE				
						$where_finale	.= txt_db(substr($where['critere'],6,4).substr($where['critere'],3,2).substr($where['critere'],0,2));
					}
					else
					{
						$where_finale	.= txt_db($where['critere']);
					}
				}
				
				// Fin d'encadrement du critere
				if($where['operateur']<6 || $where['operateur']==9){
					$where_finale	.= "'";
				} 
				if($where['operateur']==6 || $where['operateur']==7){
					$where_finale	.= "%'";
				}elseif($where['operateur']==8){
					$where_finale	.= "')";
				}
				if ($where['type'] == 1 || $where['type'] == 3) {//MAJ
					$where_finale	.= ")";
				}

				$where_finale	.= " ";
			}
		}
		##########################################################
		
		$requete_finale = $select_finale.$from_finale.$where_finale.$order_finale;
	}
	echo $id_utilisateur.' '.$requete_finale;

/* EXPORT DES DONNEES EN CSV */

	$sql=ociparse($conn,$requete_finale);
	ociexecute($sql);

	$csv = '';
	$numcols=ocinumcols($sql);
	$str_sortie = '<table width="100%" border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000">';
		for ($nbCol0=1; $nbCol0<($numcols+1); $nbCol0++){
			$sql_sel_lib_chp = "SELECT lib_champ FROM LISTE_CHP WHERE NOM_CHAMP='".ocicolumnname($sql, $nbCol0)."'";
			//echo $sql_sel_lib_chp;
			$qry_sel_lib_chp = $db->query($sql_sel_lib_chp);		
			$str_sortie .= '<td align="center" class="TX_bold">'.$qry_sel_lib_chp[0]['lib_champ'].'</td>';
			$csv[] = $qry_sel_lib_chp[0]['lib_champ'];
		}
	while(ocifetch($sql)){
		$str_sortie .= '<tr>';
		for ($nbCol=1; $nbCol<($numcols+1); $nbCol++){
			$str_sortie .= '<td align="center" class="TX_bold" style="background-color:#FFFFFF;">'.stripslashes(ociresult($sql, $nbCol)).'</td>';
		}
		$str_sortie .= '<tr>';
	}
	$str_sortie .= '</table>';
	
	if($_GET['export']){
		$path = "csv/";
		$sql_nom="select lib_requete from USER_REQUETE where num_requete='".intval($_GET['req_id'])."'";
		$qry_nom = $db->query($sql_nom);
		//$filename = mt_rand().strftime("%y%m%d").'.csv';
		$filename = $qry_nom[0]['lib_requete'].'.csv';
		$file = fopen($path.$filename, "w");
		$sql2=ociparse($conn,$requete_finale);
		$sqlExecute = ociexecute($sql2);
		while (($oracleArray = oci_fetch_assoc($sql2)))
			$oracleOutput[] = $oracleArray;
		if (empty($oracleOutput))
		{
			?>
			<script language="Javascript">
			history.go(-1);
			alert('Aucun résultat.');
			</script>
			<?php
		}
		else
		{
			fputcsv($file, $csv, ';');
			foreach($oracleOutput as $line){
				foreach($line as $key => $data)
					$line[$key] = stripslashes($data);
				fputcsv($file, $line, ';');
			}
			fclose($file);

			$type = "text/csv";
			header("Content-disposition: attachment; filename=".$filename."");
			header("Content-Type: application/force-download");
			header("Content-Transfer-Encoding: ".$type."\n");
			header("Content-Length: ".filesize($path.$filename));
			header("Pragma: no-cache");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
			header("Expires: 0");
			readfile($path.$filename);
			exit;
		}
	}
	?>
	
	<html>
	<head>
	<title>Vakom - Requetes</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">

	<script language="JavaScript">
	<!--
	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
	<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
	  <tr> 
		<td width="20">&nbsp;</td>
		<td class="Titre_Requetes"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;RESULTATS</td>
	  </tr>
	</table>
	<br>
	<table border="0" cellspacing="0" cellpadding="0" align="center">
	  <tr> 
		<td align="left" class="TX"> 
		  <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="800">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="left" valign="top" class="TX"> <br>
				<?php echo $str_sortie ?>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
		</td>
	  </tr>
	  <tr> 
		<td align="center">&nbsp;</td>
	  </tr>
	  <tr> 
		<td align="center"> 
		  <input type="button" name="Submit" value="EXPORTER " class="bn_valider_requete" onClick="window.location='requetes_apercu.php?req_id=<?php echo $_GET['req_id'] ?>&export=1'">
		</td>
	  </tr>
	  <tr> 
		<td align="right" width="180">&nbsp; </td>
	  </tr>
	</table>
	</body>
	</html>
<?php
}else{
	include('no_acces.php');
}
?>