<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']==9 && $_GET['questid']!='' && $_GET['questionid']!=''){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	$db = new db($conn);
	
	if ($_GET['choix_lang']){
		$choix_lang		= intval($_GET['choix_lang']);
	}else{
		$choix_lang		= 2; // Anglais par défaut
	}
		
	if ($_POST['posted'] && trim($_POST['question'])!=''){
		
		if(isset($_POST['quest_type_question_code_id']) && intval($_POST['quest_type_question_code_id']) == 12000)
		{
			$sql_up_question_fr = "UPDATE CHOIX SET CODE_ID=".intval(txt_db($_POST['item_selected'])).",
			CHOIX_1 = '".txt_db($_POST['question'])."',
			CHOIX_2 = '".txt_db($_POST['question2'])."',
			CHOIX_3 = '".txt_db($_POST['question3'])."' WHERE CHOIX_ID='".intval(txt_db($_GET['questionid']))."' AND CHOIX_LANG_ID='1'";
		}
		else {
		
			$sql_up_question_fr = "UPDATE CHOIX SET CHOIX_QUESTION='".txt_db($_POST['question'])."',
			CHOIX_QUESTION_COMM = '".txt_db($_POST['question_comm'])."',
			CHOIX_1 = '".txt_db($_POST['choix1'])."',
			CHOIX_2 = '".txt_db($_POST['choix2'])."',
			CHOIX_3 = '".txt_db($_POST['choix3'])."',
			CHOIX_4 = '".txt_db($_POST['choix4'])."' WHERE CHOIX_ID='".intval(txt_db($_GET['questionid']))."' AND CHOIX_LANG_ID='1'";
		
		}
		//echo $sql_up_question_fr;
		$qry_up_question_fr = $db->query($sql_up_question_fr);
		
		$sql_update_val = "UPDATE QUEST_A_VAL SET VAL_PI_1='".txt_db($_POST['pi1'])."',
		VAL_PI_2='".txt_db($_POST['pi2'])."',
		VAL_PI_3='".txt_db($_POST['pi3'])."',
		VAL_PI_4='".txt_db($_POST['pi4'])."',
		VAL_PN_1='".txt_db($_POST['pn1'])."',
		VAL_PN_2='".txt_db($_POST['pn2'])."',
		VAL_PN_3='".txt_db($_POST['pn3'])."',
		VAL_PN_4='".txt_db($_POST['pn4'])."',
		VAL_USER_MODIFICATION='".txt_db($_SESSION['vak_id'])."',
		VAL_DATE_MODIFICATION=SYSDATE WHERE VAL_QUEST_ID='".intval(txt_db($_GET['questid']))."' AND VAL_CHOIX_ID='".intval(txt_db($_GET['questionid']))."'";
		
		//echo $sql_update_val;
		$qry_update_val = $db->query($sql_update_val);
		
		if (trim($_POST['question2'])!=''){
			if ($_POST['exist']>0){ // Si la question existe déjà dans la langue choisie, on update
				$sql_up_question_e = "UPDATE CHOIX SET CHOIX_QUESTION='".txt_db($_POST['question2'])."',
				CHOIX_QUESTION_COMM = '".txt_db($_POST['question2_comm'])."',
				CHOIX_1 = '".txt_db($_POST['choix12'])."',
				CHOIX_2 = '".txt_db($_POST['choix22'])."',
				CHOIX_3 = '".txt_db($_POST['choix32'])."',
				CHOIX_4 = '".txt_db($_POST['choix42'])."' 
				WHERE CHOIX_ID='".intval(txt_db($_POST['exist']))."' AND CHOIX_LANG_ID='".txt_db($_POST['lang'])."'";
				
				//echo $sql_up_question_e;
				$qry_up_question_e = $db->query($sql_up_question_e);

		
			}else{ // Sinon on insert
				// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
				$sql_seq_num 	= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
				$qry_seq_num 	= $db->query($sql_seq_num);
				$seq_num		= intval($qry_seq_num[0]['seq_num']);
				
				$sql_insert_question_e = "INSERT INTO CHOIX (CHOIX_ID,CHOIX_LANG_ID,CHOIX_QUESTION,CHOIX_QUESTION_COMM,CHOIX_1,CHOIX_2,CHOIX_3,
				CHOIX_4) VALUES('".intval(txt_db($_GET['questionid']))."',
				'".txt_db($_POST['lang'])."',
				'".txt_db($_POST['question2'])."',
				'".txt_db($_POST['question2_comm'])."',
				'".txt_db($_POST['choix12'])."',
				'".txt_db($_POST['choix22'])."',
				'".txt_db($_POST['choix32'])."',
				'".txt_db($_POST['choix42'])."')";
				
				//echo $sql_insert_question_e;
				$qry_insert_question_e = $db->query($sql_insert_question_e);
			}
		}
		
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				//document.location.href='edit_question.php?questionid=<?php echo $seq_num ?>&questid=<?php echo $_GET['questid'] ?>;
				window.close();
			</script>		
			<?php
	}
		
		$sql_info_quest = "SELECT * FROM QUESTIONNAIRE WHERE QUEST_ID='".txt_db($_GET['questid'])."'";
		$qry_info_quest = $db->query($sql_info_quest);

		$sql_info_question_fr = "SELECT * FROM CHOIX, QUEST_A_VAL WHERE CHOIX.CHOIX_ID=QUEST_A_VAL.VAL_CHOIX_ID AND CHOIX.CHOIX_ID='".txt_db($_GET['questionid'])."' AND CHOIX_LANG_ID='1'";
		$qry_info_question_fr = $db->query($sql_info_question_fr);
		
		$sql_info_question_e = "SELECT * FROM CHOIX, QUEST_A_VAL WHERE CHOIX.CHOIX_ID=QUEST_A_VAL.VAL_CHOIX_ID AND CHOIX.CHOIX_ID='".txt_db($_GET['questionid'])."' AND CHOIX_LANG_ID='".intval(txt_db($choix_lang))."'";
		$qry_info_question_e = $db->query($sql_info_question_e);
		
		$sql_langue_list = "SELECT LANGUE.LANG_ID, LANGUE.LANG_LIBELLE FROM LANGUE, QUEST_A_LANG WHERE QUEST_A_LANG.LANG_ID=LANGUE.LANG_ID AND QUEST_A_LANG.QUEST_ID='".txt_db($_GET['questid'])."' AND LANGUE.LANG_ID NOT IN ('1')";
		$qry_langue_list = $db->query($sql_langue_list);
		
		$sql_theme_quest = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='BPM_THEME' AND CODE_LANG_ID=1 and CODE_ACTIF=1 ORDER BY CODE_ORDRE";
		$qry_theme_quest = $db->query($sql_theme_quest);
				
		if($qry_info_quest[0]['quest_type_question_code_id'] == 12000){	
				$sql_info_question_fr = "SELECT * FROM CHOIX WHERE CHOIX.CHOIX_ID='".txt_db($_GET['questionid'])."' AND CHOIX_LANG_ID='1'";
				$qry_info_question_fr = $db->query($sql_info_question_fr);
						
				$sql_bpm_quest = "SELECT * FROM BPM WHERE ITHEME_ID='".txt_db($qry_info_question_fr[0]['code_id'])."'";
				$qry_bpm_quest = $db->query($sql_bpm_quest);			
				
				$theme_id = $qry_bpm_quest[0]['theme_id'];
				$stheme_id = $qry_bpm_quest[0]['stheme_id'];
				$itheme_id = $qry_bpm_quest[0]['itheme_id'];
															
				$sql_theme_quest = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='BPM_THEME' AND CODE_LANG_ID=1 and CODE_ACTIF=1 ORDER BY CODE_ORDRE";
				$qry_theme_quest = $db->query($sql_theme_quest);
		
 				$sql_sthemes = "SELECT distinct CODE.CODE_ID, CODE.CODE_LIBELLE, CODE.CODE_ORDRE FROM CODE,BPM where BPM.THEME_ID='".$theme_id."' AND CODE.CODE_ID=BPM.STHEME_ID AND CODE.CODE_TABLE='BPM_STHEME' AND CODE.CODE_LANG_ID=1 and CODE.CODE_ACTIF=1 ORDER BY CODE.CODE_ORDRE";
				$qry_sthemes = $db->query($sql_sthemes);	
			
			 	$sql_ithemes = "SELECT CODE_ID, CODE_LIBELLE FROM CODE,BPM where BPM.STHEME_ID='".$stheme_id."' AND CODE.CODE_ID=BPM.ITHEME_ID AND CODE.CODE_TABLE='BPM_ITHEME' AND CODE.CODE_LANG_ID=1 and CODE.CODE_ACTIF=1 ORDER BY CODE.CODE_ORDRE";
				$qry_ithemes = $db->query($sql_ithemes);					
										
		}				
		if (is_array($qry_info_quest) && is_array($qry_info_question_fr)){
		
			if (is_array($qry_info_question_e)){
				$question_e = $qry_info_question_e[0]['choix_question'];
				$question_e_comm = $qry_info_question_e[0]['choix_question_comm'];
				$choix_e_1 	= $qry_info_question_e[0]['choix_1'];
				$choix_e_2 	= $qry_info_question_e[0]['choix_2'];
				$choix_e_3 	= $qry_info_question_e[0]['choix_3'];
				$choix_e_4 	= $qry_info_question_e[0]['choix_4'];		
				$exist_e	= $qry_info_question_e[0]['choix_id'];		
			}
			?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--
			function tester_question(){
				var question = document.getElementById('question').value;
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_test_question.php"; // La page qui réceptionne les données
				var data     = null; 

				var xhr_object = null; 
					 
				if(window.XMLHttpRequest) // Firefox 
				   xhr_object = new XMLHttpRequest(); 
				else if(window.ActiveXObject) // Internet Explorer 
				   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
				else { // XMLHttpRequest non supporté par le navigateur 
				   alert("<?php echo $t_browser_support_error_1 ?>");
				   return; 
				} 
				 

				data = "question="+question+"&questid=<?php echo $_GET['questid'] ?>&questionid=<?php echo $_GET['questionid'] ?>";
				
				if(method == "GET" && data != null) {
				   filename += "?"+data;
				   data      = null;
				}
				 
				xhr_object.open(method, filename, true);

				xhr_object.onreadystatechange = function() {
				   if(xhr_object.readyState == 4) {
					  var tmp = xhr_object.responseText.split(":"); 
					  //if(typeof(tmp[0]) != "undefined") { 
						//if(tmp[0]>0){
						//	alert('Question déjà existante.');
						//}else{
							document.form.submit();
						//}
					  //}
				   }
				} 

				xhr_object.send(data); //On envoie les données
			}
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function change_lang(langid){
				document.location.href='edit_question.php?questionid=<?php echo $_GET['questionid'] ?>&questid=<?php echo $_GET['questid'] ?>&choix_lang='+langid;
			}
			//-->
			</script>
			</head>

			<body bgcolor="#FFFFFF" text="#000000">
			<?php 			
			if (is_array($qry_info_quest) && $qry_info_quest[0]['quest_type_question_code_id'] != 12000) { ?>
			<form method="post" action="#" name="form">
				<table width="830" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;QUESTIONNAIRE <?php echo strtoupper($qry_info_quest[0]['quest_nom']) ?></td>
				</tr>
				</table>
			  <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="830" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td class="TX_Tarifs" colspan="5">Modification&nbsp;d'une&nbsp;question (N° <?php echo $qry_info_question_fr[0]['val_ordre'] ?>)</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td width="14"></td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">Langue : </td>
				  <td align="left" class="TX">Français</td>
				  <td align="left" class="TX"> 
					<?php
					if (is_array($qry_langue_list)){
						?>
						<select onchange="change_lang(document.getElementById('lang').options[document.getElementById('lang').selectedIndex].value);" id="lang" name="lang" class="form_ediht_Tarifs">
							<?php
							foreach ($qry_langue_list as $langue){
								if ($langue['lang_id']==$choix_lang){
									$selected_l = ' selected="selected"';
								}else{
									$selected_l = '';
								}
								echo '<option value="'.$langue['lang_id'].'"'.$selected_l.'>'.$langue['lang_libelle'].'</option>';
							}
							?>
						</select>
						<?php
					}
					?>
				  </td>
				  <td align="center">&nbsp; </td>
				  <td align="left">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">Question :</td>
				  <td align="left"> 
					<textarea name="question" id="question" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"><?php echo htmlentities($qry_info_question_fr[0]['choix_question']) ?></textarea>
				  </td>
				  <td align="left"> 
					<textarea name="question2" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"><?php echo htmlentities($question_e) ?></textarea>
				  </td>
				  <td align="left">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">Commentaire :</td>
				  <td align="left"> 
					<textarea name="question_comm" id="question_comm" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"><?php echo htmlentities($qry_info_question_fr[0]['choix_question_comm']) ?></textarea>
				  </td>
				  <td align="left"> 
					<textarea name="question2_comm" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"><?php echo htmlentities($question_e_comm) ?></textarea>
				  </td>
				  <td align="left">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" valign="top" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td align="center">+</td>
  				  <?php
				  if ($qry_info_quest[0]['quest_type_question_code_id'] != 23) 
				  {
				  ?>
				  <td align="center">-</td>
  				  <?php
				  }
				  ?>				  
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="40"></td>
				  <td align="left" class="TX" height="40">Choix 1 :</td>
				  <td align="left" height="40"> 
					<textarea name="choix1" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"><?php echo htmlentities($qry_info_question_fr[0]['choix_1']) ?></textarea>
				  </td>
				  <td align="left" height="40"> 
					<textarea name="choix12" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"><?php echo htmlentities($choix_e_1) ?></textarea>
				  </td>
				  <td align="center" height="40"> 
					<select name="pi1" class="form_ediht_Tarifs">
					  <option value="E" <?php if ($qry_info_question_fr[0]['val_pi_1']=='E') echo ' selected="selected "'?>>E</option>
					  <option value="P"<?php if ($qry_info_question_fr[0]['val_pi_1']=='P') echo ' selected="selected "'?>>P</option>
					  <option value="C"<?php if ($qry_info_question_fr[0]['val_pi_1']=='C') echo ' selected="selected "'?>>C</option>
					  <option value="A"<?php if ($qry_info_question_fr[0]['val_pi_1']=='A') echo ' selected="selected "'?>>A</option>
					  <option value="N"<?php if ($qry_info_question_fr[0]['val_pi_1']=='N') echo ' selected="selected "'?>>N</option>
					</select>
				  </td>
				  <td align="center" height="40"> 
					<select name="pn1" class="form_ediht_Tarifs">
					  <option value="E" <?php if ($qry_info_question_fr[0]['val_pn_1']=='E') echo ' selected="selected "'?>>E</option>
					  <option value="P"<?php if ($qry_info_question_fr[0]['val_pn_1']=='P') echo ' selected="selected "'?>>P</option>
					  <option value="C"<?php if ($qry_info_question_fr[0]['val_pn_1']=='C') echo ' selected="selected "'?>>C</option>
					  <option value="A"<?php if ($qry_info_question_fr[0]['val_pn_1']=='A') echo ' selected="selected "'?>>A</option>
					  <option value="N"<?php if ($qry_info_question_fr[0]['val_pn_1']=='N') echo ' selected="selected "'?>>N</option>
					</select>
				  </td>
				  <td width="14" height="40"></td>
				</tr>
				<tr> 
				  <td width="14" height="40"></td>
				  <td align="left" class="TX" height="40">Choix 2 :</td>
				  <td align="left" height="40"> 
					<textarea name="choix2" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"><?php echo htmlentities($qry_info_question_fr[0]['choix_2']) ?></textarea>
				  </td>
				  <td align="left" height="40"> 
					<textarea name="choix22" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"><?php echo htmlentities($choix_e_2) ?></textarea>
				  </td>
				  <td align="center" height="40"> 
					<select name="pi2" class="form_ediht_Tarifs">
					  <option value="E" <?php if ($qry_info_question_fr[0]['val_pi_2']=='E') echo ' selected="selected "'?>>E</option>
					  <option value="P"<?php if ($qry_info_question_fr[0]['val_pi_2']=='P') echo ' selected="selected "'?>>P</option>
					  <option value="C"<?php if ($qry_info_question_fr[0]['val_pi_2']=='C') echo ' selected="selected "'?>>C</option>
					  <option value="A"<?php if ($qry_info_question_fr[0]['val_pi_2']=='A') echo ' selected="selected "'?>>A</option>
					  <option value="N"<?php if ($qry_info_question_fr[0]['val_pi_2']=='N') echo ' selected="selected "'?>>N</option>
					</select>
				  </td>
				  <td align="center" height="40"> 
					<select name="pn2" class="form_ediht_Tarifs">
					  <option value="E" <?php if ($qry_info_question_fr[0]['val_pn_2']=='E') echo ' selected="selected "'?>>E</option>
					  <option value="P"<?php if ($qry_info_question_fr[0]['val_pn_2']=='P') echo ' selected="selected "'?>>P</option>
					  <option value="C"<?php if ($qry_info_question_fr[0]['val_pn_2']=='C') echo ' selected="selected "'?>>C</option>
					  <option value="A"<?php if ($qry_info_question_fr[0]['val_pn_2']=='A') echo ' selected="selected "'?>>A</option>
					  <option value="N"<?php if ($qry_info_question_fr[0]['val_pn_2']=='N') echo ' selected="selected "'?>>N</option>
					</select>
				  </td>
				  <td width="14" height="40"></td>
				</tr>
				<tr> 
				  <td width="14" height="40"></td>
				  <td align="left" class="TX" height="40">Choix 3 :</td>
				  <td align="left" height="40"> 
					<textarea name="choix3" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"><?php echo htmlentities($qry_info_question_fr[0]['choix_3']) ?></textarea>
				  </td>
				  <td align="left" height="40"> 
					<textarea name="choix32" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"><?php echo htmlentities($choix_e_3) ?></textarea>
				  </td>
				  <td align="center" height="40"> 
					<select name="pi3" class="form_ediht_Tarifs">
					  <option value="E" <?php if ($qry_info_question_fr[0]['val_pi_3']=='E') echo ' selected="selected "'?>>E</option>
					  <option value="P"<?php if ($qry_info_question_fr[0]['val_pi_3']=='P') echo ' selected="selected "'?>>P</option>
					  <option value="C"<?php if ($qry_info_question_fr[0]['val_pi_3']=='C') echo ' selected="selected "'?>>C</option>
					  <option value="A"<?php if ($qry_info_question_fr[0]['val_pi_3']=='A') echo ' selected="selected "'?>>A</option>
					  <option value="N"<?php if ($qry_info_question_fr[0]['val_pi_3']=='N') echo ' selected="selected "'?>>N</option>
					</select>
				  </td>
				  <td align="center" height="40"> 
					<select name="pn3" class="form_ediht_Tarifs">
					  <option value="E" <?php if ($qry_info_question_fr[0]['val_pn_3']=='E') echo ' selected="selected "'?>>E</option>
					  <option value="P"<?php if ($qry_info_question_fr[0]['val_pn_3']=='P') echo ' selected="selected "'?>>P</option>
					  <option value="C"<?php if ($qry_info_question_fr[0]['val_pn_3']=='C') echo ' selected="selected "'?>>C</option>
					  <option value="A"<?php if ($qry_info_question_fr[0]['val_pn_3']=='A') echo ' selected="selected "'?>>A</option>
					  <option value="N"<?php if ($qry_info_question_fr[0]['val_pn_3']=='N') echo ' selected="selected "'?>>N</option>
					</select>
				  </td>
				  <td width="14" height="40"></td>
				</tr>
				<tr> 
				  <td width="14" height="40"></td>
				  <td align="left" class="TX" height="40">Choix 4 :</td>
				  <td align="left" height="40"> 
					<textarea name="choix4" class="form_ediht_Tarifs" maxlength="255" size="40" cols="30" style="height: 50px;"><?php echo htmlentities($qry_info_question_fr[0]['choix_4']) ?></textarea>
				  </td>
				  <td align="left" height="40"> 
					<textarea type="text" name="choix42" class="form_ediht_Tarifs" maxlength="255" size="40" cols="30" style="height: 50px;"><?php echo htmlentities($choix_e_4) ?></textarea>
				  </td>
				  <td align="center" height="40"> 
					<select name="pi4" class="form_ediht_Tarifs">
					  <option value="E" <?php if ($qry_info_question_fr[0]['val_pi_4']=='E') echo ' selected="selected "'?>>E</option>
					  <option value="P"<?php if ($qry_info_question_fr[0]['val_pi_4']=='P') echo ' selected="selected "'?>>P</option>
					  <option value="C"<?php if ($qry_info_question_fr[0]['val_pi_4']=='C') echo ' selected="selected "'?>>C</option>
					  <option value="A"<?php if ($qry_info_question_fr[0]['val_pi_4']=='A') echo ' selected="selected "'?>>A</option>
					  <option value="N"<?php if ($qry_info_question_fr[0]['val_pi_4']=='N') echo ' selected="selected "'?>>N</option>
					</select>
				  </td>
				  <td align="center" height="40"> 
					<select name="pn4" class="form_ediht_Tarifs">
					  <option value="E" <?php if ($qry_info_question_fr[0]['val_pn_4']=='E') echo ' selected="selected "'?>>E</option>
					  <option value="P"<?php if ($qry_info_question_fr[0]['val_pn_4']=='P') echo ' selected="selected "'?>>P</option>
					  <option value="C"<?php if ($qry_info_question_fr[0]['val_pn_4']=='C') echo ' selected="selected "'?>>C</option>
					  <option value="A"<?php if ($qry_info_question_fr[0]['val_pn_4']=='A') echo ' selected="selected "'?>>A</option>
					  <option value="N"<?php if ($qry_info_question_fr[0]['val_pn_4']=='N') echo ' selected="selected "'?>>N</option>
					</select>
				  </td>
				  <td width="14" height="40"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
			  <br>
			  <table border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td align="center"> 
					<input type="hidden" name="exist" value="<?php echo intval($exist_e)?>">
					<input type="hidden" name="posted" value="1">
					<input type="button" onclick="tester_question();" name="Submit" value="Valider" class="BN">
				  </td>
				</tr>
			  </table>
			</form>
			<?php } else { ?>			
			<form name="form" method="post" action="#">
				<input type="hidden" name="quest_type_question_code_id" value="<?php echo $qry_info_quest[0]['quest_type_question_code_id']; ?>">
				<input type="hidden" name="item_selected" id="item_selected" value="<?php echo $itheme_id; ?>">												
				<table width="830" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;QUESTIONNAIRE BPM</td>
				</tr>
				</table>
				
				
			  <table border="0" cellspacing="0" cellpadding="0"  width="830" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td class="TX_Tarifs" colspan="3">Ajout&nbsp;d'une&nbsp;question</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td width="14"></td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">Langue : </td>
				  <td colspan="2" align="left"><select name="langue" id="langue" class="form_ediht_Tarifs">
					  <option value="">Français</option>
					  <option value="">Anglais</option>
					  <option value="">Allemand</option>
					  <option value="">Italien</option>
					</select>			      </td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">Thème :</td>
				  <td align="left"><span class="TX">
				    <select name="theme" id="theme" class="form_ediht_Tarifs">
				    <?php
						foreach ($qry_theme_quest as $qry_theme){
							if ( $qry_theme['code_id'] == $theme_id){
								$selected = 'selected';
							} else {
								$selected = '';
							}
							echo '<option ' . $selected . ' value="'.$qry_theme['code_id'].'">'.$qry_theme['code_libelle'].'</option>';
						}
					?>
			      </select>
				  </span></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">Sous-thème : </td>
				  <td align="left"><span class="TX">
				    <select name="stheme" id="stheme" class="form_ediht_Tarifs">
					<?php 
						foreach($qry_sthemes as $qry_stheme){
							if ( $qry_stheme['code_id'] == $stheme_id){
								$selected = 'selected';
							} else {
								$selected = '';
							}						
							echo "<option " . $selected . " id='".$qry_stheme['code_id']."'>".$qry_stheme['code_libelle']."</opion>";		
						}						
					?>					
				    </select>
				  </span></td>
				  <td align="left" class="TX">Item : 
				    <select name="item" id="item" class="form_ediht_Tarifs">	
					<?php 
						foreach($qry_ithemes as $qry_itheme){
							if ( $qry_itheme['code_id'] == $itheme_id){
								$selected = 'selected';
							} else {
								$selected = '';
							}											
							echo "<option " . $selected . " id='".$qry_itheme['code_id']."'>".$qry_itheme['code_libelle']."</opion>";		
						}						
					?>						    			      
	              </select></td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">Question : </td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">Manager</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left" class="TX"> 
					<textarea name="question" id="question" class="form_ediht_Tarifs" style="height: 50px; width: 90%"><?php echo htmlentities($qry_info_question_fr[0]['choix_1']) ?></textarea>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">Collaborateur (trice) pour un manager Homme</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left" class="TX"><textarea name="question2" class="form_ediht_Tarifs" style="height: 50px; width: 90%"><?php echo htmlentities($qry_info_question_fr[0]['choix_2']) ?></textarea></td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">Collaborateur (trice) pour un manager Femme</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left" class="TX"><textarea name="question3" class="form_ediht_Tarifs" style="height: 50px; width: 90%"><?php echo htmlentities($qry_info_question_fr[0]['choix_3']) ?></textarea></td>
				  <td></td>
			    </tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
			  <br>
			  <p style="text-align:center">
					<input type="hidden" name="posted" value="1">
					<input type="button" onClick="tester_question();" name="Submit" value="Valider" class="BN">
				  </p>
			</form>
			<?php } ?>
			</body>
			</html>
	<?php
	}
}else{
	include('no_acces.php');
}
?>
<script type="text/javascript" src="../js/jquery-latest.min.js"></script>
<script>	
	$('#theme').change(function() {
		loadTheme($(this).val(), 'stheme');
	});
	$('#stheme').change(function() {
		var id = $("#stheme option:selected").attr("id");
		loadTheme(id, 'item');
	});	
	$('#item').change(function() {
		var id = $("#item option:selected").attr("id");
	  	$("#item_selected").val(id);
	});	
		
	function loadTheme(themeid, themetype) {
		$.ajax({
		  	url: "ajax-BPM.php",
  			cache: false,
  			data: {themeid : themeid, themetype : themetype},
  			dataType: "html",
  			success: function(html){
	  			if (themetype == 'stheme'){
  					$("#stheme").empty();
					$("#stheme").append(html);
					var stheme = $("#stheme option:selected").attr("id");
					loadTheme(stheme, 'item');
	  			} else {
  					$("#item").empty();
					$("#item").append(html);  					
	  			}
	  			$("#item_selected").val($("#item option:selected").attr("id"));
			}
		});
	}
</script>