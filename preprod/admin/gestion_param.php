<?php
if ($_SESSION['droit']!='9'){
	header('location:index.php');
}

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	/* Sélection des tables */
	$sql_code_table = "SELECT DISTINCT CODE_TABLE FROM CODE ORDER BY CODE_TABLE";
	$qry_code_table = $db->query($sql_code_table);
	
	
if (is_uploaded_file($_FILES['fichier']['tmp_name'])){
//if(isset($_POST["Envoyer"])) {
	$error = '';
	$ftmp  = $_FILES['fichier']['tmp_name'];
	$fname = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['fichier']['name']));
	$fname = stripslashes($fname);
	$fname = strtr($fname,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	$fname = strtolower($fname);

	//extensions autorisées	
	$extension_autorises	= array('.pdf');
	$extension				= strrchr($fname,'.');
	
	//lien des photos
	$lien  		= '../produits/'.$fname;

	//verification de l'extension
	if (!in_array($extension,$extension_autorises)){
		$error_ext .= '<?php echo $t_fichier_not_image  ?>';
	}
	
	if(!$error_ext){
		// L'image est déplacée dans le dossier normal
		if (!move_uploaded_file($ftmp,$lien)){
			$error_copie .= '<?php echo $t_copier_fichier_probleme  ?>';
		}
	} 
	unset($lien);						
	//*************************************************************

	header('location: gestion_param.php?idnc='.($_GET['idnc']+1));
	
}

if (is_uploaded_file($_FILES['fichier_j']['tmp_name'])){
//if(isset($_POST["Envoyer"])) {
	$error = '';
	$ftmp  = $_FILES['fichier_j']['tmp_name'];
	$fname = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['fichier_j']['name']));
	$fname = stripslashes($fname);
	$fname = strtr($fname,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	$fname = strtolower($fname);

	//extensions autorisées	
	$extension_autorises	= array('.pdf');
	$extension				= strrchr($fname,'.');
	
	//lien des photos
	$lien  		= '../produits/journal/'.$fname;

	//verification de l'extension
	if (!in_array($extension,$extension_autorises)){
		$error_ext .= '<?php echo $t_fichier_not_image  ?>';
	}
	
	if(!$error_ext){
		// L'image est déplacée dans le dossier normal
		if (!move_uploaded_file($ftmp,$lien)){
			$error_copie .= '<?php echo $t_copier_fichier_probleme  ?>';
		}
	} 
	unset($lien);						
	//*************************************************************

	header('location: gestion_param.php?idnc='.($_GET['idnc']+1));
	
}
if (is_uploaded_file($_FILES['fichier_c']['tmp_name'])){
//if(isset($_POST["Envoyer"])) {
	$error = 'OK';
	$ftmp  = $_FILES['fichier_c']['tmp_name'];
	$fname = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['fichier_c']['name']));
	$fname = stripslashes($fname);
	$fname = strtr($fname,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	$fname = strtolower($fname);

	//extensions autorisées	
	$extension_autorises	= array('.csv');
	$extension				= strrchr($fname,'.');
	
	//lien des photos
	$lien  		= '../produits/candidat/'.$fname;

	//verification de l'extension
	if (!in_array($extension,$extension_autorises)){
		$error = '<?php echo $t_fichier_not_image  ?>';
	}
	
	if(!$error_ext){
		// L'image est déplacée dans le dossier normal
		if (!move_uploaded_file($ftmp,$lien)){
			$error = '<?php echo $t_copier_fichier_probleme  ?>';
		}
	} 
	unset($lien);						
	//*************************************************************

if (!$fp = fopen("../produits/candidat/".$fname,"r")) {
	echo "Echec de l'ouverture du fichier";
	exit;
}
else 
{
	$Ligne = fgets($fp);
	$num_ligne=1;
	while(!feof($fp)) 
	{
		// On récupère une ligne
		$Ligne = fgets($fp);
		$info = explode(';', $Ligne);
		
		$part_tmp = trim($info[0]);
		$rs_tmp = trim($info[1]);
		$soc_tmp = trim($info[2]);
		$nom_tmp = trim($info[3]);
		$pre_tmp = trim($info[4]);
		$nomcand_tmp = trim($info[5]);
		$precand_tmp = trim($info[6]);
		$sex_tmp = trim($info[7]);
					   
		$mail_tmp = trim($info[10]);
		$Erreur = '';
		if ($rs_tmp != '')
		{
			$sql_imp = "SELECT PART_ID,CERT_ID FROM CERTIFIE, PARTENAIRE WHERE CERT_PART_ID = PART_ID AND CERT_DATE_SUPPRESSION IS NULL AND CERT_NOM='".$nom_tmp."' AND CERT_PRENOM='".$pre_tmp."' AND PART_NOM='".$part_tmp."' AND PART_RS='".$rs_tmp."'";			
		}
		else
		{
			$sql_imp = "SELECT PART_ID,CERT_ID FROM CERTIFIE, PARTENAIRE WHERE CERT_PART_ID = PART_ID AND CERT_DATE_SUPPRESSION IS NULL AND CERT_NOM='".$nom_tmp."' AND CERT_PRENOM='".$pre_tmp."' AND PART_NOM='".$part_tmp."'";			
		}
		$qry_imp = $db->query($sql_imp);
		//echo $sql_imp.'<br/>';
		if(is_array($qry_imp))
		{
			$part_id = $qry_imp[0]['part_id'];
			$cert_id = $qry_imp[0]['cert_id'];
			$sql_imp = "SELECT CLIENT_A_CERT.CLI_ID FROM CLIENT,CLIENT_A_CERT WHERE CLIENT.CLI_ID=CLIENT_A_CERT.CLI_ID AND CLIENT_A_CERT.CERT_ID=".$cert_id." AND CLI_NOM='".$soc_tmp."'";
			$qry_imp = $db->query($sql_imp);
			if(is_array($qry_imp))
			{
				$cli_id = $qry_imp[0]['cli_id'];
				$sql_imp = "insert into candidat (CAND_ID,CAND_CLI_ID,CAND_LANG_ID,CAND_PAYS_CODE_ID,CAND_ACTIF,CAND_USER_CREATION_ID,CAND_DATE_CREATION, CAND_CERT_ID,CATEGORIE_SP_CODE_ID,ACTIVITE_CODE_ID,CAND_NOM,CAND_PRENOM,CAND_EMAIL,CAND_SEXE) values (seq_id.nextval,".$cli_id.",1,5,1,1,sysdate,".$cert_id.",-1,-1,'".$nomcand_tmp."','".$precand_tmp."','".$mail_tmp."','".$sex_tmp."')";
				$qry_imp = $db->query($sql_imp);
			}
			else
			{
				$Erreur = $Erreur . $num_ligne . ': Pas de société trouvé pour '.$nomcand_tmp.';';
			}
		}
		else
		{
			$Erreur = $Erreur . $num_ligne . ': Pas de certifié trouvé pour '.$nomcand_tmp.';';
		}
		$num_ligne = $num_ligne + 1;
	}
	fclose($fp); // On ferme le fichier
	$sql_imp = "update candidat set cand_pwd=concat(concat('N',cand_cli_id),'W') where cand_pwd is null";
	$qry_imp = $db->query($sql_imp);

}
$subject = "Depot fichier candidat ".$ftmp.' : '.$error;
$from_mail = "nepasrepondre@lesensdelhumain.com";				
$boundary = md5(uniqid(time()));
$entete = "From: $from_mail \n";
$entete .= "X-Priority: 1 \n";
$entete .= "MIME-Version: 1.0 \n";
$entete .= "Content-Type: multipart/mixed; boundary=\"$boundary\" \n";
$entete .= " \n";
$message  = "--$boundary \n";
$message .= "Content-Type: text/html; charset=\"iso-8859-1\" \n";
$message .= "Content-Transfer-Encoding:8bit \n";
$message .= "\n";
$message .= "Import de ".$num_ligne." candidats (".$Erreur.") Cordialement,";
$message .= "\n";
mail('sgueguen@vakom.fr', $subject, $message, $entete);

	header('location: gestion_param.php?idnc='.($_GET['idnc']+1));
	
}
// Si on souhaite modifier un libellé de la table code
if(trim($_POST['txt_edit_lib'])!='' && $_POST['hid_code_id']>0){
	$sql_up_lib = "UPDATE CODE SET CODE_LIBELLE='".txt_db($_POST['txt_edit_lib'])."' WHERE CODE_ID=".intval($_POST['hid_code_id']);
	$qry_up_lib = $db->query($sql_up_lib);
}

if(trim($_POST['select_table'])!=''){
	/* On sélectionne l'ensemble des libellés de la table sélectionnée */
	$sql_code_lib	= "SELECT * FROM CODE WHERE LOWER(CODE_TABLE)='".txt_db(strtolower($_POST['select_table']))."' AND CODE_ACTIF=1 ORDER BY CODE_ORDRE";
	$qry_code_lib	= $db->query($sql_code_lib);
}

	$sql_select_lib = "SELECT * FROM CODE WHERE CODE_ID=".intval($_POST['select_lib'])." AND LOWER(CODE_TABLE)='".txt_db(strtolower($_POST['select_table']))."'";
	$qry_select_lib = $db->query($sql_select_lib);
?>
<!DOCTYPE html>
<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo.css" type="text/css">
<link rel="stylesheet" href="../css/general.css" type="text/css">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<script language="JavaScript">
<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
<?php
	include("menu_top_new.php");
?>
	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>	
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td align="right">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" >
              <tr> 
                <td width="20">&nbsp;</td>
                <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;T&Eacute;L&Eacute;CHARGEMENTS</td>
                </tr>
              </table>
            <form action="#" method="post" enctype="multipart/form-data">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
                <tr> 
                  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
                  </tr>
                <tr>
                  <td width="14"></td>
                  <td align="left" class="TX"> 
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                      <tr> 
                        <td class="TX" align="center">Fichier&nbsp;PDF (Informations produits)
                          <input type="file" name="fichier">&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  <td width="14"></td>
                  </tr>
                <tr> 
                  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
                  </tr>
                </table>
              <center>
                <input type="submit" name="Envoyer" value="Envoyer" class="BN">
                </center>
              </form>
            <form action="#" method="post" enctype="multipart/form-data">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
                <tr> 
                  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
                  </tr>
                <tr>
                  <td width="14"></td>
                  <td align="left" class="TX"> 
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                      <tr> 
                        <td class="TX" align="center">Fichier&nbsp;PDF (journal)
                          <input type="file" name="fichier_j">&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  <td width="14"></td>
                  </tr>
                <tr> 
                  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
                  </tr>
                </table>
              <center>
                <input type="submit" name="Envoyer" value="Envoyer" class="BN">
                </center>
              </form>
            <form action="#" method="post" enctype="multipart/form-data">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
                <tr> 
                  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
                  </tr>
                <tr>
                  <td width="14"></td>
                  <td align="left" class="TX"> 
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                      <tr> 
                        <td class="TX" align="center">Fichier&nbsp;Candidat (CSV)
                          <input type="file" name="fichier_c">&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  <td width="14"></td>
                  </tr>
                <tr> 
                  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
                  </tr>
                </table>
              <center>
                <input type="submit" name="Envoyer" value="Envoyer" class="BN">
                </center>
              </form>
            <form action="#" method="post" name="form_tables">
              <br>
              <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td width="20">&nbsp;</td>
                  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;LIBELL&Eacute;S</td>
                  </tr>
                </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
                <tr> 
                  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
                  </tr>
                <tr>
                  <td width="14"></td>
                  <td align="left" class="TX"> 
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                      <tr> 
                        <td class="TX" align="center">Selection de la table
                          <?php
						if(is_array($qry_code_table)){
							echo '<select name="select_table" class="form_ediht_Tarifs">';
							
							foreach($qry_code_table as $code_table){
								unset($selected_table);
								if($code_table['code_table'] == stripslashes($_POST['select_table'])) $selected_table = ' selected="selected"';
								echo '<option value="'.htmlentities($code_table['code_table']).'"'.$selected_table.'>'.htmlentities($code_table['code_table']).'</option>';
							}
							echo '</select>';
						}
						?>&nbsp;<input type="submit" name="ok1" value="Ok" class="bn_ajouter">&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                      </table>
                    <?php
				  if(is_array($qry_code_lib)){
				  ?>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                      <tr> 
                        <td class="TX" align="center">Selection du libellé à modifier
                          <?php
						if(is_array($qry_code_lib)){
							echo '<select name="select_lib" class="form_ediht_Tarifs">';
							
							$tab_compta = array(
								52=>'Code société',
								53=>'Code établissement',
								54=>'Code journal',
								55=>'Code facture',
								56=>'Compte général HT',
								57=>'Compte général TVA',
								58=>'Compte général TTC'
							);
							
							foreach($qry_code_lib as $code_lib){
								unset($selected_lib);
								if($code_lib['code_id'] == $_POST['select_lib']) $selected_lib= ' selected="selected"';
								if(array_key_exists($code_lib['code_id'], $tab_compta)){
									echo '<option value="'.htmlentities($code_lib['code_id']).'"'.$selected_lib.'>'.htmlentities($tab_compta[$code_lib['code_id']].' : '.$code_lib['code_libelle']).'</option>';
								}else{
									echo '<option value="'.htmlentities($code_lib['code_id']).'"'.$selected_lib.'>'.htmlentities($code_lib['code_libelle']).'</option>';
								}
							}
							echo '</select>';
						}
						?>&nbsp;<input type="submit" name="ok2" value="Ok" class="bn_ajouter">&nbsp;&nbsp;&nbsp;
                          <?php
						if(is_array($qry_select_lib)){
							echo '<input type="hidden" name="hid_code_id" value="'.htmlentities($qry_select_lib[0]['code_id']).'"><input class="form_ediht_Tarifs" name="txt_edit_lib" type="text" value="'.htmlentities($qry_select_lib[0]['code_libelle']).'" size="25" >&nbsp;<input type="submit" value="modifier" name="submit_lib" class="bn_ajouter">';
						}
						?>
                          </td>
                        </tr>
                      </table>
                    <?php
				  }
				  ?>
                    </td>
                  <td width="14"></td>
                  </tr>
                <tr> 
                  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
                  </tr>
                </table>
              </form>
            <br>
            <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="20">&nbsp;</td>
                <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;EXTRACTIONS</td>
                </tr>
              </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
                </tr>
              <tr>
                <td width="14"></td>
                <td align="left" class="TX"> 
                  <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr> 
                      <td class="TX" align="center">Extraction&nbsp;Comptable&nbsp;<input type="button" onClick="MM_openBrWindow('export_comptable.php','export_comptable','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=1,height=1')" name="ok1" value="Ok" class="bn_ajouter">&nbsp;&nbsp;&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                <td width="14"></td>
                </tr>
              <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
  </tr>
 </table>
</p></div>	</article></div>	</div>	</div>	</div>	  		
 </body>
</html>