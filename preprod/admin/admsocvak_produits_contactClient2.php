<?php
session_start();
// Si l'utilisateur est un super admin ou un admin vakom
if ($_SESSION['droit']>2){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
		
	if ($_GET['certid']>0){
		
		/* On sélectionne les infos du certifié */
		$sql_cert_info = "SELECT CERT_ID, CERT_NOM, CERT_PRENOM, PART_NOM, PART_ID FROM CERTIFIE, PARTENAIRE WHERE CERT_PART_ID=PART_ID AND CERT_ID= '".txt_db(intval($_GET['certid']))."' AND CERT_PART_ID='".$_SESSION['part_id']."'";
		$qry_cert_info = $db->query($sql_cert_info);
		
		/* Si le formulaire est posté */
		if ($_POST['certid']!=''){
			if ($_SESSION['vak_id']>0){
				/* VAKOM */
				$resp = 1;
			}else{
				/* Client */
				$resp = 0;
			}
			$sql_insert_jet 	= "SELECT SEQ_ID.NEXTVAL seq_jeton FROM DUAL";
			$qry_insert_jet		= $db->query($sql_insert_jet);
			$nb_seq_jeton 		=  $qry_insert_jet[0]['seq_jeton'];
			
			$sql_insert_jet 	= "INSERT INTO CERT_A_JETON VALUES (".$nb_seq_jeton.",'".txt_db($_POST['certid'])."', 
			SYSDATE, 
			'".txt_db($_POST['liste_prdt'])."', 
			'".txt_db($_POST['select_type_jeton'])."', 
			'".txt_db($_POST['nbre'])."', 
			'".txt_db($_POST['comm1'])."', 
			'".txt_db($_POST['comm2'])."',
			'".$resp."',
			'','')";			
			$qry_insert_jet		= $db->query($sql_insert_jet);
			$sql_jeton = "select jet_id,JETON_CORRESP_ID,affecte-utilise reste from v_jeton_synthese where jet_part_id='".txt_db($qry_cert_info[0]['part_id'])."'
			and jet_prod_id='".txt_db($_POST['liste_prdt'])."'
			and JET_TYPE_JETON_CODE_ID='".txt_db($_POST['select_type_jeton'])."' 
			and TO_char(JET_DEB_VALIDITE,'YYYYMMDD')<=to_char(SYSDATE,'YYYYMMDD') and TO_char(JET_FIN_VALIDITE,'YYYYMMDD')>=to_char(SYSDATE,'YYYYMMDD') 
			and cert_jet_id=-1 and affecte-utilise>0 order by JET_DEB_VALIDITE,JET_FIN_VALIDITE";
			$qry_jeton = $db->query($sql_jeton);
			$jeton_tmp = $_POST['nbre'];
			foreach($qry_jeton as $jeton){
				if ($jeton_tmp > 0)
				{
					if ($jeton_tmp <= $jeton['reste'])
					{
						$sql_insert_jet 	= "INSERT INTO JETON_CORRESP_CERT (JETON_CORRESP_ID,JET_ID,CERT_ID,CERT_JET_ID,AFFECTE,UTILISE) VALUES (SEQ_ID.NEXTVAL, 
						'".$jeton['jet_id']."', 
						'".txt_db($_POST['certid'])."', 
						'".$nb_seq_jeton."',
						'".$jeton_tmp."', 
						'0')";
						$qry_insert_jet		= $db->query($sql_insert_jet);
						$sql_insert_jet 	= "UPDATE JETON_CORRESP_CERT SET AFFECTE = AFFECTE - ".$jeton_tmp." WHERE JETON_CORRESP_ID=".$jeton['jeton_corresp_id'];
						$qry_insert_jet		= $db->query($sql_insert_jet);					
						$jeton_tmp = 0;
					}
					else
					{
						$sql_insert_jet 	= "INSERT INTO JETON_CORRESP_CERT (JETON_CORRESP_ID,JET_ID,CERT_ID,CERT_JET_ID,AFFECTE,UTILISE) VALUES (SEQ_ID.NEXTVAL, 
						'".$jeton['jet_id']."', 
						'".txt_db($_POST['certid'])."', 
						'".$nb_seq_jeton."',
						'".$jeton['reste']."', 
						'0')";
						$qry_insert_jet		= $db->query($sql_insert_jet);
						$sql_insert_jet 	= "UPDATE JETON_CORRESP_CERT SET AFFECTE = 0 WHERE JETON_CORRESP_ID=".$jeton['jeton_corresp_id'];
						$qry_insert_jet		= $db->query($sql_insert_jet);					
						$jeton_tmp = $jeton_tmp - $jeton['reste'];
					}				
				}
			}
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			
			<?php
		}
		
		
		// Chargement de la liste des produits
		$sql_prod_list 			= "SELECT distinct produit.prod_id,prod_nom
		FROM PRODUIT,PRODUIT_A_CERTIF,CERT_A_CERTIF, PRODUIT_A_PRIX 
		WHERE PRODUIT.PROD_ID = PRODUIT_A_CERTIF.PROD_ID AND PRODUIT_A_CERTIF.CODE_ID = CERT_A_CERTIF.CERTIF_CODE_ID
		AND CERT_A_CERTIF.CERTIF_CERT_ID='".txt_db($_GET['certid'])."' 
		AND 
		(
		(CERT_A_CERTIF.CERTIF_CERTIFICATION Is Not Null And CERT_A_CERTIF.CERTIF_CERTIFICATION<=SYSDATE) OR (CERTIF_FORMATION=1)
		)
		AND (CERT_A_CERTIF.CERTIF_SUSPENDU=0 OR CERT_A_CERTIF.CERTIF_SUSPENDU IS NULL) 
		AND PRODUIT.PROD_ID IN (SELECT DISTINCT PROD_ID FROM RECUP_QUEST) 
		AND PRODUIT_A_PRIX.PROD_ID=PRODUIT.PROD_ID 
		AND PRODUIT_A_PRIX.NATURE_CODE_ID<>46 
		ORDER BY PROD_NOM";

		$qry_prod_list 			= $db->query($sql_prod_list);
		
		// Chargement de la liste des types de jeton
		$sql_type_jet_list 		= "SELECT * FROM CODE WHERE CODE_TABLE='TYPE_JETON'";
		$qry_type_jet_list 		= $db->query($sql_type_jet_list);
		
		// Chargement de la liste des natures de pack
		$sql_nature_pack_list 	= "SELECT * FROM CODE WHERE CODE_TABLE='NATURE_PACK'";
		$qry_nature_pack_list 	= $db->query($sql_nature_pack_list);
		
		if (is_array($qry_cert_info)){
		// Si on a choisi un partenaire
		
			?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--
			function verif(){
				error = '';
				if(document.form.nbre){
					if (document.form.nbre.value=='' || parseInt(document.form.nbre.value*10)<10 || parseInt(document.form.nbre.value) > parseInt(document.form.hid_nbre.value)){
						error += "<?php echo $t_nbre_jet_inf ?>\n";
					}
				}else{
					window.close();
				}
				if (error!=''){
					alert(error);
				}else{
					document.form.submit();
				}
			}
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function liste_type(partid, prodid){
				/* Si le produit est défini, on ressort les types de jetons disponibles */
				if (prodid>0 && partid>0){
					/* On lance la fonction ajax  qui va chercher les types de jetons disponibles pour le produit en question */					
					var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
					var filename = "ajax_type_jetons.php"; // La page qui réceptionne les données
					var data     = null; 
					
						document.getElementById("type_jetons").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" />';
						var xhr_object = null; 
							 
							if(window.XMLHttpRequest) // Firefox 
							   xhr_object = new XMLHttpRequest(); 
							else if(window.ActiveXObject) // Internet Explorer 
							   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
							else { // XMLHttpRequest non supporté par le navigateur 
							   alert("<?php echo $t_browser_support_error_1 ?>");
							   return; 
							} 
							 
							
							 
							if(prodid != ""){
								data = "prodid="+prodid+"&partid="+partid;
							}
							if(method == "GET" && data != null) {
							   filename += "?"+data;
							   data      = null;
							}
							 
							xhr_object.open(method, filename, true);

							xhr_object.onreadystatechange = function() {
							   if(xhr_object.readyState == 4) {
								  var tmp = xhr_object.responseText.split(":"); 
								  if(typeof(tmp[0]) != "undefined") { 
									 document.getElementById("type_jetons").innerHTML = '';
									 if (tmp[0]!=''){
										document.getElementById("type_jetons").innerHTML = tmp[0];
									 }
								  }
							   } 
							} 

							xhr_object.send(data); //On envoie les données
					
				}else{
					 document.getElementById("type_jetons").innerHTML = "<?php echo $t_choisir_un_pdt ?>";
				}
				document.getElementById("nbre_jetons").innerHTML = '';
			}
			
			function jeton_dispo(partid1, prodid1, type_jet1){
				/* On lance la fonction ajax  qui va chercher le nombre de résultat que la personne est autorisée à attribuer */
				if (prodid1>0 && partid1>0 && type_jet1>0){
					
					/* On lance la fonction ajax  qui va chercher les types de jetons disponibles pour le produit en question */					
					var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
					var filename = "ajax_jetons_dispo.php"; // La page qui réceptionne les données
					var data     = null; 
					
					document.getElementById("nbre_jetons").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" />';
					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("<?php echo $t_browser_support_error_1 ?>");
						   return; 
						} 
						 
						
						 
						
						data = "prodid="+prodid1+"&partid="+partid1+"&type_jet="+type_jet1;
						
						if(method == "GET" && data != null) {
						   filename += "?"+data;
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split(":"); 
							  if(typeof(tmp[0]) != "undefined") { 
								 document.getElementById("nbre_jetons").innerHTML = '';
								 if (tmp[0]!='' && tmp[1]>0){
									document.getElementById("nbre_jetons").innerHTML = tmp[0];
								 }else{
									document.getElementById("nbre_jetons").innerHTML = "<?php echo $t_desole_attr_jet ?>";
								 }
							  }
						   } 
						} 

					xhr_object.send(data); //On envoie les données
				}else{
					document.getElementById("nbre_jetons").innerHTML = "<?php echo $t_choisir_un_type ?>";
				}
			}
			
			//-->
			</script>
			</head>

			<body bgcolor="#FFFFFF" text="#000000">
			<form method="post" action="#" name="form">
			<table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Certifies2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo strtoupper($qry_cert_info[0]['part_nom']) .' > '.strtoupper($qry_cert_info[0]['cert_nom']) . ' ' . ucfirst($qry_cert_info[0]['cert_prenom']) ?></td>
				</tr>
			</table>
			  <table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td class="TX_Certifies"><?php echo $t_jet_aff_de_jeton ?></td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="center" valign="top" class="TX"> 
					<table border="0" cellspacing="0" cellpadding="0">
					  <tr> 
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_produit ?>* :</td>
						<td class="TX"> 
						  <select name="liste_prdt" class="form_ediht" id="liste_prdt" onchange="liste_type(<?php echo $qry_cert_info[0]['part_id'] ?>, document.getElementById('liste_prdt').options[document.getElementById('liste_prdt').selectedIndex].value);">
						  <option value="0">-----</option>
						  <?php
						  if (is_array($qry_prod_list)){
							foreach($qry_prod_list as $produit){
								echo '<option value="'.$produit['prod_id'].'">'.$produit['prod_nom'].'</option>';
							}
						  }
						  ?>
						  </select>
						</td>
					  </tr>
					  <!--
					  <tr> 
						<td class="TX" height="40">Type de jetons* : </td>
						<td  class="TX"> 
						  <select name="type" class="form_ediht">-->
						  <?php
						  /*if (is_array($qry_type_jet_list)){
							foreach($qry_type_jet_list as $type_jet){
								echo '<option value="'.$type_jet['code_id'].'">'.$type_jet['code_libelle'].'</option>';
							}
						  }*/
						  ?>
						  <!--</select>
						</td>
					  </tr>
					  -->
					  <tr> 
						<td class="TX" height="40"><?php echo $t_jet_type ?>* : </td>
						<td class="TX"> 
							<div id="type_jetons"><?php echo $t_choisir_un_pdt ?></div>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_jet_nbre ?>* : </td>
						<td  class="TX"> 
						  <div id="nbre_jetons"><?php echo $t_choisir_un_type ?></div>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_comment ?> :</td>
						<td  class="TX"> 
						  <input type="text" name="comm1" size="50" class="form_ediht" maxlength="50">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40">&nbsp;</td>
						<td  class="TX"> 
						  <input type="text" name="comm2" size="50" class="form_ediht" maxlength="50">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_date_attrib ?> : </td>
						<td class="TX"><?php echo date('d/m/Y') ?></td>
					  </tr>
					</table>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
			  <br>
			  <table border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td align="center"> 
					<input type="hidden" name="certid" value="<?php echo $qry_cert_info[0]['cert_id'] ?>" class="BN">
					<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_certifie" onClick="verif();">
				  </td>
				</tr>
			  </table>
			</form>
			</body>
			</html>
		<?php
		}else{
			include('../config/lib/lang.php');
			echo $t_acn_1;
		}
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{
	include('no_acces.php');
}
?>
