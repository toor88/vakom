<?php
session_start();
// Si l'utilisateur est un super admin ou un admin vakom
if ($_SESSION['droit']>2){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
		
	if ($_GET['certid']>0){
		$tab_resp = array(
		0=>'Client',
		1=>'Vakom');
		
		/* On sélectionne les infos du certifié */
		$sql_cert_info = "SELECT CERT_ID, CERT_NOM, CERT_PRENOM, PART_NOM, PART_ID FROM CERTIFIE, PARTENAIRE WHERE CERT_PART_ID=PART_ID AND CERT_ID= '".txt_db(intval($_GET['certid']))."' AND CERT_PART_ID='".txt_db(intval($_SESSION['part_id']))."'";
		$qry_cert_info = $db->query($sql_cert_info);

		if ($_GET['prodid']!='' && $_GET['typid']!=''){
			if ($_GET['action']=='delete_jeton' && $_GET['jetid'] !=''){
			
				/* On vérifie que le pack n'a pas été utilisé */
				$sql_verif = "SELECT jet_id,affecte,utilise from jeton_corresp_cert WHERE CERT_JET_ID='".txt_db(intval($_GET['jetid']))."'";
				$qry_verif = $db->query($sql_verif);
			
				/* Si le pack n'est pas utilisé */
				if($qry_verif[0]['utilise']==0){
				
					/* On supprime la ligne de jeton désirée */
					$sql_del_jeton	= "DELETE CERT_A_JETON WHERE CERT_JET_ID='".txt_db(intval($_GET['jetid']))."'";
					$qry_del_jeton = $db->query($sql_del_jeton);
					$sql_del_jeton	= "DELETE JETON_CORRESP_CERT WHERE CERT_JET_ID='".txt_db(intval($_GET['jetid']))."'";
					$qry_del_jeton = $db->query($sql_del_jeton);
					
					/* On met à jour les jetons */
					$sql_up_jeton = "UPDATE jeton_corresp_cert SET affecte=affecte+".txt_db(intval($qry_verif[0]['affecte']))." WHERE JET_ID='".txt_db(intval($qry_verif[0]['jet_id']))."' AND CERT_JET_ID='-1'";
					//echo $sql_up_jeton;
					$qry_up_jeton = $db->query($sql_up_jeton);
				}
				
				
			}
			
			$sql_jet = "SELECT CERT_A_JETON.*, 
				CODE.CODE_LIBELLE TYPE_JETON, 
				PRODUIT.*, CERTIFIE.CERT_NOM  
				FROM CERT_A_JETON,CERTIFIE,PRODUIT,CODE CODE  
				WHERE CERT_A_JETON.CERT_JET_DATE_SUPPRESSION IS NULL 
				AND CERT_A_JETON.CERT_JET_TYPE_JETON_CODE_ID=CODE.CODE_ID 
				AND CODE.CODE_TABLE='TYPE_JETON' 
				AND CERT_A_JETON.CERT_JET_PROD_ID=PRODUIT.PROD_ID 
				AND CERT_JET_PROD_ID='".txt_db($_GET['prodid'])."'
				AND CODE.CODE_ID='".txt_db($_GET['typid'])."'
				AND CERT_JET_CERT_ID='".txt_db($_GET['certid'])."'
				AND CERT_JET_CERT_ID=CERTIFIE.CERT_ID";
			//echo $sql_jet;
			$qry_jet = $db->query($sql_jet);
		}
		
		if (is_array($qry_cert_info)){
		// Si on a choisi un certifié
		$sql_jeton = "select distinct prod_nom,JET_PROD_ID prod_id,JET_TYPE_JETON_CODE_ID code_id,code_libelle,code_ordre from jeton,produit,code
			where jeton.JET_PROD_ID=produit.prod_id
			and jeton.JET_TYPE_JETON_CODE_ID=code.code_id and code.code_table='TYPE_JETON'
			and jet_part_id='".txt_db($_SESSION['part_id'])."' order by prod_nom,code_ordre";										
		//					echo $sql_jeton;				
		$qry_jeton = $db->query($sql_jeton);
				
		?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<script language="JavaScript">
		<!--
		function delete_jeton(jetid){
			if (confirm("<?php echo $t_sur_dannuler_aff ?>")){
				document.location.href='admsocvak_produits_contactClient.php?certid=<?php echo $_GET['certid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&action=delete_jeton&jetid='+jetid;
			}
		}
		
		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		
		function MM_openBrWindow(theURL,winName,features) { //v2.0
		  window.open(theURL,winName,features);
		}
		
		function show_actif(){
			if(document.form.actif.checked==true){
				document.location.href='admsocvak_produits_contactClient.php?partid=<?php echo $_GET['partid'] ?>&certid=<?php echo $_GET['certid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&actif=1';
			}
			else{
				document.location.href='admsocvak_produits_contactClient.php?partid=<?php echo $_GET['partid'] ?>&certid=<?php echo $_GET['certid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&actif=0';
			}
		}
		//-->
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="" name="form">
		  <table border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td align="left" class="TX_GD"> 
			  <table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Certifies2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo strtoupper($qry_cert_info[0]['part_nom']) .' > '.strtoupper($qry_cert_info[0]['cert_nom']) . ' ' . ucfirst($qry_cert_info[0]['cert_prenom']) ?></td>
				</tr>
			  </table>
				<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="800">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
					<tr> 
					  <td width="14"></td>
					  <td class="TX_Certifies"><?php echo $t_jet_aff_cert ?></td>
					  <td width="14"></td>
					</tr>
					<tr> 
					  <td width="14"></td>
					  <td bgcolor="#666666" height="1"></td>
					  <td width="14"></td>
					</tr>
					<tr> 
					  <td width="14"></td>
					  <td align="center" class="TX">
					  &nbsp;</td>
					  <td width="14"></td>
					</tr>
				  <tr> 
					<td width="14"></td>
					<td align="left" valign="top" class="TX">
					  <table cellpadding="0" cellspacing="0" width="730">
						  <tr><td  class="TX" style="text-align: left;">
							<input type="button" name="new" value="<?php echo $t_btn_affect_new_jet ?>" class="bn_ajouter" onClick="MM_openBrWindow('admsocvak_produits_contactClient2.php?partid=<?php echo $_GET['partid'] ?>&certid=<?php echo $qry_cert_info[0]['cert_id'] ?>','','toolbar=yes,scrollbars=yes,resizable=yes')">
						  </td><td class="TX" style="text-align: right;">
						  <input type="checkbox" name="actif" onclick="show_actif();" value="1" <?php if($_GET['actif']==1 || !isset($_GET['actif'])){ echo ' checked="checked"';} ?>>
						  <?php echo $t_jet_actifs_only ?>
					  </td></tr>
					  </table>					  
					  </td>					  
					<td width="14"></td>
				  </tr>
				  <tr> 
					<td width="14"></td>
					<td align="left" valign="top" class="TX"> 
					  <table width="730" border="0" cellspacing="0" cellpadding="2" class="TX">
						<tr align="center"> 
						  <td colspan="5" class="TX_GD"></td>
						</tr>
						<tr align="center"> 
						  <td height="1" colspan="5" bgcolor="#000000" ></td>
						</tr>
						<tr> 
						  <td class="TX_bold"><?php echo $t_produit ?></td>
						  <td class="TX_bold" align="center"><?php echo $t_jet_type ?></td>
						  <td class="TX_bold" align="center"><?php echo $t_jet_dispo_aff2 ?></td>
						  <td class="TX_bold" align="center"><?php echo $t_jet_dispo_autres ?></td>
						</tr>
						<tr> 
						  <td height="1" bgcolor="#000000"></td>
						  <td height="1" bgcolor="#000000"></td>
						  <td height="1" bgcolor="#000000"></td>
						  <td height="1" bgcolor="#000000"></td>
						</tr>
						<?php
						  if (is_array($qry_jeton)){
							foreach($qry_jeton as $jeton){
								if ($_GET['actif'] == 1 || !isset($_GET['actif'])){
								$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".txt_db($_SESSION['part_id'])."'
								and jet_prod_id='".$jeton['prod_id']."'
								and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
								and TO_char(JET_DEB_VALIDITE,'YYYYMMDD')<=to_char(SYSDATE,'YYYYMMDD') and TO_char(JET_FIN_VALIDITE,'YYYYMMDD')>=to_char(SYSDATE,'YYYYMMDD') and cert_id=-1";
								$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
								$qry_CJA_dispo_aff = $qry_CJA[0]['affecte'];				
								$qry_CJA_dispo_uti = $qry_CJA[0]['utilise'];				
								$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".txt_db($_SESSION['part_id'])."'
								and jet_prod_id='".$jeton['prod_id']."'
								and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
								and TO_char(JET_DEB_VALIDITE,'YYYYMMDD')<=to_char(SYSDATE,'YYYYMMDD') and TO_char(JET_FIN_VALIDITE,'YYYYMMDD')>=to_char(SYSDATE,'YYYYMMDD') and cert_id='".txt_db(intval($_GET['certid']))."'";
								$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
								$qry_CJA_aff_aff = $qry_CJA[0]['affecte'];				
								$qry_CJA_aff_uti = $qry_CJA[0]['utilise'];				
								}
								else
								{
								$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".txt_db($_SESSION['part_id'])."'
								and jet_prod_id='".$jeton['prod_id']."'
								and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
								and cert_id=-1";
								$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
								$qry_CJA_dispo_aff = $qry_CJA[0]['affecte'];				
								$qry_CJA_dispo_uti = $qry_CJA[0]['utilise'];				
								$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".txt_db($_SESSION['part_id'])."'
								and jet_prod_id='".$jeton['prod_id']."'
								and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
								and cert_id='".txt_db(intval($_GET['certid']))."'";
								$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
								$qry_CJA_aff_aff = $qry_CJA[0]['affecte'];				
								$qry_CJA_aff_uti = $qry_CJA[0]['utilise'];				
								}					
							 ?>
							  <tr> 
								<td><a href="admsocvak_produits_contactClient.php?certid=<?php echo $_GET['certid'] ?>&prodid=<?php echo $jeton['prod_id'] ?>&typid=<?php echo $jeton['code_id'] ?>"><?php echo ucfirst($jeton['prod_nom']) ?></a></td>
								<td align="center"><?php echo ucfirst($jeton['code_libelle']) ?></td>
								<td align="center"><?php echo $qry_CJA_aff_aff - $qry_CJA_aff_uti ?>/<?php echo $qry_CJA_aff_aff?></td>					
								<td align="center"><?php echo $qry_CJA_dispo_aff - $qry_CJA_dispo_uti ?>/<?php echo $qry_CJA_dispo_aff ?></td>													
							  </tr>
							  <tr> 
								<td colspan="5" bgcolor="#CCCCCC" height="1"></td>
							  </tr>
							  <?php
							}
						  }
						?>
					  </table>
					</td>
					<td width="14"></td>
				  </tr>
				  <tr> 
					<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				  </tr>
				</table>
				<br>
				  <?php
					if (is_array($qry_jet)){ // Si on a cliqué sur un jeton ou si le partenaire n'a encore aucun jeton
					  ?>
			  <table width="640" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Certifies2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo ucfirst($qry_jet[0]['prod_nom']) .' - '.ucfirst($qry_jet[0]['type_jeton']) ?></td>
				</tr>
			  </table>
				<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="640" align="center">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
					<tr> 
					  <td width="14"></td>
					  <td class="TX_Certifies"><?php echo $t_jet_recap_aff ?></td>
					  <td width="14"></td>
					</tr>
					<tr> 
					  <td height="1"></td>
					  <td height="1" bgcolor="#000000"></td>
					  <td height="1"></td>
					</tr>
				  <tr> 
					<td width="14"></td>
					<td align="left" valign="top" class="TX">
					  <table border="0" cellspacing="0" cellpadding="0" width="600">
						<tr> 
						  <td colspan="5" class="TX">&nbsp;</td>
						</tr>
						<tr bgcolor="#000000"> 
						  <td bgcolor="#000000" height="1"></td>
						  <td bgcolor="#000000" height="1"></td>
						  <td bgcolor="#000000" height="1"></td>
						  <td bgcolor="#000000" height="1"></td>
						  <td bgcolor="#000000" height="1"></td>
						</tr>
						<tr> 
						  <td class="TX_bold" align="center"><?php echo $t_date_aff ?></td>
						  <td class="TX_bold" align="center"><?php echo $t_jet_nbre ?></td>
						  <td class="TX_bold" align="center"><?php echo $t_responsable ?></td>
						  <td class="TX_bold" align="center"><?php echo $t_comment ?></td>
						  <td class="TX_bold" align="center"><?php echo $t_annulation ?></td>
						</tr>
						<tr bgcolor="#000000"> 
						  <td bgcolor="#000000" height="1"></td>
						  <td bgcolor="#000000" height="1"></td>
						  <td bgcolor="#000000" height="1"></td>
						  <td bgcolor="#000000" height="1"></td>
						  <td bgcolor="#000000" height="1"></td>
						</tr>
						<tr> 
						  <td colspan="5" class="TX">&nbsp;</td>
						</tr>
						  <?php
						  if (is_array($qry_jet)){
							  foreach($qry_jet as $jet){
									/* On vérifie que le pack n'a pas été utilisé */
									$sql_verif_jet = "SELECT jet_id,affecte,utilise from jeton_corresp_cert WHERE CERT_JET_ID='".txt_db(intval($jet['cert_jet_id']))."'";
									$qry_verif_jet = $db->query($sql_verif_jet);
								?>
								  <tr> 
									<td align="center" class="TX">
									<?php
										echo $jet['cert_jet_date_attribution'];
									?>
									</td>
									<td align="center" class="TX"><?php
										if($jet['cert_jet_resp']==0){							
											echo '<a href="#" onClick="MM_openBrWindow(\'admvak_produits_edit_contactClient2.php?certjetid='. $jet['cert_jet_id'] .'\',\'\',\'toolbar=yes,scrollbars=yes,resizable=yes\')">'.$jet['cert_jet_nombre'].'</a>';
										}else{
											echo $jet['cert_jet_nombre'];
										}
									?></td>
									<td align="center" class="TX"><?php echo $tab_resp[$jet['cert_jet_resp']] ?></td>
									<td align="center" class="TX"><?php echo $jet['cert_jet_info1'].'<br>'.$jet['cert_jet_info2'] ?></td>
									<td align="center" class="TX">
									<?php
									if($jet['cert_jet_resp']==0){									
										/* Si le pack n'est pas utilisé */
										if($qry_verif_jet[0]['utilise']==0){
											?>
											<img onmouseover="this.style.cursor='pointer'" src="../images/icon_supp2.gif" width="11" height="12" border="0" onclick="delete_jeton('<?php echo $jet['cert_jet_id'] ?>');" alt="annulation">
											<?php
										}
									}
									?>
									</td>
								  </tr>
								  <tr> 
									<td colspan="5" bgcolor="#CCCCCC" height="1"></td>
								  </tr>
								  <?php
							  }
						  }
						  ?>
						<tr> 
						  <td  class="TX">&nbsp;</td>
						  <td  class="TX">&nbsp;</td>
						  <td  class="TX">&nbsp;</td>
						  <td  class="TX">&nbsp;</td>
						  <td  class="TX">&nbsp;</td>
						</tr>
					  </table>
					<?php
					}
					?>
					</td>
					<td width="14"></td>
				  </tr>
				  <tr> 
					<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				  </tr>
				</table>
				<table align="center" width="800">
					<tr> 
					  <td align="center" width="800" >
						<center><input type="button" name="Submit" value="<?php echo $t_fermer ?>" class="bn_valider_certifie" onClick="window.opener.location.reload(true); window.close();"></center>
					  </td>
					</tr>
				</table>
			  </td>
			</tr>
			<tr>
			  <td align="center">&nbsp;</td>
			</tr>
			<tr> 
			  <td align="right" width="180">&nbsp; </td>
			</tr>
		  </table>
		</form>
		</body>
		</html>
		<?php
		}else{
			include('../config/lib/lang.php');
			echo $t_acn_1;
		}
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{
	include('no_acces.php');
}
?>