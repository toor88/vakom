<?php ?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">

			</head>

			<body bgcolor="#FFFFFF" text="#000000">
			<form name="form" method="post" action="#">
				<table width="830" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;QUESTIONNAIRE BPM</td>
				</tr>
				</table>
				
				
			  <table border="0" cellspacing="0" cellpadding="0"  width="830" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td class="TX_Tarifs" colspan="3">Ajout&nbsp;d'une&nbsp;question</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td width="14"></td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">Langue : </td>
				  <td colspan="2" align="left"><select name="langue" class="form_ediht_Tarifs">
					  <option value="">Français</option>
					  <option value="">Anglais</option>
					  <option value="">Allemand</option>
					  <option value="">Italien</option>
					</select>			      </td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">Thème :</td>
				  <td align="left"><span class="TX">
				    <select name="Theme" class="form_ediht_Tarifs">
				      <option value="">Piloter une équipe</option>
				      <option value="">Piloter une organisation</option>
				      <option value="">Se piloter</option>
			      </select>
				  </span></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">Sous-thème : </td>
				  <td align="left"><span class="TX">
				    <select name="stheme" class="form_ediht_Tarifs">
				      <option value="">Animer et motiver son équipe</option>
				      <option value="">Accompagner ses collaborateurs</option>
				      <option value="">Organiser</option>
				      <option value="">D&eacute;velopper </option>
				      <option>Comportement</option>
				      <option>Relations interpersonnelles</option>
			      </select>
				  </span></td>
				  <td align="left" class="TX">Item : 
				    <select name="Item" class="form_ediht_Tarifs">
				      <option value="">Porter la politique de l'entreprise</option>
				      <option value="">Communiquer et informer</option>
				      <option value="">Conduite de r&eacute;union</option>
				      <option value="">G&eacute;rer les tensions et les conflits</option>
				      <option value="">Favoriser la coh&eacute;sion d'&eacute;quipe</option>
				      <option value="">Impliquer et motiver</option>
				      <option value="">D&eacute;l&eacute;guer et d&eacute;velopper l'autonomie</option>
				      <option value="">Reconnaitre ses collaborateurs</option>
				      <option>G&eacute;rer et analyser l'activit&eacute;</option>
				      <option>Prendre des d&eacute;cisions</option>
				      <option>Faire respecter l'organisation et les r&egrave;gles </option>
				      <option>D&eacute;velopper l'activit&eacute;</option>
				      <option>Conduire le changement</option>
				      <option>Innover, &ecirc;tre force de proposition</option>
				      <option>Adopter une posture de manager</option>
				      <option>G&eacute;rer son temps et les priorit&eacute;s</option>
				      <option>G&eacute;rer le stress</option>
				      <option>D&eacute;v&eacute;lopper des relations interpersonnelles de qualit&eacute;</option>
	              </select></td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">Question : </td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">Manager</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left" class="TX"> 
					<textarea name="question" id="question" class="form_ediht_Tarifs" style="height: 50px; width: 90%"></textarea>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">Collaborateur</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left" class="TX"><textarea name="question2" class="form_ediht_Tarifs" style="height: 50px; width: 90%"></textarea></td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">Collaboratrice</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left" class="TX"><textarea name="question3" class="form_ediht_Tarifs" style="height: 50px; width: 90%"></textarea></td>
				  <td></td>
			    </tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
			  <br>
			  <p style="text-align:center">
					
					<input type="button" onClick="" name="Submit" value="Valider" class="BN">
			  </p>
                  
			</form>
			</body>
			</html>
	