<?php
require_once("../config/lib/connex.php");
require_once("../config/lib/db.oracle.php");
$db = new db($conn);
	
$sql_detail_vakom 	= "SELECT * FROM PARTENAIRE WHERE PART_ID=724";
$qry_detail_vakom 	= $db->query($sql_detail_vakom);

// Definition de l'URL du Partenaire VAKOM
$temp_site_web 		= explode('http://', $qry_detail_vakom[0]['part_site_web']);
$site_web 			= $temp_site_web[(count($temp_site_web)-1)];


?>
<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo_home.css" type="text/css">
<link rel="stylesheet" href="../css/style-connexion.css" type="text/css">

<!-- Add jQuery library -->
<script type="text/javascript" src="../js/jquery-latest.min.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="../fancybox/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen">
<script type="text/javascript" src="../fancybox/jquery.fancybox.pack.js?v=2.1.4"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="../fancybox/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen">
<script type="text/javascript" src="../fancybox/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="../fancybox/jquery.fancybox-media.js?v=1.0.5"></script>
<link rel="stylesheet" href="../fancybox/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen">
<script type="text/javascript" src="../fancybox/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript">

$(document).ready(function() {
	
	$(".various").fancybox({

		fitToView	: false,
		width		: '40%',
		height		: '40%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	

	
});
</script>
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  //window.open(theURL,winName,features);
  window.open(theURL,winName,features);	  
}
function open_email(){
	MM_openBrWindow('renvoi_mail.php','renvoi_mail','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=250')
}
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body>
<div id="wrap">
<div id="contenu">
<div id="header">
  <img src="../images/vakom-sens-de-humain.png" style="float:right">
  <img src="../images/logo-OPR-header.png" style="margin-top:40px"></div>  
<p class="bienvenu">MERCI DE BIEN VOULOIR VOUS IDENTIFIER </p>  
<div id="zoneSaisie">
<form name="identif" action="whoconnex.php" method="post">
	<input type="password" name="login" class="form_ediht" size="18" placeholder="login">
	  &nbsp;
	<input type="password" name="mdp" class="form_ediht" size="18" placeholder="password">

	<input name="submit" type="submit" value="valider" class="bn_gris_FdOrange">
<p> <a class="various" data-fancybox-type="iframe" href="pop-connexion.php">Mot de passe oublié, 
Cliquer ici</a></p>

</form>
</div><!-- / FIN ZOne saisie -->	

</div>

</div>

</body>
</html>

