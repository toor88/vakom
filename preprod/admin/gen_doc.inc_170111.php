<?php
function html2r($color)
{
  // gestion du #...
  if (substr($color,0,1)=="#") $color=substr($color,1,6);

  $tablo = hexdec(substr($color, 0, 2));
  return $tablo;
}
function html2g($color)
{
  // gestion du #...
  if (substr($color,0,1)=="#") $color=substr($color,1,6);

  $tablo = hexdec(substr($color, 2, 2));
  return $tablo;
}
function html2b($color)
{
  // gestion du #...
  if (substr($color,0,1)=="#") $color=substr($color,1,6);

  $tablo = hexdec(substr($color, 4, 2));
  return $tablo;
}

include ("Artichow/LinePlot.class.php");
include ("Artichow/ScatterPlot.class.php");
include ("Artichow/BarPlot.class.php");

define('FPDF_FONTPATH','pdf/font/');
include ("./pdf/fpdi.php");

function duree( $time ) {
	$tabTemps = array("j" => 86400,
	"h"  => 3600,
	"'" => 60,
	"\"" => 1);

	$result = "";

	foreach($tabTemps as $uniteTemps => $nombreSecondesDansUnite) {
		$$uniteTemps =  floor ($time/$nombreSecondesDansUnite);
		$time = $time%$nombreSecondesDansUnite;

		if($$uniteTemps > 0 ){
			if ($$uniteTemps<10){
				$$uniteTemps = '0'.$$uniteTemps;
			}
			$result .= $$uniteTemps."$uniteTemps";
		}
	}
	return  $result;
	
}
	
function rgb2hex($rgb){
	 if(!is_array($rgb)){
		  echo "Error : input must be an array"; 
		  return 0; 
	  }
	  
	 $hex = ""; 
	 for($i=0; $i<3; $i++) { 
		  if( ($rgb[$i] > 255) || ($rgb[$i] < 0)){
		  echo "Error : input must be between 0 and 255";
		  return 0; 
	  }
	  $tmp = dechex($rgb[$i]);
	  if(strlen($tmp) < 2) $hex .= "0". $tmp;
		else $hex .= $tmp;
	  }
	  
	  return $hex; 
}

function Cpte_Page_PDF($f){
     $handle = @fopen($f, "r");
     if ($handle) {
       while (!feof($handle)) {
        $buffer = fgets($handle, 4096);
        if( preg_match("/Type\s*\/Page[^s]/", $buffer) ){
          ++$i;
        }
       }
       fclose($handle);
     }
     $i = 0+$i;
     return $i;
}

$chemin 		= "./temp/";
$entre_tmp 		= 1;
$num_page 		= 0;
$Entre_Equipe 	= 0;
$Zone_Equipe 	= 0;
if ($var_cand_id && $var_ope_id && $var_doss_id)
{
	$candid_init = $var_cand_id;
	$fname		  	= intval($candid_init).intval($var_ope_id).intval($var_doss_id);
		
	$sql_infos_cand_equipe = "SELECT CANDIDAT.CAND_ID FROM CANDIDAT,CAND_A_OPE WHERE CANDIDAT.CAND_ID=CAND_A_OPE.CAND_ID";
	if ($var_equipe == 0){
		$sql_infos_cand_equipe.= " AND CANDIDAT.CAND_ID=".$candid_init;
	}
	$sql_infos_cand_equipe.= " AND OPE_ID=".txt_db(intval($var_ope_id))." ORDER BY CAND_NOM";
	//echo $sql_infos_cand_equipe;
	$qry_infos_cand_equipe = $db->query($sql_infos_cand_equipe);
	foreach($qry_infos_cand_equipe as $infos_cand_equipe){
		$candid_dernier_equipe = $infos_cand_equipe['cand_id'];
	}
	
	$sql_infos_cand_equipe = "SELECT DATE_QUEST, TO_CHAR(DATE_DEB, 'YYYYMMDDHH24MISS') TIME_DEB, TO_CHAR(DATE_FIN, 'YYYYMMDDHH24MISS') TIME_FIN, CANDIDAT.CAND_ID,OPERATION.DATE_CREATION FROM CANDIDAT,CAND_A_OPE,OPERATION WHERE CAND_A_OPE.OPE_ID=OPERATION.OPE_ID AND CANDIDAT.CAND_ID=CAND_A_OPE.CAND_ID";
	if ($var_equipe == 0){
		$sql_infos_cand_equipe.= " AND CANDIDAT.CAND_ID=".$candid_init;
	}
	$sql_infos_cand_equipe.= " AND CAND_A_OPE.OPE_ID=".txt_db(intval($var_ope_id))." ORDER BY CAND_NOM";
	//echo $sql_infos_cand_equipe;
	$qry_infos_cand_equipe = $db->query($sql_infos_cand_equipe);
	
	// Sélection des informations de saisie de CR
	if($var_equipe == 0){
		$sql_sel_cr = "SELECT DATE_CR, CERT_NOM, CERT_PRENOM FROM CAND_A_CR, CERTIFIE WHERE CERTIFIE.CERT_ID=CAND_A_CR.CERT_ID AND CAND_ID=".intval($candid_init)." AND OPE_ID=".intval($var_ope_id)."";
		$qry_sel_cr = $db->query($sql_sel_cr);
		$date_cr 	= $qry_sel_cr[0]['date_cr'];
		$cert_cr 	= $qry_sel_cr[0]['cert_prenom'].' '.$qry_sel_cr[0]['cert_nom'];
	}
	
	// Définition de la durée de saisie des réponses
	$deb 		= mktime(substr($qry_infos_cand_equipe[0]['time_deb'], 8, 2), substr($qry_infos_cand_equipe[0]['time_deb'], 10, 2), substr($qry_infos_cand_equipe[0]['time_deb'], 12, 2), substr($qry_infos_cand_equipe[0]['time_deb'], 4, 2), substr($qry_infos_cand_equipe[0]['time_deb'], 6, 2), substr($qry_infos_cand_equipe[0]['time_deb'], 0, 4));
	$fin 		= mktime(substr($qry_infos_cand_equipe[0]['time_fin'], 8, 2), substr($qry_infos_cand_equipe[0]['time_fin'], 10, 2), substr($qry_infos_cand_equipe[0]['time_fin'], 12, 2), substr($qry_infos_cand_equipe[0]['time_fin'], 4, 2), substr($qry_infos_cand_equipe[0]['time_fin'], 6, 2), substr($qry_infos_cand_equipe[0]['time_fin'], 0, 4));
	$time_rep 	= $fin - $deb;
	$duree_P	= duree($time_rep);
	
	foreach($qry_infos_cand_equipe as $infos_cand_equipe){
		$Entre_Erreur_Mail = 0;
		$candid_equipe = $infos_cand_equipe['cand_id'];
		$sql_infos_prod_doss = "SELECT * FROM PRODUIT_A_DOSSIER,OPERATION WHERE PRODUIT_A_DOSSIER.PROD_ID=OPERATION.PROD_ID AND OPERATION.OPE_ID=".txt_db(intval($var_ope_id))." and DOSSIER_ID=".txt_db(intval($var_doss_id))."";
		$qry_infos_prod_doss = $db->query($sql_infos_prod_doss);
	//echo $sql_infos_prod_doss;
		/* Si le candidat est sélectionné */
		$sql_info_candidat = "select CAND_NOM,CAND_PRENOM,CAND_SEXE,CLI_NOM,CAND_LANG_ID
								from candidat,client
								where candidat.cand_cli_id=client.cli_id(+)
								and cand_id=".$candid_equipe;
		$qry_info_candidat = $db->query($sql_info_candidat);
	//echo $sql_info_candidat.'<br>';	
		/* On sélectionne les informations sur le dossier pour le candidat et l'opération sélectionnés */	
		$sql_infos_doss = "SELECT PROd_ID,IMAGE_PIED_PAGE, IMAGE_SPECIFIQUE, DATE_DEB, DATE_FIN, DOSSIER_ID,CAND_ID,OPE_ID,PROD_NOM,TITRE,DOC_ID,DOC_NOM,TYPE_GABARIT,EQUIPE,TRI FROM CAND_DOC 
			WHERE CAND_ID=".$candid_equipe." 
				AND OPE_ID=".txt_db(intval($var_ope_id))." 
				AND DOSSIER_ID=".txt_db(intval($var_doss_id))." 
			GROUP BY PROD_ID,IMAGE_PIED_PAGE, IMAGE_SPECIFIQUE, DATE_DEB, DATE_FIN, DOSSIER_ID,CAND_ID,OPE_ID,PROD_NOM,TITRE,DOC_ID,DOC_NOM,TYPE_GABARIT,EQUIPE,TRI ORDER BY DOSSIER_ID,TRI";
		$qry_infos_doss = $db->query($sql_infos_doss);
		
		
		$tab_titre_page_garde 	= explode('$', $qry_infos_doss[0]['titre']);
		$titre_page_garde 		= trim($tab_titre_page_garde[0]);
		$sous_titre_page_garde 	= trim($tab_titre_page_garde[1]);
		
		if($qry_infos_doss[0]['image_specifique']!=''){
			$page_garde = $qry_infos_doss[0]['image_specifique'];
		}else{
			$page_garde = 'fond_page_garde.bmp';
		}
		
		if($qry_infos_doss[0]['image_pied_page']!=''){
			$pied_page = $qry_infos_doss[0]['image_pied_page'];
		}else{
			$pied_page = 'pied_page.jpg';
		}
/*
echo $qry_infos_doss[0]['image_specifique'].'<br><br><br>'.$qry_infos_doss[0]['image_pied_page'];
echo $sql_infos_doss.'<br>';
*/

		$sql_cert_info = "SELECT CERTIFIE.* FROM CERTIFIE, CAND_A_OPE WHERE CERTIFIE.CERT_ID=CAND_A_OPE.CERT_ID AND CAND_A_OPE.CAND_ID=".$candid_init." AND CAND_A_OPE.OPE_ID=".txt_db(intval($var_ope_id));
		$qry_cert_info = $db->query($sql_cert_info);

		$deb_page_html = '
<html>
	<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="http://www.extranet.lesensdelhumain.com/css/nvo.css" type="text/css">
		<link rel="stylesheet" href="http://www.extranet.lesensdelhumain.com/css/general.css" type="text/css">
		<link rel="stylesheet" href="http://www.extranet.lesensdelhumain.com/css/import.css" type="text/css">
	</head>
	<body bgcolor="#FFFFFF" text="#000000" width="860">
	';
	
	if ($var_equipe == 0){

		$mid_page_html = '
		<center>
			<table width="890" style="page-break-before:always" align="center" height="1280">
			<tr>
				<td valign="top" height="1280" width="886" style="background-image: url(\'http://www.extranet.lesensdelhumain.com/admin/images_produit/'.$page_garde.'\'); background-position: bottom center; background-repeat: no-repeat;">
				<div align="center">
				<table width="800" height="1030">
					<tr><td width="700" valign="top"><br><br><br><br><br><br>
					<span style="font-size: 46pt"><span style="font-family: Lucida Sans"><b><font color="#a379bb">'.$titre_page_garde.'</font></b></span></span><br>';
					if($sous_titre_page_garde!=''){
						$mid_page_html .= '<span style="font-size: 20pt"><span style="font-family: Lucida Sans"><font color="#a379bb">'.$sous_titre_page_garde.'</font></span></span><br>';
					}
					$mid_page_html .= '<br><br><h2 class="stitre_doss3">
						<br><br><b>de '.$qry_info_candidat[0]['cand_prenom'].' '.$qry_info_candidat[0]['cand_nom'].'</h2><h2 class="stitre_doss2">'.$qry_info_candidat[0]['cli_nom'].'</b></h2>
						<br />';
						if ($qry_infos_prod_doss[0]['operation']==1){
							$sql_info_ope = "SELECT CODE.CODE_LIBELLE,TYPE_OPE_LIBELLE FROM OPERATION,TYPE_OPERATION,CODE WHERE TYPE_OPERATION.TYPE_OPE_CODE_ID=CODE.CODE_ID AND OPERATION.TYPE_OPE_ID=TYPE_OPERATION.TYPE_OPE_ID AND OPE_ID=".intval($var_ope_id)."";							
							$qry_info_ope = $db->query($sql_info_ope);
							$mid_page_html.= '<span class="bold_garde">Dans le cadre de : </span>'.$qry_info_ope[0]['code_libelle'].' '.$qry_info_ope[0]['type_ope_libelle'].'<br>';
						}
						
						$mid_page_html.= '<span class="bold_garde"><br><br><br><br><br><br>';
						
						if($date_cr != ''){
							$mid_page_html.= 'Entretien individuel avec '.$cert_cr.' du '.$date_cr.'<br><br>';
						}else{
							$mid_page_html.= '<br><br><br>';
						}
						$mid_page_html.= '<span class="bold_garde">
						Temps de passation : '.$duree_P.'</span><br><br>
						<span class="bold_garde">A la demande du certifié OPR&reg; : </span>'.$qry_cert_info[0]['cert_prenom'].' '.$qry_cert_info[0]['cert_nom'].'<br><br>';
						$mid_page_html.= 'Questionnaire renseigné le : </span>'.$qry_infos_cand_equipe[0]['date_quest'].'
					</td></tr>
				</table></div>
				<table width="861" height="250" ><tr>
					<td width="500">';
										
					// Sélection des informations partenaire
					$sql_part_info = "SELECT * FROM PARTENAIRE WHERE PART_ID=".intval($qry_cert_info[0]['cert_part_id']);
					$qry_part_info = $db->query($sql_part_info);					
					if($qry_part_info[0]['part_logo3']=='')
					{
						$sql_part_info = "SELECT * FROM PARTENAIRE WHERE PART_NOM='VAKOM'";
						$qry_part_info = $db->query($sql_part_info);					
					}
					if($qry_part_info[0]['part_logo3']!='' && file_exists('./images_societe/'.$qry_part_info[0]['part_logo3'])){
						$mid_page_html .= '&nbsp;&nbsp;&nbsp;<img src="http://www.extranet.lesensdelhumain.com/admin/images_societe/'.$qry_part_info[0]['part_logo3'].'"><br><br><br><br><br><br><br><br>';
					}
					$mid_page_html .= '</td>
					<td style="color:white;"><font face="Franklin Gothic Book" size="4">';
					
					if($qry_part_info[0]['part_logo3']!='' && file_exists('./images_societe/'.$qry_part_info[0]['part_logo3'])){
						$mid_page_html .= $qry_part_info[0]['part_nom'] . '<br>'.$qry_part_info[0]['part_ad1'] . '<br>';
						if(trim($qry_part_info[0]['part_ad2'])!=''){
							$mid_page_html .= $qry_part_info[0]['part_ad2'] . '<br>';
						}
						$mid_page_html .= $qry_part_info[0]['part_cp']. ' - '.$qry_part_info[0]['part_ville'].'<br>';
						
						if($qry_part_info[0]['part_tel']!=''){
							$mid_page_html .= 'Tél. : '.$qry_part_info[0]['part_tel'];
						}
						if($qry_part_info[0]['part_tel']!='' && $qry_part_info[0]['part_fax']!=''){
							$mid_page_html .= ' - ';
						}					
						if($qry_part_info[0]['part_fax']!=''){
							$mid_page_html .= 'Fax : '.$qry_part_info[0]['part_fax'];
						}
						$mid_page_html .= '<br>';
						if($qry_part_info[0]['part_site_web']!=''){
							$mid_page_html .= '<span>'.$qry_part_info[0]['part_site_web'].'</span>';
						}
					}else{	
						$mid_page_html .= '38 rue Bouquet - 76108 Rouen Cedex 1<br>
						Tél. : 02 32 10 59 20 - Fax : 02 32 10 59 21<br>
						<span>http://www.vakom.fr</span>   - accueil@vakom.fr<br>
						RCS Rouen 438 715 252';
					}
					$mid_page_html .='</font>
					</td></tr>
				</table>
				</td>
			</tr>
		';
	}
	else
	{
		$mid_page_html = '
		<center>
			<table width="890" style="page-break-before:always" align="center" height="1280">
			<tr>
				<td valign="top" height="1280" width="886" style="background-image: url(\'http://www.extranet.lesensdelhumain.com/admin/images_produit/'.$page_garde.'\'); background-position: bottom center; background-repeat: no-repeat;">
				<div align="center">
				<table width="800" height="1030">
					<tr><td width="700" valign="top"><br><br><br><br><br><br>
					<span style="font-size: 46pt"><span style="font-family: Lucida Sans"><b><font color="#a379bb">'.$titre_page_garde.'</font></b></span></span><br />';
					
					if($sous_titre_page_garde!=''){
						$mid_page_html .= '<span style="font-size: 20pt"><span style="font-family: Lucida Sans"><font color="#a379bb">'.$sous_titre_page_garde.'</font></span></span><br />';
					}
					
					$mid_page_html .= '<br /><br /><br /><br /><br /><br /><br />';
						$sql_info_ope = "SELECT CODE.CODE_LIBELLE,TYPE_OPE_LIBELLE FROM OPERATION,TYPE_OPERATION,CODE WHERE TYPE_OPERATION.TYPE_OPE_CODE_ID=CODE.CODE_ID AND OPERATION.TYPE_OPE_ID=TYPE_OPERATION.TYPE_OPE_ID AND OPE_ID=".intval($var_ope_id)."";
						$qry_info_ope = $db->query($sql_info_ope);
						$mid_page_html.= '<span class="bold_garde">Dans le cadre de : </span>'.$qry_info_ope[0]['code_libelle'].' '.$qry_info_ope[0]['type_ope_libelle'].'<br>';
						$sql_infos_cand_equipe_garde = "SELECT CANDIDAT.* FROM CANDIDAT,CAND_A_OPE WHERE CANDIDAT.CAND_ID=CAND_A_OPE.CAND_ID AND OPE_ID=".txt_db(intval($var_ope_id))." ORDER BY CAND_NOM";
						$qry_infos_cand_equipe_garde = $db->query($sql_infos_cand_equipe_garde);
						$mid_page_html.= '<span class="bold_garde">Personnes concernées : </span>';
						foreach($qry_infos_cand_equipe_garde as $infos_cand_equipe_garde){
							$mid_page_html.= $infos_cand_equipe_garde['cand_prenom'].' '.$infos_cand_equipe_garde['cand_nom'];
							if ($candid_dernier_equipe != $infos_cand_equipe_garde['cand_id']){
								$mid_page_html.= ', ';
							}
						}
						$mid_page_html.= '<br><span class="bold_garde"><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Certifié OPR&reg; : </span>'.$qry_cert_info[0]['cert_prenom'].' '.$qry_cert_info[0]['cert_nom'].'<br>
					</td></tr>
				</table></div>
				<table width="861" height="250" ><tr>
					<td width="500">';
					
					// Sélection des informations partenaire
					$sql_part_info = "SELECT * FROM PARTENAIRE WHERE PART_ID=".intval($qry_cert_info[0]['cert_part_id']);
					$qry_part_info = $db->query($sql_part_info);	
					if($qry_part_info[0]['part_logo3']=='')
					{
						$sql_part_info = "SELECT * FROM PARTENAIRE WHERE PART_NOM='VAKOM'";
						$qry_part_info = $db->query($sql_part_info);					
					}
					
					if($qry_part_info[0]['part_logo3']!='' && file_exists('./images_societe/'.$qry_part_info[0]['part_logo3'])){
						$mid_page_html .= '&nbsp;&nbsp;&nbsp;<img src="http://www.extranet.lesensdelhumain.com/admin/images_societe/'.$qry_part_info[0]['part_logo3'].'"><br><br><br><br><br><br><br><br>';
					}
					$mid_page_html .= '</td>
					<td style="color:white;"><font face="Franklin Gothic Book" size="4">';

					if($qry_part_info[0]['part_logo3']!='' && file_exists('./images_societe/'.$qry_part_info[0]['part_logo3'])){
						$mid_page_html .= $qry_part_info[0]['part_nom'] . '<br>'.$qry_part_info[0]['part_ad1'] . '<br>';
						if(trim($qry_part_info[0]['part_ad2'])!=''){
							$mid_page_html .= $qry_part_info[0]['part_ad2'] . '<br>';
						}
						$mid_page_html .= $qry_part_info[0]['part_cp']. ' - '.$qry_part_info[0]['part_ville'].'<br>';
						
						if($qry_part_info[0]['part_tel']!=''){
							$mid_page_html .= 'Tél. : '.$qry_part_info[0]['part_tel'];
						}
						if($qry_part_info[0]['part_tel']!='' && $qry_part_info[0]['part_fax']!=''){
							$mid_page_html .= ' - ';
						}					
						if($qry_part_info[0]['part_fax']!=''){
							$mid_page_html .= 'Fax : '.$qry_part_info[0]['part_fax'];
						}
						$mid_page_html .= '<br>';
						if($qry_part_info[0]['part_site_web']!=''){
							$mid_page_html .= '<span>'.$qry_part_info[0]['part_site_web'].'</span>';
						}
					}else{	
						$mid_page_html .= '38 rue Bouquet - 76108 Rouen Cedex 1<br>
						Tél. : 02 32 10 59 20 - Fax : 02 32 10 59 21<br>
						<span>http://www.vakom.fr</span>   - accueil@vakom.fr<br>
						RCS Rouen 438 715 252';
					}
					$mid_page_html .='</font>
					</td></tr>
				</table>
				</td>
			</tr>
		';
	}
$fin_page_html = '	
		</table>
	</center>
	</body>
</html>
';
if(intval($var_passage)<1){
$footer_content = "<html><head><script>
  function subst() {
    var vars={};
    var x=document.location.search.substring(1).split('&');
    for(var i in x) {var z=x[i].split('=',2);vars[z[0]] = unescape(z[1]);}
    var x=['frompage','topage','page','webpage','section','subsection','subsubsection'];
    for(var i in x) {
      var y = document.getElementsByClassName(x[i]);
      for(var j=0; j<y.length; ++j) y[j].textContent = vars[x[i]];
    }
  }
  </script>
	<link rel=\"stylesheet\" href=\"http://www.extranet.lesensdelhumain.com/css/nvo.css\" type=\"text/css\">
	<link rel=\"stylesheet\" href=\"http://www.extranet.lesensdelhumain.com/css/general.css\" type=\"text/css\">
  </head><body bgcolor=\"#FFFFFF\" text=\"#000000\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" onload=\"subst()\">
  <table width=\"881\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"76\">
  <tr> 
    <td background=\"http://www.extranet.lesensdelhumain.com/admin/images_produit/".$pied_page."\" style='background-repeat: no-repeat;' align=\"right\"> 
		<table width=\"740\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>";
		if($qry_part_info[0]['part_logo3']!='' && file_exists('./images_societe/'.$qry_part_info[0]['part_logo3'])){
			$footer_content .=  "<td rowspan=\"2\" width=\"140\" style=\"text-align: center;\"><img height=\"70\" src=\"http://www.extranet.lesensdelhumain.com/admin/images_societe/".$qry_part_info[0]['part_logo3']."\"></td>";
		}
        $footer_content .= "<td rowspan=\"2\"  width=\"400\" class=\"TXBLANC_Bold\">".$titre_page_garde;
		if ($var_equipe == 0){
			$footer_content .= "<br>".$qry_info_candidat[0]['cand_prenom'].' '. $qry_info_candidat[0]['cand_nom'].' - '.$qry_infos_cand_equipe[0]['date_quest'];
		}
		else
		{
			$footer_content .= "<br>".$qry_infos_cand_equipe[0]['date_creation'];
		}
	    $footer_content .= "</td>";
        $footer_content .= "<td class=\"TXBLANC_Bold_Page\" rowspan=\"2\" align=\"center\">Page&nbsp;<span class=\"page\"></span>&nbsp;/&nbsp;<span class=\"topage\"></span></td>
        </tr>";
		$footer_content .= "
      </table>
    </td>
  </tr>
</table>
</body>
</html>";
  
$footp = fopen( $chemin.intval($var_cand_id).intval($var_ope_id).intval($var_doss_id).'_footer.html',"w+");
fputs($footp,$footer_content);
fclose($footp);
$var_passage++;
}
$chemin 		= "./temp/";
// On ouvre un nouveau fichier
if ($entre_tmp == 1) {
	$entre_tmp 	= 0;
	$num_page 	= $num_page+1;
	$file 		= $fname.$num_page.".html";
	$fp 		= fopen($chemin . $file ,"w+");
	fputs($fp,$deb_page_html.$mid_page_html.$fin_page_html);
	fclose($fp);
	flush();
	
	/*
	if ($qry_infos_prod_doss[0]['page_garde'] == 1){
		if ($qry_infos_prod_doss[0]['sommaire'] == 1){		
			if ($qry_infos_prod_doss[0]['pied_page'] == 1){		
				$debut_command= '/usr/local/bin/wkhtmltopdf -O Portrait -B 20 -s A4 --cover "'.$chemin . $file.'" --footer-html "'.$chemin.intval($var_cand_id).intval($var_ope_id).intval($var_doss_id).'_footer.html'.'" ';							
			}
			else
			{
				$debut_command= '/usr/local/bin/wkhtmltopdf -O Portrait -s A4 --cover "'.$chemin . $file.'" ';							
			}
		}
		else
		{
			if ($qry_infos_prod_doss[0]['pied_page'] == 1){		
				$debut_command= '/usr/local/bin/wkhtmltopdf -O Portrait -B 20 -s A4 --cover "'.$chemin . $file.'" --footer-html "'.$chemin.intval($var_cand_id).intval($var_ope_id).intval($var_doss_id).'_footer.html'.'" ';							
			}
			else
			{
				$debut_command= '/usr/local/bin/wkhtmltopdf -O Portrait -s A4 --cover "'.$chemin . $file.'" ';									
			}
		}
	}
	
	else{*/

			if ($qry_infos_prod_doss[0]['pied_page'] == 1){		
				$debut_command= '/usr/local/bin/wkhtmltopdf -O Portrait -B 25 -s A4 --footer-html "'.$chemin.intval($var_cand_id).intval($var_ope_id).intval($var_doss_id).'_footer.html'.'" ';							
			}
			else
			{
				$debut_command= '/usr/local/bin/wkhtmltopdf -O Portrait -B 10 -s A4 ';							
			}

		
	//}
}
		
		if (is_array($qry_infos_doss)){
		/* Pour chaque document du dossier */
		foreach($qry_infos_doss as $infos_document){
		if ($var_genere_tout == 0){ // Uniquement les CR
			$sql_doc_cr = "select count(*) nb from document where (type_zone1 = 6 or type_zone2 = 6 or type_zone3 = 6 or type_zone4 = 6) and doc_id=".txt_db(intval($infos_document['doc_id']))."";
			$qry_doc_cr = $db->query($sql_doc_cr);
			if ($qry_doc_cr[0]['nb']>0){
				$Entre_Cr_Tmp = 1;
			}
			else
			{
				$Entre_Cr_Tmp = 0;
			}
		}
		else
		{
			$Entre_Cr_Tmp = 1;
		}
		if ($Entre_Cr_Tmp == 1)
		{
			$j++;
			//echo '<a name="'.$j.'"></a><center><table width="810" style="page-break-before:always" align="center"><tr><td height="1100" valign="top" width="810" align="center">';
				/* On regarde le type de gabarit pour ce document, on définit dans quelle(s) zone(s) on doit chercher */
				switch ($infos_document['type_gabarit']){
					case 1:
					default:
						$nb_zones = 1;
					break;
					case 2:
						$nb_zones = 2;
					break;
					case 3:
					case 4:
						$nb_zones = 3;
					break;
					case 5:
						$nb_zones = 4;
					break;
				}
				
				/* On recherche dans les zones du document */
				for ($zone_id=1;$zone_id<=$nb_zones;$zone_id++){
					$contenu[$zone_id] = "";
					$align[$zone_id] = "";
					$sql_infos_zones = "SELECT CAND_DOC.* FROM CAND_DOC 
					WHERE CAND_ID=".$candid_equipe." 
						AND OPE_ID=".txt_db(intval($var_ope_id))." 
						AND DOSSIER_ID=".txt_db(intval($var_doss_id))." 
						AND DOC_ID=".txt_db(intval($infos_document['doc_id']))." 
						AND NUM_ZONE=".$zone_id." 
					ORDER BY DOSSIER_ID,TRI";
					//echo $sql_infos_zones.'<br>';	
					$infos_zones = $db->query($sql_infos_zones);
					$Entre_Dans_Pdf = 1;
					switch($infos_zones[0]['type_zone'.$zone_id]){
						case 1: // Si c'est un graphe
						
							// Quel type de graphe ? Courbe ? Histo ?
							$sql_sel_graphe_properties = "SELECT GRAPHE_TYPE_GRAPHE_CODE_ID FROM GRAPHE WHERE GRAPHE_ID=".intval($infos_zones[0]['doc_graphe_id']);
							$qry_sel_graphe_properties = $db->query($sql_sel_graphe_properties);
							//echo $sql_sel_graphe_properties.'<br>';	
							
							$align[$zone_id]='center';
							
							$sql_select_graphe = "SELECT * FROM GRAPHE_A_QUEST WHERE GRAPHE_ID=".txt_db(intval($infos_zones[0]['doc_graphe_id']))."";
							//echo $sql_select_graphe.'<br>';
							$qry_select_graphe = $db->query($sql_select_graphe);
											
							$sql_select_detail_graphe = "SELECT DOC_GRAPHE_MULTI FROM DOC_A_INFO WHERE DOC_GRAPHE_ID=".txt_db(intval($infos_zones[0]['doc_graphe_id']))."";
							$qry_select_detail_graphe = $db->query($sql_select_detail_graphe);
							if(is_array($qry_select_graphe[1])){
								$nb_histo = 2;
							}else{
								$nb_histo = 1;
							}
							switch($qry_sel_graphe_properties[0]['graphe_type_graphe_code_id']){
								case 44: //Si c'est un histo
								

								/* Si ce n'est pas un graphe multi utilisateur */
								if(intval($qry_select_detail_graphe[0]['doc_graphe_multi'])<1){
									
									// Les valeurs à afficher sur la courbe
									$sql_select_graphe_points = "SELECT TO_CHAR(COR_VAR_POINTS) RES FROM GRAPHE_PN WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." AND TYPE=".$qry_select_graphe[0]['type_profil_code_id']." AND QUEST_ID=".$qry_select_graphe[0]['quest_id']." ORDER BY ID";
									$qry_select_graphe_points = $db->query($sql_select_graphe_points);
									if ($qry_select_graphe[0]['type_profil_code_id'] == "29") { //PI
										$col1 = new Color(58, 170, 220);
										$col1 = new Color(html2r($qry_select_graphe[0]['couleur']),html2g($qry_select_graphe[0]['couleur']),html2b($qry_select_graphe[0]['couleur']));
									}
									elseif ($qry_select_graphe[0]['type_profil_code_id'] == "30") { //PN
										$col1 = new Color(218, 95, 155);
										$col1 = new Color(html2r($qry_select_graphe[0]['couleur']),html2g($qry_select_graphe[0]['couleur']),html2b($qry_select_graphe[0]['couleur']));
									}
									elseif ($qry_select_graphe[0]['type_profil_code_id'] == "32") { //PNPI (PR ??)
										$col1 = new Color(122, 176, 49);
										$col1 = new Color(html2r($qry_select_graphe[0]['couleur']),html2g($qry_select_graphe[0]['couleur']),html2b($qry_select_graphe[0]['couleur']));
									}
									elseif ($qry_select_graphe[0]['type_profil_code_id'] == "105") { //PNPI FOCUS
										$col1 = new Color(122, 176, 49);
										$col1 = new Color(html2r($qry_select_graphe[0]['couleur']),html2g($qry_select_graphe[0]['couleur']),html2b($qry_select_graphe[0]['couleur']));
									}
									
									// Taille du graphe par défaut
									$width_grille	= 635;
									$height_grille	= 622;
									
									$sql_images = "SELECT TITRE, TOP_GRAPHE, MID_GRAPHE, BOTTOM_GRAPHE, AFFICHE_NOM,MIN_HAUTEUR,MAX_HAUTEUR FROM GRAPHE_A_TEXTE WHERE LANG_ID=".$qry_info_candidat[0]['cand_lang_id']." AND GRAPHE_ID=".intval($qry_select_graphe[0]['graphe_id'])."";
									//echo $sql_images;
									$qry_images = $db->query($sql_images);
									
									###############################
									###############################
										// Définition de la taille de la grille si la grille est enregistrée sur le serveur
										if ($qry_images[0]['mid_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['mid_graphe'])){
											$grille_exists 	= true;
											$grille_link	= 'http://www.extranet.lesensdelhumain.com/graphes/'.$qry_images[0]['mid_graphe'];
											list($width_grille, $height_grille) = getimagesize($grille_link);
										}else{
											$grille_exists = false;
											$grille_link	= 'http://www.extranet.lesensdelhumain.com/graphes/mid_graphes0.jpg';
										}
										
										// Définition de la taille du top si il est enregistré sur le serveur
										if ($qry_images[0]['top_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['top_graphe'])){
											$top_exists = true;
											$top_link   = 'http://www.extranet.lesensdelhumain.com/graphes/'.$qry_images[0]['top_graphe'];
											list($width_top, $height_top) = getimagesize($top_link);
										}else{
											$top_exists = false;
										}
										// Définition de la taille du bottom si il est enregistré sur le serveur
										if ($qry_images[0]['bottom_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['bottom_graphe'])){
											$bottom_exists = true;
											$bottom_link   = 'http://www.extranet.lesensdelhumain.com/graphes/'.$qry_images[0]['bottom_graphe'];
											list($width_bottom, $height_bottom) = getimagesize($bottom_link);
										}else{
											$bottom_exists = false;
										}
									
									
									###############################
									###############################
									
									// Ici, le graphique mesurera X x Y pixels.
									$graph = new Graph($width_grille, $height_grille);
									$graph->setAntiAliasing(TRUE);
									
									// Masquer la bordure
									$graph->border->hide();
																
									$col = new Color(210, 200, 34,100);
									$graph->setFormat(2); // format PNG pour gerer la transparence
									$graph->setBackgroundColor($col);
									
									$group = new PlotGroup;
									//$group->setPadding($width_grille/8, $width_grille/8, 0, 5);
									$group->setPadding(0, 0, 0, 5);
									$group->setBackgroundColor($col);							
									//$group->setYMin(0);
									//$group->setYMax(29);
									$group->setYMin($qry_images[0]['min_hauteur']);
									$group->setYMax($qry_images[0]['max_hauteur']);
									
									//$group->yAxis->setLabelPrecision(1);
									$group->grid->hide();
									$group->axis->bottom->hide();
									$group->axis->left->hide();
									
									if (is_array($qry_select_graphe_points)) {
										$values = array(str_replace(',','.',$qry_select_graphe_points[0]['res']),str_replace(',','.',$qry_select_graphe_points[1]['res']),str_replace(',','.',$qry_select_graphe_points[2]['res']),str_replace(',','.',$qry_select_graphe_points[3]['res']));							
									}else{
										$values = array(0,0,0,0);
										if ($Entre_Erreur_Mail == 0){		
											$Entre_Erreur_Mail = 1;
											$mailto = 'fdegremont@vakom.fr, david.naze@deliberata.com, gilles.bensimon@risc-group.com';
											$sujet_mail = 'Pb génération PDF sur '.$candid_equipe.' '.txt_db(intval($var_ope_id)).' '.$qry_select_graphe[0]['type_profil_code_id'].' '.$qry_select_graphe[0]['quest_id'].'(1)';
											$message_mail = 'Pb génération PDF sur '.$candid_equipe.' '.txt_db(intval($var_ope_id)).' '.$qry_select_graphe[0]['type_profil_code_id'].' '.$qry_select_graphe[0]['quest_id'].'(1)';
											$name_s    		 = 'Dev';
											$email_s 		 = 'Diffusion_Developpement-ITS@risc-group.biz';
											$headers    	 = "From: ". $name_s . " <" . $email_s . ">\r\n";
											$headers   		.= 'Content-Type: text/html; charset="iso-8859-1"'."\r\n";
											$headers  		.= 'Content-Transfer-Encoding: 8bit'."\r\n";
											mail($mailto, $sujet_mail, $message_mail, $headers);
										}
									}
									
									//$values = array(12, 8, 20, 32, 15, 5);
								   
									$plot = new BarPlot($values, 1, $nb_histo);
									$plot->setBarColor($col1);
									$plot->setBarSize(0.4);
									//$plot->setYAxis(Plot_LEFT);
									   
									$group->add($plot);
									
									// 2 COURBES ??
									if(is_array($qry_select_graphe[1])){								
										// Les valeurs à afficher sur la courbe
										$sql_select_graphe_points = "SELECT TO_CHAR(COR_VAR_POINTS) RES FROM GRAPHE_PN WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." AND TYPE=".$qry_select_graphe[1]['type_profil_code_id']." AND QUEST_ID=".$qry_select_graphe[1]['quest_id']." ORDER BY ID";
										$qry_select_graphe_points = $db->query($sql_select_graphe_points);
										$col2 = new Color(html2r($qry_select_graphe[1]['couleur']),html2g($qry_select_graphe[1]['couleur']),html2b($qry_select_graphe[1]['couleur']));
	/*									
										if ($qry_select_graphe[1]['type_profil_code_id'] == "29") { //PI
											$sql_select_graphe_points = "SELECT TO_CHAR(PI) RES FROM GRAPHE_PNPI WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." ORDER BY ID";
											$qry_select_graphe_points = $db->query($sql_select_graphe_points);
											$col2 = new Color(58, 170, 220);
											$col2 = new Color(html2r($qry_select_graphe[1]['couleur']),html2g($qry_select_graphe[1]['couleur']),html2b($qry_select_graphe[1]['couleur']));
										}
										elseif ($qry_select_graphe[1]['type_profil_code_id'] == "30") { //PN
											$sql_select_graphe_points = "SELECT TO_CHAR(PN) RES FROM GRAPHE_PNPI WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." ORDER BY ID";
											$qry_select_graphe_points = $db->query($sql_select_graphe_points);
											$col2 = new Color(218, 95, 155);
											$col2 = new Color(html2r($qry_select_graphe[1]['couleur']),html2g($qry_select_graphe[1]['couleur']),html2b($qry_select_graphe[1]['couleur']));
										}
										elseif ($qry_select_graphe[1]['type_profil_code_id'] == "32") { //PNPI (PR ??)
											$sql_select_graphe_points = "SELECT TO_CHAR(PI-PN) RES FROM GRAPHE_PNPI WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." ORDER BY ID";
											$qry_select_graphe_points = $db->query($sql_select_graphe_points);
											$col2 = new Color(122, 176, 49);
											$col2 = new Color(html2r($qry_select_graphe[1]['couleur']),html2g($qry_select_graphe[1]['couleur']),html2b($qry_select_graphe[1]['couleur']));
										}
	*/
										if (is_array($qry_select_graphe_points)) {
											$values2 = array(str_replace(',','.',$qry_select_graphe_points[0]['res']),str_replace(',','.',$qry_select_graphe_points[1]['res']),str_replace(',','.',$qry_select_graphe_points[2]['res']),str_replace(',','.',$qry_select_graphe_points[3]['res']));							
										}
										else{
											$values2 = array(0,0,0,0);
											if ($Entre_Erreur_Mail == 0){		
												$Entre_Erreur_Mail = 1;
											$mailto = 'fdegremont@vakom.fr, david.naze@deliberata.com, gilles.bensimon@risc-group.com';
											$sujet_mail = 'Pb génération PDF sur '.$candid_equipe.' '.txt_db(intval($var_ope_id)).' '.$qry_select_graphe[0]['type_profil_code_id'].' '.$qry_select_graphe[0]['quest_id'].'(2)';
											$message_mail = 'Pb génération PDF sur '.$candid_equipe.' '.txt_db(intval($var_ope_id)).' '.$qry_select_graphe[0]['type_profil_code_id'].' '.$qry_select_graphe[0]['quest_id'].'(2)';
											$name_s    		 = 'Dev';
											$email_s 		 = 'Diffusion_Developpement-ITS@risc-group.biz';
											$headers    	 = "From: ". $name_s . " <" . $email_s . ">\r\n";
											$headers   		.= 'Content-Type: text/html; charset="iso-8859-1"'."\r\n";
											$headers  		.= 'Content-Transfer-Encoding: 8bit'."\r\n";
											mail($mailto, $sujet_mail, $message_mail, $headers);		
											}
										}
										
										//echo '<br>2>>>'.str_replace(',','.',$qry_select_graphe_points[0]['res']).' '.str_replace(',','.',$qry_select_graphe_points[1]['res']).' '.str_replace(',','.',$qry_select_graphe_points[2]['res']).' '.str_replace(',','.',$qry_select_graphe_points[3]['res']);
														
										$plot2 = new BarPlot($values2,2,2);
										
										$plot2->setBarColor($col2);
										$plot2->setBarSize(0.4);
										
										$group->add($plot2);
																		
									}
								
								}

								break;
								case 36:
								default:
								
								/* Si ce n'est pas un graphe multi utilisateur */
								if(intval($qry_select_detail_graphe[0]['doc_graphe_multi'])<1){
									
									// Les valeurs à afficher sur la courbe
									$sql_select_graphe_points = "SELECT TO_CHAR(COR_VAR_POINTS) RES FROM GRAPHE_PN WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." AND TYPE=".$qry_select_graphe[0]['type_profil_code_id']." AND QUEST_ID=".$qry_select_graphe[0]['quest_id']." ORDER BY ID";
//									echo $sql_select_graphe_points.'<br>';
									$qry_select_graphe_points = $db->query($sql_select_graphe_points);
									
									if ($qry_select_graphe[0]['type_profil_code_id'] == "29") { //PI
										$col1 = new Color(58, 170, 220);
										$col1 = new Color(html2r($qry_select_graphe[0]['couleur']),html2g($qry_select_graphe[0]['couleur']),html2b($qry_select_graphe[0]['couleur']));
									}
									elseif ($qry_select_graphe[0]['type_profil_code_id'] == "30") { //PN
										$col1 = new Color(218, 95, 155);
										$col1 = new Color(html2r($qry_select_graphe[0]['couleur']),html2g($qry_select_graphe[0]['couleur']),html2b($qry_select_graphe[0]['couleur']));
									}
									elseif ($qry_select_graphe[0]['type_profil_code_id'] == "32") { //PNPI (PR ??)
										$col1 = new Color(122, 176, 49);
										$col1 = new Color(html2r($qry_select_graphe[0]['couleur']),html2g($qry_select_graphe[0]['couleur']),html2b($qry_select_graphe[0]['couleur']));
									}
									elseif ($qry_select_graphe[0]['type_profil_code_id'] == "105") { //PNPI FOCUS
										$col1 = new Color(122, 176, 49);
										$col1 = new Color(html2r($qry_select_graphe[0]['couleur']),html2g($qry_select_graphe[0]['couleur']),html2b($qry_select_graphe[0]['couleur']));
									}
									
									if (is_array($qry_select_graphe_points)) {
										$values = array(str_replace(',','.',$qry_select_graphe_points[0]['res']),str_replace(',','.',$qry_select_graphe_points[1]['res']),str_replace(',','.',$qry_select_graphe_points[2]['res']),str_replace(',','.',$qry_select_graphe_points[3]['res']));							
									}
									else{
										$values = array(0,0,0,0);
										if ($Entre_Erreur_Mail == 0){		
											$Entre_Erreur_Mail = 1;
										$mailto = 'fdegremont@vakom.fr, david.naze@deliberata.com, gilles.bensimon@risc-group.com';
										$sujet_mail = 'Pb génération PDF sur '.$candid_equipe.' '.txt_db(intval($var_ope_id)).' '.$qry_select_graphe[0]['type_profil_code_id'].' '.$qry_select_graphe[0]['quest_id'].'(3)';
										$message_mail = 'Pb génération PDF sur '.$candid_equipe.' '.txt_db(intval($var_ope_id)).' '.$qry_select_graphe[0]['type_profil_code_id'].' '.$qry_select_graphe[0]['quest_id'].'(3)';
										$name_s    		 = 'Dev';
										$email_s 		 = 'Diffusion_Developpement-ITS@risc-group.biz';
										$headers    	 = "From: ". $name_s . " <" . $email_s . ">\r\n";
										$headers   		.= 'Content-Type: text/html; charset="iso-8859-1"'."\r\n";
										$headers  		.= 'Content-Transfer-Encoding: 8bit'."\r\n";
										mail($mailto, $sujet_mail, $message_mail, $headers);										
										}
									}
									//echo str_replace(',','.',$qry_select_graphe_points[0]['res']).' '.str_replace(',','.',$qry_select_graphe_points[1]['res']).' '.str_replace(',','.',$qry_select_graphe_points[2]['res']).' '.str_replace(',','.',$qry_select_graphe_points[3]['res']);
									
									// Taille du graphe par défaut
									$width_grille	= 635;
									$height_grille	= 622;
									
									$sql_images = "SELECT TITRE, TOP_GRAPHE, MID_GRAPHE, BOTTOM_GRAPHE, AFFICHE_NOM,MIN_HAUTEUR,MAX_HAUTEUR FROM GRAPHE_A_TEXTE WHERE LANG_ID=".$qry_info_candidat[0]['cand_lang_id']." AND GRAPHE_ID=".intval($qry_select_graphe[0]['graphe_id'])."";
									//echo $sql_images;
									$qry_images = $db->query($sql_images);
									
									###############################
									###############################
										// Définition de la taille de la grille si la grille est enregistrée sur le serveur
										if ($qry_images[0]['mid_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['mid_graphe'])){
											$grille_exists 	= true;
											$grille_link	= 'http://www.extranet.lesensdelhumain.com/graphes/'.$qry_images[0]['mid_graphe'];
											list($width_grille, $height_grille) = getimagesize($grille_link);
										}else{
											$grille_exists = false;
											$grille_link	= 'http://www.extranet.lesensdelhumain.com/graphes/mid_graphes0.jpg';
										}
										
										// Définition de la taille du top si il est enregistré sur le serveur
										if ($qry_images[0]['top_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['top_graphe'])){
											$top_exists = true;
											$top_link   = 'http://www.extranet.lesensdelhumain.com/graphes/'.$qry_images[0]['top_graphe'];
											list($width_top, $height_top) = getimagesize($top_link);
										}else{
											$top_exists = false;
										}
										// Définition de la taille du bottom si il est enregistré sur le serveur
										if ($qry_images[0]['bottom_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['bottom_graphe'])){
											$bottom_exists = true;
											$bottom_link   = 'http://www.extranet.lesensdelhumain.com/graphes/'.$qry_images[0]['bottom_graphe'];
											list($width_bottom, $height_bottom) = getimagesize($bottom_link);
										}else{
											$bottom_exists = false;
										}
									
									
									###############################
									###############################
									
									// Ici, le graphique mesurera X x Y pixels.
									$graph = new Graph($width_grille, $height_grille);
								   
									// L'anti-aliasing permet d'afficher des courbes plus naturelles,
									// mais cette option consomme beaucoup de ressources sur le serveur.
									$graph->setAntiAliasing(TRUE);
									
									// Masquer la bordure
									$graph->border->hide();

									$col = new Color(210, 200, 34,100);
									$graph->setFormat(2); // format PNG pour gerer la transparence
									$graph->setBackgroundColor($col);
									
									$group = new PlotGroup();
									
									$group->setPadding($width_grille/8, $width_grille/8, 0, 10);
									$group->setBackgroundColor($col);
//									$group->setYMin(0);
//									$group->setYMax(29);
									$group->setYMin($qry_images[0]['min_hauteur']);
									$group->setYMax($qry_images[0]['max_hauteur']);
									//$group->yAxis->setLabelPrecision(1);
									$group->grid->hide();
									$group->axis->bottom->hide();
									$group->axis->left->hide();
									
									//echo $qry_select_graphe_points[0]['res'].','.$qry_select_graphe_points[1]['res'].','.$qry_select_graphe_points[2]['res'].','.$qry_select_graphe_points[3]['res'];
									//$values = array(21,24.5,26.5,28);
									// On créé la courbe
									
									$plot = new LinePlot($values);
									
									//$plot->setType(LINE_DASHED);
									
									if($qry_select_graphe[0]['style']==0){
										//0 Pointillé
										$plot->setStyle(LINE_DASHED);
									}else{
										$plot->setStyle(LINE_SOLID);
									}
									
									$plot->mark->setType(MARK_CIRCLE);
									$plot->mark->setFill($col1);
									$plot->mark->setSize(6);

									//$plot->setYAxis(0,29);
									$plot->yAxis->setLabelPrecision(1);
									$plot->grid->hide();
									$plot->yAxis->hide();
									$plot->xAxis->hide();
									$plot->xAxis->hideTicks();
									$plot->xAxis->setLabelInterval(1);
									
									$plot->setColor($col1);

									// On ne change pas l'espace du haut et du bas de la courbe.
									   $plot->setSpace(
										  -10, // Gauche
										  -5, // Droite
										  0, // Haut 
										  4 // Bas
									   );
									   
									$group->add($plot);	
									/*
									$x = array(0, 1, 2, 3);

									$sc_plot = new ScatterPlot($values, $x);
									
									$sc_plot->mark->setType(MARK_CIRCLE);
									$sc_plot->mark->setFill($col1);
									$sc_plot->mark->setSize(6);

									//$sc_plot->setYAxis(0,29);
									$sc_plot->yAxis->setLabelPrecision(1);
									$sc_plot->grid->hide();
									$sc_plot->yAxis->hide();
									$sc_plot->xAxis->hide();
									$sc_plot->xAxis->hideTicks();
									$sc_plot->xAxis->setLabelInterval(1);
									
									$sc_plot->setColor($col1);

									// On ne change pas l'espace du haut et du bas de la courbe.
									   $sc_plot->setSpace(
										  -10, // Gauche 
										  -5, // Droite 
										  0, // Haut 
										  4 // Bas 
									   );
									   
									$group->add($sc_plot);
									*/
									// 2 COURBES ??
									if(is_array($qry_select_graphe[1])){
										$sql_select_graphe_points = "SELECT TO_CHAR(COR_VAR_POINTS) RES FROM GRAPHE_PN WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." AND TYPE=".$qry_select_graphe[1]['type_profil_code_id']." AND QUEST_ID=".$qry_select_graphe[1]['quest_id']." ORDER BY ID";
										$qry_select_graphe_points = $db->query($sql_select_graphe_points);
										$col2 = new Color(html2r($qry_select_graphe[1]['couleur']),html2g($qry_select_graphe[1]['couleur']),html2b($qry_select_graphe[1]['couleur']));
	/*									
										// Les valeurs à afficher sur la courbe
										if ($qry_select_graphe[1]['type_profil_code_id'] == "29") { //PI
											$sql_select_graphe_points = "SELECT TO_CHAR(PI) RES FROM GRAPHE_PNPI WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." ORDER BY ID";
											$qry_select_graphe_points = $db->query($sql_select_graphe_points);
											$col2 = new Color(58, 170, 220);
											$col2 = new Color(html2r($qry_select_graphe[1]['couleur']),html2g($qry_select_graphe[1]['couleur']),html2b($qry_select_graphe[1]['couleur']));
										}
										elseif ($qry_select_graphe[1]['type_profil_code_id'] == "30") { //PN
											$sql_select_graphe_points = "SELECT TO_CHAR(PN) RES FROM GRAPHE_PNPI WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." ORDER BY ID";
											$qry_select_graphe_points = $db->query($sql_select_graphe_points);
											$col2 = new Color(218, 95, 155);
											$col2 = new Color(html2r($qry_select_graphe[1]['couleur']),html2g($qry_select_graphe[1]['couleur']),html2b($qry_select_graphe[1]['couleur']));
										}
										elseif ($qry_select_graphe[1]['type_profil_code_id'] == "32") { //PNPI (PR ??)
											$sql_select_graphe_points = "SELECT TO_CHAR(PI-PN) RES FROM GRAPHE_PNPI WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." ORDER BY ID";
											$qry_select_graphe_points = $db->query($sql_select_graphe_points);
											$col2 = new Color(122, 176, 49);
											$col2 = new Color(html2r($qry_select_graphe[1]['couleur']),html2g($qry_select_graphe[1]['couleur']),html2b($qry_select_graphe[1]['couleur']));
										}
	*/									
										if (is_array($qry_select_graphe_points)) {
											$values2 = array(str_replace(',','.',$qry_select_graphe_points[0]['res']),str_replace(',','.',$qry_select_graphe_points[1]['res']),str_replace(',','.',$qry_select_graphe_points[2]['res']),str_replace(',','.',$qry_select_graphe_points[3]['res']));							
										}
										else{
											$values2 = array(0,0,0,0);
										if ($Entre_Erreur_Mail == 0){		
											$Entre_Erreur_Mail = 1;
											$mailto = 'fdegremont@vakom.fr, david.naze@deliberata.com, gilles.bensimon@risc-group.com';
											$sujet_mail = 'Pb génération PDF sur '.$candid_equipe.' '.txt_db(intval($var_ope_id)).' '.$qry_select_graphe[0]['type_profil_code_id'].' '.$qry_select_graphe[0]['quest_id'].'(4)';
											$message_mail = 'Pb génération PDF sur '.$candid_equipe.' '.txt_db(intval($var_ope_id)).' '.$qry_select_graphe[0]['type_profil_code_id'].' '.$qry_select_graphe[0]['quest_id'].'(4)';
											$name_s    		 = 'Dev';
											$email_s 		 = 'Diffusion_Developpement-ITS@risc-group.biz';
											$headers    	 = "From: ". $name_s . " <" . $email_s . ">\r\n";
											$headers   		.= 'Content-Type: text/html; charset="iso-8859-1"'."\r\n";
											$headers  		.= 'Content-Transfer-Encoding: 8bit'."\r\n";
											mail($mailto, $sujet_mail, $message_mail, $headers);											
											}
										}
										
										//echo '<br>2>>>'.str_replace(',','.',$qry_select_graphe_points[0]['res']).' '.str_replace(',','.',$qry_select_graphe_points[1]['res']).' '.str_replace(',','.',$qry_select_graphe_points[2]['res']).' '.str_replace(',','.',$qry_select_graphe_points[3]['res']);
														
										$plot2 = new LinePlot($values2);
										
										//var_dump($values2);
										
										if($qry_select_graphe[1]['style']==0){
											//0 Pointillé
											$plot2->setStyle(LINE_DASHED);						
										}else{
											$plot2->setStyle(LINE_SOLID);
										}
									
										$plot2->mark->setType(MARK_CIRCLE);
										$plot2->mark->setFill($col2);
										$plot2->mark->setSize(6);
										
										//$plot2->setYAxis(0,29);
										$plot2->yAxis->setLabelPrecision(1);
										$plot2->grid->hide();
										$plot2->yAxis->hide();
										$plot2->xAxis->hide();
										$plot2->xAxis->hideTicks();
										$plot2->xAxis->setLabelInterval(1);
										
										$plot2->setColor($col2);

										// On ne change pas l'espace du haut et du bas de la courbe.
										   $plot2->setSpace(
											  -10, /* Gauche */
											  -5, /* Droite */
											  0, /* Haut */
											  4 /* Bas */
										   );

										$group->add($plot2);
									}
									
								}else{
									/* Si le graphe est mutli utilisateur */
									###############################
									###############################
									
									$sql_select_list_cand = "SELECT CAND_A_OPE.CAND_ID, CAND_NOM, CAND_PRENOM FROM CAND_A_OPE, CANDIDAT WHERE CANDIDAT.CAND_ID=CAND_A_OPE.CAND_ID AND OPE_ID=".intval($var_ope_id)."";
									$qry_select_list_cand = $db->query($sql_select_list_cand);
									
									
									// Taille du graphe par défaut
									$width_grille	= 635;
									$height_grille	= 622;
									
									$sql_images = "SELECT TITRE, TOP_GRAPHE, MID_GRAPHE, BOTTOM_GRAPHE, AFFICHE_NOM,MIN_HAUTEUR,MAX_HAUTEUR FROM GRAPHE_A_TEXTE WHERE LANG_ID=".$qry_info_candidat[0]['cand_lang_id']." AND GRAPHE_ID=".intval($qry_select_graphe[0]['graphe_id'])."";
									//echo $sql_images;
									$qry_images = $db->query($sql_images);
									
									###############################
									###############################
										// Définition de la taille de la grille si la grille est enregistrée sur le serveur
										if ($qry_images[0]['mid_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['mid_graphe'])){
											$grille_exists 	= true;
											$grille_link	= 'http://www.extranet.lesensdelhumain.com/graphes/'.$qry_images[0]['mid_graphe'];
											list($width_grille, $height_grille) = getimagesize($grille_link);
										}else{
											$grille_exists = false;
											$grille_link	= 'http://www.extranet.lesensdelhumain.com/graphes/mid_graphes0.jpg';
										}
										
										// Définition de la taille du top si il est enregistré sur le serveur
										if ($qry_images[0]['top_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['top_graphe'])){
											$top_exists = true;
											$top_link   = 'http://www.extranet.lesensdelhumain.com/graphes/'.$qry_images[0]['top_graphe'];
											list($width_top, $height_top) = getimagesize($top_link);
										}else{
											$top_exists = false;
										}
										// Définition de la taille du bottom si il est enregistré sur le serveur
										if ($qry_images[0]['bottom_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['bottom_graphe'])){
											$bottom_exists = true;
											$bottom_link   = 'http://www.extranet.lesensdelhumain.com/graphes/'.$qry_images[0]['bottom_graphe'];
											list($width_bottom, $height_bottom) = getimagesize($bottom_link);
										}else{
											$bottom_exists = false;
										}
									
									
									###############################
									###############################
									
									// Ici, le graphique mesurera X x Y pixels.
									$graph = new Graph($width_grille, $height_grille);
								   
									// L'anti-aliasing permet d'afficher des courbes plus naturelles,
									// mais cette option consomme beaucoup de ressources sur le serveur.
									$graph->setAntiAliasing(TRUE);
									
									// Masquer la bordure
									$graph->border->hide();

									$col = new Color(210, 200, 34,100);
									$graph->setFormat(2); // format PNG pour gerer la transparence
									$graph->setBackgroundColor($col);
									
									$group = new PlotGroup();
									
									$group->setPadding($width_grille/8, $width_grille/8, 0, 0);
									$group->setBackgroundColor($col);
									//$group->setYMin(0);
									//$group->setYMax(29);
									$group->setYMin($qry_images[0]['min_hauteur']);
									$group->setYMax($qry_images[0]['max_hauteur']);
									
									//$group->yAxis->setLabelPrecision(1);
									$group->grid->hide();
									$group->axis->bottom->hide();
									$group->axis->left->hide();
										
									$tab_color = array(
									0=>'19,133,118',
									1=>'234,21,122',
									3=>'255,153,0',
									3=>'153,0,153',
									4=>'115,138,200',
									5=>'127,209,59',
									6=>'0,173,220',
									7=>'255,51,0',
									8=>'255,51,204',
									9=>'127,127,127',
									10=>'51,51,153',
									11=>'128,0,0');
									


									
									$numcand=0;
									foreach($qry_select_list_cand as $list_cand){
									

										$setting_cand[$numcand]['cand_nom'] 	= $list_cand['cand_nom'];
										$setting_cand[$numcand]['cand_prenom'] 	= $list_cand['cand_prenom'];
										$setting_cand[$numcand]['cand_color'] 	= $tab_color[$numcand];
										//echo $setting_cand[$numcand]['cand_color'].'<br>';
										$stab_color = explode(',',$tab_color[$numcand]);
										$scol1	= $stab_color[0];
										$scol2	= $stab_color[1];
										$scol3	= $stab_color[2];
										$col1 = new Color($scol1,$scol2,$scol3);
										
										$numcand++;
										
										// Les valeurs à afficher sur la courbe
										$sql_select_graphe_points = "SELECT TO_CHAR(COR_VAR_POINTS) RES FROM GRAPHE_PN WHERE CAND_ID=".txt_db(intval($list_cand['cand_id']))." AND OPE_ID=".txt_db(intval($var_ope_id))." AND TYPE=".$qry_select_graphe[0]['type_profil_code_id']." AND QUEST_ID=".$qry_select_graphe[0]['quest_id']." ORDER BY ID";
										//echo $sql_select_graphe_points;
										$qry_select_graphe_points = $db->query($sql_select_graphe_points);
										
										if (is_array($qry_select_graphe_points)) {
											$values = array(str_replace(',','.',$qry_select_graphe_points[0]['res']),str_replace(',','.',$qry_select_graphe_points[1]['res']),str_replace(',','.',$qry_select_graphe_points[2]['res']),str_replace(',','.',$qry_select_graphe_points[3]['res']));							
										}
										else{
											$values = array(0,0,0,0);
										if ($Entre_Erreur_Mail == 0){		
											$Entre_Erreur_Mail = 1;
											$mailto = 'fdegremont@vakom.fr, david.naze@deliberata.com, gilles.bensimon@risc-group.com';
											$sujet_mail = 'Pb génération PDF sur '.$candid_equipe.' '.txt_db(intval($var_ope_id)).' '.$qry_select_graphe[0]['type_profil_code_id'].' '.$qry_select_graphe[0]['quest_id'].'(5)';
											$message_mail = 'Pb génération PDF sur '.$candid_equipe.' '.txt_db(intval($var_ope_id)).' '.$qry_select_graphe[0]['type_profil_code_id'].' '.$qry_select_graphe[0]['quest_id'].'(5)';
											$name_s    		 = 'Dev';
											$email_s 		 = 'Diffusion_Developpement-ITS@risc-group.biz';
											$headers    	 = "From: ". $name_s . " <" . $email_s . ">\r\n";
											$headers   		.= 'Content-Type: text/html; charset="iso-8859-1"'."\r\n";
											$headers  		.= 'Content-Transfer-Encoding: 8bit'."\r\n";
											mail($mailto, $sujet_mail, $message_mail, $headers);											
											}
										}

										// On créé la courbe								
										$plot = new LinePlot($values);
										if($qry_select_graphe[0]['style']==0){
											//0 Pointillé
											$plot->setStyle(LINE_DASHED);
										}else{
											$plot->setStyle(LINE_SOLID);
										}

										//$plot->setStyle(LINE_SOLID);								
										
										$plot->mark->setType(MARK_CIRCLE);
										$plot->mark->setFill($col1);
										$plot->mark->setSize(6);

										//$plot->setYAxis(0,29);
										$plot->yAxis->setLabelPrecision(1);
										$plot->grid->hide();
										$plot->yAxis->hide();
										$plot->xAxis->hide();
										$plot->xAxis->hideTicks();
										$plot->xAxis->setLabelInterval(1);
										
										$plot->setColor($col1);

										// On ne change pas l'espace du haut et du bas de la courbe.
										   $plot->setSpace(
											  -10, // Gauche
											  -5, // Droite
											  0, // Haut 
											  4 // Bas
										   );
										   
										$group->add($plot);	
									}
								}
								
								break;
								// Fin du case COURBE
							}
							
							$graph->add($group);
							
							$graph->setFormat(Image::PNG); 
							$sql_select_seq_graphe = "SELECT seq_id.nextval seq_graphe_id from dual";
							$qry_select_seq_graphe = $db->query($sql_select_seq_graphe);

							$graph->draw("../graphes/graphe".$qry_select_seq_graphe[0]['seq_graphe_id'].".png");
							
							$picture = imagecreatefrompng("../graphes/graphe".$qry_select_seq_graphe[0]['seq_graphe_id'].".png");
							$color = imagecolorexact ($picture,255,255,255);
							imagecolortransparent($picture, $color);
							imagepng($picture, "../graphes/graphe".$candid_equipe."_".$var_ope_id."_".$infos_document['doc_id']."_".$zone_id.".png"); 
							
							@unlink("../graphes/graphe".$qry_select_seq_graphe[0]['seq_graphe_id'].".png");
							
							$contenu[$zone_id] = '<div style="display: inline; margin-bottom: 50px;">';
							
								if(intval($qry_select_detail_graphe[0]['doc_graphe_multi'])<1){
									/* Si on doit afficher le nom du candidat */
									$title_graphe = $qry_images[0]['titre'];
									$title_graphe = str_replace ( "[NOM]" , $qry_info_candidat[0]['cand_nom'] , $title_graphe );
									$title_graphe = str_replace ( "[PRENOM]" , $qry_info_candidat[0]['cand_prenom'] , $title_graphe );
									$title_graphe = str_replace ( "[SOCIETE]" , $qry_info_candidat[0]['cli_nom'] , $title_graphe );
									//$title_graphe = $qry_images[0]['titre'];
									//$ident_candidat = '&nbsp;'.$qry_info_candidat[0]['cand_prenom'].' '.$qry_info_candidat[0]['cand_nom'];
								}
								else{
									/* Si on doit afficher le nom du candidat */
										$title_graphe = $qry_images[0]['titre'];
										$title_graphe = str_replace ( "[NOM]" , "" , $title_graphe );
										$title_graphe = str_replace ( "[PRENOM]" , "" , $title_graphe );
										$title_graphe = str_replace ( "[SOCIETE]" , "" , $title_graphe );
								}
								if ($title_graphe!=''){
									$contenu[$zone_id] .= '<div style="width: '.$width_top.'px;">'.$title_graphe.'</div>';
								}
								if($top_exists){
									$contenu[$zone_id] .= '<div style="overflow: hidden;"><img style="width: '.$width_top.'px;" src="'.$top_link.'" alt="" /></div>';
								}
								/*
								echo 'wg = '.$width_grille.'<br>';
								echo 'hg = '.$height_grille.'<br>';
								echo 'gl = '.$grille_link.'<br>';
								*/							
								$contenu[$zone_id] .='<div style="overflow: hidden; width: '.$width_grille.'px; height: '.$height_grille.'px; display: block; text-align: center; background-image: url('.$grille_link.');">
								<img style="width: '.$width_grille.'px;" src="http://www.extranet.lesensdelhumain.com/graphes/graphe'.$candid_equipe."_".$var_ope_id."_".$infos_document['doc_id'].'_'.$zone_id.'.png" alt="" />
								</div>';
								
								if($bottom_exists){
									$contenu[$zone_id] .='<div style="overflow: hidden; display: block;">
									<img style="width: '.$width_bottom.'px;" src="'.$bottom_link.'" alt="" />
									</div>';
								}
								/* Si on doit afficher le nom du candidat */
								if(intval($qry_select_detail_graphe[0]['doc_graphe_multi'])==1){
									/* Si c'est un graphe multi */
									if($qry_images[0]['affiche_nom']==1){
										unset($ident_candidat2);
										$numcand=0;
										foreach($qry_select_list_cand as $list_cand){
											$ident_candidat2 .= '<div style="float: left;"><table style="display: inline"><tr><td height="5" width="5" style="line-height: 5px;" bgcolor="#'.rgb2hex(explode(',',$setting_cand[$numcand]['cand_color'])).'">&nbsp;</td><td>'.$setting_cand[$numcand]['cand_prenom'].' '.$setting_cand[$numcand]['cand_nom'].'&nbsp;&nbsp;</td></tr></table></div>';
											$numcand++;
										}
									}
									$contenu[$zone_id] .= '<div style="width: '.$width_top.'px;">'.$ident_candidat2.'<div style="clear: both;"></div></div>';
								}
								
							$contenu[$zone_id] .= '</div>';
							break;
						case 2: // Si c'est un texte fixe
							$align[$zone_id]='left';
							
							$contenu[$zone_id] = $infos_zones[0]['doc_texte_fixe'].$infos_zones[0]['doc_texte_fixe_2'].$infos_zones[0]['doc_texte_fixe_3'].$infos_zones[0]['doc_texte_fixe_4'].$infos_zones[0]['doc_texte_fixe_5'].$infos_zones[0]['doc_texte_fixe_6'].$infos_zones[0]['doc_texte_fixe_7'].$infos_zones[0]['doc_texte_fixe_8'];
							$contenu[$zone_id] = str_replace ( "[NOM]" , $qry_info_candidat[0]['cand_nom'] , $contenu[$zone_id] );
							$contenu[$zone_id] = str_replace ( "[PRENOM]" , $qry_info_candidat[0]['cand_prenom'] , $contenu[$zone_id] );
							$contenu[$zone_id] = str_replace ( "[SOCIETE]" , $qry_info_candidat[0]['cli_nom'] , $contenu[$zone_id] );
							
						break;
						case 4: // Si c'est une image fixe
							$align[$zone_id]='center';
							$contenu[$zone_id] = '<img src="http://www.extranet.lesensdelhumain.com/images/docs/'.$infos_zones[0]['doc_image_fixe'].'" alt="">';
						break;
						case 3: // Si c'est un texte dyn
							$align[$zone_id]='left';
						
							$sql_select_txt_img = "SELECT * FROM TEXTE_IMAGE WHERE TXT_IMG_ID=".txt_db(intval($infos_zones[0]['doc_txt_img_id']))."";
							//echo $sql_select_txt_img.'<br>';
							$qry_select_txt_img = $db->query($sql_select_txt_img);
							
							$tab_lettres = array(
								1=>'E',
								2=>'C',
								3=>'P',
								4=>'A');
							$tab_profils = array(
								29=>'PI',
								30=>'PN',
								31=>'PNPI',
								105=>'PNPIF',
								32=>'PR');
							unset($combi);
							foreach($tab_lettres as $key => $value){
								/* boucle pour chaque lettre */
								switch($qry_select_txt_img[0]['txt_img_precision_code_id']){
									case 33:
										/* Par points */
										$vars = 'COR_VAR_POINTS';
									break;
									case 34:
										/* Par segment */
										$vars = 'COR_REGLE_SEGMENT';
									break;
									case 35:
										/* Par bloc */
										$vars = 'COR_REGLE_BLOC';
									break;
								}
								
								$sql_combi = "SELECT ".$vars." VAR_".$value." FROM CAND_".$tab_profils[$qry_select_txt_img[0]['txt_img_type_profil_code_id']]." WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." AND REPONSE_".$tab_profils[$qry_select_txt_img[0]['txt_img_type_profil_code_id']]." = '".$value."'";
								//echo $sql_combi.'<br>';
								$qry_combi = $db->query($sql_combi);
								$combi[$key] = $qry_combi[0]['var_'.strtolower($value)];
								$valeur_combi .= $combi[$key];
								
								//echo $sql.'<br>';							
							}
							
							$sql_lib_textes = "select * from cand_comb
							where txt_img_id=".txt_db(intval($infos_zones[0]['doc_txt_img_id']))." 
							and COMBI_E_INF<=".$combi[1]." 
							and COMBI_C_INF<=".$combi[2]." 
							and COMBI_P_INF<=".$combi[3]." 
							and COMBI_A_INF<=".$combi[4]." 
							and COMBI_E_SUP>=".$combi[1]." 
							and COMBI_C_SUP>=".$combi[2]." 
							and COMBI_P_SUP>=".$combi[3]." 
							and COMBI_A_SUP>=".$combi[4]."";
							$sql_lib_textes = "select * from  texte_image_a_combi
							where combi_txt_img_id=".txt_db(intval($infos_zones[0]['doc_txt_img_id']))." 
							and COMBI_E_INF<=".$combi[1]." 
							and COMBI_C_INF<=".$combi[2]." 
							and COMBI_P_INF<=".$combi[3]." 
							and COMBI_A_INF<=".$combi[4]." 
							and COMBI_E_SUP>=".$combi[1]." 
							and COMBI_C_SUP>=".$combi[2]." 
							and COMBI_P_SUP>=".$combi[3]." 
							and COMBI_A_SUP>=".$combi[4]."";
							//echo $sql_lib_textes.'<br>';
							$sql_lib_textes = $db->query($sql_lib_textes);
							/* SI HOMME */
							if($qry_info_candidat[0]['cand_sexe']!='F'){
								$sql_export_h = "select txt_homme,txt_homme_2,txt_homme_3,txt_homme_4,txt_homme_5 from texte_a_langue where TXT_CODE_ID=".$sql_lib_textes[0]['txt_code_id'];
								$sql_export_h = "select txt_homme,txt_homme_2,txt_homme_3,txt_homme_4,txt_homme_5 from texte_a_langue,code_regroupement where texte_a_langue.TXT_CODE_ID=code_regroupement.TXT_CODE_ID and code_regroupement.CODE_REGROUP_ID=".$sql_lib_textes[0]['combi_code_regroup_id']." and txt_img_id=".txt_db(intval($infos_zones[0]['doc_txt_img_id']))." ";

								//echo $sql_export_h;
								$qry_export_h = $db->query($sql_export_h);
								$contenu[$zone_id] = $qry_export_h[0]['txt_homme'].$qry_export_h[0]['txt_homme_2'].$qry_export_h[0]['txt_homme_3'].$qry_export_h[0]['txt_homme_4'].$qry_export_h[0]['txt_homme_5'];
							}
							/* SI FEMMME */
							else{
								$sql_export_f = "select txt_femme,txt_femme_2,txt_femme_3,txt_femme_4,txt_femme_5 from texte_a_langue where TXT_CODE_ID=".$sql_lib_textes[0]['txt_code_id'];
								$sql_export_f = "select txt_femme,txt_femme_2,txt_femme_3,txt_femme_4,txt_femme_5 from texte_a_langue,code_regroupement where texte_a_langue.TXT_CODE_ID=code_regroupement.TXT_CODE_ID and code_regroupement.CODE_REGROUP_ID=".$sql_lib_textes[0]['combi_code_regroup_id']." and txt_img_id=".txt_db(intval($infos_zones[0]['doc_txt_img_id']))." ";
								//echo $sql_export_f;
								$qry_export_f = $db->query($sql_export_f);
								$contenu[$zone_id] = $qry_export_f[0]['txt_femme'].$qry_export_f[0]['txt_femme_2'].$qry_export_f[0]['txt_femme_3'].$qry_export_f[0]['txt_femme_4'].$qry_export_f[0]['txt_femme_5'];
							}
							$contenu[$zone_id] = str_replace ( "[NOM]" , $qry_info_candidat[0]['cand_nom'] , $contenu[$zone_id] );
							$contenu[$zone_id] = str_replace ( "[PRENOM]" , $qry_info_candidat[0]['cand_prenom'] , $contenu[$zone_id] );
							$contenu[$zone_id] = str_replace ( "[SOCIETE]" , $qry_info_candidat[0]['cli_nom'] , $contenu[$zone_id] );
						break;
						case 5:	// Si c'est une image dyn
							$align[$zone_id]='center';
							$sql_select_txt_img = "SELECT * FROM TEXTE_IMAGE WHERE TXT_IMG_ID IN ('".txt_db(intval($infos_zones[0]['doc_img1_txt_img_id']))."','".txt_db(intval($infos_zones[0]['doc_img2_txt_img_id']))."')";
							//echo $sql_select_txt_img.'<br>';
							$qry_select_txt_img = $db->query($sql_select_txt_img);
							$tab_lettres = array(
								1=>'E',
								2=>'C',
								3=>'P',
								4=>'A');
							$tab_profils = array(
								29=>'PI',
								30=>'PN',
								32=>'PR');
							unset($combi);
							foreach($tab_lettres as $key => $value){
								/* boucle pour chaque lettre */
								switch($qry_select_txt_img[0]['txt_img_precision_code_id']){
									case 33:
										/* Par points */
										$vars = 'COR_VAR_POINTS';
									break;
									case 34:
										/* Par segment */
										$vars = 'COR_REGLE_SEGMENT';
									break;
									case 35:
										/* Par bloc */
										$vars = 'COR_REGLE_BLOC';
									break;
								}
								
								$sql_combi = "SELECT ".$vars." VAR_".$value." FROM CAND_".$tab_profils[$qry_select_txt_img[0]['txt_img_type_profil_code_id']]." WHERE CAND_ID=".$candid_equipe." AND OPE_ID=".txt_db(intval($var_ope_id))." AND REPONSE_".$tab_profils[$qry_select_txt_img[0]['txt_img_type_profil_code_id']]." = '".$value."'";
								//echo $sql_combi.'<br>';
								$qry_combi = $db->query($sql_combi);
								$combi[$key] = $qry_combi[0]['var_'.strtolower($value)];
								$valeur_combi .= $combi[$key];
								
								//echo $sql.'<br>';							
							}
							$sql_lib_textes = "select * from cand_comb,texte_image
							where cand_comb.txt_img_id=texte_image.txt_img_id
							and cand_comb.txt_img_quest_id=texte_image.txt_img_quest_id
							and cand_comb.TXT_IMG_TYPE_PROFIL_CODE_ID=texte_image.TXT_IMG_TYPE_PROFIL_CODE_ID
							and cand_comb.TXT_IMG_PRECISION_CODE_ID=texte_image.TXT_IMG_PRECISION_CODE_ID
							and texte_image.txt_img_id IN ('".txt_db(intval($infos_zones[0]['doc_img1_txt_img_id']))."','".txt_db(intval($infos_zones[0]['doc_img2_txt_img_id']))."')
							and COMBI_E_INF<=".$combi[1]." 
							and COMBI_C_INF<=".$combi[2]." 
							and COMBI_P_INF<=".$combi[3]." 
							and COMBI_A_INF<=".$combi[4]." 
							and COMBI_E_SUP>=".$combi[1]." 
							and COMBI_C_SUP>=".$combi[2]." 
							and COMBI_P_SUP>=".$combi[3]." 
							and COMBI_A_SUP>=".$combi[4]."";
							//echo $sql_lib_textes;
							$sql_lib_textes = $db->query($sql_lib_textes);
							if(is_array($sql_lib_textes[0])){
								/* SI HOMME */
								if($qry_info_candidat[0]['cand_sexe']!='F'){
									//$contenu[$zone_id] = '<table width="600" border="0" cellspacing="0" cellpadding="0" height="848" background="http://www.extranet.lesensdelhumain.com/admin/images_txtimg/108b57opr-pn-31.png"><tr>';
//									$contenu[$zone_id] = '<table width="600" border="0" cellspacing="0" cellpadding="0" height="848" style="background-image: url(http://www.extranet.lesensdelhumain.com/admin/images_txtimg/'.$sql_lib_textes[0]['img_homme'].'); background-repeat: no-repeat; background-position: top center;"><tr>';
									$contenu[$zone_id] = '<img src="http://www.extranet.lesensdelhumain.com/admin/images_txtimg/'.$sql_lib_textes[0]['img_homme'].'" alt="">';
									
	//								<img style="position:absolute; left:233; top:5" src="http://www.extranet.lesensdelhumain.com/admin/images_txtimg/'.$sql_lib_textes[0]['img_homme'].'" alt="" />';
								}
								/* SI FEMMME */
								else{
									$contenu[$zone_id] = '<img src="http://www.extranet.lesensdelhumain.com/admin/images_txtimg/'.$sql_lib_textes[0]['img_femme'].'" alt="">';
//									$contenu[$zone_id] = '<table width="600" border="0" cellspacing="0" cellpadding="0" height="848" style="background-image: url(http://www.extranet.lesensdelhumain.com/admin/images_txtimg/'.$sql_lib_textes[0]['img_femme'].'); background-repeat: no-repeat; background-position: top center;"><tr>';
	//								$contenu[$zone_id] = '<img style="position:absolute; left:233; top:5" src="http://www.extranet.lesensdelhumain.com/admin/images_txtimg/'.$sql_lib_textes[0]['img_femme'].'" alt="" />';						
								}
								// 2 COURBES ??
								if(is_array($sql_lib_textes[1])){
									/* SI HOMME */
	//								echo $sql_lib_textes[1]['img_femme'];
									if($qry_info_candidat[0]['cand_sexe']!='F'){
										//$contenu[$zone_id] = $contenu[$zone_id].'<td><img src="http://www.extranet.lesensdelhumain.com/admin/images_txtimg/55e628opr-iv-423421.png"></td>';
										$contenu[$zone_id] = $contenu[$zone_id].'<td><img src="http://www.extranet.lesensdelhumain.com/admin/images_txtimg/'.$sql_lib_textes[1]['img_homme'].'"></td>';
									}
									/* SI FEMMME */
									else{
										$contenu[$zone_id] = $contenu[$zone_id].'<td><img src="http://www.extranet.lesensdelhumain.com/admin/images_txtimg/'.$sql_lib_textes[1]['img_femme'].'"></td>';
									}
								}
								$contenu[$zone_id] = $contenu[$zone_id].'</tr>';
							}
							$contenu[$zone_id] = str_replace ( "[NOM]" , $qry_info_candidat[0]['cand_nom'] , $contenu[$zone_id] );
							$contenu[$zone_id] = str_replace ( "[PRENOM]" , $qry_info_candidat[0]['cand_prenom'] , $contenu[$zone_id] );
							$contenu[$zone_id] = str_replace ( "[SOCIETE]" , $qry_info_candidat[0]['cli_nom'] , $contenu[$zone_id] );
						break;
						case 6:// Si c'est un cr
						
							$sql_select_detail_cr = "SELECT DOC_TYPE_CR FROM DOC_A_INFO WHERE DOC_NOM_CR IS NOT NULL AND DOC_ID=".$infos_document['doc_id']." AND NUM_ZONE=".$zone_id."";
							$qry_select_detail_cr = $db->query($sql_select_detail_cr);
							//echo $sql_select_detail_cr.'<br>';
							$align[$zone_id]='left';
							/* Sélection du CR */
							$sql_sel_cr = "SELECT * FROM CAND_A_CR WHERE OPE_ID=".intval($var_ope_id)." AND CAND_ID=".$candid_equipe." AND DOC_ID=".$infos_document['doc_id']." AND NUM_ZONE=".$zone_id." ORDER BY NUM_LIGNE";
							$qry_sel_cr = $db->query($sql_sel_cr);
							//echo $sql_sel_cr.'<br>';
							if (is_array($qry_sel_cr[0])) {
							//print_r($qry_sel_cr[0]);
							//echo '<br>';
								$contenu[$zone_id] = '<table width="100%" cellpadding="10" cellspacing="0">';
								
								//echo $qry_select_detail_cr[0]['doc_type_cr'];
								
								switch($qry_select_detail_cr[0]['doc_type_cr']){
									case 47: // Themes
										foreach($qry_sel_cr as $list_cr){
											$contenu[$zone_id] .= '<tr><td align="left" bgcolor="#ebebeb" style="padding: 20px; border-right: 1px #ACACAC dashed;border-bottom: 1px #ACACAC dashed; color: #f78414;font-family: \'Lucida Sans\'; font-size: 12pt;" width="140">'.nl2br($list_cr['theme']).'</td><td align="left" style="padding: 20px; font-family: \'Trebuchet MS\'; font-size: 12pt; border-bottom: 1px #ACACAC dashed;"><i>'.nl2br($list_cr['txt_cr']).'</i></td></tr>';
										}
										break;
									case 48: // Themes et images
										foreach($qry_sel_cr as $list_cr){
											if($pass_cr<1){
												$contenu[$zone_id] .= '<tr><td align="center" bgcolor="#ebebeb" width="1" style="padding: 20px; border-right: 1px #ACACAC dashed;border-bottom: 1px #ACACAC dashed;"><img src="http://www.extranet.lesensdelhumain.com/images/main.png" alt=""></td><td align="center" bgcolor="#ebebeb" style="padding: 20px; color: #983088; border-right: 1px #ACACAC dashed; border-bottom: 1px #ACACAC dashed; font-family: \'Lucida Sans\'; font-size: 14pt;">DOMAINES DE VIGILANCES<br>CHOISIS</td><td bgcolor="#ebebeb" align="center" style="padding: 20px; border-bottom: 1px #ACACAC dashed; color: #983088; font-family: \'Lucida Sans\'; font-size: 14pt;">LES LEVIERS<br>POUR AGIR</td></tr>';
											}
											$contenu[$zone_id] .= '<tr><td align="center" width="1" style="padding: 20px;border-right: 1px #ACACAC dashed;border-bottom: 1px #ACACAC dashed;"><img src="http://www.extranet.lesensdelhumain.com/images/main.png" alt=""></td><td align="left" style="padding: 20px;color: #a6a6a6; border-right: 1px #ACACAC dashed;border-bottom: 1px #ACACAC dashed; font-family: \'Lucida Sans\'; font-size: 12pt;">'.nl2br($list_cr['theme']).'</td><td align="left" style="padding: 20px; border-bottom: 1px #ACACAC dashed; font-family: \'Trebuchet MS\'; font-size: 12pt;"><i>'.nl2br($list_cr['txt_cr']).'</i></td></tr>';
											$pass_cr++;
										}
										break;
									case 49: // Images
										foreach($qry_sel_cr as $list_cr){
											$contenu[$zone_id] .= '<tr><td align="center" width="1" style="padding: 20px;border-right: 1px #ACACAC dashed;border-bottom: 1px #ACACAC dashed;"><img src="http://www.extranet.lesensdelhumain.com/images/main.png" alt=""></td><td align="left" style="padding: 20px;font-family: \'Trebuchet MS\'; font-size: 12pt; border-bottom: 1px #ACACAC dashed;"><i>'.nl2br($list_cr['txt_cr']).'</i></td></tr>';
										}
										break;
									case 50: //Libre
									default:
										$contenu[$zone_id] .= '<tr><td align="left" style="padding: 20px;font-family: Trebuchet MS; border-bottom: 1px #ACACAC dashed; font-size: 12pt;"><i>'.$qry_sel_cr[0]['txt_cr'].'</i></td></tr>';
										break;
								}
								$contenu[$zone_id] .= '</table><br>';
								$contenu[$zone_id] = str_replace ( "[NOM]" , $qry_info_candidat[0]['cand_nom'] , $contenu[$zone_id] );
								$contenu[$zone_id] = str_replace ( "[PRENOM]" , $qry_info_candidat[0]['cand_prenom'] , $contenu[$zone_id] );
								$contenu[$zone_id] = str_replace ( "[SOCIETE]" , $qry_info_candidat[0]['cli_nom'] , $contenu[$zone_id] );
								//echo $contenu[$zone_id].'<br>';
							}
							else
							{
								$Entre_Dans_Pdf = 0;
							}
						break;
					}
				}
				if ($Entre_Dans_Pdf == 1){ // pour le cr
					if ($infos_zones[0]['sommaire'] == 1){			
		//				if ($infos_zones[0]['equipe'] == 1){
							$sommaire_tmp = str_replace ( "[NOM]" , $qry_info_candidat[0]['cand_nom'] , $infos_zones[0]['titre_sommaire'] );
							$sommaire_tmp = str_replace ( "[PRENOM]" , $qry_info_candidat[0]['cand_prenom'] , $sommaire_tmp );
							$sommaire_tmp = str_replace ( "[SOCIETE]" , $qry_info_candidat[0]['cli_nom'] , $sommaire_tmp );
						
							$Nom_Doc_Tmp = '<h1 style="color:#FFFFFF;font-size:1px">'.$sommaire_tmp.'</h1>';
						//}
						//else
						//{
						//	$Nom_Doc_Tmp = '<h1 style="color:#FFFFFF;font-size:1px">'.$infos_zones[0]['titre_sommaire'].'</h1>';
						//}
					}
					else
					{
						$Nom_Doc_Tmp = '';
					}
					switch($infos_document['type_gabarit']){
						case 1:
							$mid_page_html = '
							<center><table width="678" style="page-break-before:always" align="center">
							<tr><td height="700" valign="top" width="674" align="center">
							<table border="0" width="652" align="center" cellpadding="5" cellspacing="0" height="700">'.$Nom_Doc_Tmp.'
								<tr><td width="652" valign="top" align="'.$align[1].'">'.$contenu[1].'</td></tr>
							</table>';
						break;
						case 2:
							$mid_page_html = '
							<center><table width="678" style="page-break-before:always" align="center">
							<tr><td height="700" valign="top" width="674" align="center">
							<table border="0" width="652" align="center" cellpadding="5" cellspacing="0" height="700">'.$Nom_Doc_Tmp.'
								<tr><td width="652" height="1" valign="top" align="'.$align[1].'">'.$contenu[1].'</td></tr>
								<tr><td width="652" valign="top" align="'.$align[2].'">'.$contenu[2].'</td></tr>
							</table>';
						break;
						case 3:
							$mid_page_html = '
							<center><table width="678" style="page-break-before:always" align="center">
							<tr><td height="700" valign="top" width="674" align="center">
							<table width="652" align="center" cellpadding="5" cellspacing="0" height="700">'.$Nom_Doc_Tmp.'
								<tr><td width="326" height="1" valign="top" align="left">'.$contenu[1].'</td>
									<td width="326" valign="top" align="right">'.$contenu[2].'</td></tr>
								<tr><td colspan="2" valign="top" width="652" align="'.$align[3].'">'.$contenu[3].'</td></tr>
							</table>';
						break;
						case 4:
							$mid_page_html = '
							<center><table width="678" style="page-break-before:always" align="center">
							<tr><td height="700" valign="top" width="674" align="center">
							<table border="0" width="652" align="center" cellpadding="5" cellspacing="0" height="700">'.$Nom_Doc_Tmp.'
								<tr><td width="652" height="1" valign="top" align="'.$align[1].'">'.$contenu[1].'</td></tr>
								<tr><td width="652" height="1" valign="top" align="'.$align[2].'">'.$contenu[2].'</td></tr>
								<tr><td width="652" valign="top" align="'.$align[3].'">'.$contenu[3].'</td></tr>
							</table>';
						break;
						case 5:
							$mid_page_html = '
							<center><table width="678" style="page-break-before:always" align="center">
							<tr><td height="700" valign="top" width="674" align="center">
							<table border="0" width="652" align="center" cellpadding="5" cellspacing="0" height="700">'.$Nom_Doc_Tmp.'
								<tr><td height="1" valign="top" align="'.$align[1].'">'.$contenu[1].'</td>
									<td rowspan="2" width="220" valign="top" align="'.$align[3].'">'.$contenu[3].'</td>
								</tr>
								<tr>
									<td height="1" valign="top" align="'.$align[2].'">'.$contenu[2].' </td>
								</tr>
								<tr><td width="652 valign="top" colspan="2" align="'.$align[4].'">'.$contenu[4].'</td></tr>
							</table>';
						break;
					}
					$mid_page_html = str_replace('src="/admin/', 'src="http://www.extranet.lesensdelhumain.com/admin/', $mid_page_html);
					
					$mid_page_html .= '</td></tr>';
					if ($infos_zones[0]['equipe'] == 1){
						$Entre_Equipe = 1;
						$Entre_Html = 1;
						$Zone_Equipe = $infos_zones[0]['tri'];
					}
					else
					{
						if ($Entre_Equipe == 1){
							if ($candid_dernier_equipe == $infos_cand_equipe['cand_id']){
							//echo "GBE".$infos_zones[0]['tri'].';'.$Zone_Equipe;
								if ($infos_zones[0]['tri'] > $Zone_Equipe && $Zone_Equipe>0){
									$Entre_Html = 1;
								}
								else
								{
									$Entre_Html = 0;
								}
							}
							else
							{
								$Entre_Html = 0;
							}
						}
						else
						{
							$Entre_Html = 1;
						}
					}
					if ($Entre_Html == 1){
						//if ($qry_infos_prod_doss[0]['sommaire'] == 1){
							$num_page += 1;
						//}
						// On ouvre un nouveau fichier
						$file 			= $fname.$num_page.".html";
						$listfile 		.= $fname.$num_page.".html,";
						$fp = fopen($chemin . $file ,"w+");
						fputs($fp,$deb_page_html.$mid_page_html.$fin_page_html);
						fclose($fp);					
						$str_filetoconvert .= ' '.$chemin . $file;
						
						$command0 	= '/usr/local/bin/wkhtmltopdf -O Portrait -B 25 -s A4 "'.$chemin . $file.'" --footer-html "'.$chemin.intval($var_cand_id).intval($var_ope_id).intval($var_doss_id).'_footer.html'.'" ';
						$command0   .= ' '. $chemin . $fname.$num_page . '.pdf';
						//echo $commandline;
						passthru($command0);
						@unlink($fname.$num_page.".html");
						
						$nb_final += Cpte_Page_PDF($chemin . $fname.$num_page . '.pdf');
						
						if ($infos_zones[0]['sommaire'] == 1 && $infos_zones[0]['titre_sommaire']!=''){
							$sommaire_tmp = str_replace ( "[NOM]" , $qry_info_candidat[0]['cand_nom'] , $infos_zones[0]['titre_sommaire'] );
							$sommaire_tmp = str_replace ( "[PRENOM]" , $qry_info_candidat[0]['cand_prenom'] , $sommaire_tmp );
							$sommaire_tmp = str_replace ( "[SOCIETE]" , $qry_info_candidat[0]['cli_nom'] , $sommaire_tmp );
		//					if ($infos_zones[0]['equipe'] == 1){
		//						$content_toc .= '<tr><td class="soustitre">'.$infos_zones[0]['titre_sommaire'].' '.$qry_info_candidat[0]['cand_prenom'].' '.$qry_info_candidat[0]['cand_nom'].'</td><td width="1%" class="page">Page&nbsp;'.($nb_final+1).'</td></tr>';
		//					}
		//					else{
								$content_toc .= '<tr><td class="soustitre">'.$sommaire_tmp.'</td><td width="1%" class="page">Page&nbsp;'.($nb_final+1).'</td></tr>';
		//					}
						}					
					}
				}
			}
			else
			{
				$num_page += 1;
				$file 			= $fname.$num_page.".html";
				$listfile 		.= $fname.$num_page.".html,";
				$str_filetoconvert .= ' '.$chemin . $file;
					
				$nb_final += Cpte_Page_PDF($chemin . $fname.$num_page . '.pdf');
				$sql_recup_som = "select * from produit_a_doc where prod_id=".txt_db(intval($infos_document['prod_id']))." and dossier_id=".$var_doss_id." and doc_id=".txt_db(intval($infos_document['doc_id']))."";
				$qry_recup_som = $db->query($sql_recup_som);	
				if ($qry_recup_som[0]['sommaire'] == 1 && $qry_recup_som[0]['titre_sommaire']!=''){
					$sommaire_tmp = str_replace ( "[NOM]" , $qry_info_candidat[0]['cand_nom'] , $qry_recup_som[0]['titre_sommaire'] );
					$sommaire_tmp = str_replace ( "[PRENOM]" , $qry_info_candidat[0]['cand_prenom'] , $sommaire_tmp );
					$sommaire_tmp = str_replace ( "[SOCIETE]" , $qry_info_candidat[0]['cli_nom'] , $sommaire_tmp );
					$content_toc .= '<tr><td class="soustitre">'.$sommaire_tmp.'</td><td width="1%" class="page">Page&nbsp;'.($nb_final+1).'</td></tr>';
				}
			}
		}
	}
	$Entre_Equipe = 1;
	}
	$head_toc 		= '<html>
<head>
	<style type="text/css">
		<!--
		td.titre{text-align: left; color: #A379BB; font-size: 28pt; font-family: Lucida Sans, Tahoma, \'Trebuchet MS\'; font-weight: bold;}
		td.soustitre{text-align: left; font-size: 10pt; font-family: Trebuchet MS;}
		td.page{font-size: 12pt; font-family: Lucida Sans; text-align: left; color: #ACACAC;}
		-->
	</style>
</head>
<body>
	<table align="center" width="100%" cellpadding="0" cellspacing="20" border="0">
		<tr><td colspan="2"><span style="font-size: 38pt"><span style="font-family: Lucida Sans"><b><font color="#7F7F7F">Sommaire</font></b></span></span></td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>';
			$content_toc 	.= '</table>
</body>
</html>';
			
	$fp_toc 		= fopen($chemin . $fname. '_sommaire.html' ,"w+");
	fputs($fp_toc,$head_toc.$content_toc);
	fclose($fp_toc);
	
	
	$ftype 			= "application/pdf";
	
	if ($qry_infos_prod_doss[0]['sommaire'] == 1){
		$str_sommaire 	= $chemin.$fname.'_sommaire.html ';
	}else{
		$str_sommaire 	= ' ';
	}
	
	/* Génération du document en PDF */
	$commandline   	= $debut_command . $str_sommaire. $str_filetoconvert . ' ' . $chemin . $fname . '.pdf';	
	passthru($commandline);
	
	/* Si on a besoin de générer la page de garde */
	if ($qry_infos_prod_doss[0]['page_garde'] == 1){
	
		/* Génération de la page de garde en PDF */
		$command_page_garde = '/usr/local/bin/wkhtmltopdf -O Portrait -B 10 -s A4 '.$chemin.$fname.'1.html ' . $chemin . $fname . '_page_garde.pdf';
		passthru($command_page_garde);
		
		/* On crée le nouveau PDF en commencant par la page de garde */
		$pdf		=	new FPDI();		
		$pdf		->AddPage();
		$pagecount 	= $pdf->setSourceFile($chemin . $fname . '_page_garde.pdf');
		$tplidx 	= $pdf->importPage(1, '/MediaBox');
		$pdf		->useTemplate($tplidx, 0, 0, 210);
		
		/* On compte le nombre total de page du PDF généré */
		$nbpagetotal	= Cpte_Page_PDF($chemin . $fname . '.pdf');		
		
		/* On importe chaque page dans l'agrégat PDF */
		for ($n=1; $n<=$nbpagetotal; $n++){
			$pdf		->AddPage();
			$pagecount 	= $pdf->setSourceFile($chemin . $fname . '.pdf');
			$tplidx 	= $pdf->importPage($n, '/MediaBox');
			$pdf		->useTemplate($tplidx, 0, 0, 210);
		}
		/* On enregistre le nouveau PDF assemblé */
		$pdf		->Output($chemin . $fname . '.pdf');
	}
	
	$tableau_html = explode(',',$listfile);
	foreach($tableau_html as $fichier_a_supprimer){
		//@unlink($chemin . $fichier_a_supprimer);
	}
	//@unlink($chemin . $fname . '1.html');
	//@unlink($chemin . $fname . '_footer.html');
}
?>
