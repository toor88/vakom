<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']>0){
		
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	function dd($date) {
	   return date("d/m/Y H:i:s",$date);
	}
	
	$directory = '../produits';
	
	$tabeau_tarifs  = '<tr><td class="TX_bold">Document&nbsp;téléchargeable</td><td class="TX_bold" width="80">Type</td><td class="TX_bold" width="100">Taille</td><td class="TX_bold"> </td></tr>';
	$tabeau_tarifs .= '<tr><td colspan="4" height="1" bgcolor="#000000"> </td></tr>';
	
	
	if($_GET['file']){		
		@unlink('../produits/'.str_replace("\'","'",$_GET['file']));
		header('location:tarifs.php?idnc='.($_GET['idnc']+1));
	}
	
	$nb=0;
	if (is_dir($directory)){
		$rep = opendir($directory);
		$fileArray = array();
		while ($f = readdir($rep)){
		   if(is_file($directory.'/'.$f)) {
				$fileArray[] = substr($f,6);
				$fileLink[] = $f;
			  $nb++;
		   }
		}
		natcasesort($fileArray);
		if($nb){
			foreach($fileArray as $filePos => $fileName){
				$ko = ceil(filesize($directory.'/'.$fileLink[$filePos])/1024);
				$tabeau_tarifs .= '<tr><td class="TX"><a target="_blank" href="'.$directory.'/'.$fileLink[$filePos].'">'.$fileName.'</a></td>';
				$tabeau_tarifs .= '<td class="TX" width="80">PDF</td>';
				$tabeau_tarifs .= '<td class="TX" width="100">'.$ko.' Ko</td>';
				$tabeau_tarifs .= '<td align="left" >';
				if ($_SESSION['droit']== 9) $tabeau_tarifs .= '<img onmouseover="this.style.cursor=\'pointer\';" onclick="confirm_suppression(\''.str_replace("'","\'",$fileLink[$filePos]).'\')" noborder src="../images/icon_supp2.gif">';
				$tabeau_tarifs .=  '</td></tr>';
			}
		}
		closedir($rep); 
	}
	if($nb==0){
		$tabeau_tarifs .= '<tr><td colspan="4" class="TX">Aucun fichier n\'est disponible.</td></tr>';
	}
	
	$directory_j = '../produits/journal';
	
	$tabeau_tarifs_j  = '<tr><td class="TX_bold">Document&nbsp;téléchargeable</td><td class="TX_bold" width="80">Type</td><td class="TX_bold" width="100">Taille</td><td class="TX_bold"> </td></tr>';
	$tabeau_tarifs_j .= '<tr><td colspan="4" height="1" bgcolor="#000000"> </td></tr>';
	
	
	if($_GET['file']){		
		@unlink('../produits/journal/'.str_replace("\'","'",$_GET['file']));
		header('location:tarifs.php?idnc='.($_GET['idnc']+1));
	}
	
	$nb=0;
	if (is_dir($directory_j)){
		$rep_j = opendir($directory_j);
		$fileArray_j = array();
		while ($f = readdir($rep_j)){
		   if(is_file($directory_j.'/'.$f)) {
				$fileArray_j[] = substr($f,6);
				$fileLink_j[] = $f;
			  $nb++;
		   }
		}
		natcasesort($fileArray_j);
		if($nb){
			foreach($fileArray_j as $filePos_j => $fileName_j){
				$ko = ceil(filesize($directory_j.'/'.$fileLink_j[$filePos_j])/1024);
				$tabeau_tarifs_j .= '<tr><td class="TX"><a target="_blank" href="'.$directory_j.'/'.$fileLink_j[$filePos_j].'">'.$fileName_j.'</a></td>';
				$tabeau_tarifs_j .= '<td class="TX" width="80">PDF</td>';
				$tabeau_tarifs_j .= '<td class="TX" width="100">'.$ko.' Ko</td>';
				$tabeau_tarifs_j .= '<td align="left" >';
				if ($_SESSION['droit']== 9) $tabeau_tarifs_j .= '<img onmouseover="this.style.cursor=\'pointer\';" onclick="confirm_suppression(\''.str_replace("'","\'",$fileLink_j[$filePos_j]).'\')" noborder src="../images/icon_supp2.gif">';
				$tabeau_tarifs_j .=  '</td></tr>';
			}
		}
		closedir($rep_j); 
	}
	if($nb==0){
		$tabeau_tarifs_j .= '<tr><td colspan="4" class="TX">Aucun fichier n\'est disponible.</td></tr>';
	}
	
	
?>
	<html>
	<head>
	<title>Vakom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<link rel="stylesheet" href="../css/style.css" type="text/css">	
	<script language="JavaScript">
	<!--
	function confirm_suppression(file){
		if(confirm('Êtes vous sûr de vouloir supprimer ce fichier ?')){
			document.location.href='tarifs.php?file='+file;
		}
	}
	function cache(xxid){
		document.getElementById(xxid).style.display='none';
	}
	function montre(xxid){
		document.getElementById(xxid).style.display='block';
	}
	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
			<?php
				$_GET['menu_selected']=5;			
				include('menu_top_new.php');
			?>
	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;PRODUITS</td>
		</tr>
		</table>
	 <table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
		<tr> 
		  <td width="14"> </td>
		  <td height="14" class="TX_Tarifs">INFORMATIONS PRODUITS</td>
		  <td width="14"> </td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td height="1" bgcolor="#000000"> </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"> </td>
		  <td height="14"> </td>
		  <td width="14"> </td>
		</tr>
		<tr> 
		  <td width="14"> </td>
		  <td align="center">
			<table align="center" width="900" cellpadding="0" cellspacing="0" border="0">
				<?php echo $tabeau_tarifs ?>
			</table>
		  </td>
		  <td width="14"> </td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX">
			<table border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr> 
				<td align="left" class="menu_Gris"> 
				
				</td>
			  </tr>
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	</p>
						<p>		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;PETIT JOURNAL</td>
		</tr>
		</table>
	 <table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
		<tr> 
		  <td width="14"> </td>
		  <td height="14" class="TX_Tarifs">INFORMATIONS PETIT JOURNAL</td>
		  <td width="14"> </td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td height="1" bgcolor="#000000"> </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"> </td>
		  <td height="14"> </td>
		  <td width="14"> </td>
		</tr>
		<tr> 
		  <td width="14"> </td>
		  <td align="center">
			<table align="center" width="900" cellpadding="0" cellspacing="0" border="0">
				<?php echo $tabeau_tarifs_j ?>
			</table>
		  </td>
		  <td width="14"> </td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX">
			<table border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr> 
				<td align="left" class="menu_Gris"> 
				
				</td>
			  </tr>
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	</p>
				</div>	
		</article>
	</div>	
	</div>	
	</div>	
	</div>
</body>
</html>
<?php
}else{
	include('no_acces.php');
}
?>