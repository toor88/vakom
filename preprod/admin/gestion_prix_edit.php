<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	if ($_GET['prixid']>0){
			
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);
		
		if($_POST['select_produits']){ // Si le formulaire est posté
			
			/* On modifie les infos dans la base de données */
			$sql_up_prix = "UPDATE PRODUIT_A_PRIX SET PROD_ID='".txt_db($_POST['select_produits'])."',
			NATURE_CODE_ID='".txt_db($_POST['select_nature_part'])."',
			DATE_DEBUT=TO_DATE('".txt_db($_POST['JJ'])."/".txt_db($_POST['MM'])."/".txt_db($_POST['AAAA'])."', 'DD/MM/YYYY'),
			QUANTITE='".txt_db($_POST['quantite'])."',
			PRIX='".txt_db($_POST['prix'])."',
			PRIX_SANS_CONTRAT='".txt_db($_POST['prix_ss_c'])."' WHERE PRIX_ID='".txt_db($_GET['prixid'])."'";
			$qry_up_prix = $db->query($sql_up_prix);
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			<?php
		}
		
		/* On sélectionne les détails du prix sélectionnés */
		$sql_prix	= "SELECT PRODUIT_A_PRIX.*, TO_CHAR(PRODUIT_A_PRIX.DATE_DEBUT, 'DD') DD_debut, TO_CHAR(PRODUIT_A_PRIX.DATE_DEBUT, 'MM') MM_debut, TO_CHAR(PRODUIT_A_PRIX.DATE_DEBUT, 'YYYY') AAAA_debut  FROM PRODUIT_A_PRIX WHERE PRIX_ID = '".txt_db($_GET['prixid'])."'";
		$qry_prix	= $db->query($sql_prix);
		
		
		/* On sélectionne la liste des natures de partenaires */
		$sql_liste_nature_part = "SELECT * FROM CODE WHERE CODE_TABLE='NATURE' ORDER BY CODE_LIBELLE ASC";
		$qry_liste_nature_part = $db->query($sql_liste_nature_part);
		
		/* On sélectionne les produits existant */
		$sql_liste_produits = "SELECT * FROM PRODUIT ORDER BY PROD_NOM ASC";
		$qry_liste_produits = $db->query($sql_liste_produits);
	}
	if (is_array($qry_prix)){
		?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<script language="JavaScript">
		<!--
		function verif(){
				error = '';
				error1 = '';
				error2 = '';
			// verif du format date du début
			if (document.form.JJ.value<1 || document.form.JJ.value>31 || document.form.JJ.value.length<2){
				error1 = true;
			}
			if (document.form.MM.value<1 || document.form.MM.value>12 || document.form.MM.value.length<1){
				error1 = true;
			}
			if (document.form.AAAA.value<2009 || document.form.AAAA.value.length<4){
				error1 = true;
			}
			if (error1==true){
				error +='<?php echo $t_date_debut_not_correcte ?>\n';
			}
			
			if (document.form.quantite.value<1){
				error +='<?php echo $t_quantite_not_renseigne ?>\n';
			}
			
			if (document.form.prix.value.length<1){
				error +='<?php echo $t_prix_not_renseigne ?>\n';
			}
			
			if (error!=''){
				alert(error);
			}else{
				document.form.submit();
			}
		}
		
		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		//-->
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#" name="form">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;MODIFIER UN PRIX</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td><table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="100%" align="center">
			<tr> 
			  <td height="14"></td>
			  <td height="14"></td>
		    </tr>
			<?php
			if (is_array($qry_liste_nature_part)){
				?>
				<tr> 
				  <td align="left" class="TX">Nature du Partenaire* :&nbsp; </td>
				  <td align="left" class="TX"> 
					<select name="select_nature_part" class="form_ediht_Tarifs">
					  <option value="9">Tous</option>
					  <?php
					  foreach($qry_liste_nature_part as $nature_part){
						unset($selected_nature);
						if($nature_part['code_id'] == $qry_prix[0]['nature_code_id']){
							$selected_nature = ' selected="selected"';
						}
						echo '<option value="'.$nature_part['code_id'].'"'.$selected_nature.'>'.$nature_part['code_libelle'].'</option>';
					  }
					  ?>
					</select>
				  </td>
			    </tr>
				<tr> 
				  <td colspan="2">&nbsp;</td>
				</tr>
			<?php
			}
			if (is_array($qry_liste_produits)){
				?>
				<tr> 
				  <td align="left" class="TX">Produit* : </td>
				  <td align="left" class="TX"> 
					<select name="select_produits" class="form_ediht_Tarifs">
					  <?php
					  foreach($qry_liste_produits as $liste_produit){
						unset($selected_prod);
						if($liste_produit['prod_id'] == $qry_prix[0]['prod_id']){
							$selected_prod = ' selected="selected"';
						}
						echo '<option value="'.$liste_produit['prod_id'].'"'.$selected_prod.'>'.$liste_produit['prod_nom'].'</option>';
					  }
					  ?>
					</select>
				  </td>
				</tr>
				<tr> 
				  <td colspan="2">&nbsp;</td>
				</tr>
				<?php
			}
			?>
			<tr> 
			  <td align="left" class="TX">Date de début* :</td>
			  <td align="left"> 
				<input type="text" name="JJ" class="form_ediht_Tarifs" size="2" maxlength="2" value="<?php echo htmlentities($qry_prix[0]['dd_debut']) ?>">
				/ 
				<input type="text" name="MM" class="form_ediht_Tarifs" size="2" maxlength="2" value="<?php echo htmlentities($qry_prix[0]['mm_debut']) ?>">
				/ 
				<input type="text" name="AAAA" class="form_ediht_Tarifs" size="4" maxlength="4" value="<?php echo htmlentities($qry_prix[0]['aaaa_debut']) ?>">
			  </td>
			</tr>
			<tr> 
			  <td colspan="2">&nbsp;</td>
			</tr>
			<tr> 
			  <td align="left" class="TX">Quantité* :</td>
			  <td align="left"> 
				<input type="text" name="quantite" class="form_ediht_Tarifs" size="5" maxlength="20" value="<?php echo htmlentities($qry_prix[0]['quantite']) ?>">
			  </td>
			</tr>
			<tr> 
			  <td align="left" class="TX" height="40">Prix* :</td>
			  <td align="left"> 
				<input type="text" name="prix" class="form_ediht_Tarifs" size="5" maxlength="20" value="<?php echo number_format(str_replace(',', '.', $qry_prix[0]['prix']), 2, ',', ' '); ?>">
			  </td>
			</tr>
			<tr> 
			  <td align="left" class="TX">Prix Sans contrat :</td>
			  <td align="left"> 
				<input type="text" name="prix_ss_c" class="form_ediht_Tarifs" size="5" maxlength="20" value="<?php echo number_format(str_replace(',', '.', $qry_prix[0]['prix_sans_contrat']), 2, ',', ' '); ?>">
			  </td>
			</tr>
			<tr> 
			  <td height="14"></td>
			  <td height="14"></td>
		    </tr>
		  </table></td>
		  </tr>
		</table>
		  
		  <br>
		  <p style="text-align:center"> 
				<input type="submit" name="Submit" value="Valider" class="BN" onClick="verif();"></p>

		</form>
		</body>
		</html>
		<?php
	}else{
		include('../config/lib/lang.php');
		echo '<center>'.$t_acn_1.'<br><input type="button" name="Submit" value="Fermer" class="BN" onClick="window.close();"></center>';
	}
}else{
	include('no_acces.php');
}
?>