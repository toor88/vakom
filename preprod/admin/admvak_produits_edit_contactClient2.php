<?php
session_start();
// Si l'utilisateur est un super admin ou un admin vakom
if ($_SESSION['droit']>2){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
		
	if ($_GET['certjetid']>0){
		
			/* Si le formulaire est posté */
			if ($_POST['min']!=''){
				
				if ($_SESSION['vak_id']>0){
					/* VAKOM */
					$resp = 1;
				}else{
					/* Client */
					$resp = 0;
				}

			/* On met à jour la base */
			$sql_up_jet 	= "UPDATE CERT_A_JETON SET CERT_JET_NOMBRE='".txt_db($_POST['nbre'])."', 
			CERT_JET_INFO1 = '".txt_db($_POST['comm1'])."', 
			CERT_JET_INFO2 = '".txt_db($_POST['comm2'])."' WHERE CERT_JET_ID='".txt_db(intval($_GET['certjetid']))."'";
			$qry_up_jet		= $db->query($sql_up_jet);
			
			$delta				= (intval($_POST['hid_old_jetons'])-intval($_POST['nbre']));
			
			$sql_up_jet2 	= "update jeton_corresp_cert set affecte='".txt_db($_POST['nbre'])."' where cert_jet_id='".txt_db(intval($_GET['certjetid']))."'";
			$qry_up_jet2 	= $db->query($sql_up_jet2);
			
			$sql_up_jet_id 	= "SELECT jet_id from jeton_corresp_cert where cert_jet_id='".txt_db(intval($_GET['certjetid']))."'";
			$qry_up_jet_id 	= $db->query($sql_up_jet_id);
			
			$sql_up_jet2 	= "update jeton_corresp_cert set affecte=affecte+".$delta." where cert_jet_id=-1 and jet_id='".$qry_up_jet_id[0]['jet_id']."'";
			$qry_up_jet2 	= $db->query($sql_up_jet2);
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			
			<?php
			}
		}
		
		/* On sélectionne les valeurs min et max pour la redéfinition du nombre de jetons */
		$sql_sel_minmax = "SELECT affecte,utilise from jeton_corresp_cert where cert_jet_id='".txt_db(intval($_GET['certjetid']))."'";
		//echo $sql_sel_minmax;
		$qry_sel_minmax	= $db->query($sql_sel_minmax);

		$nbre_min = intval($qry_sel_minmax[0]['utilise']);
		$nbre_max = intval($qry_sel_minmax[0]['affecte']);
			
		/* On sélectionne les infos  */
		$sql_cert_info = "SELECT * FROM CERTIFIE, PARTENAIRE, CERT_A_JETON WHERE CERT_PART_ID=PART_ID AND CERT_JET_ID= '".txt_db(intval($_GET['certjetid']))."' AND CERT_ID=CERT_JET_CERT_ID";
		$qry_cert_info = $db->query($sql_cert_info);
		
		if (is_array($qry_cert_info)){
		// Si on a choisi un partenaire
		
			?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--
			function verif(){
				error = '';
				if (parseInt(document.form.nbre.value) > parseInt(document.form.max.value) || parseInt(document.form.nbre.value) < parseInt(document.form.min.value)){
					error += "<?php echo $t_nbre_jet_inf_egal ?> "+ parseInt(document.form.max.value) +" <?php echo $t_nbre_jet_sup_egal ?> "+ parseInt(document.form.min.value) +"\n";
				}
				
				if (error!=''){
					alert(error);
				}else{
					document.form.submit();
				}
			}
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			//-->
			</script>
			</head>
			<body bgcolor="#FFFFFF" text="#000000">
			<form method="post" action="#" name="form">
			<table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Certifies2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo strtoupper($qry_cert_info[0]['part_nom']) .' > '.strtoupper($qry_cert_info[0]['cert_nom']) . ' ' . ucfirst($qry_cert_info[0]['cert_prenom']) ?></td>
				</tr>
			</table>
			  <table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td class="TX_Certifies"><?php echo $t_jet_modif_aff ?></td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="center" valign="top" class="TX"> 
					<table border="0" cellspacing="0" cellpadding="0">
					  <tr> 
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_jet_nbre ?>* : </td>
						<td  class="TX"> 
						  <input type="text" name="nbre" class="form_ediht" value="<?php echo htmlentities($qry_cert_info[0]['cert_jet_nombre']); ?>">
						  <input type="hidden" name="hid_old_jetons" class="form_ediht" value="<?php echo htmlentities($qry_cert_info[0]['cert_jet_nombre']); ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_comment ?> :</td>
						<td  class="TX"> 
						  <input type="text" name="comm1" size="50" class="form_ediht" maxlength="50" value="<?php echo htmlentities($qry_cert_info[0]['cert_jet_info1']); ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40">&nbsp;</td>
						<td  class="TX"> 
						  <input type="text" name="comm2" size="50" class="form_ediht" maxlength="50" value="<?php echo htmlentities($qry_cert_info[0]['cert_jet_info2']); ?>">
						</td>
					  </tr>
					</table>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
			  <br>
			  <table border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td align="center"> 
					<input type="hidden" name="min" value="<?php echo $nbre_min ?>" >
					<input type="hidden" name="max" value="<?php echo $nbre_max ?>" >
					<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_certifie" onClick="verif();">
				  </td>
				</tr>
			  </table>
			</form>
			</body>
			</html>
		<?php
		}else{
			include('../config/lib/lang.php');
			echo $t_acn_1;
		}
	}else{
	include('no_acces.php');
}
?>
