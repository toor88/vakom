<?php
session_start();
// Si l'utilisateur est un super admin ou un admin vakom
if ($_SESSION['droit']>0 && $_SESSION['part_id']>0){
	if ($_SESSION['cert_id']>0){
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);
		
		if ($_POST['nom'] != ''){ // Si le formulaire est envoyé
			if ($_POST['libre']!='1'){
				$cli_id = $_POST['select_client'];
			}else{
				$cli_id = '-1';
			}
			if ($_POST['inactif']== 1){
				$actif = 0;
			}else{
				$actif = 1;
			}
			$dns = $_POST['JJ_DNS'].'/'.$_POST['MM_DNS'].'/'.$_POST['AAAA_DNS'];
			
			$sql_insert_candidat = "INSERT INTO CANDIDAT VALUES(SEQ_ID.NEXTVAL,
			'".txt_db($cli_id)."',
			'".txt_db($_POST['nom'])."',
			'".txt_db($_POST['prenom'])."',
			'".txt_db($_POST['mail'])."',
			'".txt_db($_POST['sexe'])."',";
			
			if (strlen($_POST['JJ_DNS'])==2 && strlen($_POST['MM_DNS'])==2 && strlen($_POST['AAAA_DNS'])==4){
				$sql_insert_candidat .= "TO_DATE('".txt_db($dns)."','DD/MM/YYYY'),";
			}else{
				$sql_insert_candidat .= "'',";
			}
			
			$sql_insert_candidat .= "'".txt_db($_POST['fct'])."',
			'".txt_db($_POST['langue'])."',
			'".txt_db($_POST['adr1'])."',
			'".txt_db($_POST['adr2'])."',
			'".txt_db($_POST['cp'])."',
			'".txt_db($_POST['ville'])."',
			'".txt_db($_POST['select_pays'])."',
			'".txt_db($_POST['tel'])."',
			'".txt_db($_POST['tel_p'])."',
			'".txt_db($_POST['fax'])."',
			'".txt_db($_POST['comm1'])."',
			'".txt_db($_POST['comm2'])."',
			'".txt_db(intval($actif))."',
			'".$_SESSION['cert_id']."',
			SYSDATE,
			'',
			'',
			'',
			'',
			'".txt_db(intval($_SESSION['cert_id']))."',null,null)";
			//echo $sql_insert_candidat;
			$qry_insert_candidat = $db->query($sql_insert_candidat);
			
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			<?php
		}
		
		/* On sélectionne la liste des pays */
		$sql_pays_list = "SELECT * FROM CODE WHERE CODE_TABLE='PAYS'";
		$qry_pays_list = $db->query($sql_pays_list);
		
		/* On sélectionne la liste des langues disponibles */
		$sql_langue_list = "SELECT * FROM LANGUE";
		$qry_langue_list = $db->query($sql_langue_list);
		
		/* On sélectionne les information du certifié sélectionné */
		$sql_info_cert = "SELECT * FROM CERTIFIE WHERE CERT_ID = '".txt_db(intval($_SESSION['cert_id']))."' AND CERT_PART_ID='".txt_db($_SESSION['part_id'])."'";
		$qry_info_cert = $db->query($sql_info_cert);

		if (!is_array($qry_info_cert)){
			?>
			<script type="text/javascript">
				window.close();
			</script>
			<?php
		}
		
		/* On sélectionne la liste des clients du certifié */
		$sql_liste_clients = "SELECT CLIENT.CLI_ID, CLIENT.CLI_NOM, CLIENT.CLI_VILLE FROM CLIENT, CLIENT_A_CERT 
		WHERE CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND CLIENT_A_CERT.CERT_ID='".txt_db(intval($_SESSION['cert_id']))."' ORDER BY CLIENT.CLI_NOM ASC";
		$qry_liste_clients = $db->query($sql_liste_clients);		
		?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<script language="JavaScript">
		<!--
		function check_libre(){
			if (document.form.libre.checked==true){
				document.getElementById('pas_libre').style.display='none';
			}
			if (document.form.libre.checked==false){
				document.getElementById('pas_libre').style.display='block';
			}
		}
		
		function verif(){
			var error = '';
			
			if (document.form.nom.value == ''){
				error += "<?php echo $t_nom_oblig ?>\n";
			}
			if (document.form.prenom.value == ''){
				error += "<?php echo $t_prenom_oblig ?>\n";
			}
			if (document.form.mail.value == ''){
				error += "<?php echo $t_email_oblig ?>\n";
			}
			
			if (document.form.JJ_DNS.value.length>0 || document.form.MM_DNS.value.length>0 || document.form.AAAA_DNS.value.length>0){
				// verif du format date du début
				error1=false;
				if (document.form.JJ_DNS.value<1 || document.form.JJ_DNS.value>31 || document.form.JJ_DNS.value.length<2){
					error1 = true;
				}
				if (document.form.MM_DNS.value<1 || document.form.MM_DNS.value>12 || document.form.MM_DNS.value.length<2){
					error1 = true;
				}
				if (document.form.AAAA_DNS.value<1909 || document.form.AAAA_DNS.value.length<4){
					error1 = true;
				}
				if (error1==true){
					error +="<?php echo $t_mauvaise_date_naiss ?>\n";
				}
			}
			
			/* Si il y a une erreur, on l'affiche */
			if (error!=''){
				alert(error);
			}else{ // Sinon on soumet le formulaire
				document.form.submit();
			}
		
		}
		
		function pass(champ){
			if (champ==1){
				if (document.getElementById('JJ_DNS').value.length==2){
					document.getElementById('MM_DNS').value='';
					document.getElementById('MM_DNS').focus();
				}
			}
			if (champ==2){
				if (document.getElementById('MM_DNS').value.length==2){
					document.getElementById('AAAA_DNS').value='';
					document.getElementById('AAAA_DNS').focus();
				}
			}
			if (champ==3){
				if (document.getElementById('AAAA_DNS').value.length==4){
					document.getElementById('fct').focus();
				}
			}
		}
		
		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		//-->
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#" name="form">
			<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $t_fiche_new_cand ?></td>
			</tr>
		   </table>
		  <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="800" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="champsoblig" valign="middle" align="right"><?php echo $t_champs_oblig ?> 
				* </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="left" valign="top" class="TX"> 
				<table border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td class="TX" width="133"><?php echo $t_cand_libre ?> :</td>
					<td >
					  <input type="checkbox" name="libre" value="1" onclick="check_libre()">
					</td>
					<td class="TX" height="40"><?php echo $t_fiche_cand_inactif ?> :</td>
					<td class="TX"> 
					  <input type="checkbox" name="inactif" value="1">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" colspan="2"> 
						<div id="pas_libre">
						<table border='0' cellpadding="0" cellspacing="0"><tr><td class="TX" width="133">
							<?php echo $t_select_client ?>&nbsp;:&nbsp;</td>
							<td><select name="select_client" class="form_ediht_Candidats">
								<?php
								if (is_array($qry_liste_clients)){
									foreach($qry_liste_clients as $client_liste){
										echo '<option value="'.$client_liste['cli_id'].'">'.strtoupper($client_liste['cli_nom']).' - '.ucfirst($client_liste['cli_ville']).'</option>';
									}
								}else{
									echo '<option value="-1">'.$t_aucun_clt_cert.'</option>';
								}
								?>
							  </select></td>
							  </tr>
							</table>
						</div>
					</td>
					<td class="TX" >&nbsp;</td>
					<td >&nbsp;</td>
				  </tr>
				  <tr> 
					<td class="TX" height="40"><?php echo $t_nom ?>* :</td>
					<td > 
					  <input type="text" name="nom" size="40" class="form_ediht_Candidats" >
					  &nbsp;&nbsp;&nbsp;&nbsp; </td>
					<td class="TX" ><?php echo $t_prenom ?>* :</td>
					<td > 
					  <input type="text" name="prenom" size="40" class="form_ediht_Candidats" >
					</td>
				  </tr>
				  <tr> 
					<td class="TX" height="40"><?php echo $t_email ?>* :</td>
					<td class="TX" colspan="3" > 
					  <input type="text" name="mail" size="40" class="form_ediht_Candidats">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" height="40"><?php echo $t_sexe ?> :</td>
					<td class="TX" > 
					  <input type="radio" name="sexe" value="H">
					  <?php echo $t_M ?>&nbsp;&nbsp; 
					  <input type="radio" name="sexe" value="F">
					  <?php echo $t_F ?></td>
					<td class="TX" ><?php echo $t_date_naissance ?> : </td>
					<td > 
					  <input type="text" name="JJ_DNS" id="JJ_DNS" onkeyup="pass(1)" size="2" class="form_ediht_Candidats" maxlength="2">
					  / 
					  <input type="text" name="MM_DNS" id="MM_DNS" onkeyup="pass(2)" size="2" class="form_ediht_Candidats" maxlength="2">
					  / 
					  <input type="text" name="AAAA_DNS" id="AAAA_DNS" onkeyup="pass(3)" size="4" class="form_ediht_Candidats" maxlength="4">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" height="40"><?php echo $t_fonction ?> :</td>
					<td> 
					  <input type="text" name="fct" id="fct" size="40" class="form_ediht_Candidats" >
					</td>
					<td class="TX"><?php echo $t_langue ?>* :</td>
					<td> 
					  <select name="langue" class="form_ediht_Candidats">
						<?php
						  if (is_array($qry_langue_list)){
							foreach($qry_langue_list as $langue){
								echo '<option value="'.$langue['lang_id'].'">'.$langue['lang_libelle'].'</option>';
							}
						  }
						?>
					  </select>
					</td>
				  </tr>
				  <tr> 
					<td class="TX" height="40">&nbsp;</td>
					<td>&nbsp; </td>
					<td class="TX">&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr> 
					<td class="TX" height="40"><?php echo $t_adresse ?> :</td>
					<td> 
					  <input type="text" name="adr1" size="40" class="form_ediht_Candidats" maxlength="50">
					</td>
					<td class="TX"><?php echo $t_cp ?> :</td>
					<td> 
					  <input type="text" name="cp" size="5" class="form_ediht_Candidats" maxlength="5">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" height="40">&nbsp;</td>
					<td> 
					  <input type="text" name="adr2" size="40" class="form_ediht_Candidats" maxlength="50">
					</td>
					<td class="TX"><?php echo $t_ville ?> :</td>
					<td> 
					  <input type="text" name="ville" size="40" class="form_ediht_Candidats" maxlength="50">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" height="40"><?php echo $t_pays ?> : </td>
					<td> 
					  <select name="select_pays" class="form_ediht_Candidats">
					<?php
					  if (is_array($qry_pays_list)){
						foreach($qry_pays_list as $pays){
							echo '<option value="'.$pays['code_id'].'">'.$pays['code_libelle'].'</option>';
						}
					  }
					?>
					  </select>
					</td>
					<td class="TX">&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr> 
					<td class="TX" height="40"><?php echo $t_tel_fix ?> :</td>
					<td class="TX"> 
					  <input type="text" name="tel" size="40" class="form_ediht_Candidats" maxlength="25">
					</td>
					<td class="TX"><?php echo $t_tel_port ?> :&nbsp;</td>
					<td class="TX"> 
					  <input type="text" name="tel_p" size="40" class="form_ediht_Candidats" maxlength="25">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" height="40">&nbsp;<?php echo $t_fax ?> : </td>
					<td class="TX"> 
					  <input type="text" name="fax" size="40" class="form_ediht_Candidats" maxlength="25">
					</td>
					<td class="TX">&nbsp;</td>
					<td class="TX">&nbsp; </td>
				  </tr>
				  <tr> 
					<td class="TX" height="30"><?php echo $t_comment ?> :</td>
					<td class="TX" colspan="3"> 
					  <input type="text" name="comm1" size="40" class="form_ediht_Candidats" maxlength="70">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" valign="top">&nbsp;</td>
					<td class="TX" colspan="3"> 
					  <input type="text" name="comm2" size="40" class="form_ediht_Candidats" maxlength="70">
					</td>
				  </tr>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
		  <br>
		  <table border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td align="center"> 
				<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_candidat" onClick="verif()">
			  </td>
			</tr>
		  </table>
		</form>
		</body>
		</html>
	<?php
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{
	include('no_acces.php');
}
?>	