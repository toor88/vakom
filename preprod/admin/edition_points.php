<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
		
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	if ($_GET['questid'] > 0 && $_GET['varid'] > 0){
		/* On fait la requete pour trouver le questionnaire voulu */
		$sql_var = "SELECT QUESTIONNAIRE.QUEST_ID, QUESTIONNAIRE.QUEST_NOM, CODE.CODE_ID PROFIL_ID, CODE2.CODE_ID VARIABLE_ID, VAR.COR_VAR_ID, VAR.COR_VAR_SOMME, VAR.COR_VAR_POINTS 
		FROM QUESTIONNAIRE, CODE CODE, CODE CODE2, QUEST_A_CORRESP_VAR VAR 
		WHERE QUESTIONNAIRE.QUEST_ID='".txt_db($_GET['questid'])."' 
		AND VAR.COR_VAR_QUEST_ID=QUESTIONNAIRE.QUEST_ID 
		AND VAR.COR_VAR_TYPE_PROFIL_CODE_ID=CODE.CODE_ID 
		AND CODE.CODE_TABLE='TYPE_PROFIL' 
		AND VAR.COR_VAR_TYPE_VARIABLE_CODE_ID=CODE2.CODE_ID 
		AND CODE2.CODE_TABLE='TYPE_VARIABLE' 
		AND VAR.COR_VAR_ID='".txt_db($_GET['varid'])."'";
		//echo $sql_var;
		$qry_var = $db->query($sql_var);
		
		/* Sélection des différents profils */
		$sql_list_type_profil = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='TYPE_PROFIL' ORDER BY CODE_LIBELLE ASC";
		$qry_list_type_profil = $db->query($sql_list_type_profil);
		
		/* Sélection des différentes variables */
		$sql_list_type_var = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='TYPE_VARIABLE' AND UPPER(CODE_LIBELLE) NOT IN ('N') ORDER BY CODE_LIBELLE ASC";
		$qry_list_type_var = $db->query($sql_list_type_var);
	
		if (trim($_POST['somme'])>=0 && trim($_POST['points'])>=0 && $_POST['select_profil']>0 && $_POST['selectECPA']>0){
			$sql_up_cor_var = "UPDATE QUEST_A_CORRESP_VAR SET COR_VAR_TYPE_PROFIL_CODE_ID='".txt_db($_POST['select_profil'])."', COR_VAR_TYPE_VARIABLE_CODE_ID='".txt_db($_POST['selectECPA'])."', 
			COR_VAR_SOMME='".txt_db(str_replace('.',',',$_POST['somme']))."', 
			COR_VAR_POINTS='".txt_db(str_replace('.',',',$_POST['points']))."', 
			COR_VAR_USER_MODIFICATION='".$_SESSION['vak_id']."', COR_VAR_DATE_MODIFICATION=SYSDATE WHERE COR_VAR_ID='".txt_db($_GET['varid'])."'";
			//echo $sql_up_cor_var;
			$qry_up_cor_var = $db->query($sql_up_cor_var);
			
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			<?php
		}
	}
		?>
	<html>
		<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--
			function verif(){
				error = '';
				if (document.form.somme.value.length<1 ){
					error += '<?php echo $t_somme_obg ?>\n';
				}
				if (document.form.points.value.length<1 ){
					error += '<?php echo $t_points_obg ?>\n';
				}
				
				if (error !=''){
					alert(error);
				}else{
					document.form.submit();
				}
			}
			
			function MM_goToURL() { //v3.0
			   var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			   for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			//-->
			</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="" name="form">
		  			  	<table width="830" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;CORRESPONDANCES&nbsp;>&nbsp;<?php echo ucfirst($qry_var[0]['quest_nom']) ?></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><table border="0" cellspacing="0" cellpadding="0"  width="100%" align="center">
			<tr> 
			  <td height="14"></td>
			  <td height="14"></td>
			  <td height="14"></td>
		    </tr>
			<tr> 
			  <td class="TX_Tarifs" colspan="3">Modification&nbsp;d'un&nbsp;positionnement&nbsp;graphique&nbsp;(point)</td>
		    </tr>
			<tr> 
			  <td bgcolor="#666666" height="1"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td bgcolor="#666666" height="1"></td>
		    </tr>
			<tr>
			  <td class="TX" align="center">&nbsp;</td>
			  <td class="TX" align="center">&nbsp;</td>
			  <td class="TX" align="center">&nbsp;</td>
		    </tr>
			<?php
			if (is_array($qry_var) && is_array($qry_list_type_var) && is_array($qry_list_type_profil)){
			  ?>
				<tr> 
				  <td align="left" class="TX">Type de profil :</td>
				  <td align="center" class="TX">&nbsp;</td>
				  <td align="left"> 
					<select name="select_profil" class="form_ediht_Tarifs">
					  <?php
					  foreach($qry_list_type_profil as $type_profil){
						if ($type_profil['code_id'] == $qry_var[0]['profil_id']){
							$selected= ' selected="selected"';
						}else{
							$selected= '';
						}
						echo '<option value="'.$type_profil['code_id'].'"'.$selected.'>'.htmlentities($type_profil['code_libelle']).'</option>';
					  }
					  ?>
					</select>
				  </td>
			    </tr>
				<tr> 
					<td height="40" align="left" class="TX">Variable :</td>
					<td align="center" class="TX">&nbsp;</td>
					<td align="left"> 
					  <select name="selectECPA" class="form_ediht_Tarifs">
					  <?php
					  foreach($qry_list_type_var as $type_var){
						if ($type_var['code_id'] == $qry_var[0]['variable_id']){
							$selected= ' selected="selected"';
						}else{
							$selected= '';
						}
						echo '<option value="'.$type_var['code_id'].'"'.$selected.'>'.htmlentities($type_var['code_libelle']).'</option>';
					  }
					  ?>
					  </select>
					</td>
				</tr>
				<tr> 
				  <td align="left" class="TX" height="40">Valeur de la somme :</td>
				  <td align="center" height="40" class="TX">&nbsp;</td>
				  <td align="left" height="40"> 
					<input type="text" name="somme" class="form_ediht_Tarifs" size="5" maxlength="5" value="<?php echo htmlentities($qry_var[0]['cor_var_somme']) ?>">
				  </td>
			    </tr>
				<tr> 
				  <td align="left" class="TX" height="40">Valeur du point : </td>
				  <td align="center" height="40" class="TX">&nbsp;</td>
				  <td align="left" height="40"> 
					<input type="text" name="points" class="form_ediht_Tarifs" size="5" maxlength="5" 
					<?php
					if ($qry_var[0]['cor_var_points']<1 && !is_int($qry_var[0]['cor_var_points']) && $qry_var[0]['cor_var_points']!='0'){
						echo 'value="0'.htmlentities($qry_var[0]['cor_var_points']).'"';
					}else{
						echo 'value="'.htmlentities($qry_var[0]['cor_var_points']).'"';
					}								
					?>>
				  </td>
			    </tr>
				<?php
			}else{
			?>
				<tr> 
				  <td colspan="3" align="center" height="40" class="TX">Un problème est survenu</td>
			    </tr>
			<?php
			}
			?>
		  </table></td>
			  </tr>
            </table>
		  
		  <br>
		  
		  <p style="text-align:center"> 
			<?php
			if (is_array($qry_var)){
				?>
				<input type="button" name="Submit" value="Valider" class="BN" onClick="verif();">&nbsp;
				<?php
			}
			?>
			 </p>
             
		</form>
		</body>
	</html>
<?php
}else{
	include('no_acces.php');
}
?>