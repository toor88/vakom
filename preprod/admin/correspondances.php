<?php
session_start();
	// Si l'utilisateur est un super admin
if (@$_SESSION['droit']=='9'){
		
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	if (@$_POST['select_quest'] > 0){ // Si un questionnaire a été sélectionné
		/* On fait la requete pour trouver le questionnaire voulu */
		$sql_quest = "SELECT QUEST_ID FROM QUESTIONNAIRE WHERE QUEST_ID='".txt_db($_POST['select_quest'])."'";
		$qry_quest = $db->query($sql_quest);
		if (is_array($qry_quest)){
			header('location:correspondances.php?questid='.$qry_quest[0]['quest_id'].'&order1='.$_GET['oreder1'].'&order2='.$_GET['order2'].'&idnc='.$_GET['idnc']);
		}
	}
	
	/* Si on veut supprimer une ou plusieurs regles */
	if (@is_array($_POST['che_regle'])){
		foreach($_POST['che_regle'] as $regleid){
			/* On supprime chaque regle cochée */
			$sql_del_regle = "DELETE FROM QUEST_A_CORRESP_REGLE WHERE COR_REGLE_ID='".txt_db(intval($regleid))."'";
			$qry_del_regle = $db->query($sql_del_regle);
		}
		header('location:correspondances.php?questid='.$_GET['questid'].'&order1='.$_GET['oreder1'].'&order2='.$_GET['order2'].'&idnc='.$_GET['idnc']);
	}
	/* Si on veut supprimer une ou plusieurs variables */
	if (@is_array($_POST['che_var'])){
		foreach($_POST['che_var'] as $varid){
			/* On supprime chaque regle cochée */
			$sql_del_var = "DELETE FROM QUEST_A_CORRESP_VAR WHERE COR_VAR_ID='".txt_db(intval($varid))."'";
			$qry_del_var = $db->query($sql_del_var);
		}
		header('location:correspondances.php?questid='.$_GET['questid'].'&order1='.$_GET['oreder1'].'&order2='.$_GET['order2'].'&idnc='.$_GET['idnc']);
	}
	
	if (@$_GET['questid'] > 0){
	
		if (@$_GET['action']){
			switch ($_GET['action']){
				case 'delete_var':
					if ($_GET['varid']>0){
						$sql_del_var 	= "UPDATE QUEST_A_CORRESP_VAR SET COR_VAR_DATE_SUPPRESSION=SYSDATE, COR_VAR_USER_SUPPRESSION='".$_SESSION['vak_id']."' WHERE COR_VAR_QUEST_ID='".txt_db(intval($_GET['questid']))."' AND COR_VAR_ID='".txt_db(intval($_GET['varid']))."'";
						$qry_del_var 	= $db->query($sql_del_var);
						header('location:correspondances.php?questid='.$_GET['questid'].'&order1='.$_GET['oreder1'].'&order2='.$_GET['order2']);
					}
				break;
				case 'delete_regle':
					if ($_GET['regleid']>0){
						$sql_del_regle = "UPDATE QUEST_A_CORRESP_REGLE SET COR_REGLE_DATE_SUPPRESSION=SYSDATE, COR_REGLE_USER_SUPPRESSION='".$_SESSION['vak_id']."' WHERE COR_REGLE_QUEST_ID='".txt_db(intval($_GET['questid']))."' AND COR_REGLE_ID='".txt_db(intval($_GET['regleid']))."'";
						$qry_del_regle = $db->query($sql_del_regle);
						header('location:correspondances.php?questid='.$_GET['questid'].'&order1='.$_GET['order1'].'&order2='.$_GET['order2']);
					}
				break;				
			}
		}
	
		/* On fait la requete pour trouver le questionnaire voulu */
		$sql_quest = "SELECT QUEST_ID, QUEST_NOM FROM QUESTIONNAIRE WHERE QUEST_ID='".txt_db($_GET['questid'])."'";
		$qry_quest = $db->query($sql_quest);
	
		switch($_GET['order1']){
			case 'type_profil';
				$order_1 = " ORDER BY CODE.CODE_LIBELLE ASC";
			break;
			case 'point';
				$order_1 = " ORDER BY REGLE.COR_REGLE_POINTS_INF ASC";
			break;
			case 'segment';
				$order_1 = " ORDER BY REGLE.COR_REGLE_SEGMENT ASC";
			break;
			case 'bloc';
				$order_1 = " ORDER BY REGLE.COR_REGLE_BLOC ASC";
			break;
			default:
				$order_1 = " ORDER BY CODE.CODE_LIBELLE, COR_REGLE_SEGMENT ASC";
			break;
		}
		
		switch($_GET['order2']){
			case 'type_profil';
				$order_2 = " ORDER BY CODE.CODE_LIBELLE ASC, CODE2.CODE_LIBELLE ASC,VAR.COR_VAR_SOMME ASC";
			break;
			case 'variable';
				$order_2 = " ORDER BY CODE2.CODE_LIBELLE ASC";
			break;
			case 'somme';
				$order_2 = " ORDER BY VAR.COR_VAR_SOMME ASC";
			break;
			case 'point';
				$order_2 = " ORDER BY VAR.COR_VAR_POINTS ASC";
			break;
			default:
				$order_2 = " ORDER BY CODE.CODE_LIBELLE ASC, CODE2.CODE_LIBELLE ASC,VAR.COR_VAR_SOMME ASC";
			break;
		}
		
		/* On sélectionne toutes les variables définies pour le questionnaire sélectionné */
		$sql_quest_var = "SELECT VAR.*, QUEST.QUEST_ID, CODE.CODE_LIBELLE PROFIL_LIB, CODE2.CODE_LIBELLE VAR_LIB FROM CODE CODE, CODE CODE2, QUEST_A_CORRESP_VAR VAR, QUESTIONNAIRE QUEST WHERE VAR.COR_VAR_TYPE_PROFIL_CODE_ID=CODE.CODE_ID AND CODE.CODE_TABLE='TYPE_PROFIL' AND VAR.COR_VAR_TYPE_VARIABLE_CODE_ID=CODE2.CODE_ID AND CODE2.CODE_TABLE='TYPE_VARIABLE' AND QUEST.QUEST_ID=VAR.COR_VAR_QUEST_ID AND QUEST.QUEST_ID='".txt_db($_GET['questid'])."' AND VAR.COR_VAR_DATE_SUPPRESSION IS NULL ".$order_2."";
		//echo $sql_quest_var.'<br />';
		$qry_quest_var = $db->query($sql_quest_var);
		
		/* On sélectionne toutes les règles définies pour le questionnaire sélectionné */
		$sql_quest_regle = "SELECT REGLE.*, QUEST.QUEST_ID, CODE.CODE_LIBELLE PROFIL_LIB FROM CODE, QUEST_A_CORRESP_REGLE REGLE, QUESTIONNAIRE QUEST WHERE CODE.CODE_TABLE='TYPE_PROFIL' AND CODE.CODE_ID=REGLE.COR_REGLE_TYPE_PROFIL_CODE_ID AND QUEST.QUEST_ID=REGLE.COR_REGLE_QUEST_ID AND QUEST.QUEST_ID='".txt_db($_GET['questid'])."' AND REGLE.COR_REGLE_DATE_SUPPRESSION IS NULL ".$order_1."";
		//$sql_quest_regle = "SELECT TO_NUMBER(REGLE.COR_REGLE_POINTS_SUP, '9999999999999D9999999999', 'nls_numeric_characters='',.''') FROM CODE, QUEST_A_CORRESP_REGLE REGLE, QUESTIONNAIRE QUEST WHERE CODE.CODE_TABLE='TYPE_PROFIL' AND CODE.CODE_ID=REGLE.COR_REGLE_TYPE_PROFIL_CODE_ID AND QUEST.QUEST_ID=REGLE.COR_REGLE_QUEST_ID AND QUEST.QUEST_ID='".txt_db($_GET['questid'])."' AND REGLE.COR_REGLE_DATE_SUPPRESSION IS NULL ".$order_1."";
		//echo $sql_quest_regle.'<br />';
		$qry_quest_regle = $db->query($sql_quest_regle);
	}
	
	/* On fait la liste des questionnaires contenus dans la base */
	$sql_liste_quest = "SELECT QUEST_ID, QUEST_NOM FROM QUESTIONNAIRE WHERE QUEST_DATE_SUPPRESSION IS NULL ORDER BY QUEST_NOM ASC";
	$qry_liste_quest = $db->query($sql_liste_quest);
	?>
	<html>
		<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<link rel="stylesheet" href="../css/style.css" type="text/css">			
			<script language="JavaScript">
				<!--
				function delete_var(){
					if (confirm('<?php echo $t_supprimer_variables ?>')){
						document.form_var.submit();
					}
				}
				
				function delete_regle(){
					if (confirm('<?php echo $t_supprimer_regles ?>')){
						document.form_regle.submit();
					}
				}
				
				function coche1(field){
					if (document.form_regle.select_all1.checked==true){
						field.checked=true;
						for (i=0;i<field.length;i++)
						{
						field[i].checked = true;
						}
					}
					if (document.form_regle.select_all1.checked==false){
						field.checked=false;
						for (i=0;i<field.length;i++)
						{
						field[i].checked = false;
						}
					}
				}
				
				function coche2(field){
					if (document.form_var.select_all2.checked==true){
						field.checked=true;
						for (i=0;i<field.length;i++)
						{
						field[i].checked = true;
						}
					}
					if (document.form_var.select_all2.checked==false){
						field.checked=false;
						for (i=0;i<field.length;i++)
						{
						field[i].checked = false;
						}
					}
				}
				
				function MM_openBrWindow(theURL,winName,features) { //v2.0
				  window.open(theURL,winName,features);
				}
				//-->
			</script>
		</head>

	<body bgcolor="#FFFFFF" text="#000000">
		<?php
				include("menu_top_new.php");
		?>
	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						
		<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;CORRESPONDANCES</td>
		</tr>
		</table>
		<?php
		if (is_array($qry_liste_quest)){
			?>
			<form action="#" method="post" name="form1">
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="center" valign="top"> 
					<table border="0" cellspacing="0" cellpadding="0">
					  <tr> 
						<td class="TX">S&eacute;lectionnez un questionnaire existant :&nbsp;&nbsp;</td>
						<td> 
						  <select name="select_quest" class="form_ediht_Tarifs">
							<?php
							foreach($qry_liste_quest as $questionnaire){
								if ($questionnaire['quest_id'] == $_GET['questid']){
									$selected = ' selected="selected"';
								}else{
									$selected = '';
								}
								echo '<option value="'.$questionnaire['quest_id'].'"'.$selected.'>'.htmlentities($questionnaire['quest_nom']).'</option>';
							}
							?>
						  </select>
						</td>
						<td>&nbsp;&nbsp; 
						  
						</td>
					  </tr>
					</table>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
			  <br>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr><td align="center">
					<input type="submit" name="valid_1" value="Valider" class="BN">
				</td></tr>
			  </table>
			  </form>
			<br>
			<?php
		}
		if (@is_array($qry_quest)){
			?>
			<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo ucfirst($qry_quest[0]['quest_nom']) ?></td>
			</tr>
			</table> 
			  <table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
			<tr>
			  <td width="14"></td>
			  <td class="TX_Tarifs">Correspondances&nbsp;points&nbsp;/&nbsp;segments&nbsp;/&nbsp;blocs</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr>
			  <td width="14"></td>
			  <td class="TX" align="center">&nbsp;</td>
			  <td width="14"></td>
			</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX"> 
					<input type="button" name="Submit3" value="Ajouter une ligne" class="bn_ajouter" onClick="MM_openBrWindow('creation_regles.php?questid=<?php echo $_GET['questid'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=400')">
				  </td>
				  <td width="14"></td>
				</tr>
				<?php
				if (is_array($qry_quest_regle)){
				?>
				<form name="form_regle" action="#" method="post">
					<tr> 
					  <td width="14"></td>
					  <td align="left" class="TX"> 
					  <a name="1"></a>
						<table class="tabloGeneral">
						  <tr align="center"> 
							<td class="TX_bold" width="20%"><a href="correspondances.php?questid=<?php echo $_GET['questid'] ?>&order1=type_profil&order2=<?php echo $_GET['order2'] ?>#1">Type de profil</a></td>
							<td class="TX_bold" width="20%"><a href="correspondances.php?questid=<?php echo $_GET['questid'] ?>&order1=point&order2=<?php echo $_GET['order2'] ?>#1">Point</a></td>
							<td class="TX_bold" width="20%"><a href="correspondances.php?questid=<?php echo $_GET['questid'] ?>&order1=segment&order2=<?php echo $_GET['order2'] ?>#1">Segment</a></td>
							<td class="TX_bold" width="20%"><a href="correspondances.php?questid=<?php echo $_GET['questid'] ?>&order1=bloc&order2=<?php echo $_GET['order2'] ?>#1">Bloc</a></td>
							<td class="TX_bold" align="center" width="20%">Modifier</td>
							<td class="TX_bold" align="center" width="20%">Supprimer<br>
							<input type="checkbox" name="select_all1" onClick="coche1(document.form_regle.che_regle)" value="1">Tous</td>
						  </tr>
						<?php
						//print_r($qry_quest_regle);
						foreach($qry_quest_regle as $regle){
							?>
							<tr> 
								<td align="center" class="TX"><?php echo $regle['profil_lib'] ?></td>
								<td class="TX" align="center">De <?php if(substr($regle['cor_regle_points_inf'], 0, 1) == ',') echo '0'; echo $regle['cor_regle_points_inf'] ?> &agrave; <?php if(substr($regle['cor_regle_points_sup'], 0, 1) == ',') echo '0'; echo $regle['cor_regle_points_sup'] ?></td>
								<td class="TX" align="center"><?php echo $regle['cor_regle_segment'] ?></td>
								<td class="TX" align="center"><?php echo $regle['cor_regle_bloc'] ?></td>
								<td class="TX" align="center"><img  onmouseover="this.style.cursor='pointer'" onClick="MM_openBrWindow('edition_regles.php?questid=<?php echo $_GET['questid'] ?>&regleid=<?php echo $regle['cor_regle_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=400')" src="modifier.png" border="0" width="20"></td>
								<td class="TX" align="center"><input type="checkbox" name="che_regle[]" id="che_regle" value="<?php echo $regle['cor_regle_id']?>"></td>
							</tr>
							<?php
						}
						?>
						<tr><td colspan="6" align="right">&nbsp;<input class="bn_ajouter" type="button" value="Supprimer la sélection" onClick="delete_regle();"></td></tr>
						</table>
					  </td>
					  <td width="14"></td>
					</tr>
				</form>
				  <?php
				}else{
					echo '<tr><td colspan="3" class="TX" align="center"><?php echo $t_aucune_regle ?></td></tr>';
				}
				?>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
				<br>
			
			  <table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="TX_Tarifs">Positionnement&nbsp;graphique&nbsp;(point)&nbsp;des&nbsp;variables</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr>
			  <td width="14"></td>
			  <td class="TX" align="center">&nbsp;</td>
			  <td width="14"></td>
			</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX"> 
					<input type="button" name="Submit32" value="Ajouter une ligne" class="bn_ajouter" onClick="MM_openBrWindow('creation_points.php?questid=<?php echo $_GET['questid'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=400')">
				  </td>
				  <td width="14"></td>
				</tr>
				<?php
				if (is_array($qry_quest_var)){
					?>
					<form name="form_var" action="#" method="post">
					<tr> 
					  <td width="14"></td>
					  <td align="left" class="TX"> 
					  <a name="2"></a>
						<table class="tabloGeneral">
						  <tr align="center"> 
							<td class="TX_bold" width="10%"><a href="correspondances.php?questid=<?php echo $_GET['questid'] ?>&order1=<?php echo $_GET['order1'] ?>&order2=type_profil#2">Type de profil</a></td>
							<td class="TX_bold" width="10%"><a href="correspondances.php?questid=<?php echo $_GET['questid'] ?>&order1=<?php echo $_GET['order1'] ?>&order2=variable#2">Variable</a></td>
							<td class="TX_bold" width="20%"><a href="correspondances.php?questid=<?php echo $_GET['questid'] ?>&order1=<?php echo $_GET['order1'] ?>&order2=somme#2">Somme</a></td>
							<td class="TX_bold" width="20%"><a href="correspondances.php?questid=<?php echo $_GET['questid'] ?>&order1=<?php echo $_GET['order1'] ?>&order2=point#2">Point</a></td>
							<td class="TX_bold" align="center" width="20%">Modifier</td>
							<td class="TX_bold" align="center" width="20%">Supprimer<br>
							<input type="checkbox" name="select_all2" onClick="coche2(document.form_var.che_var)" value="1">Tous</td>
							</td>
						  </tr>
						  <?php
						  foreach($qry_quest_var as $var){
						  ?>
							  <tr> 
								<td align="center" class="TX"><?php echo $var['profil_lib'] ?></td>
								<td class="TX" align="center"><?php echo $var['var_lib'] ?></td>
								<td class="TX" align="center">
								<?php
									echo $var['cor_var_somme'];
								?>							
								</td>
								<td class="TX" align="center">
								<?php
								if ($var['cor_var_points']<1 && !is_int($var['cor_var_points']) && $var['cor_var_points']!='0'){
									echo '0'.$var['cor_var_points'];
								}else{
									echo $var['cor_var_points'];
								}								
								?>
								</td>
								<td class="TX" align="center"><img onMouseOver="this.style.cursor='pointer'" onClick="MM_openBrWindow('edition_points.php?questid=<?php echo $_GET['questid'] ?>&varid=<?php echo $var['cor_var_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=400')" src="modifier.png" border="0" width="20"></td>
								<td class="TX" align="center"><input type="checkbox" name="che_var[]" id="che_var" value="<?php echo $var['cor_var_id']?>"></td>
							  </tr>
						  <?php
						  }
						  ?>
						  <tr><td colspan="6" align="right">&nbsp;<input class="bn_ajouter" type="button" value="Supprimer la sélection" onClick="delete_var();"></td></tr>
						</table>
					  </td>
					  <td width="14"></td>
					</tr>
					</form>
					<?php
				}else{
					echo '<tr><td colspan="3" class="TX" align="center"><?php echo $t_aucune_variable ?></td></tr>';
				}
				?>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
		  <?php
	  }
	  ?>
</p></div>	</article></div>	</div>	</div>	</div>	  
	</body>
	</html>
<?php
	}else{
		include('no_acces.php');
	}
?>