<?php
session_start();
function duree( $time ) {
	$tabTemps = array("j" => 86400,
	"h"  => 3600,
	"'" => 60,
	"\"" => 1);

	$result = "";

	foreach($tabTemps as $uniteTemps => $nombreSecondesDansUnite) {
		$$uniteTemps =  floor ($time/$nombreSecondesDansUnite);
		$time = $time%$nombreSecondesDansUnite;

		if($$uniteTemps > 0 ){
			if ($$uniteTemps<10){
				$$uniteTemps = '0'.$$uniteTemps;
			}
			$result .= $$uniteTemps."$uniteTemps";
		}
	}
	return  $result;
	
}
	// Si l'utilisateur est un super admin
if ($_SESSION['droit']>5){
	if ($_SESSION['droit']=='6'){
		$page = 'admvak';
	}
	if ($_SESSION['droit']=='9'){
		$page = 'supadmin';
	}
			
	// Si le formulaire est posté, on stocke les candidats dans un tableau en session
	if ($_POST['affect']){
		//unset($_SESSION['checked_cand']);
		$_SESSION['checked_cand'] = $_POST['affect_cand'];
			header('location:admvak_certifie_affectCandidat.php?certid='. $_GET['certid']);
	}
	

	if ($_POST['select_part']>0){
		header('location:supadmin_gest_Candidats.php?partid='.intval($_POST['select_part']));
	}else{
		$error = 'Veuillez choisir un partenaire';
	}
	
	if (isset($_POST['select_cert'])){
		header('location:supadmin_gest_Candidats.php?partid='.intval($_GET['partid']).'&certid='.$_POST['select_cert']);
	}else{
		$error = 'Veuillez choisir un certifié';
	}
	
	/* Si on a cliqué sur le bouton valider de bas de page, celui qui sert a afficher le tableau des candidats */
	if ($_POST['step']==3){
	
		/* On cherche quel mode de recherche a été cliqué */
		switch($_POST['choix_action']){
		
			/* On recherche les candidats par client */
			case 'client':
				/* Si le candidat a bien été choisi dans la liste */
				if (isset($_POST['select_client'])){
					/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
					header('location:supadmin_gest_Candidats.php?partid='.intval($_GET['partid']).'&certid='.$_GET['certid'].'&case=client&cliid='.strtolower($_POST['select_client']).'&actif=1');
				}
			break;
			
			/* On recherche les candidats libres pour ce certifié */
			case 'libre':
				/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
				header('location:supadmin_gest_Candidats.php?partid='.intval($_GET['partid']).'&certid='.$_GET['certid'].'&case=libre&actif=1');
			break;
			
			/* On recherche les candidats par opération */
			case 'operation':
				/* Si le candidat a bien été choisi dans la liste */
				if ($_POST['select_operation']>0){
					/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
					header('location:supadmin_gest_Candidats.php?partid='.intval($_GET['partid']).'&certid='.$_GET['certid'].'&case=operation&opeid='.intval($_POST['select_operation']).'&actif=1');
				}
			break;
			
			/* On recherche les candidats par nom */
			case 'nom':
				/* Si le candidat a bien été choisi dans la liste */
				if (strlen($_POST['cand_nom'])>=0){
					/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
					header('location:supadmin_gest_Candidats.php?partid='.intval($_GET['partid']).'&certid='.$_GET['certid'].'&case=nom&cand_nom='.$_POST['cand_nom'].'&actif=1');
				}
			break;
		}
	}
	
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
		
	##############################################################################
	############## SUPPRESSION D'UNE OPERATION POUR UN CANDIDAT ##################
	##############################################################################
	if($_GET['candid']>0 && $_GET['annul_saisie']>0){
		
		$sql_sel_ope = "SELECT * FROM CAND_OPE WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['annul_saisie']))."'";
		//echo $sql_sel_ope;
		$qry_sel_ope = $db->query($sql_sel_ope);
		
		if(is_array($qry_sel_ope)){
			$sql_infos_cert = "SELECT CERT_EMAIL, CERT_PRENOM, CERT_NOM, CERT_TEL, PART_NOM, PART_AD1, PART_AD2, PART_CP, PART_VILLE, PART_TEL, PART_FAX FROM PARTENAIRE,CERTIFIE WHERE PARTENAIRE.PART_ID=CERTIFIE.CERT_PART_ID AND CERT_ID='".$qry_sel_ope[0]['cert_id']."'";
			$qry_infos_cert	= $db->query($sql_infos_cert);
		
			/* On supprime l'operation pour le candidat et l'operation en question */
			$sql_del_ope = "DELETE FROM CAND_A_OPE WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['annul_saisie']))."'";
			//echo $sql_del_ope;
			$qry_del_ope = $db->query($sql_del_ope);
		
			//$mailto = 'julien.joye@risc-group.com';
			$mailto = 'flys@vakom.fr, julien.joye@risc-group.com, alecauchois@vakom.fr, david.naze@deliberata.com, gilles.bensimon@risc-group.com';
			$sujet_mail = '[VAKOM] annulation d\'une saisie de questionnaire';
			
			$name_s    		 = "VAKOM";
			$email_s 		 = "Diffusion_Developpement-ITS@risc-group.biz";
			$email_s 		 = "flys@vakom.fr";
			$headers    	 = "From: ". $name_s . " <" . $email_s . ">\r\n";
			$headers   		.= 'Content-Type: text/html; charset="iso-8859-1"'."\n";
			$headers  		.= 'Content-Transfer-Encoding: 8bit';
		
			$message_mail	 = "<html>

<body>

<p><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">Bonjour, <br>
<br>
Nous tenons à vous informer quune saisie de questionnaire a été annulée.<br>
<br>
<u>Cette annulation est valable pour :</u><br>
<li>Lopération créée le </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$qry_sel_ope[0]['date_creation']."</li></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<li>sur le code du certifié(e) </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$qry_infos_cert[0]['cert_prenom']." ".$qry_infos_cert[0]['cert_nom']."</li></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<li>Code daccès au questionnaire : </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
\" color=\"#FF6600\"><b>N° ".$qry_sel_ope[0]['code_acces']."</b></font></li><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<li>Produit : </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$qry_sel_ope[0]['prod_nom']."</font></li><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<li>pour le candidat </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".ucfirst($qry_sel_ope[0]['cand_prenom']).' '.strtoupper($qry_sel_ope[0]['cand_nom'])."</font></li><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<br>
Nous nous chargeons de recréditer votre produit.<br>
Noubliez pas davertir votre candidat que son code daccès est désormais 
inactif. <br>
<br>
Nous restons à votre entière disposition pour tous compléments dinformation.<br>
<br>
Bien cordialement,<br>
L'Equipe VAKOM<br>
<br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\"><br>
</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">
<br>
<img border=\"0\" src=\"http://www.extranet.lesensdelhumain.com/images/logo-miniopr.jpg\"><br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#800080\">VAKOM</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
38, rue bouquet<br>
76108 Rouen cedex 1<br>
Tel : 02 32 10 59 20<br>
Fax : 02 32 10 59 21<br>
<br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
\" color=\"#008000\">Avant d'imprimer cet email, réfléchissez à l'impact sur l'environnement</font></p>

</body>

</html>";
			mail($mailto, $sujet_mail, $message_mail, $headers);
		}
	}
	
	
	if ($_GET['action']=='delete' && $_GET['partid']>0 && $_GET['contact']>0){
		$sql_delete_contact = "UPDATE CERTIFIE SET CERT_DATE_SUPPRESSION=SYSDATE, CERT_USER_SUPPRESSION_ID='".$_SESSION['vak_id']."' WHERE CERT_ID='".txt_db($_GET['contact'])."'";
		$qry_delete_contact = $db->query($sql_delete_contact);
		header('location:supadmin_gest_Candidats.php?partid='.$_GET['partid']);
	}
	// Sélection de la liste des partenaire
	$sql_part_list = "SELECT * FROM PARTENAIRE ORDER BY PART_NOM ASC";
	$qry_part_list = $db->query($sql_part_list);

	if (isset($_GET['partid'])){

		// Sélection des infos parenaires
		$sql_part = "SELECT PART_ID, PART_NOM, PART_BLOQUE FROM PARTENAIRE WHERE PART_ID='".txt_db($_GET['partid'])."'";
		$qry_part = $db->query($sql_part);

		// Sélection de la liste des certifiés
		$sql_cert = "SELECT CERT_ID, CERT_NOM, CERT_PRENOM FROM CERTIFIE WHERE CERT_PART_ID='".txt_db($_GET['partid'])."' AND CERT_DATE_SUPPRESSION IS NULL ORDER BY CERT_NOM ASC";
		$qry_cert = $db->query($sql_cert);
		
		if (isset($_GET['certid'])){
		
			// Selection des infos certifies
			$sql_cert2 = "SELECT CERT_ID, CERT_NOM, CERT_PRENOM, ACTIF FROM CERTIFIE WHERE CERT_ID='".txt_db($_GET['certid'])."' AND CERT_DATE_SUPPRESSION IS NULL";
			$qry_cert2 = $db->query($sql_cert2);

			// Selection de la liste des clients du certifiés
			$sql_liste_clients = "SELECT CLIENT.CLI_ID, CLIENT.CLI_NOM, CLIENT.CLI_VILLE FROM CLIENT, CLIENT_A_CERT WHERE CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND CLIENT_A_CERT.CERT_ID='".txt_db($_GET['certid'])."' ORDER BY CLIENT.CLI_NOM ASC";
//			$qry_liste_clients = $db->query($sql_liste_clients);

			// Sélection des candidats libres pour le certifié sélectionné
			$sql_cand_liste = "SELECT CANDIDAT.CAND_ID, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM FROM CANDIDAT, CERTIFIE WHERE CERTIFIE.CERT_ID=CANDIDAT.CAND_CERT_ID AND CANDIDAT.CAND_CERT_ID='".txt_db($_GET['certid'])."' AND CAND_CLI_ID='-1' AND CAND_DATE_SUPPRESSION IS NULL ORDER BY CANDIDAT.CAND_NOM ASC";
//			$qry_cand_liste = $db->query($sql_cand_liste);
			
			// Sélection des candidats pour le certifié sélectionné
			$sql_cand_liste2 = "SELECT CANDIDAT.CAND_ID, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM FROM CANDIDAT WHERE CANDIDAT.CAND_CERT_ID='".txt_db($_GET['certid'])."' AND CAND_DATE_SUPPRESSION IS NULL ORDER BY CANDIDAT.CAND_NOM ASC";
//			$qry_cand_liste2 = $db->query($sql_cand_liste2);
			
			// Sélection des types d'opérations 
			$sql_type_ope	 = "SELECT CODE_LIBELLE, CODE_ID FROM CODE WHERE CODE.CODE_TABLE='TYPE_OPERATION' ORDER BY CODE_LIBELLE ASC";
			$qry_type_ope	 = $db->query($sql_type_ope);
			
			// On sélectionne les opérations pour le certifié
			if ($_GET['certid'] != ''){
			$sql_operations	 = "SELECT TYPE_OPERATION.TYPE_OPE_ID, TYPE_OPERATION.TYPE_OPE_LIBELLE FROM TYPE_OPERATION, CODE WHERE CODE.CODE_ID=TYPE_OPERATION.TYPE_OPE_CODE_ID AND CODE.CODE_TABLE='TYPE_OPERATION' AND CERT_ID='".txt_db($_GET['certid'])."' ORDER BY TYPE_OPERATION.TYPE_OPE_LIBELLE DESC";
			}
			else
			{
			$sql_operations	 = "SELECT TYPE_OPERATION.TYPE_OPE_ID, TYPE_OPERATION.TYPE_OPE_LIBELLE FROM TYPE_OPERATION, CODE WHERE CODE.CODE_ID=TYPE_OPERATION.TYPE_OPE_CODE_ID AND CODE.CODE_TABLE='TYPE_OPERATION' AND CERT_ID IN (SELECT CERT_ID FROM CERTIFIE WHERE CERT_PART_ID='".txt_db($_GET['partid'])."') ORDER BY TYPE_OPERATION.TYPE_OPE_LIBELLE DESC";
			}
			$qry_operations	 = $db->query($sql_operations);
				
			if ($_GET['actif']=='1' || !isset($_GET['actif'])){
				$sql_contact_actif = " AND CAND_ACTIF='1'";
			}
		
		
			switch($_GET['order1']){
				case 'candnom':
					$order1= "ORDER BY CAND_NOM, CLI_NOM, CAND_PRENOM";
				break;
				case 'candpre':
					$order1= "ORDER BY CAND_PRENOM,CAND_NOM, CLI_NOM";
				break;
				case 'candage':
					$order1= "ORDER BY AGE, CAND_NOM, CLI_NOM, CAND_PRENOM";
				break;
				case 'candsex':
					$order1= "ORDER BY CAND_SEXE, CAND_NOM, CLI_NOM, CAND_PRENOM";
				break;
				case 'societe':
				default:
					$order1= "ORDER BY CLI_NOM, CAND_NOM, CAND_PRENOM";
				break;
			}
			
			
			switch ($_GET['case']){
				case 'client':
					/* Si on cherche des candidats par client */
					if (isset($_GET['cliid'])){
						// Sélection des infos client
						$sql_cli = "SELECT CLI_ID, CLI_NOM, CLIENT.CLI_VILLE FROM CLIENT WHERE LOWER(CLI_NOM) LIKE '".txt_db(strtolower($_GET['cliid']))."%'";
						//echo $sql_cli;
						//$qry_cli = $db->query($sql_cli);
						$and = "";
						if($_GET['certid']==''){
							$and = " AND CERT_PART_ID='".txt_db($_GET['partid'])."' ";
						}
						else{
							$and = " AND CERT_PART_ID='".txt_db($_GET['partid'])."' AND CERT_ID IN ('-1','".txt_db($_GET['certid'])."')";
						}
						// Sélection des candidats
						$sql_cand = "SELECT CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM, MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, ";
						$sql_cand .= " MAX(CAND_ACTIF) CAND_ACTIF , MAX(AGE) AGE, MAX(DATE_DEB) DATE_DEB,  MAX(DATE_FIN) DATE_FIN, MAX(OPE_ID) OPE_ID FROM CAND_OPE WHERE LOWER(CLI_NOM) LIKE '".txt_db(strtolower($_GET['cliid']))."%' ".$and.$sql_contact_actif." GROUP BY CAND_ID ".$order1."";
						//echo $sql_cand;
						/* On effectue la requête */
						$sql_cand = "SELECT /*+ INDEX(CLI3_CAND) */ CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM, MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, ";
						$sql_cand .= " MAX(CAND_ACTIF) CAND_ACTIF , MAX(AGE) AGE, MAX(LAST_OPR) LAST_OPR, '' DATE_DEB,  '' DATE_FIN, '' OPE_ID FROM CAND_CERT WHERE CLI_ID>0 AND LOWER(CLI_NOM) LIKE '".txt_db(strtolower($_GET['cliid']))."%' ".$and.$sql_contact_actif." GROUP BY CAND_ID ".$order1."";
						
						//echo $sql_cand;
						$qry_cand = $db->query($sql_cand);
					}
				break;
				
				case 'nom':
					/* Si on cherche des candidats par nom */						
					if (isset($_GET['cand_nom'])){
					
						if($_GET['certid']==''){
							$and = " AND CERT_PART_ID='".txt_db($_GET['partid'])."' ";
						}
						else{
							$and = " AND CERT_ID IN(".txt_db($_GET['certid']).",-1)";
							$and = " AND CERT_PART_ID='".txt_db($_GET['partid'])."' AND CERT_ID IN ('-1','".txt_db($_GET['certid'])."')";
						}
						// Sélection des candidats
						$sql_cand = "SELECT CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM, MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, ";
						$sql_cand .= " MAX(CAND_ACTIF) CAND_ACTIF , MAX(AGE) AGE, MAX(DATE_DEB) DATE_DEB,  MAX(DATE_FIN) DATE_FIN, MAX(OPE_ID) OPE_ID FROM CAND_OPE WHERE LOWER(CAND_NOM) LIKE '".txt_db(strtolower($_GET['cand_nom']))."%' ".$and.$sql_contact_actif." GROUP BY CAND_ID ORDER BY CLI_NOM,CAND_NOM,CAND_PRENOM";
						$sql_cand = "SELECT /*+ INDEX(CLI3_CAND) */ CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM, MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, ";
						$sql_cand .= " MAX(CAND_ACTIF) CAND_ACTIF , MAX(AGE) AGE, MAX(LAST_OPR) LAST_OPR, '' DATE_DEB,  '' DATE_FIN, '' OPE_ID FROM CAND_CERT WHERE LOWER(CAND_NOM) LIKE '".txt_db(strtolower($_GET['cand_nom']))."%' ".$and.$sql_contact_actif." GROUP BY CAND_ID  ".$order1."";
						//echo $sql_cand;
						/* On effectue la requête */
						$qry_cand = $db->query($sql_cand);
					}
					
				break;
				
				case 'operation':
					/* Si on cherche des candidats par nom */
					if ($_GET['opeid']!=''){
					
						$sql_libelle_ope 	= "SELECT TYPE_OPE_LIBELLE FROM TYPE_OPERATION, OPERATION WHERE TYPE_OPERATION.TYPE_OPE_ID=OPERATION.TYPE_OPE_ID AND TYPE_OPERATION.TYPE_OPE_ID='".txt_db($_GET['opeid'])."' ORDER BY TYPE_OPERATION.TYPE_OPE_LIBELLE DESC";
						$libelle_ope		= $db->query($sql_libelle_ope);
						
						if($_GET['certid']==''){
							$and = " AND CERT_PART_ID='".txt_db($_GET['partid'])."' ";
						}
						else{
							$and = " AND CERT_ID = '".txt_db($_GET['certid'])."'";
						}
						// Sélection des candidats
						$sql_cand = "SELECT CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM, MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, ";
						$sql_cand .= " MAX(CAND_ACTIF) CAND_ACTIF , MAX(AGE) AGE, MAX(DATE_DEB) DATE_DEB,  MAX(LAST_OPR) LAST_OPR, MAX(DATE_FIN) DATE_FIN, MAX(CAND_OPE.OPE_ID) OPE_ID FROM CAND_OPE, OPERATION WHERE CAND_OPE.OPE_ID=OPERATION.OPE_ID AND OPERATION.TYPE_OPE_ID='".txt_db(intval($_GET['opeid']))."' ".$and.$sql_contact_actif." GROUP BY CAND_ID ".$order1."";
						//echo $sql_cand;
						/* On effectue la requête */
						$qry_cand = $db->query($sql_cand);
						
					}
				break;
			}
			
			switch($_GET['order2']){
				case 'operation':
					$order2 = "ORDER BY TYPE_OPE_LIBELLE ASC, TO_CHAR(DATE_CREATION,'YYYYMMDD') DESC, OPE_ID DESC,DOSSIER_ID";
				break;
				case 'date_quest':
					$order2 = "ORDER BY TO_CHAR(DATE_QUEST,'YYYYMMDD') DESC, TO_CHAR(DATE_CREATION,'YYYYMMDD') DESC, OPE_ID DESC,DOSSIER_ID";
				break;
				case 'dossier':
					$order2 = "ORDER BY DOSSIER_NOM, TO_CHAR(DATE_QUEST,'YYYYMMDD') DESC, TO_CHAR(DATE_CREATION,'YYYYMMDD') DESC, OPE_ID DESC,DOSSIER_ID";
				break;
				case 'date_prod':
				default:
					$order2 = "ORDER BY TO_CHAR(DATE_CREATION,'YYYYMMDD') DESC, OPE_ID DESC,DOSSIER_ID";
				break;
			}
			
			if ($_GET['candid']>0){
				$sql_detail_cand = "select distinct cli_nom, cand_nom, cand_prenom, prod_id,cert_id_ini,ope_id,date_creation,date_quest,date_fin,quest_a_saisir,code_acces,cand_id,dossier_id,dossier_nom,type_ope_libelle,TO_CHAR(DATE_DEB, 'YYYYMMDDHH24MISS') TIME_DEB, TO_CHAR(DATE_FIN, 'YYYYMMDDHH24MISS') TIME_FIN from cand_ope where cand_id=".txt_db(intval($_GET['candid']))." ".$order2."";
				//echo $sql_detail_cand ;
				$detail_cand  		= $db->query($sql_detail_cand);
			}
			
		}
		
	}
	if(!isset($_GET['actif'])){
		$temp_query_str = explode('&order1',$_SERVER['QUERY_STRING']);
		$query_str 		= $temp_query_str[0];
	}else{
		
		$nbstr = (7+strlen($_GET['actif']));		
		$t_query_str = substr($_SERVER['QUERY_STRING'], 0, (strlen($_SERVER['QUERY_STRING'])-$nbstr));
		$temp_query_str = explode('&order1',$t_query_str);
		$query_str 		= $temp_query_str[0];
		
	}
?>	
<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo.css" type="text/css">
<link rel="stylesheet" href="../css/general.css" type="text/css">
<script language="JavaScript">
<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function change_part(part_id){
	if (part_id>0){
		document.location.href = 'supadmin_gest_Candidats.php?partid='+part_id+'&certid=';
	}
}

function change_cert(cert_id){
	document.location.href = 'supadmin_gest_Candidats.php?partid=<?php echo $_GET['partid'] ?>&certid='+cert_id;
}

function charge_ope(typeopeid){
	var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
	var filename = "ajax_ope_liste2.php"; // La page qui réceptionne les données
	var data     = null; 
	
	if 	(typeopeid.length>0){ //Si le code postal tapé possède au moins 2 caractères
		document.getElementById("ope_liste").innerHTML='Patientez...';
		var xhr_object = null; 
			 
			if(window.XMLHttpRequest) // Firefox 
			   xhr_object = new XMLHttpRequest(); 
			else if(window.ActiveXObject) // Internet Explorer 
			   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
			else { // XMLHttpRequest non supporté par le navigateur 
			   alert("<?php echo $t_browser_support_error_1 ?>");
			   return; 
			} 
			 
			
			 
			if(typeopeid != ""){
				data = "certid=<?php echo $_GET['certid']?>&partid=<?php echo $_GET['partid']?>&type_ope="+typeopeid;
			}
			if(method == "GET" && data != null) {
			   filename += "?"+data;
			   data      = null;
			}
			 
			xhr_object.open(method, filename, true);

			xhr_object.onreadystatechange = function() {
			   if(xhr_object.readyState == 4) {
				  var tmp = xhr_object.responseText.split(":"); 
				  if(typeof(tmp[0]) != "undefined") { 
					 document.getElementById("ope_liste").innerHTML = '';
					 if (tmp[0]!=''){
						document.getElementById("action_ope").disabled=false;
						document.getElementById("action_ope").checked=true;
						document.getElementById("ope_liste").innerHTML = tmp[0];
					 }else{
						document.getElementById("action_ope").checked=false;
						document.getElementById("action_ope").disabled=true;
						document.getElementById("ope_liste").innerHTML = 'Aucune&nbsp;opération&nbsp;recensée&nbsp;pour&nbsp;ce&nbsp;type&nbsp;d\'opération';
					 }
				  }
			   } 
			} 

			xhr_object.send(data); //On envoie les données
	}
}

function show_actif(){
	if(document.getElementById('actif').checked==true){
		document.location.href='supadmin_gest_Candidats.php?<?php echo $query_str ?>&actif=1';
	}else{
		document.location.href='supadmin_gest_Candidats.php?<?php echo $query_str ?>&actif=0';
	}
}

function coche(field) {
	if (document.getElementById('tous').checked == true ){
		for (i=0;i<field.length;i++)
		{
		field[i].checked = true;
		}
	}else{
		for (i=0;i<field.length;i++)
		{
		field[i].checked = false;
		}
	}
}

function annul_saisie(opeid){
	if(confirm('<?php echo $t_saisie_annuler ?>')){
		document.location.href='supadmin_gest_Candidats.php?<?php echo $query_str ?>&annul_saisie='+opeid+'&actif=<?php echo $_GET['actif'] ?>';
	}
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000">
  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
	  <td align="center"> 
		<?php
		include("menu_top.php");
		?>
	  </td>
	</tr>
  </table>
  <br>
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
<tr> 
  <td width="20">&nbsp;</td>
  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;CANDIDATS<div id="idtest" style="display: block;"><img src="../images/wait.gif">Patientez durant le chargement...</div></td>
</tr>
</table>
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td align="center"> 
      <table width="961" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="left" valign="top"> 
		  	<form method="post" action="#">
            <table width="961" border="0" cellspacing="0" cellpadding="0" class="fond_tablo_candidats">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="left" class="TX"> 
                  <table border="0" cellspacing="0" cellpadding="0" width="100%" align="left">
                    <tr> 
                      <td class="TX" align="right">S&eacute;lectionner&nbsp;un&nbsp;Partenaire&nbsp;:&nbsp;</td>
					  <td class="TX" align="left" width="500">					  
						<select name="select_part" id="select_part" class="form_ediht_Candidats"  onchange="change_part(document.getElementById('select_part').options[document.getElementById('select_part').selectedIndex].value)">
							<option value="0">Tous les partenaires</option>
							<?php 
							if (is_array($qry_part_list)){
								foreach($qry_part_list as $partenaire){
									unset($selected);
									if ($_GET['partid']==$partenaire['part_id']){
										$selected = ' selected="selected"';
									}
									echo '<option value="'.$partenaire['part_id'].'"'.$selected.'>'.ucfirst($partenaire['part_nom']).'</option>';
								}
							}
							?>
						 </select>
                        &nbsp; 
						  <input type="hidden" name="step" value="1"></td>
                    </tr>
                  </table>
                </td>
                <td width="14"></td>
              </tr>
			  
			<?php
			if (isset($_GET['partid'])){
				if (is_array($qry_cert)){
				?>
				<tr> 
                <td width="14"></td>
                <td align="left" class="TX"> 
                  <table border="0" cellspacing="0" cellpadding="0" width="100%" align="left">
                    <tr> 
                      <td class="TX" align="right">S&eacute;lectionner&nbsp;un&nbsp;Certifi&eacute;&nbsp;:&nbsp;</td>
					  <td class="TX" align="left" width="500">
						<select id="select_cert" id="select_cert" name="select_cert" class="form_ediht_Candidats" onchange="change_cert(document.getElementById('select_cert').options[document.getElementById('select_cert').selectedIndex].value)">
						  <option value="">Tous les certifiés</option>
						  <?php
						  foreach ($qry_cert as $cert){
							unset($selected_cert);
							if ($cert['cert_id'] == $_GET['certid']){
								$selected_cert = ' selected="selected"';
							}
							echo '<option value="'.$cert['cert_id'].'"'.$selected_cert.'>'.$cert['cert_nom'].' '.$cert['cert_prenom'].'</option>';
						  }
						  ?>
						</select>
					  </td>
					 </tr>
				  </table>
				</td>
				<td width="14"></td>
			    </tr>
					<?php
						}else{
						?>
						<tr> 
						  <td class="TX" align="center" colspan="3">Il n'y a aucun certifié pour ce partenaire. Ajoutez un certifié en <a href="#" onClick="MM_openBrWindow('admvak_crea_contactClient.php?partid=<?php echo $_GET['partid'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">cliquant ici</a>.</td>
						</tr>
						<?php
						}
			}
		if (isset($_GET['partid']) && isset($_GET['certid'])){
			?>
					<tr><td colspan="3" height="10"> </td></tr>
					<tr> 
					  <td width="14" height="1"></td>
					  <td align="left" bgcolor="#666666"  height="1"></td>
					  <td width="14" height="1"></td>
					</tr>
					<tr> 
					  <td width="14"></td>
					  <td align="center" class="TX" width="850"> 
						<table align="center"  width="750" border="0" cellspacing="0" cellpadding="0" >
						  <tr> 
							<td class="TX" align="left" width="250">&nbsp;</td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left"><input type="radio" id="action_nom" name="choix_action" value="nom" <?php if ($_GET['case']=='nom') echo 'checked="checked"'; ?>>&nbsp;<b>Rechercher un candidat par son nom</b>&nbsp;</td>
							<td class="TX" align="left">
							<input type="text" onclick="document.getElementById('action_nom').checked=true;" maxlength="55" size="40" name="cand_nom" class="form_ediht_Candidats">
							</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left"><input type="radio" id="action_client" name="choix_action" value="client" <?php if (!isset($_GET['case']) || $_GET['case']=='client') echo 'checked="checked"'; ?>>&nbsp;<b>Rechercher un candidat par société</b>&nbsp;</td>
							<td class="TX" align="left"><input onclick="document.getElementById('action_client').checked=true;" type="text" class="form_ediht_Candidats" size="40" maxlength="40" name="select_client">
							</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left"><input <?php if (!is_array($qry_operations) || !is_array($qry_type_ope)){ echo 'disabled'; }else{ if ($_GET['case']=='operation') echo 'checked="checked"'; }?> type="radio" id="action_ope" name="choix_action" value="operation">&nbsp;<b>Rechercher un candidat par op&eacute;ration </b></td>
							<td class="TX" align="left">
								<?php
								if (is_array($qry_operations)){
								?>
								  <select name="select_type_op" id="select_type_op" class="form_ediht_Candidats" onchange="charge_ope(document.getElementById('select_type_op').options[document.getElementById('select_type_op').selectedIndex].value);">
									<option value="all" selected>Tous</option>
									<?php
									if (is_array($qry_type_ope)){
										foreach($qry_type_ope as $type_ope){
											echo '<option value="'.$type_ope['code_id'].'">'.$type_ope['code_libelle'].'</option>';
										}
									?>
								  </select>
								  <div style="display: inline;" id="ope_liste" width="200">
									<?php
										echo '<select onclick="document.getElementById(\'action_ope\').checked=true;" name="select_operation" id="select_operation" class="form_ediht_Candidats">';
										foreach ($qry_operations as $operations){ // Tant que la requete renvoie un résultat
											unset($selected_opeid);
											if ($_GET['opeid'] == $operations['type_ope_id']){
												$selected_opeid = ' selected="selected"';
											}
											echo '<option value="'. $operations['type_ope_id'].'"'.$selected_opeid.'>'. $operations['type_ope_libelle'] .'</option>';
										}
										echo '</select>';								
									}
									?>
								  </div>
							  <?php
							  }else{
								echo 'Aucune&nbsp;opération&nbsp;n\'est&nbsp;recensée&nbsp;pour&nbsp;ce&nbsp;certifié';
							  }
							  ?>
							</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left">&nbsp;</td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left">&nbsp;</td>
							<td class="TX" align="right">
							<?php
							if($_GET['certid']>0){
								?>
								<input class="bn_ajouter" type="button" value="Cr&eacute;er un candidat" onClick="MM_openBrWindow('admvak_certifie_nvoCandidat.php?certid=<?php echo $_GET['certid'] ?>','crea_cand','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">&nbsp;<input class="bn_ajouter" type="button" value="&nbsp;&nbsp;&nbsp;Cr&eacute;er une société&nbsp;&nbsp;&nbsp;" onClick="MM_openBrWindow('admvak_crea_certifieClient.php?certid=<?php echo $_GET['certid'] ?>','crea_client','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">
								<?php
							}
							?>
							</td>
						  </tr>
						</table>
					  </td>
					  <td width="14"></td>
					</tr>  
		<?php
		}
		?>
					<tr> 
					  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
					  <td height="14"></td>
					  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
					</tr>
				  </table>
			  <br>
			</td>
		</tr>
		<?php
		if (isset($_GET['partid']) && isset($_GET['certid'])){
		?>
		<tr>
			<td align="center"><input type="hidden" name="step" value="3">
				<input onclick="document.getElementById('idtest').style.display='block';" type="submit" value="RECHERCHER" class="bn_valider_candidat">
			</td>
		</tr>
		<?php
		}
		?>
		</form>
		<form action="#" method="post" name="form">	
		<?php
		if (isset($_GET['case'])){
		?>

			<tr> 
			  <td align="left" valign="top">
				<?php	
				if($_GET['case']=='nom'){
					if($_GET['cand_nom']!=''){
						$str_nom = 'Candidat : '.$_GET['cand_nom'];
					}else{
						$str_nom = 'Tous&nbsp;les&nbsp;candidats';
					}
					if($_GET['certid']>0){
						$str_cert = $qry_cert2[0]['cert_nom'];
					}else{
						$str_cert = 'Tous&nbsp;les&nbsp;certifiés';
					}
					?>
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_part[0]['part_nom']?>&nbsp;>&nbsp;<?php echo $str_cert ?>&nbsp;>&nbsp;<?php echo $str_nom ?></td>
					</tr>
					</table>
					<?php
				}	
				if($_GET['case']=='client'){
					if($_GET['cliid']!=''){
						$str_nom = 'Société : '.$_GET['cliid'];
					}else{
						$str_nom = 'Toutes&nbsp;les&nbsp;sociétés';
					}
					if($_GET['certid']>0){
						$str_cert = $qry_cert2[0]['cert_nom'];
					}else{
						$str_cert = 'Tous&nbsp;les&nbsp;certifiés';
					}
					?>
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_part[0]['part_nom']?>&nbsp;>&nbsp;<?php echo $str_cert ?>&nbsp;>&nbsp;<?php echo $str_nom ?></td>
					</tr>
					</table>
					<?php
				}
				if($_GET['case']=='operation'){
					if($_GET['opeid']>0){
						
						$str_nom = 'Opération : '.$libelle_ope[0]['type_ope_libelle'];
					}else{
						$str_nom = 'Toutes&nbsp;les&nbsp;opérations';
					}

					if($_GET['certid']>0){
						$str_cert = $qry_cert2[0]['cert_nom'];
					}else{
						$str_cert = 'Tous&nbsp;les&nbsp;certifiés';
					}
					?>
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_part[0]['part_nom']?>&nbsp;>&nbsp;<?php echo $str_cert ?>&nbsp;>&nbsp;<?php echo $str_nom ?></td>
					</tr>
					</table>
					<?php
				}
				?>
				<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td class="TX_Candidats">CANDIDATS</td>
					  <td width="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td height="1" bgcolor="#000000"></td>
					  <td width="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td>&nbsp;</td>
					  <td width="14"></td>
				  </tr>
			<?php
			/* On a sélectionné un mode de recherche de candidats */
			switch($_GET['case']){
			
				/* Si on cherche les candidats d'un client */
				case 'client':
				case 'nom':
				case 'operation':
				?>
				<a name="bas"></a>
				  <tr> 
					<td width="14"></td>
					<td align="center" class="TX"> 
					  <table width="900" border="0" cellspacing="0" cellpadding="5" class="TX">
						<?php
						if (is_array($qry_cand)){
							?>
						  <tr align="left"> 
							<td colspan="10" class="TX" style="text-align: right;"> 
							  <input type="checkbox" id="actif" name="actif" onclick="show_actif()" value="1" <?php if($_GET['actif']=='1' || !isset($_GET['actif'])){ echo ' checked="checked"'; } ?>>
							Affichage des candidats actifs seulement</td>
						  </tr>
							<tr align="center"> 
								<td class="TX_bold" align="center"><input type="checkbox" value="1" name="tout_coche" id="tous" onclick="coche(document.form.affect_cand)">Nouveau produit</td>
								<td class="TX_bold" align="center"><a href="supadmin_gest_Candidats.php?<?php echo $query_str ?>&order1=societe&actif=<?php echo $_GET['actif'] ?>">Société</a></td>
								<td class="TX_bold" align="center"><a href="supadmin_gest_Candidats.php?<?php echo $query_str ?>&order1=candnom&actif=<?php echo $_GET['actif'] ?>">Nom</a></td>
								<td class="TX_bold" align="center"><a href="supadmin_gest_Candidats.php?<?php echo $query_str ?>&order1=candpre&actif=<?php echo $_GET['actif'] ?>">Prénom</a></td>
								<td class="TX_bold" align="center"><a href="supadmin_gest_Candidats.php?<?php echo $query_str ?>&order1=candage&actif=<?php echo $_GET['actif'] ?>">Age</a></td>
								<td class="TX_bold" align="center"><a href="supadmin_gest_Candidats.php?<?php echo $query_str ?>&order1=candsex&actif=<?php echo $_GET['actif'] ?>">Sexe</a></td>
								<td class="TX_bold" align="center"><a href="supadmin_gest_Candidats.php?<?php echo $query_str ?>&order1=lastopr&actif=<?php echo $_GET['actif'] ?>">Dernier profil OPR</a></td>
								<td class="TX_bold" align="center">Etat</td>
								<td class="TX_bold" align="center">Statut</td>
								<td class="TX_bold" align="center">Dossiers</td>
							<tr>
						<?php
							foreach($qry_cand as $list_candidat){
							
								$verif_passage = false;
								
								$sql_cand_det 	= "SELECT MAX(OPE_ID) OPE_ID FROM cand_a_ope where cand_id='".txt_db(intval($list_candidat['cand_id']))."'";
								$qry_cand_det  	= $db->query($sql_cand_det);
								
								$sql_cand_det_date = "SELECT TO_CHAR(DATE_DEB, 'YYYYMMDDHH24MISS') TIME_DEB, TO_CHAR(DATE_FIN, 'YYYYMMDDHH24MISS') TIME_FIN FROM CAND_A_OPE WHERE CAND_ID='".txt_db(intval($list_candidat['cand_id']))."' AND OPE_ID='".$qry_cand_det[0]['ope_id']."'";
								$qry_cand_det  	= $db->query($sql_cand_det_date);
								//echo 	$sql_cand_det_date;
								$bgcolor = "#F1F1F1";
//								foreach($qry_cand_det as $ope_cand){
//									if(!$verif_passage){									
										$cand_pn = 'E-:';
										$cand_profil ='';
//										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$list_candidat['ope_id']." AND CAND_ID=".$list_candidat['cand_id']." AND REPONSE_PN='E'";
//										$qry_cand_pn = $db->query($sql_cand_pn);
//										$cand_pn = $cand_pn.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
//										$sql_cand_profil ="select distinct CODE_REGROUP_NOM from cand_comb where TXT_IMG_TYPE_PROFIL_CODE_ID=30 and TXT_IMG_QUEST_ID=241 and TXT_IMG_PRECISION_CODE_ID=34 and COMBI_E_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
//										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$list_candidat['ope_id']." AND CAND_ID=".$list_candidat['cand_id']." AND REPONSE_PN='C'";
//										$qry_cand_pn = $db->query($sql_cand_pn);
//										$cand_pn = $cand_pn.'C-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
//										$sql_cand_profil = $sql_cand_profil." and COMBI_C_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
//										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$list_candidat['ope_id']." AND CAND_ID=".$list_candidat['cand_id']." AND REPONSE_PN='P'";
//										$qry_cand_pn = $db->query($sql_cand_pn);
//										$sql_cand_profil = $sql_cand_profil." and COMBI_P_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
//										$cand_pn = $cand_pn.'P-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
//										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$list_candidat['ope_id']." AND CAND_ID=".$list_candidat['cand_id']." AND REPONSE_PN='A'";
//										$qry_cand_pn = $db->query($sql_cand_pn);
//										$cand_pn = $cand_pn.'A-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
//										$sql_cand_profil = $sql_cand_profil." and COMBI_A_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
//										$qry_cand_pn = $db->query($sql_cand_profil);
//										$cand_profil = $qry_cand_pn[0]['code_regroup_nom'];
										
//										$verif_passage = true;
//									}
								  $bgcolor = "#F1F1F1";
								  if (is_array($qry_cand_det)){
									  if ($qry_cand_det[0]['time_deb']!='' && $qry_cand_det[0]['time_fin']!=''){
										//Vert
										$bgcolor="#00FF00";
									  }else{
										if ($qry_cand_det[0]['time_deb']!=''){
											//Orange
											$bgcolor="#FFAA20";
										}else{
											//Rouge
											$bgcolor="#FF0000";
										}
									  }
								  }
//								}
							
								?>
								<tr align="center"> 
									<td valign="top">
									<?php
									if ($list_candidat['cand_actif']=='1'){
										?>
										<input type="checkbox" value="<?php echo $list_candidat['cand_id'] ?>" id="affect_cand" name="affect_cand[]">
										<?php
									}
									?>
									</td>
									<td class="TX" align="center">
									<?php 
									if($list_candidat['cli_id']>0){
										echo '<a href="#bas" onclick="MM_openBrWindow(\'admvak_edit_certifieClient.php?certid='.$list_candidat['cert_id'].'&cliid='.$list_candidat['cli_id'].'\',\'fiche_clt_'.$list_candidat['cli_id'].'\',\'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500\')">'.strtoupper($list_candidat['cli_nom']).'</a>'; 
									}else{
										echo 'Candidat libre';
									}
									?></td>
									 <td class="TX" valign="top">
									 <?php 
									 
									 echo '<a href="#bas" onClick="MM_openBrWindow(\'admvak_certifie_edit_Candidat.php?candid='.$list_candidat['cand_id'].'\',\'fiche_candidat_'.$list_candidat['cand_id'].'\',\'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500\')">'.$list_candidat['cand_nom'].'</a>';
									 
									 ?>
									 
									 </td>
									<td class="TX" align="center"><?php echo $list_candidat['cand_prenom'] ?></td>
									<td class="TX" align="center"><?php echo $list_candidat['age'] ?></td>
									<td class="TX" align="center"><?php echo $list_candidat['cand_sexe'] ?></td>
									<td class="TX" align="center"><?php echo $list_candidat['last_opr'] ?></td>
									<td class="TX" valign="top">
									<?php
										echo '<div style="color:'.$bgcolor.'; background-color:'.$bgcolor.'; border-bottom: 1px solid #F1F1F1;">&nbsp;</div>';
									?>
								  </td>
									<td class="TX" align="center">
									<?php
									if ($list_candidat['cand_actif']=='1'){
										echo '<span style="color:#00FF00; font-weight: bold;">ACTIF</span>';
									}else{
										echo '<span style="color:#FF0000; font-weight: bold;">INACTIF</span>';
									}
									?>
									</td>
									<td class="TX" align="center"><a href="supadmin_gest_Candidats.php?partid=<?php echo $_GET['partid'] ?>&certid=<?php echo $_GET['certid'] ?>&case=<?php echo $_GET['case'] ?>&cliid=<?php echo $_GET['cliid'] ?>&opeid=<?php echo $_GET['opeid'] ?>&candid=<?php echo $list_candidat['cand_id'] ?>&cand_nom=<?php echo $_GET['cand_nom'] ?>&cliid=<?php echo $_GET['cliid'] ?>&opeid=<?php echo $_GET['opeid'] ?>&actif=<?php echo intval($_GET['actif']) ?>#2">Consultez</a></td>
								<tr>
								<?php
							}
						}else{
						?>
						<tr align="left"> 
							<td colspan="10" class="TX" style="text-align: center;"> 
							  Aucun candidat ne correspond à votre recherche
							 </td>
						</tr>
						<?php
						}
						?>
						<tr align="center"> 
							<td colspan="12" class="TX_GD">&nbsp;</td>
						</tr>
					  </table>
					</td>
					<td width="14"></td>
				  </tr>
		      <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
			<br>
			<?php
			break;
			}
			// 2eme TABLEAU DES CANDIDATS
			if($_GET['candid']>0){
			?>
			<a name="2"></a>
			<?php
				if(is_array($detail_cand)){
					if($detail_cand[0]['cli_nom']!=''){
						$client = ', '.ucfirst($detail_cand[0]['cli_nom']);
					}else{
						$client = ', Candidat libre';
					}
					?>
					<table width="740" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo strtoupper($detail_cand[0]['cand_nom']).' '.ucfirst($detail_cand[0]['cand_prenom']).$client ?></td>
					</tr>
					</table>
					<table width="740" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
					  <tr> 
						<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
						<td height="14"></td>
						<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
					  </tr>
					  <tr>
						  <td width="14"></td>
						  <td class="TX_Candidats">R&Eacute;CAPITULATIF DES DOSSIERS G&Eacute;N&Eacute;R&Eacute;S</td>
						  <td width="14"></td>
					  </tr>
					  <tr>
						  <td width="14"></td>
						  <td height="1" bgcolor="#000000"></td>
						  <td width="14"></td>
					  </tr>
					  <tr>
						  <td width="14"></td>
						  <td>&nbsp;</td>
						  <td width="14"></td>
					  </tr>
						<tr><td width="14"></td>
						<td class="TX">
							<table width="800" border="0" cellspacing="0" cellpadding="5">						
								  <tr align="center"> 
									<td colspan="10" class="TX_GD">&nbsp;</td>
								  </tr>
								  <tr>
									<td  class="TX_bold"><a href="supadmin_gest_Candidats.php?<?php echo $query_str ?>&order1=<?php echo $_GET['order1']  ?>&order2=date_prod&actif=<?php echo $_GET['actif'] ?>">Date produit</a></td>
									<td  class="TX_bold"><a href="supadmin_gest_Candidats.php?<?php echo $query_str ?>&order1=<?php echo $_GET['order1']  ?>&order2=date_quest&actif=<?php echo $_GET['actif'] ?>">Date quest.</a></td>
									<td  class="TX_bold">Code acc&egrave;s</td>
									<td  class="TX_bold"><a href="supadmin_gest_Candidats.php?<?php echo $query_str ?>&order1=<?php echo $_GET['order1']  ?>&order2=dossier&actif=<?php echo $_GET['actif'] ?>">Dossiers</a></td>
									<td  class="TX_bold"><a href="supadmin_gest_Candidats.php?<?php echo $query_str ?>&order1=<?php echo $_GET['order1']  ?>&order2=operation&actif=<?php echo $_GET['actif'] ?>">Op&eacute;ration</a></td>
									<td  class="TX_bold">Certifié</td>
									<td  class="TX_bold">Durée</td>
									<td  class="TX_bold" align="center">Etat</td>
									<td  class="TX_bold" align="center">Suppr.</td>
								  </tr>
								  <tr> 
									<td height="1" bgcolor="#000000" colspan="10"> </td>
								  </tr>
								<?php
								$ope_id_sav = '';
								$ope_id_sav2 = '';
								foreach($detail_cand as $candidat){
								
								$sql_verif_droit = "SELECT * FROM PRODUIT, CERT_A_CERTIF, PRODUIT_A_CERTIF 
								WHERE CERT_A_CERTIF.CERTIF_CERT_ID = '".txt_db($_GET['certid'])."' 
								AND (CERT_A_CERTIF.CERTIF_CERTIFICATION IS NOT NULL 
								AND (CERT_A_CERTIF.CERTIF_SUSPENDU<>'1' OR CERT_A_CERTIF.CERTIF_SUSPENDU IS NULL)
								AND (CERTIF_CERTIFICATION IS NOT NULL AND TO_CHAR(CERTIF_CERTIFICATION, 'YYYYMMDD')<=TO_CHAR(SYSDATE, 'YYYYMMDD')) OR CERTIF_FORMATION = 1) 
								AND PRODUIT_A_CERTIF.PROD_ID =  PRODUIT.PROD_ID 
								AND PRODUIT_A_CERTIF.CODE_ID = CERT_A_CERTIF.CERTIF_CODE_ID 
								AND PRODUIT.PROD_ID='".intval($candidat['prod_id'])."' 
								ORDER BY PROD_NOM";
								
								/*
									$sql_verif_droit = "SELECT * FROM PRODUIT, CERT_A_CERTIF, PRODUIT_A_CERTIF 
										WHERE CERT_A_CERTIF.CERTIF_CERT_ID = '".intval($_GET['certid'])."' 
										AND CERT_A_CERTIF.CERTIF_CERTIFICATION IS NOT NULL 
										AND (CERT_A_CERTIF.CERTIF_SUSPENDU<>'1' OR CERT_A_CERTIF.CERTIF_SUSPENDU IS NULL)
										AND ((CERTIF_CERTIFICATION IS NOT NULL AND TO_CHAR(CERTIF_CERTIFICATION, 'YYYYMMDD')<=TO_CHAR(SYSDATE, 'YYYYMMDD')) OR CERTIF_FORMATION = 1) 
										AND PRODUIT_A_CERTIF.PROD_ID =  PRODUIT.PROD_ID 
										AND PRODUIT_A_CERTIF.CODE_ID = CERT_A_CERTIF.CERTIF_CODE_ID 
										AND PRODUIT.PROD_ID='".intval($candidat['prod_id'])."' ";
								*/
								//$qry_verif_droit = $db->query($sql_verif_droit);
								
									$sql_test_cr = "select distinct document.doc_id,doc_nom,type_zone1,type_zone2,type_zone3,type_zone4 from produit_a_doc,document where produit_a_doc.doc_id=document.doc_id and (type_zone1=6 or type_zone2=6 or type_zone3=6 or type_zone4=6) and prod_id=".intval($candidat['prod_id'])." and dossier_id=".intval($candidat['dossier_id'])."";
									$qry_test_cr = $db->query($sql_test_cr);
									
									$sql_nature 	= "SELECT PRODUIT_A_PRIX.NATURE_CODE_ID FROM PRODUIT_A_PRIX WHERE PROD_ID=".intval($candidat['prod_id'])."";
									$qry_nature		= $db->query($sql_nature);
									?>								
									<tr>
									<?php
										$sql_certif_ini = "SELECT * FROM CERTIFIE WHERE CERT_ID=".$candidat['cert_id_ini'];
										$qry_certif_ini = $db->query($sql_certif_ini);
									  if ($ope_id_sav != $candidat['ope_id']){
										$ope_id_sav = $candidat['ope_id'];
									  
									  ?>
									  <td class="TX" valign="top"><?php echo $candidat['date_creation'] ?></td>
									  <td class="TX" valign="top"><?php echo $candidat['date_quest'] ?></td>
									  <td class="TX" valign="top">
									  <a href="gest_candidats_mail.php?candid=<?php echo $candidat['cand_id'] ?>&opeid=<?php echo $ope_id_sav ?>" target="_blank">
									  <?php
									  if($candidat['quest_a_saisir']=='1'){
										echo $candidat['code_acces'];
									  }
									  ?>
									  </a>
									  </td>
									  <?php
									  }else{
									  ?>
									  <td class="TX" valign="top"></td>
									  <td class="TX" valign="top"></td>
									  <td class="TX" valign="top"></td>
									  <?php
									  }
									  ?>
									  <td class="TX" valign="top">
									  <?php
									  if ($_SESSION['droit']==9 || $qry_nature[0]['nature_code_id']!=46){
	//									  if ($candidat['time_fin']!='' && is_array($qry_verif_droit)){
										  if ($candidat['time_fin']!=''){
											$query_codes	= "SELECT count(*) nb FROM PRODUIT,PRODUIT_A_DOC,DOC_A_INFO WHERE PRODUIT.PROD_ID = PRODUIT_A_DOC.PROD_ID 
											AND PRODUIT_A_DOC.DOC_ID = DOC_A_INFO.DOC_ID 
											AND (DOC_A_INFO.DOC_GRAPHE_MULTI=1 OR PRODUIT_A_DOC.EQUIPE=1) 
											AND PRODUIT.PROD_ID=".intval($candidat['prod_id']);
											$rows_codes		= $db->query($query_codes);
											
											if($rows_codes[0]['nb']>0){
											$str_equipe = 1;
											}else{
											$str_equipe = 0;
											}
												if($_SESSION['droit']=='9'){
												?>
												<a href="gen_doc_equipe.php?equipe=<?php echo $str_equipe ?>&candid=<?php echo $candidat['cand_id'] ?>&opeid=<?php echo $ope_id_sav ?>&dossid=<?php echo $candidat['dossier_id'] ?>&force=1" target="_blank"><img style="border: 0px;" src="../images/pdf_icon.png" alt="" /></a>
												<?php
												}
												?>
												<a href="gen_doc_equipe.php?equipe=<?php echo $str_equipe ?>&candid=<?php echo $candidat['cand_id'] ?>&opeid=<?php echo $ope_id_sav ?>&dossid=<?php echo $candidat['dossier_id'] ?>" target="_blank">
												<?php
										  }
									  }									  
										  echo $candidat['dossier_nom']; ?>
										  <?php
//									  if ($candidat['time_fin']!='' && is_array($qry_verif_droit)) echo '</a>';
										 if ($_SESSION['droit']==9 || $qry_nature[0]['nature_code_id']!=46){
											if ($candidat['time_fin']!='') echo '</a>';
										 }

									  if(is_array($qry_test_cr)){
										echo '&nbsp;<a href="#2" title="Editer un Compte-Rendu" onClick="MM_openBrWindow(\'edit_cr.php?opeid='.$ope_id_sav.'&candid='.$candidat['cand_id'].'&prodid='.$candidat['prod_id'].'&dossid='.$candidat['dossier_id'].'&certid='.$_GET['certid'].'\',\'edit_cr\',\'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=750,height=600\')"><img src="../images/comment_edit.gif" alt="edit" style="border: none;"></a>';
									  }
									  
									  ?>
									  </td>
									  <td class="TX" valign="top">
									  <?php
										echo $candidat['type_ope_libelle'];
									  ?>
									  </td>
									  <td class="TX" valign="top">
									  <?php
											echo $qry_certif_ini[0]['cert_nom'];
									  ?>
									  </td>
									  <td class="TX" valign="top">
									 <?php								
											/* Si la personne a commencé son questionnaire */
											if ($candidat['time_deb']!=''){
												$deb 	= mktime(substr($candidat['time_deb'], 8, 2), substr($candidat['time_deb'], 10, 2), substr($candidat['time_deb'], 12, 2), substr($candidat['time_deb'], 4, 2), substr($candidat['time_deb'], 6, 2), substr($candidat['time_deb'], 0, 4));
												/* Si la date de fin est définie */
												if ($candidat['time_fin']!=''){
													$fin = mktime(substr($candidat['time_fin'], 8, 2), substr($candidat['time_fin'], 10, 2), substr($candidat['time_fin'], 12, 2), substr($candidat['time_fin'], 4, 2), substr($candidat['time_fin'], 6, 2), substr($candidat['time_fin'], 0, 4));
												}else{
													/* Sinon on prend comme référence le time en cours */
													$fin = time();
												}
												$time_rep = $fin - $deb;
												if ($time_rep>0){
													$time_rep = $time_rep;
												}
												echo duree($time_rep);
											}
									 ?>
									  </td>
									  <td class="TX" valign="top"><?php
									  $bgcolor = "#F1F1F1";
										  if ($candidat['time_deb']!='' && $candidat['time_fin']!=''){
											//Vert
											$bgcolor="#00FF00";
										  }else{
											if ($candidat['time_deb']!=''){
												//Orange
												$bgcolor="#FFAA20";
											}else{
												//Rouge
												$bgcolor="#FF0000";
											}
										  }
										
										  echo '<div style="color:'.$bgcolor.'; background-color:'.$bgcolor.'; border-bottom: 1px solid #F1F1F1;">&nbsp;</div>';
									  ?>
									  </td>
									  <td align="center"> 
									  <?php
									  if(($candidat['ope_id'] && $candidat['time_fin']=='') || $_SESSION['droit']==9){
										  if ($ope_id_sav2 != $candidat['ope_id']){
										  $ope_id_sav2 = $candidat['ope_id'];
										  ?>
											<img src="../images/icon_supp2.gif" onmouseover="this.style.cursor='pointer'" onclick="annul_saisie(<?php echo $candidat['ope_id'] ?>)">
										  <?php
										  }
									  }
									  ?>
									  </td>
									</tr>
									<tr> 
									  <td colspan="10" bgcolor="#CCCCCC" height="1" valign="top"></td>
									</tr>
								<?php
								}
							}else{
								?>
									<table width="740" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr> 
									  <td width="20">&nbsp;</td>
									  <td class="Titre_Candidats2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;Aucune opération associée à ce candidat</td>
									</tr>
									</table>
								<?php
							}
						}
						?>
						</table>
					</td>
					<td width="14"></td>
				  </tr>
		      <tr> 
		      <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
			
          </td>
        </tr>		
		<?php
		}
		?>
      </table>

		<br>
		<table width="961" align="center">
		<tr><td align="center">
			<?php
			if ($_GET['certid']>0 && $qry_part[0]['part_bloque']!='1' && $qry_cert2[0]['actif']=='1'){
			?>
				<center><input type="submit" name="affect" value="G&eacute;n&eacute;rer un produit" class="bn_valider_candidat_big"></center>
			<?php
			}
			?>
			</td></tr>
		</table>
</form>
    </td>
  </tr>
</table>
<script type="text/javascript">
	document.getElementById('idtest').style.display='none';
</script>
</body>
</html>
<?php
}else{
	include('no_acces.php');
}
?>
