<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
		
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	?>
	<html>
	<head>
	<title>Vakom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<script language="JavaScript">
	<!--
	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
	<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
	  <tr> 
		<td> 
		   <table width="961" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td width="360"><img src="../images/top2.jpg" width="389" height="121"></td>
			  <td>&nbsp;&nbsp;</td>
			  <td><img src="../images/pap1.jpg" width="30" height="30" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="questionnaire.php" class="menu_Gris">Créer les questionnaires</a><br>
				<img src="../images/pap2.jpg" width="30" height="50" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="regles.php" class="menu_Gris">Créer les règles</a> <br>
				<img src="../images/pap4.jpg" width="30" height="30" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="documents.php" class="menu_Gris">Créer vos documents</a><br></td>
			  <td valign="middle">
			  </td>
			  <td valign="middle"><img src="../images/pap5.jpg" width="30" height="30" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="question.php" class="menu_Gris">Diffuser les questionnaires</a><br>
				<img src="../images/pap3.jpg" width="30" height="50" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="resultats.php" class="menu_Gris">Suivre les résultats</a> 
				<br>
				<img src="../images/pap6.jpg" width="30" height="30" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="analyser.php" class="menu_Gris">Analyser les résultats</a></td>
			</tr>
		  </table>
		</td>
	  </tr>
	  <tr> 
		<td>&nbsp;</td>
	  </tr>
	  <tr> 
		<td align="center" class="menu_Gris">&nbsp;</td>
	  </tr>
	  <tr> 
		<td align="right"> 
		  <table width="961" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td width="180" valign="top" align="center"><font color="EA98AA" class="TX"><b>LISTE DES DOCUMENTS
				</b> </font></td>
			  <td align="left" valign="top"> 
				<table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
				  <tr> 
					<td width="14"></td>
					<td align="center" class="TX"> 
					  <table border="1" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="100%">
						<tr align="center" bgcolor="#C4C4C4"> 
						  <td class="TX">Nom</td>
						  <td class="TX">Langue</td>
						  <td class="TX">Date création - Auteur</td>
						  <td class="TX">Date modification - Auteur</td>
						  <td class="TX" align="center">Modifier</td>
						  <td class="TX" align="center">Supprimer</td>
						</tr>
						<tr > 
						  <td class="TX" align="center">Profil naturel</td>
						  <td class="TX" align="center">Anglais</td>
						  <td class="TX" align="center">19/03/2009 - Vakom</td>
						  <td class="TX">&nbsp;</td>
						  <td class="TX" align="center"><img src="modifier.png" border="0" width="20"></td>
						  <td class="TX" align="center"><img src="supprimer.jpg" border="0" width="10"></td>
						</tr>
					  </table>
					</td>
					<td width="14"></td>
				  </tr>
				  <tr> 
					<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				  </tr>
				</table>
			  </td>
			</tr>
			<tr> 
			  <td width="180" valign="top">&nbsp;</td>
			  <td align="left" valign="top">&nbsp;</td>
			</tr>
			<tr> 
			  <td width="180" valign="top" align="center"><font color="EA98AA" class="TX"><b>CR&Eacute;ER UN DOCUMENT</b> 
				</font></td>
			  <td align="left" valign="top"> 
				<table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
				  <tr> 
					<td width="14"></td>
					<td align="center" class="TX"> 
					  
					  <table width="600" border="0" cellspacing="0" cellpadding="0">
					   <tr> 
						  <td class="TX">Nom</td>
						  <td colspan="2"> 
							<input type="text" name="nom" size="50" class="form_ediht">
						  </td>
						</tr>
						<tr> 
						  <td width="180" valign="top">&nbsp;</td>
						  <td align="left" valign="top">&nbsp;</td>
						</tr>
						<tr> 
						  <td class="TX">Langue</td>
						  <td colspan="2"> 
							<select name="langue">
								<option>Allemand</option>
							  <option>Anglais</option>
							  <option>Espagnol</option>
							  <option selected>Fran&ccedil;ais</option>
							  <option>Italien</option>
							</select>
						  </td>
						</tr>
						<tr> 
						  <td width="180" valign="top">&nbsp;</td>
						  <td align="left" valign="top">&nbsp;</td>
						</tr>
						<tr> 
							<td  class="TX">Contenu</td>
						  <td align="center"> <img src="fckeditor.jpg" width="670"></td>
						</tr>
						<tr bgcolor="F1F1F1"> 
						  <td height="2"></td>
						</tr>
						<tr> 
						  <td class="TX">&nbsp;</td>
						</tr>

					  </table>
					</td>
					<td width="14"></td>
				  </tr>
				  <tr> 
					<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				  </tr>
				</table>
				<p>&nbsp;</p>
			  </td>
			</tr>
		  </table>
		</td>
	  </tr>
	  <tr> 
		<td align="center">
		  <input type="button" name="Submit" value="Valider" class="BN" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
		</td>
	  </tr>
	  <tr> 
		<td align="right" width="180">&nbsp; </td>
	  </tr>
	</table>
	</body>
	</html>
<?php
}else{
	include('no_acces.php');
}
?>