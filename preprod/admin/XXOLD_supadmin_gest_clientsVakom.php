<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	if (isset($_POST['bn_part'])){

		if ($_POST['bn_part']=='create'){
			?>
			<script type="text/javascript">
				window.open('admvak_crea_client.php','crea_client','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes');
				document.location.href='supadmin_gest_clientsVakom.php?idnc=012';
			</script>
			<?php
		}else{
			if (trim($_POST['search_part'])!='' || trim($_GET['search_part'])!=''){
				if($_POST['search_part']){
					$search_part = $_POST['search_part'];
				}else{
					$search_part = $_GET['search_part'];
				}
				$where = "AND LOWER(PART_NOM) LIKE '".txt_db(strtolower($search_part))."%' AND PART_BLOQUE = '".intval($_GET['bloque'])."'";
			}
			$sql_part_list = "SELECT * FROM PARTENAIRE, CODE WHERE CODE.CODE_TABLE='NATURE' AND CODE.CODE_ID=PARTENAIRE.PART_NATURE_CODE_ID ".$where."ORDER BY PART_NOM ASC";
			$qry_part_list = $db->query($sql_part_list);

			if(is_array($qry_part_list)){
				// Si il n'y a qu'un résultat
				if(!is_array($qry_part_list[1])){
					header('location:supadmin_gest_clientsVakom.php?partid='.$qry_part_list[0]['part_id']);
				}else{
					$multi_rep = true;
				}
			}else{
				header('location:supadmin_gest_clientsVakom.php?idnc='.($_GET['idnc']+1));
			}
		}
	}


	

	
	if ($_GET['partid']!=''){
		$sql_part = "SELECT * FROM PARTENAIRE WHERE PART_ID='".txt_db($_GET['partid'])."'";
		$qry_part = $db->query($sql_part);
	}

	if ($_GET['prodid']!='' && $_GET['typid']!='' && $_GET['partid']!=''){
		
		$sql_jet = "SELECT JETON.*, 
			CODE.CODE_LIBELLE TYPE_JETON, 
			PRODUIT.* 
			FROM JETON,PARTENAIRE,PRODUIT,CODE CODE 
			WHERE JETON.JET_TYPE_JETON_CODE_ID=CODE.CODE_ID 
			AND CODE.CODE_TABLE='TYPE_JETON' 
			AND JETON.JET_PROD_ID=PRODUIT.PROD_ID 
			AND PROD_ID='".txt_db($_GET['prodid'])."'
			AND CODE.CODE_ID='".txt_db($_GET['typid'])."'
			AND JET_PART_ID='".txt_db($_GET['partid'])."'
			AND JET_PART_ID=PARTENAIRE.PART_ID";
			//echo $sql_jet;
		$qry_jet = $db->query($sql_jet);
	}
	?>
	<html>
	<head>
	<title>Vakom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<script language="JavaScript">
	<!--
	function show_bloque(){
		if(document.getElementById('idpart_bloque').checked==true){
			document.location.href='supadmin_gest_clientsVakom.php?search_part=<?php echo $search_part ?>&bloque=1';
		}else{
			document.location.href='supadmin_gest_clientsVakom.php?search_part=<?php echo $search_part ?>&bloque=0';
		}
	}
	
	function show_actif(){
		if(document.getElementById('jet_actif').checked==true){
			document.location.href='supadmin_gest_clientsVakom.php?prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&partid=<?php echo $_GET['partid'] ?>&actif=1#bas';
		}else{
			document.location.href='supadmin_gest_clientsVakom.php?prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&partid=<?php echo $_GET['partid'] ?>&actif=0#bas';
		}
	}
	
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  //window.open(theURL,winName,features);
	  window.open(theURL,winName,features).focus();	  
	}

	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
	  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td align="center"> 
			<?php
				include("menu_top.php");
			?>
		  </td>
		</tr>
	  </table>
	  <br>
	<form method="post" action="supadmin_gest_clientsVakom.php">
	  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
		  <td width="20">&nbsp;</td>
		  <td width="250" class="Titre_Partenaires"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;PARTENAIRES</td>
		  <td align="center">&nbsp;</td>
		</tr>
	  </table>
	  <table width="961" border="0" cellspacing="0" cellpadding="0" class="fond_tablo_partenaires" align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr> 
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX"> 
			<table border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td class="TX" align="left">&nbsp;</td>
				<td class="TX" align="left">&nbsp;</td>
			  </tr>
			  <tr> 
				<td class="TX" align="left"> 
				  <input checked="checked" type="radio" name="bn_part" value="select">
				  &nbsp;Saisir&nbsp;le&nbsp;nom&nbsp;d'un&nbsp;partenaire&nbsp;:&nbsp;&nbsp; </td>
				<td class="TX" align="left"> 
					<input type="text" name="search_part" size="50" class="form_ediht_Partenaires">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" align="left"> 
				  <input type="radio" name="bn_part" value="create">
				  &nbsp;Cr&eacute;er un nouveau partenaire &nbsp;</td>
				<td class="TX" align="left">&nbsp; </td>
			  </tr>
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	  <br>
	  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr><td align="center">
		<input type="submit" name="submit1" value="VALIDER" class="bn_valider_partenaire">
		</td></tr>
	  </table>
	  <br>
	</form>
		<?php
		if (is_array($qry_part)){ // Si on a choisi un partenaire
			$sql_jeton = "SELECT PROD_ID, PROD_NOM, CODE_LIBELLE, CODE_ID, Sum(JET_NOMBRE) JET_NOMBRE_SUM 
					FROM JETON,PRODUIT,CODE
					WHERE JETON.JET_PROD_ID = PRODUIT.PROD_ID AND JET_TYPE_JETON_CODE_ID = CODE.CODE_ID AND CODE_TABLE='TYPE_JETON' AND JET_PART_ID='".txt_db($_GET['partid'])."'
					GROUP BY PROD_ID, PROD_NOM, CODE_LIBELLE, CODE_ID";

			$sql_jeton = "select distinct prod_nom,JET_PROD_ID prod_id,JET_TYPE_JETON_CODE_ID code_id,code_libelle,code_ordre from jeton,produit,code
			where jeton.JET_PROD_ID=produit.prod_id
			and jeton.JET_TYPE_JETON_CODE_ID=code.code_id and code.code_table='TYPE_JETON'
			and jet_part_id='".txt_db($_GET['partid'])."' order by prod_nom,code_ordre";
			//echo $sql_jeton;
			$qry_jeton = $db->query($sql_jeton);
		?>
	  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Partenaires"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<a class="Titre_Partenaires" onClick="MM_openBrWindow('admvak_edit_client.php?partid=<?php echo $qry_part[0]['part_id'] ?>','Vakom_Fiche_Partenaire_<?php echo $qry_part[0]['part_id'] ?>','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=400')" href="#"><?php echo htmlentities($qry_part[0]['part_nom']) ?></a></td>
		</tr>
	  </table>
	  <table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
	<tr> 
		  <td width="14"></td>
		  <td class="TX_Partenaires">SYNTH&Egrave;SE DES JETONS ATTRIBU&Eacute;S</td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td bgcolor="#666666" height="1"></td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX">&nbsp;</td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX"> 
			<table cellpadding="0" cellspacing="0" border="0" align="center" width="900">
				<tr><td style="text-align: left;" class="TX">
					<input type="button" name="new_pdt2" value="Ajouter" class="bn_ajouter" onClick="MM_openBrWindow('admvak_ajout_produit.php?partid=<?php echo addslashes($_GET['partid']) ?>','ajout_jeton','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes')">
				</td><td style="text-align: right;" class="TX">
					<input type="checkbox" name="jet_actif" id="jet_actif" onclick="show_actif();" value="1" <?php if($_GET['actif']==1 || $_GET['actif']=='' || !isset($_GET['actif'])){ echo ' checked="checked"'; }?>>Affichage des jetons actifs uniquement
				</td></tr>
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX"> 
			<table width="900" border="0" cellspacing="0" cellpadding="2" class="TX">
			  <tr align="center"> 
				<td colspan="5" class="TX_GD"></td>
			  </tr>
			  <tr align="center"> 
				<td height="1" colspan="5" bgcolor="#000000" ></td>
			  </tr>
			  <tr> 
				<td class="TX_bold">Produit</td>
				<td class="TX_bold" align="center">Type de jetons</td>
				<td class="TX_bold" align="center">Jetons disponibles</td>
				<td class="TX_bold" align="center">Jetons affect&eacute;s <br>disponibles</td>
				<td class="TX_bold" align="center"> 
				  <p>Jetons non affect&eacute;s <br>disponibles</p>
				</td>
			  </tr>
			  <tr> 
				<td height="1" bgcolor="#000000"></td>
				<td height="1" bgcolor="#000000"></td>
				<td height="1" bgcolor="#000000"></td>
				<td height="1" bgcolor="#000000"></td>
				<td height="1" bgcolor="#000000"></td>
			  </tr>
			  <?php
			  if (is_array($qry_jeton)){
				$prod_id_sav = '';
				foreach($qry_jeton as $jeton){
				/* Calcul des jetons affectés à tous les certifiés du partenaire pour le produit et le type donné */
				if ($_GET['actif'] == 1 || $_GET['actif'] == '' || !isset($_GET['actif'])){
				$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".$qry_part[0]['part_id']."'
				and jet_prod_id='".$jeton['prod_id']."'
				and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
				and TO_char(JET_DEB_VALIDITE,'YYYYMMDD')<=to_char(SYSDATE,'YYYYMMDD') and TO_char(JET_FIN_VALIDITE,'YYYYMMDD')>=to_char(SYSDATE,'YYYYMMDD') and cert_id=-1";
				$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
				$qry_CJA_dispo_aff = $qry_CJA[0]['affecte'];				
				$qry_CJA_dispo_uti = $qry_CJA[0]['utilise'];				
				$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".$qry_part[0]['part_id']."'
				and jet_prod_id='".$jeton['prod_id']."'
				and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
				and TO_char(JET_DEB_VALIDITE,'YYYYMMDD')<=to_char(SYSDATE,'YYYYMMDD') and TO_char(JET_FIN_VALIDITE,'YYYYMMDD')>=to_char(SYSDATE,'YYYYMMDD') and cert_id<>-1";
				$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
				$qry_CJA_aff_aff = $qry_CJA[0]['affecte'];				
				$qry_CJA_aff_uti = $qry_CJA[0]['utilise'];				
				}
				else
				{
				$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".$qry_part[0]['part_id']."'
				and jet_prod_id='".$jeton['prod_id']."'
				and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
				and cert_id=-1";
				//echo $sql_calcul_jetons_affectes;
				$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
				$qry_CJA_dispo_aff = $qry_CJA[0]['affecte'];				
				$qry_CJA_dispo_uti = $qry_CJA[0]['utilise'];				
				$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".$qry_part[0]['part_id']."'
				and jet_prod_id='".$jeton['prod_id']."'
				and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
				and cert_id<>-1";
				//echo $sql_calcul_jetons_affectes;
				$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
				$qry_CJA_aff_aff = $qry_CJA[0]['affecte'];				
				$qry_CJA_aff_uti = $qry_CJA[0]['utilise'];				
				}
				?>
				  <tr> 
					<td><a href="supadmin_gest_clientsVakom.php?partid=<?php echo $_GET['partid'] ?>&prodid=<?php echo $jeton['prod_id'] ?>&typid=<?php echo $jeton['code_id'] ?>&actif=<?php echo $_GET['actif'] ?>#bas"><?php echo ucfirst($jeton['prod_nom']) ?></a></td>
					<td align="center"><?php echo ucfirst($jeton['code_libelle']) ?></td>
					<td align="center"><?php echo $qry_CJA_dispo_aff +  $qry_CJA_aff_aff - $qry_CJA_dispo_uti - $qry_CJA_aff_uti ?>/<?php echo $qry_CJA_dispo_aff +  $qry_CJA_aff_aff?></td>
					<td align="center"><?php echo $qry_CJA_aff_aff - $qry_CJA_aff_uti ?>/<?php echo $qry_CJA_aff_aff?></td>					
					<td align="center"><?php echo $qry_CJA_dispo_aff - $qry_CJA_dispo_uti ?>/<?php echo $qry_CJA_dispo_aff ?></td>					
				  </tr>
				  <tr> 
					<td colspan="5" bgcolor="#CCCCCC" height="1"></td>
				  </tr>
				  <?php
				}
			  }
			  ?>
			  </table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	  <br>
	  <?php
		if (is_array($qry_jet)){ // Si on a cliqué sur un jeton ou si le partenaire n'a encore aucun jeton
		  ?>
		  <a name="#bas"></a>
		  <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Partenaires2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_jet[0]['prod_nom'].' - '.$qry_jet[0]['type_jeton'] ?></td>
			</tr>
		  </table>
		  <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="520" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="TX_Partenaires">R&Eacute;CAPITULATIF DES ATTRIBUTIONS DE JETONS</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr>
			  <td width="14"></td>
			  <td class="TX" align="center">&nbsp;</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"><a	name="table_jet"></a></td>
			  <td align="center" valign="top" class="TX"> 
				<table width="450" border="0" cellspacing="0" cellpadding="2" class="TX">
				  <tr align="center"> 
					<td height="1" colspan="7" bgcolor="#000000" ></td>
				  </tr>
				  <tr valign="middle"> 
					<td class="TX_bold"><a href="supadmin_gest_clientsVakom.php?partid=<?php echo $_GET['partid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&actif=<?php echo $_GET['actif'] ?>&order=date_attrib#table_jet">Date d'attribution</a></td>
					<td class="TX_bold" align="center"><a href="supadmin_gest_clientsVakom.php?partid=<?php echo $_GET['partid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&actif=<?php echo $_GET['actif'] ?>&order=nbre_jet#table_jet">Nombre de Jetons</a></td>
					<td class="TX_bold" align="center"><a href="supadmin_gest_clientsVakom.php?partid=<?php echo $_GET['partid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&actif=<?php echo $_GET['actif'] ?>&order=date_deb#table_jet">Date d&eacute;but <br>
						validit&eacute;</a>
					</td>
					<td class="TX_bold" align="center"><a href="supadmin_gest_clientsVakom.php?partid=<?php echo $_GET['partid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&actif=<?php echo $_GET['actif'] ?>&order=date_fin#table_jet">Date fin <br>
					  validit&eacute;</a></td>
					<td class="TX_bold" align="center">Commentaires</td>
				  </tr>
				  <tr> 
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
				  </tr>
				  <?php
				  if (is_array($qry_jet)){
					  foreach($qry_jet as $jet){
						?>
						  <tr> 
							<td><a href="#" onClick="MM_openBrWindow('admvak_edit_produit.php?jetid=<?php echo addslashes($jet['jet_id']) ?>','edit_jeton','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes')"><?php echo $jet['jet_date_attribution'] ?></a></td>
							<td align="center"><?php echo $jet['jet_nombre'] ?></td>
							<td align="center"><?php echo $jet['jet_deb_validite'] ?></td>
							<td align="center"><?php echo $jet['jet_fin_validite'] ?></td>
							<td align="center"><?php echo $jet['jet_comm1'].'<br>'.$jet['jet_comm2'] ?></td>
						  </tr>
						  <tr> 
							<td colspan="7" bgcolor="#CCCCCC" height="1"></td>
						  </tr>
						  <?php
					  }
				  }
				  ?>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
		  <?php
		}
	  }else{
		if ($multi_rep){
			?>
		<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Partenaires"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;
			  <?php
			  if (trim($search_part) != ''){
				echo htmlentities($search_part);
			  }else{
				echo 'TOUS&nbsp;LES&nbsp;PARTENAIRES';
			  }
			  ?>
			  </td>
			</tr>
		</table>
		<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX"> 
			<table cellpadding="0" cellspacing="0" border="0" align="center" width="900">
				<tr><td style="text-align: left;" class="TX">
				&nbsp;</td><td style="text-align: right;" class="TX">
					<input type="checkbox" name="idpart_bloque" id="idpart_bloque" onclick="show_bloque();" value="1" <?php if($_GET['bloque']==1 || $_GET['bloque']=='' || !isset($_GET['bloque'])){ echo ' checked="checked"'; }?>>Affichage des partenaires actifs seulement
				</td></tr>
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="TX_Partenaires">PARTENAIRES</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="center" class="TX">
			  &nbsp;</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="center" class="TX">
				<table width="900" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
					<tr>
						<td class="TX_bold">Nom_</td>
						<td class="TX_bold">Raison sociale</td>
						<td class="TX_bold">Code postal</td>
						<td class="TX_bold">Ville</td>
						<td class="TX_bold">Nature</td>
						<td class="TX_bold">Certifiés actifs</td>
						<td class="TX_bold">Etat</td>
					</tr>
					<?php
					foreach ($qry_part_list as $elmt){
						echo '<tr>
							<td class="TX"><a href="supadmin_gest_clientsVakom.php?partid='.$elmt['part_id'].'">'.$elmt['part_nom'].'</a></td>
							<td class="TX">'.$elmt['part_rs'].'</td>
							<td class="TX">';
							if ($elmt['part_cp']!= 'NULL'){
								echo $elmt['part_cp'];
							}
							echo '</td>
							<td class="TX">';
							if ($elmt['part_ville']!= 'NULL'){
								echo $elmt['part_ville'];
							}
							echo '</td>
							<td class="TX">'.$elmt['code_libelle'].'</td>
							<td class="TX">';
							
							$sql_cpte_cert	= "select COUNT(certifie.cert_id) cpt from certifie where cert_part_id='".$elmt['part_id']."' AND ACTIF='1' AND CERT_DATE_SUPPRESSION IS NULL";
							$cpte_cert 		= $db->query($sql_cpte_cert);
							echo intval($cpte_cert[0]['cpt']);
							
							echo '</td>
							<td class="TX">';
							if ($elmt['part_bloque']=='1'){
								echo '<span style="font-weight: bold; color: #FF0000;">Bloqué</span>';
							}else{
								echo '<span style="font-weight: bold; color: #00FF00;">OK</span>';
							}
							echo '</td>
						</tr>';
					}
					?>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		</table>
		<table width="961">
			<tr><td>&nbsp;</td></tr>
		</table>
			<?php
		}
	}
	  ?>
	</body>
	</html>
<?php
}else{
	header('location:index.php');
}
?>