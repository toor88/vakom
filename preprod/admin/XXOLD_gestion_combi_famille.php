<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);

	switch($_POST['choix_action']){
		case 'selection':
			if ($_POST['select_select']!=''){
				/* On sélectionne le tableau dans la base de données */
				$sql_sel_tableau = "SELECT * FROM TEXTE_IMAGE WHERE TXT_IMG_ID='".txt_db(intval($_POST['select_select']))."'";
				$qry_sel_tableau = $db->query($sql_sel_tableau);
				
				/* Si on trouve une correspondance */
				if (is_array($qry_sel_tableau)){
				
					/* On sélectionne la liste des codes de regroupement */
					$sql_sel_groupes0 = "SELECT * FROM CODE_REGROUPEMENT WHERE TXT_IMG_ID=".intval($qry_sel_tableau[0]['txt_img_id'])." ORDER BY CODE_REGROUPEMENT.CODE_REGROUP_NOM ASC";
					$qry_sel_groupes0 = $db->query($sql_sel_groupes0);
					
					/* Sélection du premier code de regroupement de la liste pour le positionnement */
					$premierCode = intval($qry_sel_groupes0[0]['code_regroup_id']);
					
					$sql_txt_code_regroup0 = "SELECT * FROM TEXTE WHERE TXT_CODE_ID=".intval($qry_sel_groupes0[0]['txt_code_id'])."";
					$qry_txt_code_regroup0 = $db->query($sql_txt_code_regroup0);
					header('location:gestion_combi_famille2-1.php?txtimgid='.intval($qry_sel_tableau[0]['txt_img_id']).'&regroupid='.$qry_sel_groupes0[0]['code_regroup_id'].'&textid='.$qry_txt_code_regroup0[0]['txt_code_id'].'&onglet=textes');
				}
			}
		break;
		case 'creation': // Création d'un nouveau questionnaire
			if (trim($_POST['nom_tableau'])!=''){
				// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
				$sql_seq_num 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
				$qry_seq_num 		= $db->query($sql_seq_num);
				$seq_num			= intval($qry_seq_num[0]['seq_num']);
			
				$sql_insert_txt_img 	= "INSERT INTO TEXTE_IMAGE VALUES('".$seq_num."', 
				'".txt_db($_POST['nom_tableau'])."', 
				'',
				'',
				'',
				'',
				'')";
				$qry_insert_txt_img	= $db->query($sql_insert_txt_img);
				
				header('location:gestion_combi_famille2.php?txtimgid='.$seq_num);
			}else{
				$error = '<?php echo $t_manque_questionnaire ?>';
			}
		break;
		case 'duplication': // Création d'un nouveau questionnaire
			if (trim($_POST['nvo_nom_tableau'])!='' && $_POST['select_duplic']>0){
			
				/* On sélectionne le tableau dans la base de données */
				$sql_sel_tableau = "SELECT * FROM TEXTE_IMAGE WHERE TXT_IMG_ID='".txt_db(intval($_POST['select_duplic']))."'";
				$qry_sel_tableau = $db->query($sql_sel_tableau);
				
				/* On sélectionne les combinaisons */
				$sql_sel_combi = "SELECT * FROM TEXTE_IMAGE_A_COMBI WHERE COMBI_TXT_IMG_ID='".txt_db(intval($_POST['select_duplic']))."'";
				$qry_sel_combi = $db->query($sql_sel_combi);
				
				// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
				$sql_seq_num 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
				$qry_seq_num 		= $db->query($sql_seq_num);
				$seq_num			= intval($qry_seq_num[0]['seq_num']);
			
				$sql_insert_txt_img 	= "INSERT INTO TEXTE_IMAGE VALUES('".$seq_num."', 
				'".txt_db($_POST['nvo_nom_tableau'])."', 
				'".txt_db($qry_sel_tableau[0]['txt_img_quest_id'])."',
				'".txt_db($qry_sel_tableau[0]['txt_img_type_profil_code_id'])."',
				'".txt_db($qry_sel_tableau[0]['txt_img_precision_code_id'])."',
				'".txt_db($qry_sel_tableau[0]['txt_img_code_regroup_id'])."',
				'".txt_db($qry_sel_tableau[0]['txt_code_id'])."')";
				$qry_insert_txt_img	= $db->query($sql_insert_txt_img);
				$sql_insert_txt_img 	= "INSERT INTO CODE_REGROUPEMENT (SELECT SEQ_ID.NEXTVAL,CODE_REGROUP_NOM,'".$seq_num."',TXT_CODE_ID FROM CODE_REGROUPEMENT WHERE TXT_IMG_ID='".txt_db(intval($_POST['select_duplic']))."')";
				$qry_insert_txt_img	= $db->query($sql_insert_txt_img);
				/* On redirige */
				header('location:gestion_combi_famille2-1.php?txtimgid='.intval($seq_num).'&regroupid='.$qry_sel_tableau[0]['txt_img_code_regroup_id'].'&textid='.$qry_sel_tableau[0]['txt_code_id'].'&onglet=textes');
			}else{
				$error = '<?php echo $t_manque_questionnaire ?>';
			}
		break;
	}
	
	/* Si on a demandé de supprimer le tableau txt/img */
	if($_GET['action']=='delete_txtimg' && $_GET['txtimgid']>0){

		/* On fait un DELETE dans la table */
		$sql_del_txtimgid  = "DELETE FROM TEXTE_IMAGE WHERE TXT_IMG_ID='".txt_db(intval($_GET['txtimgid']))."'";
		$qry_del_txtimgid  = $db->query($sql_del_txtimgid);
		
		/* On redirige la personne sur la page d'accueil des txt/img */
		header('location:gestion_combi_famille.php');
	}
	
			
	/* On sélectionne toutes les combinaisons présentes dans la base de données */
	$sql_txt_img_liste = "SELECT * FROM TEXTE_IMAGE ORDER BY TXT_IMG_NOM ASC";
	$qry_txt_img_liste = $db->query($sql_txt_img_liste);
	
	/* On sélectionne toutes les combinaisons présents dans la base de données */
	$sql_txt_img_liste2 = "SELECT * FROM TEXTE_IMAGE ORDER BY TXT_IMG_NOM ASC";
	$qry_txt_img_liste2 = $db->query($sql_txt_img_liste2);
	?>
	<html>
	<head>
	<title>Vakom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<script language="JavaScript">
	<!--
	function supprimer_txtimg(txtimgid){
		if (txtimgid>0){
			var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
			var filename = "ajax_supp_txtimg.php"; // La page qui réceptionne les données
			var data     = null; 

			var xhr_object = null; 
				 
			if(window.XMLHttpRequest) // Firefox 
			   xhr_object = new XMLHttpRequest(); 
			else if(window.ActiveXObject) // Internet Explorer 
			   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
			else { // XMLHttpRequest non supporté par le navigateur 
			   alert("<?php echo $t_browser_support_error_1 ?>");
			   return; 
			} 
			 

			data = "txtimgid="+txtimgid;
			
			if(method == "GET" && data != null) {
			   filename += "?"+data;
			   data      = null;
			}
			 
			xhr_object.open(method, filename, true);

			xhr_object.onreadystatechange = function() {
			   if(xhr_object.readyState == 4) {
				  var tmp = xhr_object.responseText.split(":"); 
				  if(typeof(tmp[0]) != "undefined") { 
					if(tmp[0]==0){
						if(confirm('<?php echo $t_supprimer_tableau ?>')){
							document.location.href='gestion_combi_famille.php?txtimgid='+txtimgid+'&action=delete_txtimg';
						}
					}else{
						alert('<?php echo $t_doc_deleted_error ?>');
					}
				  }
			   } 
			} 

			xhr_object.send(data); //On envoie les données
		}
		
	}
	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
	<form method="post" action="#">
	  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td align="center"> 
			<?php
				include('menu_top.php');
			?>
		  </td>
		</tr>
	  </table>
	  <br>
		<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;TEXTES ET IMAGES</td>
		</tr>
		</table>
	  <table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
		  <td width="14"></td>
		  <td align="center" class="TX"> 
			<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center" id="table1">
			 <?php
			  /* S'il y a au moins un graphe dans la base de données */
			  if (is_array($qry_txt_img_liste)){
			  ?>
			  <tr> 
				<td align="left" class="TX"> 
				  <input type="radio" id="choix_action1" name="choix_action" value="selection" checked="checked">
				  Sélectionner un tableau de textes & images :&nbsp;&nbsp;</td>
				<td align="left" class="TX"> 
				  <select name="select_select" id="sel_txtimg" class="form_ediht_Tarifs" onclick="document.getElementById('choix_action1').checked=true;">
					<?php
					foreach($qry_txt_img_liste as $txt_img_liste){
						echo '<option value="'.$txt_img_liste['txt_img_id'].'">'.$txt_img_liste['txt_img_nom'].'</option>';
					}
					?>
				  </select>&nbsp;<input type="button" class="bn_ajouter" value="Supprimer" onclick="supprimer_txtimg(document.getElementById('sel_txtimg').options[document.getElementById('sel_txtimg').selectedIndex].value);">
				</td>
			  </tr>
			  <?php
			  }
			  ?>
			  <tr align="left"> 
				<td class="TX"> 
				  <input type="radio" id="choix_action2" name="choix_action" value="creation">
				  Cr&eacute;er un nouveau tableau de textes &amp; images :&nbsp;&nbsp;</td>
				<td class="TX"> 
				  <input type="text" name="nom_tableau" class="form_ediht_Tarifs" size="45" maxlength="120" onfocus="document.getElementById('choix_action2').checked=true;">
				  &nbsp;&nbsp; </td>
			  </tr>
			  <?php
			  /* S'il y a au moins un graphe dans la base de données */
			  if (is_array($qry_txt_img_liste2)){
			  ?>
			  <tr> 
				<td align="left" class="TX"> 
				  <input type="radio" id="choix_action3" name="choix_action" value="duplication">
				  Dupliquer un tableau de textes & images :&nbsp; &nbsp;</td>
				<td align="left" class="TX"> 
				  <select name="select_duplic" class="form_ediht_Tarifs" onclick="document.getElementById('choix_action3').checked=true;">
					<?php
					foreach($qry_txt_img_liste2 as $txt_img_liste2){
						echo '<option value="'.$txt_img_liste2['txt_img_id'].'">'.$txt_img_liste2['txt_img_nom'].'</option>';
					}
					?>
				  </select>
				  &nbsp;Nouveau&nbsp;libellé&nbsp;:&nbsp;
				  <input type="text" name="nvo_nom_tableau" class="form_ediht_Tarifs" size="45" maxlength="100" onclick="document.getElementById('choix_action3').checked=true;">
				</td>
			  </tr>
			  <?php
			  }
			  ?>
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14" class="TX"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	  <br>
	  <tr>
		  <td width="14" height="14">&nbsp;</td>
		  <td height="14">&nbsp;</td>
		  <td width="14" height="14">&nbsp;</td>
		</tr>
	  <p align="center">
			
		<input type="submit" name="ok" value="Valider" class="BN">
		<br>
	  </p>
	</form>
	</body>
	</html>
	<?php
}else{
	include('no_acces.php');
}
?>