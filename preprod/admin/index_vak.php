<?php
require_once("../config/lib/connex.php");
require_once("../config/lib/db.oracle.php");
$db = new db($conn);
	
$sql_detail_vakom 	= "SELECT * FROM PARTENAIRE WHERE PART_ID=724";
$qry_detail_vakom 	= $db->query($sql_detail_vakom);

// Definition de l'URL du Partenaire VAKOM
$temp_site_web 		= explode('http://', $qry_detail_vakom[0]['part_site_web']);
$site_web 			= $temp_site_web[(count($temp_site_web)-1)];


?>
<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo_home.css" type="text/css">
<link rel="stylesheet" href="../css/general_home.css" type="text/css">
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  //window.open(theURL,winName,features);
  window.open(theURL,winName,features);	  
}
function open_email(){
	MM_openBrWindow('renvoi_mail.php','renvoi_mail','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=250')
}
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
<form action="whoconnex.php" method="post">
	
  <table width="1024" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td align="left">
	  <?php
	  if ($site_web!=''){
		echo '<a target="_blank" href="http://'.$site_web.'"><img style="border: none;" src="../images/logo.jpg" width="139" height="87"></a>';
	  }else{
		echo '<img src="../images/logo.jpg" width="139" height="87">';
	  }
	  ?>
	  </td>
	  <td align="right" valign="top"><a href="http://www.vakom.fr/page-hommes-et-reseaux-vakom-1.html" target="_blank"><img style="border:none;" src="../images/partenariat2.jpg" width="500" height="45"></a></td>
    </tr>
    <tr> 
      <td colspan="2" align="left">

	  </td>
    </tr>
    <tr> 
      <td colspan="2" align="left">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" align="left"> 
        <table width="1024" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="758" align="right"> 
              <table width="733" border="0" cellspacing="0" cellpadding="0" class="fond_tablo_violet" height="232">
                <tr> 
                  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
                </tr>
                <tr> 
                  <td width="14"></td>
                  <td width="50"><img src="../images/fleche_partenaire.gif" width="28" height="28" align="absmiddle"></td>
                  <td class="Titre_Blanc"> BIENVENUE SUR L'ESPACE PARTENAIRES 
                    VAKOM</td>
                  <td width="14"></td>
                </tr>
                <tr> 
                  <td width="14"></td>
                  <td class="TX_blanc"></td>
                  <td class="Titre_Orange18Italic">Les nouvelles technologies 
                    au service des Ressources Humaines</td>
                  <td width="14"></td>
                </tr>
                <tr> 
                  <td width="14"></td>
                  <td class="TX_blanc"></td>
                  <td class="TX_blancNormal">&nbsp;</td>
                  <td width="14"></td>
                </tr>
                <tr> 
                  <td width="14"></td>
                  <td class="TX_blanc"></td>
                  <td class="TX_blancNormal" valign="top"> Le r&ocirc;le de la 
                    gestion et du d&eacute;veloppement des comportements devient 
                    une composante de plus <br>
                    en plus importante, sinon majeure de la vie d&#146;une entreprise.<br>
                    <br>
                    <b>Fort de ce constat, nous avons d&eacute;cid&eacute; de 
                    rendre disponible aux entreprises <br>
                    la m&eacute;thode OPR </b>(Optimisation du Potentiel Relationnel)</td>
                  <td width="14"></td>
                </tr>
                <tr> 
                  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
                </tr>
              </table>
            </td>
            <td width="30"></td>
            <td> 
              <table width="232" border="0" cellspacing="0" cellpadding="0" class="fond_tablo_orange" height="232">
                <tr> 
                  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td class="TX_violet" valign="top"><img src="../images/fleche_orange.jpg" width="28" height="28"></td>
                  <td class="TX_violet" valign="top" height="30">MERCI DE BIEN 
                    VOULOIR<br>
                    VOUS IDENTIFIER</td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td colspan="2"> 
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="TX">login&nbsp;</td>
                        <td> 
                          <input type="password" name="login" class="form_ediht" size="18">
                        </td>
                      </tr>
                      <tr> 
                        <td class="TX">mot de passe&nbsp;</td>
                        <td> 
                          <input type="password" name="mdp" class="form_ediht" size="18">
                        </td>
                      </tr>
                      <tr align="center"> 
                        <td class="TX" colspan="2" height="8"></td>
                      </tr>
                      <tr align="center"> 
                        <td class="TX" colspan="2"> 
                          <input name="submit" type="submit" value="valider" class="bn_gris_FdOrange">
                        </td>
                      </tr>
                      <tr> 
                        <td class="TX" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="TX" colspan="2"><b>></b>&nbsp;<a href="#" onclick="open_email();" class="TX_violet10"><u>MOT&nbsp;DE&nbsp;PASSE&nbsp;PERDU</u></a><br>
                          <b>></b>&nbsp;<a href="mailto:accueil@vakom.fr" class="TX_violet10"><u>CONTACTEZ&nbsp;VAKOM</u></a></td>
                      </tr>
                    </table>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                  <td height="14"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td width="758" align="right">&nbsp;</td>
            <td width="10"></td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td width="758" align="right" valign="top"> 
              <table width="733" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="14" align="right">&nbsp;</td>
                  <td width="50" align="left"><img src="../images/fleche_grise.jpg" width="28" height="28"></td>
                  <td class="Titre_Violet">QU'EST-CE QUE L'ESPACE PARTENAIRE ?</td>
                </tr>
                <tr> 
                  <td class="TX_normal">&nbsp;</td>
                  <td class="TX_normal">&nbsp;</td>
                  <td class="TX_normal">&nbsp;</td>
                </tr>
                <tr> 
                  <td class="TX">&nbsp;</td>
                  <td class="TX">&nbsp;</td>
                  <td class="TX_normal"><b>C&#146;est une plateforme</b> privil&egrave;ge 
                    qui permet &agrave; nos partenaires de g&eacute;rer les &eacute;valuations 
                    de leurs candidats, <br>
                    de leurs collaborateurs, de leurs clients &agrave; l&#146;aide 
                    d&#146;une m&eacute;thode d&eacute;velopp&eacute;e et valid&eacute;e 
                    par Vakom.<br>
                    C&#146;est un espace d&#146;aide &agrave; l&#146;action, accessible 
                    depuis un simple navigateur web.<br>
                    Il int&egrave;gre des outils sp&eacute;cialis&eacute;s en 
                    ressources humaines et des modules d&#146;analyse des donn&eacute;es 
                    qui permettent d&#146;exploiter pleinement les r&eacute;sultats 
                    des questionnaires, des profils, des &eacute;valuations pass&eacute;s.</td>
                </tr>
                <tr> 
                  <td class="TX">&nbsp;</td>
                  <td class="TX">&nbsp;</td>
                  <td class="TX">&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td><img src="../images/fleche_grise.jpg" width="28" height="28"></td>
                  <td class="Titre_Violet">A QUOI &Ccedil;A SERT ?</td>
                </tr>
                <tr> 
                  <td class="TX">&nbsp;</td>
                  <td class="TX">&nbsp;</td>
                  <td class="TX">&nbsp;</td>
                </tr>
                <tr> 
                  <td class="TX_normal">&nbsp;</td>
                  <td class="TX_normal">&nbsp;</td>
                  <td class="TX_normal">La m&eacute;thode OPR propose sur cette 
                    plateforme des outils simples, efficaces, accessibles, op&eacute;rationnels, 
                    <br>
                    reconnus pour aider et accompagner les professionnels des 
                    ressources humaines dans leurs missions</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="TX_normal"> 
                    <blockquote> &#149; Identifier les potentialit&eacute;s, orienter 
                      et promouvoir. <br>
                      D&eacute;finir le projet personnel et professionnel d&#146;un 
                      salari&eacute;<br>
                      &#149; Evaluer, recruter, assurer l&#146;ad&eacute;quation 
                      profil/poste, favoriser l&#146;int&eacute;gration<br>
                      &#149; Construire des r&eacute;f&eacute;rentiels de comportements 
                      et de mode de fonctionnement pour d&eacute;tecter <br>
                      les potentiels<br>
                      &#149; Manager, cr&eacute;er les conditions de r&eacute;ussite 
                      d&#146;une &eacute;quipe, d&#146;un projet, &eacute;tablir 
                      le profil d&#146;une &eacute;quipe<br>
                      pour trouver les leviers de d&eacute;veloppement<br>
                      &#149; Motiver, optimiser la communication, valoriser les 
                      talents<br>
                      &#149; Innover, d&eacute;velopper la cr&eacute;ativit&eacute;, 
                      impliquer une &eacute;quipe autour d&#146;un projet<br>
                      &#149; D&eacute;velopper la performance commerciale d&#146;une 
                      personne, d&#146;une &eacute;quipe<br>
                    </blockquote>
                  </td>
                </tr>
                <tr> 
                  <td class="TX_normal">&nbsp;</td>
                  <td class="TX_normal">&nbsp;</td>
                  <td class="TX_normal">&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td><img src="../images/fleche_grise.jpg" width="28" height="28"></td>
                  <td class="Titre_Violet">POUR QUI ?</td>
                </tr>
                <tr> 
                  <td class="TX_normal">&nbsp;</td>
                  <td class="TX_normal">&nbsp;</td>
                  <td class="TX_normal">&nbsp;</td>
                </tr>
                <tr> 
                  <td class="TX_normal">&nbsp;</td>
                  <td class="TX_normal">&nbsp;</td>
                  <td class="TX_normal">Cet espace est destin&eacute; &agrave; 
                    tout partenaire b&eacute;n&eacute;ficiant d&#146;un contrat 
                    privil&egrave;ge. </td>
                </tr>
              </table>
            </td>
            <td width="10"></td>
            <td valign="top"> 
              <table width="232" border="0" cellspacing="0" cellpadding="0" align="center" class="fond_tablo_gris" height="232">
                <tr> 
                  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                  <td height="14" width="36"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
                </tr>
                <tr> 
                  <td width="14"></td>
                  <td class="TX_violet" width="36" valign="top"><img src="../images/fleche_grise.gif" width="28" height="28" align="absmiddle"></td>
                  <td class="TX_Tarifs" valign="top"> 
                    <p>VOUS N'&Ecirc;TES PAS<br>
                      ENCORE PARTENAIRE<br>
                      ET VOUS SOUHAITEZ<br>
                      EN SAVOIR PLUS</p>
                  </td>
                  <td width="14"></td>
                </tr>
                <tr> 
                  <td width="14"></td>
                  <td width="36"></td>
                  <td class="TX" valign="top"><i>Contactez, <a href="mailto:accueil@vakom.fr"><u>Fr&eacute;d&eacute;rique 
                    D&eacute;gremont</u></a>, animatrice du r&eacute;seau H&eacute;r&egrave;s 
                    pour en <br>
                    savoir plus.</i></td>
                  <td width="14"></td>
                </tr>
                <tr> 
                  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                  <td height="14" width="36"></td>
                  <td height="14"></td>
                  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td colspan="2" align="left">&nbsp;</td>
    </tr>
  </table>
</form>
</body>
</html>

