<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']>5){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	$db = new db($conn);

	if ($_POST['posted']){
	
		if (is_uploaded_file($_FILES['photo']['tmp_name'])){
			$error = '';
			
			$ftmp  = $_FILES['photo']['tmp_name'];
			$fname = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['photo']['name']));	
			$fname = strtr($fname,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
			$fname = strtolower($fname);

			//extensions autorisées	
			$extension_autorises= array('.jpg','.jpeg', '.png', '.gif');	
			$extension= strrchr($fname,'.');	
			
			//lien des photos
			$lien  		= './images_societe/'.$fname;


			/*Definition de la taille max des minatures. */

			$width_n  	= 600;
			$height_n 	= 600;

			//verification de l'extension
			if (!in_array($extension,$extension_autorises)){
				$error .= 'Le fichier n\'est pas une image';
			}
		
			// L'image est déplacée dans le dossier normal
			if (!move_uploaded_file($ftmp,$lien)){
				$error .= 'Impossible de copier le fichier';
			}

			/* Content type */	
			//***************************	

				/*creation des miniatures. */
				list($width_orig, $height_orig) = getimagesize($lien);

				// On stoppe le script si la taille de la photo est trop grande
				if ($width_orig > 1500 || $height_orig > 1500){
					if (file_exists($lien)){
						unlink($lien);
					}
					exit('L\'image1 est trop grande <br /><a href="javascript:history.back();">retour</a><br />'.$rappel.' <hr />');
				} 
				$ratio_orig   = $width_orig/$height_orig;

				
				// Ratio normal
				if ($width_orig > $width_n || $height_orig > $height_n){
					if ($width_n/$height_n > $ratio_orig){
						$width_n  = $height_n*$ratio_orig;

					}else{
						$height_n = $width_n/$ratio_orig;

					}
				}else{
					$width_n  = $width_orig;
					$height_n = $height_orig;
				}



				// Config normale
				$image_p_n 	  = imagecreatetruecolor($width_n,$height_n);
				if ($extension=='.jpeg' || $extension=='.jpg'){
					$image_n   	  = @imagecreatefromjpeg($lien);
				}
				if ($extension=='.png'){
					$image_n   	  = @imagecreatefrompng($lien);	
				}
				if ($extension=='.gif'){
					$image_n   	  = @imagecreatefromgif($lien);	
				}
				@imagecopyresampled($image_p_n,$image_n,0,0,0,0,$width_n, $height_n, $width_orig, $height_orig);


				unset($width_n);
				unset($height_n);
				unset($ratio_orig);

				
				//movance  de la miniature crée au dessus dans le repertoire normal 
					if ($extension=='.jpeg' || $extension=='.jpg'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagejpeg($image_p_n,$lien,90);
					}
					if ($extension=='.png'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagepng($image_p_n,$lien,7);
					}
					if ($extension=='.gif'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagegif($image_p_n,$lien,90);
					}
				imagedestroy($image_p_n);
				
				unset($lien);

				
			//*************************************************************

		}else{
			$fname='';
		}
		
		
		if (is_uploaded_file($_FILES['photo2']['tmp_name'])){
			$error = '';
			
			$ftmp  = $_FILES['photo2']['tmp_name'];
			$fname2 = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['photo2']['name']));	
			$fname2 = strtr($fname2,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
			$fname2 = strtolower($fname2);

			//extensions autorisées	
			$extension_autorises= array('.jpg','.jpeg', '.png', '.gif');	
			$extension= strrchr($fname2,'.');	
			
			//lien des photos
			$lien  		= './images_societe/'.$fname2;


			/*Definition de la taille max des minatures. */

			$width_n  	= 600;
			$height_n 	= 600;

			//verification de l'extension
			if (!in_array($extension,$extension_autorises)){
				$error .= 'Le fichier n\'est pas une image';
			}
		
			// L'image est déplacée dans le dossier normal
			if (!move_uploaded_file($ftmp,$lien)){
				$error .= 'Impossible de copier le fichier';
			}

			/* Content type */	
			//***************************	

				/*creation des miniatures. */
				list($width_orig, $height_orig) = getimagesize($lien);

				// On stoppe le script si la taille de la photo est trop grande
				if ($width_orig > 1500 || $height_orig > 1500){
					if (file_exists($lien)){
						unlink($lien);
					}
					exit('L\'image1 est trop grande <br /><a href="javascript:history.back();">retour</a><br />'.$rappel.' <hr />');
				} 
				$ratio_orig   = $width_orig/$height_orig;

				
				// Ratio normal
				if ($width_orig > $width_n || $height_orig > $height_n){
					if ($width_n/$height_n > $ratio_orig){
						$width_n  = $height_n*$ratio_orig;

					}else{
						$height_n = $width_n/$ratio_orig;

					}
				}else{
					$width_n  = $width_orig;
					$height_n = $height_orig;
				}



				// Config normale
				$image_p_n 	  = imagecreatetruecolor($width_n,$height_n);
				if ($extension=='.jpeg' || $extension=='.jpg'){
					$image_n   	  = @imagecreatefromjpeg($lien);
				}
				if ($extension=='.png'){
					$image_n   	  = @imagecreatefrompng($lien);	
				}
				if ($extension=='.gif'){
					$image_n   	  = @imagecreatefromgif($lien);	
				}
				@imagecopyresampled($image_p_n,$image_n,0,0,0,0,$width_n, $height_n, $width_orig, $height_orig);


				unset($width_n);
				unset($height_n);
				unset($ratio_orig);

				
				//movance  de la miniature crée au dessus dans le repertoire normal 
					if ($extension=='.jpeg' || $extension=='.jpg'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagejpeg($image_p_n,$lien,90);
					}
					if ($extension=='.png'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagepng($image_p_n,$lien,7);
					}
					if ($extension=='.gif'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagegif($image_p_n,$lien,90);
					}
				imagedestroy($image_p_n);
				
				unset($lien);

				
			//*************************************************************

		}else{
			$fname2='';
		}
		
		if (is_uploaded_file($_FILES['photo3']['tmp_name'])){
			$error = '';
			
			$ftmp  = $_FILES['photo3']['tmp_name'];
			$fname3 = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['photo3']['name']));	
			$fname3 = strtr($fname3,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
			$fname3 = strtolower($fname3);

			//extensions autorisées	
			$extension_autorises= array('.jpg','.jpeg', '.png', '.gif');	
			$extension= strrchr($fname3,'.');	
			
			//lien des photos
			$lien  		= './images_societe/'.$fname3;


			/*Definition de la taille max des minatures. */

			$width_n  	= 600;
			$height_n 	= 600;

			//verification de l'extension
			if (!in_array($extension,$extension_autorises)){
				$error .= 'Le fichier n\'est pas une image';
			}
		
			// L'image est déplacée dans le dossier normal
			if (!move_uploaded_file($ftmp,$lien)){
				$error .= 'Impossible de copier le fichier';
			}

			/* Content type */	
			//***************************	

				/*creation des miniatures. */
				list($width_orig, $height_orig) = getimagesize($lien);

				// On stoppe le script si la taille de la photo est trop grande
				if ($width_orig > 1500 || $height_orig > 1500){
					if (file_exists($lien)){
						unlink($lien);
					}
					exit('L\'image1 est trop grande <br /><a href="javascript:history.back();">retour</a><br />'.$rappel.' <hr />');
				} 
				$ratio_orig   = $width_orig/$height_orig;

				
				// Ratio normal
				if ($width_orig > $width_n || $height_orig > $height_n){
					if ($width_n/$height_n > $ratio_orig){
						$width_n  = $height_n*$ratio_orig;

					}else{
						$height_n = $width_n/$ratio_orig;

					}
				}else{
					$width_n  = $width_orig;
					$height_n = $height_orig;
				}



				// Config normale
				$image_p_n 	  = imagecreatetruecolor($width_n,$height_n);
				if ($extension=='.jpeg' || $extension=='.jpg'){
					$image_n   	  = @imagecreatefromjpeg($lien);
				}
				if ($extension=='.png'){
					$image_n   	  = @imagecreatefrompng($lien);	
				}
				if ($extension=='.gif'){
					$image_n   	  = @imagecreatefromgif($lien);	
				}
				@imagecopyresampled($image_p_n,$image_n,0,0,0,0,$width_n, $height_n, $width_orig, $height_orig);


				unset($width_n);
				unset($height_n);
				unset($ratio_orig);

				
				//movance  de la miniature crée au dessus dans le repertoire normal 
					if ($extension=='.jpeg' || $extension=='.jpg'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagejpeg($image_p_n,$lien,90);
					}
					if ($extension=='.png'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagepng($image_p_n,$lien,7);
					}
					if ($extension=='.gif'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagegif($image_p_n,$lien,90);
					}
				imagedestroy($image_p_n);
				
				unset($lien);

				
			//*************************************************************

		}else{
			$fname3='';
		}
	
	
	
		// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
		$sql_seq_num 	= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
		$qry_seq_num 	= $db->query($sql_seq_num);
		$seq_num		= intval($qry_seq_num[0]['seq_num']);
		
		/* Si le pays selectionné n'est pas la france */
		if ($_POST['select_pays']!='5'){
			/* Le CP a pour valeur le champ cp_etr et la ville a pour valeur le champ ville_etr*/
			$cp 	= $_POST['cp_etr'];
			$ville 	= $_POST['ville_etr'];
		}else{
		/* Si le pays selectionné est la france, le CP porte la valeur du champ cp */
			$cp 	= $_POST['cp'];
			/* Si la ville n'existe pas dans la base, la ville porte la valeur du champ nom_new_ville */
			if ($_POST['new_ville']=='1'){
				$ville 	= $_POST['nom_new_ville'];
			}else{
				/* Si elle existe, la valeur de la ville est celle de la liste de sélection */
				$tab_v 	=  explode("_",$_POST['ville']);
				$ville 	= $tab_v[1];
			}
		}
		
		
		$sql_insert_part = "INSERT INTO PARTENAIRE VALUES('".$seq_num."',
		'".txt_db($_POST['nom'])."',
		'".txt_db($_POST['rs'])."',
		'".txt_db($_POST['cc'])."',
		'".txt_db($_POST['nature'])."',
		'".txt_db($_POST['adr1'])."',
		'".txt_db($_POST['adr2'])."',
		'".txt_db($cp)."',
		'".txt_db($ville)."',
		'".txt_db($_POST['select_pays'])."',
		'".txt_db($_POST['tel'])."',
		'".txt_db($_POST['fax'])."',
		'".txt_db($_POST['siren'])."',
		'".txt_db($_POST['siret'])."',
		'".txt_db($_POST['ctct'])."',
		'".txt_db($_POST['web'])."',
		'".txt_db($_POST['comm1'])."',
		'".txt_db($_POST['comm2'])."',
		'".txt_db($_POST['bloque'])."',
		'".txt_db($_POST['langue'])."',
		'".txt_db($fname)."',
		'".txt_db($fname2)."',
		'".txt_db($_POST['couleur_fond'])."',
		'".txt_db($_POST['police_texte'])."',
		SYSDATE,
		'',
		1,
		'','".txt_db($fname3)."')";
		//echo $sql_insert_part;
		$qry_insert_part = $db->query($sql_insert_part);
			?>
			<script type="text/javascript">
				//window.opener.location.reload(true);
				//document.location.href='admvak_edit_client.php?partid='+<?php echo $seq_num ?>;
				window.close();
			</script>		
			<?php
	}

		/* On sélectionne la liste des natures de clients */
		$sql_nature_list = "SELECT * FROM CODE WHERE CODE_TABLE='NATURE' ORDER BY CODE_ORDRE";
		$qry_nature_list = $db->query($sql_nature_list);

		/* On sélectionne la liste des pays*/
		$sql_pays_list = "SELECT * FROM CODE WHERE CODE_TABLE='PAYS' ORDER BY CODE_ORDRE";
		$qry_pays_list = $db->query($sql_pays_list);
		
		/* On sélectionne la liste des langues disponibles */
		$sql_langue_list = "SELECT * FROM LANGUE ORDER BY LANG_ID";
		$qry_langue_list = $db->query($sql_langue_list);

		/* On sélectionne la liste des polices disponibles */
		$sql_police_list = "SELECT * FROM CODE WHERE CODE_TABLE='POLICE_TEXTE' ORDER BY CODE_ORDRE";
		$qry_police_list = $db->query($sql_police_list);

	?>
	<html>
	<head>
	<title>Vakom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<script language="JavaScript">
	<!--
	function hid_ville(id_pays){
		if (id_pays != "5"){
			document.getElementById('cp1').style.display='none';
			document.getElementById('cp2').style.display='inline';
			document.getElementById('ville2').style.display='none';
			document.getElementById('ville3').style.display='inline';
		}else{
			document.getElementById('cp1').style.display='inline';
			document.getElementById('cp2').style.display='none';
			document.getElementById('ville2').style.display='inline';
			document.getElementById('ville3').style.display='none';
		}
	}
	
	function charge_code(code){
		var tmp_code = code.split("_");
		document.getElementById('cp').value = tmp_code[0];
	}

	function TestVille(){
		var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
		var filename = "ajax_ville.php"; // La page qui réceptionne les données
		var cp = document.getElementById('cp').value; 
		var data     = null; 
		
		if 	(cp.length>1){ //Si le code postal tapé possède au moins 2 caractères
			document.getElementById("ville2").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" />';
			var xhr_object = null; 
				 
				if(window.XMLHttpRequest) // Firefox 
				   xhr_object = new XMLHttpRequest(); 
				else if(window.ActiveXObject) // Internet Explorer 
				   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
				else { // XMLHttpRequest non supporté par le navigateur 
				   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
				   return; 
				} 
				 
				
				 
				if(cp != ""){
					data = "cp="+cp;
				}
				if(method == "GET" && data != null) {
				   filename += "?"+data;
				   data      = null;
				}
				 
				xhr_object.open(method, filename, true);

				xhr_object.onreadystatechange = function() {
				   if(xhr_object.readyState == 4) {
					  var tmp = xhr_object.responseText.split(":"); 
					  if(typeof(tmp[0]) != "undefined") { 
						 document.getElementById("ville2").innerHTML = '';
						 if (tmp[0]!=''){
							document.getElementById('saisie_ville_m').style.display='inline';
							document.getElementById('saisie_ville_m_lab').style.display='inline';
							document.getElementById('saisie_ville_m').checked = false;								 						 						 
							document.getElementById("ville2").innerHTML = tmp[0];
						 }else{
							 document.getElementById('saisie_ville_m').style.display='none';
							 document.getElementById('saisie_ville_m_lab').style.display='none';								 						 						 
							document.getElementById("ville2").innerHTML = '<input type="hidden" name="new_ville" value="1"><input type="text" size="46" name="nom_new_ville" maxlength="255" class="form_ediht_Partenaires" />';
						 }
					  }
				   } 
				} 

				xhr_object.send(data); //On envoie les données
		}
	}

	function doSaisieLibre(){
		var saisie_ville_m = document.getElementById('saisie_ville_m');
		if (saisie_ville_m.checked){
			document.getElementById('saisie_ville_m').style.display='none';
			document.getElementById('saisie_ville_m_lab').style.display='none';
			document.getElementById("ville2").innerHTML = '<input type="hidden" name="new_ville" value="1"><input type="text" size="46" name="nom_new_ville" maxlength="255" class="form_ediht_Certifies" />';
		}
	}	
	
	blocolor = true;
	function hexa(couleur){
	if(blocolor)
	document.getElementById('couleur').value = '#'+couleur; // On inscrit dans le champs TEXT la valeur HEX de la couleur
	}

	function palette_couleurs(){

	// Ouverture du tableau
	var tableau_palette = "<TABLE border='1' cellpadding='0' cellspacing='1' ><TR>";

	// Déclaration des variables
	var tabk = new Array('FF','CC','99','66','33','00'); // Tableau principal de couleur
	var tabj = tabk;
	var tabi = new Array('CC', '66', '00'); // Petit tableau principal (colonne de 6 couleurs)
	var tabi2 = new Array('00','33','66','99','CC','FF'); // Tableau principal inversé pour la colone du milieu (colonne de 6 couleurs)
	var color=""; // initialisation de color a vide
	var cmp = 0; // initialisation du compteur a 0

	// Début prog
	for(var k=0;k<6;k++) // Boucle pour les lignes de couleurs
	{
	for(var i=0;i<3;i++) // Boucle pour les colonnes (colonnes de 6 couleurs)
	{
	if (i == 1) // Si on attaque la 2 eme colonne de 6 couleurs
	{
	tabj = tabi2; // on inverse le tableau principale de couleurs
	}
	else // sinon
	{
	tabj = Array('FF','CC','99','66','33','00'); // On remet le tableau par default
	}

	for(var j=0;j<6;j++) // Boucle pour l'affichage couleur par couleur
	{
	color="#"+tabi[i]+tabk[k]+tabj[j]; // concaténation des chaines pour la valeur de la couleur
	// et on affiche la couleur
	tableau_palette += "<TD width='15' height='15' style='border: 1px solid #000000;' bgcolor='"+color+"' onClick=\"hexa('"+tabi[i]+tabk[k]+tabj[j]+"')\" onMouseOver=\"this.style.cursor='pointer'\"></TD>";
	}
	}
	tableau_palette += "</tr>";

	cmp = cmp + 1; // On compte le nombre de ligne faite
	if (cmp == 6) // si on a fait les 6 lignes
	{
	var tabi = new Array('FF', '99', '33'); // on redéfini le nouveau tableau principal (colonne de 6 couleurs)
	var tabk = tabi2; // on re initialise le tableau des lignes
	k = -1; // on défini k à -1 car on a déja fait un passage
	}
	}
	// Fin prog
	tableau_palette += "</TABLE><BR>"; // On ferme le tableau + saut de ligne
	document.getElementById('tab_palette').innerHTML = tableau_palette;
	}

	function verif(){
			error = '';
		if (document.form.nom.value == ''){
			error += "<?php echo $t_nom_oblig ?>\n";
		}
		if (document.form.cc.value == ''){
			error += "<?php echo $t_code_clt_oblig ?>\n";
		}
		if (document.form.adr1.value == ''){
			error += "<?php echo $t_adresse_oblig ?>\n";
		}
		if (document.getElementById('pays').options[document.getElementById('pays').selectedIndex].value == '5'){
			if (document.form.cp.value.length != 5){
				error += "<?php echo $t_cp_oblig ?>\n";
			}
			if (document.form.new_ville){
				if (document.form.new_ville.value!='1'){
					if (document.form.ville.value == ''){
						error += "<?php echo $t_ville_oblig ?>\n";
					}
				}else{
					if (document.form.nom_new_ville.value == ''){
						error += "<?php echo $t_ville_oblig ?>\n";
					}				
				}
			}else{
				if (document.form.ville.value == ''){
					error += "<?php echo $t_ville_oblig ?>\n";;
				}
			}
		}else{
			if (document.form.cp_etr.value.length != 5){
				error += "<?php echo $t_cp_oblig ?>\n";
			}
			if (document.form.ville_etr.value == ''){
				error += "<?php echo $t_ville_oblig ?>\n";
			}
		}
		

		if (document.form.tel.value == ''){
			error += "<?php echo $t_tel10_oblig ?>\n";
		}/*
		if (document.form.couleur_fond.value == ''){
			error += 'La couleur de fond est obligatoire\n';
		}*/

		if (document.form.comm1.value == ''){
			error += "<?php echo $t_adr_retour_oblig ?>\n";
		}
		if (error!=''){
			alert(error);
		}else{
			document.form.submit();
		}
	}

	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}

	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
	<form method="post" name="form" action="#" enctype="multipart/form-data">
	  <table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Partenaires"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $t_fiche_new_part ?></td>
		</tr>
	  </table>
	  <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="800" align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" valign="top" class="TX" colspan="2"> 
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
			  <tr> 
				<td class="fond_tablo_Partenaires" height="40" align="left"><?php echo $t_fiche_part ?></td>
				<td class="fond_tablo_Partenaires2" valign="middle" align="left"><?php echo $t_date_creation ?> 
				  : le <?php echo date('d/m/Y H:i') ?>
				  </td>
			  </tr>
			  <tr> 
				<td class="TX">&nbsp;</td>
				<td class="champsoblig" valign="middle" align="right"><?php echo $t_champs_oblig ?> 
				  * </td>
			  </tr>
			  <tr> 
				<td class="TX_Partenaires"><?php echo $t_fiche_info_part ?></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr bgcolor="#000000"> 
				<td class="TX" height="1"></td>
				<td height="1"></td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_fiche_nom_part ?>* :</td>
					<td class="TX"> 
					  <input type="text" name="nom" size="70" maxlength="70" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_nom'] ?>">
						&nbsp;<?php echo $t_fiche_part_bloq ?>&nbsp;<input type="checkbox" name="bloque" value="1">
					</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_fiche_design_part ?> : </td>
				<td class="TX"> 
				  <input type="text" name="rs" size="70" maxlength="70" class="form_ediht_Partenaires">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_fiche_code_clt ?>* :</td>
				<td class="TX"> 
				  <input type="text" name="cc" size="9" maxlength="10" class="form_ediht_Partenaires" value="C" maxlength="8">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_nat ?>* :</td>
				<td class="TX"> 
				  <select name="nature" class="form_ediht_Partenaires">
				  <?php
				  if (is_array($qry_nature_list)){
					foreach($qry_nature_list as $nature){
						echo '<option value="'.$nature['code_id'].'">'.$nature['code_libelle'].'</option>';
					}
				  }
				  ?>
				  </select>
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_adresse ?>* :</td>
				<td> 
				  <input type="text" name="adr1" size="70" maxlength="70" class="form_ediht_Partenaires">
				</td>
			  </tr>
			  <tr> 
				<td class="TX">&nbsp;</td>
				<td class="TX"> 
				  <input type="text" name="adr2" size="70" maxlength="70" class="form_ediht_Partenaires">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_pays ?>* :</td>
				<td> 
				  <select name="select_pays" id="pays" class="form_ediht_Partenaires" onchange="hid_ville(document.getElementById('pays').options[document.getElementById('pays').selectedIndex].value)">
				<?php
				  if (is_array($qry_pays_list)){
					foreach($qry_pays_list as $pays){
						echo '<option value="'.$pays['code_id'].'">'.$pays['code_libelle'].'</option>';
					}
				  }
				?>
				  </select>
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_cp ?>* :</td>
				<td class="TX"> 
				  <div id="cp1" width="1%" style="display: inline;">
				  <input type="text" name="cp" id="cp" size="5" maxlength="5" class="form_ediht_Partenaires" onkeyUp="TestVille()" onblur="charge_code(document.getElementById('ville').options[document.getElementById('ville').selectedIndex].value)">
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  </div>
				  <div id="cp2" width="1%" style="display: none;">
				  <input type="text" name="cp_etr" id="cp_etr" size="5" maxlength="5" class="form_ediht_Partenaires">
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  </div><?php echo $t_ville ?>*&nbsp;:&nbsp;
				 <div id="ville2" width="1%" style="display: inline;"><input type="hidden" id="ville" name="ville" value=""></div>
				<div style="float: right;margin-right: 35px;">
					<input type="checkbox" name="saisie_ville_m" id="saisie_ville_m"  onclick="doSaisieLibre()" style="display: none;float: left;    margin-left: 25px;margin-top: 5px;">
					<span id="saisie_ville_m_lab" style="display: none;line-height: 20px;" >&nbsp;&nbsp;Saisie libre</span>
				</div>								 
				<div id="ville3" style="display: none;">
					<input type="text" name="ville_etr" maxlength="255" class="form_ediht_Partenaires">
				</div>
				</td>
			  </tr>
				<tr> 
				<td class="TX" height="40"><?php echo $t_tel ?>* :</td>
				<td class="TX"> 
				  <input type="text" name="tel" size="25" maxlength="25" class="form_ediht_Partenaires">
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $t_fax ?> 
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
				  <input type="text" name="fax" size="24" maxlength="25" class="form_ediht_Partenaires">
				</td>
			  </tr>          <tr> 
				<td class="TX" height="40"><?php echo $t_fiche_tva_int ?> :</td>
				<td class="TX"> 
				  <input type="text" name="siren" size="25" maxlength="25" class="form_ediht_Partenaires">
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $t_fiche_siret ?> : 
				  <input type="text" name="siret" size="24" maxlength="25" class="form_ediht_Partenaires">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_contact ?> : </td>
				<td class="TX"> 
				  <input type="text" name="ctct" size="70" maxlength="70" class="form_ediht_Partenaires">
				</td>
			  </tr>
			  
			  <tr> 
				<td class="TX" height="40"><?php echo $t_website ?> :</td>
				<td> 
				  <input type="text" name="web" size="70" maxlength="128" class="form_ediht_Partenaires">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40" valign="middle"><?php echo $t_redirect_web ?>* :</td>
				<td> 
				  <input type="text" name="comm1" size="70" maxlength="128" class="form_ediht_Partenaires">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40" valign="middle"><?php echo $t_info ?> :</td>
				<td> 
				  <input type="text" name="comm2" size="70" maxlength="70" class="form_ediht_Partenaires">
				</td>
			  </tr>
			  <tr> 
				<td class="TX_Partenaires">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr> 
				<td class="TX_Partenaires"><?php echo $t_fiche_ergonomie ?></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr bgcolor="#000000"> 
				<td class="TX" height="1"></td>
				<td height="1"></td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_fiche_lang_extra ?>* :</td>
				<td class="TX"> 
				  <select name="langue" class="form_ediht_Partenaires">
					<?php
					  if (is_array($qry_langue_list)){
						foreach($qry_langue_list as $langue){
							echo '<option value="'.$langue['lang_id'].'">'.$langue['lang_libelle'].'</option>';
						}
					  }
					?>
				  </select>
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_fiche_logo_part1 ?>&nbsp;:</td>
				<td> 
				  <input type="file" name="photo" size="50" class="form_ediht_Partenaires"><img src="../images/logo.jpg" alt="" width="46" height="29">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_fiche_logo_part2 ?>&nbsp;:</td>
				<td> 
				  <input type="file" name="photo2" size="50" class="form_ediht_Partenaires"><img src="../images/partenariat.jpg" alt="" width="200" height="18">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40"><?php echo $t_fiche_logo_part3.'<br>'.$t_fiche_ilogo_part3 ?></td>
				<td> 
				  <input type="file" name="photo3" size="50" class="form_ediht_Partenaires">
				</td>
			  </tr>
			  <!--
			  <tr> 
				<td class="TX" height="40" valign="top">Couleur de fond* :</td>
				<td align="left"> 
				  <input type="text" name="couleur_fond" id="couleur" size="7" maxlength="7" class="form_ediht_Partenaires" readonly value="default">
				  &nbsp;&nbsp;<div id="tab_palette"></div>
						<script language="JavaScript">
						  palette_couleurs();
						</script>
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40">Police du texte* :</td>
				<td> 
				  <select name="police_texte" class="form_ediht_Partenaires">
					<?php
					  /*if (is_array($qry_police_list)){
						foreach($qry_police_list as $police){
							echo '<option value="'.$police['code_id'].'">'.$police['code_libelle'].'</option>';
						}
					  }*/
					?>
				  </select>
				</td>
			  </tr>
			  <tr> 
				<td class="TX" height="40">&nbsp;</td>
				<td>&nbsp; </td>
			  </tr>-->
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	  <br>
	  <table border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td align="center"> 
			<input type="hidden" name="posted" value="1">
			<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_partenaire" onclick="verif();">
		  </td>
		</tr>
	  </table>
	</form>
	</body>
	</html>
<?php
}else{
	include('no_acces.php');
}
?>