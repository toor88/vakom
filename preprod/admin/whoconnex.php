<?php
ini_set('session.gc_maxlifetime', 3600);
session_start();
unset($_SESSION['error']);
include ("../config/lib/connex.php");
include ("../config/lib/db.oracle.php");
$db = new db($conn);

if ($_POST['submit']){
	
	if(trim($_POST['login'])!='' && trim($_POST['mdp'])!=''){
	
		$sql_detail_vakom 		= "SELECT * FROM PARTENAIRE WHERE PART_ID=724";
		$qry_detail_vakom 		= $db->query($sql_detail_vakom);
			
		// Definition de l'URL du Partenaire VAKOM
		$temp_site_web 			= explode('http://', $qry_detail_vakom[0]['part_site_web']);
		$_SESSION['website']	= $temp_site_web[(count($temp_site_web)-1)];
		
		// On vérifie si la personne est un admin de chez VAKOM
		$sql_ident 	= "SELECT * FROM USER_VAKOM WHERE UPPER(VAK_LOGIN)='".strtoupper(txt_db($_POST['login']))."' AND UPPER(VAK_PWD)='".strtoupper(txt_db($_POST['mdp']))."'";
		//echo $sql_ident;
		$qry_ident 	= $db->query($sql_ident);
		
		if (is_array($qry_ident)){
			
			$_SESSION['vak_id'] 	= $qry_ident[0]['vak_id'];
			$_SESSION['vak_type'] 	= $qry_ident[0]['vak_type'];
			$_SESSION['nom'] 		= $qry_ident[0]['vak_nom'];
			$_SESSION['prenom'] 	= $qry_ident[0]['vak_prenom'];
			$_SESSION['lang'] 		= 1;			
			if ($qry_ident[0]['vak_type']=='SAV'){
				
				$_SESSION['droit'] 		= 9;
				$_SESSION['home'] 		= 'candidats.php';
			
			}elseif ($qry_ident[0]['vak_type']=='AV'){
				
				$_SESSION['droit'] 		= 6;
				$_SESSION['home'] 		= 'candidats.php';
				
			}else{
				$error = 1;
			}
			
		}else{
			/* REQUETE POUR VERIFIER QUELS DROITS POSSEDE LA PERSONNE QUI SE LOG */
			
			// On vérifie si la personne est un certifié, Admin Partenaire ou Certifié Client
			$sql_ident2 	= "SELECT CERTIFIE.CERT_PART_ID,
			CERTIFIE.CERT_ID,
			CERTIFIE.CERT_NOM,
			CERTIFIE.CERT_PRENOM,
			CERTIFIE.CERT_DROIT_ADMIN,
			CERTIFIE.CERT_DROIT_CERTIFIE, 
			PARTENAIRE.PART_LANG_ID,
			PARTENAIRE.PART_LOGO1,
			PARTENAIRE.PART_LOGO2,
			PARTENAIRE.PART_LOGO3,
			PARTENAIRE.PART_COULEUR_FOND,
			PARTENAIRE.PART_SITE_WEB,
			PARTENAIRE.PART_POLICE_TEXTE_CODE_ID 
			FROM CERTIFIE, PARTENAIRE 
			WHERE CERTIFIE.CERT_PART_ID=PARTENAIRE.PART_ID 
			AND UPPER(CERT_LOGIN)='".strtoupper(txt_db($_POST['login']))."' 
			AND UPPER(CERT_PWD)='".strtoupper(txt_db($_POST['mdp']))."' 
			AND CERT_USER_SUPPRESSION_ID IS NULL 
			AND CERT_DATE_SUPPRESSION IS NULL 
			AND ACTIF='1' 
			AND (PARTENAIRE.PART_BLOQUE<'1' OR PARTENAIRE.PART_BLOQUE IS NULL)";
			$qry_ident2 	= $db->query($sql_ident2);
			
			if(is_array($qry_ident2)){
			
				$sql_details	= "SELECT CODE.CODE_LIBELLE FROM CODE, PARTENAIRE WHERE PARTENAIRE.PART_POLICE_TEXTE_CODE_ID=CODE.CODE_ID AND CODE.CODE_TABLE='POLICE_TEXTE' AND PARTENAIRE.PART_ID='".txt_db($qry_ident2[0]['cert_part_id'])."'";
				$qry_details 	= $db->query($sql_details);
				if (is_array($qry_details)){
					$_SESSION['police'] = $qry_details[0]['code_libelle'];
				}else{
					$_SESSION['police'] = 'Arial';
				}
				
				$_SESSION['vak_id']		= '0';
				$_SESSION['part_id'] 	= $qry_ident2[0]['cert_part_id'];
				$_SESSION['cert_id'] 	= $qry_ident2[0]['cert_id'];
				$_SESSION['nom'] 		= strtoupper($qry_ident2[0]['cert_nom']);
				$_SESSION['prenom'] 	= ucfirst($qry_ident2[0]['cert_prenom']);
				
				$_SESSION['lang'] 		= $qry_ident2[0]['part_lang_id'];
				
				$_SESSION['logo1'] 		= $qry_ident2[0]['part_logo1'];
				$_SESSION['logo2'] 		= $qry_ident2[0]['part_logo2'];
				$_SESSION['logo3'] 		= $qry_ident2[0]['part_logo3'];
				$_SESSION['couleur'] 	= $qry_ident2[0]['part_couleur_fond'];
				$_SESSION['adminCertif'] = $qry_ident2[0]['cert_droit_certifie'];
				
				if($qry_ident2[0]['part_logo3']!='' && $qry_ident2[0]['part_site_web'] != ''){
					$temp_site_web 			= explode('http://', $qry_ident2[0]['part_site_web']);
					$_SESSION['website']	= $temp_site_web[(count($temp_site_web)-1)];
				}
				
				if ($qry_ident2[0]['cert_droit_admin']=='1'){
					$_SESSION['droit'] 		= 4;
					$_SESSION['home'] 		= 'accueil.php';
				}else{
					$_SESSION['droit'] 		= 2;
					$_SESSION['home'] 		= 'accueil.php';
				}
			}else{
				$error = 2;
			}
			
		}
		
	}else{ // Sinon, on redirige vers le formulaire de login
		$error 	= 3;
	}
}else{ // Sinon, on redirige vers le formulaire de login
	$error = 4;
}

if ($error<1){
	header('location:'.$_SESSION['home']);
}else{
	//echo $error;
	header('location:./index.php');
}
?>
