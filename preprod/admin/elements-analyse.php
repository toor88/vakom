<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="fr-FR">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="fr-FR">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html lang="fr-FR">
<!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title>VAKOM | Barometre des postures manag&eacute;riales</title>
	<link rel="icon" type="image/jpg" href="/favicon.jpg" />
	
<meta name="robots" content="noindex,follow" />
<link href="style.css" rel="stylesheet" type="text/css" />    
</head>
<?php

    include ("../config/lib/connex.php");
    include ("../config/lib/db.oracle.php");
    $db = new db($conn);
    
//6570640
$opeid = $_GET['opeid'];
$manger_nom ="";
$mangerplusun_nom ="";
$manger_cand_id = null;
$nb_personnes_mg=-2;

if (isset($opeid)){
	$sql_select_question = "SELECT distinct CAND_ID FROM CAND_A_QUEST WHERE OPE_ID = ".txt_db(intval($opeid));
    $condidats   	 = $db->query($sql_select_question);
	    
    foreach($condidats as $condidat){
		$sql_infos_cand = "SELECT CANDIDAT.*,CAND_A_OPE.* FROM CANDIDAT, CAND_A_OPE WHERE CAND_A_OPE.CAND_ID = CANDIDAT.CAND_ID AND CANDIDAT.CAND_ACTIF='1' AND CANDIDAT.CAND_ID='".txt_db(intval($condidat['cand_id']))."'  AND CAND_A_OPE.OPE_ID=".txt_db(intval($opeid))."";
		$infos_candidat = $db->query($sql_infos_cand);            
        if ($infos_candidat[0]['niveau'] == 1){
			$manger_cand_id = $condidat['cand_id'];
        	$manger_nom = $infos_candidat[0]['cand_prenom'] . ' '. $infos_candidat[0]['cand_nom'];
			$cand_fonction = $infos_candidat[0]['cand_fonction'];
        }
        if ($infos_candidat[0]['niveau'] == 2){
        	$mangerplusun_nom = $infos_candidat[0]['cand_prenom'] . ' '. $infos_candidat[0]['cand_nom'];        	
        }        
		$nb_personnes_mg++;
		
		/* BPM */
		$sql_questionnaire = "SELECT distinct questionnaire.quest_id,quest_type_question_code_id FROM recup_quest,questionnaire,cand_ope 
		WHERE recup_quest.txt_img_quest_id=questionnaire.quest_id and cand_ope.prod_id=recup_quest.prod_id and cand_id='".txt_db(intval($condidat['cand_id']))."' AND ope_id='".txt_db(intval($opeid))."'";
		$qry_questionnaire = $db->query($sql_questionnaire);				
		$quest_id = $qry_questionnaire[0]['quest_id'];
    }

    $sql_questions  = "SELECT * FROM QUEST_A_LANG,QUESTIONNAIRE, CHOIX, QUEST_A_VAL WHERE QUEST_A_VAL.VAL_QUEST_ID=QUESTIONNAIRE.QUEST_ID
		AND QUEST_A_VAL.VAL_CHOIX_ID=CHOIX.CHOIX_ID AND QUESTIONNAIRE.QUEST_ID=".txt_db(intval($quest_id))."
		AND QUEST_A_LANG.QUEST_ID(+)=QUESTIONNAIRE.QUEST_ID AND QUEST_A_VAL.VAL_DATE_SUPPRESSION IS NULL
		ORDER BY QUEST_A_VAL.VAL_ORDRE, QUEST_A_VAL.VAL_CHOIX_ID ASC, CHOIX.CODE_ID DESC";
    	$questions 		= $db->query($sql_questions);
    	
	$themes = array();		
    foreach($questions as $question){
		$sql_bpm_quest = "SELECT * FROM BPM WHERE ITHEME_ID='".txt_db($question['code_id'])."'";
		$qry_bpm_quest = $db->query($sql_bpm_quest);			

		$theme_id = $qry_bpm_quest[0]['theme_id'];
		$stheme_id = $qry_bpm_quest[0]['stheme_id'];
		$itheme_id = $qry_bpm_quest[0]['itheme_id'];	
		
		$sql_theme_quest = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='BPM_THEME' AND CODE_LANG_ID=1 and CODE_ACTIF=1 AND CODE_ID='".$theme_id."' ";
		$qry_theme_quest = $db->query($sql_theme_quest);
											
		$sql_sthemes = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='BPM_STHEME' AND CODE_LANG_ID=1 and CODE_ACTIF=1 AND CODE_ID='".$stheme_id."' ";
		$qry_sthemes = $db->query($sql_sthemes);
											
		$sql_ithemes = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='BPM_ITHEME' AND CODE_LANG_ID=1 and CODE_ACTIF=1 AND CODE_ID='".$itheme_id."' ";
		$qry_ithemes = $db->query($sql_ithemes);
											
		$code_libelle_theme = $qry_theme_quest[0]['code_libelle'];
		$code_libelle_stheme = $qry_sthemes[0]['code_libelle'];
		$code_libelle_itheme = $qry_ithemes[0]['code_libelle'];	

		if (empty($themes[$theme_id]))
		{
			$choix = array();			
		 	$theme = new stdClass;
		 	$stheme = new stdClass;
		 	$itheme = new stdClass;
		 			 	
		 	$theme->max = 1;		 	
		 	$stheme->max = 1;
		 	$itheme->max = 1;

		 	$theme->code_libelle = $code_libelle_theme;		 	
		 	$stheme->code_libelle = $code_libelle_stheme;
		 	$itheme->code_libelle = $code_libelle_itheme;
			
		 	$theme->stheme[$stheme_id]=$stheme;
		 	
			//Auto evaluation : 
			$sql_auto_eval = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=1 and choix_id=".$question['choix_id'];
			$qry_auto_eval = $db->query($sql_auto_eval);
			//N+1 : 
			$sql_nplusun = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=2 and choix_id=".$question['choix_id'];
			$qry_nplusun = $db->query($sql_nplusun);
			//Coll : 
			$sql_coll = "select round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0 and choix_id=".$question['choix_id'];
			$qry_coll = $db->query($sql_coll);
			
			$quest = new stdClass;
			$quest->question = $question['choix_1'];
			$quest->auto_eval = $qry_auto_eval[0]['txt_libre'];
			$quest->nplusun = $qry_nplusun[0]['txt_libre'];
			$quest->coll = $qry_coll[0]['note'];
															
			$itheme->questions[] = $quest;
										 	
		 	$theme->stheme[$stheme_id]->itheme[$itheme_id]=$itheme;
		 		 	
			$themes[$theme_id]=$theme;
		}
		else 
		{
			$themeObj = $themes[$theme_id];
			$themeObj->max += 1;
			
			if (empty($themeObj->stheme[$stheme_id]))
			{
				$sthemes = array();				
				$theme = $themes[$theme_id];
				
				$stheme = new stdClass;		 					
		 		$stheme->max = 1;	
						 
		 		$stheme->code_libelle = $code_libelle_stheme;
		 					
				$themeObj->stheme[$stheme_id] = $stheme;
				
				$itheme = new stdClass;
		 		$itheme->max = 1;
		 		$itheme->code_libelle = $code_libelle_itheme;
					 			 			

				//Auto evaluation : 
				$sql_auto_eval = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=1 and choix_id=".$question['choix_id'];
				$qry_auto_eval = $db->query($sql_auto_eval);
				//N+1 : 
				$sql_nplusun = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=2 and choix_id=".$question['choix_id'];
				$qry_nplusun = $db->query($sql_nplusun);
				//Coll : 
				$sql_coll = "select round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0 and choix_id=".$question['choix_id'];
				$qry_coll = $db->query($sql_coll);
					
				$quest = new stdClass;
				$quest->question = $question['choix_1'];
				$quest->auto_eval = $qry_auto_eval[0]['txt_libre'];
				$quest->nplusun = $qry_nplusun[0]['txt_libre'];
				$quest->coll = $qry_coll[0]['note'];
															
				$itheme->questions[] = $quest;
									 			 			
				$themeObj->stheme[$stheme_id]->itheme[$itheme_id] = $itheme;
					
			} else {
				$stheme = $themeObj->stheme[$stheme_id];
				$stheme->max += 1;
				$themeObj->stheme[$stheme_id] = $stheme;					
				
				if (empty($themeObj->stheme[$stheme_id]->itheme[$itheme_id]))
				{
					$itheme = new stdClass;
			 		$itheme->max = 1;
		 			$itheme->code_libelle = $code_libelle_itheme;			 							
					
					//Auto evaluation : 					
					$sql_auto_eval = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=1 and choix_id=".$question['choix_id'];
					$qry_auto_eval = $db->query($sql_auto_eval);
					//N+1 : 
					$sql_nplusun = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=2 and choix_id=".$question['choix_id'];
					$qry_nplusun = $db->query($sql_nplusun);
					//Coll : 
					$sql_coll = "select round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0 and choix_id=".$question['choix_id'];
					$qry_coll = $db->query($sql_coll);
					
					$quest = new stdClass;
					$quest->question = $question['choix_1'];
					$quest->auto_eval = $qry_auto_eval[0]['txt_libre'];
					$quest->nplusun = $qry_nplusun[0]['txt_libre'];
					$quest->coll = $qry_coll[0]['note'];
															
					$itheme->questions[] = $quest;
										
					$themeObj->stheme[$stheme_id]->itheme[$itheme_id] = $itheme;			 		
				} else {
					$itheme = $stheme->itheme[$itheme_id];
					$itheme->max += 1;

					//Auto evaluation : 
					$sql_auto_eval = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=1 and choix_id=".$question['choix_id'];
					$qry_auto_eval = $db->query($sql_auto_eval);
					//N+1 : 
					$sql_nplusun = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=2 and choix_id=".$question['choix_id'];
					$qry_nplusun = $db->query($sql_nplusun);
					//Coll : 
					$sql_coll = "select round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0 and choix_id=".$question['choix_id'];
					$qry_coll = $db->query($sql_coll);
					
					$quest = new stdClass;					
					$quest->question = $question['choix_1'];
					$quest->auto_eval = $qry_auto_eval[0]['txt_libre'];
					$quest->nplusun = $qry_nplusun[0]['txt_libre'];
					$quest->coll = $qry_coll[0]['note'];
															
					$itheme->questions[] = $quest;
										
					$themeObj->stheme[$stheme_id]->itheme[$itheme_id] = $itheme;
				}
			}				
			$themes[$theme_id] = $themeObj;			
		}		
    }		
//	print_r($themes);

}
?>
<body >
	<button id="cmd">Imprimer</button>	
<div id="pageTop" class="hfeed site top">
	<header  class="site-header" >
	<div class="logo-vakom">				
		<!--<img src="img/Le-sens-de-humain.png" alt="logo"/>-->
		<img src="../images/logo-bpm.png" style="height: 75px;" alt="logo"/>				
	</div>
	<div id="secondary" class="sidebar-container sidebar-header">
		<div class="widget-area">
			<aside id="nav_menu-2" class="widget widget_nav_menu">
				<div class="menu-menu-top-container">
        	    	<ul id="menu-menu-top" class="menu">
						<li  class="menu-item "><a href="#">
								<!--<img src="img/vakom-sens-de-humain.png" alt="logo client">-->
								<img src="../images/vakom-sens-de-humain.png" style="height: 105px;margin-top: -15px;" alt="logo client"></a>								
						</li>
					</ul>
				</div>
			</aside>		
		</div><!-- .widget-area -->
	</div><!-- #secondary -->
	
		</header><!-- #masthead -->
	
	</div><!-- #page -->
	
	<div id="navbar" class="navbar">
		
	</div><!-- #navbar -->
	<div id="page" class="site">	
		<div id="main" class="site-main">
		<div id="content">
			<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">							
				<div class="entry-contentAdmin">	
					<p class="TitreTheme">&eacute;l&eacute;ments d'analyse</p>
					<table class="tableAnalyse">
  <tbody>
    <tr>
      <td style="width: 30%"  >Nom et pr&eacute;nom de la personne &eacute;valu&eacute;e</td>
      <td><?php echo$manger_nom?></td>
      </tr>
    <tr>
      <td  >Fonction</td>
      <td  ><?php echo$cand_fonction?></td>
      </tr>
    <tr>
      <td  >Nombre de collaborateurs inscrits</td>
      <td ><?php echo$nb_personnes_mg?></td>
      </tr>
    <tr>
      <td  >Nom du manager de la personne &eacute;valu&eacute;e</td>
      <td ><?php echo$mangerplusun_nom?></td>
      </tr>
    </tbody>
</table>
	
<div class="CadreMoyenne">	
	<?php
		$sql_coll = "select cand_id,round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0 group by cand_id";
		$qry_coll = $db->query($sql_coll);	
	?>

	<table class="tableMoyenne">
  		<tbody>
	<?php 
		$index = 1;
		foreach($qry_coll as $coll) { ?>
    	<tr>
	      <th>Moyenne collaborateur <?php echo$index?></th>
    	  <td><?php echo $coll['note'];?></td>
	    </tr>		
	<?php $index++;} 
			$sql_coll = "select round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0";
			$qry_coll = $db->query($sql_coll);
	?>
    <tr>
      <th >Moyenne collaborateur G&eacute;n&eacute;rale</th>
      <td ><?php echo $qry_coll[0]['note'];?></td>
    </tr>
    <!--	
    <tr>
      <th>Moyenne collaborateur 1</th>
      <td>5</td>
      <th>Moyenne collaborateur 4</th>
      <td>5</td>
      </tr>
    <tr>
      <th  >Moyenne collaborateur 2</th>
      <td  >4,8</td>
      <th  >Moyenne collaborateur 5</th>
      <td  >7,4</td>
      </tr>
    <tr>
      <th  >Moyenne collaborateur 3</th>
      <td >5,3</td>
      <th >Moyenne collaborateur G&eacute;n&eacute;rale</th>
      <td >5,5</td>
    </tr>-->
    </tbody>
	</table>
</div>
<div class="CadreCentre">
	<form name="questionnaire" method="post" action="#">
<div class="cadreCentre">
	<?php 
	$table ='<table class="tableResultAnalyse"><tbody>';
	$table .='<tr><th >Th&egrave;me</th><th >Sous-th&egrave;me</th><th >Items</th><th >Affirmations</th>';
	$table .='<th style="background-color: #0b2e43; width: 8%" >Auto<br>&eacute;valuation</th>';
	$table .='<th style="background-color: #3785b3; width: 8%" >N+1</th>';
	$table .='<th style="background-color: #46b0ef; width: 8%">Moyenne<br>Coll</th></tr>';
    $style = 0;    
    foreach($themes as $theme){
    	$index_theme = 0;    
	    foreach($theme->stheme as $stheme){
    		$index_stheme = 0; 
		    foreach($stheme->itheme as $itheme){  
    			$index_itheme = 0;
    			
		    	foreach($itheme->questions as $quest)
		    	//for($max = 0; $max<$itheme->max;$max++ )
		    	{
	    			$table .="<tr>";
		    		if ($index_theme == 0)
		    		{
						$table .="<td rowspan=$theme->max style='width: 4%;text-align: center;'>".$theme->code_libelle."</td>";
		    		}
					if ($index_stheme == 0)
		    		{		    		
		    			$table .="<td rowspan=$stheme->max style='width: 4%;text-align: center;'>".$stheme->code_libelle."</td>";
					}		    		
		    		if ($index_itheme == 0)
		    		{
						if ($style%2 == 0){
			    			$table .="<td class='color' rowspan=$itheme->max style='width: 4%;text-align: center;'>".$itheme->code_libelle."</td>";						
						} else {
			    			$table .="<td rowspan=$itheme->max>".$itheme->code_libelle."</td>";						
						}
		    		}
		    		if ( $style%2 == 0 ){
		    			$table .="<td class='color'>".$quest->question."</td>";
		    		}else {
		    			$table .="<td style='width: 46%;'>".$quest->question."</td>";		    		
		    		}
		    		$table .="<td class='center'>".$quest->auto_eval."</td>";
		    		$table .="<td class='center'>".$quest->nplusun."</td>";
		    		$table .="<td class='center'>".$quest->coll."</td>";		    		
	    			$table .="</tr>";
	    			$index_itheme++;		    		    			
					$index_stheme++;	    			
					$index_theme++;					
		    	}
				$style++;
			}
		}			    			
    }
	$table .="</tbody></table>";	
	?>
	<?php echo$table?>
	<!--
<table class="tableResultAnalyse">
  <tr>
    <th >Th&egrave;me</th>
    <th >Sous-th&egrave;me</th>
    <th >Items</th>
    <th >Affirmations</th>
    <th style="background-color: #0b2e43; width: 8%" >Auto<br>
      &eacute;valuation</th>
    <th style="background-color: #3785b3; width: 8%" >N+1</th>
    <th style="background-color: #46b0ef; width: 8%">Moyenne<br>
      Coll</th>
    </tr>
  <tr>
    <td rowspan="38" class="center" >Piloter <br>
      une &eacute;quipe</td>
    <td rowspan="23" class="center">Animer <br>
      et 
      motiver <br>
      son &eacute;quipe</td>
    <td rowspan="3" class="color center" >Porter la politique <br>
      de l'entreprise</td>
    <td class="color">Il (elle) explique le bien fond&eacute; de la politique de l'entreprise</span></td>
    <td class="color center">5</td>
    <td class="color center">9</td>
    <td class="color center">5</td>
    </tr>
  <tr>
    <td class="color">Il (elle) agit en coh&eacute;rence avec la politique de l'entreprise</td>
    <td class="color center">5</td>
    <td class="color center">5</td>
    <td class="color center">5</td>
  </tr>
  <tr>
    <td class="color">Il (elle) adh&egrave;re et d&eacute;fend les valeurs de l'entreprise</td>
    <td class="color center">6</td>
    <td class="color center">6</td>
    <td class="color center">6</td>
  </tr>
  <tr>
    <td rowspan="4" class="center">Communiquer<br>
      et informer</td>
    <td >Il (elle) communique et informe r&eacute;guli&egrave;rement ses collaborateurs</td>
    <td class="center" >9</td>
    <td class="center" >8</td>
    <td class="center" >2</td>
  </tr>
  <tr>
    <td >Il (elle) communique et informe r&eacute;guli&egrave;rement sa hi&eacute;rarchie</td>
    <td class="center" >5</td>
    <td class="center" >5</td>
    <td class="center" >5</td>
  </tr>
  <tr>
    <td >Les messages  sont correctement relay&eacute;s des collaborateurs vers la direction et inversement</td>
    <td class="center" >5</td>
    <td class="center" >5</td>
    <td class="center" >5</td>
  </tr>
  <tr>
    <td >Il (elle) prend le temps d'&eacute;couter ses collaborateurs</td>
    <td class="center" >6</td>
    <td class="center" >5</td>
    <td class="center" >7</td>
  </tr>
  <tr>
    <td rowspan="4" class="color center" >Conduite <br>
      de r&eacute;union</td>
    <td class="color" >Il (elle) pr&eacute;pare et organise des r&eacute;unions avec un ordre du jour et un temps d&eacute;fini</td>
    <td class="color center" >&nbsp;</td>
    <td class="color center" >&nbsp;</td>
    <td class="color center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Il (elle) m&egrave;ne des r&eacute;unions adapt&eacute;es en fonction de l'objectif d&eacute;fini</td>
    <td class="color center" >&nbsp;</td>
    <td class="color center" >&nbsp;</td>
    <td class="color center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Ses r&eacute;unions se terminent par une conclusion ou des d&eacute;cisions</td>
    <td class="color center" >&nbsp;</td>
    <td class="color center" >&nbsp;</td>
    <td class="color center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Il (elle) fait participer et  g&egrave;re les disgr&eacute;tions des participants lors des r&eacute;unions</td>
    <td class="color center" >&nbsp;</td>
    <td class="color center" >&nbsp;</td>
    <td class="color center" >&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="6" class="center">G&eacute;rer <br>
      les tensions<br>
      et les conflits</td>
    <td >Il (elle) anticipe les tensions et les conflits</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) identifie ses propres responsabilit&eacute;s dans un conflit</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) d&eacute;samorce les conflits en utilisant une d&eacute;marche adapt&eacute;e : n&eacute;gociation, m&eacute;diation, arbitrage</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) arrive Ã  obtenir une implication et un accord des diff&eacute;rentes personnes impliqu&eacute;es dans un conflit</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) sait rester neutre bienviellant et factuel avec tous les participants du conflit</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) veille Ã  remettre en place les conditions de travail normale apr&egrave;s le conflit</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
    <td class="center" >&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="6" class="color center" >Favoriser <br>
      la
      coh&eacute;sion <br>
      d'&eacute;quipe</td>
    <td class="color" >Il (elle) met en place des r&egrave;gles de vie comprises par tous</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Il (elle) instaure un climat d'&eacute;quipe propice Ã  l'efficacit&eacute; collective</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Il (elle) fait en sorte que chacun soit tourn&eacute; vers l'int&eacute;rÃªt collectif et cherche des solutions aux probl&egrave;mes de l'&eacute;quipe</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Il (elle) g&egrave;re les dysfonctionnements et malentendus qui pourraient nuire au fonctionnement de l'&eacute;quipe</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Il (elle) r&eacute;partit les missions et les tÃ¢ches &eacute;quitablement entre les diff&eacute;rents collaborateurs</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Il (elle) r&eacute;partit les missions en fonction des comp&eacute;tences de ses collaborateurs</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="15" class="center" >Accompagner <br>
      ses <br>
      collaborateurs</td>
    <td rowspan="6" class="center" >Impliquer <br>
      et motiver</td>
    <td >Il (elle) &eacute;value et suit l'activit&eacute; de ses collaborateurs dans leur domaine d'activit&eacute; r&eacute;guli&egrave;rement pour les faire progresser</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) identifie les potentiels et les envies des collaborateurs pour leur permettre de d&eacute;velopper de nouvelles comp&eacute;tences</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) pr&eacute;pare et fait pr&eacute;parer les entretiens d'&eacute;valuation et/ou professionnel</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
  <tr>
    <td >Ses remarques sont comprises, justes et constructives</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) propose un plan d'actions individuel et r&eacute;aliste</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) veille Ã  l'application des conclusions de l'entretien d'&eacute;valuation/professionnel</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="5" class="color center" >D&eacute;l&eacute;guer et <br>
      d&eacute;velopper<br>
      l'autonomie</td>
    <td class="color" >Il (elle) d&eacute;l&egrave;gue en pr&eacute;cisant l'attendu en terme d'objectif, de qualit&eacute; et de d&eacute;lais et en laissant un niveau d'autonomie coh&eacute;rent en fonction de son collaborateur</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Ses collaborateurs sont autonomes et responsabilis&eacute;s</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Il (elle) contrÃ´le et accompagne son collaborateur tout au long de la d&eacute;l&eacute;gation</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Il (elle) connait les motivations de chacun de ses collaborateurs</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td class="color" >Il (elle) cr&eacute;&eacute; les conditions pour que ses collaborateurs soient motiv&eacute;s en tenant compte de leurs besoins</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="4" class="center" >Reconnaitre<br>
      ses <br>
      collaborateurs</td>
    <td >Il (elle) fait r&eacute;guli&egrave;rement un retour Ã  ses collaborateurs sur la qualit&eacute; du travail qu'il soit positif ou n&eacute;gatif</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
  <tr>
    <td >Ses recadrages sont pertinents et justes</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) m&egrave;ne des entretiens de recadrage en &eacute;tant factuel, en identifiant les causes et les effets et en sortant avec un plan d'action</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
  <tr>
    <td >Il (elle) f&eacute;licite ses collaborateurs Ã  bon escient</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
	<tr>
    <td rowspan="26" class="center" >Piloter <br>
une <br>
organisation</td>
    <td rowspan="12" class="center">Organiser l'activit&eacute;</td>
    <td rowspan="4" class="color center" >G&eacute;rer <br>
      et analyser <br>
      l'activit&eacute;</td>
    <td class="color" >Il (elle) d&eacute;finit des objectifs et des plans d'actions clairs et compris par tous</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
	<tr>
	  <td class="color" >Il (elle) organise  l'activit&eacute; de son &eacute;quipe</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  </tr>
	<tr>
	  <td class="color" >Il (elle) supervise et contrÃ´le l'activit&eacute; de son &eacute;quipe</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  </tr>
	<tr>
	  <td class="color" >Il (elle) utilise des outils de pilotage pour  accompagner son &eacute;quipe</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  </tr>
	<tr>
    <td rowspan="5" class="center" >Prendre<br>
      des<br>
      d&eacute;cisions</td>
    <td >Il (elle) prend des d&eacute;cisions et des intiatives claires et les assume</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
	<tr>
	  <td >Il (elle) communique sur ses d&eacute;cisions</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) informe la direction de ses initiatives</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) sollicite l'avis de ses collaborateurs ou des autres responsables de service avant de prendre une d&eacute;cision</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) ne change pas facilement d'avis quand une d&eacute;cision est prise et annonc&eacute;e</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td rowspan="3" class="color center" >Faire respecter l'organisation<br>
	    et les r&egrave;gles </td>
	  <td class="color" >Il (elle) respecte les r&egrave;gles et l'organisation</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  </tr>
	<tr>
	  <td class="color" >Il (elle) fait respecter les r&egrave;gles et l'organisation</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  </tr>
	<tr>
    <td class="color" >En cas de hors jeux, les sanctions sont appliqu&eacute;es de faÃ§on &eacute;quitable</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
	<tr>
    <td rowspan="14" class="center" >D&eacute;velopper </td>
    <td rowspan="6" class="center" >D&eacute;velopper l'activit&eacute;</td>
    <td >Il (elle) suit et analyse les r&eacute;sultats</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
	<tr>
	  <td >Il (elle) utilise des tableaux de bord appropri&eacute;s</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) implique les collaborateurs dans l'analyse et le suivi des r&eacute;sultats</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) anticipe les &eacute;volutions de son activit&eacute;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) propose r&eacute;guli&egrave;rement des am&eacute;liorations</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) valide et met en application les id&eacute;es propos&eacute;es par ses collaborateurs</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td rowspan="3" class="color center" >Conduire le <br>
	    changement</td>
	  <td class="color" >Il (elle) s'adapte au changement</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  </tr>
	<tr>
	  <td class="color" >Il (elle) accompagne le changement dans son &eacute;quipe</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  </tr>
	<tr>
    <td class="color" >Il (elle) utilise le changement pour faire progresser les collaborateurs</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
	<tr>
    <td rowspan="5" class="center" >Innover,<br>
      Ãªtre force<br>
      de proposition</td>
    <td >Il (elle) met en place des groupes de travail autour de l'innovation</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
	<tr>
	  <td >Il (elle) fait des propositions innovantes</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) prend des initiatives en mati&egrave;re d'innovation</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) &eacute;coute les propositions innovantes de ses collaborateurs</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) accompagne ses collaborateurs dans l'&eacute;l&eacute;boration de leur proposition</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	
	<tr>
	  <td rowspan="15" class="center" >Se<br>
	    piloter</td>
	  <td rowspan="10" class="center">Mes <br>
	    Comportements</td>
	  <td rowspan="3" class="color center" >Adopter <br>
	    une posture <br>
	    de manager</td>
	  <td class="color" >Il (elle) est reconnu par son &eacute;quipe dans son rÃ´le de manager</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  </tr>
	<tr>
	  <td class="color" >Il (elle) adopte avec ses collaborateurs un mode de relation coh&eacute;rent avec son rÃ´le de manager et la culture de l'entreprise</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  </tr>
	<tr>
    <td class="color" >Son comportement est exemplaire et conforme aux r&egrave;gles de l'entreprise</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
	
	<tr>
    <td rowspan="5" class="center" >G&eacute;rer<br>
      son temps <br>
      et les <br>
      priorit&eacute;s</td>
    <td >Il (elle) rend ses travaux dans les d&eacute;lais</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
	<tr>
	  <td >Son &eacute;quipe respecte les d&eacute;lais</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) priorise et planifie ses priorit&eacute;s en coh&eacute;rence avec sa mission</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) sait dire &quot;non&quot;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) aide ses collaborateurs Ã  organiser leurs temps</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	
	<tr>
	  <td rowspan="2" class="color center" >G&eacute;rer <br>
	    le 
	    stress</td>
	  <td class="color" >Il (elle) reste d'humeur &eacute;gale quelle que soit les situations</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  <td class="color center">&nbsp;</td>
	  </tr>
	<tr>
    <td class="color" >Il (elle) rep&egrave;re les collaborateurs en stress et met en place des actions pour favoriser leur bien Ãªtre</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
    <td class="color center">&nbsp;</td>
  </tr>
	
	<tr>
    <td rowspan="5" class="center" >Mes relations <br>
      interpersonnelles</td>
    <td rowspan="5" class="center" >D&eacute;v&eacute;lopper <br>
      des relations <br>
      interpersonnelles <br>
      de qualit&eacute;</td>
    <td >Il (elle) connait son mode  de communication peronnel</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
    <td class="center">&nbsp;</td>
  </tr>
	<tr>
	  <td >Il (elle) mesure l'impact de son mode de communication sur les autres</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) applique les fondamentaux de la communication : &eacute;coute,questionne, reformule</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) rep&egrave;re les diff&eacute;rents comportements de son interlocuteur</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	<tr>
	  <td >Il (elle) adapte sa faÃ§on de communiquer Ã  son interlocuteur</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  <td class="center">&nbsp;</td>
	  </tr>
	</table>

-->
	
<div class="cadrePagination"> </div>	
<!-- FIN PAGINATION -->

</div><!-- FIN CADRE CENTRE -->
</form>	
</div><!-- FIN CADRE CENTRE -->
</div><!-- .entry-content -->

</article><!-- #post -->

</div><!-- #content -->
</div><!-- #main -->
</div><!-- #page -->
	


</body>
<script src="html2canvas.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
<script>
	$('#cmd').click(function () {
		$('#cmd').hide();
	    html2canvas(window.document.body[0], 
    	{
			onrendered: function(canvas) {
        		var myImage = canvas.toDataURL("image/png");
    		    $(window.document.body[0])
        		    .html("<img id='Image' src=" + myImage + " style='width:100%;'></img>")
            		.ready(function() {
                		window.focus();
	                	window.print();
						$('#cmd').show();
    	        });
    		}        	
	    });
	});
</script>
</html>