<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	
	if($_GET['txtimgid']>0){
	
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);
	
		/* On sélectionne les infos concernant le tableau txtimg et le code de regroupement */
		$sql_sel_infos = "SELECT TXT_IMG_NOM, CODE_REGROUP_NOM FROM TEXTE_IMAGE,CODE_REGROUPEMENT WHERE texte_image.TXT_IMG_ID='".txt_db(intval($_GET['txtimgid']))."' AND TEXTE_IMAGE.TXT_IMG_ID=CODE_REGROUPEMENT.TXT_IMG_ID AND CODE_REGROUPEMENT.CODE_REGROUP_ID='".txt_db(intval($_GET['regroupid']))."'";
		$qry_sel_infos = $db->query($sql_sel_infos);
		
		/* Si le formulaire est envoyé */
		if (isset($_POST['select_l1'])){
			// On traite les données
			$sql_ins_combi = "INSERT INTO TEXTE_IMAGE_A_COMBI VALUES (SEQ_ID.NEXTVAL, 
			'".txt_db(intval($_GET['txtimgid']))."', ";
			
			if ($_POST['l1_c2']>0){
				$sql_ins_combi .= "'".txt_db(intval($_POST['l1_c2']))."',
				'',";
			}else{
				$sql_ins_combi .= "'".txt_db(intval($_POST['l1_c']))."',
				'".txt_db(intval($_POST['l1_d']))."',";
			}
			
			if ($_POST['l2_c2']>0){
				$sql_ins_combi .= "'".txt_db(intval($_POST['l2_c2']))."',
				'',";
			}else{
				$sql_ins_combi .= "'".txt_db(intval($_POST['l2_c']))."',
				'".txt_db(intval($_POST['l2_d']))."',";
			}
			
			if ($_POST['l3_c2']>0){
				$sql_ins_combi .= "'".txt_db(intval($_POST['l3_c2']))."',
				'',";
			}else{
				$sql_ins_combi .= "'".txt_db(intval($_POST['l3_c']))."',
				'".txt_db(intval($_POST['l3_d']))."',";
			}
			
			if ($_POST['l4_c2']>0){
				$sql_ins_combi .= "'".txt_db(intval($_POST['l4_c2']))."',
				'',";
			}else{
				$sql_ins_combi .= "'".txt_db(intval($_POST['l4_c']))."',
				'".txt_db(intval($_POST['l4_d']))."',";
			}
			
			$sql_ins_combi .= "'".intval($_SESSION['vak_id'])."',
			SYSDATE,
			'',
			'',
			'',
			'',".txt_db(intval($_GET['regroupid'])).")";
			$qry_ins_combi = $db->query($sql_ins_combi);
			
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			<?php
		}
		?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<script language="JavaScript">
		<!--
		function modif_form(x, value){
			if (value=='0'){
				document.getElementById('l'+x+'c1').style.display='none';
				document.getElementById('l'+x+'c2').style.display='block';
			}else{
				document.getElementById('l'+x+'c1').style.display='block';
				document.getElementById('l'+x+'c2').style.display='none';
			}
		}
		
		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		//-->
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#">
		  	<table width="500" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_sel_infos[0]['txt_img_nom'] ?>&nbsp;>&nbsp;<?php echo $qry_sel_infos[0]['code_regroup_nom'] ?></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td ><table border="0" cellspacing="0" cellpadding="0"  width="100%">
				  <tr> 
					<td height="14"></td>
					<td height="14"></td>
					<td height="14"></td>
					<td height="14"></td>
				  </tr>
				<tr> 
				  <td class="TX_Tarifs" colspan="4">Ajout/modification de combinaison</td>
				  </tr>
				<tr> 
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  </tr>
				<tr>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="center" class="TX" height="40"> 1 :</td>
					<td align="center" height="40"> 
					  <select name="select_l1" class="form_ediht_Tarifs">
						<option value="E">E</option>
					  </select>
					</td>
					<td align="center" height="40"> 
					  <select name="select_l1b" id="select_l1b" class="form_ediht_Tarifs" onChange="modif_form(1, document.getElementById('select_l1b').options[document.getElementById('select_l1b').selectedIndex].value)">
						<option value="0">=</option>
						<option value="1" selected="selected">Entre</option>
					  </select>
					</td>
					<td align="center" height="40" class="TX">
					<div>
					<div id="l1c1">
					  <input type="text" name="l1_c" size="2" class="form_ediht_Tarifs" maxlength="4">
					  et 
					  <input type="text" name="l1_d" size="2" class="form_ediht_Tarifs" maxlength="4">
					</div>
					<div id="l1c2" style="display: none;">
						<input type="text" name="l1_c2" size="2" class="form_ediht_Tarifs" maxlength="4">
					</div>
					</td>
				  </tr>
				  <tr> 
					<td align="center" class="TX" height="40">2 :</td>
					<td align="center" height="40"> 
					  <select name="select_l2" class="form_ediht_Tarifs">
						<option value="C">C</option>
					  </select>
					</td>
					<td align="center" height="40"> 
					  <select name="select_l2b" id="select_l2b" class="form_ediht_Tarifs" onChange="modif_form(2, document.getElementById('select_l2b').options[document.getElementById('select_l2b').selectedIndex].value)">
						<option value="0">=</option>
						<option value="1" selected="selected">Entre</option>
					  </select>
					</td>
					<td align="center" height="40" class="TX"> 
					<div id="l2c1">
					  <input type="text" name="l2_c" size="2" class="form_ediht_Tarifs" maxlength="4">
					  et 
					  <input type="text" name="l2_d" size="2" class="form_ediht_Tarifs" maxlength="4">
					</div>
					<div id="l2c2" style="display: none;">
						<input type="text" name="l2_c2" size="2" class="form_ediht_Tarifs" maxlength="4">
					</div>
					</td>
				  </tr>
				  <tr> 
					<td align="center" class="TX" height="40">3 :</td>
					<td align="center" height="40"> 
					  <select name="select_l3" class="form_ediht_Tarifs">
						<option value="P">P</option>
					  </select>
					</td>
					<td align="center" height="40"> 
					  <select name="select_l3b" id="select_l3b" class="form_ediht_Tarifs" onChange="modif_form(3, document.getElementById('select_l3b').options[document.getElementById('select_l3b').selectedIndex].value)">
						<option value="0">=</option>
						<option value="1" selected="selected">Entre</option>
					  </select>
					</td>
					<td align="center" height="40" class="TX"> 
					<div id="l3c1">
					  <input type="text" name="l3_c" size="2" class="form_ediht_Tarifs" maxlength="4">
					  et 
					  <input type="text" name="l3_d" size="2" class="form_ediht_Tarifs" maxlength="4">
					</div>
					<div id="l3c2" style="display: none;">
						<input type="text" name="l3_c2" size="2" class="form_ediht_Tarifs" maxlength="4">
					</div>
					</td>
				  </tr>
				  <tr> 
					<td align="center" class="TX" height="40">4 :</td>
					<td align="center" height="40"> 
					  <select name="select_l4" class="form_ediht_Tarifs">
						<option value="A">A</option>
					  </select>
					</td>
					<td align="center" height="40"> 
					  <select name="select_l4b" id="select_l4b" class="form_ediht_Tarifs" onChange="modif_form(4, document.getElementById('select_l4b').options[document.getElementById('select_l4b').selectedIndex].value)">
						<option value="0">=</option>
						<option value="1" selected="selected">Entre</option>
					  </select>
					</td>
					<td align="center" height="40" class="TX"> 
					<div id="l4c1">
					 <input type="text" name="l4_c" size="2" class="form_ediht_Tarifs" maxlength="4">
					  et 
					  <input type="text" name="l4_d" size="2" class="form_ediht_Tarifs" maxlength="4">
					</div>
					<div id="l4c2" style="display: none;">
						<input type="text" name="l4_c2" size="2" class="form_ediht_Tarifs" maxlength="4">
					</div>
					</td>
				  </tr>
				  <tr> 
					<td align="left" class="TX">&nbsp;</td>
					<td align="left">&nbsp;</td>
					<td align="left">&nbsp;</td>
					<td align="left" class="TX">&nbsp;</td>
				  </tr>
			    </table></td>
			  </tr>
			</table>
		  <p style="text-align:center"> 
				<input type="submit" name="Submit" value="Valider" class="BN">
			 </p>
		</form>
		</body>
		</html>
	<?php
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{
	include('no_acces.php');
}
?>