<?php
include ("../config/lib/connex.php");
include ("../config/lib/db.oracle.php");
$db = new db($conn);

$sql_cert = "SELECT CERTIFIE.*, TO_CHAR(CERTIFIE.CERT_DATE_CREATION, 'DD/MM/YYYY HH24:MI') CERT_DATE_CREATION, TO_CHAR(CERTIFIE.CERT_DATE_MODIFICATION, 'DD/MM/YYYY HH24:MI') CERT_DATE_MODIFICATION, TO_CHAR(CERTIFIE.CERT_DNS, 'DD') JJ_DNS, TO_CHAR(CERTIFIE.CERT_DNS, 'MM') MM_DNS, TO_CHAR(CERTIFIE.CERT_DNS, 'YYYY') AAAA_DNS, PARTENAIRE.PART_NOM, PARTENAIRE.PART_ID FROM CERTIFIE, PARTENAIRE WHERE CERTIFIE.CERT_PART_ID=PARTENAIRE.PART_ID AND CERTIFIE.CERT_ID='".txt_db($_GET['certid'])."'";
$qry_cert = $db->query($sql_cert);

//$mailto = 'flys@vakom.fr, julien.joye@risc-group.com, alecauchois@vakom.fr, david.naze@deliberata.com, gilles.bensimon@risc-group.com';
$mailto = $qry_cert[0]['cert_email'];
$sujet_mail = '[VAKOM] Vos codes d\'accès';

$name_s    		 = "VAKOM";
$email_s 		 = "infos-nepasrepondre@vakom.fr";
$headers    	 = "From: ". $name_s . " <" . $email_s . ">\r\n";
$headers   		.= 'Content-Type: text/html; charset="iso-8859-1"'."\n";
$headers  		.= 'Content-Transfer-Encoding: 8bit';			
$message_mail ="<html>
<body>
<p><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">Bonjour </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".ucfirst($qry_cert[0]['cert_prenom'])."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">,<br>
<br>
Dans le cadre de notre partenariat vous bénéficiez d'un accès privé à notre site 
: <br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
text-decoration:underline\" color=\"#0000FF\"><a href=\"http://www.vakom.fr\">Mon espace partenaire</a></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<br>
Afin de pouvoir accéder aux différentes rubriques de l'Espace partenaire, il est 
nécessaire de vous authentifier.<br>
<br>
Vos identifiants sont les suivants :<br>
Login : </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$qry_cert[0]['cert_login']."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
Mot de passe : </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$qry_cert[0]['cert_pwd']."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<br>
Ces informations ne peuvent en aucun cas être communiquées par téléphone, nous 
vous invitons donc à les conserver.<br>
<br>
Bien cordialement,<br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;font-weight:700\" color=\"#FF6600\">L'Equipe VAKOM</font>
<br>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\"><br>
</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">
<br>
<img border=\"0\" src=\"http://www.extranet.lesensdelhumain.com/images/logo-miniopr.jpg\"><br><br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#800080\"><b>VAKOM</b></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
38, rue bouquet<br>
76108 ROUEN<br>
Tel : 02 32 10 59 20<br>
<br>
</font>
</p>

</body>

</html>";			

mail($mailto, $sujet_mail, $message_mail, $headers);
?>
<?php echo $t_mail_OK ?>

<script type="text/javascript">
	//window.close();
	setTimeout('self.close()',1000 );
</script>