<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']>5){

	if ($_GET['partid'] && $_GET['partid']>0){

		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");

		$db = new db($conn);
		if ($_POST['partid']!=''){
			$sql_insert_contrat	= "INSERT INTO PART_A_CONTRAT VALUES(SEQ_ID.NEXTVAL,
			'".txt_db($_POST['partid'])."', 
			'".txt_db($_POST['select_ctt'])."', 
			TO_DATE('".txt_db($_POST['JJ'])."/".txt_db($_POST['MM'])."/".txt_db($_POST['AAAA'])."', 'DD/MM/YYYY'), 
			TO_DATE('".txt_db($_POST['JJ2'])."/".txt_db($_POST['MM2'])."/".txt_db($_POST['AAAA2'])."', 'DD/MM/YYYY'), 
			'".txt_db($_POST['exclu'])."', 
			'".txt_db(str_replace('.', ',', $_POST['montant']))."', 
			'".txt_db($_POST['redevence'])."', 
			'".txt_db($_POST['comm1'])."', 
			'".txt_db($_POST['comm2'])."')";
			
			$qry_insert_contrat		= $db->query($sql_insert_contrat);
			//echo $sql_insert_contrat;
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			<?php
		}
		
		// Chargement des données du partenaire
		$sql_partenaire	= "SELECT PART_ID, PART_NOM FROM PARTENAIRE WHERE PART_ID = '". txt_db($_GET['partid']) ."'";
		$qry_partenaire	= $db->query($sql_partenaire);
		
		// Chargement de la liste des types de contrat
		$sql_type_contrat_list 		= "SELECT * FROM CODE WHERE CODE_TABLE='TYPE_CONTRAT'";
		$qry_type_contrat_list 		= $db->query($sql_type_contrat_list);
		
		if (is_array($qry_partenaire)){
			?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--
			function verif(){
					error = '';
					error1 = '';
					error2 = '';
				// verif du format date du début
				if (document.form.JJ.value<1 || document.form.JJ.value>31 || document.form.JJ.value.length<2){
					error1 = true;
				}
				if (document.form.MM.value<1 || document.form.MM.value>12 || document.form.MM.value.length<1){
					error1 = true;
				}
				if (document.form.AAAA.value<2009 || document.form.AAAA.value.length<4){
					error1 = true;
				}
				if (error1==true){
					error +="<?php echo $t_mauvaise_date_deb ?>\n";
				}
				
				// Verif du format date de fin
				if (document.form.JJ2.value<1 || document.form.JJ2.value>31 || document.form.JJ2.value.length<2){
					error2 = true;
				}
				if (document.form.MM2.value<1 || document.form.MM2.value>12 || document.form.MM2.value.length<1){
					error2 = true;
				}
				if (document.form.AAAA2.value<2009 || document.form.AAAA2.value.length<4){
					error2 = true;
				}
				if (error2==true){
					error +="<?php echo $t_mauvaise_date_fin ?>\n";
				}
				
				if (error!=''){
					alert(error);
				}else{
					document.form.submit();
				}
			}
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			//-->
			</script>
			</head>

			<body bgcolor="#FFFFFF" text="#000000">
			<form name="form" method="post" action="#">
			  <table border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td>&nbsp;</td>
				</tr>
				<tr> 
				  <td align="left" class="TX"> 
					<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="800">
					  <tr> 
						<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
						<td height="14"></td>
						<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
					  </tr>
					  <tr> 
						<td width="14"></td>
						<td class="TX_rose"><?php echo $t_part_ajout_contrat.' '.$qry_partenaire[0]['part_nom'] ?></td>
						<td width="14"></td>
					  </tr>
					  <tr> 
						<td width="14"></td>
						<td bgcolor="#666666" height="1"></td>
						<td width="14"></td>
					  </tr>
					  <tr> 
						<td width="14"></td>
						<td align="center" valign="top" class="TX"> 
						   
						  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="TX">
							<tr align="center"> 
							  <td colspan="6" bgcolor="#000000" height="1"></td>
							</tr>
							<tr align="center"> 
							  <td class="TX_bold" align="left"><?php echo $t_part_typ_contrat ?></td>
							  <td class="TX_bold"><?php echo $t_part_date_debut ?></td>
							  <td class="TX_bold"><?php echo $t_part_date_fin ?></td>
							  <td class="TX_bold"><?php echo $t_part_exclu_terr ?></td>
							  <td class="TX_bold"><?php echo $t_part_montant_droit_e ?></td>
							  <td class="TX_bold"><?php echo $t_part_redevance_ann ?></td>
							</tr>
							<tr align="center"> 
							  <td colspan="6" bgcolor="#000000" height="1"></td>
							</tr>
							<tr align="center"> 
							  <td colspan="6" height="6"></td>
							</tr>
							<tr align="center"> 
							  <td align="left"> 
								<select name="select_ctt" class="form_ediht">
								  <?php
								  if (is_array($qry_type_contrat_list)){
									foreach($qry_type_contrat_list as $contrat){
										echo '<option value="'.$contrat['code_id'].'">'.$contrat['code_libelle'].'</option>';
									}
								  }
								  ?>
								</select>
							  </td>
							  <td> 
								<input type="text" name="JJ" size="2" class="form_ediht" value="<?php echo date('d') ?>" maxlength="2">
								/ 
								<input type="text" name="MM" size="2" class="form_ediht" value="<?php echo date('m') ?>" maxlength="2">
								/ 
								<input type="text" name="AAAA" size="4" class="form_ediht" value="<?php echo date('Y') ?>" maxlength="4">
							  </td>
							  <td> 
								<input type="text" name="JJ2" size="2" class="form_ediht" maxlength="2">
								/ 
								<input type="text" name="MM2" size="2" class="form_ediht" maxlength="2">
								/ 
								<input type="text" name="AAAA2" size="4" class="form_ediht" maxlength="4">
							  </td>
							  <td> 
								<input type="checkbox" name="exclu" value="1">
							  </td>
							  <td> 
								<input type="text" name="montant" size="4" class="form_ediht" >
								&euro; <?php echo $t_HT ?> </td>
							  <td> 
								<input type="checkbox" name="redevence" value="1">
							  </td>
							</tr>
							<tr align="left"> 
							  <td colspan="6">&nbsp;<?php echo $t_info3 ?> : 
								<input type="text" name="comm1" size="100" maxlength="100" class="form_ediht">
							  </td>
							</tr>
							<tr align="center"> 
							  <td colspan="6" align="left">&nbsp;<?php echo $t_info4 ?> : 
								<input type="text" name="comm2" size="100" maxlength="100" class="form_ediht">
							  </td>
							</tr>
							<tr align="center"> 
							  <td colspan="6" align="left">&nbsp;</td>
							</tr>
							<tr align="center" bgcolor="CCCCCC"> 
							  <td colspan="6" align="left" height="1"></td>
							</tr>
							<tr align="center"> 
							  <td colspan="6" align="left">&nbsp;</td>
							</tr>
						  </table>
						</td>
						<td width="14"></td>
					  </tr>
					  <tr> 
						<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
						<td height="14"></td>
						<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
					  </tr>
					</table>
				  </td>
				</tr>
				<tr> 
				  <td align="center"> 
					<input type="hidden" name="partid" value="<?php echo $qry_partenaire[0]['part_id'] ?>">
					<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="BN" onclick="verif();">
				  </td>
				</tr>
				<tr> 
				  <td align="right" width="180">&nbsp; </td>
				</tr>
			  </table>
			</form>
			</body>
			</html>
		<?php
		}
	}
}else{
	include('no_acces.php');
}
?>