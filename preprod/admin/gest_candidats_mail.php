<?php
include ("../config/lib/connex.php");
include ("../config/lib/db.oracle.php");
$db = new db($conn);

$sql_infos_cand = "SELECT * from cand_ope where cand_id=".txt_db(intval($_GET['candid']))." and ope_id=".txt_db(intval($_GET['opeid']));
$infos_candidat	= $db->query($sql_infos_cand);	
$sql_infos_cand_email = "SELECT * from candidat where cand_id=".txt_db(intval($_GET['candid']));
$infos_candidat_email	= $db->query($sql_infos_cand_email);	
if($infos_candidat_email[0]['cand_lang_id'] == 4){
	include('../config/langs/IT.php');
}
elseif($infos_candidat_email[0]['cand_lang_id'] == 2){
	include('../config/langs/EN.php');
}
else{
	include('../config/langs/FR.php');
}

$sql_recup_bpm	= "select distinct questionnaire.quest_id,quest_type_question_code_id from recup_quest,questionnaire,cand_ope
where recup_quest.txt_img_quest_id=questionnaire.quest_id and cand_ope.prod_id=recup_quest.prod_id and cand_id=".txt_db(intval($_GET['candid']))." and ope_id=".txt_db(intval($_GET['opeid']));
$qry_recup_bpm = $db->query($sql_recup_bpm);
											
$sql_infos_cert = "SELECT CERT_EMAIL, CERT_PRENOM, CERT_NOM, CERT_AD1, CERT_AD2, CERT_CP, CERT_VILLE, CERT_TEL, PART_NOM, PART_AD1, PART_AD2, PART_CP, PART_VILLE, PART_TEL, PART_FAX, CERT_AUTRE_NOM_PART FROM PARTENAIRE,CERTIFIE WHERE PARTENAIRE.PART_ID=CERTIFIE.CERT_PART_ID AND CERT_ID=".$infos_candidat[0]['cert_id_ini'];
$qry_infos_cert	= $db->query($sql_infos_cert);	
$date = "''";


	$sql_cand_mgr = "SELECT distinct * FROM CAND_A_OPE  WHERE ope_id='".txt_db(intval($_GET['opeid']))."' AND NIVEAU = 1";
	$qry_cand_mgr = $db->query($sql_cand_mgr);		

    $sql_infos_manager = "SELECT * FROM CANDIDAT WHERE CAND_ID='".txt_db(intval($qry_cand_mgr[0]['cand_id']))."' AND CAND_ACTIF='1'";
	$infos_manager = $db->query($sql_infos_manager);

    $sql_infos_autre = "SELECT distinct * FROM CAND_A_OPE  WHERE ope_id='".txt_db(intval($_GET['opeid']))."' AND CAND_ID='".txt_db(intval($_GET['candid']))."'";
	$infos_manager_autre = $db->query($sql_infos_autre);

	$manager = $infos_manager[0]['cand_prenom'] . ' ' . $infos_manager[0]['cand_nom'];
	
	
if ($infos_candidat[0]['code_acces'] != ''){
$mailto = $infos_candidat_email[0]['cand_email'];
//echo $mailto;
//$mailto = "eric.lefevre@risc-group.com";
$sujet_mail = $t_gen_prod_renvoi_0;

$name_s    		 = $qry_infos_cert[0]['cert_prenom']." ".$qry_infos_cert[0]['cert_nom'];
$email_s   		 = $qry_infos_cert[0]['cert_email'];
//$email_s 		 = "Diffusion_Developpement-ITS@risc-group.biz";

//$headers    	 = "From: ". $name_s . " <" . $email_s . ">\r\n";
//$headers        .= 'Bcc: alefebvre@vakom.fr,'.$email_s."\r\n";							
//$headers   		.= 'Content-Type: text/html; charset="iso-8859-1"'."\r\n";
//$headers  		.= 'Content-Transfer-Encoding: 8bit'."\r\n";
/*
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br><br>
".$t_gen_prod_12."<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;	font-weight:700\" color=\"#FF6600\">".$infos_candidat[0]['code_acces']."</font><br><br>".$t_gen_prod_renvoi_6."
<br>
</font>
*/

if ($qry_infos_cert[0]['cert_autre_nom_part'] != '')
	$nom_part_affiche = $qry_infos_cert[0]['cert_autre_nom_part'];
else
	$nom_part_affiche = $qry_infos_cert[0]['part_nom'];
	
if ($qry_recup_bpm[0]['quest_type_question_code_id'] == 12000)
{
	if ($infos_manager_autre[0]['niveau'] == '1')
	{
		$sujet_mail = $sujet_mail.' BPM';	
		$pour_votre = "";
		$t_gen_prod_renvoi_2_tmp = $t_gen_prod_renvoi_2b;
	}
	else
	{
		$sujet_mail = $sujet_mail.' BPM pour '.$manager;
		$pour_votre = " pour votre manager ".$manager;
		$t_gen_prod_renvoi_2_tmp = $t_gen_prod_renvoi_2;
	}

	$headers    	 = "From: Questionnaire BPM <questionnaire@lesensdelhumain.com>\r\n";

	$message_mail	 = "<html>

<body>

<p><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">".$t_gen_prod_renvoi_1." </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#307bae\">".ucfirst($infos_candidat[0]['cand_prenom'])." ".strtoupper($infos_candidat[0]['cand_nom'])."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">,<br>
<br>
".$t_gen_prod_renvoi_2_tmp." </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#307bae\">".$infos_candidat[0]['prod_nom'].$pour_votre.".</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"> <br>".$t_gen_prod_renvoi_3."
<br><br>
".$t_gen_prod_renvoi_7."<br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
text-decoration:underline\" color=\"#0000FF\"><a href=\"http://www.lesensdelhumain.com/public/passerelle.php?case=2&acces=".$infos_candidat[0]['code_acces']."\">".$t_gen_prod_renvoi_5."</a></font>
<br><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">
<br>".$t_gen_prod_renvoi_8."<br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#307bae\">".$qry_infos_cert[0]['cert_prenom']." ".$qry_infos_cert[0]['cert_nom']."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#307bae\">
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<br>
<img border=\"0\" src=\"http://www.extranet.lesensdelhumain.com/images/logo-bpm.png\">
<br><b>".$nom_part_affiche."</b> <br>
".$qry_infos_cert[0]['cert_ad1']." ".$qry_infos_cert[0]['cert_ad2']." <br>
".$qry_infos_cert[0]['cert_cp']." ".$qry_infos_cert[0]['cert_ville']."<br>
Tel : ".$qry_infos_cert[0]['cert_tel']."<br>
</font>
</p>

</body>

</html>";
	
} else {
	$headers    	 = "From: Questionnaire OPR <questionnaire@lesensdelhumain.com>\r\n";
	
	$message_mail	 = "<html>

<body>

<p><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">".$t_gen_prod_renvoi_1." </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".ucfirst($infos_candidat[0]['cand_prenom'])." ".strtoupper($infos_candidat[0]['cand_nom'])."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">,<br>
<br>
".$t_gen_prod_renvoi_2." </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$infos_candidat[0]['prod_nom'].".</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"> <br>".$t_gen_prod_renvoi_3." <br><br>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
text-decoration:underline\" color=\"#0000FF\"><a href = \"http://www.vakom.fr/category/nos-activites/optimisation-du-potentiel-relationnel/\">".$t_gen_prod_10."</a>
</font>
<br><br>".$t_gen_prod_renvoi_4." :<br>
".$t_gen_prod_renvoi_7."<br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
text-decoration:underline\" color=\"#0000FF\"><a href=\"http://www.lesensdelhumain.com/public/passerelle.php?case=2&acces=".$infos_candidat[0]['code_acces']."\">".$t_gen_prod_renvoi_5."</a></font>
<br><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">
<br>".$t_gen_prod_renvoi_8."<br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$qry_infos_cert[0]['cert_prenom']." ".$qry_infos_cert[0]['cert_nom']."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<br>
<img border=\"0\" src=\"http://www.extranet.lesensdelhumain.com/images/logo-miniopr.jpg\">
<br><b>".$nom_part_affiche."</b> <br>
".$qry_infos_cert[0]['cert_ad1']." ".$qry_infos_cert[0]['cert_ad2']." <br>
".$qry_infos_cert[0]['cert_cp']." ".$qry_infos_cert[0]['cert_ville']."<br>
Tel : ".$qry_infos_cert[0]['cert_tel']."<br>
</font>
</p>

</body>

</html>";
	
}

$headers        .= 'Cc: '. $email_s . "\r\n";							
$headers        .= 'Bcc: sgueguen@vakom.fr'."\r\n";							
$headers   		.= 'Content-Type: text/html; charset="iso-8859-1"'."\r\n";
$headers  		.= 'Content-Transfer-Encoding: 8bit'."\r\n";
								

mail($mailto, $sujet_mail, $message_mail, $headers);
echo '<?php echo $t_mail_envoye ?>';
}
else
{
echo '<?php echo $t_quest_renseigne ?>';
}
?>
<script type="text/javascript">
	//window.close();
	setTimeout('self.close()',1000 );
</script>
