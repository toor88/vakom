<?php
session_start();
if ($_SESSION['droit']>3){
	if ($_SESSION['part_id']>0 && $_SESSION['cert_id']>0){
	
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);
		
		if ($_GET['action']=='delete' && $_GET['contact']>0){
			$sql_delete_contact = "UPDATE CERTIFIE SET CERT_DATE_SUPPRESSION=SYSDATE, CERT_USER_SUPPRESSION_ID='".$_SESSION['cert_id']."' WHERE CERT_ID='".txt_db($_GET['contact'])."'";
			$qry_delete_contact = $db->query($sql_delete_contact);
			header('location:admsocvak_gest_Certifies.php?idnc='.($_GET['idnc']+1));
		}
		
		if ($_GET['actif']=='1' || !isset($_GET['actif'])){
			$sql_contact_actif = " AND ACTIF='1'";
		}
		
		$sql_contact = "SELECT * FROM CERTIFIE, PARTENAIRE WHERE PARTENAIRE.PART_ID='".txt_db($_SESSION['part_id'])."' AND CERTIFIE.CERT_PART_ID=PARTENAIRE.PART_ID AND CERTIFIE.CERT_DATE_SUPPRESSION IS NULL".$sql_contact_actif."";
		$contact = $db->query($sql_contact);
		if (is_array($contact)){
		?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<link rel="stylesheet" href="../css/style.css" type="text/css">		
		<script language="JavaScript">
			<!--
			function MM_openBrWindow(theURL,winName,features) { //v2.0
			  window.open(theURL,winName,features);
			}

			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function c_delete(x,y){
				if (confirm('<?php echo $t_supprimer_certif ?>')){
					document.location.href='admsocvak_gest_Certifies.php?action=delete&contact='+y;
				}
			}
			
			function show_actif(){
				if(document.getElementById('actif').checked==true){
					document.location.href='admsocvak_gest_Certifies.php?actif=1';
				}else{
					document.location.href='admsocvak_gest_Certifies.php?actif=0';
				}
			}
			//-->
		</script>
		</head>
		<body bgcolor="#FFFFFF" text="#000000">
			<?php
				include("menu_top_new.php");
			?>
<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>				
			<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Certifies"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo htmlentities($contact[0]['part_nom']); ?>
				</td>
			</tr>
			</table>
			<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="center" class="TX"> 
					<table width="900" border="0" cellspacing="0" cellpadding="2" class="TX">
					  <tr align="left"> 
						<td colspan="9" class="TX_Certifies"><?php echo $t_contacts ?></td>
					  </tr>
					  <tr align="left">
						<td colspan="9" height="1" bgcolor="#666666"> </td>
					  </tr>
					  <tr align="left"> 
						<td colspan="9" class="TX_GD"> 
						&nbsp;
						</td>
					  </tr>
					  <tr align="left"> 
						<td colspan="5" class="TX_GD"> 
						  <input type="button" name="new_ctact" value="<?php echo $t_btn_ajouter_contact ?>" class="bn_ajouter" onClick="MM_openBrWindow('admsocvak_crea_contactClient.php','Creation_Contact','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">
						</td>
						<td colspan="5" class="TX" style="text-align: right;"> 
						  <input type="checkbox" name="actif" id="actif" value="1" onClick="show_actif();" <?php if ($_GET['actif']=='1' || !isset($_GET['actif'])) echo ' checked="checked"' ?>><?php echo $t_cert_actifs_only ?>
						</td>
						</tr>
							  <tr> 
								<td class="TX_bold"><?php echo $t_nom ?></td>
								<td class="TX_bold"><?php echo $t_prenom ?></td>
								<td align="center" class="TX_bold"><?php echo $t_droits ?></td>
								<td align="left" class="TX_bold"><?php echo $t_profil_OPR ?></td>
								<td  class="TX_bold"><?php echo $t_niveau_certif ?></td>
								<td class="TX_bold"><?php echo $t_date_certif ?></td>
								<td class="TX_bold" align="center"><?php echo $t_etat ?></td>
								<td class="TX_bold" align="center"><?php echo $t_jetons ?></td>
								<td class="TX_bold" align="center"><?php echo $t_statut ?></td>
							  </tr>
							  <tr> 
								<td colspan="9" bgcolor="#CCCCCC" height="1" valign="top"></td>
							  </tr>
								<?php
								foreach($contact as $cert){
									if ($cert['cert_droit_certifie']=='1'){
										$sql_certif = "SELECT * FROM CERT_A_CERTIF, CODE WHERE CERT_A_CERTIF.CERTIF_CODE_ID=CODE.CODE_ID AND (CERTIF_CERTIFICATION IS NOT NULL OR CERTIF_FORMATION = 1 OR CERTIF_SUSPENDU= 1 ) AND CODE.CODE_TABLE='CERTIFICATION' AND CERT_A_CERTIF.CERTIF_CERT_ID='".txt_db($cert['cert_id'])."' ORDER BY CODE.CODE_LIBELLE ASC";
										//echo $sql_certif;
										$qry_certif = $db->query($sql_certif);
									}
								?>
								  <tr> 
									<td class="TX"><a href="#" onClick="MM_openBrWindow('admsocvak_edit_contactClient.php?certid=<?php echo $cert['cert_id'] ?>','edit_<?php echo$cert['cert_id']?>','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')"><?php echo htmlentities($cert['cert_nom']) ?></a></td>
									<td class="TX"><?php echo ucfirst($cert['cert_prenom']) ?></td>
									<td align="center" class="TX">
									<?php
									if ($cert['cert_droit_admin']=='1'){
										echo $t_admin;
										if ($cert['cert_droit_certifie']=='1'){
										echo '/<br>';
										}
									}
									if ($cert['cert_droit_certifie']=='1'){
										echo $t_cert;
									}
									?>
									</td>
									<td align="left" class="TX">
									<?php
									if($cert['cert_cand_id']!=''){
										$sql_opr = "SELECT LAST_OPR FROM CANDIDAT WHERE CAND_ID='".txt_db($cert['cert_cand_id'])."'";
										$qry_opr = $db->query($sql_opr);
										echo $qry_opr[0]['last_opr'];
									}
									?>
									  </td>
									<td class="TX">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												echo $certif['code_libelle'].'<br>';
											}
										}
										?>
									</td>
									<td class="TX">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												echo $certif['certif_certification'].'<br>';
											}
										}
										?>
									</td>
									<td class="TX" align="center">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												if ($certif['certif_formation']=='1'){
													echo $t_formation;
												}elseif($certif['certif_suspendu']=='1'){
													echo $t_suspendu;
												}
												echo '<br>';												
											}
										}
										?>
									</td>
									<td class="TX" align="center"><a href="#" onClick="MM_openBrWindow('admsocvak_produits_contactClient.php?certid=<?php echo $cert['cert_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes')"><?php echo $t_consultez ?></a></td>
									<td class="TX" align="center">
									<?php
									if ($cert['actif']=='1'){
										echo '<span style="color:#00FF00; font-weight: bold;">'.$t_actif.'</span>';
									}else{
										echo '<span style="color:#FF0000; font-weight: bold;">'.$t_inactif.'</span>';
									}
									?>
									</td>
								 </tr>
								  <tr> 
									<td colspan="9" bgcolor="#CCCCCC" height="1" valign="top"></td>
								  </tr>
								<?php
								}
						?>
					</table>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			</table>
		</p></div>	</article></div>	</div>	</div>	</div>			
		</body>
		</html>
		<?php
		}
	}
}else{
	include('no_acces.php');
}

?>
