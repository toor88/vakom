<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']>1 && $_SESSION['cert_id']!=''){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	$db = new db($conn);

	if ($_POST['posted']){
		// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
		$sql_seq_num 	= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
		$qry_seq_num 	= $db->query($sql_seq_num);
		$seq_num		= intval($qry_seq_num[0]['seq_num']);
		
		$sql_insert_part = "INSERT INTO CLIENT VALUES('".$seq_num."',
		'".txt_db($_POST['nom'])."',
		'".txt_db($_POST['adr1'])."',
		'".txt_db($_POST['adr2'])."',
		'".txt_db($_POST['cp'])."',
		'".txt_db($_POST['ville'])."',
		'".txt_db($_POST['select_pays'])."',
		'".txt_db($_POST['tel'])."',
		'".txt_db($_POST['fax'])."',
		'".txt_db($_POST['email'])."',
		'".txt_db($_POST['contact'])."',
		'".txt_db($_POST['comm1'])."',
		'".txt_db($_POST['comm2'])."',
		'".txt_db($_POST['actif'])."',
		'".$_SESSION['cert_id']."',
		SYSDATE,
		'',
		'')";
		
		//echo $sql_insert_part;
		$qry_insert_part = $db->query($sql_insert_part);
		
		$sql_insert_ctable	= "INSERT INTO CLIENT_A_CERT VALUES('".$seq_num."', '".txt_db(intval($_SESSION['cert_id']))."')";
		$qry_insert_ctable	= $db->query($sql_insert_ctable);
		
		if (is_array($_POST['partage'])){
			foreach($_POST['partage'] as $cert_id){
				$sql_select_ins_clt_a_cert 	= "SELECT * FROM CLIENT_A_CERT WHERE CLI_ID='".$seq_num."' AND CERT_ID='".txt_db(intval($cert_id))."'";
				$qry_select_ins_clt_a_cert	= $db->query($sql_select_ins_clt_a_cert);
				if (!is_array($qry_select_ins_clt_a_cert)){
					$sql_ins_clt_a_cert 		= "INSERT INTO CLIENT_A_CERT VALUES('".$seq_num."', '".txt_db(intval($cert_id))."')";
					$qry_ins_clt_a_cert			= $db->query($sql_ins_clt_a_cert);
				}
			}
		}
		
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				document.location.href='admcertif_edit_certifieClient.php?cliid='+<?php echo $seq_num ?>;
				window.close();
			</script>		
			<?php
	}


		$sql_info_cert = "SELECT * FROM CERTIFIE WHERE CERT_ID='".txt_db($_SESSION['cert_id'])."' AND CERT_DATE_SUPPRESSION IS NULL";
		$qry_info_cert = $db->query($sql_info_cert);

		$sql_pays_list = "SELECT * FROM CODE WHERE CODE_TABLE='PAYS'";
		$qry_pays_list = $db->query($sql_pays_list);

	if (is_array($qry_info_cert)){
	
		/* On sélectionne tous les certifiés du partenaire */
		$sql_list_certifie = "SELECT CERT_NOM, CERT_PRENOM, CERT_ID FROM CERTIFIE, PARTENAIRE WHERE CERT_DATE_SUPPRESSION IS NULL AND PARTENAIRE.PART_ID=CERTIFIE.CERT_PART_ID AND PARTENAIRE.PART_ID='".$qry_info_cert[0]['cert_part_id']."'";
		$qry_list_certifie = $db->query($sql_list_certifie);
		?>
		<html>
			<head>
				<title>Vakom</title>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<link rel="stylesheet" href="../css/nvo.css" type="text/css">
				<link rel="stylesheet" href="../css/general.css" type="text/css">
				<script language="JavaScript">
					<!--
					function che_all(field){
						if (document.form.select_all.checked==true){
							field.checked=true;
							for (i=0;i<field.length;i++)
							{
							field[i].checked = true;
							}
						}
						if (document.form.select_all.checked==false){
							field.checked=false;
							for (i=0;i<field.length;i++)
							{
							field[i].checked = false;
							}
						}
					}
					function verif(){
						error = '';
						if (document.form.nom.value == ''){
							error += "<?php echo $t_nom_oblig ?>\n";
						}
						if (error!=''){
							alert(error);
						}else{
							document.form.submit();
						}
					}
					//-->
				</script>
			</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#" name="form">
		  <table border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td align="left" class="TX"> 
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $t_fiche_new_clt ?></td>
					</tr>
				   </table>
				<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="800">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
				  <tr> 
					<td width="14"></td>
					<td align="center" valign="top" class="TX"> 
					  <table border="0" cellspacing="0" cellpadding="0">
						<tr> 
						  <td class="TX">&nbsp;</td>
						  <td class="champsoblig" valign="middle" align="right"><?php echo $t_champs_oblig	?> * </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_fiche_ent_nom ?>* :&nbsp;</td>
						  <td> 
							<input type="text" name="nom" size="70" maxlength="70" class="form_ediht_Candidats">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_adresse ?> :</td>
						  <td> 
							<input type="text" name="adr1" size="70" maxlength="70" class="form_ediht_Candidats">
						  </td>
						</tr>
						<tr> 
						  <td class="TX">&nbsp;</td>
						  <td class="TX"> 
							<input type="text" name="adr2" size="70" maxlength="70" class="form_ediht_Candidats">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_cp ?>:</td>
						  <td class="TX"> 
							<input type="text" name="cp" size="5" maxlength="10" class="form_ediht_Candidats">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $t_ville ?> 
							<input type="text" name="ville" size="50" maxlength="50" class="form_ediht_Candidats">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_pays ?>* :</td>
						  <td> 
							  <select name="select_pays" class="form_ediht_Candidats">
								<?php
								  if (is_array($qry_pays_list)){
									foreach($qry_pays_list as $pays){
										echo '<option value="'.$pays['code_id'].'">'.$pays['code_libelle'].'</option>';
									}
								  }
								?>
							  </select>
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_tel ?> :</td>
						  <td class="TX"> 
							<input type="text" name="tel" size="25" maxlength="25" class="form_ediht_Candidats">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $t_fax ?> 
							: 
							<input type="text" name="fax" size="25" maxlength="25" class="form_ediht_Candidats">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_email ?>&nbsp;&nbsp;&nbsp;: </td>
						  <td> 
							<input type="text" name="email" size="25" maxlength="70" class="form_ediht_Candidats">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_contact ?> :</td>
						  <td> 
							<input type="text" name="contact" size="70" maxlength="70" class="form_ediht_Candidats">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40" valign="top"><?php echo $t_comment ?> :</td>
						  <td> 
							<input type="text" name="comm1" size="70" maxlength="70" class="form_ediht_Candidats">
							<br>
							<input type="text" name="comm2" size="70" maxlength="70" class="form_ediht_Candidats">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_fiche_clt_actif ?> :</td>
						  <td class="TX"> 
							<input type="checkbox" name="actif" value="1" checked="checked">
						  </td>
						</tr>
						<tr> 
						  <td class="TX">&nbsp;</td>
						  <td class="TX">&nbsp;</td>
						</tr>
						
						<tr> 
						  <td class="TX" height="40" valign="top"><?php echo $t_fiche_soc_partage ?> 
							:&nbsp;&nbsp; </td>
						  <td class="TX"> 
							<table width="100%" border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000">
							  <tr> 
								<td class="TX_bold"><?php echo $t_certs1 ?></td>
								<td class="TX_bold" align="center"><?php echo $t_selection ?></td>
							  </tr>
							  <tr bgcolor="F1F1F1"> 
								<td>&nbsp;</td>
								<td align="center"> 
								  <input type="checkbox" name="select_all" id="select_all" onclick="che_all(document.form.partage)" value="1">
								  <?php echo $t_tous ?></td>
							  </tr>
							  <?php
							  if (is_array($qry_list_certifie)){
								  foreach($qry_list_certifie as $list_certifie){
									?>
									  <tr bgcolor="F1F1F1"> 
										<td><?php echo ucfirst($list_certifie['cert_prenom']).' '.strtoupper($list_certifie['cert_nom'])?></td>
										<td align="center"> 
										  <input type="checkbox" name="partage[]" id="partage" value="<?php echo $list_certifie['cert_id'] ?>" <?php if ($list_certifie['cert_id'] == $_SESSION['cert_id']){ echo 'checked="checked"';}?>>
										</td>
									  </tr>
									<?php
								  }
							  }
							  ?>
							</table>
						  </td>
						</tr>
						
					  </table>
					  <p>&nbsp;</p>
					</td>
					<td width="14"></td>
				  </tr>
				  <tr> 
					<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				  </tr>
				</table>
			  </td>
			</tr>
			<tr>
			  <td align="center">&nbsp;</td>
			</tr>
			<tr> 
			  <td align="center"> 
				<input type="hidden" name="posted" value="1">
				<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_candidat" onclick="verif();">
			  </td>
			</tr>
			<tr> 
			  <td align="right" width="180">&nbsp; </td>
			</tr>
		  </table>
		</form>
		</body>
		</html>
	<?php
	}
}else{
	include('no_acces.php');
}
?>