<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo.css" type="text/css">
<link rel="stylesheet" href="../css/general.css" type="text/css">
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
<form method="post" action="">
  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td align="center"> 
        <?php
	include("menu_top_supadmin.php");
?>
      </td>
    </tr>
  </table>
  <br>
  <table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
    <tr> 
      <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
      <td height="14"></td>
      <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
    </tr>
    <tr> 
      <td width="14"></td>
      <td align="left" class="TX"> 
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr> 
            <td class="TX_rose" align="left">CERTIFI&Eacute;S</td>
          </tr>
          <tr> 
            <td height="1" bgcolor="#666666"></td>
          </tr>
          <tr>
            <td class="TX" align="center">&nbsp;</td>
          </tr>
          <tr> 
            <td class="TX" align="center">S&eacute;lectionner le Partenaire : 
              <select name="select2" class="form_ediht">
                <option selected>RISC GROUP</option>
                <option>Autres entreprises (VAKOM)</option>
              </select>
            </td>
          </tr>
        </table>
      </td>
      <td width="14"></td>
    </tr>
    <tr> 
      <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
      <td height="14"></td>
      <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
    </tr>
  </table>
  <br>
  <p align="center">
  <input type="submit" name="valid_1" value="Valider" class="bn_ajouter" onClick="MM_goToURL('parent','supadmin_gestion_contactsClients2.php');return document.MM_returnValue">
  </p><br>
  <table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
    <tr> 
      <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
      <td height="14"></td>
      <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
    </tr>
    <tr> 
      <td width="14"></td>
      <td align="center" class="TX"> 
        <table width="900" border="0" cellspacing="0" cellpadding="2" class="TX">
          <tr align="center"> 
            <td colspan="9" class="TX_GD"> RISC GROUP</td>
          </tr>
          <tr align="left"> 
            <td colspan="9" class="TX_rose">CONTACTS DU PARTENAIRE</td>
          </tr>
          <tr align="left"> 
            <td colspan="9" height="1" bgcolor="#666666"></td>
          </tr>
          <tr align="left">
            <td colspan="9" class="TX_GD">&nbsp;</td>
          </tr>
          <tr align="left"> 
            <td colspan="9" class="TX_GD"> 
              <input type="button" name="new_ctact" value="Ajouter un contact" class="bn_ajouter" onClick="MM_openBrWindow('admvak_nvo_contactClient.php','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">
            </td>
          </tr>
          <tr align="center"> 
            <td height="1" colspan="9" bgcolor="#000000" ></td>
          </tr>
          <tr> 
            <td  class="TX_bold">Nom</td>
            <td  class="TX_bold">Pr&eacute;nom</td>
            <td  align="center" class="TX_bold">Droits</td>
            <td  align="left" class="TX_bold">Profil <br/>
              OPR</td>
            <td  class="TX_bold">Niveau de <br/>
              certification</td>
            <td  class="TX_bold">Date de <br/>
              certification</td>
            <td  class="TX_bold" align="center">Etat</td>
            <td  class="TX_bold" align="center">Produits</td>
            <td  class="TX_bold">&nbsp;</td>
          </tr>
          <tr> 
            <td height="1" bgcolor="#000000"></td>
            <td height="1" bgcolor="#000000"></td>
            <td height="1" bgcolor="#000000"></td>
            <td height="1" bgcolor="#000000" align="left"></td>
            <td height="1" bgcolor="#000000"></td>
            <td height="1" bgcolor="#000000"></td>
            <td height="1" bgcolor="#000000"></td>
            <td height="1" bgcolor="#000000"></td>
            <td height="1" bgcolor="#000000"></td>
          </tr>
          <tr> 
            <td valign="top"><a href="#" onClick="MM_openBrWindow('admvak_nvo_contactClient.php','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=500')">BENSIMON</a></td>
            <td valign="top">Gilles</td>
            <td valign="top" align="center">Administrateur</td>
            <td valign="top" align="left">2.3.1</td>
            <td valign="top" align="center">-</td>
            <td valign="top" align="center">-</td>
            <td align="center" valign="top">-</td>
            <td align="center" valign="top">-</td>
            <td align="center"><a href="#"><img src="../images/icon_supp2.gif" border="0" width="11" height="12"></a></td>
          </tr>
          <tr> 
            <td colspan="9" bgcolor="#CCCCCC" height="1"></td>
          </tr>
          <tr> 
            <td valign="top">BERNARDINI</td>
            <td valign="top">Edith</td>
            <td valign="top" align="center">Administrateur/<br>
              Certifi&eacute;e</td>
            <td valign="top" align="left">2.2.1</td>
            <td valign="top">Niveau 1<br>
              Niveau 2<br>
              Niveau PRO</td>
            <td valign="top">10/06/2008<br>
              22/10/2008<br>
              07/01/2009</td>
            <td valign="top" align="center">-<br>
              -<br />
              Formation </td>
            <td align="center"><a href="admvak_produits_contactClient.php" target="_blank">Détails</a></td>
            <td align="center"><a href="#"><img src="../images/icon_supp2.gif" border="0" width="11" height="12"></a></td>
          </tr>
          <tr> 
            <td colspan="9" bgcolor="#CCCCCC" height="1" valign="top"></td>
          </tr>
          <tr> 
            <td valign="top">ROSIER</td>
            <td valign="top">Am&eacute;lie</td>
            <td valign="top" align="center">Certifi&eacute;e</td>
            <td valign="top" align="left">3.1.2</td>
            <td valign="top">Niveau 1<br>
              Niveau 2</td>
            <td valign="top">08/10/2007<br>
              25/07/2008</td>
            <td valign="top" align="center">-<br>
              Suspendu </td>
            <td align="center">D&eacute;tails</td>
            <td align="center"><a href="#"><img src="../images/icon_supp2.gif" border="0" width="11" height="12"></a></td>
          </tr>
          <tr> 
            <td colspan="9" bgcolor="#CCCCCC" height="1" valign="top"></td>
          </tr>
          <tr> 
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
            <td valign="top" align="center">&nbsp;</td>
            <td valign="top" align="left">&nbsp;</td>
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
            <td valign="top" align="center">&nbsp;</td>
            <td align="center" valign="top">&nbsp;</td>
            <td align="center" valign="top">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="14"></td>
    </tr>
    <tr> 
      <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
      <td height="14"></td>
      <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
    </tr>
  </table>
<p>&nbsp;</p></form>
</body>
</html>
