<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']>5){

		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");

		$db = new db($conn);
		
		header('Content-type: text/html; charset=UTF-8'); 
		
		if ($_GET['partid']>0 && $_GET['contrat']>0){
		
			if ($_GET['action']=='del'){
				$sql_act_contrat	= "DELETE FROM PART_A_CONTRAT WHERE CONT_ID='".txt_db(intval($_GET['contrat']))."' AND CONT_PART_ID='".txt_db(intval($_GET['partid']))."'";
			}else{
				$sql_act_contrat	= "INSERT INTO PART_A_CONTRAT VALUES(SEQ_ID.NEXTVAL,
				'".txt_db(intval($_GET['partid']))."', 
				'".txt_db(intval($_GET['contrat']))."', 
				TO_DATE('".txt_db($_GET['JJ'])."/".txt_db($_GET['MM'])."/".txt_db($_GET['AAAA'])."', 'DD/MM/YYYY'), 
				TO_DATE('".txt_db($_GET['JJ2'])."/".txt_db($_GET['MM2'])."/".txt_db($_GET['AAAA2'])."', 'DD/MM/YYYY'), 
				'".txt_db(intval($_GET['exclu']))."', 
				'".txt_db(str_replace('.', ',', $_GET['montant']))."', 
				'".txt_db(intval($_GET['redev']))."', 
				'".txt_db(utf8_decode($_GET['info3']))."', 
				'".txt_db(utf8_decode($_GET['info4']))."')";
			}
			//echo $sql_insert_contrat;
			$qry_act_contrat		= $db->query($sql_act_contrat);
		}
		
		// Chargement des contrats du partenaire
		$sql_contrat 		= "SELECT CODE.CODE_LIBELLE, PART_A_CONTRAT.*, TO_CHAR(PART_A_CONTRAT.CONT_DATE_DEB, 'DD/MM/YYYY') CONT_DATE_DEB, TO_CHAR(PART_A_CONTRAT.CONT_DATE_FIN, 'DD/MM/YYYY') CONT_DATE_FIN FROM PART_A_CONTRAT, CODE WHERE CODE.CODE_ID=PART_A_CONTRAT.CONT_TYPE_CONTRAT_CODE_ID AND PART_A_CONTRAT.CONT_PART_ID='".txt_db($_GET['partid'])."'";
		$qry_contrat 		= $db->query($sql_contrat);
		
		?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="TX">
			<tr align="center"> 
			  <td colspan="9" bgcolor="#000000" height="1"></td>
			</tr>
			<tr align="center"> 
				<td class="TX_bold"><?php echo $t_part_typ_contrat ?></td>
				<td class="TX_bold"><?php echo $t_part_date_debut ?></td>
				<td class="TX_bold"><?php echo $t_info3 ?></td>
				<td class="TX_bold"><?php echo $t_part_date_fin ?></td>
				<td class="TX_bold"><?php echo $t_part_exclu_terr ?></td>
				<td class="TX_bold"><?php echo $t_part_montant_droit_e ?></td>
				<td class="TX_bold"><?php echo $t_part_redevance_ann ?></td>
				<td class="TX_bold"><?php echo $t_info4 ?></td>
				<td class="TX_bold"><?php echo $t_suppr_abr ?></td>
			</tr>
			<tr align="center"> 
			  <td colspan="9" bgcolor="#000000" height="1"></td>
			</tr>
			<tr align="center"> 
			  <td colspan="9" height="6"></td>
			</tr>
			<?php
			if (is_array($qry_contrat)){
				foreach($qry_contrat as $contrat){
					?>
					<tr align="center"> 
					  <td><a href="admvak_edit_client.php?<?php echo 'partid='.$_GET['partid'].'&modif=1'.'&dateBegins='.
					  $contrat['cont_date_deb'].'&dateEnds='.$contrat['cont_date_fin'].'&exclu='.$contrat['cont_exclusivite'].
					  '&montant='.$contrat['cont_montant'].'&redevance='.$contrat['cont_redevance'].
					  '&info1='.$contrat['cont_info1'].'&info2='.$contrat['cont_info2']
					  ;?>" onclick="del_contrat(<?php echo $contrat['cont_id'] ?>, 1);">
					  <?php echo $contrat['code_libelle'] ?></a></td>
					  <td><?php echo $contrat['cont_date_deb'] ?></td>
					  <td><?php echo $contrat['cont_info1'] ?></td>
					  <td><?php echo $contrat['cont_date_fin'] ?></td>
					  <td>
					  <?php
					  if ($contrat['cont_exclusivite']=='1'){
						$checked1 = ' checked="checked" ';
					  }else{
						$checked1 = ' ';
					  }
					  ?>
						<input type="checkbox" disabled="disabled"<?php echo $checked1 ?>name="redev2" value="checkbox">
					  </td>
					  <td><?php echo $contrat['cont_montant'] ?> &euro; HT </td>
					  <td>
					  <?php
					  if ($contrat['cont_redevance']=='1'){
						$checked2 = ' checked="checked" ';
					  }else{
						$checked2 = ' ';
					  }
					  ?>
						<input type="checkbox" disabled="disabled"<?php echo $checked2 ?>name="redev" value="checkbox">
					  </td>
					  <td><?php echo $contrat['cont_info2'] ?></td>
					  <td><img src="../images/icon_supp2.gif" onmouseover="this.style.cursor='pointer'" onclick="del_contrat(<?php echo $contrat['cont_id'] ?>);"></td>
					</tr>
					<tr align="center" bgcolor="CCCCCC"> 
					  <td colspan="9" align="left" height="1"></td>
					</tr>
					<?php
				}
			}
			?>
			<tr align="center"> 
			  <td colspan="9" align="left">&nbsp;</td>
			</tr>
		  </table>
		<?php
}
echo ':_:_:_:';
?>