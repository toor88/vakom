<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	$tab_style = array(
		0=>'pointillé',
		1=>'continu');
	
	
	if ($_GET['grapheid']>0){
	
		/* On fait la liste des langues répertoriées dans la base */
		$sql_lang_list	= "SELECT * FROM LANGUE ORDER BY LANG_LIBELLE ASC";
		$qry_lang_list	= $db->query($sql_lang_list);
		
		if ($_GET['action'] == 'delete_gquest' && $_GET['gquestid']>0){
		
			/* On supprime le questionnaire assigné au graphique */
			$sql_delete_gquest = "DELETE FROM GRAPHE_A_QUEST WHERE GRAPHE_A_QUEST_ID='".txt_db(intval($_GET['gquestid']))."'";
			$qry_delete_gquest = $db->query($sql_delete_gquest);
			
			/* On supprime les libellés connexes */
			$sql_delete_libelles = "DELETE FROM GRAPHE_A_LIB WHERE GRAPHE_A_QUEST_ID='".txt_db(intval($_GET['gquestid']))."'";
			$qry_delete_libelles = $db->query($sql_delete_libelles);
			
			header('location:graphes2.php?grapheid='.$_GET['grapheid'].'&langid='.$_GET['langid']);
		}
		
		/* Si la variable de langue n'est pas définie, la langue est le FR */
		if ($_GET['langid']<1){
			$langid 		= '1';
		}else{
			$langid			= intval($_GET['langid']);
		}
		
		/* On sélectionne le nom de la langue qui correspond a l'id de la langue passé en GET */ 
		$sql_info_lang	= "SELECT LANG_LIBELLE FROM LANGUE WHERE LANG_ID='".txt_db($langid)."'";	
		$qry_info_lang	= $db->query($sql_info_lang);
		
		if ($_POST['nom_graphe']!=''){ // Si le formulaire est posté 
			/* On vérifie si les textes sont déjà présents pour la langue demandée */
			$sql_verif_texte	= "SELECT GRAPHE_ID FROM GRAPHE_A_TEXTE WHERE GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."' AND LANG_ID='".txt_db(intval($langid))."'";
			$qry_verif_texte 	= $db->query($sql_verif_texte);
			
			if (is_array($qry_verif_texte)){
				$sql_up_textes	= "UPDATE GRAPHE_A_TEXTE SET 
				ENTETE_COL1 = '".txt_db($_POST['entete_col1'])."',
				ENTETE_COL2 = '".txt_db($_POST['entete_col2'])."',
				ENTETE_COL3 = '".txt_db($_POST['entete_col3'])."',
				ENTETE_COL4 = '".txt_db($_POST['entete_col4'])."',
				TITRE = '".txt_db($_POST['titre'])."',
				PIED_COL1 = '".txt_db($_POST['pied_col1'])."',
				PIED_COL2 = '".txt_db($_POST['pied_col2'])."',
				PIED_COL3 = '".txt_db($_POST['pied_col3'])."',
				PIED_COL4 = '".txt_db($_POST['pied_col4'])."'
				WHERE LANG_ID='".txt_db($langid)."' AND GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
				$qry_up_textes	= $db->query($sql_up_textes);
			}else{			
				/* On enregistre les textes pour la langue choisie */
				$sql_insert_textes	= "INSERT INTO GRAPHE_A_TEXTE VALUES('".txt_db(intval($_GET['grapheid']))."', 
				'".txt_db($langid)."', 
				'".txt_db($_POST['entete_col1'])."',
				'".txt_db($_POST['entete_col2'])."',
				'".txt_db($_POST['entete_col3'])."',
				'".txt_db($_POST['entete_col4'])."',
				'".txt_db($_POST['titre'])."',
				'".txt_db($_POST['pied_col1'])."',
				'".txt_db($_POST['pied_col2'])."',
				'".txt_db($_POST['pied_col3'])."',
				'".txt_db($_POST['pied_col4'])."')";
				$qry_insert_textes	= $db->query($sql_insert_textes);
			}
			/* On update les infos de base pour le graphe */
			$sql_up_graphe = "UPDATE GRAPHE SET GRAPHE_NOM = '".txt_db($_POST['nom_graphe'])."', GRAPHE_TYPE_GRAPHE_CODE_ID='".txt_db($_POST['type_graphe'])."' WHERE GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
			$qry_up_graphe	= $db->query($sql_up_graphe);
		}
		
		/* On sélectionne le graphe voulu */
		$sql_graphe = "SELECT * FROM GRAPHE WHERE GRAPHE_ID='".txt_db($_GET['grapheid'])."'";
		$qry_graphe = $db->query($sql_graphe);
	
		/* On sélectionne tous les textes du graphe sélectionné, par langue */
		$sql_graphe_texte	= "SELECT * FROM GRAPHE_A_TEXTE WHERE GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."' AND LANG_ID='".txt_db(intval($langid))."'";
		$qry_graphe_texte 	= $db->query($sql_graphe_texte);
		
		/* On sélectionne la liste de tous les types de graphes disponibles */
		$sql_type_graphe_liste = "SELECT * FROM CODE WHERE CODE_TABLE = 'TYPE_GRAPHE' ORDER BY CODE_LIBELLE ASC";
		$qry_type_graphe_liste = $db->query($sql_type_graphe_liste);
		
		/* On sélectionne tous les questionnaires qui utilisent le graphe */
		$sql_quest_graphe = "SELECT CODE.CODE_LIBELLE, GRAPHE_A_QUEST.*, QUESTIONNAIRE.QUEST_NOM, QUESTIONNAIRE.QUEST_ID FROM CODE, GRAPHE_A_QUEST, QUESTIONNAIRE WHERE CODE.CODE_ID=GRAPHE_A_QUEST.TYPE_PROFIL_CODE_ID AND CODE.CODE_TABLE='TYPE_PROFIL' AND GRAPHE_A_QUEST.QUEST_ID = QUESTIONNAIRE.QUEST_ID AND QUESTIONNAIRE.QUEST_DATE_SUPPRESSION IS NULL AND GRAPHE_A_QUEST.GRAPHE_ID='".txt_db($qry_graphe[0]['graphe_id'])."' ORDER BY QUESTIONNAIRE.QUEST_NOM ASC";
		$qry_quest_graphe = $db->query($sql_quest_graphe);
		
		?>
		<!DOCTYPE html>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<link rel="stylesheet" href="../css/style.css" type="text/css">
		<strong><script src="https://cdn.tiny.cloud/1/n15lggt937iydpcbh026tvnj0a11vtiwcjd2tdoxiaf4y4da/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script></strong>
		<script type="text/javascript">
		<!--
		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}

		function MM_openBrWindow(theURL,winName,features) { //v2.0
		  window.open(theURL,winName,features);
		}
		
		function delete_quest_graphe(gquestid){
			if (confirm('<?php echo $t_supprimer_quest ?>')){
				document.location.href='graphes2.php?action=delete_gquest&langid=<?php echo $_GET['langid'] ?>&grapheid=<?php echo $_GET['grapheid'] ?>&gquestid='+gquestid;
			}
		}
		//-->
		tinymce.init({
			selector: 'textarea',
			height: 300,
			width:700,
			plugins: [
			  'advlist autolink link image lists charmap print preview hr anchor pagebreak',
			  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			  'table emoticons template paste help'
			],
			toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
			  'bullist numlist outdent indent | link image | print preview media fullpage | ' +
			  'forecolor backcolor emoticons | help',
			menu: {
			  favs: {title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons'}
			},
			menubar: 'favs file edit view insert format tools table help',
			content_css: 'css/content.css'
		  });
		function uploadFile(inp, editor) {
			var input = inp.get(0);
			var data = new FormData();
			data.append('image[file]', input.files[0]);

			$.ajax({
				url: 'uploads/post.php',
				type: 'POST',
				data: data,
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				success: function(data, textStatus, jqXHR) {
					data = jQuery.parseJSON(data);
					editor.insertContent('<img class="content-img" src="uploads/images/' + data.location + '"/>');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if(jqXHR.responseText) {
						errors = JSON.parse(jqXHR.responseText).errors
						alert('Error uploading image: ' + errors.join(", ") + '. Make sure the file is an image and has extension jpg/jpeg/png.');
					}
				}
			});
		}			
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#">
			<?php
			include('menu_top_new.php');
			?>
		  <br>
			<div id="page" class="hfeed site">
				<div id="main" class="site-main">
					<div id="primary" class="content-area">
						<div id="content" class="site-content" role="main">
							<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">
								<div class="entry-contentAdmin">
									<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
		<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;GRAPHES</td>
		</tr>
		</table>
		  <table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="left" style="text-align: left;" class="TX_Tarifs">Caract&eacute;ristiques&nbsp;du&nbsp;graphe</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="1"></td>
			  <td align="center" class="TX" height="1" bgcolor="#666666"></td>
			  <td width="14" height="1"></td>
			</tr>
			<tr> 
			  <td width="14" height="1"></td>
			  <td align="center" class="TX" >&nbsp;</td>
			  <td width="14" height="1"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="center" class="TX"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">Nom du graphe : 
					  <input type="text" name="nom_graphe" class="form_ediht_Tarifs" size="60" maxlength="60" value="<?php echo htmlentities($qry_graphe[0]['graphe_nom']) ?>">
					</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top" class="TX">&nbsp;</td>
					<td align="left" valign="top" class="TX">Type de graphe : 
					  <select name="type_graphe" class="form_ediht_Tarifs">
						<?php
						if (is_array($qry_type_graphe_liste)){
							foreach($qry_type_graphe_liste as $type_graphe_liste){
								unset($selected_type_G);
								if ($qry_graphe[0]['graphe_type_graphe_code_id'] == $type_graphe_liste['code_id']){
									$selected_type_G = ' selected="selected"';
								}
								echo '<option value="'.$type_graphe_liste['code_id'].'"'.$selected_type_G.'>'.htmlentities($type_graphe_liste['code_libelle']).'</option>';
							}
						}else{
							echo '<option value="0">Aucun type de graphe dans la base</option>';
						}
						?>
					  </select>
					</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top"> 
					  <input type="button" name="new_ctact" value="Ajouter " class="bn_ajouter" onClick="MM_openBrWindow('graphes_ajout.php?grapheid=<?php echo $qry_graphe[0]['graphe_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=970,height=400')">
					</td>
				  </tr>
				  <tr> 
					<td valign="top" align="center">&nbsp; </td>
					<td align="left" valign="top"> 
					  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
						<tr align="center"> 
						  <td height="1" colspan="6" bgcolor="#000000" ></td>
						</tr>
						<tr> 
						  <td  align="left" class="TX_bold">Questionnaire</td>
						  <td  align="center" class="TX_bold">Type de profil</td>
						  <td  class="TX_bold">Couleur</td>
						  <td  class="TX_bold">Style</td>
						  <td  class="TX_bold" align="center">Editer</td>
						  <td  class="TX_bold" align="center">Suppr</td>
						</tr>
						<tr> 
						  <td height="1" bgcolor="#000000"></td>
						  <td height="1" bgcolor="#000000"></td>
						  <td height="1" bgcolor="#000000"></td>
						  <td height="1" bgcolor="#000000" align="left"></td>
						  <td height="1" bgcolor="#000000"></td>
						  <td height="1" bgcolor="#000000"></td>
						</tr>
						<?php
						if (is_array($qry_quest_graphe)){
							foreach($qry_quest_graphe as $quest_graphe){
								?>
								<tr> 
								  <td valign="top" align="left"><?php echo $quest_graphe['quest_nom'] ?></td>
								  <td valign="top" align="center"><?php echo $quest_graphe['code_libelle'] ?></td>
								  <td valign="top"><div style="width: 50px; background-color:<?php echo $quest_graphe['couleur'] ?>;">&nbsp;</div></td>
								  <td valign="top"><?php echo $tab_style[$quest_graphe['style']] ?></td>
								  <td align="center"><img onmouseover="this.style.cursor='pointer'" onclick="MM_openBrWindow('graphes_edit.php?grapheid=<?php echo $_GET['grapheid'] ?>&gquestid=<?php echo $quest_graphe['graphe_a_quest_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=970,height=400')" src="modifier.png" width="15" border="0"></td>
								  <td align="center"><img onmouseover="this.style.cursor='pointer'" onclick="delete_quest_graphe(<?php echo $quest_graphe['graphe_a_quest_id'] ?>)" src="../images/icon_supp2.gif" border="0" width="11" height="12"></td>
								</tr>
								<tr> 
								  <td colspan="6" bgcolor="#CCCCCC" height="1"></td>
								</tr>
								<?php
							}
						}else{
						echo '<tr> 
								<td align="center" valign="top" colspan="6">Aucun questionnaire n\'est présent.</td>
							  </tr>';
						}
						?>
						<tr> 
						  <td valign="top">&nbsp;</td>
						  <td valign="top">&nbsp;</td>
						  <td valign="top" align="center">&nbsp;</td>
						  <td valign="top" align="left">&nbsp;</td>
						  <td align="center" valign="top">&nbsp;</td>
						  <td align="center" valign="top">&nbsp;</td>
						</tr>
					  </table>
					</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>

				  <tr> 
					<td align="center" bgcolor="#EAEAEA">&nbsp; </td>
					<td align="left" bgcolor="#EAEAEA" valign="middle"><b>Saisissez vos 
					  textes en : </b>&nbsp;&nbsp; 
					  <?php
					  foreach($qry_lang_list as $lang_list){
						echo '<input type="button" value="'.htmlentities($lang_list['lang_libelle']).'" onclick="document.location.href=\'graphes2.php?grapheid='.$_GET['grapheid'].'&langid='.$lang_list['lang_id'].'\'" class="bn_Liens">&nbsp;';
					  }
					  ?>
					  
					</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="center" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="center">&nbsp; </td>
					<td align="left">Ent&ecirc;tes de colonnes (<?php echo $qry_info_lang[0]['lang_libelle'] ?>) :</td>
				  </tr>
				  <tr> 
					<td valign="top" align="center">&nbsp; </td>
					<td align="center" valign="top"><br>
					  <table width="100%" border="0" cellspacing="0" cellpadding="0" id="table6">
						<tr align="center"> 
						  <td> 
							<textarea name="entete_col1" rows="5" cols="20"><?php echo $qry_graphe_texte[0]['entete_col1'] ?></textarea>
						  </td>
						  <td> 
							<textarea name="entete_col2" rows="5" cols="20"><?php echo $qry_graphe_texte[0]['entete_col2'] ?></textarea>
						  </td>
						  <td> 
							<textarea name="entete_col3" rows="5" cols="20"><?php echo $qry_graphe_texte[0]['entete_col3'] ?></textarea>
						  </td>
						  <td> 
							<textarea name="entete_col4" rows="5" cols="20"><?php echo $qry_graphe_texte[0]['entete_col4'] ?></textarea>
						  </td>
						</tr>
					  </table>
					</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>

				  <tr> 
					<td align="center">&nbsp; </td>
					<td align="left">Titre (<?php echo $qry_info_lang[0]['lang_libelle'] ?>) :</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="center" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="center" valign="top">
					<textarea name="titre" id="titre" width="601" height="299" class="myTextEditor"><?php echo $qry_graphe_texte[0]['titre'] ?></textarea>
					</td>
				  </tr>
				  <tr> 
					<td valign="top" align="center">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="center">&nbsp; </td>
					<td align="left">Pieds de colonnes (<?php echo $qry_info_lang[0]['lang_libelle'] ?>) : </td>
				  </tr>
				  <tr> 
					<td valign="top" align="center">&nbsp;</td>
					<td align="center" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td valign="top" align="center">&nbsp; </td>
					<td align="center" valign="top"> 
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr align="center"> 
						  <td> 
							<textarea name="pied_col1" rows="5" cols="20"><?php echo $qry_graphe_texte[0]['pied_col1'] ?></textarea>
						  </td>
						  <td> 
							<textarea name="pied_col2" rows="5" cols="20"><?php echo $qry_graphe_texte[0]['pied_col2'] ?></textarea>
						  </td>
						  <td> 
							<textarea name="pied_col3" rows="5" cols="20"><?php echo $qry_graphe_texte[0]['pied_col3'] ?></textarea>
						  </td>
						  <td> 
							<textarea name="pied_col4" rows="5" cols="20"><?php echo $qry_graphe_texte[0]['pied_col4'] ?></textarea>
						  </td>
						</tr>
					  </table>
					  &nbsp;&nbsp;</td>
				  </tr>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
		  <br>
		  <table border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td align="center" valign="top"> 
				<input type="submit" name="Submit" value="VALIDER" class="BN">
			  </td>
			</tr>
		  </table>
		</form>
		</p></div>	</article></div>	</div>	</div>	</div>
		</body>
		</html>
		<?php
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{
	include('no_acces.php');
}
?>