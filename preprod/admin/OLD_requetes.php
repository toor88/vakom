<?php
session_start();

if($_SESSION['droit']>4){
	$id_utilisateur = $_SESSION['vak_id'];
}else{
	$id_utilisateur = $_SESSION['cert_id'];
}

if($_SESSION['droit']>5){
	$where_liste_req = "USER_ID=".intval($id_utilisateur);
}else{
	if($_SESSION['droit']==4){
		$where_liste_req = "USER_ID=".intval($id_utilisateur)." OR ((USER_ID=".$_SESSION['part_id']." AND INDIC_ADM=1) OR (USER_ID IN (SELECT VAK_ID FROM USER_VAKOM WHERE VAK_TYPE LIKE '%AV') AND INDIC_ADM=1))";
	}
	if($_SESSION['droit']==2){
		$where_liste_req = "USER_ID=".intval($id_utilisateur)." OR ((USER_ID=".$_SESSION['part_id']." AND INDIC_CERT=1) OR (USER_ID IN (SELECT VAK_ID FROM USER_VAKOM WHERE VAK_TYPE LIKE '%AV') AND INDIC_CERT=1))";
	}
}
	

// Si l'utilisateur est un super admin
if ($_SESSION['droit']>0){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	$tab_operateurs = array(
		1=>'&gt;',
		2=>'&gt; =',
		3=>'=',
		4=>'&lt;',
		5=>'&lt; =',
		6=>'Contient',
		7=>'Commence par',
		8=>'In'
	);
	
	if($_POST['step']==1){
	###################################
	// Création d'un nouveau libellé de requête

		if($_POST['choix_action'] == 1){ // Sélection
			header('location: requetes.php?req_id='.$_POST['select_req'].'&type=1');
		}
		if($_POST['choix_action'] == 2 && trim($_POST['txt_nom_req'])!=''){ // Création
			$sql_nb 		= "SELECT SEQ_ID.NEXTVAL NB FROM DUAL";
			$qry_nb 		= $db->query($sql_nb);
			$num_requete 	= $qry_nb[0]['nb'];
			
			$sql_new_req 	= "INSERT INTO USER_REQUETE (NUM_REQUETE, USER_ID, LIB_REQUETE) VALUES (".intval($num_requete).", ".intval($id_utilisateur).", '".txt_db($_POST['txt_nom_req'])."')";
			$qry_new_req	= $db->query($sql_new_req);
			
			header('location: requetes.php?req_id='.$num_requete.'&type=1');
		}
		if($_POST['choix_action'] == 3 && $_POST['select_req2']>0 && trim($_POST['txt_nom_req2'])!=''){ // Duplication
			$sql_nb 		= "SELECT SEQ_ID.NEXTVAL NB FROM DUAL";
			$qry_nb 		= $db->query($sql_nb);
			$num_requete 	= $qry_nb[0]['nb'];
			
			// On duplique les filtres
			$sql_ins_filtre	=  "insert into requete_a_filtre (select ".intval($num_requete).",num_ligne,INDIC_TABLE,INDIC_CHP,OPERATEUR,CRITERE,AND_OR from requete_a_filtre where num_requete=".intval($_POST['select_req2']).")";
			$qry_ins_filtre	= $db->query($sql_ins_filtre);
			
			// On duplique les champs
			$sql_ins_chp	=  "insert into requete_a_chp (select seq_id.nextval, ".intval($num_requete).",INDIC_TABLE,INDIC_CHP,ORDRE,TRI,AFFICHE from requete_a_chp where num_requete=".intval($_POST['select_req2']).")";
			$qry_ins_chp	= $db->query($sql_ins_chp);

			// On insère les infos de la requete
			$sql_new_req 	= "INSERT INTO USER_REQUETE (NUM_REQUETE, USER_ID, LIB_REQUETE) VALUES (".intval($num_requete).", ".intval($id_utilisateur).", '".txt_db($_POST['txt_nom_req2'])."')";
			$qry_new_req	= $db->query($sql_new_req);
			
			
			header('location: requetes.php?req_id='.$num_requete.'&type=1');
		}
		
	###################################
	}
		
	unset($selected1);
	unset($selected2);
	unset($selected3);
	unset($selected4);
	switch($_GET['type']){
		case 1:
		default:
			$selected1 	= 'checked="checked"';
			$lib_type 	= 'PARTENAIRES';
			break;
		case 2:
			$selected2 	= 'checked="checked"';
			$lib_type 	= 'PRODUITS';
			break;
		case 3:
			$selected3 	= 'checked="checked"';
			$lib_type 	= 'CERTIFI&Eacute;S';
			break;
		case 4:
			$selected4 	= 'checked="checked"';
			$lib_type 	= 'CANDIDATS';
			break;
	}
	
	if(intval($_GET['req_id'])>0 && intval($_GET['type'])>0){
	$step = 2;
	
	###################################
	// Ajout d'un champ à la requête
	if($_POST['ajouter']){
		if(is_array($_POST['che_champs'])){
			foreach($_POST['che_champs'] as $champ_coche){
				$tab_champ_coche 	= explode('_',$champ_coche);
				$indic_chp			= $tab_champ_coche[0];
				$indic_table		= $tab_champ_coche[1];
				
				// On vérifie que le champ n'existe pas dans la table
				$sql_verif 			= "SELECT * FROM REQUETE_A_CHP WHERE NUM_REQUETE=".intval($_GET['req_id'])." AND INDIC_TABLE=".intval($indic_table)." AND INDIC_CHP=".intval($indic_chp);
				$qry_verif			= $db->query($sql_verif);
				
				if(!is_array($qry_verif)){
					$sql_ins_champ 		= "INSERT INTO REQUETE_A_CHP (NUM_ID, NUM_REQUETE, INDIC_TABLE, INDIC_CHP) VALUES(SEQ_ID.NEXTVAL, ".intval($_GET['req_id']).", ".intval($indic_table).", ".intval($indic_chp).")";
					//echo $sql_ins_champ.'<br>';
					$db->query($sql_ins_champ);
				}
			}
		}
		header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&idnc='.($_GET['idnc']+1));
	}
	###################################

	

	###################################
	// Suppression d'un filtre pour la requête sélectionnée
	if($_GET['del']>0){
		$sql_delete 		= "DELETE FROM REQUETE_A_FILTRE WHERE NUM_LIGNE=".intval($_GET['del'])." AND NUM_REQUETE=".intval($_GET['req_id']);
		$qry_delete			= $db->query($sql_delete);
		
		header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&idnc='.($_GET['idnc']+1).'#3');
	}
	###################################
	
	
	###################################
	// Application d'un nouveau filtre
	if ($_POST['ajout_filtre']) {

		$tab_chp		= explode('_',$_POST['critere']);
		$indic_table 	= intval($tab_chp[0]);
		$indic_chp 		= intval($tab_chp[1]);
		$operateur 		= $_POST['operateur'];
		$valeur 		= txt_db($_POST['valeur']);
		$and_or 		= intval($_POST['and_or']);
		
		/*
		if ($_POST['num_id']){
			$sql  = "Update REQUETE_A_FILTRE SET NUM_REQUETE='".$infos['num_requete']."',INDIC_TABLE='".$infos['indic_table']."',INDIC_CHP='".$infos['indic_chp']."',OPERATEUR='".$infos['operateur']."',CRITERE='".$infos['critere']."', AND_OR='".$infos['and_or']."' Where NUM_LIGNE='".$_POST['num_id']."' ";
			//echo($sql);
			$_GET['mod'] = "";
			$requette = "";
		}*/

		$sql_ins_filtre  = "INSERT INTO REQUETE_A_FILTRE (NUM_REQUETE,NUM_LIGNE,INDIC_TABLE,INDIC_CHP,OPERATEUR,CRITERE, AND_OR) 
		VALUES (".intval($_GET['req_id']).", SEQ_ID.NEXTVAL, ".$indic_table.", ".$indic_chp.", '".$operateur."', '".$valeur."', '".$and_or."')";

		$db->query($sql_ins_filtre);
		
		header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&idnc='.($_GET['idnc']+1).'#3');
	}
	###################################
	
	
	###################################
	// Sauvegarder les champs de la requête
	if($_POST['sauver']){
		for($i=1; $i<($_POST['nb_champs']+1); $i++){
			unset($num_id);
			unset($tri);
			unset($ordre);
			
			$num_id 			= $_POST["ident$i"];
			$tri				= intval($_POST["tri$i"]);
			$ordre				= intval($_POST["ordre$i"]);
			$affiche			= intval($_POST["affiche$i"]);
			
			$sql_up_champs 		= "UPDATE REQUETE_A_CHP SET TRI=".intval($tri).", AFFICHE=".intval($affiche).", ORDRE=".intval($ordre)." WHERE NUM_ID=".intval($num_id);
			$qry_up_champs		= $db->query($sql_up_champs);	
		}
		
		if($_POST['proprio']==1){
			$sql_up_req 		= "UPDATE USER_REQUETE SET INDIC_CERT=".intval($_POST['che_droit_cert']).", INDIC_ADM=".intval($_POST['che_droit_adm'])." WHERE NUM_REQUETE=".intval($_GET['req_id']);
			$qry_up_req			= $db->query($sql_up_req);
		}
		header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&idnc='.($_GET['idnc']+1));
	}
	###################################
	
	
	###################################
	// Suppression d'un champ de la requête
	if($_POST['supprimer']){
		if(is_array($_POST['che_del'])){
			foreach($_POST['che_del'] as $champ_delete){
				$sql_del_champ = "DELETE FROM REQUETE_A_CHP WHERE NUM_ID=".intval($champ_delete);
				//echo $sql_ins_champ.'<br>';
				$db->query($sql_del_champ);
			}
		}
		header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&idnc='.($_GET['idnc']+1));
	}	
	###################################
	
	
	###################################
	// Génération de la liste des champs disponibles
		$sql_champs = "SELECT * FROM LISTE_CHP WHERE INDIC_TABLE=".intval($_GET['type'])." AND CONCAT(LISTE_CHP.INDIC_TABLE, LISTE_CHP.INDIC_CHP) NOT IN (SELECT CONCAT(REQUETE_A_CHP.INDIC_TABLE, REQUETE_A_CHP.INDIC_CHP) FROM REQUETE_A_CHP WHERE NUM_REQUETE=".intval($_GET['req_id']).") AND LIB_CHAMP IS NOT NULL ORDER BY INDIC_CHP";
		$qry_champs = $db->query($sql_champs);
		
		if(is_array($qry_champs)){
			foreach($qry_champs as $data_champs){
				$liste_champs .= '<tr bgcolor="F1F1F1">
				  <td>'.htmlentities($data_champs['lib_champ']).'</td>
				  <td align="center"> 
					<input type="checkbox" name="che_champs[]" value="'.$data_champs['indic_chp'].'_'.$data_champs['indic_table'].'">
				  </td>
				</tr>';
			}
		}else{
			$liste_champs = '<tr bgcolor="F1F1F1"><td align="center" colspan="4">Aucun champ à sélectionner.</td></tr>';
		}
		
		
	// Informations sur la requête sélectionnée
		$sql_infos_req = "SELECT * FROM USER_REQUETE WHERE USER_REQUETE.NUM_REQUETE=".intval($_GET['req_id'])." AND (".$where_liste_req.")";
		$infos_req 		= $db->query($sql_infos_req);
		$lib_requete	= $infos_req[0]['lib_requete'];
		
		if ($infos_req[0]['user_id'] == $id_utilisateur){
			$proprietaire = 1;
		}else{
			$proprietaire = 0;
		}
		
		
		
	if($_GET['edit']){
		if ($_POST['edit_filtre']){

			$tab_chp		= explode('_',$_POST['critere']);
			$indic_table 	= intval($tab_chp[0]);
			$indic_chp 		= intval($tab_chp[1]);
			$operateur 		= $_POST['operateur'];
			$valeur 		= txt_db($_POST['valeur']);
			$and_or 		= intval($_POST['and_or']);

			$sql_up_filtre  = "UPDATE REQUETE_A_FILTRE SET INDIC_TABLE = ".$indic_table.", INDIC_CHP=".$indic_chp.",OPERATEUR='".$operateur."',CRITERE='".$valeur."', AND_OR='".$and_or."' WHERE NUM_LIGNE=".intval($_GET['edit']);
			//echo $sql_up_filtre;
			$db->query($sql_up_filtre);
			
			header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&idnc='.($_GET['idnc']+1).'#filtre');
		}	
	
		$sql_champs_a_modifier 		= "SELECT * FROM LISTE_CHP, USER_REQUETE, REQUETE_A_FILTRE 
		WHERE LISTE_CHP.INDIC_CHP=REQUETE_A_FILTRE.INDIC_CHP 
		AND LISTE_CHP.INDIC_TABLE=REQUETE_A_FILTRE.INDIC_TABLE 
		AND USER_REQUETE.NUM_REQUETE=REQUETE_A_FILTRE.NUM_REQUETE AND 
		USER_REQUETE.NUM_REQUETE=REQUETE_A_FILTRE.NUM_REQUETE 
		AND USER_REQUETE.NUM_REQUETE=".intval($_GET['req_id'])." AND (".$where_liste_req.") AND NUM_LIGNE=".intval($_GET['edit']);
		$qry_champs_req_a_modifier = $db->query($sql_champs_a_modifier);
		$valeur_a_modifier 		   = $qry_champs_req_a_modifier[0]['critere'];
		$operateur_a_modifier 	   = $qry_champs_req_a_modifier[0]['operateur'];
		
		$critere_a_modifier		   = $qry_champs_req_a_modifier[0]['indic_table'].'_'. $qry_champs_req_a_modifier[0]['indic_chp'];		
		
	}
		
		
	// Génération de la liste des champs pour la requête sélectionnée
		$sql_champs_req = "SELECT * FROM USER_REQUETE, REQUETE_A_CHP, LISTE_CHP 
		WHERE LISTE_CHP.INDIC_CHP=REQUETE_A_CHP.INDIC_CHP 
		AND LISTE_CHP.INDIC_TABLE=REQUETE_A_CHP.INDIC_TABLE 
		AND USER_REQUETE.NUM_REQUETE=REQUETE_A_CHP.NUM_REQUETE 
		AND USER_REQUETE.NUM_REQUETE=".intval($_GET['req_id'])." AND (".$where_liste_req.") ORDER BY ORDRE";
		$qry_champs_req = $db->query($sql_champs_req);
		
		if(is_array($qry_champs_req)){
			$j=0;
			foreach($qry_champs_req as $data_champs_req){
				unset($checked_tri);
				unset($checked_affich);
				if(intval($data_champs_req['tri'])==1){
					$checked_tri = ' checked="checked"';
				}
				
				if(intval($data_champs_req['affiche'])==1){
					$checked_affich = ' checked="checked"';
				}
				$j++;
				$champs_req .= '<tr bgcolor="F1F1F1">
				  <td>'.htmlentities($data_champs_req['lib_champ']).'</td>
				  <td align="center"> 
					<input type="hidden" name="ident'.$j.'" value="'.intval($data_champs_req['num_id']).'"><input type="text" name="ordre'.$j.'" value="'.intval($data_champs_req['ordre']).'" class="form_ediht_Requetes" size="2">
				  </td>
				  <td align="center" valign="middle"> 
					<input type="checkbox" name="tri'.$j.'" value="1"'.$checked_tri.'>
				  </td>
				  <td align="center" valign="middle"> 
					<input type="checkbox" name="affiche'.$j.'" value="1"'.$checked_affich.'>
				  </td>
				  <td align="center" valign="middle"><input type="checkbox" name="che_del[]" value="'.intval($data_champs_req['num_id']).'"></td>
				</tr>';
				unset($champ_a_modifier);
				if($critere_a_modifier == intval($data_champs_req['indic_table']).'_'.intval($data_champs_req['indic_chp'])){
					$champ_a_modifier=' selected="selected"';
				}
				$options_req .= '<option value="'.intval($data_champs_req['indic_table']).'_'.intval($data_champs_req['indic_chp']).'"'.$champ_a_modifier.'>'.htmlentities($data_champs_req['lib_champ']).'</option>';
			}
			$champs_req .= '<input type="hidden" name="nb_champs" value="'.$j.'"';
		}else{
			$champs_req = '<tr bgcolor="F1F1F1"><td align="center" colspan="4">Aucun champ affecté à cette requête.</td></tr>';
		}
	
	
	// Génération de la liste des filtres enregistrés pour la requête sélectionnée
		$sql_champs_req = "SELECT * FROM LISTE_CHP, USER_REQUETE, REQUETE_A_FILTRE 
		WHERE LISTE_CHP.INDIC_CHP=REQUETE_A_FILTRE.INDIC_CHP 
		AND LISTE_CHP.INDIC_TABLE=REQUETE_A_FILTRE.INDIC_TABLE 
		AND USER_REQUETE.NUM_REQUETE=REQUETE_A_FILTRE.NUM_REQUETE AND 
		USER_REQUETE.NUM_REQUETE=REQUETE_A_FILTRE.NUM_REQUETE 
		AND USER_REQUETE.NUM_REQUETE=".intval($_GET['req_id'])." AND (".$where_liste_req.")";
		$qry_champs_req = $db->query($sql_champs_req);
		
		if($proprietaire==1){
			$nb_col = 4;
		}else{
			$nb_col = 3;
		}
		if(is_array($qry_champs_req)){
			foreach($qry_champs_req as $filtre){
				if($filtre['num_ligne'] != $_GET['edit']){
				$tableau_filtres	.= '<tr bgcolor="F1F1F1"> 
					<td bgcolor="F1F1F1">'.htmlentities($filtre['lib_champ']).'</td>
					<td align="center">'.$tab_operateurs[$filtre['operateur']].'</td>
					<td align="center" valign="middle">'.$filtre['critere'].'</td>';
					if($proprietaire==1){
						$tableau_filtres	.='<td align="center" valign="middle"><a href="requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&edit='.$filtre['num_ligne'].'#filtre"><img src="./modifier.png" height="12" border="0"></a><td align="center" valign="middle"><a href="requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&del='.$filtre['num_ligne'].'#filtre"><img src="../images/icon_supp2.gif" width="11" height="12" border="0"></a></td></td>';
					}
					$tableau_filtres	.='</tr>';
				}
			}
		}else{
			$tableau_filtres = '<tr bgcolor="F1F1F1"><td colspan="'.$nb_col.'" align="center">Aucun filtre n\'est enregistré pour cette requête.</td></tr>';
		}
		
	###################################
	}
	
	
	###################################
	// génération de la liste des requêtes enregistrées
	
	$sql_sel_req = "SELECT DISTINCT * FROM USER_REQUETE WHERE ".$where_liste_req." ORDER BY LIB_REQUETE";
	$qry_sel_req = $db->query($sql_sel_req);
	
	if(is_array($qry_sel_req)){
		foreach($qry_sel_req as $sav_req){
			unset($selected_requete);
			if($sav_req['num_requete'] == $_GET['req_id']){
				$selected_requete = 'selected="selected"';
			}
			$options_sav_req .= '<option value="'.$sav_req['num_requete'].'"'.$selected_requete.'>'.htmlentities($sav_req['lib_requete']).'</option>';
		}
	}
	###################################
	?>
	
<html>
<head>
<title>Vakom - Requetes</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<link rel="stylesheet" href="../css/style.css" type="text/css">
<script language="JavaScript">
<!--
function change_type(typid){
	document.location.href='requetes.php?req_id=<?php echo intval($_GET['req_id'])?>&type='+typid;
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function MM_openBrWindow(theURL,winName) { //v2.0
  window.open(theURL,winName);
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
			<?php
			include("menu_top_new.php");
			?>
	  <br>
<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>	  		  
      <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  
    <td class="Titre_Requetes"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;REQU&Ecirc;TES</td>
		</tr>
	  </table>
	  
<form method="post" action="#" name="form1">
  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="fond_tablo_requetes">
    <tr>
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="center"> 
        <table border="0" cellspacing="0" cellpadding="0" align="center" class="TX">
          <tr> 
            <td align="left" class="TX"><input type="radio" name="choix_action" value="1" id="choix1" checked="checked"/>&nbsp;<?php if($options_sav_req!=''){ echo 'S&eacute;lectionner une requ&ecirc;te existante :&nbsp;&nbsp;'; } ?></td>
            <td align="left" class="TX"> 
				<?php
				if($options_sav_req!=''){
					echo '<select onfocus="document.getElementById(\'choix1\').checked=true;" name="select_req" class="form_ediht_Requetes">';
					echo $options_sav_req;
					echo '</select>';
				}
				?>			  
            </td>
          </tr>
          <tr> 
            <td align="left" class="TX">
			<input type="radio" name="choix_action" value="2" id="choix2" />&nbsp;Cr&eacute;er une nouvelle requ&ecirc;te :&nbsp;&nbsp;</td>
            <td align="left" class="TX"> 
              <input type="text" onfocus="document.getElementById('choix2').checked=true;" name="txt_nom_req" class="form_ediht_Requetes" size="50">
            </td>
          </tr>
          <tr> 
            <td align="left" class="TX">
			<input type="radio" name="choix_action" value="3" id="choix3" />&nbsp;Dupliquer une requ&ecirc;te :&nbsp;&nbsp;</td>
            <td align="left" class="TX"> 
				<?php
				if($options_sav_req!=''){
					echo '<select onfocus="document.getElementById(\'choix3\').checked=true;" name="select_req2" class="form_ediht_Requetes">';
					echo $options_sav_req;
					echo '</select>';
				}
				?>		
              <input type="text" onfocus="document.getElementById('choix3').checked=true;" name="txt_nom_req2" class="form_ediht_Requetes" size="50">
            </td>
          </tr>
        </table>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
    </tr>
  </table>
  <br>
  <table cellpadding="0" cellspacing="0" width="961" align="center">
    <tr>
      <td align="center">
		<input type="hidden" name="step" value="1">
        <input type="submit" name="submit" value="VALIDER" class="bn_valider_requete">
      </td>
    </tr>
  </table>
</form>
<br>
<?php
if($step>1){
?>
	<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td width="20">&nbsp;</td>				  
			<td class="Titre_Requetes"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $lib_requete ?></td>
		</tr>
	 </table>
	<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
	  <tr> 
		<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
	  </tr>
	  <tr> 
		<td width="14"></td>
		<td align="left" class="TX_Requetes">DONNEES A EXTRAIRE</td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="1"></td>
		<td align="center" class="TX" height="1" bgcolor="#666666"></td>
		<td width="14" height="1"></td>
	  </tr>
	  <tr> 
		<td width="14"></td>
		<td align="center" class="TX"> 
		  <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
			<tr> 
			  <td height="14"></td>
			</tr>
			<tr> 
			  <td align="left" class="TX">S&eacute;lectionner les donn&eacute;es &agrave; extraire : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type="radio" name="Bn1" value="1" onclick="change_type(1)" <?php echo $selected1 ?>>Partenaires&nbsp;&nbsp;
				<input type="radio" name="Bn2" value="2" onclick="change_type(2)" <?php echo $selected2 ?>>Produits&nbsp;&nbsp;
				<input type="radio" name="Bn3" value="3" onclick="change_type(3)" <?php echo $selected3 ?>>Certifi&eacute;s&nbsp;&nbsp;
				<input type="radio" name="Bn4" value="4" onclick="change_type(4)" <?php echo $selected4 ?>>Candidats&nbsp;
			  </td>
			</tr>
			<tr> 
			  <td align="left" class="TX">&nbsp; </td>
			</tr>
		  </table>
		</td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
	  </tr>
	</table>
	<p>&nbsp;</p>
	<form action="#" method="post" name="form2">
	<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
	  <tr> 
		<td width="20">&nbsp;</td>
		<td class="Titre_Requetes"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $lib_type ?></td>
	  </tr>
	</table>
	<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
	  <tr> 
		<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
	  </tr>
	  <tr> 
		<td width="14"></td>
		<td align="center" class="TX"> 
		  <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
			<tr> 
			  <td height="14"></td>
			</tr>
			<tr> 
			  <td align="left" class="TX">&nbsp; </td>
			</tr>
			<tr> 
			  <td align="left" class="TX"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr> 
						<td width="300" valign="top"> 
						  <table border="0" cellspacing="1" cellpadding="0" class="TX" bgcolor="#000000" width="290">
							<tr> 
							  <td class="TX_bold">Donn&eacute;es &agrave; extraire</td>
							  <td class="TX_bold" align="center">Select</td>
							</tr>
							<?php
							if($liste_champs != ''){
								echo $liste_champs;
							}
							?>
						  </table>
						</td>
						<td align="center" valign="top"> 
						  <p><br>
							<?php if($liste_champs != '' && $proprietaire==1) echo '<input type="submit" name="ajouter" value="Ajouter &gt;&gt;" class="bn_ajouter">'; ?>
						  </p>
						  <p> 
							<?php if($champs_req != '' && $proprietaire==1) echo '<input type="submit" name="supprimer" value="&lt; &lt; Supprimer" class="bn_ajouter">'; ?>
						  </p>
						</td>
						<td valign="top" align="right" width="323"> 
						  <table border="0" cellspacing="1" cellpadding="0" class="TX" bgcolor="#000000" width="318">
							<tr> 
							  <td class="TX_bold">Donn&eacute;es &agrave; extraire</td>
							  <td class="TX_bold" align="center">Ordre</td>
							  <td class="TX_bold" align="center">Triée</td>
							  <td class="TX_bold" align="center">Affichée</td>
							  <td class="TX_bold" align="center">Select</td>
							</tr>
							<?php
							if($champs_req!=''){
								echo $champs_req;
							}
							?>
						  </table>
						</td>
					  </tr>
					</table>
			  </td>
			</tr>
			<tr> 
			  <td align="left" class="TX">&nbsp;</td>
			</tr>
		  </table>
		</td>
		<td width="14"></td>
	  </tr>
	  			  <?php
			  unset($checked_1);
			  unset($checked_2);
			  if($infos_req[0]['indic_cert']==1){
				$checked_1 = ' checked="checked"';
			  }
			  if($infos_req[0]['indic_adm']==1){
				$checked_2 = ' checked="checked"';
			  }
			  ?>
			<tr>
			  <td width="14"></td>
			  <td class="TX">
			  <?php
			  if($proprietaire==1){
				  ?>
				  Accès&nbsp;aux&nbsp;certifiés&nbsp;:&nbsp;<input type="checkbox" value="1" name="che_droit_cert"<?php echo $checked_1?>/>&nbsp;
				  Accès&nbsp;aux&nbsp;administrateurs&nbsp;:&nbsp;<input type="checkbox" value="1" name="che_droit_adm"<?php echo $checked_2?>/>
				  <?php
			  }
			  ?>
			  <input type="hidden" name="proprio" value="<?php echo $proprietaire ?>" />
			  </td>
			  <td width="14"></td>
			</tr>
	  <tr> 
		<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
	  </tr>
	</table>
	<br>

	<table cellpadding="0" cellspacing="0" width="961" align="center">
	  <tr> 
		<td align="center"> 
		<?php
		if($proprietaire==1){
		?>
		  <input type="submit" name="sauver" value="VALIDER" class="bn_valider_requete">
		<?php
		}
		?>
		</td>
	  </tr>
	</table>
</form>
	<p>&nbsp;</p>
	<?php
}

if($step>1){
?>
	<a name="3"></a>
	<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
	  <tr> 
		<td width="20">&nbsp;</td>
		<td class="Titre_Requetes"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;FILTRE 
		  DES DONNEES</td>
	  </tr>
	</table>
	<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
	  <tr> 
		<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
	  </tr>
	  <tr> 
		<td width="14"></td>
		<td align="center" class="TX"> <a name="filtre"></a>
		  <table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
			<tr> 
			  <td height="14"></td>
			  <td height="14"></td>
			  <td height="14"></td>
			  <td height="14"></td>
			  <td height="14"></td>
			</tr>
			<?php
			if($proprietaire==1){
				?>
				<form action="#" method="post" name="form3">
					<tr> 
					  <td align="left" class="TX_bold">Crit&egrave;res</td>
					  <td align="left" class="TX_bold">&nbsp;&nbsp;Op&eacute;rateurs&nbsp;&nbsp;</td>
					  <td align="left" class="TX_bold">Valeurs</td>
					  <td align="left" class="TX">&nbsp;</td>
					  <td align="left" class="TX">&nbsp;</td>
					</tr>
					<tr> 
					  <td align="left" class="TX"> 
						<select name="critere" class="form_ediht_Requetes">
							<?php
							echo $options_req;
							?>
						</select>
					  </td>
					  <td align="center" class="TX"> 
						<select name="operateur" class="form_ediht_Requetes">
							<?php
							foreach($tab_operateurs as $key=>$value){
								unset($selected_a_valider);
								if($operateur_a_modifier == $key){
									$selected_a_valider = ' selected="selected"';
								}
								echo '<option value="'.$key.'"'.$selected_a_valider.'>'.$value.'</option>';
							}
							?>
						</select>
					  </td>
					  <td align="left" class="TX"> 
						<input type="text" name="valeur" class="form_ediht_Requetes" size="30" value="<?php echo htmlentities($valeur_a_modifier) ?>">
					  </td>
					  <td align="left" class="TX" valign="middle"> 
					  </td>
					  <td align="center" class="TX"> 
							<?php
							if($valeur_a_modifier != ''){
								echo '<input type="submit" name="edit_filtre" value="Edit" class="bn_ajouter">';
							}else{
								echo '<input type="submit" name="ajout_filtre" value="ok" class="bn_ajouter">';
							}
							?>
							
					  </td>
					</tr>
				</form>
				<tr> 
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="center" class="TX">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX" valign="middle">&nbsp;</td>
				  <td align="center" class="TX">&nbsp;</td>
				</tr>
				<?php
			}
			?>
			<tr> 
			  <td align="left" class="TX" colspan="5"> 
				<table border="0" cellspacing="1" cellpadding="0" class="TX" bgcolor="#000000" width="100%">
				  <tr> 
					<td class="TX_bold">Crit&egrave;res</td>
					<td class="TX_bold" align="center">Op&eacute;rateurs</td>
					<td class="TX_bold" align="center">Valeurs</td>
					<?php
					if($proprietaire==1){
						?>
						<td class="TX_bold" align="center">Edit</td>
						<td class="TX_bold" align="center">Suppr</td>
						<?php
					}
					?>
				  </tr>
				  <?php
				  echo $tableau_filtres;
				  ?>
				</table>
			  </td>
			</tr>
			<tr> 
			  <td height="14"></td>
			  <td height="14"></td>
			  <td height="14"></td>
			  <td height="14"></td>
			  <td height="14"></td>
			</tr>
		  </table>
		</td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
	  </tr>
	</table>
	<br>

	<table cellpadding="0" cellspacing="0" width="961" align="center">
	  <tr> 
		<td align="center"> 
		  <input type="button" name="ap" value="APERCU" class="bn_valider_requete" onClick="MM_openBrWindow('requetes_apercu.php?req_id=<?php echo $_GET['req_id'] ?>','')">
		  <input type="button" name="export" value="EXPORT" class="bn_valider_requete" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
		</td>
	  </tr>
	</table>
	<?php
}
?>
<p>&nbsp;</p>
<p>&nbsp;</p>
		</p></div>	</article></div>	</div>	</div>	</div>					
</body>
</html>
<?php
}else{
	include('no_acces.php');
}
?>