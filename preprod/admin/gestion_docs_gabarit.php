<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
		
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	$choix_zone = array(
	1 => 'Graphe',
	2 => 'Texte Fixe',
	3 => 'Texte Dynamique',
	4 => 'Image Fixe (170 * 170)',
	5 => 'Images Dynamiques',
	6 => 'Compte-rendu',
	7 => 'Réponse');
	
	$inc_zone = array(
	1 => 'doc_graphe.inc.php',
	2 => 'doc_texte_fixe.inc.php',
	3 => 'doc_texte_dynamique.inc.php',
	4 => 'doc_image_fixe.inc.php',
	5 => 'doc_images_dynamiques.inc.php',
	6 => 'doc_nom_cr.inc.php',
	7 => 'doc_reponse.inc.php');
	
	/* Sélection des graphes */
	$sql_graphes 	= "SELECT GRAPHE_ID, GRAPHE_NOM FROM GRAPHE ORDER BY GRAPHE_NOM ASC";
	$qry_graphes	= $db->query($sql_graphes);
	
	/* On sélectionne la liste des langues disponibles */
	$sql_langue_list = "SELECT * FROM LANGUE";
	$qry_langue_list = $db->query($sql_langue_list);	
	
	/* On génère la liste des types de cr disponibles */
	$sql_type_cr = "SELECT * FROM CODE WHERE CODE_TABLE='TYPE_CR' AND CODE_LANG_ID=1 AND CODE_ACTIF=1 ORDER BY CODE_ORDRE";
	$qry_type_cr = $db->query($sql_type_cr);

	/* Sélection des combinaisons images/textes */
	$sql_img_textes 	= "SELECT TXT_IMG_ID, TXT_IMG_NOM FROM TEXTE_IMAGE ORDER BY TXT_IMG_NOM ASC";
	$qry_img_textes		= $db->query($sql_img_textes);
	
	/* Sélection des questionnaires */
	$sql_img_quest 	= "SELECT * FROM QUESTIONNAIRE WHERE QUEST_DATE_SUPPRESSION IS NULL ORDER BY QUEST_NOM ASC";
	$qry_img_quest	= $db->query($sql_img_quest);

	if (trim($_POST['modif_nom_doc'])!=''){
		$sql_update_nom = "UPDATE DOCUMENT SET DOC_NOM='".txt_db($_POST['modif_nom_doc'])."' WHERE DOC_ID='".txt_db(intval($_GET['docid']))."'";
		$qry_update_nom = $db->query($sql_update_nom);
	}
	
	/* On vérifie si les zones ont été postées*/
	if (is_array($_POST['zones'])){
		/* On suprime la vieille valeur dans la base de données  */
		$sql_del_previous = "DELETE FROM DOC_A_INFO WHERE DOC_ID='".txt_db($_GET['docid'])."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
		$qry_del_previous = $db->query($sql_del_previous);
		foreach($_POST['zones'] as $zone){
			/* On check l'identité du champ posté pour la zone */
			if (isset($_POST[$zone.'_nom_cr'])){ // La zone est identifiée 
				if (trim($_POST[$zone.'_nom_cr'])!=''){
					/* On suprime la vieille valeur dans la base de données  */
					$sql_ins_zone_value = "INSERT INTO DOC_A_INFO (DOC_ID,NUM_ZONE,DOC_NOM_CR,DOC_LANG_ID,DOC_TYPE_CR) VALUES('".txt_db($_GET['docid'])."', 
					'".intval($zone)."',
					'".txt_db($_POST[$zone.'_nom_cr'])."',
					'".intval($_POST['langue'])."','".txt_db($_POST[$zone.'_type_cr'])."')";
					//echo $sql_ins_zone_value;
					$qry_ins_zone_value = $db->query($sql_ins_zone_value);
				}
			}elseif (isset($_POST[$zone.'_select_graph']) && isset($_POST[$zone.'_multi_U'])){ // La zone est identifiée 
				/* On suprime la vieille valeur dans la base de données  */
//				$sql_del_previous = "DELETE FROM DOC_A_INFO WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($zone)."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
//				$qry_del_previous = $db->query($sql_del_previous);
				
				if ($_POST[$zone.'_select_graph']>0){
					/* On suprime la vieille valeur dans la base de données  */
					$sql_ins_zone_value = "INSERT INTO DOC_A_INFO (DOC_ID,NUM_ZONE,DOC_GRAPHE_ID,DOC_GRAPHE_MULTI,DOC_LANG_ID) VALUES('".txt_db($_GET['docid'])."', 
					'".intval($zone)."',
					'".txt_db($_POST[$zone.'_select_graph'])."',
					'".txt_db($_POST[$zone.'_multi_U'])."',
					'".intval($_POST['langue'])."')";
					$qry_ins_zone_value = $db->query($sql_ins_zone_value);
				}
			}elseif (isset($_POST[$zone.'_txt_fixe'])){ // La zone est identifiée 
				if (trim($_POST[$zone.'_txt_fixe'])!=''){
					/* On suprime la vieille valeur dans la base de données  */
					$sql_ins_zone_value = "INSERT INTO DOC_A_INFO (DOC_ID,NUM_ZONE,DOC_TEXTE_FIXE,DOC_LANG_ID) VALUES('".txt_db($_GET['docid'])."', 
					'".intval($zone)."',
					'".substr(txt_db(stripslashes($_POST[$zone.'_txt_fixe'])),0,4000)."',
					'".intval($_POST['langue'])."')";
					$qry_ins_zone_value = $db->query($sql_ins_zone_value);
					$sql_ins_zone_value = "UPDATE DOC_A_INFO SET DOC_TEXTE_FIXE_2='".substr(txt_db(stripslashes($_POST[$zone.'_txt_fixe'])),4000,4000)."' WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($zone)."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
					$qry_ins_zone_value	= $db->query($sql_ins_zone_value);
					$sql_ins_zone_value = "UPDATE DOC_A_INFO SET DOC_TEXTE_FIXE_3='".substr(txt_db(stripslashes($_POST[$zone.'_txt_fixe'])),8000,4000)."' WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($zone)."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
					$qry_ins_zone_value	= $db->query($sql_ins_zone_value);
					$sql_ins_zone_value = "UPDATE DOC_A_INFO SET DOC_TEXTE_FIXE_4='".substr(txt_db(stripslashes($_POST[$zone.'_txt_fixe'])),12000,4000)."' WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($zone)."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
					$qry_ins_zone_value	= $db->query($sql_ins_zone_value);
					$sql_ins_zone_value = "UPDATE DOC_A_INFO SET DOC_TEXTE_FIXE_5='".substr(txt_db(stripslashes($_POST[$zone.'_txt_fixe'])),16000,4000)."' WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($zone)."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
					$qry_ins_zone_value	= $db->query($sql_ins_zone_value);
					$sql_ins_zone_value = "UPDATE DOC_A_INFO SET DOC_TEXTE_FIXE_6='".substr(txt_db(stripslashes($_POST[$zone.'_txt_fixe'])),20000,4000)."' WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($zone)."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
					$qry_ins_zone_value	= $db->query($sql_ins_zone_value);
					$sql_ins_zone_value = "UPDATE DOC_A_INFO SET DOC_TEXTE_FIXE_7='".substr(txt_db(stripslashes($_POST[$zone.'_txt_fixe'])),24000,4000)."' WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($zone)."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
					$qry_ins_zone_value	= $db->query($sql_ins_zone_value);
					$sql_ins_zone_value = "UPDATE DOC_A_INFO SET DOC_TEXTE_FIXE_8='".substr(txt_db(stripslashes($_POST[$zone.'_txt_fixe'])),28000,4000)."' WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($zone)."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
					$qry_ins_zone_value	= $db->query($sql_ins_zone_value);					
				}
			}elseif (isset($_POST[$zone.'_select_combi'])){ // La zone est identifiée 
				if ($_POST[$zone.'_select_combi']>0){
					/* On suprime la vieille valeur dans la base de données  */
					$sql_ins_zone_value = "INSERT INTO DOC_A_INFO (DOC_ID,NUM_ZONE,DOC_TXT_IMG_ID,DOC_LANG_ID) VALUES ('".txt_db($_GET['docid'])."', 
					'".intval($zone)."',
					'".txt_db($_POST[$zone.'_select_combi'])."',
					'".intval($_POST['langue'])."')";
					$qry_ins_zone_value = $db->query($sql_ins_zone_value);
				}
			}elseif (isset($_POST[$zone.'_select_reponse'])){ // La zone est identifiée 
				if ($_POST[$zone.'_select_reponse']>0){
					/* On suprime la vieille valeur dans la base de données  */
					$sql_ins_zone_value = "INSERT INTO DOC_A_INFO (DOC_ID,NUM_ZONE,DOC_REPONSE_ID,DOC_LANG_ID) VALUES ('".txt_db($_GET['docid'])."', 
					'".intval($zone)."',
					'".txt_db($_POST[$zone.'_select_reponse'])."',
					'".intval($_POST['langue'])."')";
					$qry_ins_zone_value = $db->query($sql_ins_zone_value);
				}
			}elseif (isset($_POST[$zone.'_multi']) && isset($_POST[$zone.'_select_img1']) && isset($_POST[$zone.'_select_img2'])){ // La zone est identifiée 
				if ($_POST[$zone.'_select_img1']>0 || $_POST[$zone.'_select_img2']>0){
					/* On suprime la vieille valeur dans la base de données  */
					$sql_ins_zone_value = "INSERT INTO DOC_A_INFO (DOC_ID,NUM_ZONE,DOC_IMG1_TXT_IMG_ID,DOC_IMG2_TXT_IMG_ID,DOC_IMG_MULTI,DOC_LANG_ID) VALUES ('".txt_db($_GET['docid'])."', 
					'".intval($zone)."',
					'".txt_db($_POST[$zone.'_select_img1'])."',
					'".txt_db($_POST[$zone.'_select_img2'])."',
					'".txt_db($_POST[$zone.'_multi'])."','".intval($_POST['langue'])."')";
					$qry_ins_zone_value = $db->query($sql_ins_zone_value);
				}
			}elseif (isset($_POST[$zone.'_image_fixe_chk'])){ // La zone est identifiée 
				if (is_uploaded_file($_FILES[$zone.'_image_fixe']['tmp_name'])){
					$error = '';
					
					$ftmp  = $_FILES[$zone.'_image_fixe']['tmp_name'];
					$fname = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES[$zone.'_image_fixe']['name']));	
					$fname = strtr($fname,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
					$fname = strtolower($fname);

					//extensions autorisées	
					$extension_autorises= array('.jpg','.jpeg', '.png', '.gif');	
					$extension= strrchr($fname,'.');	
					
					//lien des photos
					$lien  		= '../images/docs/'.$fname;


					/*Definition de la taille max des minatures. */

					$width_n  	= 600;
					$height_n 	= 600;

					//verification de l'extension
					if (!in_array($extension,$extension_autorises)){
						$error .= '<?php echo $t_fichier_not_image  ?>';
					}
				
					// L'image est déplacée dans le dossier normal
					if (!move_uploaded_file($ftmp,$lien)){
						$error .= '<?php echo $t_copier_fichier_probleme  ?>';
					}

					/* Content type */	
					//***************************	

						/*creation des miniatures. */
						list($width_orig, $height_orig) = getimagesize($lien);

						// On stoppe le script si la taille de la photo est trop grande
						if ($width_orig > 1500 || $height_orig > 1500){
							if (file_exists($lien)){
								unlink($lien);
							}
							exit('<?php echo $t_image1_grande  ?> <br /><a href="javascript:history.back();">retour</a><br />'.$rappel.' <hr />');
						} 
						$ratio_orig   = $width_orig/$height_orig;

						
						// Ratio normal
						if ($width_orig > $width_n || $height_orig > $height_n){
							if ($width_n/$height_n > $ratio_orig){
								$width_n  = $height_n*$ratio_orig;

							}else{
								$height_n = $width_n/$ratio_orig;

							}
						}else{
							$width_n  = $width_orig;
							$height_n = $height_orig;
						}



						// Config normale
						$image_p_n 	  = imagecreatetruecolor($width_n,$height_n);
						if ($extension=='.jpeg' || $extension=='.jpg'){
							$image_n   	  = @imagecreatefromjpeg($lien);
						}
						if ($extension=='.png'){
							$image_n   	  = @imagecreatefrompng($lien);	
						}
						if ($extension=='.gif'){
							$image_n   	  = @imagecreatefromgif($lien);	
						}
						@imagecopyresampled($image_p_n,$image_n,0,0,0,0,$width_n, $height_n, $width_orig, $height_orig);


						unset($width_n);
						unset($height_n);
						unset($ratio_orig);

						
						//movance  de la miniature crée au dessus dans le repertoire normal 
							if ($extension=='.jpeg' || $extension=='.jpg'){
								//movance  de la miniature crée au dessus dans le repertoire normal 
								imagejpeg($image_p_n,$lien,90);
							}
							if ($extension=='.png'){
								//movance  de la miniature crée au dessus dans le repertoire normal 
								imagepng($image_p_n,$lien,7);
							}
							if ($extension=='.gif'){
								//movance  de la miniature crée au dessus dans le repertoire normal 
								imagegif($image_p_n,$lien,90);
							}
						imagedestroy($image_p_n);
						
						unset($lien);

						
					//*************************************************************
				
					/* On sélectionne l'image présente dans la base de données */
					$sql_sel_img = "SELECT DOC_IMAGE_FIXE FROM DOC_A_INFO WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($zone)."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
					$qry_sel_img = $db->query($sql_sel_img);
					
					if (is_array($qry_sel_img)){ // Si il y a déjà une image pour ce document dans cette zone, on la supprime du serveur et de la base
						if (file_exists('../images/docs/'.$qry_sel_img[0]['doc_image_fixe']) && $qry_sel_img[0]['doc_image_fixe']!=''){
							//@unlink('../images/docs/'.$qry_sel_img[0]['doc_image_fixe']);
						}
						
						/* On met à jour la valeur dans la base de données  */
						$sql_del_previous = "UPDATE DOC_A_INFO SET DOC_IMAGE_FIXE='".$fname."' WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($zone)."' AND DOC_LANG_ID='".intval($_POST['langue'])."'";
						$qry_del_previous = $db->query($sql_del_previous);
						
					}else{ //Si aucune image n'est stockée dans la base
						/* On insert la nouvelle image */
						$sql_ins_img = "INSERT INTO DOC_A_INFO (DOC_ID,NUM_ZONE,DOC_IMAGE_FIXE,DOC_LANG_ID) VALUES('".txt_db($_GET['docid'])."', 
						'".intval($zone)."', 
						'".$fname."',
						'".intval($_POST['langue'])."')";
						//echo $sql_ins_img;
						$qry_ins_img = $db->query($sql_ins_img);
					}
				}
			}
		}
	}
	
	
	/* Selection des différents documents déjà existants */
	$sql_list_doc = "SELECT DOC_ID, DOC_NOM FROM DOCUMENT ORDER BY DOC_NOM ASC";
	$qry_list_doc = $db->query($sql_list_doc);
	
	/* Selection du document */
	$sql_doc = "SELECT * FROM DOCUMENT WHERE DOC_ID='".txt_db($_GET['docid'])."'";
	$qry_doc = $db->query($sql_doc);
	
	// SUBMIT
	if($insert){
			$insert = 'INSERT ALL '.$insert.'SELECT * FROM DUAL';
			$db->query($insert);
		}
	if(array_key_exists('Submit', $_POST) && $_POST['Submit']){
		$doc_id = $_GET['docid'];
		$sql_delete =  "DELETE FROM doc_a_info_reponse WHERE doc_id = '".$doc_id."'";
		$db->query($sql_delete);
		
		$suffix = '_select_reponse';
		$insert = '';
		$lang = $_POST['langue'];
		//print_r($_POST);
		foreach($_POST as $key => $data){
			if(preg_match('#'.$suffix.'#', $key)){
				$zone = explode($suffix, $key);
				$zone = $zone[0];
				$quest_id = $data.'_'.$zone;
				//echo $quest_id.'<br />';
				if(array_key_exists($quest_id, $_POST)){
					foreach($_POST[$quest_id] as $choice){
						$insert .= "INTO doc_a_info_reponse (doc_id, num_zone, doc_lang_id, choix_id) 
						VALUES('".$doc_id."', '".$zone."', '".$lang."', '".$choice."')";
					}
				}
			//	print_r($_POST[$data]);
			}
		}
		//echo $insert.'<br /><br />';
		if($insert){
			$insert = 'INSERT ALL '.$insert.'SELECT * FROM DUAL';
			$db->query($insert);
		}
		
		//echo $insert."\n";
	}
	
	
	//-------------
	
	
	
	if (is_array($qry_doc)){

		?>
		<!DOCTYPE html>
		<html>
		<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<link rel="stylesheet" href="../css/style.css" type="text/css">
			<script type="text/javascript" src="xhr_script.js"></script>
			<script src="../js/jquery-latest.min.js"></script>
			<strong><script src="https://cdn.tiny.cloud/1/n15lggt937iydpcbh026tvnj0a11vtiwcjd2tdoxiaf4y4da/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script></strong>
			<script language="JavaScript">
			<!--
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			//-->
			
			function che_all(id, obj){
			var checkbox;
				for(var i = 0; checkbox = id+'_'+i; i++){
					if(document.getElementById(checkbox)){
						if(obj.checked)
							document.getElementById(checkbox).checked = "checked";
						else
							document.getElementById(checkbox).checked = "";
					}else
						break;
				} 
			}
			function decoche(id){

				document.getElementById(id).checked = "";
			}

			tinymce.init({
			selector: 'textarea',
			height: 500,
			width:900,
			plugins: [
			  'advlist autolink link image lists charmap print preview hr anchor pagebreak',
			  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			  'table emoticons template paste help'
			],
			toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
			  'bullist numlist outdent indent | link image | print preview media fullpage | ' +
			  'forecolor backcolor emoticons | help',
			menu: {
			  favs: {title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons'}
			},
			menubar: 'favs file edit view insert format tools table help',
			content_css: 'css/content.css'
		  });
//			'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
			function uploadFile(inp, editor) {
				var input = inp.get(0);
				var data = new FormData();
				data.append('image[file]', input.files[0]);

				$.ajax({
					url: 'uploads/post.php',
					type: 'POST',
					data: data,
					processData: false, // Don't process the files
					contentType: false, // Set content type to false as jQuery will tell the server its a query string request
					success: function(data, textStatus, jqXHR) {
						data = jQuery.parseJSON(data);
						editor.insertContent('<img class="content-img" src="uploads/images/' + data.location + '"/>');
					},
					error: function(jqXHR, textStatus, errorThrown) {
						if(jqXHR.responseText) {
							errors = JSON.parse(jqXHR.responseText).errors
							alert('Error uploading image: ' + errors.join(", ") + '. Make sure the file is an image and has extension jpg/jpeg/png.');
						}
					}
				});
			}			
			function cacher()
			{
				document.getElementById('imagedeux').style.display = 'none';
			}
			
			function affiche()
			{
				document.getElementById('imagedeux').style.display='inline';
			}
			</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<?php
		include("menu_top_new.php");
		?>

		<div id="page" class="hfeed site">
			<div id="main" class="site-main">
				<div id="primary" class="content-area">
					<div id="content" class="site-content" role="main">
						<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">
							<div class="entry-contentAdmin">
								<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
		<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">

		  <tr> 
			<td>&nbsp;</td>
		  </tr>
		  <tr> 
			<td align="center" class="menu_Gris">&nbsp;</td>
		  </tr>
		  <tr> 
			<td align="right"> 		
			
		<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;DOCUMENTS</td>
		</tr>
		</table>
		<form method="post" name="form1" action="gestion_docs.php">
		 <table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="center" class="TX">
				<table border="0" cellspacing="0" cellpadding="0" align="center">
				  <tr> 
					<td align="left" class="menu_Gris"> 
					  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
					
						  <td height="14"></td>
						  <td height="14"></td>
						</tr>
						<?php
						if (is_array($qry_list_doc)){
							?>
							<tr> 
							  <td align="left" class="TX"> 
							  <?php
							  if (is_array($qry_doc)){
								$checked = ' checked="checked"';
							  }else{
								$checked = '';	
							  }
							  ?>
								<input type="radio" id="choix1" name="choix" checked="checked" value="selection"<?php echo $checked ?>>
								S&eacute;lectionner un document existant :&nbsp; </td>
							  <td align="left" class="TX"> 
								<select name="select_doc" id="sel_doc" class="form_ediht_Tarifs" onclick="document.getElementById('choix1').checked=true;">
								<?php
									foreach($qry_list_doc as $doc_list){
										if (is_array($qry_doc)){
											if ($doc_list['doc_id'] == $qry_doc[0]['doc_id']){
												$selected= ' selected="selected"';
											}else{
												$selected = '';
											}
										}
										echo '<option value="'.$doc_list['doc_id'].'"'.$selected.'>'.htmlentities($doc_list['doc_nom']).'</option>';
									}
								?>								
								</select>&nbsp;<input type="button" class="bn_ajouter" value="Supprimer" onclick="supprimer_doc(document.getElementById('sel_doc').options[document.getElementById('sel_doc').selectedIndex].value);">
							  </td>
							</tr>
							<?php
						}
						?>
						<tr> 
						  <td class="TX"> 
							<input onclick="montre('gabarits');" type="radio" id="choix2" name="choix" value="creation">
							Cr&eacute;er un nouveau document :</td>
						  <td class="TX"> 
							<input type="text" name="doc_nom" class="form_ediht_Tarifs" size="30" maxlength="120" onfocus="montre('gabarits'); document.getElementById('choix2').checked=true;">
						  </td>
						</tr>
				<?php
				  /* S'il y a au moins un document dans la base de données */
				  if (is_array($qry_list_doc)){
				  ?>
				  <tr> 
					<td align="left" class="TX"> 
					  <input type="radio" id="choix_action3" name="choix" value="duplication">
					  Dupliquer un document :&nbsp; &nbsp;</td>
					<td align="left" class="TX"> 
					  <select name="select_duplic" class="form_ediht_Tarifs" onclick="document.getElementById('choix_action3').checked=true;">
						<?php
						foreach($qry_list_doc as $doc_liste2){
							if (is_array($qry_doc)){
								if ($doc_liste2['doc_id'] == $qry_doc[0]['doc_id']){
									$selected= ' selected="selected"';
								}else{
									$selected = '';
								}
							}
							echo '<option value="'.$doc_liste2['doc_id'].'"'.$selected.'>'.htmlentities($doc_liste2['doc_nom']).'</option>';
						}
						?>
					  </select>
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nouveau libellé du document : 
					  <input type="text" name="nvo_nom_doc" class="form_ediht_Tarifs" size="30" maxlength="120" onclick="document.getElementById('choix_action3').checked=true;">
					</td>
				  </tr>
				  <?php
				  }
				  ?>
					  </table>
					</td>
				  </tr>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14" class="TX"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
			<br>
		  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr><td align="center">
				<input type="hidden" name="step" value="1">
				<input type="submit" value="Valider" class="BN">
			</td></tr>
		  </table>
		</form>
		  <br>
		
	  	<form action="gestion_docs_gabarit.php?docid=<?php echo $_GET['docid'] ?>&langid=<?php echo $_GET['langid'] ?>" method="post" name="form2" enctype="multipart/form-data" >
			<input type="hidden" value="<?php echo $_GET['docid'] ?>" name="docid"/>
			<input type="hidden" value="<?php echo $_GET['langid'] ?>" name="langid"/>
			<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo ucfirst($qry_doc[0]['doc_nom']) ?> ( GABARIT <?php echo $qry_doc[0]['type_gabarit'] ?> )</td>
			</tr>
			</table>
			
			  <table width="961" border="0" cellspacing="0" cellpadding="0">
				<tr> 
				  <td width="180" valign="top">&nbsp;</td>
				  <td align="left" valign="top">&nbsp;</td>
				</tr>
				<tr> 
				  <td width="180" valign="top" class="TX_Tarifs"> 
					<p>GABARIT CHOISI</p>
					<?php
					include ('gabarit_'.$qry_doc[0]['type_gabarit'].'_inc.php');
					?>
				  </td>
				  <td align="left" valign="top"> 
					<table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
					  <tr> 
						<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
						<td height="14"></td>
						<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
					  </tr>
					  <tr> 
						<td width="14"></td>
						<td align="center" class="TX"> 
						<table width="730" border="0" cellspacing="0" cellpadding="2" class="TX">
							<tr><td class="TX" width="1%">
							Nom&nbsp;du&nbsp;Document&nbsp;:&nbsp;</td>
							<td class="TX" align="left">
							<input type="text" size="50" maxlength="120" class="form_ediht_Tarifs" name="modif_nom_doc" value="<?php echo htmlentities(ucfirst($qry_doc[0]['doc_nom'])) ?>"</td>
							<td class="TX" height="40">Langue des zones du document :</td>
							<td class="TX"> 
							  <select name="langue" class="form_ediht_Tarifs" onchange="document.location.href='gestion_docs_gabarit.php?docid=<?php echo intval($_GET['docid']) ?>&langid='+this.options[this.selectedIndex].value+''">
								<?php
								  if (is_array($qry_langue_list)){
									foreach($qry_langue_list as $langue){
										unset($selected_langue);
										if($langue['lang_id']==$_GET['langid']){
											$selected_langue = ' selected="selected"';
										}
										echo '<option value="'.$langue['lang_id'].'"'.$selected_langue.'>'.$langue['lang_libelle'].'</option>';
									}
								  }
								?>
							  </select>
							</td>
							</tr>
						</table>
						<br>
						  <table width="730" border="0" cellspacing="0" cellpadding="2" class="TX">
							<tr align="center"> 
							  <td height="1" colspan="2" bgcolor="#000000" ></td>
							</tr>
							<tr> 
							  <td bgcolor="#C4C4C4" width="100" align="center">Position 
								dans <br>
								la page</td>
							  <td bgcolor="#C4C4C4" align="left"><b>Contenu</b></td>
							</tr>
							<tr> 
							  <td height="1" bgcolor="#000000"></td>
							  <td height="1" bgcolor="#000000"></td>
							</tr>
							<tr> 
							  <td align="center" valign="top">&nbsp;</td>
							  <td align="left" valign="top">&nbsp;</td>
							</tr>
							<?php
							for ($i=1; $i<5; $i++){
								if ($qry_doc[0]["type_zone".$i]!=''){
									$sql_zone = "SELECT * FROM DOC_A_INFO WHERE DOC_ID='".txt_db($_GET['docid'])."' AND NUM_ZONE='".intval($i)."' AND DOC_LANG_ID='".intval($_GET['langid'])."'";
									$qry_zone = $db->query($sql_zone);
								?>
								<input type="hidden" name="zones[]" value="<?php echo $i ?>">
								<tr> 
								  <td colspan="2" bgcolor="#CCCCCC" height="1" valign="top" align="center"></td>
								</tr>
								<tr> 
								  <td align="center" bgcolor="#EAEAEA">Zone <?php echo $i ?></td>
								  <td align="left" bgcolor="#EAEAEA"><b><?php echo $choix_zone[$qry_doc[0]["type_zone".$i]] ?></b></td>
								</tr>
								<?php
								$_GET['zoneid'] = $i;
								
									include($inc_zone[$qry_doc[0]["type_zone".$i]]);
								?>
								<tr> 
								  <td align="center" valign="top">&nbsp;</td>
								  <td align="left" valign="top">&nbsp; </td>
								</tr>
								
								<?php
								}
							}
							?>							
						  </table>
						</td>
						<td width="14"></td>
					  </tr>
					  <tr> 
						<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
						<td height="14"></td>
						<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
					  </tr>
					</table>
				  </td>
				</tr>
				<tr> 
				  <td width="180" valign="top" class="TX_rose">&nbsp;</td>
				  <td align="center" valign="top">&nbsp;</td>
				</tr>
				<tr> 
				  <td width="180" valign="top" class="TX_rose">&nbsp;</td>
				  <td align="center" valign="top"> 
					<input type="submit" name="Submit" value="VALIDER" class="BN">
					&nbsp;&nbsp; 
					<!--<input type="button" name="Submit2" value="APERCU" onClick="MM_goToURL('parent','gestion_docs.php');return document.MM_returnValue" class="BN">-->
				  </td>
				</tr>
				<tr>
				  <td width="180" valign="top" class="TX_rose">&nbsp;</td>
				  <td align="center" valign="top">&nbsp;</td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table>
		</form>
</p></div>	</article></div>	</div>	</div>	</div>
		</body>
		</html>
<?php
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{
	include('no_acces.php');
}
?>