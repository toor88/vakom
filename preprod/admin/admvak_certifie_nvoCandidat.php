<?php
session_start();
// Si l'utilisateur est un super admin ou un admin vakom
if ($_SESSION['droit']>1){

	if ($_GET['certid']>0){
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);
		
		if ($_POST['nom'] != ''){ // Si le formulaire est envoyé
			if ($_POST['libre']!='1'){
				$cli_id = $_POST['select_client'];
			}else{
				$cli_id = '-1';
			}
			if ($_POST['inactif']== 1){
				$actif = 0;
			}else{
				$actif = 1;
			}
			$dns = $_POST['JJ_DNS'].'/'.$_POST['MM_DNS'].'/'.$_POST['AAAA_DNS'];
			$sql_cand_id	= "SELECT SEQ_ID.NEXTVAL NB FROM DUAL";
			$qry_cand_id	= $db->query($sql_cand_id);
			
			$new_cand_id	= $qry_cand_id[0]['nb'];
			
		
		/* Si le pays selectionné n'est pas la france */
		if ($_POST['select_pays']!='5'){
			/* Le CP a pour valeur le champ cp_etr et la ville a pour valeur le champ ville_etr*/
			$cp 	= $_POST['cp_etr'];
			$ville 	= $_POST['ville_etr'];
		}else{
		/* Si le pays selectionné est la france, le CP porte la valeur du champ cp */
			$cp 	= $_POST['cp'];
			/* Si la ville n'existe pas dans la base, la ville porte la valeur du champ nom_new_ville */
			if ($_POST['new_ville']=='1'){
				$ville 	= $_POST['nom_new_ville'];
			}else{
				/* Si elle existe, la valeur de la ville est celle de la liste de sélection */
				$tab_v 	=  explode("_",$_POST['ville']);
				$ville 	= $tab_v[1];
			}
		}
			
			
			
			$sql_insert_candidat = "INSERT INTO CANDIDAT(CAND_ID,
			CAND_PWD,
			CAND_CLI_ID,
			CAND_NOM,
			CAND_PRENOM,
			CAND_EMAIL,
			CAND_SEXE,
			CAND_DNS,
			CAND_FONCTION,
			CAND_LANG_ID,
			CAND_AD1,
			CAND_AD2,
			CAND_CP,
			CAND_VILLE,
			CAND_PAYS_CODE_ID,
			CAND_TEL,
			CAND_PORTABLE,
			CAND_FAX,
			CAND_INFO1,
			CAND_INFO2,
			CAND_ACTIF,
			CAND_USER_CREATION_ID,
			CAND_DATE_CREATION,
			CAND_CERT_ID,
			CATEGORIE_SP_CODE_ID,
			ACTIVITE_CODE_ID) VALUES (".$new_cand_id.",'N".$new_cand_id."W',
			'".txt_db($cli_id)."',
			upper('".txt_db($_POST['nom'])."'),
			'".txt_db($_POST['prenom'])."',
			'".txt_db($_POST['mail'])."',
			'".txt_db($_POST['sexe'])."',";
			
			if (strlen($_POST['JJ_DNS'])==2 && strlen($_POST['MM_DNS'])==2 && strlen($_POST['AAAA_DNS'])==4){
				$sql_insert_candidat .= "TO_DATE('".txt_db($dns)."','DD/MM/YYYY'),";
			}else{
				$sql_insert_candidat .= "'',";
			}
			
			$sql_insert_candidat .= "'".txt_db($_POST['fct'])."',
			'".txt_db($_POST['langue'])."',
			'".txt_db($_POST['adr1'])."',
			'".txt_db($_POST['adr2'])."',
			'".txt_db($cp)."',
			'".txt_db($ville)."',
			'".txt_db($_POST['select_pays'])."',
			'".txt_db($_POST['tel'])."',
			'".txt_db($_POST['tel_p'])."',
			'".txt_db($_POST['fax'])."',
			'".txt_db($_POST['comm1'])."',
			'".txt_db($_POST['comm2'])."',
			'".txt_db(intval($actif))."',
			'".$_SESSION['vak_id']."',
			SYSDATE,
			'".txt_db(intval($_GET['certid']))."',
			".intval($_POST['categorie_SP']).",
			".intval($_POST['activite']).")";
			//echo $sql_insert_candidat;
			$qry_insert_candidat = $db->query($sql_insert_candidat);
			
			if($_GET['origine']=='gen_prod'){
				if (is_array($_SESSION['checked_cand'])){
					if (!in_array($new_cand_id, $_SESSION['checked_cand'])) {
						array_push($_SESSION['checked_cand'], $new_cand_id);
					}
				}else{
					$_SESSION['checked_cand'] = array(0=>$new_cand_id);
				}
			}
			?>
			<script type="text/javascript">
				window.close();
				<?php
				if($_GET['origine']=='gen_prod'){
				 echo 'window.opener.location.reload(true);';
				}
				?>
			</script>
			<?php
		}
		
		/* On génére la liste des catégories socio-professionnelles */
		$sql_categorie_SP_list = "SELECT * FROM CODE WHERE CODE_TABLE='CATEGORIE_SP' AND CODE_ACTIF=1 AND CODE_LANG_ID=" .$_SESSION['lang'] ." ORDER BY CODE_ORDRE";
		$qry_categorie_SP_list = $db->query($sql_categorie_SP_list);
		//echo $sql_categorie_SP_list;
		/* On génére la liste des activités */
		$sql_activite_list = "SELECT * FROM CODE WHERE CODE_TABLE='ACTIVITE' AND CODE_ACTIF=1 AND CODE_LANG_ID=" .$_SESSION['lang'] ." ORDER BY CODE_ORDRE";
		$qry_activite_list = $db->query($sql_activite_list);
		
		/* On sélectionne la liste des pays */
		$sql_pays_list = "SELECT * FROM CODE WHERE CODE_TABLE='PAYS' ORDER BY CODE_ORDRE";
		$qry_pays_list = $db->query($sql_pays_list);
		
		/* On sélectionne la liste des langues disponibles */
		$sql_langue_list = "SELECT * FROM LANGUE ORDER BY LANG_ID";
		$qry_langue_list = $db->query($sql_langue_list);
		
		/* On sélectionne les information du certifié sélectionné */
		$sql_info_cert = "SELECT * FROM CERTIFIE WHERE CERT_ID = '".txt_db(intval($_GET['certid']))."'";
		$qry_info_cert = $db->query($sql_info_cert);		
		
		/* On sélectionne la liste des clients du certifié */
		$sql_liste_clients = "SELECT CLIENT.CLI_ID, CLIENT.CLI_NOM, CLIENT.CLI_VILLE,CLIENT.CLI_CP FROM CLIENT, CLIENT_A_CERT 
		WHERE CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND (CLIENT.CLI_ACTIF IS NULL OR CLIENT.CLI_ACTIF=1) AND CLIENT_A_CERT.CERT_ID='".txt_db(intval($_GET['certid']))."' ORDER BY CLIENT.CLI_NOM ASC";
		$qry_liste_clients = $db->query($sql_liste_clients);		
		
		?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<script language="JavaScript">
		<!--
		
		function MM_openBrWindow(theURL,winName,features) { //v2.0
		  window.open(theURL,winName,features);
		}
		
		function hid_ville(id_pays){
				if (id_pays != "5"){
					document.getElementById('cp1').style.display='none';
					document.getElementById('cp2').style.display='inline';
					document.getElementById('ville2').style.display='none';
					document.getElementById('ville3').style.display='inline';
				}else{
					document.getElementById('cp1').style.display='inline';
					document.getElementById('cp2').style.display='none';
					document.getElementById('ville2').style.display='inline';
					document.getElementById('ville3').style.display='none';
				}
			}
		
		function check_libre(){
			if (document.form.libre.checked==true){
				document.getElementById('pas_libre').style.display='none';
			}
			if (document.form.libre.checked==false){
				document.getElementById('pas_libre').style.display='block';
			}
		}
		
		function TestVille(){
				/* var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_ville.php"; // La page qui réceptionne les données
				var cp = document.getElementById('cp').value; 
				var data     = null; 
				
				if 	(cp.length>1){ //Si le code postal tapé possède au moins 2 caractères
					document.getElementById("ville2").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" />';
					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("<?php echo $t_browser_support_error_1 ?>");
						   return; 
						} 

						if(cp != ""){
							data = "cp="+cp+"&class=cand";
						}
						if(method == "GET" && data != null) {
						   filename += "?"+data;
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split(":"); 
							  if(typeof(tmp[0]) != "undefined") { 
								 document.getElementById("ville2").innerHTML = '';
								 if (tmp[0]!=''){
									document.getElementById("ville2").innerHTML = tmp[0];
								 }else{
									document.getElementById("ville2").innerHTML = '<input type="hidden" name="new_ville" value="1"><input type="text" size="46" name="nom_new_ville" maxlength="255" class="form_ediht_Candidats" />';
								 }
							  }
						   } 
						} 

						xhr_object.send(data); //On envoie les données
				} */
			}
			
			function charge_code(code){
				var tmp_code = code.split("_");
				document.getElementById('cp').value = tmp_code[0];
			}
		
		function verif(){
		
			document.form.valider.value='Patientez...';
			
			var mail 		= document.form.mail.value;
			var method   	= "GET"; //On définit la methode (ici je passe le code postal par l'url)
			var filename 	= "ajax_verif_exist.php"; // La page qui réceptionne les données
			var data     	= null; 
			
				var xhr_object = null; 
					 
				if(window.XMLHttpRequest) // Firefox 
				   xhr_object = new XMLHttpRequest(); 
				else if(window.ActiveXObject) // Internet Explorer 
				   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
				else { // XMLHttpRequest non supporté par le navigateur 
				   alert("<?php echo $t_browser_support_error_1 ?>");
				   return; 
				} 
				 
				
				 
				
				data = "mail="+mail;	// seul le mail est vérifié
				
				if(method == "GET" && data != null) {
				   filename += "?"+data;
				   data      = null;
				}
				 
				xhr_object.open(method, filename, true);

				xhr_object.onreadystatechange = function() {
				   if(xhr_object.readyState == 4) {
					  var tmp = xhr_object.responseText.split(":"); 
					  /* if(typeof(tmp[0]) != "undefined") {
						 if (tmp[0]!=''){
							var exist = true;
						 }
 						 else
							var exist = false;
					  } */

					  if(typeof(tmp[0]) != "undefined") {
						 if (tmp[0]==1){
							var exist = true;
						 }
 						 else
							var exist = false;
						if (tmp[0]==2){
							var mailexist = true;
						 }
 						 else
							var mailexist = false;
					  }
					  
					var error = '';
					var warning = '';
					
					if (document.form.nom.value == ''){
						error += "<?php echo $t_nom_oblig ?>\n";
					}
					if (document.form.prenom.value == ''){
						error += "<?php echo $t_prenom_oblig ?>\n";
					}
					if (document.form.mail.value == ''){
						warning += "<?php echo $t_aucune_adr_email  ?>";
					}
					if (mailexist == true){
						warning += "<?php echo $t_email_exist_deja ?>";
					}
					
					if (document.form.JJ_DNS.value.length>0 || document.form.MM_DNS.value.length>0 || document.form.AAAA_DNS.value.length>0){
						// verif du format date du début
						error1=false;
						if (document.form.JJ_DNS.value<1 || document.form.JJ_DNS.value>31 || document.form.JJ_DNS.value.length<2){
							error1 = true;
						}
						if (document.form.MM_DNS.value<1 || document.form.MM_DNS.value>12 || document.form.MM_DNS.value.length<2){
							error1 = true;
						}
						if (document.form.AAAA_DNS.value<1909 || document.form.AAAA_DNS.value.length<4){
							error1 = true;
						}
						if (error1==true){
							error +="<?php echo $t_mauvaise_date_naiss ?>\n";
						}
					}
					
					if (document.getElementById('sexe1').checked == false && document.getElementById('sexe2').checked == false){
						error += '<?php echo $t_sex_obligatoire  ?>';
					}
					
					/* Si il y a une erreur, on l'affiche */
					if (error!=''){
						document.form.valider.value='VALIDER';
						alert(error);
					}else{ // Sinon on soumet le formulaire
						if(warning == '')
							document.form.submit();
						else if(warning != ''){
							if(confirm(warning)){
								if (exist==true){
									if (confirm("<?php echo $t_utilisateur_existe  ?>")){
										document.form.submit();
									}
									else{
										document.form.valider.value='VALIDER';
									}
								}
								else
									document.form.submit();
							}
							else
								return;
						}
					}
				   }
				} 

				xhr_object.send(data); //On envoie les données
			
		}
		
		function pass(champ){
			if (champ==1){
				if (document.getElementById('JJ_DNS').value.length==2){
					document.getElementById('MM_DNS').value='';
					document.getElementById('MM_DNS').focus();
				}
			}
			if (champ==2){
				if (document.getElementById('MM_DNS').value.length==2){
					document.getElementById('AAAA_DNS').value='';
					document.getElementById('AAAA_DNS').focus();
				}
			}
			if (champ==3){
				if (document.getElementById('AAAA_DNS').value.length==4){
					document.getElementById('fct').focus();
				}
			}
		}
		
		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		//-->
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#" name="form">
			<table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $t_fiche_new_cand ?></td>
			</tr>
			<tr>
			  <td></td>
			  <td><table border="0" cellspacing="0" cellpadding="0"  width="100%" align="center">
			    <tbody>
			      <tr>
			        <td>&nbsp;</td>
			        <td>&nbsp;</td>
		          </tr>
			      <tr>
				<td class="fond_tablo_candidats" height="40" width="443" align="left"><?php echo $t_fiche_cand ?></td>
				<td class="fond_tablo_candidats2" valign="middle" align="left"><?php echo $t_date_creation ?> 
				  : le <?php echo date('d/m/Y H:i') ?>
				  </td>
		          </tr>
			      <tr>
			  <td>&nbsp;</td>
			  <td class="champsoblig" valign="middle" align="right"><?php echo $t_champs_oblig ?> 
				* </td>
		          </tr>
		        </tbody>
		      </table></td>
			  </tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><table border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td class="TX"><?php echo $t_cand_libre ?></td>
					<td class="TX">
					  <input type="checkbox" name="libre" value="1" onclick="check_libre()">
					</td>
					<td class="TX" height="40"><?php echo $t_fiche_cand_inactif ?> :</td>
					<td class="TX"> 
					  <input type="checkbox" name="inactif" value="1">
					</td>
				  </tr>
				<tr id="pas_libre">
					<td class="TX"><?php echo $t_societe ?>&nbsp;:</td>
					<td class="TX" style="text-align: left; " colspan="3">
						<select name="select_client" class="form_ediht_Candidats"  >
								<?php
								if (is_array($qry_liste_clients)){
									foreach($qry_liste_clients as $client_liste){
										echo '<option value="'.$client_liste['cli_id'].'">'.strtoupper($client_liste['cli_nom']).' - '.ucfirst($client_liste['cli_cp']).' '.ucfirst($client_liste['cli_ville']).'</option>';
									}
								}else{
									echo '<option value="-1">'.$t_aucun_clt_cert.'</option>';
								}
								?>
							  </select>&nbsp;<input class="bn_ajouter" type="button" value="<?php echo $t_btn_creer_client ?>" onClick="MM_openBrWindow('admvak_crea_certifieClient.php?certid=<?php echo $_GET['certid'] ?>','crea_client','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">
						  </td>
							  </tr>				  
				  <tr> 
					<td class="TX" ><?php echo $t_nom ?>* :</td>
					<td class="TX"> 
					  <input type="text" name="nom" size="40" class="form_ediht_Candidats" >
					  &nbsp;&nbsp;&nbsp;&nbsp; </td>
					<td class="TX" ><?php echo $t_prenom ?>* :</td>
					<td class="TX"> 
					  <input type="text" name="prenom" size="40" class="form_ediht_Candidats" >
					</td>
				  </tr>
				  <tr> 
					<td class="TX" ><?php echo $t_email ?>* :</td>
					<td class="TX" colspan="3" > 
					  <input type="text" name="mail" size="40" class="form_ediht_Candidats">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" ><?php echo $t_sexe ?>* :</td>
					<td class="TX" > 
					  <input type="radio" id="sexe1" name="sexe" value="H">
					  <?php echo $t_M ?>&nbsp;&nbsp; 
					  <input type="radio" id="sexe2" name="sexe" value="F">
					  <?php echo $t_F ?> </td>
					<td class="TX" ><?php echo $t_date_naissance ?> : </td>
					<td class="TX"> 
					  <input type="text" name="JJ_DNS" id="JJ_DNS" onkeyup="pass(1)" size="2" class="form_ediht_Candidats" maxlength="2">
					  / 
					  <input type="text" name="MM_DNS" id="MM_DNS" onkeyup="pass(2)" size="2" class="form_ediht_Candidats" maxlength="2">
					  / 
					  <input type="text" name="AAAA_DNS" id="AAAA_DNS" onkeyup="pass(3)" size="4" class="form_ediht_Candidats" maxlength="4">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" ><?php echo $t_categorie_SP ?> :</td>
					<td class="TX"> 
					 <select name="categorie_SP" class="form_ediht_Candidats" style="margin-right: 5px; width:270px">
					 <option value="-1"></option>
						<?php
						  if (is_array($qry_categorie_SP_list)){
							foreach($qry_categorie_SP_list as $categorie_SP){
								echo '<option value="'.$categorie_SP['code_id'].'">'.$categorie_SP['code_libelle'].'</option>';
							}
						  }
						?>
					  </select>
					</td>
					<td class="TX"><?php echo $t_activite ?> :</td>
					<td class="TX"> 
					  <select name="activite" class="form_ediht_Candidats" style="width:270px">
					  <option value="-1"></option>
						<?php
						  if (is_array($qry_activite_list)){
							foreach($qry_activite_list as $activite){
								echo '<option value="'.$activite['code_id'].'">'.$activite['code_libelle'].'</option>';
							}
						  }
						?>
					  </select>
					</td>
				  </tr>
				  
				  <tr> 
					<td class="TX" ><?php echo $t_fonction ?> :</td>
					<td class="TX"> 
					  <input type="text" name="fct" id="fct" size="40" class="form_ediht_Candidats" >
					</td>
					<td class="TX"><?php echo $t_langue ?>* :</td>
					<td class="TX"> 
					  <select name="langue" class="form_ediht_Candidats" style="width:270px">
						<?php
						  if (is_array($qry_langue_list)){
							foreach($qry_langue_list as $langue){
								echo '<option value="'.$langue['lang_id'].'">'.$langue['lang_libelle'].'</option>';
							}
						  }
						?>
					  </select>
					</td>
				  </tr>
				  
				  <tr> 
					<td class="TX"><?php echo $t_adresse ?>&nbsp;:&nbsp;</td>
					<td class="TX"> 
					  <input type="text" name="adr1" size="40" class="form_ediht_Candidats" maxlength="50"><br></td>
					<td class="TX" width="150"><?php echo $t_cp ?> :
				
					
					</td>
					<td class="TX" valign="top"> 
						<div id="cp1" width="1%" style="display: inline;">
				  <input type="text" name="cp" id="cp" size="5" maxlength="5" class="form_ediht_Candidats"> <!--onblur="charge_code(document.getElementById('ville').options[document.getElementById('ville').selectedIndex].value)"-->
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  </div>
				  <div id="cp2" width="1%" style="display: none;">
				  <input type="text" name="cp_etr" id="cp_etr" size="5" maxlength="5" class="form_ediht_Candidats">
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  </div>
					  <!--<input type="text" name="cp" size="5" class="form_ediht_Candidats" maxlength="5" onkeyUp="TestVille()" onblur="charge_code(document.getElementById('ville').options[document.getElementById('ville').selectedIndex].value)">-->
					  
					  
					</td>
				  </tr>
				  <tr>
				    <td class="TX">&nbsp;</td>
				    <td class="TX"><input type="text" name="adr2" size="40" class="form_ediht_Candidats" maxlength="50"></td>
				    <td class="TX"><?php echo $t_ville ?> :</td>
				    <td class="TX"><div id="ville2" width="1%" style="display: inline;"><input type="hidden" id="ville" name="ville" value=""></div>
					  <input type="hidden" name="new_ville" value="1"><input type="text" size="40" name="nom_new_ville" maxlength="255" class="form_ediht_Candidats" />
						<div id="ville3" style="display: none;">
						<input type="text" name="ville_etr" maxlength="255" class="form_ediht_Candidats">
				</div>
					  
					  
				    <!--<input type="text" name="ville" size="40" class="form_ediht_Candidats" maxlength="50">--></td>
			    </tr>
				  <tr> 
					<td class="TX" ><?php echo $t_pays ?>&nbsp;:&nbsp;</td>
					<td class="TX"> 
					  <select name="select_pays" class="form_ediht_Candidats">
					<?php
					  if (is_array($qry_pays_list)){
						foreach($qry_pays_list as $pays){
							echo '<option value="'.$pays['code_id'].'">'.$pays['code_libelle'].'</option>';
						}
					  }
					?>
					  </select>
					</td>
					<td class="TX">&nbsp;</td>
					<td class="TX">&nbsp;</td>
				  </tr>
				  <tr> 
					<td class="TX" ><?php echo $t_tel_fix ?>&nbsp;:&nbsp;</td>
					<td class="TX"> 
					  <input type="text" name="tel" size="40" class="form_ediht_Candidats" maxlength="25">
					</td>
					<td class="TX"><?php echo $t_tel_port ?>&nbsp;:&nbsp;</td>
					<td class="TX"> 
					  <input type="text" name="tel_p" size="40" class="form_ediht_Candidats" maxlength="25">
					</td>
				  </tr>
				  <tr> 
					<td class="TX"><?php echo $t_fax ?>&nbsp;:&nbsp;</td>
					<td class="TX"> 
					  <input type="text" name="fax" size="40" class="form_ediht_Candidats" maxlength="25">
					</td>
					<td class="TX">&nbsp;</td>
					<td class="TX">&nbsp; </td>
				  </tr>
				  <tr> 
					<td class="TX"><?php echo $t_comment ?>&nbsp;:&nbsp;</td>
					<td class="TX" colspan="3"> 
					  <input type="text" name="comm1" style="width:100%" class="form_ediht_Candidats" maxlength="70">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" valign="top">&nbsp;</td>
					<td class="TX" colspan="3"> 
					  <input type="text" name="comm2" style="width:100%" class="form_ediht_Candidats" maxlength="70">
					</td>
				  </tr>
				<tr> 
					<td class="TX" valign="top" colspan="4">
						<p style="text-align:center">
							<input type="button" name="valider" value="<?php echo $t_btn_valider ?>" class="bn_valider_candidat" onClick="verif()">
		 			    </p>
					</td>
				</tr>				  
				</table></td>
			  </tr>
           </table>
		  
		  <br>

		  <p>&nbsp;</p>
		</form>
		</body>
		</html>
	<?php
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{
	include('no_acces.php');
}
?>
