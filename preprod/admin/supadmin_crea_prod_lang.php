<?php
session_start();
	// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	if ($_GET['prodid']!=''){
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);
		
		if ($_POST['langue']!=''){
			// On vérifie que la langue n'est pas déjà en place
			$sql_verif_langue = "SELECT * FROM PROD_A_LANG WHERE PROD_ID='".txt_db(intval($_GET['prodid']))."' AND LANG_ID='".txt_db(intval($_POST['langue']))."'";
			$qry_verif_langue	= $db->query($sql_verif_langue);
			
			if (!is_array($qry_verif_langue)){
				// Si la langue n'existe pas, on insert la nouvelle langue
				$sql_insert_langue = "INSERT INTO PROD_A_LANG (PROD_ID,LANG_ID,TITRE,COMMENTAIRE_IN,COMMENTAIRE_OUT,EMAIL_PUBLIC) VALUES('".txt_db(intval($_GET['prodid']))."', '".txt_db(intval($_POST['langue']))."','".txt_db($_POST['titre'])."','".substr(txt_db(stripslashes($_POST['txt1'])),0,4000)."','".substr(txt_db(stripslashes($_POST['txt2'])),0,4000)."','".txt_db(stripslashes($_POST['txt3']))."')";
				$qry_insert_langue	= $db->query($sql_insert_langue);
				$sql_update_langue = "UPDATE PROD_A_LANG SET COMMENTAIRE_IN_2='".substr(txt_db(stripslashes($_POST['txt1'])),4000,4000)."',COMMENTAIRE_OUT_2='".substr(txt_db(stripslashes($_POST['txt2'])),4000,4000)."' WHERE LANG_ID='".txt_db(intval($_POST['langue']))."' AND PROD_ID='".txt_db($_GET['prodid'])."'";
				$qry_update_langue	= $db->query($sql_update_langue);
				$valid = true;
				?>
				<script type="text/javascript">
					window.opener.location.reload(true);
					window.close();
				</script>
				<?php
			}else{
				$error = true;
			}

		}
		
		// Sélection des informations de base du questionnaire
		$sql_info_prod	= "SELECT * FROM PRODUIT WHERE PROD_ID='".txt_db($_GET['prodid'])."'";
		$qry_info_prod	= $db->query($sql_info_prod);
		if (is_array($qry_info_prod)){
			$sql_info_lang	= "SELECT DISTINCT * FROM LANGUE ORDER BY LANG_LIBELLE";
			$qry_info_lang	= $db->query($sql_info_lang);
			if (is_array($qry_info_lang)){
					
				?>
				<!DOCTYPE html>
				<html>
				<head>
				<title>Vakom</title>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<link rel="stylesheet" href="../css/nvo.css" type="text/css">
				<link rel="stylesheet" href="../css/general.css" type="text/css">
				<script language="JavaScript">
				<!--
				function MM_goToURL() { //v3.0
				  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
				  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
				}
				//-->
				</script>
					<script src="../js/jquery-latest.min.js"></script>
				<strong><script src="https://cdn.tiny.cloud/1/n15lggt937iydpcbh026tvnj0a11vtiwcjd2tdoxiaf4y4da/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script></strong>
				<script type="text/javascript">
					tinymce.init({
			selector: 'textarea',
			height: 300,
			width:600,
			plugins: [
			  'advlist autolink link image lists charmap print preview hr anchor pagebreak',
			  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			  'table emoticons template paste help'
			],
			toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
			  'bullist numlist outdent indent | link image | print preview media fullpage | ' +
			  'forecolor backcolor emoticons | help',
			menu: {
			  favs: {title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons'}
			},
			menubar: 'favs file edit view insert format tools table help',
			content_css: 'css/content.css'
		  });
		  function uploadFile(inp, editor) {
						var input = inp.get(0);
						var data = new FormData();
						data.append('image[file]', input.files[0]);

						$.ajax({
							url: 'uploads/post.php',
							type: 'POST',
							data: data,
							processData: false, // Don't process the files
							contentType: false, // Set content type to false as jQuery will tell the server its a query string request
							success: function(data, textStatus, jqXHR) {
								data = jQuery.parseJSON(data);
								editor.insertContent('<img class="content-img" src="uploads/images/' + data.location + '"/>');
							},
							error: function(jqXHR, textStatus, errorThrown) {
								if(jqXHR.responseText) {
									errors = JSON.parse(jqXHR.responseText).errors
									alert('Error uploading image: ' + errors.join(", ") + '. Make sure the file is an image and has extension jpg/jpeg/png.');
								}
							}
						});
					}					
				</script>
				</head>

				<body bgcolor="#FFFFFF" text="#000000">
				<form method="post" action="#">
				<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;PRODUIT <?php echo strtoupper($qry_info_prod[0]['prod_nom']) ?></td>
				</tr>
				</table>
				  <table border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td align="center" class="TX">
					  <?php
					  if ($error){
						?>
						<script style="text/javascript">alert('<?php echo $t_lang_exist ?>');</script>
						<?php
						}elseif($valid){
						echo '<b><?php echo $t_param_enreg ?></b>';
					  }
					  ?>
					  </td>
					</tr>
					<tr> 
					  <td align="left" class="TX"> 
						<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="800">
						  <tr> 
							<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
							<td height="14"></td>
							<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
						  </tr>
						<tr> 
						  <td width="14"></td>
						  <td class="TX_Tarifs">Ajout&nbsp;d'une&nbsp;version&nbsp;linguistique</td>
						  <td width="14"></td>
						</tr>
						<tr> 
						  <td width="14"></td>
						  <td bgcolor="#666666" height="1"></td>
						  <td width="14"></td>
						</tr>
						<tr>
						  <td width="14"></td>
						  <td class="TX" align="center">&nbsp;</td>
						  <td width="14"></td>
						</tr>
						  <tr> 
							<td width="14"></td>
							<td align="center" valign="top" class="TX"> 
							  <table border="0" cellspacing="0" cellpadding="0">
								<tr> 
								  <td class="TX" height="40">Langue :&nbsp;</td>
								  <td> 
									<select name="langue" class="form_ediht_Tarifs">
										<?php
										foreach($qry_info_lang as $lang){
											if (!$error){
												if ($lang['lang_id']==1){
													$selected = ' selected="selected"';
												}else{
													$selected = '';
												}
											}else{
												if ($lang['lang_id']==$_POST['langue']){
													$selected = ' selected="selected"';
												}else{
													$selected = '';
												}
											}
											echo '<option value="'.$lang['lang_id'].'"'.$selected.'>'.$lang['lang_libelle'].'</option>';
										}
										?>
									</select>
								  </td>
								</tr>
								<?php
								if ($error){
									$titre		= stripslashes($_POST['titre']);
									$content1	= stripslashes($_POST['txt1']);
									$content2	= stripslashes($_POST['txt2']);
									$content3	= stripslashes($_POST['txt3']);
								}
								?>
								<tr> 
								  <td class="TX" height="40">Titre :</td>
								  <td> 
									<input type="text" name="titre" size="70" class="form_ediht_Tarifs" value="<?php echo htmlentities($titre) ?>">
								  </td>
								</tr>
								<tr> 
								  <td class="TX" valign="top">Commentaire&nbsp;d'entrée&nbsp;:</td><td class="TX"> </td>
								</tr>
								<tr>
								  <td class="TX" colspan="2"><textarea id="txt1" name="txt1" rows="15" cols="80" style="width: 80%"><?php echo htmlentities($content1) ?></textarea>
								  </td>
								</tr>
								<tr> 
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr> 
								  <td class="TX" valign="top">Commentaire&nbsp;de&nbsp;sortie&nbsp;:</td><td class="TX"> </td>
								</tr>
								<tr>
								  <td class="TX" colspan="2"><textarea id="txt2" name="txt2" rows="15" cols="80" style="width: 80%"><?php echo htmlentities($content2) ?></textarea>
								  </td>
								</tr>
								<tr> 
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr> 
								  <td class="TX" valign="top">Corps de l'email public&nbsp;:</td><td class="TX"> </td>
								</tr>
								<tr>
								  <td class="TX" colspan="2"><textarea id="txt3" name="txt3" rows="15" cols="80" style="width: 80%"><?php echo htmlentities($content3) ?></textarea>
								  </td>
								</tr>
							  </table>
							  <p>&nbsp;</p>
							</td>
							<td width="14"></td>
						  </tr>
						  <tr> 
							<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
							<td height="14"></td>
							<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
						  </tr>
						</table>
					  </td>
					</tr>
					<tr> 
					  <td align="center"> 
						<input type="submit" value="Valider" class="BN">
					  </td>
					</tr>
					<tr> 
					  <td align="right" width="180">&nbsp; </td>
					</tr>
				  </table>
				</form>
				</body>
				</html>
			<?php
			}
		}
	}
}else{

}
?>
