<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
		
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	/* On récupère les infos sur le dossier à simuler */
	$sql_sel_nomdoss 	= "SELECT DOSSIER_NOM FROM DOSSIER WHERE DOSSIER_ID=".intval($_GET['dossid']);
	$qry_sel_nomdoss 	= $db->query($sql_sel_nomdoss);
	$nom_dossier 		= $qry_sel_nomdoss[0]['dossier_nom'];	
	
	$sql_sel_questid 	= "SELECT * FROM RECUP_QUEST WHERE PROD_ID=".txt_db($_GET['prodid']);
	$qry_sel_questid 	= $db->query($sql_sel_questid);
	$questid_tmp 		= $qry_sel_questid[0]['txt_img_quest_id'];	
	
	
	if(isset($_POST['e1'])){
	
		/* On définit l'id candidat en fonction du sexe */
		switch($_GET['sexe']){
			case 'H':
			default:
				$cand_id = '-100';
			break;
			case 'F':
				$cand_id = '-99';
			break;
		}
		
		/* Supression des données temporaires */
		$sql_del = "DELETE OPERATION WHERE OPE_ID=-100";
		$qry_del = $db->query($sql_del);
		
		/* Insertion de l'operation */
		$sql_ins_ope = "INSERT INTO OPERATION (OPE_ID,PROD_ID) VALUES (-100,".txt_db($_GET['prodid']).")";
		$qry_ins_ope = $db->query($sql_ins_ope);

		/* Suppression des réponses temporaires */
		$sql_del_cand = "DELETE CAND_A_QUEST WHERE CAND_ID=".$cand_id;
		$qry_del_cand = $db->query($sql_del_cand);
		
		$x = 1;
		
		/* Boucle d'insertion des réponses pour les E+ */
		if($_POST['e1']>0){
			for ($i=1;$i<=intval($_POST['e1']);$i++){
				$sql_insert_pe1 = "INSERT INTO CAND_A_QUEST (CAND_ID,OPE_ID,QUEST_ID,CHOIX_ID,REPONSE_PI,REPONSE_PN) VALUES (".$cand_id.",-100,".$questid_tmp.",".$x.",'E','N')";
				$qry_insert_pe1 = $db->query($sql_insert_pe1);
				$x++;
			}
		}

		/* Boucle d'insertion des réponses pour les C+ */
		if($_POST['c1']>0){
			for ($i=1;$i<=intval($_POST['c1']);$i++){
				$sql_insert_pc1 = "INSERT INTO CAND_A_QUEST (CAND_ID,OPE_ID,QUEST_ID,CHOIX_ID,REPONSE_PI,REPONSE_PN) VALUES (".$cand_id.",-100,".$questid_tmp.",".$x.",'C','N')";
				$qry_insert_pc1 = $db->query($sql_insert_pc1);
				$x++;
			}
		}
		
		/* Boucle d'insertion des réponses pour les P+ */
		if($_POST['p1']>0){
			for ($i=1;$i<=intval($_POST['p1']);$i++){
				$sql_insert_pp1 = "INSERT INTO CAND_A_QUEST (CAND_ID,OPE_ID,QUEST_ID,CHOIX_ID,REPONSE_PI,REPONSE_PN) VALUES (".$cand_id.",-100,".$questid_tmp.",".$x.",'P','N')";
				$qry_insert_pp1 = $db->query($sql_insert_pp1);
				$x++;
			}
		}
		
		/* Boucle d'insertion des réponses pour les A+ */
		if($_POST['a1']>0){
			for ($i=1;$i<=intval($_POST['a1']);$i++){
				$sql_insert_pa1 = "INSERT INTO CAND_A_QUEST (CAND_ID,OPE_ID,QUEST_ID,CHOIX_ID,REPONSE_PI,REPONSE_PN) VALUES (".$cand_id.",-100,".$questid_tmp.",".$x.",'A','N')";
				$qry_insert_pa1 = $db->query($sql_insert_pa1);
				$x++;
			}
		}	
		
		/* Boucle d'insertion des réponses pour les E- */
		if($_POST['e2']>0){
			for ($i=1;$i<=intval($_POST['e2']);$i++){
				$sql_insert_pe2 = "INSERT INTO CAND_A_QUEST (CAND_ID,OPE_ID,QUEST_ID,CHOIX_ID,REPONSE_PI,REPONSE_PN) VALUES (".$cand_id.",-100,".$questid_tmp.",".$x.",'N','E')";
				$qry_insert_pe2 = $db->query($sql_insert_pe2);
				$x++;
			}
		}
		/* Boucle d'insertion des réponses pour les C- */
		if($_POST['c2']>0){
			for ($i=1;$i<=intval($_POST['c2']);$i++){
				$sql_insert_pc2 = "INSERT INTO CAND_A_QUEST (CAND_ID,OPE_ID,QUEST_ID,CHOIX_ID,REPONSE_PI,REPONSE_PN) VALUES (".$cand_id.",-100,".$questid_tmp.",".$x.",'N','C')";
				$qry_insert_pc2 = $db->query($sql_insert_pc2);
				$x++;
			}
		}
		
		/* Boucle d'insertion des réponses pour les P- */
		if($_POST['p2']>0){
			for ($i=1;$i<=intval($_POST['p2']);$i++){
				$sql_insert_pp2 = "INSERT INTO CAND_A_QUEST (CAND_ID,OPE_ID,QUEST_ID,CHOIX_ID,REPONSE_PI,REPONSE_PN) VALUES (".$cand_id.",-100,".$questid_tmp.",".$x.",'N','P')";
				$qry_insert_pp2 = $db->query($sql_insert_pp2);
				$x++;
			}
		}
		
		/* Boucle d'insertion des réponses pour les A- */
		if($_POST['a2']>0){
			for ($i=1;$i<=intval($_POST['a2']);$i++){
				$sql_insert_pa2 = "INSERT INTO CAND_A_QUEST (CAND_ID,OPE_ID,QUEST_ID,CHOIX_ID,REPONSE_PI,REPONSE_PN) VALUES (".$cand_id.",-100,".$questid_tmp.",".$x.",'N','A')";
				$qry_insert_pa2 = $db->query($sql_insert_pa2);
				$x++;
			}
		}
		header('location:gen_doc_equipe.php?force=1&equipe=0&candid='.$cand_id.'&opeid=-100&dossid='.$_GET['dossid']);
	}


	$sql1 = "select max(COR_VAR_SOMME) SOMME,COR_VAR_POINTS from quest_a_corresp_var where COR_VAR_QUEST_ID=".$questid_tmp." AND COR_VAR_TYPE_PROFIL_CODE_ID=29 AND 
	COR_VAR_TYPE_VARIABLE_CODE_ID=24 GROUP BY COR_VAR_POINTS";
	$qry1 = $db->query($sql1);
	
	$sql2 = "select max(COR_VAR_SOMME) SOMME,COR_VAR_POINTS from quest_a_corresp_var where COR_VAR_QUEST_ID=".$questid_tmp." AND COR_VAR_TYPE_PROFIL_CODE_ID=29 AND 
	COR_VAR_TYPE_VARIABLE_CODE_ID=26 GROUP BY COR_VAR_POINTS";
	$qry2 = $db->query($sql2);
	
	$sql3 = "select max(COR_VAR_SOMME) SOMME,COR_VAR_POINTS from quest_a_corresp_var where COR_VAR_QUEST_ID=".$questid_tmp." AND COR_VAR_TYPE_PROFIL_CODE_ID=29 AND 
	COR_VAR_TYPE_VARIABLE_CODE_ID=25 GROUP BY COR_VAR_POINTS";
	$qry3 = $db->query($sql3);
	
	$sql4 = "select max(COR_VAR_SOMME) SOMME,COR_VAR_POINTS from quest_a_corresp_var where COR_VAR_QUEST_ID=".$questid_tmp." AND COR_VAR_TYPE_PROFIL_CODE_ID=29 AND 
	COR_VAR_TYPE_VARIABLE_CODE_ID=27 GROUP BY COR_VAR_POINTS";
	$qry4 = $db->query($sql4);
	
	$sql5 = "select max(COR_VAR_SOMME) SOMME,COR_VAR_POINTS from quest_a_corresp_var where COR_VAR_QUEST_ID=".$questid_tmp." AND COR_VAR_TYPE_PROFIL_CODE_ID=30 AND 
	COR_VAR_TYPE_VARIABLE_CODE_ID=24 GROUP BY COR_VAR_POINTS";
	$qry5 = $db->query($sql5);
	
	$sql6 = "select max(COR_VAR_SOMME) SOMME,COR_VAR_POINTS from quest_a_corresp_var where COR_VAR_QUEST_ID=".$questid_tmp." AND COR_VAR_TYPE_PROFIL_CODE_ID=30 AND 
	COR_VAR_TYPE_VARIABLE_CODE_ID=26 GROUP BY COR_VAR_POINTS";
	$qry6 = $db->query($sql6);
	
	$sql7 = "select max(COR_VAR_SOMME) SOMME,COR_VAR_POINTS from quest_a_corresp_var where COR_VAR_QUEST_ID=".$questid_tmp." AND COR_VAR_TYPE_PROFIL_CODE_ID=30 AND 
	COR_VAR_TYPE_VARIABLE_CODE_ID=25 GROUP BY COR_VAR_POINTS";
	$qry7 = $db->query($sql7);
	
	$sql8 = "select max(COR_VAR_SOMME) SOMME,COR_VAR_POINTS from quest_a_corresp_var where COR_VAR_QUEST_ID=".$questid_tmp." AND COR_VAR_TYPE_PROFIL_CODE_ID=30 AND 
	COR_VAR_TYPE_VARIABLE_CODE_ID=27 GROUP BY COR_VAR_POINTS";
	$qry8 = $db->query($sql8);
		
	
	
	?>
	<html>
		<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script type="text/javascript">
				<!--
				function MM_openBrWindow(theURL,winName,features) { //v2.0
				  window.open(theURL,winName,features);
				}
				-->
			</script>
		</head>
		<body bgcolor="#FFFFFF" text="#000000">
			<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;APERCU&nbsp;DU&nbsp;DOSSIER&nbsp;<?php echo htmlentities($nom_dossier) ?></td>
			</tr>
			</table>
			<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14" align="right"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14" align="left"></td>
				  <td align="center" class="TX"> 
				    <form name="form" method="post" action="#">
						<table border="0" cellpadding="0" cellspacing="0" width="800" align="center">
							<tr>
								<td class="TX">&nbsp;</td>
								<td class="TX" align="center"><b>E</b></td>
								<td class="TX" align="center"><b>C</b></td>
								<td class="TX" align="center"><b>P</b></td>
								<td class="TX" align="center"><b>A</b></td>
							</tr>
								<td class="TX" align="center"><b>PI</b></td>
								<td class="TX" align="center">
								<select name="e1" class="form_ediht_Tarifs" style="width:80px;">
								<?php
								if(is_array($qry1)){
									foreach($qry1 as $res1){
										unset($zero);
										if(intval($res1['cor_var_points'])<1){
											$zero = 0;
										}
										echo '<option value="'.$res1['somme'].'">'.$zero.$res1['cor_var_points'].'</option>';
									}
								}
								?>
								</select>
								</td>
								<td class="TX" align="center">
								<select name="c1" class="form_ediht_Tarifs" style="width:80px;">
								<?php
								if(is_array($qry2)){
									foreach($qry2 as $res2){
										unset($zero);
										if(intval($res2['cor_var_points'])<1){
											$zero = 0;
										}
										echo '<option value="'.$res2['somme'].'">'.$zero.$res2['cor_var_points'].'</option>';
									}
								}
								?>
								</select>
								</td>
								<td class="TX" align="center">
								<select name="p1" class="form_ediht_Tarifs" style="width:80px;">
								<?php
								if(is_array($qry3)){
									foreach($qry3 as $res3){
										unset($zero);
										if(intval($res3['cor_var_points'])<1){
											$zero = 0;
										}
										echo '<option value="'.$res3['somme'].'">'.$zero.$res3['cor_var_points'].'</option>';
									}
								}
								?>
								</select>
								</td>
								<td class="TX" align="center">
								<select name="a1" class="form_ediht_Tarifs" style="width:80px;">
								<?php
								if(is_array($qry4)){
									foreach($qry4 as $res4){
										unset($zero);
										if(intval($res4['cor_var_points'])<1){
											$zero = 0;
										}
										echo '<option value="'.$res4['somme'].'">'.$zero.$res4['cor_var_points'].'</option>';
									}
								}
								?>
								</select>
								</td>
							</tr>					
							<tr>
								<td class="TX" align="center"><b>PN</b></td>
								<td class="TX" align="center">
								<select name="e2" class="form_ediht_Tarifs" style="width:80px;">
								<?php
								if(is_array($qry5)){
									foreach($qry5 as $res5){
										unset($zero);
										if(intval($res5['cor_var_points'])<1){
											$zero = 0;
										}
										echo '<option value="'.$res5['somme'].'">'.$zero.$res5['cor_var_points'].'</option>';
									}
								}
								?>
								</select>
								</td>
								<td class="TX" align="center">
								<select name="c2" class="form_ediht_Tarifs" style="width:80px;">
								<?php
								if(is_array($qry6)){
									foreach($qry6 as $res6){
										unset($zero);
										if(intval($res6['cor_var_points'])<1){
											$zero = 0;
										}
										echo '<option value="'.$res6['somme'].'">'.$zero.$res6['cor_var_points'].'</option>';
									}
								}
								?>
								</select>
								</td>
								<td class="TX" align="center">
								<select name="p2" class="form_ediht_Tarifs" style="width:80px;">
								<?php
								if(is_array($qry7)){
									foreach($qry7 as $res7){
										unset($zero);
										if(intval($res7['cor_var_points'])<1){
											$zero = 0;
										}
										echo '<option value="'.$res7['somme'].'">'.$zero.$res7['cor_var_points'].'</option>';
									}
								}
								?>
								</select>
								</td>
								<td class="TX" align="center">
								<select name="a2" class="form_ediht_Tarifs" style="width:80px;">
									<?php
									if(is_array($qry8)){
										foreach($qry8 as $res8){
										unset($zero);
										if(intval($res8['cor_var_points'])<1){
											$zero = 0;
										}
											echo '<option value="'.$res8['somme'].'">'.$zero.$res8['cor_var_points'].'</option>';
										}
									}
									?>
								</select>
								</td>
							</tr>
						</table>
					</form>
				  </td>
				  <td width="14" align="right"></td>
				</tr>
				
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14" align="right"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			</table>
			<br>
			<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr><td class="TX" align="center"><input type="button" onclick="document.form.action='pop_apercu.php?prodid=<?php echo $_GET['prodid'] ?>&dossid=<?php echo $_GET['dossid'] ?>&sexe=H'; document.form.submit();" class="BN" value="HOMME">&nbsp;<input type="button" class="BN" onclick="document.form.action='pop_apercu.php?prodid=<?php echo $_GET['prodid'] ?>&dossid=<?php echo $_GET['dossid'] ?>&sexe=F'; document.form.submit();" value="FEMME"></td></tr>
			</table>
			
		</body>
	</html>
	<?php
	
	
}else{
	include('no_acces.php');
}
?>