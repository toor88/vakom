<?php
session_start();
// Si l'utilisateur est un super admin ou un admin vakom
if ($_SESSION['droit']>1){

	if ($_GET['candid']>0){
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);
				
		if ($_POST['nom'] != ''){ // Si le formulaire est envoyé
			if ($_POST['libre']!='1'){
				$cli_id = $_POST['select_client'];
			}else{
				$cli_id = '-1';
			}
			if ($_POST['inactif']== 1){
				$actif = 0;
			}else{
				$actif = 1;
			}
			$dns = $_POST['JJ_DNS'].'/'.$_POST['MM_DNS'].'/'.$_POST['AAAA_DNS'];
			
			/* Si le pays selectionné n'est pas la france */
			if ($_POST['select_pays']!='5'){
				/* Le CP a pour valeur le champ cp_etr et la ville a pour valeur le champ ville_etr*/
				$cp 	= $_POST['cp_etr'];
				$ville 	= $_POST['ville_etr'];
			}else{
			/* Si le pays selectionné est la france, le CP porte la valeur du champ cp */
				$cp 	= $_POST['cp'];
				/* Si la ville n'existe pas dans la base, la ville porte la valeur du champ nom_new_ville */
				if ($_POST['new_ville']=='1'){
					$ville 	= $_POST['nom_new_ville'];
				}else{
					/* Si elle existe, la valeur de la ville est celle de la liste de sélection */
					$tab_v 	=  explode("_",$_POST['ville']);
					$ville 	= $tab_v[1];
				}
			}
			
			
			$sql_update_candidat = "UPDATE CANDIDAT SET 
			CAND_NOM = upper('".txt_db($_POST['nom'])."'),
			CAND_PRENOM = '".txt_db($_POST['prenom'])."',
			CAND_EMAIL = '".txt_db($_POST['mail'])."',
			CAND_SEXE = '".txt_db($_POST['sexe'])."',";
			
			if (strlen($_POST['JJ_DNS'])==2 && strlen($_POST['MM_DNS'])==2 && strlen($_POST['AAAA_DNS'])==4){
				$sql_update_candidat .= "CAND_DNS = TO_DATE('".txt_db($dns)."','DD/MM/YYYY'),";
			}else{
				$sql_update_candidat .= "CAND_DNS = '',";
			}
			
			if($_POST['transfert']==1 && $_POST['certs']>0){
				$sql_update_candidat .= "CAND_CERT_ID = ".intval($_POST['certs']).", CAND_CLI_ID = -1,";
			}else{
				$sql_update_candidat .= "CAND_CLI_ID='".txt_db($cli_id)."',";
			}
			
			$sql_update_candidat .= "CAND_FONCTION = '".txt_db($_POST['fct'])."',
			CAND_LANG_ID  = '".txt_db($_POST['langue'])."',
			CAND_AD1 = '".txt_db($_POST['adr1'])."',
			CAND_AD2 = '".txt_db($_POST['adr2'])."',
			CAND_CP = '".txt_db($cp)."',
			CAND_VILLE = '".txt_db($ville)."',
			CAND_PAYS_CODE_ID = '".txt_db($_POST['select_pays'])."',
			CAND_TEL = '".txt_db($_POST['tel'])."',
			CAND_PORTABLE = '".txt_db($_POST['tel_p'])."',
			CAND_FAX = '".txt_db($_POST['fax'])."',
			CAND_INFO1 = '".txt_db($_POST['comm1'])."',
			CAND_INFO2 = '".txt_db($_POST['comm2'])."',
			CAND_ACTIF = '".txt_db(intval($actif))."',
			CATEGORIE_SP_CODE_ID = '".txt_db(intval($_POST['categorie_SP']))."',
			ACTIVITE_CODE_ID = '".txt_db(intval($_POST['activite']))."',
			CAND_USER_MODIFICATION_ID = '".$_SESSION['vak_id']."',
			CAND_DATE_MODIFICATION = SYSDATE WHERE CAND_ID = '".txt_db(intval($_GET['candid']))."'";
			//echo $sql_update_candidat;
			$qry_update_candidat = $db->query($sql_update_candidat);
			
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			<?php
		}
		/* On génére la liste des catégories socio-professionnelles */
		$sql_categorie_SP_list = "SELECT * FROM CODE WHERE CODE_TABLE='CATEGORIE_SP' AND CODE_ACTIF=1 AND CODE_LANG_ID=" .$_SESSION['lang'] ." ORDER BY CODE_ORDRE";

		$qry_categorie_SP_list = $db->query($sql_categorie_SP_list);
		
		/* On génére la liste des activités */
		$sql_activite_list = "SELECT * FROM CODE WHERE CODE_TABLE='ACTIVITE' AND CODE_ACTIF=1 AND CODE_LANG_ID=" .$_SESSION['lang'] ." ORDER BY CODE_ORDRE";

		$qry_activite_list = $db->query($sql_activite_list);
		
		/* On sélectionne les informations du candidat */
		$sql_cand		= "SELECT TO_CHAR(CAND_DNS, 'DD') JJ_DNS, TO_CHAR(CAND_DNS, 'MM') MM_DNS, TO_CHAR(CAND_DNS, 'YYYY') AAAA_DNS, CANDIDAT.*, TO_CHAR(CANDIDAT.CAND_DATE_CREATION, 'DD/MM/YYYY HH24:MI') CAND_DATE_CREATION1, TO_CHAR(CANDIDAT.CAND_DATE_MODIFICATION, 'DD/MM/YYYY HH24:MI') CAND_DATE_MODIFICATION1 FROM CANDIDAT WHERE CAND_ID=".intval($_GET['candid']);
		$qry_cand 		= $db->query($sql_cand);
		//echo $sql_cand;
		/* On sélectionne la liste des pays */
		$sql_pays_list = "SELECT * FROM CODE WHERE CODE_TABLE='PAYS'";
		$qry_pays_list = $db->query($sql_pays_list);
		
		/* On sélectionne la liste des langues disponibles */
		$sql_langue_list = "SELECT * FROM LANGUE ORDER BY LANG_ID";
		$qry_langue_list = $db->query($sql_langue_list);
		
		/* On sélectionne la liste des clients du certifié */
		$sql_liste_clients = "SELECT DISTINCT CLIENT.CLI_ID, CLIENT.CLI_NOM, CLIENT.CLI_VILLE,CLIENT.CLI_CP FROM CLIENT, CLIENT_A_CERT, CERTIFIE 
		WHERE cli_actif = 1 AND CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND CLIENT_A_CERT.CERT_ID=CERTIFIE.CERT_ID AND CERTIFIE.CERT_PART_ID='".txt_db(intval($_GET['partid']))."' ORDER BY CLIENT.CLI_NOM ASC";
//		CLIENT_A_CERT.CERT_ID='".txt_db(intval($qry_cand[0]['cand_cert_id']))."' ORDER BY CLIENT.CLI_NOM ASC";
//		echo $sql_liste_clients;
		$qry_liste_clients = $db->query($sql_liste_clients);

		?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<script language="JavaScript">
		<!--
		function MM_openBrWindow(theURL,winName,features) { //v2.0
		  window.open(theURL,winName,features);
		}
		function hid_ville(id_pays){
				if (id_pays != "5"){
					document.getElementById('cp1').style.display='none';
					document.getElementById('cp2').style.display='inline';
					document.getElementById('ville2').style.display='none';
					document.getElementById('ville3').style.display='inline';
				}else{
					document.getElementById('cp1').style.display='inline';
					document.getElementById('cp2').style.display='none';
					document.getElementById('ville2').style.display='inline';
					document.getElementById('ville3').style.display='none';
				}
			}
		
		function check_libre(){
			if (document.form.libre.checked==true){
				document.getElementById('pas_libre').style.display='none';
			}
			if (document.form.libre.checked==false){
				document.getElementById('pas_libre').style.display='block';
			}
		}
		
		function verif(){
			document.form.valider.value='Patientez...';
			/*---------------- new -----------------------*/
			var mail 		= document.form.mail.value;
			var method   	= "GET"; //On définit la methode (ici je passe le code postal par l'url)
			var filename 	= "ajax_verif_exist.php"; // La page qui réceptionne les données
			var data     	= null; 
			
			
			var xhr_object = null; 
					 
			if(window.XMLHttpRequest) // Firefox 
			   xhr_object = new XMLHttpRequest(); 
			else if(window.ActiveXObject) // Internet Explorer 
			   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
			else { // XMLHttpRequest non supporté par le navigateur 
			   alert("<?php echo $t_browser_support_error_1 ?>");
			   return; 
			}
			
			data = "mail="+mail;	// seul le mail est vérifié
				
			if(method == "GET" && data != null) {
			   filename += "?"+data;
			   data      = null;
			}
			
			xhr_object.open(method, filename, true);
			
			xhr_object.onreadystatechange = function() {
				   if(xhr_object.readyState == 4) {
					  var tmp = xhr_object.responseText.split(":"); 
					  /* if(typeof(tmp[0]) != "undefined") {
						 if (tmp[0]!=''){
							var exist = true;
						 }
 						 else
							var exist = false;
					  } */

					  if(typeof(tmp[0]) != "undefined") {
						 if (tmp[0]==1){
							var exist = true;
						 }
 						 else
							var exist = false;
						if (tmp[0]==2){
							var mailexist = true;
						 }
 						 else
							var mailexist = false;
					  }
			
			
			/*--------------------------------------------*/	
			var error = '';
			var warning = '';
			
			
			if (document.form.nom.value == ''){
				error += "<?php echo $t_nom_oblig ?>\n";
			}
			if (document.form.prenom.value == ''){
				error += "<?php echo $t_prenom_oblig ?>\n";
			}
			if (document.form.mail.value == ''){
				warning += "<?php echo $t_aucune_adr_email  ?>";
			}else{
				if(testMail(document.form.mail.value) == false)
					error += "<?php echo $t_email_format_incor ?>";;
			}
			if (mailexist == true){
						warning += "<?php echo $t_email_exist_deja ?>";
					}
			
			if (document.form.JJ_DNS.value.length>0 || document.form.MM_DNS.value.length>0 || document.form.AAAA_DNS.value.length>0){
				// verif du format date du début
				error1=false;
				if (document.form.JJ_DNS.value<1 || document.form.JJ_DNS.value>31 || document.form.JJ_DNS.value.length<2){
					error1 = true;
				}
				if (document.form.MM_DNS.value<1 || document.form.MM_DNS.value>12 || document.form.MM_DNS.value.length<2){
					error1 = true;
				}
				if (document.form.AAAA_DNS.value<1909 || document.form.AAAA_DNS.value.length<4){
					error1 = true;
				}
				if (error1==true){
					error +="<?php echo $t_mauvaise_date_naiss ?>\n";
				}
			}
			if (document.getElementById('sexe1').checked == false && document.getElementById('sexe2').checked == false){
				error += '<?php echo $t_sex_obligatoire  ?>';
			}
			
			/* Si il y a une erreur, on l'affiche */
					if (error!=''){
						document.form.valider.value='VALIDER';
						alert(error);
					}else{ // Sinon on soumet le formulaire
						if(warning == '')
							document.form.submit();
						else if(warning != ''){
							if(confirm(warning)){
								if (exist==true){
									if (confirm("<?php echo $t_utilisateur_existe  ?>")){
										document.form.submit();
									}
									else{
										document.form.valider.value='VALIDER';
									}
								}
								else
									document.form.submit();
							}
							else
								return;
						}
					}
				}
			}
			xhr_object.send(data); //On envoie les données
		}
		
		function charge_code(code){
				var tmp_code = code.split("_");
				document.getElementById('cp').value = tmp_code[0];
			}
		
			function TestVille(){
				/* var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_ville.php"; // La page qui réceptionne les données
				var cp = document.getElementById('cp').value; 
				var data     = null; 
				
				if 	(cp.length>1){ //Si le code postal tapé possède au moins 2 caractères
					document.getElementById("ville2").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" />';
					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("<?php echo $t_browser_support_error_1 ?>");
						   return; 
						} 
						 
						
						 
						if(cp != ""){
							data = "cp="+cp+"&class=cand";
						}
						if(method == "GET" && data != null) {
						   filename += "?"+data;
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split(":"); 
							  if(typeof(tmp[0]) != "undefined") { 
								 document.getElementById("ville2").innerHTML = '';
								 if (tmp[0]!=''){
									document.getElementById("ville2").innerHTML = tmp[0];
								 }else{
									document.getElementById("ville2").innerHTML = '<input type="hidden" name="new_ville" value="1"><input type="text" size="46" name="nom_new_ville" maxlength="255" class="form_ediht_Candidats" />';
								 }
							  }
						   } 
						} 

						xhr_object.send(data); //On envoie les données
				} */
			}
			
			function charge_certifies(){
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_liste_certifies.php"; // La page qui réceptionne les données
				var partid = document.getElementById('select_partenaires').options[document.getElementById('select_partenaires').selectedIndex].value; 
				var data     = null; 
				
				if 	(partid>0){ //Si le code postal tapé possède au moins 2 caractères
					document.getElementById("div_list_certif").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" />';
					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("<?php echo $t_browser_support_error_1 ?>");
						   return; 
						} 
						 
						
						data = "partid="+partid+"&class=cand";
						
						if(method == "GET" && data != null) {
						   filename += "?"+data;
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split(":"); 
							  if(typeof(tmp[0]) != "undefined") { 
								 document.getElementById("div_list_certif").innerHTML = '';
								 if (tmp[0]!=''){
									document.getElementById("div_list_certif").innerHTML = tmp[0];
								 }
							  }
						   } 
						} 

						xhr_object.send(data); //On envoie les données
				}
			}
		
		function pass(champ){
			if (champ==1){
				if (document.getElementById('JJ_DNS').value.length==2){
					document.getElementById('MM_DNS').value='';
					document.getElementById('MM_DNS').focus();
				}
			}
			if (champ==2){
				if (document.getElementById('MM_DNS').value.length==2){
					document.getElementById('AAAA_DNS').value='';
					document.getElementById('AAAA_DNS').focus();
				}
			}
			if (champ==3){
				if (document.getElementById('AAAA_DNS').value.length==4){
					document.getElementById('fct').focus();
				}
			}
		}
		
		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
function testMail(emailStr) {
	var checkTLD = 1;
	var knownDomsPat = /^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum|fr)$/;
	var emailPat = /^(.+)@(.+)$/;
	var specialChars = "\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	var validChars = "\[^\\s" + specialChars + "\]";
	var quotedUser = "(\"[^\"]*\")";
	var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom = validChars + '+';
	var word = "(" + atom + "|" + quotedUser + ")";
	var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat = new RegExp("^" + atom + "(\\." + atom +")*$");
	var matchArray = emailStr.match(emailPat);
	if (matchArray == null) { return false; }
	var user = matchArray[1];
	var domain = matchArray[2];
	for (i=0; i<user.length; i++) {
		if (user.charCodeAt(i) > 127) { return false; }
	}
	for (i=0; i<domain.length; i++) {
		if (domain.charCodeAt(i) > 127) { return false; }
	}
	if (user.match(userPat) == null) { return false; }
	var IPArray=domain.match(ipDomainPat);
	if (IPArray != null) {
		for (var i=1; i<=4; i++) {
			if (IPArray[i] > 255) { return false; }
		}
		return true;
	}
	var atomPat = new RegExp("^" + atom + "$");
	var domArr = domain.split(".");
	var len = domArr.length;
	for (i=0; i<len; i++) {
		if (domArr[i].search(atomPat) == -1) { return false; }
	}
	if (checkTLD && domArr[domArr.length-1].length!=2 && domArr[domArr.length-1].search(knownDomsPat)==-1) { return false; }
	if (len < 2) { return false; }
	return true;
}
		//-->
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#" name="form">
			<table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo ucfirst($qry_cand[0]['cand_nom']).' '.ucfirst($qry_cand[0]['cand_prenom']) ?></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><table border="0" cellspacing="0" cellpadding="0"  width="100%" align="center">
			<tr>
			  <td >&nbsp;</td>
			  <td>&nbsp;</td>
			  </tr>
			<tr> 
			  <td class="fond_tablo_candidats" height="40" width="433" align="left"> <?php echo $t_fiche_edit_cand ?></td>
			  <td class="fond_tablo_candidats2" valign="middle" align="left" width="439"><?php echo $t_date_creation ?> 
			    : le <?php echo $qry_cand[0]['cand_date_creation1'] ?><br>
			    <?php
				  if ($qry_cand[0]['cand_date_modification1']!=''){
					  ?>
				    <?php echo $t_date_modif ?> : le <?php echo $qry_cand[0]['cand_date_modification1'] ?>
				    <?php
				  }
				  ?>
			    </td>
		      </tr>
			<tr> 
			  <td></td>
			  <td class="champsoblig" valign="middle" align="right" width="439"><?php echo $t_champs_oblig ?> 
				* </td>
		    </tr>

			</table></td>
			  </tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><table border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td class="TX"><?php echo $t_cand_libre ?> :  				  </td>
				<td class="TX"><input type="checkbox" name="libre" value="1" onClick="check_libre()" <?php if ($qry_cand[0]['cand_cli_id']=='-1'){ echo 'checked="checked"';}?>></td>
				<td class="TX"><?php if ($qry_cand[0]['cand_actif']=='0'){ echo '<span style="color: #FF0000; font-weight: bold; font-size: 20px;">'; }else{ echo '<span>'; } ?><?php echo $t_fiche_cand_inactif ?> </span>:</td>
					<td class="TX"> 
					  <input type="checkbox" name="inactif" value="1" <?php if ($qry_cand[0]['cand_actif']=='0'){ echo ' checked="checked"';} ?>>
					</td>
				</tr>
				 <tr> 
					<td class="TX" colspan="4"> 
						<div id="pas_libre" <?php if ($qry_cand[0]['cand_cli_id']=='-1'){ echo 'style="display: none"';}?>>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr><td class="TX"><?php echo $t_societe ?>&nbsp;:</td>
							<td>
							<select name="select_client" class="form_ediht_Candidats" style="margin-left:48px">
								<?php
								if (is_array($qry_liste_clients)){
									foreach($qry_liste_clients as $client_liste){
										unset($selected_clt);
										if ($qry_cand[0]['cand_cli_id'] == $client_liste['cli_id']){
											$selected_clt = ' selected="selected"';
										}
										echo '<option value="'.$client_liste['cli_id'].'"'.$selected_clt.'>'.strtoupper($client_liste['cli_nom']).' - '.ucfirst($client_liste['cli_cp']).' '.ucfirst($client_liste['cli_ville']).'</option>';
									}
								}else{
									echo '<option value="-1">'.$t_aucun_clt_cert_cand.'</option>';
								}
								?>
							  </select>&nbsp;<input class="bn_ajouter" type="button" value="<?php echo $t_btn_creer_client ?>" onClick="MM_openBrWindow('admvak_crea_certifieClient.php?certid=<?php echo $qry_cand[0]['cand_cert_id'] ?>','crea_client','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')"></td></tr></table>
						</div>
					</td>
				</tr>
				  <tr> 
					<td class="TX"><?php echo $t_nom ?>*&nbsp;:</td>
					<td class="TX"> 
					  <input type="text" name="nom" size="40" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_cand[0]['cand_nom']) ?>">
					  &nbsp;&nbsp;&nbsp;&nbsp; </td>
					<td class="TX"><?php echo $t_prenom ?>*&nbsp;:</td>
					<td class="TX"> 
					  <input type="text" name="prenom" size="40" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_cand[0]['cand_prenom']) ?>">
					</td>
				  </tr>
				  <tr> 
					<td class="TX"><?php echo $t_email ?>*&nbsp;:</td>
					<td class="TX" colspan="3" > 
					  <input type="text" name="mail" size="40" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_cand[0]['cand_email']) ?>">
					</td>
				  </tr>
				  <tr> 
					<td class="TX"><?php echo $t_sexe ?>*&nbsp;:</td>
					<td class="TX"> 
					  <input type="radio"  id="sexe1" name="sexe" value="H" <?php if ($qry_cand[0]['cand_sexe']=='H'){ echo ' checked="checked"';} ?>>
					  <?php echo $t_M ?>&nbsp;&nbsp; 
					  <input type="radio"  id="sexe2" name="sexe" value="F" <?php if ($qry_cand[0]['cand_sexe']=='F'){ echo ' checked="checked"';} ?>>

					  <?php echo $t_F ?> </td>
					<td class="TX"><?php echo $t_date_naissance ?>&nbsp;:&nbsp;</td>
					<td class="TX"> 
					  <input type="text" name="JJ_DNS" id="JJ_DNS" onKeyUp="pass(1)" size="2" class="form_ediht_Candidats" maxlength="2" value="<?php echo htmlentities($qry_cand[0]['jj_dns']) ?>">
					  / 
					  <input type="text" name="MM_DNS" id="MM_DNS" onKeyUp="pass(2)" size="2" class="form_ediht_Candidats" maxlength="2" value="<?php echo htmlentities($qry_cand[0]['mm_dns']) ?>">
					  / 
					  <input type="text" name="AAAA_DNS" id="AAAA_DNS" onKeyUp="pass(3)" size="4" class="form_ediht_Candidats" maxlength="4" value="<?php echo htmlentities($qry_cand[0]['aaaa_dns']) ?>">
					</td>
				  </tr>
				  <tr> 
					<td class="TX"><?php echo $t_categorie_SP ?> :</td>
					<td class="TX"> 
				    <select name="categorie_SP" class="form_ediht_Candidats" style="margin-right: 5px; width:270px">
					<option value="-1"></option>
						<?php
						  if (is_array($qry_categorie_SP_list)){
							foreach($qry_categorie_SP_list as $categorie_SP){
								unset($selected_categorie_SP);
								if($categorie_SP['code_id']==$qry_cand[0]['categorie_sp_code_id']){
									$selected_categorie_SP = ' selected="selected"';
								}
								echo '<option value="'.$categorie_SP['code_id'].'"'.$selected_categorie_SP.'>'.$categorie_SP['code_libelle'].'</option>';
							}
						  }
						?>
					  </select>
					</td>
					<td class="TX"><?php echo $t_activite ?> :</td>
					<td class="TX"> 
					  <select name="activite" class="form_ediht_Candidats" style="width:270px">
					  <option value="-1"></option>
						<?php
						  if (is_array($qry_activite_list)){
							foreach($qry_activite_list as $activite){
								unset($selected_activite);
								if($activite['code_id']==$qry_cand[0]['activite_code_id']){
									$selected_activite = ' selected="selected"';
								}
								echo '<option value="'.$activite['code_id'].'"'.$selected_activite.'>'.$activite['code_libelle'].'</option>';
							}
						  }
						?>
					  </select>
					</td>
				  </tr>
				  <tr> 
					<td class="TX"><?php echo $t_fonction ?>&nbsp;:</td>
					<td class="TX"> 
					  <input type="text" name="fct" id="fct" size="40" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_cand[0]['cand_fonction']) ?>">
					</td>
					<td class="TX"><?php echo $t_langue ?>*&nbsp;:</td>
					<td class="TX"> 
					  <select name="langue" class="form_ediht_Candidats" style="width:270px">
						<?php
						  if (is_array($qry_langue_list)){
							foreach($qry_langue_list as $langue){
								unset($selected_lang);
								if ($qry_cand[0]['cand_lang_id'] == $langue['lang_id']){
									$selected_lang = ' selected="selected"';
								}
								echo '<option value="'.$langue['lang_id'].'"'.$selected_lang.'>'.$langue['lang_libelle'].'</option>';
							}
						  }
						?>
					  </select>
					</td>
				  </tr>
				  <tr>
				    <td class="TX"><?php echo $t_adresse ?>&nbsp;:</td>
				    <td class="TX"><input type="text" name="adr1" size="40" class="form_ediht_Candidats" maxlength="100" value="<?php echo htmlentities($qry_cand[0]['cand_ad1']) ?>"></td>
				    <td class="TX"><?php echo $t_cp ?>&nbsp;:</td>
				    <td class="TX"><div id="cp1" width="1%" <?php if($qry_cand[0]['cand_pays_code_id']==5 || !$qry_cand[0]['cand_pays_code_id']){ echo 'style="display: inline;"'; } else{ echo 'style="display: none;"'; } ?>>
						  <input type="text" name="cp" id="cp" value="<?php echo htmlentities($qry_cand[0]['cand_cp']) ?>" size="5" maxlength="5" class="form_ediht_Candidats" onkeyUp="TestVille()"> <!--onblur="charge_code(document.getElementById('ville').options[document.getElementById('ville').selectedIndex].value)"-->
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						  </div>
						  <div id="cp2" width="1%" <?php if($qry_cand[0]['cand_pays_code_id']==5){ echo 'style="display: none;"'; } else{ echo 'style="display: inline;"'; } ?>>
						  <input type="text" name="cp_etr" id="cp_etr" value="<?php echo htmlentities($qry_cand[0]['cand_cp']) ?>" size="5" maxlength="5" class="form_ediht_Candidats">
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						  </div>
					
					
					  <!--<input type="text" name="cp" size="5" class="form_ediht_Candidats" maxlength="5" value="<?php echo htmlentities($qry_cand[0]['cand_cp']) ?>">--></td>
			      </tr>
				  <tr> 
					<td class="TX"></td>
					<td class="TX"> 
					  <input type="text" name="adr2" size="40" class="form_ediht_Candidats" maxlength="100" value="<?php echo htmlentities($qry_cand[0]['cand_ad2']) ?>">
					</td>
					<td class="TX">
					
					<?php echo $t_ville ?> :
					</td>
					<td class="TX"> 
					
					<div id="ville2" width="1%" style="display: inline;"><input type="hidden" id="ville" name="ville" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_cand[0]['cand_ville']) ?>"></div>
					<input type="hidden" name="new_ville" value="1"><input type="text" size="40" name="nom_new_ville" maxlength="255" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_cand[0]['cand_ville']) ?>"/>
						<div id="ville3" style="display: none;">
							<input type="text" name="ville_etr" value="<?php echo htmlentities($qry_cand[0]['cand_ville']) ?>" maxlength="255" class="form_ediht_Candidats">
						</div>
					
					
					
					 <!--<input type="text" name="ville" size="40" class="form_ediht_Candidats" maxlength="50" value="<?php echo htmlentities($qry_cand[0]['cand_ville']) ?>">-->
					</td>
				  </tr>
				  <tr> 
					<td class="TX"><?php echo $t_pays ?>&nbsp;:&nbsp;</td>
					<td class="TX"> 
					  <select name="select_pays" class="form_ediht_Candidats" onChange="hid_ville(document.getElementById('pays').options[document.getElementById('pays').selectedIndex].value)">
					<?php
					  if (is_array($qry_pays_list)){
						foreach($qry_pays_list as $pays){
							unset($selected_pays);
							if ($qry_cand[0]['cand_pays_code_id'] == $pays['code_id']){
								$selected_pays = ' selected="selected"';
							}
							echo '<option value="'.$pays['code_id'].'"'.$selected_pays.'>'.$pays['code_libelle'].'</option>';
						}
					  }
					?>
					  </select>
					</td>
					<td class="TX">&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr> 
					<td class="TX"><?php echo $t_tel_fix ?>&nbsp;:</td>
					<td class="TX"> 
					  <input type="text" name="tel" size="40" class="form_ediht_Candidats" maxlength="25" value="<?php echo htmlentities($qry_cand[0]['cand_tel']) ?>">
					</td>
					<td class="TX"><?php echo $t_tel_port ?>&nbsp;:&nbsp;</td>
					<td class="TX"> 
					  <input type="text" name="tel_p" size="40" class="form_ediht_Candidats" maxlength="25" value="<?php echo htmlentities($qry_cand[0]['cand_portable']) ?>">
					</td>
				  </tr>
				  <tr> 
					<td class="TX"><?php echo $t_fax ?>&nbsp;:&nbsp;</td>
					<td class="TX"> 
					  <input type="text" name="fax" size="40" class="form_ediht_Candidats" maxlength="25" value="<?php echo htmlentities($qry_cand[0]['cand_fax']) ?>">
					</td>
					<td class="TX">&nbsp;</td>
					<td class="TX">&nbsp; </td>
				  </tr>
				  <tr> 
					<td class="TX"><?php echo $t_comment ?>&nbsp;:</td>
					<td class="TX" colspan="3"> 
					  <input type="text" name="comm1" class="form_ediht_Candidats" maxlength="70" style="width:100%" value="<?php echo htmlentities($qry_cand[0]['cand_info1']) ?>">
				    </td>
				</tr>
				<tr>
					<td class="TX"></td>
					<td class="TX" colspan="3"> 
						<input type="text" name="comm2" style="width:100%" class="form_ediht_Candidats" maxlength="70" value="<?php echo htmlentities($qry_cand[0]['cand_info2']) ?>">
				  </td>
    </tr>
				  
				  <?php
				  if ($_SESSION['droit']==9){
				  /* Génération de la liste des partenaires */
					$sql_list_part = "SELECT * FROM PARTENAIRE WHERE (PART_BLOQUE=0 OR PART_BLOQUE IS NULL) ORDER BY PART_NOM,PART_RS ASC";
					$qry_list_part = $db->query($sql_list_part);
					  ?>					  
					  <tr>
						<td class="TX" colspan="3"><input type="checkbox" name="transfert" value="1">&nbsp;Transf&eacute;rer le candidat : 
						  <select name="select_partenaires" id="select_partenaires" onChange="charge_certifies();" class="form_ediht_Candidats">
							<option selected value="0">Liste des Partenaires</option>
							<?php
							if(is_array($qry_list_part)){
								foreach($qry_list_part as $elmt_part){
									echo '<option value="'.$elmt_part['part_id'].'">'.htmlentities($elmt_part['part_nom'].' '.$elmt_part['part_rs']).'</option>';
								}
							}
							?>							
						  </select>&nbsp;<div style="display: inline;" id="div_list_certif"></div>
						</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  
					  <?php
				  }
				  ?>
		  </table></td>
			  </tr>
           </table>
		 




<br>




		  <br>
		  <p style="text-align:center">
				<input type="button" name="valider" value="<?php echo $t_btn_valider ?>" class="bn_valider_candidat" onClick="verif()">
		  </p>
		  <p style="text-align:center">&nbsp;</p>
		</form>
		</body>
		</html>
	<?php
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{
	include('no_acces.php');
}
?>
