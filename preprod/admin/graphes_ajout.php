<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	/* On récupère le nom du graphe voulu */
	$sql_info_graphe = "SELECT GRAPHE_NOM,CODE_LIBELLE FROM GRAPHE,CODE WHERE GRAPHE.GRAPHE_TYPE_GRAPHE_CODE_ID=CODE.CODE_ID AND GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
	$qry_info_graphe = $db->query($sql_info_graphe);

	
	if (is_array($qry_info_graphe)){	
	
		/* Si le formulaire est posté */
		if ($_POST['select_quest']>0){
			
			/* On attribue le graphe au questionnaire */
			// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
			$sql_seq_num 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
			$qry_seq_num 		= $db->query($sql_seq_num);
			$seq_num			= intval($qry_seq_num[0]['seq_num']);
			
			/* On insert les info pour le graphe associé au questionnaire */
			$sql_ins_quest		= "INSERT INTO GRAPHE_A_QUEST VALUES('".$seq_num."', '".txt_db(intval($_GET['grapheid']))."', '".txt_db(intval($_POST['select_quest']))."', '".txt_db(intval($_POST['select_type_profil']))."', '".txt_db($_POST['couleur_fond'])."', '".txt_db($_POST['select_style'])."')";
			$qry_ins_quest		= $db->query($sql_ins_quest);
			
			
			/* On gèr les libellés du graphe pour le questionnaire sélectionné */
			if (is_array($_POST['langue'])){
				foreach ($_POST['langue'] as $langue_postee){ // Pour chaque langue postée
					if (trim($_POST[$langue_postee.'_lib']) != ''){ // Si le champ n'est pas vide
						/* On insere le nouveau texte */
						$sql_ins_lib = "INSERT INTO GRAPHE_A_LIB VALUES('".$seq_num."', '".txt_db(intval($langue_postee))."', '".txt_db($_POST[$langue_postee.'_lib'])."')";
						$qry_ins_lib = $db->query($sql_ins_lib);
					}
				}
			}
		
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>		
			<?php			
		}
		
		/* On fait la liste de tous les questionnaires de la base de données */
		$sql_quest_liste = "SELECT QUEST_NOM, QUEST_ID FROM QUESTIONNAIRE WHERE QUEST_DATE_SUPPRESSION IS NULL ORDER BY QUESTIONNAIRE.QUEST_NOM ASC";
		$qry_quest_liste = $db->query($sql_quest_liste);
		
		/* On fait la liste de tous les questionnaires de la base de données */
		$sql_type_profil_liste = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='TYPE_PROFIL' ORDER BY CODE_LIBELLE ASC";
		$qry_type_profil_liste = $db->query($sql_type_profil_liste);	
	
	?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<script type="text/javascript">
			<!--
			blocolor = true;
			function hexa(couleur){
			if(blocolor)
			document.getElementById('couleur').value = '#'+couleur; // On inscrit dans le champs TEXT la valeur HEX de la couleur
			}

			function palette_couleurs(){

			// Ouverture du tableau
			var tableau_palette = "<TABLE border='1' cellpadding='0' cellspacing='1'><TR>";

			// Déclaration des variables
			var tabk = new Array('FF','CC','99','66','33','00'); // Tableau principal de couleur
			var tabj = tabk;
			var tabi = new Array('CC', '66', '00'); // Petit tableau principal (colonne de 6 couleurs)
			var tabi2 = new Array('00','33','66','99','CC','FF'); // Tableau principal inversé pour la colone du milieu (colonne de 6 couleurs)
			var color=""; // initialisation de color a vide
			var cmp = 0; // initialisation du compteur a 0

			// Début prog
			for(var k=0;k<6;k++) // Boucle pour les lignes de couleurs
			{
			for(var i=0;i<3;i++) // Boucle pour les colonnes (colonnes de 6 couleurs)
			{
			if (i == 1) // Si on attaque la 2 eme colonne de 6 couleurs
			{
			tabj = tabi2; // on inverse le tableau principale de couleurs
			}
			else // sinon
			{
			tabj = Array('FF','CC','99','66','33','00'); // On remet le tableau par default
			}

			for(var j=0;j<6;j++) // Boucle pour l'affichage couleur par couleur
			{
			color="#"+tabi[i]+tabk[k]+tabj[j]; // concaténation des chaines pour la valeur de la couleur
			// et on affiche la couleur
			tableau_palette += "<TD width='15' height='15' style='border: 1px solid #000000;' bgcolor='"+color+"' onClick=\"hexa('"+tabi[i]+tabk[k]+tabj[j]+"')\" onMouseOver=\"this.style.cursor='pointer'\"></TD>";
			}
			}
			tableau_palette += "</tr>";

			cmp = cmp + 1; // On compte le nombre de ligne faite
			if (cmp == 6) // si on a fait les 6 lignes
			{
			var tabi = new Array('FF', '99', '33'); // on redéfini le nouveau tableau principal (colonne de 6 couleurs)
			var tabk = tabi2; // on re initialise le tableau des lignes
			k = -1; // on défini k à -1 car on a déja fait un passage
			}
			}
			// Fin prog
			tableau_palette += "</TABLE>"; // On ferme le tableau + saut de ligne
			document.getElementById('tab_palette').innerHTML = tableau_palette;
			}
			
			/* Fonction ajx qui récupère les langues du questionnaire sélectionné pour la génération des champs input de libellé */
			function select_lang_list(questid){
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_quest_lang.php"; // La page qui réceptionne les données
				var data     = null; 
				
				if 	(questid>0){ //Si le code postal tapé possède au moins 2 caractères
					document.getElementById("cible_ajx").innerHTML='Patientez...<img src="../images/wait.gif" alt="" />';
					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("<?php echo $t_browser_support_error_1 ?>");
						   return; 
						} 
						 
						
						 
						if(questid != ""){
							data = "questid="+questid;
						}
						if(method == "GET" && data != null) {
						   filename += "?"+data;
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split("__"); 
							  /* On sépare chaque réponse postée qui est séparée par ":" */
							  if(typeof(tmp[0]) != "undefined") { //S'il y a au moins un retour
								 document.getElementById("cible_ajx").innerHTML = '';
								 contenu = '&nbsp;';
								 if (tmp[0]!=''){
									contenu = tmp[0];
								 }
								 document.getElementById("cible_ajx").innerHTML = contenu;
							  }
							  
						   } 
						} 

						xhr_object.send(data); //On envoie les données
				}else{
					 document.getElementById("cible_ajx").innerHTML = '';
				}
			}
			-->
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#">
		  	<table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_info_graphe[0]['code_libelle'] ?>&nbsp;<?php echo $qry_info_graphe[0]['graphe_nom'] ?>&nbsp;/&nbsp;Nouvelle donnée</td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><table width="900" border="0" cellspacing="0" cellpadding="0"  align="center">
			<tr> 
			  <td height="14"></td>
		    </tr>
			<tr> 
			  <td align="left" style="text-align: left;" class="TX_Tarifs">Ajout de donnée</td>
		    </tr>
			<tr> 
			  <td align="center"  height="1" bgcolor="#666666"></td>
		    </tr>
			<tr> 
			  <td height="1" align="center" class="TX" >&nbsp;</td>
		    </tr>
			<tr> 
			  <td align="center" class="TX"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
				  <tr> 
					<td align="left" valign="top" class="TX_Gras"> 1 Questionnaire :</td>
					<td align="left" valign="top">
					  <select name="select_quest" id="select_quest" class="form_ediht_Tarifs" onChange="select_lang_list(document.getElementById('select_quest').options[document.getElementById('select_quest').selectedIndex].value);">
					  <option value="0" selected="selected">Choisir un questionnnaire</option>
						<?php
						if (is_array($qry_quest_liste)){ // On fait la liste des questionnaires
							foreach($qry_quest_liste as $quest_liste){
								echo '<option value="'.$quest_liste['quest_id'].'">'.$quest_liste['quest_nom'].'</option>';
							}
						}else{
							echo '<option value="0"><?php echo $t_aucune_quest ?></option>';
						}
						?>
					  </select>
					</td>
				  </tr>
				  <tr> 
					<td align="left" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="left" valign="top" class="TX_Gras"> 2 Type de profil : </td>
					<td align="left" valign="top" >
					  <select name="select_type_profil" class="form_ediht_Tarifs">
						<?php
						if (is_array($qry_type_profil_liste)){ // On fait la liste des questionnaires
							foreach($qry_type_profil_liste as $quest_type_profil){
								echo '<option value="'.$quest_type_profil['code_id'].'">'.$quest_type_profil['code_libelle'].'</option>';
							}
						}else{
							echo '<option value="0"><?php echo $t_aucune_profil ?></option>';
						}
						?>
					  </select>
				  </tr>
				  <tr> 
					<td align="left" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="left" class="TX_Gras"> 3 Apparence : </td>
					<td align="left"> </td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">
						<table cellpadding="0" cellspacing="0">
						<tr><td class="TX" valign="top">
								Couleur : <input type="text" name="couleur_fond" id="couleur" size="7" maxlength="7" class="form_ediht_Tarifs" value="default"><div width="1%" id="tab_palette" style="display: inline;"></div>
								<script language="JavaScript">
									  palette_couleurs();
								</script>
							</td></tr>
						</table>
					</td>
				  </tr>
				  <?php
				  if ($_GET['type']=='36') // si courbe
				  {
				  ?>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">
						<table cellpadding="0" cellspacing="0">
						<tr><td class="TX" valign="top">Style 
							  : 
							  <select name="select_style" class="form_ediht_Tarifs">
								<option value="0">Pointill&eacute;</option>
								<option value="1">Continu</option>
							  </select>
							  &nbsp;&nbsp;&nbsp;
							</td>
						</tr></table>
					</td>
				  </tr>
				 <?php
				 }
				 ?>
				  <tr> 
					<td align="left" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="left" valign="top" class="TX_Gras">4 Libell&eacute; d'affichage : </td>
					<td align="left" valign="top"></td>
				  </tr>
				  <tr> 
					<td colspan="2" align="left" valign="top"> 
					<div id="cible_ajx"></div>
					</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
			    </table>
			  </td>
		    </tr>
			<tr> 
			  <td height="14"></td>
		    </tr>
		  </table></td>
			  </tr>
			</table>
		  
		  <br>
          
		  <p style="text-align:center"><input type="submit" name="Submit" value="VALIDER" class="BN">
		  </p>
		</form>
		</body>
		</html>
		<?php
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{
	include('no_acces.php');
}
?>