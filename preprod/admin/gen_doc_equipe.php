<?php
session_start();

$var_cand_id 	= false;
$var_ope_id 	= false;
$var_doss_id 	= false;

$var_equipe 	= intval($_GET['equipe']);

if($_GET['force'] != ''){
	$var_force	 	= intval($_GET['force']);
}
if($_GET['candid'] != ''){
	$var_cand_id 	= intval($_GET['candid']);
}
if($_GET['opeid'] != ''){
	$var_ope_id 	= intval($_GET['opeid']);
}
if($_GET['dossid'] != ''){
	$var_doss_id 	= intval($_GET['dossid']);
}
if(!file_exists('./temp/'.$var_cand_id.$var_ope_id.$var_doss_id.'.pdf') || $var_force==1){
	$var_genere_tout = 1;
}else{
	$var_genere_tout = 0;
}
if($_SESSION['droit']!='')
{
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);

	include ('./gen_doc.inc.php');

	if (!file_exists($chemin . $fname . '.pdf'))
	{
	?>
	<script type="text/javascript">
	alert ("Nous rencontrons un problème sur la génération du document de ce candidat, l'équipe VAKOM vient d'être avertie et se mobilise pour intervenir dans les plus  brefs délais. Vous serez averti par mail dès que le document sera disponible. Nous vous remercions de votre compréhension.");
	</script>
	<?php
	}
	else
	{
		if($_SESSION['droit']=='9')
		{
		?>
		<meta http-equiv="refresh" content="0; URL=<?php echo $chemin . $fname . '.pdf' ?>">	
		<?php
		}
		else
		{
			$sql_pdf_info = "SELECT VALID_PDF FROM CAND_A_OPE WHERE CAND_ID=".$var_cand_id." AND OPE_ID=".$var_ope_id;
			$qry_pdf_info = $db->query($sql_pdf_info);
			if ($qry_pdf_info[0]['valid_pdf'] == 1)
			{
			?>
			<meta http-equiv="refresh" content="0; URL=<?php echo $chemin . $fname . '.pdf' ?>">	
			<?php
			}
			else
			{
			?>
			<script type="text/javascript">
			alert ("Nous rencontrons un problème sur la génération du document de ce candidat, l'équipe VAKOM vient d'être avertie et se mobilise pour intervenir dans les plus  brefs délais. Vous serez averti par mail dès que le document sera disponible. Nous vous remercions de votre compréhension.");
			</script>
			<?php
			}
		}
	}
}	
else
{
	include('no_acces.php');
}
?>
