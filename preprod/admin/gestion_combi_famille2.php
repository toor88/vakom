<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	if ($_GET['txtimgid']>0){
	
	if ($_POST['nom_tableau']!=''){
		/* Si le formulaire est envoyé */
		$sql_update_txtimg	= "UPDATE TEXTE_IMAGE SET 
		TXT_IMG_NOM='".txt_db($_POST['nom_tableau'])."', 
		TXT_IMG_QUEST_ID='".txt_db(intval($_POST['select_quest']))."', 
		TXT_IMG_TYPE_PROFIL_CODE_ID = '".txt_db(intval($_POST['select_profil']))."', 
		TXT_IMG_PRECISION_CODE_ID = '".txt_db(intval($_POST['select_precision']))."'  
		WHERE TXT_IMG_ID='".txt_db(intval($_GET['txtimgid']))."'";
		$qry_update_txtimg 	= $db->query($sql_update_txtimg);
		
		/* On redirige la personne */
		header('location:gestion_combi_famille2-1.php?txtimgid='.txt_db(intval($_GET['txtimgid'])));
	}
	
	/* On sélectionne les questionnaires présents dans la base de données */
	$sql_sel_quest 		= "SELECT * FROM QUESTIONNAIRE WHERE QUEST_DATE_SUPPRESSION IS NULL ORDER BY QUEST_NOM ASC";
	$qry_sel_quest 		= $db->query($sql_sel_quest);
	
	/* On sélectionne les types de profil présents dans la base de données */
	$sql_sel_profil 	= "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='TYPE_PROFIL' ORDER BY CODE_LIBELLE ASC";
	$qry_sel_profil 	= $db->query($sql_sel_profil);
	
	/* On sélectionne les types de précision présents dans la base de données */
	$sql_sel_precision 	= "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='PRECISION' ORDER BY CODE_LIBELLE ASC";
	$qry_sel_precision 	= $db->query($sql_sel_precision);
	
	/* On sélectionne le tableau txt/img désigné par le txtimgid en GET */
	$sql_sel_txtimg	    = "SELECT TXT_IMG_NOM, TXT_IMG_QUEST_ID, TXT_IMG_TYPE_PROFIL_CODE_ID, TXT_IMG_PRECISION_CODE_ID FROM TEXTE_IMAGE WHERE TXT_IMG_ID='".txt_db(intval($_GET['txtimgid']))."'";
	$qry_sel_txtimg 	= $db->query($sql_sel_txtimg);
	?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<link rel="stylesheet" href="../css/style.css" type="text/css">		
		<script language="JavaScript">
		<!--
		function MM_openBrWindow(theURL,winName,features) { //v2.0
		  window.open(theURL,winName,features);
		}

		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		//-->
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
				<?php
					include('menu_top_new.php');
				?>
<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>	  				
		<form method="post" action="#">

		  <br>
		<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;TEXTES ET IMAGES</td>
		</tr>
		</table>
		  <table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>   
			<tr> 
			  <td width="14"></td>
			  <td align="center" class="TX"> 
				<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
				  <tr> 
					<td align="left" class="TX"> <b>Nom du tableau de textes & images 
					  :&nbsp;&nbsp;</b></td>
					<td align="left" class="TX"> 
					  <input type="text" name="nom_tableau" class="form_ediht_Tarifs" size="50" value="<?php echo $qry_sel_txtimg[0]['txt_img_nom'] ?>" maxlength="50" >
					  <input type="submit" name="nvo_client" value="Importer les textes" class="bn_ajouter" onClick="MM_openBrWindow('gestion_combi_import.php','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=360')">
					</td>
				  </tr>
				  <tr> 
					<td align="left" class="TX">Questionnaire :&nbsp;&nbsp;</td>
					<td align="left" class="TX"> 
					  <select name="select_quest" class="form_ediht_Tarifs">
					  <?php
					  if (is_array($qry_sel_quest)){
						foreach($qry_sel_quest as $quest_list){
							unset($selected_quest);
							if ($quest_list['quest_id'] == $qry_sel_txtimg[0]['txt_img_quest_id']){
								$selected_quest = ' selected="selected"';
							}
							echo '<option value="'.$quest_list['quest_id'].'"'.$selected_quest.'>'.$quest_list['quest_nom'].'</option>';
						}
					  }
					  ?>
					  </select>
					</td>
				  </tr>
				  <tr> 
					<td align="left" class="TX">Type de profil :&nbsp;&nbsp;</td>
					<td align="left" class="TX"> 
					  <select name="select_profil" class="form_ediht_Tarifs">
					  <?php
					  if (is_array($qry_sel_profil)){
						foreach($qry_sel_profil as $profil_list){
							unset($selected_profil);
							if ($profil_list['code_id'] == $qry_sel_txtimg[0]['txt_img_type_profil_code_id']){
								$selected_profil = ' selected="selected"';
							}
							echo '<option value="'.$profil_list['code_id'].'"'.$selected_profil.'>'.$profil_list['code_libelle'].'</option>';
						}
					  }
					  ?>
					  </select>
					</td>
				  </tr>
				  <tr> 
					<td align="left" class="TX">Pr&eacute;cision :&nbsp;&nbsp;</td>
					<td align="left" class="TX"> 
					  <select name="select_precision" class="form_ediht_Tarifs">
					  <?php
					  if (is_array($qry_sel_precision)){
						foreach($qry_sel_precision as $precision_list){
							unset($selected_precision);
							if ($precision_list['code_id'] == $qry_sel_txtimg[0]['txt_img_precision_code_id']){
								$selected_precision = ' selected="selected"';
							}
							echo '<option value="'.$precision_list['code_id'].'"'.$selected_precision.'>'.$precision_list['code_libelle'].'</option>';
						}
					  }
					  ?>
					  </select>
					</td>
				  </tr>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14" class="TX"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
			
		  <br>
		  <br>
		  <tr>
			  <td width="14" height="14">&nbsp;</td>
			  <td height="14">&nbsp;</td>
			  <td width="14" height="14">&nbsp;</td>
			</tr>
		  <p align="center">
				<input type="submit" name="ok" value="Valider" class="BN"><br>
		  </p>
		</form>
		</p></div>	</article></div>	</div>	</div>	</div>					
		</body>
		</html>
		<?php
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{	
	include('no_acces.php');
}
?>
