<?php
session_start();
	// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	if ($_GET['action'] == 'delete_prix' && $_GET['prixid']>0){ /* Si on veut supprimer un prix */
		/* On supprime le prix sélectionné */
		$sql_del_prix = "DELETE FROM PRODUIT_A_PRIX WHERE PRIX_ID='".txt_db($_GET['prixid'])."'";
		$qry_del_prix = $db->query($sql_del_prix);
		
		/* On recharge la page */
		header('location:gestion_prix.php');
	}
	
	
	if(strlen($_POST['JJ']) == 2 && strlen($_POST['MM']) == 2 && strlen($_POST['AAAA']) == 4 && $_POST['JJ'] < 32 && $_POST['MM'] < 13 && $_POST['AAAA'] > 2008 ){
		/* On sélectionne les informations pour chaque tarif actif */
		$sql_liste_prix1 = "select pack.* from pack order by quantite";
		$qry_liste_prix1 = $db->query($sql_liste_prix1);
		
		/* Pour chaque tarif actif trouvé */
		foreach($qry_liste_prix1 as $prix1){
			/* On insert la nouvelle donnée */
			$sql_renouvellement = "INSERT INTO PRODUIT_A_PRIX(PRIX_ID, PROD_ID,NATURE_CODE_ID,DATE_DEBUT,QUANTITE,PRIX,PRIX_SANS_CONTRAT) VALUES(SEQ_ID.NEXTVAL, '".txt_db(intval($prix1['prod_id']))."', '".txt_db(intval($prix1['nature_code_id']))."', TO_DATE('".txt_db($_POST['JJ'])."/".txt_db($_POST['MM'])."/".txt_db($_POST['AAAA'])."', 'DD/MM/YYYY'), '".txt_db($prix1['quantite'])."', '".txt_db($prix1['prix'])."', '".txt_db($prix1['prix_sans_contrat'])."')";
			$qry_renouvellement = $db->query($sql_renouvellement);
		}
		/* On redirige la page pour vider le cache */
		header('location:gestion_prix.php?idnc='.($_GET['idnc']+1).'&actif='.$_GET['actif']);
	}
	
	switch($_GET['order']){
		case 'nature':
			$order = "ORDER BY PACK.NATURE_CODE_ID, PRODUIT.prod_nom";
			$order2 = "ORDER BY PRODUIT_A_PRIX.NATURE_CODE_ID, PRODUIT.prod_nom";
		break;
		case 'date':
			$order = "ORDER BY TO_CHAR(PACK.DATE_DEBUT, 'YYYYMMDD')";
			$order2 = "ORDER BY TO_CHAR(PRODUIT_A_PRIX.DATE_DEBUT, 'YYYYMMDD')";
		break;
		case 'prix':
			$order = "ORDER BY PACK.PRIX";
			$order2 = "ORDER BY PRODUIT_A_PRIX.PRIX";
		break;
		case 'prix2':
			$order = "ORDER BY PACK.PRIX_SANS_CONTRAT";
			$order2 = "ORDER BY PRODUIT_A_PRIX.PRIX_SANS_CONTRAT";
		break;
		case 'produit':
			$order = "ORDER BY PRODUIT.prod_nom";
			$order2 = "ORDER BY PRODUIT.prod_nom";
		break;
		case 'quantite':
		default:
			$order = "ORDER BY PACK.QUANTITE";
			$order2 = "ORDER BY PRODUIT_A_PRIX.QUANTITE";
	}
	
	if($_GET['actif']==1){
		/* On sélectionne tous les prix présents dans la base */
		$sql_liste_prix = "select distinct pack.*, produit.prod_nom from pack, PRODUIT WHERE PRODUIT.PROD_ID=PACK.PROD_ID ".$order;
	}else{
		/* On sélectionne tous les prix présents dans la base */
		$sql_liste_prix = "SELECT distinct PRODUIT.PROD_NOM, PRODUIT_A_PRIX.* FROM PRODUIT_A_PRIX, PRODUIT, PACK WHERE PRODUIT.PROD_ID=PACK.PROD_ID(+) AND PRODUIT_A_PRIX.PROD_ID = PRODUIT.PROD_ID ".$order2;
	}
	//echo $sql_liste_prix ;
	$qry_liste_prix = $db->query($sql_liste_prix);
	
	?>
	<html>
	<head>
	<title>Vakom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<link rel="stylesheet" href="../css/style.css" type="text/css">	
	<script language="JavaScript">
	<!--
	
	function pass(champ){
		if (champ == 1){
			if(document.form.JJ.value.length==2){
				document.form.MM.value='';
				document.form.MM.focus();
			}
		}
		if (champ == 2){
			if(document.form.MM.value.length==2){
				document.form.AAAA.value='';
				document.form.AAAA.focus();
			}
		}
	}
	
	function delete_prix(prixid){
		if (confirm('<?php echo $t_supprimer_prix ?>')){
			document.location.href='gestion_prix.php?action=delete_prix&prixid='+prixid;
		}
	}
	
	function show_actif(){
		if(document.form.actif.checked==true){
			document.location.href = 'gestion_prix.php?actif=1&order=<?php echo $_GET['order'] ?>';
		}else{
			document.location.href = 'gestion_prix.php?actif=0&order=<?php echo $_GET['order'] ?>';
		}
	}
	
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
			<?php
				include("menu_top_new.php");
			?>
	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>	
	<form method="post" action="#" name="form">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
	  <td width="20">&nbsp;</td>
	  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;PRIX</td>
	</tr>
	</table>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
			<tr>
			  <td width="14"></td>
			  <td class="TX_Tarifs">Récapitulatif&nbsp;des&nbsp;tarifs&nbsp;des&nbsp;jetons&nbsp;pr&eacute;pay&eacute;s</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr>
			  <td width="14"></td>
			  <td class="TX" align="center">&nbsp;</td>
			  <td width="14"></td>
			</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="left" class="TX"> 
			<input type="button" name="bn_pdt" value="Ajouter" class="bn_ajouter" onClick="MM_openBrWindow('gestion_prix_ajout.php','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=400')">
			&nbsp;&nbsp;&nbsp;&nbsp;</td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="right" class="TX"> 
			<input type="checkbox" onClick="show_actif();" name="actif" value="1" <?php if($_GET['actif']==1){ echo ' checked="checked"';} ?>>
			Affichage&nbsp;des&nbsp;tarifs&nbsp;actifs&nbsp;uniquement</td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="left" class="TX"> 
			<table border="1" cellspacing="0" cellpadding="0" bgcolor="ffffff" width="100%">
			  <tr align="center" bgcolor="#F1F1F1"> 
				<td class="TX"><a href="gestion_prix.php?actif=<?php echo $_GET['actif'] ?>&order=nature">Nature du partenaire</a></td>
				<td class="TX"><a href="gestion_prix.php?actif=<?php echo $_GET['actif'] ?>&order=produit">Produit</a></td>
				<td class="TX"><a href="gestion_prix.php?actif=<?php echo $_GET['actif'] ?>&order=date">Date de d&eacute;but</a></td>
				<td class="TX"><a href="gestion_prix.php?actif=<?php echo $_GET['actif'] ?>&order=quantite">Quantite</a></td>
				<td class="TX"><a href="gestion_prix.php?actif=<?php echo $_GET['actif'] ?>&order=prix">Prix</a></td>
				<td class="TX"><a href="gestion_prix.php?actif=<?php echo $_GET['actif'] ?>&order=prix2">Prix sans Contrat</a></td>
				<td class="TX">Modifier</td>
				<td class="TX">Supp</td>
			  </tr>
			  <?php
			  if (is_array($qry_liste_prix)){ /* S'il y a au moins un prix référencé */
				foreach($qry_liste_prix as $liste_prix){ /* Pour chaque prix */
			  ?>
				  <tr> 
					<td align="left" class="TX">
					<?php
					if ($liste_prix['nature_code_id']!='9'){
						$sql_nature_nom = "SELECT CODE_LIBELLE FROM CODE WHERE CODE_TABLE='NATURE' AND CODE_ID='".txt_db($liste_prix['nature_code_id'])."'";
						$qry_nature_nom = $db->query($sql_nature_nom);
						echo strtoupper($qry_nature_nom[0]['code_libelle']);
					}else{
						echo 'TOUS';
					}					
					?>					
					</td>
					<td align="left" class="TX"><?php echo $liste_prix['prod_nom'] ?></td>
					<td class="TX" align="center"><?php echo $liste_prix['date_debut'] ?></td>
					<td class="TX" align="center"><?php echo $liste_prix['quantite'] ?></td>
					<td class="TX" align="center"><?php echo number_format(str_replace(',', '.', $liste_prix['prix']), 2, ',', ' '); ?> &euro;</td>
					<td class="TX" align="center"><?php echo number_format(str_replace(',', '.', $liste_prix['prix_sans_contrat']), 2, ',', ' ') ?> &euro;</td>
					<td class="TX" align="center"><img onMouseOver="this.style.cursor='pointer'" onClick="MM_openBrWindow('gestion_prix_edit.php?prixid=<?php echo $liste_prix['prix_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=400')" src="modifier.png" border="0" width="20"></td>
					<td class="TX" align="center"><img onMouseOver="this.style.cursor='pointer'" onClick="delete_prix('<?php echo $liste_prix['prix_id'] ?>');" src="../images/icon_supp2.gif" width="11" height="12"></td>
				  </tr>
			  <?php
				}
			  }else{
				echo '<tr><td class="TX" align="center" colspan="8"><?php echo $t_aucune_price ?></td></tr>';
			  }
			  ?>
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td colspan="3">&nbsp;</td>
		 </tr>
		<tr> 
		  <td width="14"></td>
		  <td align="left" class="TX">
			  Renouveler les tarifs actifs &agrave; la date du : 
				<input type="text" name="JJ" value="<?php echo date('d') ?>" class="form_ediht_Tarifs" size="2" maxlength="2" onKeyUp="pass(1)">
				/ 
				<input type="text" name="MM" value="<?php echo date('m') ?>" class="form_ediht_Tarifs" size="2" maxlength="2" onKeyUp="pass(2)">
				/ 
				<input type="text" name="AAAA" value="<?php echo date('Y') ?>" class="form_ediht_Tarifs" size="4" maxlength="4">
				<input type="submit" value="OK" class="bn_ajouter">
		  </td>
		  <td width="14"></td>
		</tr>

		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	</form>
</p></div>	</article></div>	</div>	</div>	</div>	  			
	</body>
	</html>
<?php
}else{
	include('no_acces.php');
}
?>