<?php
session_start();
//print_r($_SESSION);
// Si l'utilisateur est un admin vakom ou un super admin
if(($_SESSION['droit']>5)||($_SESSION['droit']==4)){
	if ($_GET['partid'] && $_GET['partid']>0){

		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");

		$db = new db($conn);
		/* On sélectionne les images déjà présentes */
		$sql_sel_photos = "SELECT PART_LOGO1, PART_LOGO3 FROM PARTENAIRE WHERE PART_ID='".txt_db(intval($_GET['partid']))."'";
		$qry_sel_photos = $db->query($sql_sel_photos);
		
		if (is_uploaded_file($_FILES['photo']['tmp_name'])){
			$error = '';
			
			$ftmp  = $_FILES['photo']['tmp_name'];
			$fname = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['photo']['name']));	
			$fname = strtr($fname,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
			$fname = strtolower($fname);

			//extensions autorisées	
			$extension_autorises= array('.jpg','.jpeg', '.png', '.gif');	
			$extension= strrchr($fname,'.');	
			
			//lien des photos
			$lien  		= './images_societe/'.$fname;


			/*Definition de la taille max des minatures. */

			$width_n  	= 600;
			$height_n 	= 600;

			//verification de l'extension
			if (!in_array($extension,$extension_autorises)){
				$error .= '<?php echo $t_fichier_not_image  ?>';
			}
		
			// L'image est déplacée dans le dossier normal
			if (!move_uploaded_file($ftmp,$lien)){
				$error .= '<?php echo $t_copier_fichier_probleme  ?>';
			}

			/* Content type */	
			//***************************	

				/*creation des miniatures. */
//				list($width_orig, $height_orig) = getimagesize($lien);

				// On stoppe le script si la taille de la photo est trop grande
				if ($width_orig > 1500 || $height_orig > 1500){
					if (file_exists($lien)){
						unlink($lien);
					}
					exit('<?php echo $t_image1_grande  ?> <br /><a href="javascript:history.back();">retour</a><br />'.$rappel.' <hr />');
				} 
				$ratio_orig   = $width_orig/$height_orig;

				
				// Ratio normal
				if ($width_orig > $width_n || $height_orig > $height_n){
					if ($width_n/$height_n > $ratio_orig){
						$width_n  = $height_n*$ratio_orig;

					}else{
						$height_n = $width_n/$ratio_orig;

					}
				}else{
					$width_n  = $width_orig;
					$height_n = $height_orig;
				}



				// Config normale
//				$image_p_n 	  = imagecreatetruecolor($width_n,$height_n);
//				if ($extension=='.jpeg' || $extension=='.jpg'){
//					$image_n   	  = @imagecreatefromjpeg($lien);
//				}
//				if ($extension=='.png'){
//					$image_n   	  = @imagecreatefrompng($lien);	
//				}
//				if ($extension=='.gif'){
//					$image_n   	  = @imagecreatefromgif($lien);	
//				}
//				@imagecopyresampled($image_p_n,$image_n,0,0,0,0,$width_n, $height_n, $width_orig, $height_orig);


				unset($width_n);
				unset($height_n);
				unset($ratio_orig);

				
				//movance  de la miniature crée au dessus dans le repertoire normal 
//					if ($extension=='.jpeg' || $extension=='.jpg'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
//						imagejpeg($image_p_n,$lien,90);
//					}
//					if ($extension=='.png'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
//						imagepng($image_p_n,$lien,7);
//					}
//					if ($extension=='.gif'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
//						imagegif($image_p_n,$lien,90);
//					}
//				imagedestroy($image_p_n);
				
//				unset($lien);

				
			//*************************************************************

		}else{
			$fname = $qry_sel_photos[0]['part_logo1'];
		}
		
		if (is_uploaded_file($_FILES['photo3']['tmp_name'])){
			$error = '';
			
			$ftmp  = $_FILES['photo3']['tmp_name'];
			$fname3 = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['photo3']['name']));	
			$fname3 = strtr($fname3,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
			$fname3 = strtolower($fname3);

			//extensions autorisées	
			$extension_autorises= array('.jpg','.jpeg', '.png', '.gif');	
			$extension= strrchr($fname3,'.');	
			
			//lien des photos
			$lien  		= './images_societe/'.$fname3;


			/*Definition de la taille max des minatures. */

			$width_n  	= 600;
			$height_n 	= 600;

			//verification de l'extension
			if (!in_array($extension,$extension_autorises)){
				$error .= '<?php echo $t_fichier_not_image  ?>';
			}
		
			// L'image est déplacée dans le dossier normal
			if (!move_uploaded_file($ftmp,$lien)){
				$error .= '<?php echo $t_copier_fichier_probleme  ?>';
			}
					unset($width_n);
					unset($height_n);
					unset($ratio_orig);					
					unset($lien);

			/* Content type */	
			//***************************	

				/*creation des miniatures. */
//				list($width_orig, $height_orig) = getimagesize($lien);

				// On stoppe le script si la taille de la photo est trop grande
				if ($width_orig > 1500 || $height_orig > 1500){
					if (file_exists($lien)){
						unlink($lien);
					}
					exit('<?php echo $t_image1_grande  ?> <br /><a href="javascript:history.back();">retour</a><br />'.$rappel.' <hr />');
				} 
				$ratio_orig   = $width_orig/$height_orig;

				
				// Ratio normal
				if ($width_orig > $width_n || $height_orig > $height_n){
					if ($width_n/$height_n > $ratio_orig){
						$width_n  = $height_n*$ratio_orig;

					}else{
						$height_n = $width_n/$ratio_orig;

					}
				}else{
					$width_n  = $width_orig;
					$height_n = $height_orig;
				}



				// Config normale
/*
				$image_p_n 	  = imagecreatetruecolor($width_n,$height_n);
				if ($extension=='.jpeg' || $extension=='.jpg'){
					$image_n   	  = @imagecreatefromjpeg($lien);
				}
				if ($extension=='.png'){
					$image_n   	  = @imagecreatefrompng($lien);	
				}
				if ($extension=='.gif'){
					$image_n   	  = @imagecreatefromgif($lien);	
				}
				@imagecopyresampled($image_p_n,$image_n,0,0,0,0,$width_n, $height_n, $width_orig, $height_orig);

*/
				unset($width_n);
				unset($height_n);
				unset($ratio_orig);

				
				//movance  de la miniature crée au dessus dans le repertoire normal 
/*
					if ($extension=='.jpeg' || $extension=='.jpg'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagejpeg($image_p_n,$lien,90);
					}
					if ($extension=='.png'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagepng($image_p_n,$lien,7);
					}
					if ($extension=='.gif'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagegif($image_p_n,$lien,90);
					}
				imagedestroy($image_p_n);
				unset($lien);
*/				

				
			//*************************************************************

		}else{
			$fname3 = $qry_sel_photos[0]['part_logo3'];
		}
		// Mise à jour des informations du partenaire
		if ($_POST['part_id']!=''){
		
			if ($_POST['select_pays']==5){
				$cp1 = $_POST['cp'];
			}else{
				$cp1 = $_POST['cp_etr'];
			}
			if (!empty($_POST['nom_new_ville']))
			{
				$input_hid_ville = $_POST['nom_new_ville'];
			} else {
				$input_hid_ville = $_POST['input_hid_ville'];
			}
			$sql_update_part = "UPDATE PARTENAIRE 
			SET PART_NOM='".txt_db($_POST['nom'])."',
			PART_RS='".txt_db($_POST['rs'])."',
			PART_CODE_CLIENT='".txt_db($_POST['cc'])."',
			PART_NATURE_CODE_ID='".txt_db($_POST['nature'])."',
			PART_AD1='".txt_db($_POST['adr1'])."',
			PART_AD2='".txt_db($_POST['adr2'])."',
			PART_CP='".txt_db($cp1)."',
			PART_VILLE='".txt_db(strtoupper($input_hid_ville))."',
			PART_PAYS_CODE_ID='".txt_db($_POST['select_pays'])."',
			PART_TEL='".txt_db($_POST['tel'])."',
			PART_FAX='".txt_db($_POST['fax'])."',
			PART_TVA='".txt_db($_POST['siren'])."',
			PART_SIRET='".txt_db($_POST['siret'])."',
			PART_CONTACT='".txt_db($_POST['ctct'])."',
			PART_SITE_WEB='".txt_db($_POST['web'])."',
			PART_INFO1='".txt_db($_POST['comm1'])."',
			PART_INFO2='".txt_db($_POST['comm2'])."',
			PART_BLOQUE='".txt_db($_POST['bloque'])."',
			PART_LANG_ID='".txt_db($_POST['langue'])."',
			PART_LOGO1='".txt_db($fname)."',
			PART_LOGO3='".txt_db($fname3)."',
			PART_COULEUR_FOND='".txt_db($_POST['couleur_fond'])."',
			PART_POLICE_TEXTE_CODE_ID='".txt_db($_POST['police_texte'])."',
			PART_DATE_MODIFICATION=SYSDATE,
			PART_USER_MODIFICATION_ID='2'
			WHERE PART_ID='".txt_db($_POST['part_id'])."'";
			//echo $sql_update_part;
			$qry_update_part = $db->query($sql_update_part);
			
		?>
		<script type="text/javascript">
				window.opener.location.reload(true);
				//document.location.href='admvak_edit_client.php?partid=<?php echo txt_db($_POST['part_id']) ?>&idnc=012';
				window.close();
			</script>		
			<?php
			//header('location:admvak_edit_client.php?partid='.txt_db($_POST['part_id']).'&idnc=012');
		}

		// Chargement de la liste des natures de pack
		$sql_nature_list 	= "SELECT * FROM CODE WHERE CODE_TABLE='NATURE' ORDER BY CODE_ORDRE";
		$qry_nature_list 	= $db->query($sql_nature_list);

		// Chargement de la liste des pats
		$sql_pays_list 		= "SELECT * FROM CODE WHERE CODE_TABLE='PAYS' ORDER BY CODE_ORDRE";
		$qry_pays_list 		= $db->query($sql_pays_list);

		// Chargement de la liste des langues
		$sql_langue_list 	= "SELECT * FROM LANGUE order by lang_id";
		$qry_langue_list 	= $db->query($sql_langue_list);

		// Chargement de la liste des polices
		$sql_police_list 	= "SELECT * FROM CODE WHERE CODE_TABLE='POLICE_TEXTE' ORDER BY CODE_ORDRE";
		$qry_police_list 	= $db->query($sql_police_list);

		// Chargement des données du partenaire
		$sql_partenaire 	= "SELECT PARTENAIRE.*, TO_CHAR(PARTENAIRE.PART_DATE_CREATION, 'DD/MM/YYYY HH24:MI') PART_DATE_CREATION, TO_CHAR(PARTENAIRE.PART_DATE_MODIFICATION, 'DD/MM/YYYY HH24:MI') PART_DATE_MODIFICATION FROM PARTENAIRE WHERE PART_ID='".txt_db($_GET['partid'])."'";
		$qry_partenaire 	= $db->query($sql_partenaire);
		
		// Chargement de la liste des types de contrat
		$sql_type_contrat_list 		= "SELECT * FROM CODE WHERE CODE_TABLE='TYPE_CONTRAT' ORDER BY CODE_ORDRE";
		$qry_type_contrat_list 		= $db->query($sql_type_contrat_list);
		
		// Chargement des contrats du partenaire
		$sql_contrat 		= "SELECT CODE.CODE_LIBELLE, PART_A_CONTRAT.*, TO_CHAR(PART_A_CONTRAT.CONT_DATE_DEB, 'DD/MM/YYYY') CONT_DATE_DEB, TO_CHAR(PART_A_CONTRAT.CONT_DATE_FIN, 'DD/MM/YYYY') CONT_DATE_FIN FROM PART_A_CONTRAT, CODE WHERE CODE.CODE_ID=PART_A_CONTRAT.CONT_TYPE_CONTRAT_CODE_ID AND PART_A_CONTRAT.CONT_PART_ID='".txt_db($_GET['partid'])."'";
		$qry_contrat 		= $db->query($sql_contrat);
		
		if (is_array($qry_partenaire)){
			?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--
			function hid_ville(id_pays){
				if (id_pays != "5"){
					document.getElementById('cp1').style.display='none';
					document.getElementById('cp2').style.display='inline';
					document.getElementById('ville2').style.display='none';
					document.getElementById('ville3').style.display='inline';
				}else{
					document.getElementById('cp1').style.display='inline';
					document.getElementById('cp2').style.display='none';
					document.getElementById('ville2').style.display='inline';
					document.getElementById('ville3').style.display='none';
				}
			}
			
			function charge_code(code){
				var tmp_code = code.split("_");
				document.getElementById('cp').value = tmp_code[0];
				document.getElementById('testtest').value = tmp_code[1];
			}

			function TestVille(){
			if(document.form.select_pays.options[document.form.select_pays.selectedIndex].value==5){
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_ville.php"; // La page qui réceptionne les données
				var cp = document.getElementById('cp').value; 
				var data     = null; 
				
				if 	(cp.length>1){ //Si le code postal tapé possède au moins 2 caractères
					document.getElementById("ville2").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" />';
					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("<?php echo $t_browser_support_error_1 ?>");
						   return; 
						} 
						 
						
						 
						if(cp != ""){
							data = "cp="+cp;
						}
						if(method == "GET" && data != null) {
						   filename += "?"+data;
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split(":"); 
							  if(typeof(tmp[0]) != "undefined") { 
								 document.getElementById("ville2").innerHTML = '';
								 if (tmp[0]!=''){
									 document.getElementById('saisie_ville_m').style.display='inline';
									 document.getElementById('saisie_ville_m_lab').style.display='inline';
									 document.getElementById('saisie_ville_m').checked = false;								 
									document.getElementById("ville2").innerHTML = tmp[0];
								 }else{
									 document.getElementById('saisie_ville_m').style.display='none';
									 document.getElementById('saisie_ville_m_lab').style.display='none';								 
									document.getElementById("ville2").innerHTML = '<input type="hidden" name="new_ville" value="1"><input type="text" size="46" name="input_hid_ville" maxlength="255" class="form_ediht_Partenaires" onblur="document.getElementById(\'testtest\').value=this.value" style="text-transform:uppercase"/>';
								 }
							  }
						   } 
						} 

						xhr_object.send(data); //On envoie les données
				}
				}
			}
			
			function doSaisieLibre(){
				var saisie_ville_m = document.getElementById('saisie_ville_m');
				if (saisie_ville_m.checked){
					document.getElementById('saisie_ville_m').style.display='none';
					document.getElementById('saisie_ville_m_lab').style.display='none';
					document.getElementById("ville2").innerHTML = '<input type="hidden" name="new_ville" value="1"><input type="text" size="46" name="nom_new_ville" maxlength="255" class="form_ediht_Certifies" style="text-transform:uppercase"/>';
				}
			}
			
			function supp_logo(objet){
					var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
					var filename = "ajax_supp_logo.php"; // La page qui réceptionne les données
					var data     = null; 

					var xhr_object = null; 
						 
					if(window.XMLHttpRequest) // Firefox 
					   xhr_object = new XMLHttpRequest(); 
					else if(window.ActiveXObject) // Internet Explorer 
					   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
					else { // XMLHttpRequest non supporté par le navigateur 
					   alert("<?php echo $t_browser_support_error_1 ?>");
					   return;
					}		 
					
					/* Déclaration des parametres envoyés */
					data = "partid=<?php echo $_GET['partid']?>&objet="+objet;
					
					if(method == "GET" && data != null) {
						filename 	= filename+"?"+data
						data      	= null;
					}					
					 
					xhr_object.open(method, filename, true);
					xhr_object.onreadystatechange = function() {
					   if(xhr_object.readyState == 4) {
						  var tmp = xhr_object.responseText.split(":"); 
						  if(typeof(tmp[0]) != "undefined") {
							if (objet=='part_logo1'){
							 document.getElementById("div_img").innerHTML = '';
							 document.getElementById("div_img").innerHTML = '<img src="../images/logo.jpg" alt="" width="46">';
							 }
							if (objet=='part_logo3'){
							 document.getElementById("div_img3").innerHTML = '';
							 //document.getElementById("div_img3").innerHTML = '<img src="../images/partenariat.jpg" alt="" width="200">';
							}
						  }
					   }
					}
					xhr_object.send(data); //On envoie les données
			}
			
			function pass(champ){
				if (champ==1){
					if (document.form.JJ.value.length==2){
						document.form.MM.value='';
						document.form.MM.focus();
					}
				}
				if (champ==2){
					if (document.form.MM.value.length==2){
						document.form.AAAA.value='';
						document.form.AAAA.focus();
					}
				}
				if (champ==3){
					if (document.form.JJ2.value.length==2){
						document.form.MM2.value='';
						document.form.MM2.focus();
					}
				}
				if (champ==4){
					if (document.form.MM2.value.length==2){
						document.form.AAAA2.value='';
						document.form.AAAA2.focus();
					}
				}
			}
			
			blocolor = true;
			function hexa(couleur){
			if(blocolor)
			document.getElementById('couleur').value = '#'+couleur; // On inscrit dans le champs TEXT la valeur HEX de la couleur
			}

			function palette_couleurs(){

			// Ouverture du tableau
			var tableau_palette = "<TABLE border='1' cellpadding='0' cellspacing='1' ><TR>";

			// Déclaration des variables
			var tabk = new Array('FF','CC','99','66','33','00'); // Tableau principal de couleur
			var tabj = tabk;
			var tabi = new Array('CC', '66', '00'); // Petit tableau principal (colonne de 6 couleurs)
			var tabi2 = new Array('00','33','66','99','CC','FF'); // Tableau principal inversé pour la colone du milieu (colonne de 6 couleurs)
			var color=""; // initialisation de color a vide
			var cmp = 0; // initialisation du compteur a 0

			// Début prog
			for(var k=0;k<6;k++) // Boucle pour les lignes de couleurs
			{
			for(var i=0;i<3;i++) // Boucle pour les colonnes (colonnes de 6 couleurs)
			{
			if (i == 1) // Si on attaque la 2 eme colonne de 6 couleurs
			{
			tabj = tabi2; // on inverse le tableau principale de couleurs
			}
			else // sinon
			{
			tabj = Array('FF','CC','99','66','33','00'); // On remet le tableau par default
			}

			for(var j=0;j<6;j++) // Boucle pour l'affichage couleur par couleur
			{
			color="#"+tabi[i]+tabk[k]+tabj[j]; // concaténation des chaines pour la valeur de la couleur
			// et on affiche la couleur
			tableau_palette += "<TD width='15' height='15' style='border: 1px solid #000000;' bgcolor='"+color+"' onClick=\"hexa('"+tabi[i]+tabk[k]+tabj[j]+"')\" onMouseOver=\"this.style.cursor='pointer'\"></TD>";
			}
			}
			tableau_palette += "</tr>";

			cmp = cmp + 1; // On compte le nombre de ligne faite
			if (cmp == 6) // si on a fait les 6 lignes
			{
			var tabi = new Array('FF', '99', '33'); // on redéfini le nouveau tableau principal (colonne de 6 couleurs)
			var tabk = tabi2; // on re initialise le tableau des lignes
			k = -1; // on défini k à -1 car on a déja fait un passage
			}
			}
			// Fin prog
			tableau_palette += "</TABLE><BR>"; // On ferme le tableau + saut de ligne
			document.getElementById('tab_palette').innerHTML = tableau_palette;
			}

			function verif1(){
					error = '';
					error1 = '';
					error2 = '';
				// verif du format date du début
				if (document.form.JJ.value<1 || document.form.JJ.value>31 || document.form.JJ.value.length<2){
					error1 = true;
				}
				if (document.form.MM.value<1 || document.form.MM.value>12 || document.form.MM.value.length<1){
					error1 = true;
				}
				if (document.form.AAAA.value.length<4){
					error1 = true;
				}
				if (error1==true){
					error +="<?php echo $t_mauvaise_date_deb ?>\n";
				}
				
				// Verif du format date de fin
				if (document.form.JJ2.value<1 || document.form.JJ2.value>31 || document.form.JJ2.value.length<2){
					error2 = true;
				}
				if (document.form.MM2.value<1 || document.form.MM2.value>12 || document.form.MM2.value.length<1){
					error2 = true;
				}
				if (document.form.AAAA2.value.length<4){
					error2 = true;
				}
				if (error2==true){
					error +="<?php echo $t_mauvaise_date_fin ?>\n";
				}
				
				if (error!=''){
					alert(error);
				}else{
					/* Ici on place la fonction ajax */
					var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
					var filename = "ajax_ins_contrat.php"; // La page qui réceptionne les données
					var data     = null; 

					var xhr_object = null; 
						 
					if(window.XMLHttpRequest) // Firefox 
					   xhr_object = new XMLHttpRequest(); 
					else if(window.ActiveXObject) // Internet Explorer 
					   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
					else { // XMLHttpRequest non supporté par le navigateur 
					   alert("<?php echo $t_browser_support_error_1 ?>");
					   return;
					}		 
					
					/* Déclaration des variables */
					var contrat = document.form.select_ctt.options[document.form.select_ctt.selectedIndex].value;
					if (document.form.exclu.checked == true){
						var exclu	= '1';
					}else{
						var exclu	= '0';
					}
					var montant	= document.form.montant.value;
					if (document.form.redevence.checked == true){
						var redev	= '1';
					}else{
						var redev	= '0';
					}
					var JJ		= document.form.JJ.value;
					var MM		= document.form.MM.value;
					var AAAA	= document.form.AAAA.value;
					var JJ2		= document.form.JJ2.value;
					var MM2		= document.form.MM2.value;
					var AAAA2	= document.form.AAAA2.value;
					var info3	= document.form.comm3.value;
					var info4	= document.form.comm4.value;
					
					/* On vide le formulaire qui vient d'être saisi */
					document.form.exclu.checked=false;
					document.form.redevence.checked=false;
					document.form.montant.value='';
					document.form.JJ.value='';
					document.form.MM.value='';
					document.form.AAAA.value='';
					document.form.JJ2.value='';
					document.form.MM2.value='';
					document.form.AAAA2.value='';
					document.form.comm3.value='';
					document.form.comm4.value='';
					
					/* Déclaration des parametres envoyés */
					data = "partid=<?php echo $_GET['partid']?>";
					data+= "&contrat="+contrat;
					data+= "&exclu="+exclu;
					data+= "&montant="+montant;
					data+= "&redev="+redev;
					data+= "&JJ="+JJ;
					data+= "&MM="+MM;
					data+= "&AAAA="+AAAA;
					data+= "&JJ2="+JJ2;
					data+= "&MM2="+MM2;
					data+= "&AAAA2="+AAAA2;
					data+= "&info3="+info3;
					data+= "&info4="+info4;

					
					if(method == "GET" && data != null) {
						filename 	= filename+"?"+data
						data      	= null;
					}
					
					 
					xhr_object.open(method, filename, true);
					xhr_object.onreadystatechange = function() {
					   if(xhr_object.readyState == 4) {
						  var tmp = xhr_object.responseText.split(":_:_:_:"); 
						  if(typeof(tmp[0]) != "undefined") {
							 document.getElementById("contrat_liste").innerHTML = '';
							 if (tmp[0]!=''){
								document.getElementById("contrat_liste").innerHTML = tmp[0];
							 }else{
								document.getElementById("contrat_liste").innerHTML = "<?php echo $t_aucun_contrat_part2 ?>";
							 }
						  }
					   }
					}
					xhr_object.send(data); //On envoie les données
				}
			}
			
			function del_contrat(cont_id, mod){
				var confirmMsg;
				if(mod == 0)
					confirmMsg = "<?php echo $t_supp_contrat_part ?>";
				else
					confirmMsg = "<?php echo $t_mod_contrat_part ?>";
				if (cont_id>0){
					if (confirm(confirmMsg)){
						/* Ici on place la fonction ajax */
						var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
						var filename = "ajax_ins_contrat.php"; // La page qui réceptionne les données
						var data     = null; 
						
						var xhr_object = null; 
							 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("<?php echo $t_browser_support_error_1 ?>");
						   return;
						}		 
						
						data = "action=del&partid=<?php echo $_GET['partid'] ?>&contrat="+cont_id;
						/* SUPPRESSION DES CONTRATS */
						if(method == "GET" && data != null) {
							filename 	= filename+"?"+data
							data      	= null;
						}
						
						 
						xhr_object.open(method, filename, true);
						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split(":_:_:_:"); 
							  if(typeof(tmp[0]) != "undefined") {
								 document.getElementById("contrat_liste").innerHTML = '';
								 if (tmp[0]!=''){
									document.getElementById("contrat_liste").innerHTML = tmp[0];
								 }else{
									document.getElementById("contrat_liste").innerHTML = "<?php echo $t_aucun_contrat_part2 ?>";
								 }
							  }
						   }
						}
						xhr_object.send(data); //On envoie les données
					}
				}
			}
			
			
			function verif(){			
					error = '';
				if (document.form.nom.value == ''){
					error += "<?php echo $t_nom_oblig ?>\n";
				}
				if (document.form.cc.value == ''){
					error += "<?php echo $t_code_clt_oblig ?>\n";
				}
				if (document.form.adr1.value == ''){
					error += "<?php echo $t_adresse_oblig ?>\n";
				}
				if (document.form.cp.value == ''){
					error += "<?php echo $t_cp_oblig ?>\n";
				}
				/*
				if (document.form.input_hid_ville.value == ''){
					error += "<?php echo $t_ville_oblig ?>\n";
				}
				*/
				if (document.form.tel.value == ''){
					error += "<?php echo $t_tel_oblig ?>\n";
				}
				if (document.form.comm1.value == ''){
					error += "<?php echo $t_adr_retour_oblig ?>\n";
				}
				if (document.form.couleur_fond.value == ''){
					error += "<?php echo $t_couleur_oblig ?>\n";
				}
				if (error!=''){
					alert(error);
				}else{
					document.form.submit();
				}
			}

			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}

			function MM_openBrWindow(theURL,winName,features) { //v2.0
			  window.open(theURL,winName,features);
			}
			
			function CONTROL_MM_openBrWindow(theURL,winName,features) { //v2.0
			  if (confirm("<?php echo $t_risque_de_perdre ?>\n <?php echo $t_validez_bien_contrat ?>")){
				window.open(theURL,winName,features);
			  }
			}
			//-->
			</script>
			</head>
			<body bgcolor="#FFFFFF" text="#000000">
			<form method="post" name="form" action="#" enctype="multipart/form-data">
			  <table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Partenaires"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo htmlentities($qry_partenaire[0]['part_nom']) ?></td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td class="Titre_Partenaires">&nbsp;</td>
			    </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td><table border="0" cellspacing="0" cellpadding="0" width="100%">
					  <tr> 
						<td class="fond_tablo_Partenaires" height="40" align="left"><?php echo $t_fiche_part ?></td>
						<td class="fond_tablo_Partenaires2" valign="middle" align="left"><?php echo $t_date_creation ?> 
						  : le <?php echo $qry_partenaire[0]['part_date_creation'] ?><br>
						  <?php
						  if ($qry_partenaire[0]['part_date_modification']!=''){
							  ?>
							  <?php echo $t_date_modif ?> : le <?php echo $qry_partenaire[0]['part_date_modification'] ?>
							  <?php
						  }
						  ?>
					    </td>
					  </tr>
					  <tr> 
						<td class="TX">&nbsp;</td>
						<td class="champsoblig" valign="middle" align="right"><?php echo $t_champs_oblig ?> 
						  * </td>
					  </tr>
					  <tr> 
						<td class="TX_Partenaires"><?php echo $t_fiche_info_part ?></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr bgcolor="#000000"> 
						<td height="1"></td>
						<td height="1"></td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_fiche_nom_part ?>* :</td>
						<td class="TX"> 
						  <input type="text" name="nom" size="70" maxlength="70" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_nom'] ?>">
						<?php 
						if ($qry_partenaire[0]['part_bloque']=='1'){
							$checked=' checked="checked" ';
							$color = '#FF0000';
							$font_weight = 'bold';
						}else{
							$checked=' ';
							$color = '#000';
							$font_weight = 'normal';
						}
						echo '&nbsp;<span style="color: '.$color.'; font-weight: '.$font_weight.';">Partenaire&nbsp;bloqué</span>&nbsp;';
						echo '<input'.$checked.'type="checkbox" name="bloque" value="1">';
						?>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_fiche_design_part ?> :</td>
						<td class="TX"> 
						  <input type="text" name="rs" size="70" maxlength="70" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_rs'] ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_fiche_code_clt ?>* :</td>
						<td class="TX"> 
						  <input type="text" name="cc" size="9" maxlength="10" class="form_ediht_Partenaires" maxlength="8" value="<?php echo $qry_partenaire[0]['part_code_client'] ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_nat ?>* :</td>
						<td class="TX"> 
						  <select name="nature" class="form_ediht_Partenaires">
						  <?php
						  if (is_array($qry_nature_list)){
							foreach($qry_nature_list as $nature){
								if ($nature['code_id'] == $qry_partenaire[0]['part_nature_code_id']){
									$selected = ' selected="selected" ';
								}else{
									$selected = ' ';
								}
								echo '<option'.$selected.'value="'.$nature['code_id'].'">'.$nature['code_libelle'].'</option>';
							}
						  }
						  ?>
						  </select>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_adresse ?>* :</td>
						<td> 
						  <input type="text" name="adr1" size="70" maxlength="70" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_ad1'] ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX">&nbsp;</td>
						<td class="TX"> 
						  <input type="text" name="adr2" size="70" maxlength="70" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_ad2'] ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_pays ?>* :</td>
						<td> 
						<select name="select_pays" id="pays" class="form_ediht_Partenaires" onChange="hid_ville(document.getElementById('pays').options[document.getElementById('pays').selectedIndex].value)">
						<?php
						  if (is_array($qry_pays_list)){
							foreach($qry_pays_list as $pays){
								if ($pays['code_id'] == $qry_partenaire[0]['part_pays_code_id']){
									$selected = ' selected="selected" ';
								}else{
									$selected = ' ';
								}
								echo '<option'.$selected.'value="'.$pays['code_id'].'">'.$pays['code_libelle'].'</option>';
							}
						  }
						?>
						  </select>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_cp ?>* :</td>
				<td class="TX"> 
				  <div id="cp1" width="1%" <?php if($qry_partenaire[0]['part_pays_code_id']==5){ echo 'style="display: inline;"'; }else{ echo 'style="display: none;"'; }?>>
				  <input type="text" name="cp" id="cp" value="<?php echo $qry_partenaire[0]['part_cp'] ?>" size="5" maxlength="5" class="form_ediht_Partenaires" onkeyUp="TestVille()" onBlur="charge_code(document.getElementById('ville').options[document.getElementById('ville').selectedIndex].value)">
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  </div>
				  <div id="cp2" width="1%" <?php if($qry_partenaire[0]['part_pays_code_id']==5){ echo 'style="display: none;"'; }else{ echo 'style="display: inline;"'; }?>>
				  <input type="text" name="cp_etr" id="cp_etr" value="<?php echo $qry_partenaire[0]['part_cp'] ?>" size="5" maxlength="5" class="form_ediht_Partenaires">
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  </div><?php echo $t_ville ?>*&nbsp;:&nbsp;
	
				<div id="ville2" width="1%" <?php if($qry_partenaire[0]['part_pays_code_id']==5){ echo 'style=" display: inline;"'; }else{ echo 'style="display: none;"'; }?>><?php echo $qry_partenaire[0]['part_ville'] ?></div>
				<div style="float: right;margin-right: 35px;">
					<input type="checkbox" name="saisie_ville_m" id="saisie_ville_m"  onclick="doSaisieLibre()" style="display: none;float: left;    margin-left: 25px;margin-top: 5px;">
					<span id="saisie_ville_m_lab" style="display: none;line-height: 20px;" >&nbsp;&nbsp;Saisie libre</span>
				</div>				
				<div id="ville3" <?php if($qry_partenaire[0]['part_pays_code_id']==5){ echo 'style="display: none;"'; }else{ echo 'style="display: inline;"'; }?>>
					<input type="text" name="input_hid_ville" maxlength="255" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_ville'] ?>" onBlur="document.getElementById('testtest').value=this.value">
				</div>
				<input type="hidden" id="testtest" name="input_hid_ville" value="<?php echo $qry_partenaire[0]['part_ville'] ?>">
				</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_tel ?>* :</td>
						<td class="TX"> 
						  <input type="text" name="tel" size="25" maxlength="25" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_tel'] ?>">
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $t_fax ?> 
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
						  <input type="text" name="fax" size="23" maxlength="25" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_fax'] ?>">
						</td>
					  </tr>          <tr> 
						<td class="TX" height="40"><?php echo $t_fiche_tva_int ?> :</td>
						<td class="TX"> 
						  <input type="text" name="siren" size="25" maxlength="25" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_tva'] ?>">
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $t_fiche_siret ?> : 
						  <input type="text" name="siret" size="23" maxlength="25" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_siret'] ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_contact ?> : </td>
						<td class="TX"> 
						  <input type="text" name="ctct" size="70" maxlength="70" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_contact'] ?>">
						</td>
					  </tr>
					  
					  <tr> 
						<td class="TX" height="40"><?php echo $t_website ?> :</td>
						<td> 
						  <input type="text" name="web" size="70" maxlength="128" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_site_web'] ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40" valign="middle"><?php echo $t_redirect_web ?>* :</td>
						<td> 
						  <input type="text" name="comm1" size="70" maxlength="128" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_info1'] ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40" valign="middle"><?php echo $t_info ?> :</td>
						<td> 
						  <input type="text" name="comm2" size="70" maxlength="70" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_info2'] ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX_partenaires">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  
					  <tr> 
						<td class="TX_partenaires" colspan="2"><?php echo $t_fiche_info_contrat ?></td>
					  </tr>
					  <tr bgcolor="#000000"> 
						<td height="1"></td>
						<td height="1"></td>
					  </tr>
					  <tr> 
						<td class="TX_partenaires">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
<!--------------Champs contrat modif---------------------->
					<?php if($_GET['modif'] == 1){ ?>
					  <tr> 
						<td class="TX" colspan="2" align="left">
					<table border="0" cellspacing="0" cellpadding="0"  align="left">
							<tr align="left"> 
							  <td class="TX" align="left">
							  <?php echo $t_contrat ?>&nbsp;:&nbsp;
							  </td>
							  <td class="TX" align="left"> 
								<select name="select_ctt" class="form_ediht_Partenaires">
								  <?php
								  if (is_array($qry_type_contrat_list)){
									foreach($qry_type_contrat_list as $contrat){
										echo '<option value="'.$contrat['code_id'].'">'.$contrat['code_libelle'].'</option>';
									}
								  }
								  ?>
								</select>
							  </td>
							  <td width="20">&nbsp;</td>
							  <td class="TX"> 
								<?php echo $t_exclu ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"><input type="checkbox" name="exclu" value="1" <?php if($_GET['exclu']) echo 'checked="checked"'?>>
							  </td>
					  </tr>
							 <tr>
							  <td class="TX"><?php echo $t_debut ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"> 
								<input type="text" name="JJ" size="2" class="form_ediht_Partenaires" maxlength="2" onKeyUp="pass(1);" <?php if($_GET['dateBegins']) echo 'value="'.substr($_GET['dateBegins'], 0, 2).'"'?>>
								/ 
								<input type="text" name="MM" size="2" class="form_ediht_Partenaires" maxlength="2" onKeyUp="pass(2);" <?php if($_GET['dateBegins']) echo 'value="'.substr($_GET['dateBegins'], 3, 2).'"'?>>
								/ 
								<input type="text" name="AAAA" size="4" class="form_ediht_Partenaires" maxlength="4"  <?php if($_GET['dateBegins']) echo 'value="'.substr($_GET['dateBegins'], 6, 4).'"'?>>
							  </td>
							  <td>&nbsp;</td><td class="TX"> 
								<?php echo $t_montant ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"><input type="text" name="montant" size="4" class="form_ediht_Partenaires" <?php if($_GET['montant']) echo 'value="'.$_GET['montant'].'"'?>>
								&euro;&nbsp;<?php echo $t_HT ?>&nbsp;</td>
					  </tr>
							  <tr>
							    <td class="TX"><?php echo $t_fin ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"> 
								<input type="text" name="JJ2" size="2" class="form_ediht_Partenaires" maxlength="2" onKeyUp="pass(3);" <?php if($_GET['dateEnds']) echo 'value="'.substr($_GET['dateEnds'], 0, 2).'"'?>>
								/ 
								<input type="text" name="MM2" size="2" class="form_ediht_Partenaires" maxlength="2" onKeyUp="pass(4);" <?php if($_GET['dateEnds']) echo 'value="'.substr($_GET['dateEnds'], 3, 2).'"'?>>
								/ 
								<input type="text" name="AAAA2" size="4" class="form_ediht_Partenaires" maxlength="4" <?php if($_GET['dateEnds']) echo 'value="'.substr($_GET['dateEnds'], 6, 4).'"'?>>
							  </td>
							   <td>&nbsp;</td>
							  <td class="TX"><?php echo $t_redevance ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left">
								<input type="checkbox" name="redevence" value="1" <?php if($_GET['redevance']) echo 'checked="checked"'?>>
							  </td>
							</tr>
							<tr align="left"> 
							  <td class="TX"><?php echo $t_info3 ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"> 
								<input type="text" name="comm3" size="25" maxlength="100" class="form_ediht_Partenaires" <?php if($_GET['info1']) echo 'value="'.$_GET['info1'].'"'?>></td>
							 <td>&nbsp;</td><td class="TX" align="left"> 
								<?php echo $t_info4 ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"> 
								<input type="text" name="comm4" size="25" maxlength="100" class="form_ediht_Partenaires" <?php if($_GET['info2']) echo 'value="'.$_GET['info2'].'"'?>>
							  </td>
							</tr>
					</table>
					
						</td>
					  </tr>
					  <?php if($_SESSION['droit'] != 4){ 
					  echo '<tr> 
						<td class="TX" height="20" colspan="2"> 
						  <input type="button" name="new_IC" value="'.$t_btn_ajouter_contrat.'" class="bn_ajouter" onClick="verif1();">
						</td>
					  </tr>';
					  } ?>
<!------------------------------------------------------->
<!--------------Champs contrat nouveau ------------------>
					<?php } else{ ?>
					  <tr> 
						<td class="TX" colspan="2" align="left">
					<table border="0" cellspacing="0" cellpadding="0" align="left">
							<tr align="left"> 
							  <td class="TX" align="left">
							  <?php echo $t_contrat ?>&nbsp;:&nbsp;
							  </td>
							  <td class="TX" align="left"> 
								<select name="select_ctt" class="form_ediht_Partenaires">
								  <?php
								  if (is_array($qry_type_contrat_list)){
									foreach($qry_type_contrat_list as $contrat){
										echo '<option value="'.$contrat['code_id'].'">'.$contrat['code_libelle'].'</option>';
									}
								  }
								  ?>
								</select>
							  </td>
							  <td width="20">&nbsp;</td>
							  <td class="TX"> 
								<?php echo $t_exclu ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"><input type="checkbox" name="exclu" value="1">
							  </td>
					  </tr>
							 <tr>
							  <td class="TX"><?php echo $t_debut ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"> 
								<input type="text" name="JJ" size="2" class="form_ediht_Partenaires" maxlength="2" onKeyUp="pass(1);">
								/ 
								<input type="text" name="MM" size="2" class="form_ediht_Partenaires" maxlength="2" onKeyUp="pass(2);">
								/ 
								<input type="text" name="AAAA" size="4" class="form_ediht_Partenaires" maxlength="4">
							  </td>
							  <td>&nbsp;</td><td class="TX"> 
								<?php echo $t_montant ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"><input type="text" name="montant" size="4" class="form_ediht_Partenaires">
								&euro;&nbsp;<?php echo $t_HT ?>&nbsp;</td>
					  </tr>
							  <tr>
							    <td class="TX"><?php echo $t_fin ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"> 
								<input type="text" name="JJ2" size="2" class="form_ediht_Partenaires" maxlength="2" onKeyUp="pass(3);">
								/ 
								<input type="text" name="MM2" size="2" class="form_ediht_Partenaires" maxlength="2" onKeyUp="pass(4);">
								/ 
								<input type="text" name="AAAA2" size="4" class="form_ediht_Partenaires" maxlength="4">
							  </td>
							   <td>&nbsp;</td>
							  <td class="TX"><?php echo $t_redevance ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left">
								<input type="checkbox" name="redevence" value="1">
							  </td>
							</tr>
							<tr align="left"> 
							  <td class="TX"><?php echo $t_info3 ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"> 
								<input type="text" name="comm3" size="25" maxlength="100" class="form_ediht_Partenaires"></td>
							 <td>&nbsp;</td><td class="TX" align="left"> 
								<?php echo $t_info4 ?>&nbsp;:&nbsp;</td>
							  <td class="TX" align="left"> 
								<input type="text" name="comm4" size="25" maxlength="100" class="form_ediht_Partenaires">
							  </td>
							</tr>
					</table>
					
						</td>
					  </tr>
					  <?php if($_SESSION['droit'] != 4){ 
					  echo '<tr> 
						<td class="TX" height="20" colspan="2"> 
						  <input type="button" name="new_IC" value="'.$t_btn_ajouter_contrat.'" class="bn_ajouter" onClick="verif1();">
						</td>
					  </tr>';
					  } ?>
					<?php }?>
<!------------------------------------------------------->
					  <tr><td colspan="2">&nbsp;</td></tr>
					  <tr> 
						<td class="TX" height="40" colspan="2"> 
						<div id="contrat_liste">
						  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="TX">
							<tr align="center"> 
							  <td colspan="9" bgcolor="#000000" height="1"></td>
							</tr>
							<tr align="center"> 
							  <td class="TX_bold"><?php echo $t_part_typ_contrat ?></td>
							  <td class="TX_bold"><?php echo $t_part_date_debut ?></td>
							  <td class="TX_bold"><?php echo $t_info3 ?></td>
							  <td class="TX_bold"><?php echo $t_part_date_fin ?></td>
							  <td class="TX_bold"><?php echo $t_part_exclu_terr ?></td>
							  <td class="TX_bold"><?php echo $t_part_montant_droit_e ?></td>
							  <td class="TX_bold"><?php echo $t_part_redevance_ann ?></td>
							  <td class="TX_bold"><?php echo $t_info4 ?></td>
							  <td class="TX_bold"><?php echo $t_suppr_abr ?></td>
							</tr>
							<tr align="center"> 
							  <td colspan="9" bgcolor="#000000" height="1"></td>
							</tr>
							<tr align="center"> 
							  <td colspan="9" height="6"></td>
							</tr>
							<?php
							if (is_array($qry_contrat)){
								foreach($qry_contrat as $contrat){
									?>
									<tr align="center">
									  <td><a href="admvak_edit_client.php?<?php echo 'partid='.$_GET['partid'].'&modif=1'.'&dateBegins='.
									  $contrat['cont_date_deb'].'&dateEnds='.$contrat['cont_date_fin'].'&exclu='.$contrat['cont_exclusivite'].
									  '&montant='.$contrat['cont_montant'].'&redevance='.$contrat['cont_redevance'].
									  '&info1='.$contrat['cont_info1'].'&info2='.$contrat['cont_info2']
									  ;?>" onClick="del_contrat(<?php echo $contrat['cont_id'] ?>, 1);">
									  <?php echo $contrat['code_libelle'] ?></a></td>
									  <td><?php echo $contrat['cont_date_deb'] ?></td>
									  <td><?php echo $contrat['cont_info1'] ?></td>
									  <td><?php echo $contrat['cont_date_fin'] ?></td>
									  <td>
									  <?php
									  if ($contrat['cont_exclusivite']=='1'){
										$checked1 = ' checked="checked" ';
									  }else{
										$checked1 = ' ';
									  }
									  ?>
										<input type="checkbox" disabled="disabled"<?php echo $checked1 ?>name="redev2" value="checkbox">
									  </td>
									  <td><?php echo $contrat['cont_montant'] ?> &euro; <?php echo $t_HT ?> </td>
									  <td>
									  <?php
									  if ($contrat['cont_redevance']=='1'){
										$checked2 = ' checked="checked" ';
									  }else{
										$checked2 = ' ';
									  }
									  ?>
										<input type="checkbox" disabled="disabled"<?php echo $checked2 ?>name="redev" value="checkbox">
									  </td>
									  <td><?php echo $contrat['cont_info2'] ?></td>
									  <td><img src="../images/icon_supp2.gif" onMouseOver="this.style.cursor='pointer'" onClick="del_contrat(<?php echo $contrat['cont_id'] ?>, 0);"></td>
									</tr>
									<tr align="center" bgcolor="CCCCCC"> 
									  <td colspan="9" align="left" height="1"></td>
									</tr>
									<?php
								}
							}
							?>
							<tr align="center"> 
							  <td colspan="9" align="left">&nbsp;</td>
							</tr>
						  </table>
						  </div>
						</td>
					  </tr>
					  <tr> 
						<td class="TX_partenaires">&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr> 
						<td class="TX_partenaires"><?php echo $t_fiche_ergonomie ?></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr bgcolor="#000000"> 
						<td height="1"></td>
						<td height="1"></td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_fiche_lang_extra ?>* :</td>
						<td class="TX"> 
						  <select name="langue" class="form_ediht_Partenaires">
							<?php
							  if (is_array($qry_langue_list)){
								foreach($qry_langue_list as $langue){
									if ($langue['lang_id'] == $qry_partenaire[0]['part_lang_id']){
										$selected = ' selected="selected" ';
									}else{
										$selected = ' ';
									}
									echo '<option'.$selected.'value="'.$langue['lang_id'].'">'.$langue['lang_libelle'].'</option>';
								}
							  }
							?>
						  </select>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_fiche_logo_part1 ?></td>
						<td> 
						  <input type="file" name="photo" size="50" class="form_ediht_Partenaires">
						  <?php
						  if (file_exists('./images_societe/'.$qry_partenaire[0]['part_logo1']) && $qry_partenaire[0]['part_logo1']!=''){
							if($_SESSION['droit'] != 4){ 
							echo '<div id="div_img" style="display: inline;"><img src="./images_societe/'.$qry_partenaire[0]['part_logo1'].'" alt="" width="46" >&nbsp;&nbsp;<img src="../images/icon_supp2.gif" alt="supp." onmouseover="this.style.cursor=\'pointer\'" onclick="supp_logo(\'part_logo1\')"></div>';
							}
							else
							{
							echo '<div id="div_img" style="display: inline;"><img src="./images_societe/'.$qry_partenaire[0]['part_logo1'].'" alt="" width="46" ></div>';
							}
						  }
						  else{
							?>
							<img src="../images/logo.jpg" alt="" width="46" height="29">
							<?php
						  }
						  ?>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_fiche_logo_part3.'<br>'.$t_fiche_ilogo_part3 ?></td>
						<td> 
						  <input type="file" name="photo3" size="50" class="form_ediht_Partenaires">
						  <?php
						  if (file_exists('./images_societe/'.$qry_partenaire[0]['part_logo3']) && $qry_partenaire[0]['part_logo3']!=''){
							if($_SESSION['droit'] != 4){ 
								echo '<div id="div_img3" style="display: inline;"><img src="./images_societe/'.$qry_partenaire[0]['part_logo3'].'" alt="" width="46" >&nbsp;&nbsp;<img src="../images/icon_supp2.gif" alt="supp." onmouseover="this.style.cursor=\'pointer\'" onclick="supp_logo(\'part_logo3\')"></div>';
							}
							else
							{
								echo '<div id="div_img3" style="display: inline;"><img src="./images_societe/'.$qry_partenaire[0]['part_logo3'].'" alt="" width="46" ></div>';
							}
						  }
						  ?>
						</td>
					  </tr>
					  <!--
					  <tr> 
						<td class="TX" height="40" valign="top">Couleur de fond* :</td>
						<td align="left"> 
						  <input type="text" name="couleur_fond" id="couleur" size="7" maxlength="7" class="form_ediht_Partenaires" value="<?php echo $qry_partenaire[0]['part_couleur_fond'] ?>" readonly>
						&nbsp;&nbsp;<div id="tab_palette"></div>
						<script language="JavaScript">
						  palette_couleurs();
						</script>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40">Police du texte* :</td>
						<td> 
						  <select name="police_texte" class="form_ediht_Partenaires">
						  -->
							<?php
							  /*
							  if (is_array($qry_police_list)){
								foreach($qry_police_list as $police){
									if ($police['code_id'] == $qry_partenaire[0]['part_police_texte_code_id']){
										$selected = ' selected="selected" ';
									}else{
										$selected = ' ';
									}
									echo '<option'.$selected.'value="'.$police['code_id'].'">'.$police['code_libelle'].'</option>';
								}
							  }
							  */
							?>
						 <!-- </select>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40">&nbsp;</td>
						<td>&nbsp; </td>
					  </tr>
					  -->
					</table></td>
			    </tr>
			  </table>
			  
			  <br>
			  <?php if($_SESSION['droit'] != 4){ 
					  echo '<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td align="center"> 
					<input type="hidden" name="police_texte" value="1">
					<input type="hidden" name="couleur_fond" value="#FFFFFF">
					<input type="hidden" name="part_id" value="'.$qry_partenaire[0]['part_id'].'">
					<input type="button" name="Submit" value="'.$t_btn_valider.'" class="bn_valider_partenaire" onclick="verif();"> 
					</td>
				</tr>
			  </table>';
					  } ?>
                      
                      <br>

			</form>
			</body>
			</html>
		<?php
		}
	}
}else{
	include('no_acces.php');
}
?>
