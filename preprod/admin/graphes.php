<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	$tab_style = array(
		0=>'pointillé',
		1=>'continu');
	
	
	if ($_POST['choix_action']!=''){

		switch($_POST['choix_action']){
			case 'selection':
				if ($_POST['select_select']!=''){
					header('location:graphes.php?grapheid='.intval($_POST['select_select']));
				}
			break;
			case 'creation': // Création d'un nouveau questionnaire
				if (trim($_POST['txt_graphe'])!=''){
					// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
					$sql_seq_num 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
					$qry_seq_num 		= $db->query($sql_seq_num);
					$seq_num			= intval($qry_seq_num[0]['seq_num']);
				
					$sql_insert_graphe 	= "INSERT INTO GRAPHE VALUES('".$seq_num."', 
					'".txt_db($_POST['txt_graphe'])."', 
					'')";
					$qry_insert_graphe	= $db->query($sql_insert_graphe);
					
					header('location:graphes.php?grapheid='.$seq_num);
				}else{
					$error = '<?php echo $t_manque_questionnaire ?>';
				}
			break;
			
			case 'duplication':
				if ($_POST['select_duplic']!='' && trim($_POST['nvo_nom_graphe'])!=''){
					// On sélectionne les informations pour le graphe à dupliquer
					$sql_select_graphe_a_dupliquer	= "SELECT * FROM GRAPHE WHERE GRAPHE_ID='".txt_db($_POST['select_duplic'])."'";
					$qry_gad						= $db->query($sql_select_graphe_a_dupliquer);
					
					if (is_array($qry_gad)){
						// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
						$sql_seq_num 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
						$qry_seq_num 		= $db->query($sql_seq_num);
						$seq_num			= intval($qry_seq_num[0]['seq_num']);
						
						$sql_insert_gai	= "INSERT INTO GRAPHE VALUES('".txt_db($seq_num)."', 
						'".txt_db($_POST['nvo_nom_graphe'])."',
						'".txt_db($qry_gad[0]['graphe_type_graphe_code_id'])."')";
						$qry_insert_gai	= $db->query($sql_insert_gai);						
						
						/* Récupération des données de la table GRAPHE_A_TEXTE pour le graphe sélectionné */
						$sql_recup_gal 	= "SELECT * FROM GRAPHE_A_TEXTE WHERE GRAPHE_ID='".$qry_gad[0]['graphe_id']."'";
						$qry_recup_gal	= $db->query($sql_recup_gal);
						
						if (is_array($qry_recup_gal)){
							foreach($qry_recup_gal as $data_graphe_a_lang){					
								/* Pour chaque langue insérée dans le graphe à dupliquer, on fait une insertion avec l'id du nouveau graphe, en récupérant les memes données */
								$sql_insert_gal = "INSERT INTO GRAPHE_A_TEXTE (GRAPHE_ID,LANG_ID,ENTETE_COL1,ENTETE_COL2,ENTETE_COL3,ENTETE_COL4,
								TITRE,PIED_COL1,PIED_COL2,PIED_COL3,PIED_COL4,TOP_GRAPHE,MID_GRAPHE,BOTTOM_GRAPHE,AFFICHE_NOM,MIN_HAUTEUR,
								MAX_HAUTEUR,AFFICHE_SOC) VALUES ('".txt_db($seq_num)."',
								'".txt_db($data_graphe_a_lang['lang_id'])."',
								'".txt_db($data_graphe_a_lang['entete_col1'])."',
								'".txt_db($data_graphe_a_lang['entete_col2'])."',
								'".txt_db($data_graphe_a_lang['entete_col3'])."',
								'".txt_db($data_graphe_a_lang['entete_col4'])."',
								'".txt_db($data_graphe_a_lang['titre'])."',
								'".txt_db($data_graphe_a_lang['pied_col1'])."',
								'".txt_db($data_graphe_a_lang['pied_col2'])."',
								'".txt_db($data_graphe_a_lang['pied_col3'])."',
								'".txt_db($data_graphe_a_lang['pied_col4'])."',
								'".txt_db($data_graphe_a_lang['top_graphe'])."',
								'".txt_db($data_graphe_a_lang['mid_graphe'])."',
								'".txt_db($data_graphe_a_lang['bottom_graphe'])."',
								'".txt_db($data_graphe_a_lang['affiche_nom'])."',
								'".txt_db($data_graphe_a_lang['min_hauteur'])."',
								'".txt_db($data_graphe_a_lang['max_hauteur'])."',
								'".txt_db($data_graphe_a_lang['affiche_soc'])."')";
								$qry_insert_gal 		= $db->query($sql_insert_gal);								
							}
						}
						
						/* Récupération de tous les questionnaires pour le graphe sélectionné */
						$sql_recup_valeurs 	= "SELECT * FROM GRAPHE_A_QUEST WHERE GRAPHE_ID='".$qry_gad[0]['graphe_id']."' order by GRAPHE_A_QUEST_ID";
						$qry_recup_valeurs	= $db->query($sql_recup_valeurs);
						
						if (is_array($qry_recup_valeurs)){
							foreach ($qry_recup_valeurs as $valeur){
								
								/* ON récupère les libellés des différentes langues pour chaque valeur retrouvée */
								$sql_libelles	= "SELECT * FROM GRAPHE_A_LIB WHERE GRAPHE_A_QUEST_ID='".txt_db($valeur['graphe_a_quest_id'])."'";
								$qry_libelles	= $db->query($sql_libelles);
								
								// On récupère l'id du libellé à insérer.
								$sql_seq_num_gquestid 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
								$qry_seq_num_gquestid		= $db->query($sql_seq_num_gquestid);
								$seq_num_gquestid			= intval($qry_seq_num_gquestid[0]['seq_num']);
								
								
								$sql_insert_valeurs = "INSERT INTO GRAPHE_A_QUEST VALUES('".txt_db($seq_num_gquestid)."',
								'".txt_db($seq_num)."',								
								'".txt_db($valeur['quest_id'])."',									
								'".txt_db($valeur['type_profil_code_id'])."',										
								'".txt_db($valeur['couleur'])."',										
								'".txt_db($valeur['style'])."')";
								//echo $sql_insert_valeurs.'<br><br>';
								$qry_insert_valeur = $db->query($sql_insert_valeurs);
								
								/* Pour chaque libellé sélectionné, on insère un nouveau libellé avec la langue en quesion. */
								if (is_array($qry_libelles)){
									foreach ($qry_libelles as $libelle){
									
										/* Pour chaque langue trouvée, on fait une insertion des questions */
										$sql_insert_libelles = "INSERT INTO GRAPHE_A_LIB VALUES('".txt_db($seq_num_gquestid)."',
										'".txt_db($libelle['lang_id'])."',
										'".txt_db($libelle['libelle'])."')";
										
										//echo $sql_insert_libelles.'<br>';
										$qry_insert_libelles = $db->query($sql_insert_libelles);
									}
								}
								
							}
						}
						header('location:graphes.php?grapheid='.$seq_num);
					}
				}
			break;
			case 'update_img':
			
				$sql_up_textes	= "UPDATE GRAPHE_A_TEXTE SET 
				AFFICHE_NOM = '".intval($_POST['affiche_nom'])."',
				TOP_GRAPHE = '".txt_db($fname1)."',
				TITRE = '".txt_db($_POST['titre'])."',
				MID_GRAPHE = '".txt_db($fname2)."',
				BOTTOM_GRAPHE = '".txt_db($fname3)."',
				MAX_HAUTEUR = '".txt_db($_POST['max'])."',
				MIN_HAUTEUR = '".txt_db($_POST['min'])."'
				WHERE LANG_ID='".txt_db($langid)."' AND GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
				$qry_up_textes	= $db->query($sql_up_textes);
		}
	}
	/* On sélectionne tous les graphes présents dans la base de données */
	$sql_graphe_liste = "SELECT * FROM GRAPHE ORDER BY GRAPHE_NOM ASC";
	$qry_graphe_liste = $db->query($sql_graphe_liste);
	
	/* On sélectionne tous les graphes présents dans la base de données */
	$sql_graphe_liste2 = "SELECT * FROM GRAPHE ORDER BY GRAPHE_NOM ASC";
	$qry_graphe_liste2 = $db->query($sql_graphe_liste2);
	
	
	if ($_GET['grapheid']>0){
	
		/* On fait la liste des langues répertoriées dans la base */
		$sql_lang_list	= "SELECT * FROM LANGUE WHERE LANG_ID IN 
		(SELECT LANG_ID FROM QUEST_A_LANG WHERE QUEST_ID IN 
		(SELECT QUEST_ID FROM GRAPHE_A_QUEST WHERE GRAPHE_ID='".intval($_GET['grapheid'])."')) 
		ORDER BY LANG_LIBELLE ASC";
		$qry_lang_list	= $db->query($sql_lang_list);
		
		if ($_GET['action'] == 'delete_gquest' && $_GET['gquestid']>0){
		
			/* On supprime le questionnaire assigné au graphique */
			$sql_delete_gquest = "DELETE FROM GRAPHE_A_QUEST WHERE GRAPHE_A_QUEST_ID='".txt_db(intval($_GET['gquestid']))."'";
			$qry_delete_gquest = $db->query($sql_delete_gquest);
			
			/* On supprime les libellés connexes */
			$sql_delete_libelles = "DELETE FROM GRAPHE_A_LIB WHERE GRAPHE_A_QUEST_ID='".txt_db(intval($_GET['gquestid']))."'";
			$qry_delete_libelles = $db->query($sql_delete_libelles);
			
			header('location:graphes.php?grapheid='.$_GET['grapheid'].'&langid='.$_GET['langid']);
		}
		
		/* Si la variable de langue n'est pas définie, la langue est le FR */
		if ($_GET['langid']<1){
			$langid 		= '1';
		}else{
			$langid			= intval($_GET['langid']);
		}
		
		if ($_GET['action'] == 'update_img' && $_GET['gquestid']>0){
		
			if ($_GET['gquestid']=="1")
			{
			$sql_update_img = "UPDATE GRAPHE_A_TEXTE SET TOP_GRAPHE='' WHERE LANG_ID='".txt_db($langid)."' AND GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
			}
			if ($_GET['gquestid']=="2")
			{
			$sql_update_img = "UPDATE GRAPHE_A_TEXTE SET MID_GRAPHE='' WHERE LANG_ID='".txt_db($langid)."' AND GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
			}
			if ($_GET['gquestid']=="3")
			{
			$sql_update_img = "UPDATE GRAPHE_A_TEXTE SET BOTTOM_GRAPHE='' WHERE LANG_ID='".txt_db($langid)."' AND GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
			}
			$qry_update_img = $db->query($sql_update_img);
			
			
			header('location:graphes.php?grapheid='.$_GET['grapheid'].'&langid='.$_GET['langid']);
		}
		
		/* On sélectionne le nom de la langue qui correspond a l'id de la langue passé en GET */ 
		$sql_info_lang	= "SELECT LANG_LIBELLE FROM LANGUE WHERE LANG_ID='".txt_db($langid)."'";	
		$qry_info_lang	= $db->query($sql_info_lang);
		
		if ($_POST['nom_graphe']!=''){ // Si le formulaire est posté 
			/* On vérifie si les textes sont déjà présents pour la langue demandée */
			$sql_verif_texte	= "SELECT * FROM GRAPHE_A_TEXTE WHERE GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."' AND LANG_ID='".txt_db(intval($langid))."'";
			$qry_verif_texte 	= $db->query($sql_verif_texte);
			
		
		if (is_uploaded_file($_FILES['top_graphe']['tmp_name'])){
			$error = '';
			
			$ftmp  = $_FILES['top_graphe']['tmp_name'];
			$fname1 = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['top_graphe']['name']));	
			$fname1 = strtr($fname1,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
			$fname1 = strtolower($fname1);

			//extensions autorisées	
			$extension_autorises= array('.jpg','.jpeg', '.png', '.gif');	
			$extension= strrchr($fname1,'.');	
			
			//lien des photos
			$lien  		= '../graphes/'.$fname1;


			/*Definition de la taille max des minatures. */

			$width_n  	= 600;
			$height_n 	= 600;

			//verification de l'extension
			if (!in_array($extension,$extension_autorises)){
				$error .= '<?php echo $t_fichier_not_image  ?>';
			}
		
			// L'image est déplacée dans le dossier normal
			if (!move_uploaded_file($ftmp,$lien)){
				$error .= '<?php echo $t_copier_fichier_probleme  ?>';
			}

			/* Content type */	
			//***************************	

				/*creation des miniatures. */
				list($width_orig, $height_orig) = getimagesize($lien);

				// On stoppe le script si la taille de la photo est trop grande
				if ($width_orig > 1500 || $height_orig > 1500){
					if (file_exists($lien)){
						unlink($lien);
					}
					exit('<?php echo $t_image1_grande  ?> <br /><a href="javascript:history.back();">retour</a><br />'.$rappel.' <hr />');
				} 
				$ratio_orig   = $width_orig/$height_orig;

				
				// Ratio normal
				if ($width_orig > $width_n || $height_orig > $height_n){
					if ($width_n/$height_n > $ratio_orig){
						$width_n  = $height_n*$ratio_orig;

					}else{
						$height_n = $width_n/$ratio_orig;

					}
				}else{
					$width_n  = $width_orig;
					$height_n = $height_orig;
				}



				// Config normale
				$image_p_n 	  = imagecreatetruecolor($width_n,$height_n);
				if ($extension=='.jpeg' || $extension=='.jpg'){
					$image_n   	  = @imagecreatefromjpeg($lien);
				}
				if ($extension=='.png'){
					$image_n   	  = @imagecreatefrompng($lien);	
				}
				if ($extension=='.gif'){
					$image_n   	  = @imagecreatefromgif($lien);	
				}
				@imagecopyresampled($image_p_n,$image_n,0,0,0,0,$width_n, $height_n, $width_orig, $height_orig);


				unset($width_n);
				unset($height_n);
				unset($ratio_orig);

				
				//movance  de la miniature crée au dessus dans le repertoire normal 
					if ($extension=='.jpeg' || $extension=='.jpg'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagejpeg($image_p_n,$lien,90);
					}
					if ($extension=='.png'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagepng($image_p_n,$lien,7);
					}
					if ($extension=='.gif'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagegif($image_p_n,$lien,90);
					}
				imagedestroy($image_p_n);
				
				unset($lien);

				
			//*************************************************************

		}else{
			$fname1 = $qry_verif_texte[0]['top_graphe'];
		}
		
		if (is_uploaded_file($_FILES['mid_graphe']['tmp_name'])){
			$error = '';
			
			$ftmp  = $_FILES['mid_graphe']['tmp_name'];
			$fname2 = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['mid_graphe']['name']));	
			$fname2 = strtr($fname2,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
			$fname2 = strtolower($fname2);

			//extensions autorisées	
			$extension_autorises= array('.jpg','.jpeg', '.png', '.gif');	
			$extension= strrchr($fname2,'.');	
			
			//lien des photos
			$lien  		= '../graphes/'.$fname2;


			/*Definition de la taille max des minatures. */

			$width_n  	= 600;
			$height_n 	= 600;

			//verification de l'extension
			if (!in_array($extension,$extension_autorises)){
				$error .= '<?php echo $t_fichier_not_image  ?>';
			}
		
			// L'image est déplacée dans le dossier normal
			if (!move_uploaded_file($ftmp,$lien)){
				$error .= '<?php echo $t_copier_fichier_probleme  ?>';
			}

			/* Content type */	
			//***************************	

				/*creation des miniatures. */
				list($width_orig, $height_orig) = getimagesize($lien);

				// On stoppe le script si la taille de la photo est trop grande
				if ($width_orig > 1500 || $height_orig > 1500){
					if (file_exists($lien)){
						unlink($lien);
					}
					exit('<?php echo $t_image1_grande  ?> <br /><a href="javascript:history.back();">retour</a><br />'.$rappel.' <hr />');
				} 
				$ratio_orig   = $width_orig/$height_orig;

				
				// Ratio normal
				if ($width_orig > $width_n || $height_orig > $height_n){
					if ($width_n/$height_n > $ratio_orig){
						$width_n  = $height_n*$ratio_orig;

					}else{
						$height_n = $width_n/$ratio_orig;

					}
				}else{
					$width_n  = $width_orig;
					$height_n = $height_orig;
				}



				// Config normale
				$image_p_n 	  = imagecreatetruecolor($width_n,$height_n);
				if ($extension=='.jpeg' || $extension=='.jpg'){
					$image_n   	  = @imagecreatefromjpeg($lien);
				}
				if ($extension=='.png'){
					$image_n   	  = @imagecreatefrompng($lien);	
				}
				if ($extension=='.gif'){
					$image_n   	  = @imagecreatefromgif($lien);	
				}
				@imagecopyresampled($image_p_n,$image_n,0,0,0,0,$width_n, $height_n, $width_orig, $height_orig);


				unset($width_n);
				unset($height_n);
				unset($ratio_orig);

				
				//movance  de la miniature crée au dessus dans le repertoire normal 
					if ($extension=='.jpeg' || $extension=='.jpg'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagejpeg($image_p_n,$lien,90);
					}
					if ($extension=='.png'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagepng($image_p_n,$lien,7);
					}
					if ($extension=='.gif'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagegif($image_p_n,$lien,90);
					}
				imagedestroy($image_p_n);
				
				unset($lien);

				
			//*************************************************************

		}else{
			$fname2 = $qry_verif_texte[0]['mid_graphe'];
		}	
		
		if (is_uploaded_file($_FILES['bottom_graphe']['tmp_name'])){
			$error = '';
			
			$ftmp  = $_FILES['bottom_graphe']['tmp_name'];
			$fname3 = (substr(md5(uniqid(rand(), true)),0,6)).(str_replace(' ','_', $_FILES['bottom_graphe']['name']));	
			$fname3 = strtr($fname3,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
			$fname3 = strtolower($fname3);

			//extensions autorisées	
			$extension_autorises= array('.jpg','.jpeg', '.png', '.gif');	
			$extension= strrchr($fname3,'.');	
			
			//lien des photos
			$lien  		= '../graphes/'.$fname3;


			/*Definition de la taille max des minatures. */

			$width_n  	= 600;
			$height_n 	= 600;

			//verification de l'extension
			if (!in_array($extension,$extension_autorises)){
				$error .= '<?php echo $t_fichier_not_image  ?>';
			}
		
			// L'image est déplacée dans le dossier normal
			if (!move_uploaded_file($ftmp,$lien)){
				$error .= '<?php echo $t_copier_fichier_probleme  ?>';
			}

			/* Content type */	
			//***************************	

				/*creation des miniatures. */
				list($width_orig, $height_orig) = getimagesize($lien);

				// On stoppe le script si la taille de la photo est trop grande
				if ($width_orig > 1500 || $height_orig > 1500){
					if (file_exists($lien)){
						unlink($lien);
					}
					exit('<?php echo $t_image1_grande  ?> <br /><a href="javascript:history.back();">retour</a><br />'.$rappel.' <hr />');
				} 
				$ratio_orig   = $width_orig/$height_orig;

				
				// Ratio normal
				if ($width_orig > $width_n || $height_orig > $height_n){
					if ($width_n/$height_n > $ratio_orig){
						$width_n  = $height_n*$ratio_orig;

					}else{
						$height_n = $width_n/$ratio_orig;

					}
				}else{
					$width_n  = $width_orig;
					$height_n = $height_orig;
				}



				// Config normale
				$image_p_n 	  = imagecreatetruecolor($width_n,$height_n);
				if ($extension=='.jpeg' || $extension=='.jpg'){
					$image_n   	  = @imagecreatefromjpeg($lien);
				}
				if ($extension=='.png'){
					$image_n   	  = @imagecreatefrompng($lien);	
				}
				if ($extension=='.gif'){
					$image_n   	  = @imagecreatefromgif($lien);	
				}
				@imagecopyresampled($image_p_n,$image_n,0,0,0,0,$width_n, $height_n, $width_orig, $height_orig);


				unset($width_n);
				unset($height_n);
				unset($ratio_orig);

				
				//movance  de la miniature crée au dessus dans le repertoire normal 
					if ($extension=='.jpeg' || $extension=='.jpg'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagejpeg($image_p_n,$lien,90);
					}
					if ($extension=='.png'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagepng($image_p_n,$lien,7);
					}
					if ($extension=='.gif'){
						//movance  de la miniature crée au dessus dans le repertoire normal 
						imagegif($image_p_n,$lien,90);
					}
				imagedestroy($image_p_n);
				
				unset($lien);

				
			//*************************************************************

		}else{
			$fname3 = $qry_verif_texte[0]['bottom_graphe'];
		}
			
			if (is_array($qry_verif_texte)){
				$sql_up_textes	= "UPDATE GRAPHE_A_TEXTE SET 
				AFFICHE_NOM = '".intval($_POST['affiche_nom'])."',
				TOP_GRAPHE = '".txt_db($fname1)."',
				TITRE = '".txt_db($_POST['titre'])."',
				MID_GRAPHE = '".txt_db($fname2)."',
				BOTTOM_GRAPHE = '".txt_db($fname3)."',
				MAX_HAUTEUR = '".txt_db($_POST['max'])."',
				MIN_HAUTEUR = '".txt_db($_POST['min'])."'
				WHERE LANG_ID='".txt_db($langid)."' AND GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
				$qry_up_textes	= $db->query($sql_up_textes);
			}else{			
				/* On enregistre les textes pour la langue choisie */
				$sql_insert_textes	= "INSERT INTO GRAPHE_A_TEXTE (GRAPHE_ID, LANG_ID, AFFICHE_NOM, TITRE, TOP_GRAPHE, MID_GRAPHE, BOTTOM_GRAPHE, MAX_HAUTEUR,MIN_HAUTEUR) VALUES('".txt_db(intval($_GET['grapheid']))."', 
				'".txt_db($langid)."',
				'".intval($_POST['affiche_nom'])."',
				'".txt_db($_POST['titre'])."',
				'".txt_db($fname1)."',
				'".txt_db($fname2)."',
				'".txt_db($fname3)."',
				'".txt_db($_POST['max'])."',
				'".txt_db($_POST['min'])."')";
				$qry_insert_textes	= $db->query($sql_insert_textes);
			}
			/* On update les infos de base pour le graphe */
			$sql_up_graphe = "UPDATE GRAPHE SET GRAPHE_NOM = '".txt_db($_POST['nom_graphe'])."', GRAPHE_TYPE_GRAPHE_CODE_ID='".txt_db($_POST['type_graphe'])."' WHERE GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
			$qry_up_graphe	= $db->query($sql_up_graphe);
			
			header('location:graphes.php?grapheid='.$_GET['grapheid'].'&idnc='.($_GET['idnc']+1));
		}
		
		/* On sélectionne le graphe voulu */
		$sql_graphe = "SELECT * FROM GRAPHE WHERE GRAPHE_ID='".txt_db($_GET['grapheid'])."'";
		$qry_graphe = $db->query($sql_graphe);
	
		/* On sélectionne tous les textes du graphe sélectionné, par langue */
		$sql_graphe_texte	= "SELECT * FROM GRAPHE_A_TEXTE WHERE GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."' AND LANG_ID='".txt_db(intval($langid))."'";
		$qry_graphe_texte 	= $db->query($sql_graphe_texte);
		
		/* On sélectionne la liste de tous les types de graphes disponibles */
		$sql_type_graphe_liste = "SELECT * FROM CODE WHERE CODE_TABLE = 'TYPE_GRAPHE' ORDER BY CODE_LIBELLE ASC";
		$qry_type_graphe_liste = $db->query($sql_type_graphe_liste);
		
		/* On sélectionne tous les questionnaires qui utilisent le graphe */
		$sql_quest_graphe = "SELECT CODE.CODE_LIBELLE, GRAPHE_A_QUEST.*, QUESTIONNAIRE.QUEST_NOM, QUESTIONNAIRE.QUEST_ID,GRAPHE_TYPE_GRAPHE_CODE_ID FROM CODE, GRAPHE, GRAPHE_A_QUEST, QUESTIONNAIRE WHERE GRAPHE.GRAPHE_ID=GRAPHE_A_QUEST.GRAPHE_ID AND CODE.CODE_ID=GRAPHE_A_QUEST.TYPE_PROFIL_CODE_ID AND CODE.CODE_TABLE='TYPE_PROFIL' AND GRAPHE_A_QUEST.QUEST_ID = QUESTIONNAIRE.QUEST_ID AND QUESTIONNAIRE.QUEST_DATE_SUPPRESSION IS NULL AND GRAPHE_A_QUEST.GRAPHE_ID='".txt_db($qry_graphe[0]['graphe_id'])."' ORDER BY GRAPHE_A_QUEST_ID";
		$qry_quest_graphe = $db->query($sql_quest_graphe);
				
		/* Si le graphe n'est pas utilisé sur un document */
		if(!is_array($qry_verif_affil)){
			/* Si on a demandé de supprimer le graphe */
			if($_GET['action']=='delete_graphe'){

				/* On fait un DELETE dans la table */
				$sql_del_graphe  = "DELETE FROM GRAPHE WHERE GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
				$qry_del_graphe  = $db->query($sql_del_graphe);
				
				/* On redirige la personne sur la page d'accueil des graphes */
				header('location:graphes.php');
			}
		}
	}	
		?>
		<!DOCTYPE html>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<link rel="stylesheet" href="../css/style.css" type="text/css">
		<script src="../js/jquery-latest.min.js"></script>			
		<strong><script src="https://cdn.tiny.cloud/1/n15lggt937iydpcbh026tvnj0a11vtiwcjd2tdoxiaf4y4da/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script></strong>
		<script type="text/javascript">
		<!--
		function supprimer_graphe(grapheid){
			if (grapheid>0){
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_supp_graphe.php"; // La page qui réceptionne les données
				var data     = null; 

				var xhr_object = null; 
					 
				if(window.XMLHttpRequest) // Firefox 
				   xhr_object = new XMLHttpRequest(); 
				else if(window.ActiveXObject) // Internet Explorer 
				   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
				else { // XMLHttpRequest non supporté par le navigateur 
				   alert("<?php echo $t_browser_support_error_1 ?>");
				   return; 
				} 
				 

				data = "grapheid="+grapheid;
				
				if(method == "GET" && data != null) {
				   filename += "?"+data;
				   data      = null;
				}
				 
				xhr_object.open(method, filename, true);

				xhr_object.onreadystatechange = function() {
				   if(xhr_object.readyState == 4) {
					  var tmp = xhr_object.responseText.split(":"); 
					  if(typeof(tmp[0]) != "undefined") { 
						if(tmp[0]==0){
							if(confirm('<?php echo $t_supprimer_gph ?>')){
								document.location.href='graphes.php?grapheid='+grapheid+'&action=delete_graphe';
							}
						}else{
							alert('<?php echo $t_graph_utilise ?>');
						}
					  }
				   } 
				} 

				xhr_object.send(data); //On envoie les données
			}
			
		}
		
		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}

		function MM_openBrWindow(theURL,winName,features) { //v2.0
		  window.open(theURL,winName,features);
		}
		
		function delete_quest_graphe(gquestid){
			if (confirm('<?php echo $t_supprimer_data ?>')){
				document.location.href='graphes.php?action=delete_gquest&langid=<?php echo $_GET['langid'] ?>&grapheid=<?php echo $_GET['grapheid'] ?>&gquestid='+gquestid;
			}
		}
		function enleve(gquestid){
			if (confirm('<?php echo $t_supprimer_img ?>')){
				document.location.href='graphes.php?action=update_img&langid=<?php echo $_GET['langid'] ?>&grapheid=<?php echo $_GET['grapheid'] ?>&gquestid='+gquestid;
			}
		}
		//-->
		<?php
		if($_GET['grapheid']>0 && is_array($qry_lang_list)){
			?>
			tinymce.init({
			selector: 'textarea',
			height: 300,
			width:600,
			plugins: [
			  'advlist autolink link image lists charmap print preview hr anchor pagebreak',
			  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			  'table emoticons template paste help'
			],
			toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
			  'bullist numlist outdent indent | link image | print preview media fullpage | ' +
			  'forecolor backcolor emoticons | help',
			menu: {
			  favs: {title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons'}
			},
			menubar: 'favs file edit view insert format tools table help',
			content_css: 'css/content.css'
		  });
		function uploadFile(inp, editor) {
			var input = inp.get(0);
			var data = new FormData();
			data.append('image[file]', input.files[0]);

			$.ajax({
				url: 'uploads/post.php',
				type: 'POST',
				data: data,
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				success: function(data, textStatus, jqXHR) {
					data = jQuery.parseJSON(data);
					editor.insertContent('<img class="content-img" src="uploads/images/' + data.location + '"/>');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if(jqXHR.responseText) {
						errors = JSON.parse(jqXHR.responseText).errors
						alert('Error uploading image: ' + errors.join(", ") + '. Make sure the file is an image and has extension jpg/jpeg/png.');
					}
				}
			});
		}
			<?php
		}
		?>
			
			
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
			<?php
				include('menu_top_new.php');
			?>
	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
		<form method="post" action="#">				
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;GRAPHES</td>
		</tr>
		</table>
        
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14" align="right"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
    <tr>
      <td width="14" height="14"></td>
      <td align="center" class="TX"><table  bgcolor="F1F1F1" align="center">
				  <?php
				  /* S'il y a au moins un graphe dans la base de données */
				  if (is_array($qry_graphe_liste)){
				  ?>
				  <tr> 
					<td align="left" class="TX"> 
					  <input type="radio" id="choix_action1" name="choix_action" value="selection" checked="checked">
					  S&eacute;lectionner un graphe :&nbsp;&nbsp;</td>
					<td align="left" class="TX"> 
					  <select name="select_select" id="sel_graphe" class="form_ediht_Tarifs" onclick="document.getElementById('choix_action1').checked=true;">
						<?php
						foreach($qry_graphe_liste as $graphe_liste){
							echo '<option value="'.$graphe_liste['graphe_id'].'">'.$graphe_liste['graphe_nom'].'</option>';
						}
						?>
					  </select>&nbsp;<input type="button" class="bn_ajouter" value="Supprimer" onClick="supprimer_graphe(document.getElementById('sel_graphe').options[document.getElementById('sel_graphe').selectedIndex].value);">
					</td>
				  </tr>
				  <?php
				  }
				  ?>
				  <tr> 
					<td align="left" class="TX"> 
					  <input type="radio" id="choix_action2" name="choix_action" value="creation"> 
					  Cr&eacute;er&nbsp;un&nbsp;nouveau&nbsp;graphe&nbsp;:&nbsp;&nbsp;</td>
					<td align="left" class="TX"> 
					  <input type="text" name="txt_graphe" class="form_ediht_Tarifs" size="30" maxlength="50" onClick="document.getElementById('choix_action2').checked=true;">
					</td>
				  </tr>
				  <?php
				  /* S'il y a au moins un graphe dans la base de données */
				  if (is_array($qry_graphe_liste2)){
				  ?>
				  <tr> 
					<td align="left" class="TX"> 
					  <input type="radio" id="choix_action3" name="choix_action" value="duplication">
					  Dupliquer un graphe :&nbsp;&nbsp;</td>
					<td align="left" class="TX"> 
					  <select name="select_duplic" class="form_ediht_Tarifs" onclick="document.getElementById('choix_action3').checked=true;">
						<?php
						foreach($qry_graphe_liste2 as $graphe_liste2){
							echo '<option value="'.$graphe_liste2['graphe_id'].'">'.$graphe_liste2['graphe_nom'].'</option>';
						}
						?>
					  </select>
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nouveau libellé du graphe : 
					  <input type="text" name="nvo_nom_graphe" class="form_ediht_Tarifs" size="30" maxlength="50" onClick="document.getElementById('choix_action3').checked=true;">
					</td>
				  </tr>
				  <?php
				  }
				  ?>
				</table></td>
      <td width="14" height="14"></td>
    </tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14" align="right"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>

</table>
        
        
        
        
        
        
        
        
        
        
        
		
		  
		  <br>
		  <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
			<tr> 
			  <td align="center" valign="top"> 
				<input type="submit" name="Submit" value="VALIDER" class="BN">
			  </td>
			</tr>
		  </table>
		  <br>
		</form>
		<?php
	if ($_GET['grapheid']>0){
		?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo htmlentities($qry_graphe[0]['graphe_nom']) ?></td>
		</tr>
		</table>
		<form name="form" method="post" action="#" enctype="multipart/form-data">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="left" style="text-align: left;" class="TX_Tarifs">Caract&eacute;ristiques du graphe</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="1"></td>
			  <td align="center" height="1" bgcolor="#666666"></td>
			  <td width="14" height="1"></td>
			</tr>
			<tr> 
			  <td width="14" height="1"></td>
			  <td align="center" class="TX" >&nbsp;</td>
			  <td width="14" height="1"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="center" class="TX"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">Nom du graphe : 
					  <input type="text" name="nom_graphe" class="form_ediht_Tarifs" size="60" maxlength="60" value="<?php echo htmlentities($qry_graphe[0]['graphe_nom']) ?>">
					</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top" class="TX">&nbsp;</td>
					<td align="left" valign="top" class="TX">Type de graphe : 
					  <select name="type_graphe" class="form_ediht_Tarifs">
						<?php
						if (is_array($qry_type_graphe_liste)){
							foreach($qry_type_graphe_liste as $type_graphe_liste){
								unset($selected_type_G);
								if ($qry_graphe[0]['graphe_type_graphe_code_id'] == $type_graphe_liste['code_id']){
									$selected_type_G = ' selected="selected"';
								}
								echo '<option value="'.$type_graphe_liste['code_id'].'"'.$selected_type_G.'>'.htmlentities($type_graphe_liste['code_libelle']).'</option>';
							}
						}else{
							echo '<option value="0"><?php echo $t_aucune_type_graph ?></option>';
						}
						?>
					  </select>
					</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
				<?php
				if(is_array($qry_lang_list)){
				?>
				  <tr> 
					<td align="center" bgcolor="#EAEAEA">&nbsp; </td>
					<td align="left" bgcolor="#EAEAEA" valign="middle"><b>Saisissez vos 
					  textes en : </b>&nbsp;&nbsp; 
					  <?php
					  foreach($qry_lang_list as $lang_list){
						echo '<input type="button" value="'.htmlentities($lang_list['lang_libelle']).'" onclick="document.location.href=\'graphes.php?grapheid='.$_GET['grapheid'].'&langid='.$lang_list['lang_id'].'\'" class="bn_Liens">&nbsp;';
					  }
					  ?>
					  
					</td>
				  </tr>
				  <tr> 
					<td >&nbsp;</td>
					<td >&nbsp;</td>
				  </tr>
				  <tr> 
					<td>&nbsp; </td>
					<td align="left">Titre (<?php echo $qry_info_lang[0]['lang_libelle'] ?>) :</td>
				  </tr>
				  <tr> 
					<td >&nbsp;</td>
					<td >&nbsp;</td>
				  </tr>
				  <tr> 
					<td >&nbsp;</td>
					<td align="left" valign="top">
					<textarea name="titre" id="titre" width="601" height="299"><?php echo $qry_graphe_texte[0]['titre'] ?></textarea>
					</td>
				  </tr>
				   <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">
					+ affichage des légendes&nbsp;<input type="checkbox" name="affiche_nom" value="1" <?php if($qry_graphe_texte[0]['affiche_nom']==1){echo ' checked="checked"';} ?>>
					</td>
				  </tr>
				   <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="center" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">Hauteur maximale du graphe&nbsp;:&nbsp;&nbsp;<input type="text" name="max" size="5" value="<?php echo $qry_graphe_texte[0]['max_hauteur'];?>"></td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">Hauteur minimale du graphe&nbsp;:&nbsp;&nbsp;<input type="text" name="min" size="5" value="<?php echo $qry_graphe_texte[0]['min_hauteur'];?>"></td>
				  </tr>
				   <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="center" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="center">&nbsp; </td>
					<td align="left">Partie supérieure du graphe 600*120 (<?php echo $qry_info_lang[0]['lang_libelle'] ?>) :&nbsp;<input type="file" name="top_graphe"></td>
				  </tr>
				  <tr> 
					<td align="center">&nbsp; </td>
					<td align="left"></td>
				  </tr>
				  <tr> 
					<td align="center">&nbsp; </td>
					<td align="left">Partie centrale du graphe 600*600 (<?php echo $qry_info_lang[0]['lang_libelle'] ?>) : <input type="file" name="mid_graphe"></td>
				  </tr>
				  <tr> 
					<td align="center">&nbsp; </td>
					<td align="left"></td>
				  </tr>
				  <tr> 
					<td align="center">&nbsp; </td>
					<td align="left">Partie inférieure du graphe 600*120 (<?php echo $qry_info_lang[0]['lang_libelle'] ?>) : <input type="file" name="bottom_graphe"></td>
				  </tr>
				  <tr> 
					<td align="center">&nbsp; </td>
					<td align="left"></td>
				  </tr>
				  <tr> 
					<td align="center">&nbsp; </td>
					<td align="left">
					  <?php
					  if($qry_graphe_texte[0]['top_graphe']!='' && file_exists('../graphes/'.$qry_graphe_texte[0]['top_graphe'])){
						  ?>
						  <img width="100" src="../graphes/<?php echo $qry_graphe_texte[0]['top_graphe'] ?>" alt="">
						  &nbsp;&nbsp;<img src="../images/icon_supp2.gif" onMouseOver="this.style.cursor='pointer'" border="0" onClick="enleve(1)">
						  <br>
					  <?php
					  }				  

					  if($qry_graphe_texte[0]['mid_graphe']!='' && file_exists('../graphes/'.$qry_graphe_texte[0]['mid_graphe'])){
						  ?>
						  <img width="100" src="../graphes/<?php echo $qry_graphe_texte[0]['mid_graphe'] ?>" alt="">
						  &nbsp;&nbsp;<img src="../images/icon_supp2.gif" onMouseOver="this.style.cursor='pointer'" border="0" onClick="enleve(2)">
						  <br>
					  <?php
					  }
					  
					  if($qry_graphe_texte[0]['bottom_graphe']!='' && file_exists('../graphes/'.$qry_graphe_texte[0]['bottom_graphe'])){
						  ?>
						  <img width="100" src="../graphes/<?php echo $qry_graphe_texte[0]['bottom_graphe'] ?>" alt="">
						  &nbsp;&nbsp;<img src="../images/icon_supp2.gif" onMouseOver="this.style.cursor='pointer'" border="0" onClick="enleve(3)">
						  <br>
					  <?php
					  }
				  }
				  ?>
				  </td></tr>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
		  <br>
		  <?php
		  if(is_array($qry_lang_list)){
			  ?>
			  <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
				<tr> 
				  <td align="center" valign="top"> 
					<input type="submit" name="Submit" value="VALIDER" class="BN">
				  </td>
				</tr>
			  </table>
			  <?php
		  }
		  ?>
		  		 
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;DONN&Eacute;ES DU GRAPHE</td>
			</tr>
			</table>
			  <table width="100%">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14" valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
					 <tr> 
						<td align="center" valign="top">&nbsp;</td>
						<td align="left" valign="top"> 
						  <input type="button" name="new_ctact" value="Ajouter" class="bn_ajouter" onClick="document.form.submit(); MM_openBrWindow('graphes_ajout.php?grapheid=<?php echo $qry_graphe[0]['graphe_id'] ?>&type=<?php echo $qry_graphe[0]['graphe_type_graphe_code_id'] ?>', '','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=970,height=400')">
						</td>
					  </tr>
					  <tr> 
						<td valign="top" align="center">&nbsp; </td>
						<td align="left" valign="top"> 
						  <table class="tabloGeneral">
							<tr> 
							  <td  align="left" class="TX_bold">Questionnaire</td>
							  <td  align="center" class="TX_bold">Type de profil</td>
							  <td  class="TX_bold">Couleur</td>
							  <td  class="TX_bold">Style</td>
							  <td  class="TX_bold" align="center">Modifier</td>
							  <td  class="TX_bold" align="center">Suppr</td>
							</tr>
							<?php
							if (is_array($qry_quest_graphe)){
								foreach($qry_quest_graphe as $quest_graphe){
									?>
									<tr> 
									  <td class="TX"><?php echo $quest_graphe['quest_nom'] ?></td>
									  <td class="TX" align="center"><?php echo $quest_graphe['code_libelle'] ?></td>
									  <td valign="top"><div style="width: 50px; background-color:<?php echo $quest_graphe['couleur'] ?>;">&nbsp;</div></td>
									  <td class="TX"><?php echo $tab_style[$quest_graphe['style']] ?></td>
									  <td align="center"><img onMouseOver="this.style.cursor='pointer'" onClick="document.form.submit(); MM_openBrWindow('graphes_edit.php?grapheid=<?php echo $_GET['grapheid'] ?>&gquestid=<?php echo $quest_graphe['graphe_a_quest_id'] ?>&type=<?php echo $quest_graphe['graphe_type_graphe_code_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=970,height=400')" src="modifier.png" width="15" border="0"></td>
									  <td align="center"><img onMouseOver="this.style.cursor='pointer'" onClick="delete_quest_graphe(<?php echo $quest_graphe['graphe_a_quest_id'] ?>)" src="../images/icon_supp2.gif" border="0" width="11" height="12"></td>
									</tr>
									<tr>
									  <td class="TX">&nbsp;</td>
									  <td class="TX" align="center">&nbsp;</td>
									  <td valign="top">&nbsp;</td>
									  <td class="TX">&nbsp;</td>
									  <td align="center">&nbsp;</td>
									  <td align="center">&nbsp;</td>
						    </tr>
									
									<?php
								}
							}else{
							echo '<tr> 
									<td align="center" valign="top" colspan="6"><?php echo $t_aucune_quest ?></td>
								  </tr>';
							}
							?>
						  </table>
						</td>
					  </tr>
					</table></td>
				  <td width="14"></td>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
		</form>
</p></div>	</article></div>	</div>	</div>	</div>	  		
		</body>
		</html>
		<?php
	}
}else{
	include('no_acces.php');
}
?>
