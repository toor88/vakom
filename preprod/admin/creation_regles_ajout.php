<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo.css" type="text/css">
<link rel="stylesheet" href="../css/general.css" type="text/css">
<script language="JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
<form method="post" action="">
  <table border="0" cellspacing="0" cellpadding="0" align="center" width="400">
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td align="left" class="TX"> 
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="100%">
          <tr> 
            <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
            <td height="14"></td>
            <td height="14"></td>
            <td height="14"></td>
            <td height="14"></td>
            <td height="14"></td>
            <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
          </tr>
          <tr> 
            <td width="14"></td>
            <td class="TX" colspan="6"><font color="EA98AA" ><b>CREATION DE NOUVELLES 
              REGLES</b></font></td>
          </tr>
          <tr> 
            <td width="14"></td>
            <td colspan="5" bgcolor="#666666" height="1"></td>
            <td width="14"></td>
          </tr>
          <tr> 
            <td width="14"></td>
            <td align="left" valign="top" class="TX">&nbsp;</td>
            <td align="center" class="TX">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="center" class="TX">&nbsp;</td>
            <td align="center" class="TX">&nbsp;</td>
            <td width="14"></td>
          </tr>
          <tr> 
            <td width="14"></td>
            <td align="left" class="TX">Type de profil :</td>
            <td align="center" class="TX"></td>
            <td align="left"> 
              <select name="select" class="form_ediht">
                <option>PI (+)</option>
                <option>PN (-)</option>
                <option>PR (#)</option>
                <option>ECARTS (&lt; &gt;)</option>
              </select>
            </td>
            <td align="center" class="TX">&nbsp;</td>
            <td align="center" class="TX">&nbsp;</td>
            <td width="14"></td>
          </tr>
          <tr> 
            <td width="14"></td>
            <td align="left" class="TX">&nbsp;</td>
            <td align="center" class="TX">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="center" class="TX">&nbsp;</td>
            <td align="center" class="TX">&nbsp;</td>
            <td width="14"></td>
          </tr>
          <tr> 
            <td width="14" height="40"></td>
            <td align="left" class="TX" height="40">Valeur des points : </td>
            <td align="center" height="40" class="TX">de</td>
            <td align="left" height="40"> 
              <input type="text" name="points_d" size="5">
            </td>
            <td align="center" height="40" class="TX">&agrave;</td>
            <td align="center" height="40" class="TX">
             <input type="text" name="points_f" size="5">
            </td>
            <td width="14" height="40">&nbsp; </td>
          </tr>
          <tr> 
            <td width="14" height="40"></td>
            <td align="left" class="TX" height="40">Valeur des segments :</td>
            <td align="center" height="40" class="TX">&nbsp;</td>
            <td align="left" height="40"> 
              <input type="text" name="segments" size="5">
            </td>
            <td align="center" height="40" class="TX">&nbsp;</td>
            <td align="center" height="40" class="TX">&nbsp;</td>
            <td width="14" height="40"></td>
          </tr>
          <tr> 
            <td width="14" height="40"></td>
            <td align="left" class="TX" height="40">Valeur des blocs : </td>
            <td align="center" height="40" class="TX">&nbsp;</td>
            <td align="left" height="40"> 
              <input type="text" name="bloc" size="5">
            </td>
            <td align="center" height="40" class="TX">&nbsp;</td>
            <td align="center" height="40" class="TX">&nbsp;</td>
            <td width="14" height="40"></td>
          </tr>
          <tr> 
            <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
            <td height="14"></td>
            <td height="14"></td>
            <td height="14"></td>
            <td height="14"></td>
            <td height="14"></td>
            <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td align="center" class="menu_Gris">&nbsp;</td>
    </tr>
    <tr> 
      <td align="center"> 
        <input type="button" name="Submit" value="Valider" class="BN" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
      </td>
    </tr>
    <tr> 
      <td align="right" width="180">&nbsp; </td>
    </tr>
  </table>
</form>
</body>
</html>
