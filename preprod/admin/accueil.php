<?php 
session_start();
	// Si l'utilisateur est un super admin

if (isset($_SESSION['droit'])){
?>
<!DOCTYPE html>
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<link rel="stylesheet" href="../css/style.css" type="text/css">
		<script src="../js/jquery-latest.min.js"></script>
		<strong><script src="https://cdn.tiny.cloud/1/n15lggt937iydpcbh026tvnj0a11vtiwcjd2tdoxiaf4y4da/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script></strong>
</head>
<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
table {
    border-bottom: 1px solid #ededed;
    border-collapse: collapse;
    border-spacing: 0;
    font-size: 14px;
    line-height: 2;
    margin: 0 0 20px;
    width: 100%;
}
caption, th, td {
    font-weight: normal;
    text-align: left;
}
td {
    border-top: 1px solid #ededed;
    padding: 6px 10px 6px 0;
}
td.accueil {
    vertical-align: top;
    line-height: normal;
    font-family: "Open Sans",sans-serif;
}
.texteOrangeTitre {
    color: #f29400;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 16px;
}
</style>
<body>
<?php 
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	$db = new db($conn);

	
	if ($_SESSION['droit'] == 9){
		if (isset($_POST['title'])){
			
			$title = $_POST['title'];
			$objet = $_POST['objet'];
			$title = str_replace("'","''",$title);
			$objet = str_replace("'","''",$objet);
			
			if (isset($_POST['idExist']) && !empty($_POST['idExist'])){
				$sql_update = "Update actualites set titre = '".$title. "',  objet = '".$objet."', date_creation = sysdate where act_id = ".$_POST['idExist'];				
				$db->query($sql_update);				
				if (!empty($_FILES['image']['tmp_name'])) {
					$name = rand();
					$uploaddir = '../news/img/';
					$path = $_FILES['image']['name'];
					$ext = pathinfo($path, PATHINFO_EXTENSION);
					$uploadfile = $uploaddir . $name . '.'. $ext;

					if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
					} else {
					}
						
					$sql_update = "Update actualites set image = '".$name. '.'. $ext. "' where act_id = ".$_POST['idExist'];
					$db->query($sql_update);					
				}
			} else {
				$name = rand();
				$uploaddir = '../news/img/';
				$path = $_FILES['image']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				$uploadfile = $uploaddir . $name . '.'. $ext;

				if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
				} else {
				}
			
				/* On sÃlectionne les images dÃjâ¡ prÃsentes */
				$sql_insert = "Insert into actualites values (seq_id.nextval,'".$title."','".$name. '.'. $ext."','".$objet."',sysdate)";			
				$qry_insert = $db->query($sql_insert);		
			}	
		}
		
		if (isset($_GET['delete']))
		{		
			$id = $_GET['delete'];
			$sql_del = "DELETE FROM actualites where act_id = ".$id;
			$news = $db->query($sql_del);
		} else if (isset($_GET['edit']))
		{	
			$id = $_GET['edit'];
			$sql_sel_upd = "SELECT * FROM actualites where act_id = ".$id;
			$existNews = $db->query($sql_sel_upd);
		}
		if (isset($_GET['delete'])) {
			 $url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";	
			 $url = strtok($url, '?');		 		 
			 header( 'Location: '.$url );		 			 
		 }					

	}		
?>

	<body bgcolor="#FFFFFF" text="#000000">
		<?php
			$_GET['menu_selected']=1;		
			include("menu_top_new.php");
		?>	
	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>			
<?php  if ($_SESSION['droit'] == 9){ ?>
<form action="accueil.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="idExist" value="<?php if (isset($existNews)){echo $existNews[0]['act_id'];} ?>">
<table>
<tr>
	<td style="width: 10%;">&nbsp;</td>
	<td>Titre :</td>
	<td >
		<input type="text" name="title" class="form_ediht_Tarifs" size="60" maxlength="60" value="<?php if (isset($existNews)){echo $existNews[0]['titre'];} ?>">
	</td>
	<td style="width: 10%;">&nbsp;</td>	
</tr>
<tr>
	<td>&nbsp;</td>
	<td>Contenu : </td>
	<td><textarea id="contenu" name="objet"><?php if (isset($existNews)){echo $existNews[0]['objet'];} ?></textarea></td>	
	<td>&nbsp;</td>	
</tr>
<tr>
	<td>&nbsp;</td>
	<td>Image (210*300) : </td>
	<td><input type="file" id="image" name="image"></td>
	<td>&nbsp;</td>	
</tr>
<tr>
	<td>&nbsp;</td>
	<td colspan="2" style="text-align: center;"><input style="text-align: centre;" type="submit" value="Valider" class="bn_valider_candidat"></td>
	<td>&nbsp;</td>
</tr>
</table>
</form>
<?php  } ?>
<table>
<tbody>
<?php 

			/* On sÃlectionne les images dÃjâ¡ prÃsentes */
	$sql_sel = "SELECT act_id, titre,image, objet, to_char(date_creation,'DD/MM/YYYY HH24:MI') as dcreation  FROM actualites order by date_creation desc";
	$news = $db->query($sql_sel);					
			
	if (!empty($news)){
		$table = "";		
		foreach ($news as $info) {
//			$date_creation = new DateTime($info['date_creation']);
			$table .= '<tr>';	
			$table .= '<td style="width: 300px;" class="accueil"><img src="../news/img/'.$info['image']. '" alt="" width="300" height="210"></td>';
			$table .= '<td class="accueil"><span class="texteOrangeTitre">'.$info['titre'].'</span> <span style="    float: right;">'.$info['dcreation'].'</span>';
			$table .= '<br>'.$info['objet'].'</td>';
			
			if ($_SESSION['droit'] == 9){
				$table .= '<td><a href="accueil.php?edit='.$info['act_id'].'"><img src="../images/edit.png" style="width: 25px;"></a></td>';
				$table .= '<td><a href="accueil.php?delete='.$info['act_id'].'"><img src="../images/delete.png" style="width: 25px;"></td>';			
			} else {
				$table .= '<td>&nbsp;</td>';
				$table .= '<td>&nbsp;</td>';
			}
			
			$table .= '</tr>';			
			
		}	
		echo $table;
	}
?>
	</tbody></table>
</form>
</p></div>	</article></div>	</div>	</div>	</div>
</body>
<script>
			tinymce.init({
			selector: 'textarea',
			height: 300,
			width:600,
			plugins: [
			  'advlist autolink link image lists charmap print preview hr anchor pagebreak',
			  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			  'table emoticons template paste help'
			],
			toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
			  'bullist numlist outdent indent | link image | print preview media fullpage | ' +
			  'forecolor backcolor emoticons | help',
			menu: {
			  favs: {title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons'}
			},
			menubar: 'favs file edit view insert format tools table help',
			content_css: 'css/content.css'
		  });
					//    '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
			function uploadFile(inp, editor) {
				var input = inp.get(0);
				var data = new FormData();
				data.append('image[file]', input.files[0]);

				$.ajax({
					url: 'uploads/post.php',
					type: 'POST',
					data: data,
					processData: false, // Don't process the files
					contentType: false, // Set content type to false as jQuery will tell the server its a query string request
					success: function(data, textStatus, jqXHR) {
						data = jQuery.parseJSON(data);
						editor.insertContent("<img class='content-img' src='uploads/images/" + data.location + "'/>");
					},
					error: function(jqXHR, textStatus, errorThrown) {
						if(jqXHR.responseText) {
							errors = JSON.parse(jqXHR.responseText).errors
							alert('Error uploading image: ' + errors.join(", ") + '. Make sure the file is an image and has extension jpg/jpeg/png.');
						}
					}
				});
			}
</script>
</html>
	<?php
}else{
	include('no_acces.php');
}
?>

