<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	/* On récupère le nom du graphe voulu */
	$sql_info_graphe = "SELECT GRAPHE_NOM,CODE_LIBELLE FROM GRAPHE,CODE WHERE GRAPHE.GRAPHE_TYPE_GRAPHE_CODE_ID=CODE.CODE_ID AND GRAPHE_ID='".txt_db(intval($_GET['grapheid']))."'";
	$qry_info_graphe = $db->query($sql_info_graphe);

	
	if (is_array($qry_info_graphe)){
	
		if ($_GET['gquestid']>0){
	
			/* On sélectionne tous les libellés pour le graphe/quest sélectionné */
			$sql_sel_info	= "SELECT * FROM GRAPHE_A_QUEST WHERE GRAPHE_A_QUEST_ID='".txt_db(intval($_GET['gquestid']))."'";
			$qry_sel_info	= $db->query($sql_sel_info);
			
			/* On sélectionne le nom du questionnaire duquel dépend le graphe */
			$sql_sel_quest0	= "SELECT QUEST_NOM FROM QUESTIONNAIRE WHERE QUESTIONNAIRE.QUEST_ID='".$qry_sel_info[0]['quest_id']."'";
			$qry_sel_quest0	= $db->query($sql_sel_quest0);
						
			/* On fait la liste de tous les questionnaires de la base de données */
			$sql_type_profil0 = "SELECT CODE_LIBELLE FROM CODE WHERE CODE_TABLE='TYPE_PROFIL' AND CODE_ID='".$qry_sel_info[0]['type_profil_code_id']."'";
			$qry_type_profil0 = $db->query($sql_type_profil0);	
			
			/* On sélectionne tous les libellés pour le graphe/quest sélectionné */
			$sql_sel_lib	= "SELECT * FROM GRAPHE_A_LIB WHERE GRAPHE_A_QUEST_ID='".txt_db(intval($_GET['gquestid']))."'";
			$qry_sel_lib	= $db->query($sql_sel_lib);
	
			
			/* Si le formulaire est posté */
			if ($_POST['select_type_profil']>0){

				/* On insert les info pour le graphe associé au questionnaire */
				$sql_up_quest		= "UPDATE GRAPHE_A_QUEST SET QUEST_ID=".intval($_POST['select_quest']).", TYPE_PROFIL_CODE_ID='".txt_db(intval($_POST['select_type_profil']))."', COULEUR='".txt_db($_POST['couleur_fond'])."', STYLE='".txt_db($_POST['select_style'])."' WHERE GRAPHE_A_QUEST_ID='".txt_db(intval($_GET['gquestid']))."'";
				$qry_up_quest		= $db->query($sql_up_quest);
				
				
				/* On gèr les libellés du graphe pour le questionnaire sélectionné */
				if (is_array($_POST['langue'])){
					foreach ($_POST['langue'] as $langue_postee){ // Pour chaque langue postée
						if (trim($_POST[$langue_postee.'_lib']) != ''){ // Si le champ n'est pas vide
							
							/* On vérifie que ce libellé n'existe pas déjà */
							$sql_verif_lib = "SELECT * FROM GRAPHE_A_LIB WHERE GRAPHE_A_QUEST_ID='".txt_db(intval($_GET['gquestid']))."' AND LANG_ID='".txt_db(intval($langue_postee))."'";
							$qry_verif_lib = $db->query($sql_verif_lib);

							if (is_array($qry_verif_lib)){
								/* On modifie le texte existant */
								$sql_up_lib = "UPDATE GRAPHE_A_LIB SET LIBELLE='".txt_db($_POST[$langue_postee.'_lib'])."' WHERE GRAPHE_A_QUEST_ID='".txt_db(intval($_GET['gquestid']))."' AND LANG_ID='".txt_db(intval($langue_postee))."'";
								$qry_up_lib = $db->query($sql_up_lib);
							}else{
								/* On insere le nouveau texte */
								$sql_ins_lib = "INSERT INTO GRAPHE_A_LIB VALUES('".txt_db(intval($_GET['gquestid']))."', '".txt_db(intval($langue_postee))."', '".txt_db($_POST[$langue_postee.'_lib'])."')";
								$qry_ins_lib = $db->query($sql_ins_lib);
							}
							
						}
					}
				}
			
				?>
				<script type="text/javascript">
					window.opener.location.reload(true);
					window.close();
				</script>		
				<?php
			}
		}
		
		/* On fait la liste de tous les questionnaires de la base de données */
		$sql_quest_liste = "SELECT QUEST_NOM, QUEST_ID FROM QUESTIONNAIRE WHERE QUEST_DATE_SUPPRESSION IS NULL ORDER BY QUESTIONNAIRE.QUEST_NOM ASC";
		$qry_quest_liste = $db->query($sql_quest_liste);
		
		/* On fait la liste de tous les questionnaires de la base de données */
		$sql_type_profil_liste = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='TYPE_PROFIL' ORDER BY CODE_LIBELLE ASC";
		$qry_type_profil_liste = $db->query($sql_type_profil_liste);	
	
	?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<script type="text/javascript">
			<!--
			blocolor = true;
			function hexa(couleur){
			if(blocolor)
			document.getElementById('couleur').value = '#'+couleur; // On inscrit dans le champs TEXT la valeur HEX de la couleur
			}

			function palette_couleurs(){

			// Ouverture du tableau
			var tableau_palette = "<TABLE border='1' cellpadding='0' cellspacing='1'><TR>";

			// Déclaration des variables
			var tabk = new Array('FF','CC','99','66','33','00'); // Tableau principal de couleur
			var tabj = tabk;
			var tabi = new Array('CC', '66', '00'); // Petit tableau principal (colonne de 6 couleurs)
			var tabi2 = new Array('00','33','66','99','CC','FF'); // Tableau principal inversé pour la colone du milieu (colonne de 6 couleurs)
			var color=""; // initialisation de color a vide
			var cmp = 0; // initialisation du compteur a 0

			// Début prog
			for(var k=0;k<6;k++) // Boucle pour les lignes de couleurs
			{
			for(var i=0;i<3;i++) // Boucle pour les colonnes (colonnes de 6 couleurs)
			{
			if (i == 1) // Si on attaque la 2 eme colonne de 6 couleurs
			{
			tabj = tabi2; // on inverse le tableau principale de couleurs
			}
			else // sinon
			{
			tabj = Array('FF','CC','99','66','33','00'); // On remet le tableau par default
			}

			for(var j=0;j<6;j++) // Boucle pour l'affichage couleur par couleur
			{
			color="#"+tabi[i]+tabk[k]+tabj[j]; // concaténation des chaines pour la valeur de la couleur
			// et on affiche la couleur
			tableau_palette += "<TD width='15' height='15' style='border: 1px solid #000000;' bgcolor='"+color+"' onClick=\"hexa('"+tabi[i]+tabk[k]+tabj[j]+"')\" onMouseOver=\"this.style.cursor='pointer'\"></TD>";
			}
			}
			tableau_palette += "</tr>";

			cmp = cmp + 1; // On compte le nombre de ligne faite
			if (cmp == 6) // si on a fait les 6 lignes
			{
			var tabi = new Array('FF', '99', '33'); // on redéfini le nouveau tableau principal (colonne de 6 couleurs)
			var tabk = tabi2; // on re initialise le tableau des lignes
			k = -1; // on défini k à -1 car on a déja fait un passage
			}
			}
			// Fin prog
			tableau_palette += "</TABLE>"; // On ferme le tableau + saut de ligne
			document.getElementById('tab_palette').innerHTML = tableau_palette;
			}
			-->
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#">
		  	<table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_info_graphe[0]['code_libelle'] ?>&nbsp;<?php echo $qry_info_graphe[0]['graphe_nom'] ?>&nbsp;/&nbsp;DONNEE&nbsp;<?php echo $qry_sel_quest0[0]['quest_nom'] ?> - <?php echo $qry_type_profil0[0]['code_libelle'] ?></td>
			</tr>
			</table>
		  <table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="left" style="text-align: left;" class="TX_Tarifs">Modification de donnée</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="1"></td>
			  <td align="center"  height="1" bgcolor="#666666"></td>
			  <td width="14" height="1"></td>
			</tr>
			<tr> 
			  <td width="14" height="1"></td>
			  <td align="center" class="TX" >&nbsp;</td>
			  <td width="14" height="1"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="center" class="TX"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
				  <tr> 
					<td align="left" valign="top" class="TX_Gras"> 1 Questionnaire :</td>
					<td align="left" valign="top" >
					  <select name="select_quest" id="select_quest" class="form_ediht_Tarifs" onChange="select_lang_list(document.getElementById('select_quest').options[document.getElementById('select_quest').selectedIndex].value);">
						<?php
						if (is_array($qry_quest_liste)){ // On fait la liste des questionnaires
							foreach($qry_quest_liste as $quest_liste){
								unset($selected_Q);
								if ($qry_sel_info[0]['quest_id'] == $quest_liste['quest_id']){
									$selected_Q = ' selected="selected"';
								}
								echo '<option value="'.$quest_liste['quest_id'].'"'.$selected_Q.'>'.$quest_liste['quest_nom'].'</option>';
							}
						}else{
							echo '<option value="0">Aucun Questionnaire trouvé.</option>';
						}
						?>
					  </select>
					</td>
				  </tr>
				  <tr> 
					<td align="left" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="left" valign="top" class="TX_Gras"> 2 Type de profil :</td>
					<td align="left" valign="top" >
					  <select name="select_type_profil" class="form_ediht_Tarifs">
						<?php
						if (is_array($qry_type_profil_liste)){ // On fait la liste des questionnaires
							foreach($qry_type_profil_liste as $quest_type_profil){
								unset($selected_P);
								if ($qry_sel_info[0]['type_profil_code_id'] == $quest_type_profil['code_id']){
									$selected_P = ' selected="selected"';
								}
								echo '<option value="'.$quest_type_profil['code_id'].'"'.$selected_P.'>'.$quest_type_profil['code_libelle'].'</option>';
							}
						}else{
							echo '<option value="0">Aucun Profil trouvé.</option>';
						}
						?>
					  </select>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="left" class="TX_Gras"> 3 Apparence :</td>
					<td align="left" > </td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">
						<table cellpadding="0" cellspacing="0">
						<tr><td class="TX" valign="top">
								Couleur : <input type="text" name="couleur_fond" id="couleur" size="7" maxlength="7" class="form_ediht_Tarifs" value="<?php echo $qry_sel_info[0]['couleur'] ?>"><div width="1%" id="tab_palette" style="display: inline;"></div>
								<script language="JavaScript">
									  palette_couleurs();
								</script>
							</td>
							</tr>
						</table>
					</td>
				   </tr>
				   <?php
				  if ($_GET['type']=='36') // si courbe
				  {
				  ?>
				   <tr>
						<td align="center" valign="top">&nbsp;</td>
						<td class="TX" valign="top">
							<table cellpadding="0" cellspacing="0">
							<tr><td class="TX" valign="top">Style 
							  : 
							  <select name="select_style" class="form_ediht_Tarifs">
								<?php
								if ($qry_sel_info[0]['style'] == 0){
									$selected_style1 = ' selected="selected"';
									$selected_style2 = '';
								}else{
									$selected_style1 = '';
									$selected_style2 = ' selected="selected"';
								}
								?>
								<option value="0"<?php echo $selected_style1 ?>>Pointill&eacute;</option>
								<option value="1"<?php echo $selected_style2 ?>>Continu</option>
							  </select>
							  &nbsp;&nbsp;&nbsp;
							  </td>
							</tr></table>
						</td>
				  </tr>
				  <?php
				  }
				  ?>
				  <tr> 
					<td align="left" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td align="left" valign="top" class="TX_Gras"> 4 Libell&eacute; d'affichage : </td>
					<td align="left" valign="top" > </td>
				  </tr>
				  <tr> 
					<td colspan="2" align="left" valign="top">
						<?php
						/* On fait la liste des langues disponibles */
						$sql_lang = "SELECT LANGUE.LANG_ID, LANGUE.LANG_LIBELLE FROM QUEST_A_LANG, LANGUE WHERE QUEST_A_LANG.LANG_ID=LANGUE.LANG_ID AND QUEST_A_LANG.QUEST_ID='".$qry_sel_info[0]['quest_id']."' ORDER BY LANGUE.LANG_LIBELLE ASC";
						$qry_lang = $db->query($sql_lang);	
						?>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<?php
						if (is_array($qry_lang)){
							foreach($qry_lang as $lang){
								$sql_value_lib = "SELECT * FROM GRAPHE_A_LIB WHERE GRAPHE_A_QUEST_ID='".txt_db(intval($_GET['gquestid']))."' AND LANG_ID='".txt_db(intval($lang['lang_id']))."'";
								$qry_value_lib = $db->query($sql_value_lib);
								echo '<tr> 
								  <td class="TX">'. $lang['lang_libelle'] .' :</td>
								  <td class="TX">
									<input type="hidden" name="langue[]" value="'. $lang['lang_id'] .'">
									<input type="text" name="'. $lang['lang_id'] .'_lib" class="form_ediht_Tarifs" size="100" value="'.$qry_value_lib[0]['libelle'].'">
								  </td>
								</tr>';
							}
						}
						?>
						</table>
	
	
					</td>
				  </tr>
				  <tr> 
					<td align="center" valign="top">&nbsp;</td>
					<td align="left" valign="top">&nbsp;</td>
				  </tr>
				  <tr> 
					<td colspan="2" bgcolor="#CCCCCC" height="1" valign="top" align="center"></td>
				  </tr>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
		  <br>
		   <p style="text-align:center"><input type="submit" name="Submit" value="VALIDER" class="BN">
			 </p>
		</form>
		</body>
		</html>
		<?php
	}else{
		include('../config/lib/lang.php');
		echo $t_acn_1;
	}
}else{
	include('no_acces.php');
}
?>