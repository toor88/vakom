<?phpPHP
if ($_POST['ajout']) {
	for ($i=0; $i < count($_POST['id_configuration']); $i++) {
		$db->setConfigurationChapitre(
			$_POST['id_configuration'][$i], 
			$_POST['id_garantie'],
			$_POST['id_chapitre'][$i],
			$_POST['mini_chapitre'][$i],
			$_POST['maxi_chapitre'][$i],
			$_POST['tranche_minimum'][$i],
			$_POST['tranche_maximum'][$i]
		);
	}
	
	echo "<a href=''>Retourner sur l'ï¿½dition des garanties</a>";
	echo "<SCRIPT LANGUAGE=\"JavaScript\">document.location.href=\"index.php?c=1&p=1&o=1\"</SCRIPT>";
}

if ($_POST['maj_statut']) {
	$garantie = $db->getGaranties($_SESSION['id_garantie']);
	$db->setGarantie($garantie[0]['id'], $garantie[0]['label'], $garantie[0]['id_societe'], $_POST['college']);
	
	$_SESSION['college'] = $_POST['college'];
	
	echo "<a href=''>Retourner sur l'ï¿½dition des garanties</a>";
	echo "<SCRIPT LANGUAGE=\"JavaScript\">document.location.href=\"index.php?c=1&p=1&o=1\"</SCRIPT>";
}

if ($_POST['maj_publication']) {
	$db->setPublication($_POST['publication'],$_SESSION['id_garantie']);
	$db->setDatePublication($_POST['date_publication'],$_SESSION['id_garantie']);
	$_SESSION['publication'] = $_POST['publication'];
	
	echo "<a href=''>Retourner sur l'edition des garanties</a>";
	echo "<SCRIPT LANGUAGE=\"JavaScript\">document.location.href=\"index.php?c=1&p=1&o=1\"</SCRIPT>";
}

?>
<form id="form_param_general" name="form_param_general" method="post" action="index.php?param_general=1">
<table width="100%" cellpadding="2" cellspacing="2" border="0" bgcolor="#999999">
<?phpPHP
$garanties[0] = $_SESSION['id_garantie'];
$j=0;
foreach ($garanties AS $garantie)
{
	$chapitres = $db->getChapitre();
	$i = 0;
	foreach ($chapitres AS $chapitre)
	{
		$configuration = $db->getConfigurationChapitre($_SESSION['id_garantie'], $chapitre['id']);
		echo "<tr><td bgcolor='#CCCCCC' style='font-size:12px' width='200px'><strong>";
		echo $chapitre['label'];
		echo "</strong></td><td bgcolor='#FFFFFF' style='font-size:10px'>";
		echo "<input name='id_configuration[]' type='hidden' id='id_configuration[]' value='".$configuration[0]['id']."'>";
		echo "<input name='id_chapitre[]' type='hidden' id='id_chapitre[]' value='".$chapitre['id']."'>";
		echo "<input name='id_garantie' type='hidden' id='id_garantie' value='".$_SESSION['id_garantie']."'>";
		echo "Minimum <input name='mini_chapitre[]' type='text' class='formulaireCadreNoir' id='mini_chapitre[]' size='10' value='".$configuration[0]['minimum']."'>";
		echo " % ".makeSelect('tranche_minimum[]', $db->getTranches(), $configuration[0]['id_tranche_minimum'])."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "Maximum <input name='maxi_chapitre[]' type='text' class='formulaireCadreNoir' id='maxi_chapitre[]' size='10' value='".$configuration[0]['maximum']."'>";
		echo " % ".makeSelect('tranche_maximum[]', $db->getTranches(), $configuration[0]['id_tranche_maximum'])."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "</td></tr>";
		$i++;
	}
}
?>
</table>
<span class="ArialNoir12Normal">
<input type="submit" name="ajout" value="Enregister" class="bnOkrouge">
</span>
</form>


<form id="form_set_statut" name="form_set_statut" method="post" action="index.php?param_general=1">
<table width="100%" cellpadding="2" cellspacing="2" border="0" bgcolor="#999999">
<?phpPHP
		echo "<tr><td bgcolor='#CCCCCC' style='font-size:12px' width='200px'><strong>";
		echo "College par defaut&nbsp;";
		echo "</strong></td><td bgcolor='#FFFFFF' style='font-size:10px'>";
		echo makeSelect('college', $db->getStatut(), $_SESSION['college']);
		echo "</td></tr>";
?>
</table>
<span class="ArialNoir12Normal">
<input type="submit" name="maj_statut" value="Enregister" class="bnOkrouge">
</span>
</form>

<form id="form_set_publication" name="form_set_publication" method="post" action="index.php?param_general=1">
<table width="100%" cellpadding="2" cellspacing="2" border="0" bgcolor="#999999">
<?phpPHP
		echo "<tr><td bgcolor='#CCCCCC' style='font-size:12px' width='200px'><strong>";
		echo "Etat de la publication&nbsp;";
		echo "</strong></td><td bgcolor='#FFFFFF' style='font-size:10px'>";
		$liste_publication[0]["label"] = "Publie";
		$liste_publication[0]["id"] = "Publie";
		$liste_publication[1]["label"] = "Non-Publie";
		$liste_publication[1]["id"] = "Non-Publie";
		echo makeSelect('publication', $liste_publication, $_SESSION['publication']);
		$garanties = $db->getGaranties($_SESSION['id_garantie']);
		foreach ($garanties AS $garantie) {
		echo "&nbsp;Date de fin de publication (DD/MM/YY) <input name='date_publication' type='text' class='formulaireCadreNoir' id='date_publication' size='10' value='".$garantie['date_debut']."'>";
		}
		echo "</td></tr>";
?>
</table>
<span class="ArialNoir12Normal">
<input type="submit" name="maj_publication" value="Enregister" class="bnOkrouge">
</span>
</form>