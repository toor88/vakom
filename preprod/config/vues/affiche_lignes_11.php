	<table width="100%" border="0" cellspacing="1" cellpadding="0" height="25" class="ArialNoir">
		<tr align="center" bgcolor="#FFCC33">
			<td>Ordre</td>
			<td>Option</td>
			<td>Statut</td>
			<td>Situation de famille</td>
			<td width='5%'>Nb enfants</td>
			<td>Libellé garantie</td>
			<td>Formules</td>
			<td>Minimum</td>
			<td>Maximum</td>
			<td>Libellé de la formule</td>
			<td>&nbsp;</td>
		</tr>
	<?phpPHP
	$result = $db->getLigneGarantie('', $_SESSION['id_garantie'], $_GET['p']);
	$i=0;
	if ($result) {
		foreach ($result['ligne'] AS $ligne) {
			
			//Récupération des formules liées à la ligne de garantie
			unset($affiche_formule);
			$formules = $db->getFormule($ligne['id']);
			$i=0;
			if($formules) {
				foreach ($formules as $formule) {
					$affiche_formule[$i]  = $formule['taux']."% de ";
					$nom_tranche = $db->getTranches($formule['id_tranche']);
					$affiche_formule[$i] .= $nom_tranche['0']['label'];
					if ($formule['condition'] == 'on') $affiche_formule[$i] .= " moins SS";
					
					$minimum = $db->getMinimumMat($ligne['id'], $formule['id']);
					$nom_tranche = $db->getTranches($minimum[0]['id_tranche']);
					if ($minimum) $affiche_minimum[$i] = $minimum[0]['taux']."% de ".$nom_tranche['0']['label'];
					
					$maximum = $db->getMaximumMat($ligne['id'], $formule['id']);
					$nom_tranche = $db->getTranches($maximum[0]['id_tranche']);
					if ($maximum) $affiche_maximum[$i] = $maximum[0]['taux']."% de ".$nom_tranche['0']['label'];
					
					$i++;
				}
			}
			
			//Récupération des minimum liées à la ligne de garantie
			// unset($affiche_minimum);
			// $minimums = $db->getMinimum($ligne['id']);
			// $i=0;
			// if($minimums) {
				// foreach ($minimums as $minimum) {
					// $affiche_minimum[$i]  = $minimum['taux']."% de ";
					// $nom_tranche = $db->getTranches($minimum['id_tranche']);
					// $affiche_minimum[$i] .= $nom_tranche['0']['label'];
					// $i++;
				// }
			// }
			
			//Récupération des maximum liées à la ligne de garantie
			// unset($affiche_maximum);
			// $maximums = $db->getMaximum($ligne['id']);
			// $i=0;
			// if($maximums) {
				// foreach ($maximums as $maximum) {
					// $affiche_maximum[$i]  = $maximum['taux']."% de ";
					// $nom_tranche = $db->getTranches($maximum['id_tranche']);
					// $affiche_maximum[$i] .= $nom_tranche['0']['label'];
					// $i++;
				// }
			// }
			
			$array_situation_famille = explode('##', $ligne['id_situation_famille']);
			$select_statut = makeSelect('statut', $db->getStatut(), $ligne['id_statut'], 1);
			$select_situation_famille = makeSelect('situation_famille', $db->getSituationFamille(), $ligne['id_situation_famille'], 1);
			echo <<<EOT
			<tr align='center' bgcolor='#FFFFCC'>
				<td>{$ligne['ordre']}</td>
				<td width='5%'>{$ligne['id_option_parametrage']}</td>
				<td>{$select_statut}</td>
				<td>{$select_situation_famille}</td>
				<td width='5%'>{$ligne['nb_enfant']}</td>
				<td>{$ligne['label']}</td>
				<td>
				{$affiche_formule['0']}<br>
				{$affiche_formule['1']}<br>
				{$affiche_formule['2']}<br>
				{$affiche_formule['3']}<br>
				{$affiche_formule['4']}<br>
				</td>
				<td>
				{$affiche_minimum['0']}<br>
				{$affiche_minimum['1']}<br>
				{$affiche_minimum['2']}<br>
				{$affiche_minimum['3']}<br>
				{$affiche_minimum['4']}<br>
				</td>
				<td>
				{$affiche_maximum['0']}<br>
				{$affiche_maximum['1']}<br>
				{$affiche_maximum['2']}<br>
				{$affiche_maximum['3']}<br>
				{$affiche_maximum['4']}<br>
				</td>
				<td>{$ligne['label_formule']}</td>
				<td><a href='index.php?c={$c}&p={$p}&o=1&edit={$ligne['id']}'>Editer</a><br><a href='index.php?c={$c}&p={$p}&o=1&del={$ligne['id']}'>Effacer</a></td>
			</tr>
EOT;
	unset($affiche_minimum);
	unset($affiche_maximum);
		}
	}
	?>  