<?php

class db_ascore_garanties extends db_oracle
{
	function getTables()
	{
		$this->query = "SELECT * FROM USER_TABLES WHERE TABLE_NAME LIKE 'GAR_%'";
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['label']    = stripslashes($this->tab_result['TABLE_NAME']);
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getColumns($table_name)
	{
		$this->query = "SELECT * from ".$table_name;
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['label']    = $this->tab_result['id'];
			$i++;
		}
		$this->finish();
		return $this->query;
	}
	
	function getChapitre($id_chapitre='')
	{
		if (empty($id_chapitre)) {
			$this->query = "select * from gar_chapitre";
		} else {
			$this->query = "select * from gar_chapitre where id='".$id_chapitre."'";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    = $this->tab_result['ID'];
			$result[$i]['label'] = stripslashes($this->tab_result['LABEL']);
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getUtilisateurs($id_utilisateur='')
	{
		if (empty($id_chapitre)) {
			$this->query = "select * from gar_utilisateurs";
		} else {
			$this->query = "select * from gar_utilisateurs where id='".$id_utilisateur."'";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    	= $this->tab_result['ID'];
			$result[$i]['label'] 	= stripslashes($this->tab_result['LABEL']);
			$result[$i]['droits']	= stripslashes($this->tab_result['DROITS']);
			$result[$i]['login'] 	= stripslashes($this->tab_result['LOGIN']);
			$result[$i]['mdp'] 		= stripslashes($this->tab_result['MDP']);
			$result[$i]['date_conn']= stripslashes($this->tab_result['DATE_CONN']);
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getUtilisateur_by_login($login)
	{
		$this->query = "select * from gar_utilisateurs where login = '".$login."'";
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    	= $this->tab_result['ID'];
			$result[$i]['label'] 	= stripslashes($this->tab_result['LABEL']);
			$result[$i]['droits']	= stripslashes($this->tab_result['DROITS']);
			$result[$i]['login'] 	= stripslashes($this->tab_result['LOGIN']);
			$result[$i]['mdp'] 		= stripslashes($this->tab_result['MDP']);
			$result[$i]['date_conn']= stripslashes($this->tab_result['DATE_CONN']);
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function setUtilisateur($id_utilisateur, $mdp)
	{
		$this->query = "update gar_utilisateurs set DATE_CONN ='".date('d/m/Y')."', MDP ='".$mdp."' where ID ='".$id_utilisateur."'";
		$this->check_exec();
		$this->finish();
		return $this->query;
	}
	
	function getOption($id_option='')
	{
		if (empty($id_option)) {
			$this->query = "select * from gar_option_parametrage order by id";
		} else {
			$this->query = "select * from gar_option_parametrage where id='".$id_option."' order by id";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    = $this->tab_result['ID'];
			$result[$i]['label'] = stripslashes($this->tab_result['LABEL']);
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getParametrage($id_parametrage='')
	{
		if (empty($id_parametrage)) {
			$this->query = "select * from gar_parametrage order by id";
		} else {
			$this->query = "select * from gar_parametrage where id='".$id_parametrage."'";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    = $this->tab_result['ID'];
			$result[$i]['label'] = stripslashes($this->tab_result['LABEL']);
			$result[$i]['id_chapitre'] = $this->tab_result['ID_CHAPITRE'];
			$result[$i]['label_colonnes'] = $this->tab_result['LABEL_COLONNES'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getParametrageBy_id_chapitre($id_chapitre)
	{
		$this->query = "select * from gar_parametrage where id_chapitre='".$id_chapitre."' order by id";
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    = $this->tab_result['ID'];
			$result[$i]['label'] = stripslashes($this->tab_result['LABEL']);
			$result[$i]['id_chapitre'] = $this->tab_result['ID_CHAPITRE'];
			$result[$i]['label_colonnes'] = $this->tab_result['LABEL_COLONNES'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getSituationFamille($id_situation_famille='')
	{
		if (empty($id_situation_famille)) {
			$this->query = "select * from gar_situation_famille";
		} else {
			$this->query = "select * from gar_situation_famille where id='".$id_situation_famille."'";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    = $this->tab_result['ID'];
			$result[$i]['label'] = stripslashes($this->tab_result['LABEL']);
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getStatut($id_statut='')
	{
		if (empty($id_statut)) {
			$this->query = "select * from gar_statut order by id";
		} else {
			$this->query = "select * from gar_statut where id='".$id_statut."' order by id";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    = $this->tab_result['ID'];
			$result[$i]['label'] = stripslashes($this->tab_result['LABEL']);
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getTranches($id_tranche='')
	{
		if (empty($id_tranche)) {
			$this->query = "select * from gar_tranches order by id";
		} else {
			$this->query = "select * from gar_tranches where id='".$id_tranche."' order by id";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    = $this->tab_result['ID'];
			$result[$i]['label'] = stripslashes($this->tab_result['LABEL']);
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getSocietes($id_societe='')
	{
		if (empty($id_societe)) {
			$this->query = "select * from adh_societe order by groupe";
		} else {
			$this->query = "select * from adh_societe where soc_code='".$id_societe."' order by groupe";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    = $this->tab_result['SOC_CODE'];
			$result[$i]['label'] = stripslashes($this->tab_result['GROUPE'])." - ".stripslashes($this->tab_result['SOC_LIBELLEFRANCAIS']);
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getGaranties($id_garantie='')
	{
		if (empty($id_garantie)) {
			$this->query = "select * from gar_garantie order by label";
		} else {
			$this->query = "select * from gar_garantie where id='".$id_garantie."' order by label";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    		= $this->tab_result['ID'];
			$result[$i]['label'] 		= stripslashes($this->tab_result['LABEL']);
			$result[$i]['id_societe']   = $this->tab_result['ID_SOCIETE'];
			$result[$i]['college']   	= $this->tab_result['COLLEGE'];
			$result[$i]['publication']= $this->tab_result['PUBLICATION'];
			$result[$i]['date_debut']= $this->tab_result['DATE_DEBUT'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getNextGarantie() {
		$this->query = "select seq_garantie.nextval AS seqgar from dual";
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['seqgar']    		= $this->tab_result['SEQGAR'];
			$i++;
		}
		$this->finish();
		return $result[0]['seqgar'];
	}
	
	function setGarantie($id_garantie='', $label, $id_societe, $college="", $publication="")
	{
		if (empty($id_garantie)) {
			//$id_garantie = getNextRow('GAR_GARANTIE');
			$id_garantie = $this->getNextGarantie();
			$publication = "Non-Publi";
			$this->query = "insert into gar_garantie (ID, LABEL, ID_SOCIETE, COLLEGE, PUBLICATION) VALUES (".$id_garantie.", '".ereg_replace("'","''", $label)."', '".$id_societe."', '".$college."', '".$publication."')";
		} else {
			$this->query = "update gar_garantie set ID ='".$id_garantie."', LABEL = '".ereg_replace("'","''", $label)."', ID_SOCIETE = '".$id_societe."', COLLEGE = '".$college."', PUBLICATION = '".$publication."' where ID ='".$id_garantie."'";
		}
		$this->check_exec();
		$this->finish();
		//return $this->query;
		return $id_garantie;
	}
	
	////////////////////////
	function setGarantieSuivi($id_garantie, $id_utilisateur)
	{
		$this->query = "insert into gar_garantie_suivi (ID, ID_GARANTIE, ID_UTILISATEUR, DATE_CONNEXION) VALUES ('".getNextRow('GAR_GARANTIE_SUIVI')."', '".$id_garantie."', '".$id_utilisateur."', TO_DATE('".date('d/m/y')."', 'DD/MM/RR'))";
		$this->check_exec();
		$this->finish();
		//return $this->query;
		return $id_garantie;
	}
	////////////////////////
	
	
	function setPublication($publication, $id_garantie)
	{
		$this->query = "update gar_garantie set PUBLICATION ='".$publication."' where ID ='".$id_garantie."'";
		$this->check_exec();
		$this->finish();
	}
	
	function setDatePublication($date_publication, $id_garantie)
	{
		$this->query = "update gar_garantie set DATE_DEBUT =to_date('".$date_publication."','DD/MM/YY') where ID ='".$id_garantie."'";
		$this->check_exec();
		$this->finish();
	}

	function getLigneGarantie($id_ligne_garantie='', $id_garantie='', $id_parametrage='')
	{
		if (empty($id_ligne_garantie)) {
			$this->query = "select * from gar_ligne_garantie where id_garantie ='".$id_garantie."' AND id_parametrage = '".$id_parametrage."' order by ordre";
		} else {
			$this->query = "select * from gar_ligne_garantie where id='".$id_ligne_garantie."' order by ordre";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result['ligne'][$i]['id']    							= $this->tab_result['ID'];
			$result['ligne'][$i]['label'] 							= stripslashes($this->tab_result['LABEL']);
			$result['ligne'][$i]['label_formule'] 					= stripslashes($this->tab_result['LABEL_FORMULE']);
			$result['ligne'][$i]['id_garantie'] 					= $this->tab_result['ID_GARANTIE'];
			$result['ligne'][$i]['id_parametrage'] 					= $this->tab_result['ID_PARAMETRAGE'];
			$result['ligne'][$i]['id_option_parametrage'] 			= $this->tab_result['ID_OPTION_PARAMETRAGE'];
			$result['ligne'][$i]['id_statut'] 						= $this->tab_result['ID_STATUT'];
			$result['ligne'][$i]['id_situation_famille'] 			= $this->tab_result['ID_SITUATION_FAMILLE'];
			$result['ligne'][$i]['nb_enfant'] 						= $this->tab_result['NB_ENFANT'];
			$result['ligne'][$i]['nb_charge'] 						= $this->tab_result['NB_CHARGE'];
			$result['ligne'][$i]['ordre'] 							= $this->tab_result['ORDRE'];
			$result['ligne'][$i]['cumuler'] 						= $this->tab_result['CUMULER'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function setLigneGarantie($id_ligne_garantie = '', $label, $label_formule, $id_garantie, $id_parametrage, $id_option, $statut, $situation_famille, $nb_enfant, $nb_charge, $ordre, $cumuler='')
	{
		if (empty($id_ligne_garantie)) {
			$id_ligne_garantie = getNextRow('GAR_LIGNE_GARANTIE');
			$this->query = "INSERT INTO GAR_LIGNE_GARANTIE (ID, LABEL, LABEL_FORMULE, ID_GARANTIE, ID_PARAMETRAGE, ID_OPTION_PARAMETRAGE, ID_STATUT, ID_SITUATION_FAMILLE, NB_ENFANT, NB_CHARGE, ORDRE, CUMULER) VALUES (".$id_ligne_garantie.", '".ereg_replace("'","''", $label)."', '".ereg_replace("'","''", $label_formule)."','".$id_garantie."', '".$id_parametrage."', '".$id_option."', '".$statut."', '".$situation_famille."', '".$nb_enfant."', '".$nb_charge."', '".$ordre."', '".$cumuler."')";
		} else {
			$this->query = "UPDATE GAR_LIGNE_GARANTIE SET LABEL = '".ereg_replace("'","''", $label)."', LABEL_FORMULE = '".ereg_replace("'","''", $label_formule)."', ID_GARANTIE = '".$id_garantie."', ID_PARAMETRAGE = '".$id_parametrage."', ID_OPTION_PARAMETRAGE = '".$id_option."', ID_STATUT = '".$statut."', ID_SITUATION_FAMILLE = '".$situation_famille."', NB_ENFANT = '".$nb_enfant."', NB_CHARGE = '".$nb_charge."', ORDRE = '".$ordre."' , CUMULER = '".$cumuler."' WHERE ID = '".$id_ligne_garantie."'";
		}
		$this->check_exec();
		$this->finish();
		return $id_ligne_garantie;
	}
	
	function updateLigneGarantie() {
		$this->query = "update gar_ligne_garantie set fl_graph=0 where id not in (select distinct id_ligne_garantie from gar_formule)";
		$this->check_exec();
		$this->finish();
	}
	
	function delLigneGarantie($id)
	{
		$this->query = "DELETE FROM GAR_LIGNE_GARANTIE WHERE ID='".$id."'";
		$this->check_exec();
		$this->finish();
		$this->query = "DELETE FROM GAR_FORMULE WHERE ID_LIGNE_GARANTIE='".$id."'";
		$this->check_exec();
		$this->finish();
		$this->query = "DELETE FROM GAR_MINIMUM WHERE ID_LIGNE_GARANTIE='".$id."'";
		$this->check_exec();
		$this->finish();
		$this->query = "DELETE FROM GAR_MAXIMUM WHERE ID_LIGNE_GARANTIE='".$id."'";
		$this->check_exec();
		$this->finish();
	}
	
	function del($id, $table) {
		$this->query = "DELETE FROM ".$table." WHERE ID ='".$id."'";
		$this->check_exec();
		$this->finish();
	}
	
	function delFormule_By_id_ligne_garantie($id_ligne_garantie) {
		$this->query = "DELETE FROM GAR_FORMULE WHERE ID_LIGNE_GARANTIE ='".$id_ligne_garantie."'";
		$this->check_exec();
		$this->finish();
	}
	
	function delMinimum_By_id_ligne_garantie($id_ligne_garantie) {
		$this->query = "DELETE FROM GAR_MINIMUM WHERE ID_LIGNE_GARANTIE ='".$id_ligne_garantie."'";
		$this->check_exec();
		$this->finish();
	}
	
	function delMaximum_By_id_ligne_garantie($id_ligne_garantie) {
		$this->query = "DELETE FROM GAR_MAXIMUM WHERE ID_LIGNE_GARANTIE ='".$id_ligne_garantie."'";
		$this->check_exec();
		$this->finish();
	}
	
	function getFormule($id_ligne_garantie, $id_formule='')
	{
		if (empty($id_formule)) {
			$this->query = "select * from gar_formule where id_ligne_garantie='".$id_ligne_garantie."' order by id";
		} else {
			$this->query = "select * from gar_formule where id='".$id_formule."' order by id";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    				= $this->tab_result['ID'];
			$result[$i]['id_ligne_garantie']    = $this->tab_result['ID_LIGNE_GARANTIE'];
			$result[$i]['taux'] 				= $this->tab_result['TAUX'];
			$result[$i]['id_tranche'] 			= $this->tab_result['ID_TRANCHE'];
			$result[$i]['condition'] 			= $this->tab_result['CONDITION'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function setFormule($id_formule='', $id_ligne_garantie, $taux, $id_tranche, $condition)
	{
		if (empty($id_formule)) {
			$id_formule = getNextRow('GAR_FORMULE');
			$this->query = "insert into gar_formule (ID, ID_LIGNE_GARANTIE, TAUX, ID_TRANCHE, CONDITION) VALUES (".$id_formule.", '".$id_ligne_garantie."', '".$taux."', '".$id_tranche."', '".$condition."')";
		} else {
			$this->query = "update gar_formule set ID ='".$id_formule."', ID_LIGNE_GARANTIE = '".$id_ligne_garantie."', TAUX = '".$taux."', ID_TRANCHE = '".$id_tranche."', CONDITION = '".$condition."' where ID ='".$id_formule."'";
		}
		$this->check_exec();
		$this->finish();
		return $id_formule;
	}
	
	function setMinimum($id_minimum='', $id_ligne_garantie, $taux, $id_tranche, $id_formule)
	{
		if (empty($id_minimum)) {
			$this->query = "insert into gar_minimum (ID, ID_LIGNE_GARANTIE, TAUX, ID_TRANCHE, ID_FORMULE) VALUES (".getNextRow('GAR_MINIMUM').", '".$id_ligne_garantie."', '".$taux."', '".$id_tranche."', '".$id_formule."')";
		} else {
			$this->query = "update gar_minimum set ID ='".$id_minimum."', ID_LIGNE_GARANTIE = '".$id_ligne_garantie."', TAUX = '".$taux."', ID_TRANCHE = '".$id_tranche."', ID_FORMULE = '".$id_formule."' where ID ='".$id_minimum."'";
		}
		$this->check_exec();
		$this->finish();
		return $this->query;
	}
	
	function setMaximum($id_maximum='', $id_ligne_garantie, $taux, $id_tranche, $id_formule)
	{
		if (empty($id_maximum)) {
			$this->query = "insert into gar_maximum (ID, ID_LIGNE_GARANTIE, TAUX, ID_TRANCHE, ID_FORMULE) VALUES (".getNextRow('GAR_MAXIMUM').", '".$id_ligne_garantie."', '".$taux."', '".$id_tranche."', '".$id_formule."')";
		} else {
			$this->query = "update gar_maximum set ID ='".$id_maximum."', ID_LIGNE_GARANTIE = '".$id_ligne_garantie."', TAUX = '".$taux."', ID_TRANCHE = '".$id_tranche."', ID_FORMULE = '".$id_formule."' where ID ='".$id_maximum."'";
		}
		$this->check_exec();
		$this->finish();
		return $this->query;
	}
	
	function getAllLignes()
	{
		$this->query = "select DISTINCT LABEL from gar_ligne_garantie where id_parametrage != '10' order by label";
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    = $this->tab_result['ID'];
			$result[$i]['label'] = stripslashes($this->tab_result['LABEL']);
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function setConfigurationChapitre($id_configuration='', $id_garantie, $id_chapitre, $minimum, $maximum, $id_tranche_minimum, $id_tranche_maximum)
	{
		if (empty($id_configuration)) {
			$this->query = "insert into gar_chapitre_label (ID, ID_GARANTIE, ID_CHAPITRE, MINIMUM, MAXIMUM, ID_TRANCHE_MINIMUM, ID_TRANCHE_MAXIMUM) VALUES (".getNextRow('GAR_CHAPITRE_LABEL').", '".$id_garantie."', '".$id_chapitre."', '".$minimum."', '".$maximum."', '".$id_tranche_minimum."', '".$id_tranche_maximum."')";
		} else {
			$this->query = "UPDATE gar_chapitre_label SET ID = '".$id_configuration."', ID_GARANTIE = '".$id_garantie."', ID_CHAPITRE = '".$id_chapitre."', MINIMUM = '".$minimum."', MAXIMUM = '".$maximum."', ID_TRANCHE_MINIMUM = '".$id_tranche_minimum."', ID_TRANCHE_MAXIMUM = '".$id_tranche_maximum."' WHERE ID = '".$id_configuration."'";
		}
		$this->check_exec();
		$this->finish();
		return $this->query;
	}
	
	function getConfigurationChapitre($id_garantie, $id_chapitre='')
	{
		if (empty($id_chapitre)) {
			$this->query = "select * from gar_chapitre_label where id_garantie='".$id_garantie."' order by id_chapitre";
		} else {
			$this->query = "select * from gar_chapitre_label where id_chapitre='".$id_chapitre."' AND id_garantie='".$id_garantie."' order by id_chapitre";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    				= $this->tab_result['ID'];
			$result[$i]['id_garantie']    		= $this->tab_result['ID_GARANTIE'];
			$result[$i]['id_chapitre'] 			= $this->tab_result['ID_CHAPITRE'];
			$result[$i]['minimum'] 				= $this->tab_result['MINIMUM'];
			$result[$i]['maximum'] 				= $this->tab_result['MAXIMUM'];
			$result[$i]['id_tranche_minimum'] 	= $this->tab_result['ID_TRANCHE_MINIMUM'];
			$result[$i]['id_tranche_maximum'] 	= $this->tab_result['ID_TRANCHE_MAXIMUM'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function setConfigurationParametrage($id_configuration='', $label, $id_parametrage, $id_garantie)
	{
		if (empty($id_configuration)) {
			$this->query = "insert into gar_parametrage_label (ID, LABEL, ID_PARAMETRAGE, ID_GARANTIE) VALUES ('".getNextRow('GAR_PARAMETRAGE_LABEL')."', '".ereg_replace("'","''", $label)."', '".$id_parametrage."', '".$id_garantie."')";
		} else {
			$this->query = "UPDATE gar_parametrage_label SET ID = '".$id_configuration."', LABEL = '".ereg_replace("'","''", $label)."', ID_PARAMETRAGE = '".$id_parametrage."', ID_GARANTIE = '".$id_garantie."' WHERE ID = '".$id_configuration."'";
		}
		$this->check_exec();
		$this->finish();
		return $this->query;
	}
	
	function getConfigurationParametrage($id_garantie='', $id_parametrage='')
	{
		if (empty($id_garantie) && empty($id_parametrage)) {
			$this->query = "select * from gar_parametrage_label order by label";
		} else {
			$this->query = "select * from gar_parametrage_label where id_parametrage='".$id_parametrage."' AND id_garantie='".$id_garantie."'";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    			= $this->tab_result['ID'];
			$result[$i]['label']    		= stripslashes($this->tab_result['LABEL']);
			$result[$i]['id_parametrage'] 	= $this->tab_result['ID_PARAMETRAGE'];
			$result[$i]['id_garantie'] 		= $this->tab_result['ID_GARANTIE'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getConfigurationParametrageDeroul()
	{
		$this->query = "select distinct(label) from gar_parametrage_label order by label";
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    			= $this->tab_result['ID'];
			$result[$i]['label']    		= stripslashes($this->tab_result['LABEL']);
			$result[$i]['id_parametrage'] 	= $this->tab_result['ID_PARAMETRAGE'];
			$result[$i]['id_garantie'] 		= $this->tab_result['ID_GARANTIE'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	

	function getMinimum($id_ligne_garantie, $id_minimum='', $id_tranche='')
	{
		if (empty($id_minimum)) {
			$this->query = "select * from gar_minimum where id_ligne_garantie='".$id_ligne_garantie."' order by id_tranche";
		} else {
			$this->query = "select * from gar_minimum where id='".$id_minimum."' order by id_tranche";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    				= $this->tab_result['ID'];
			$result[$i]['id_ligne_garantie']    = $this->tab_result['ID_LIGNE_GARANTIE'];
			$result[$i]['taux'] 				= $this->tab_result['TAUX'];
			$result[$i]['id_tranche'] 			= $this->tab_result['ID_TRANCHE'];
			$result[$i]['id_formule'] 			= $this->tab_result['ID_FORMULE'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getMaximum($id_ligne_garantie, $id_maximum='', $id_tranche='')
	{
		if (empty($id_maximum)) {
			$this->query = "select * from gar_maximum where id_ligne_garantie='".$id_ligne_garantie."' order by id_tranche";
		} else {
			$this->query = "select * from gar_maximum where id='".$id_maximum."' order by id_tranche";
		}
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    				= $this->tab_result['ID'];
			$result[$i]['id_ligne_garantie']    = $this->tab_result['ID_LIGNE_GARANTIE'];
			$result[$i]['taux'] 				= $this->tab_result['TAUX'];
			$result[$i]['id_tranche'] 			= $this->tab_result['ID_TRANCHE'];
			$result[$i]['id_formule'] 			= $this->tab_result['ID_FORMULE'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	
	
	
	function getMinimumMat($id_ligne_garantie, $id_formule)
	{
		$this->query = "select * from gar_minimum where id_ligne_garantie='".$id_ligne_garantie."' and id_formule='".$id_formule."' order by id_tranche";

		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    				= $this->tab_result['ID'];
			$result[$i]['id_ligne_garantie']    = $this->tab_result['ID_LIGNE_GARANTIE'];
			$result[$i]['taux'] 				= $this->tab_result['TAUX'];
			$result[$i]['id_tranche'] 			= $this->tab_result['ID_TRANCHE'];
			$result[$i]['id_formule'] 			= $this->tab_result['ID_FORMULE'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getMaximumMat($id_ligne_garantie, $id_formule)
	{
		$this->query = "select * from gar_maximum where id_ligne_garantie='".$id_ligne_garantie."' and id_formule='".$id_formule."' order by id_tranche";
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['id']    				= $this->tab_result['ID'];
			$result[$i]['id_ligne_garantie']    = $this->tab_result['ID_LIGNE_GARANTIE'];
			$result[$i]['taux'] 				= $this->tab_result['TAUX'];
			$result[$i]['id_tranche'] 			= $this->tab_result['ID_TRANCHE'];
			$result[$i]['id_formule'] 			= $this->tab_result['ID_FORMULE'];
			$i++;
		}
		$this->finish();
		return $result;
	}
	
	function getMaxOrdre($id_garantie, $id_parametrage)
	{
		$this->query = "select ordre from gar_ligne_garantie where id_garantie='".$id_garantie."' and id_parametrage='".$id_parametrage."' order by ordre desc";
		
		$i=0;
		while ($this->fetch()) {
			$result[$i]['ordre']    				= $this->tab_result['ORDRE'];
			$i++;
		}
		$this->finish();
		return $result[0];
	}
	
	function getGaranties_soc($id_soc, $college='')
	{
		if ($college) {
			$this->query = "select * from gar_garantie where id_societe='".$id_soc."' and college='".$college."' order by label";
		} else {
			$this->query = "select * from gar_garantie where id_societe='".$id_soc."' order by label";
		}
			$i=0;
			while ($this->fetch()) {
				$result[$i]['id']    		= $this->tab_result['ID'];
				$result[$i]['label'] 		= $this->tab_result['LABEL'];
				$result[$i]['id_societe']   = $this->tab_result['ID_SOCIETE'];
				$i++;
			}
		$this->finish();
		return $result;
		
	}
}




// function getMaximum($id_ligne_garantie, $id_maximum='')
	// {
		// if (empty($id_maximum)) {
			// $this->query = "select * from gar_maximum where id_ligne_garantie='".$id_ligne_garantie."' order by id_formule";
		// } else {
			// $this->query = "select * from gar_maximum where id='".$id_maximum."' order by id_formule";
		// }
		
		// $i=0;
		// while ($this->fetch()) {
			// $result[$i]['id']    				= $this->tab_result['ID'];
			// $result[$i]['id_ligne_garantie']    = $this->tab_result['ID_LIGNE_GARANTIE'];
			// $result[$i]['taux'] 				= $this->tab_result['TAUX'];
			// $result[$i]['id_tranche'] 			= $this->tab_result['ID_TRANCHE'];
			// $i++;
		// }
		// $this->finish();
		// return $result;
	// }
	
	// function getMinimum($id_ligne_garantie, $id_formule)
	// {
		// $this->query = "select * from gar_minimum where id_ligne_garantie='".$id_ligne_garantie."' and id_formule='".$id_formule."' order by id_formule";

		// $i=0;
		// while ($this->fetch()) {
			// $result[$i]['id']    				= $this->tab_result['ID'];
			// $result[$i]['id_ligne_garantie']    = $this->tab_result['ID_LIGNE_GARANTIE'];
			// $result[$i]['taux'] 				= $this->tab_result['TAUX'];
			// $result[$i]['id_tranche'] 			= $this->tab_result['ID_TRANCHE'];
			// $result[$i]['id_formule'] 			= $this->tab_result['ID_FORMULE'];
			// $i++;
		// }
		// $this->finish();
		// return $result;
	// }
?>