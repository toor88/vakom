<?php

class db
{
	var $login;
	var $pwd;
	var $machine;
	var $port;
	var $sid;
	
	var $host;
	var $obj_connection;
	var $obj_parse;
	var $obj_execute;
	var $query;
	var $tab_result;
	
	function db($conn)
	{
		$this->obj_connection = $conn;
	}
	
	function connect()
	{
		$this->host = "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=".$this->machine.")(PORT=".$this->port."))(CONNECT_DATA=(SERVICE_NAME=".$this->sid.")))";
		$this->obj_connection = ocilogon($this->login,$this->pwd,$this->host);
	}
	
	function parse()
	{
		$this->obj_parse = ociparse( $this->obj_connection, $this->query );
	}
	
	function query($query)
	{
		$this->query = $query;
		$this->parse();
		@ociexecute($this->obj_parse);
		
		switch (substr(strtolower($this->query), 0, 6))
		{
			case "select":
				return $this->select();
				break;
			
			case "insert":
				return $this->insert();
				break;
			
			case "update":
				return $this->update();
				break;
			
			case "delete":
				return $this->delete();
				break;
			
			default:
		}
		
	}
	
	function select()
	{
		$num_cols = ocinumcols($this->obj_parse);
		for ($i = 1; $i <= $num_cols; $i++) {
			$column[$i] = strtolower(ocicolumnname($this->obj_parse, $i));
		}

		$nb_lignes = 0;
		$res=false;
		while (@ocifetch($this->obj_parse)){
			for ($i = 1; $i <= $num_cols; $i++) {
				$res[$nb_lignes][$column[$i]] = stripslashes(ociresult($this->obj_parse,$i));
			}
			$nb_lignes++;
		}
		return $res;
	}
	
	function insert()
	{
		return ocirowcount($this->obj_parse);
	}
	
	function update()
	{
		return ocirowcount($this->obj_parse);
	}
	
	function delete()
	{
		return ocirowcount($this->obj_parse);
	}
}

?>