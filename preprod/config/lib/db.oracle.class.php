<?php

class db_oracle
{
	var $login;
	var $passw;
	var $machine;
	var $port;
	var $sid;
	var $host;
	var $obj_connection;
	var $obj_parse;
	var $obj_execute;
	var $query;
	var $tab_result;
	
	function connect()
	{
		$this->host = "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=".$this->machine.")(PORT=".$this->port."))(CONNECT_DATA=(SERVICE_NAME=".$this->sid.")))";
		$this->obj_connection = ocilogon($this->login,$this->passw,$this->host);
	}
	
	function check_exec()
	{
		if (!isset($this->obj_parse)) $this->parse();
		if (!isset($this->obj_execute)) $this->execute();
	}
	
	function parse()
	{
		$this->obj_parse = ociparse($this->obj_connection, $this->query);
	}
	
	function execute()
	{
		$this->obj_execute = ociexecute($this->obj_parse);
	}
	
	function fetchall()
	{
		$this->check_exec();
		return ociFetchStatement($this->obj_parse, $this->tab_result);
	}
	
	function fetch()
	{
		$this->check_exec();
		return ociFetchInto($this->obj_parse, $this->tab_result, OCI_ASSOC);
	}
	
	function finish()
	{
		OCIFreeStatement($this->obj_parse);
		unset($this->obj_parse);
		unset($this->obj_execute);
	}
}

?>