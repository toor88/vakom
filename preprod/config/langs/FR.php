<?php

/* FICHIER DE LANGUE POUR L'INTRANET VAKOM */
// Ce fichier sera appelé par inclusion par défault par l'application, et sera remplacé par son équivalent pour les partenaires étrangers
// L'inclusion est faite dans le fichier de connexion connex.php

/* LISTE DES LIBELLES */
// Général

$t_opr					= "OPR";
$t_M					= "M";
$t_F					= "F";
$t_btn_rechercher		= "Rechercher";
$t_btn_valider			= "Valider";
$t_btn_ajouter			= "Ajouter";
$t_btn_inscrire			= "S'inscrire";
$t_btn_poursuivre		= "Poursuivre";
$t_btn_generer_produit	= "GENERER UN PRODUIT";
$t_btn_generer			= "GENERER";
$t_btn_creer			= "Créer";
$t_btn_creer_client		= "Créer une société";
$t_btn_creer_societe	= "Créer une société";
$t_btn_creer_candidat	= "Créer un candidat";
$t_btn_ajouter_contrat	= "Ajouter ce contrat";
$t_btn_ajouter_contact	= "Ajouter un contact";
$t_btn_affect_new_jet	= "Affecter de nouveaux jetons";
$t_date_certif			= "Date de certification";
$t_date_creation		= "Date de création";
$t_date_modif			= "Date de dernière modification";
$t_date_attrib			= "Date d'attribution";
$t_date_aff				= "Date d'affectation";
$t_date_naissance		= "Date de naissance";
$t_date_deb_valid		= "Date début validité";
$t_date_deb_valid2		= "Date de début de validité";
$t_date_fin_valid		= "Date fin validité";
$t_date_fin_valid2		= "Date de fin de validité";
$t_date_perfection		= "Date de perfectionnement";
$t_date_produit			= "Date produit";
$t_date_quest			= "Date quest.";
$t_date_last_quest		= "Date du dernier questionnaire saisi";
$t_formation			= "Formation";
$t_en_formation			= "En formation";
$t_suspendu				= "Suspendu";
$t_champs_oblig			= "champs obligatoires";
$t_adresse				= "Adresse";
$t_pays					= "Pays";
$t_cp		 			= "Code postal";
$t_ville	 			= "Ville";
$t_nom		 			= "Nom";
$t_autre_nom_part		= "Eventuelle autre désignation à afficher";
$t_prenom		 		= "Prénom";
$t_part					= "Partenaire";
$t_droits				= "Droits";
$t_profil_OPR			= "Profil OPR";
$t_niveau_certif		= "Niveau de certification";
$t_tel		 			= "Téléphone";
$t_tel_fix	 			= "Téléphone fixe";
$t_tel_port	 			= "Téléphone portable";
$t_last_opr	 			= "Dernier profil OPR";
$t_email	 			= "Email";
$t_statut	 			= "Statut";
$t_consultez	 		= "Consultez";
$t_actif	 			= "ACTIF";
$t_inactif	 			= "INACTIF";
$t_fax		 			= "Fax";
$t_nat	 				= "Nature";
$t_etat					= "Etat";
$t_contacts				= "Contacts";
$t_contact				= "Contact";
$t_contrat				= "Contrat";
$t_debut				= "Signature du contrat";
$t_fin					= "Fin du contrat";
$t_website				= "Site web";
$t_exclu				= "Exclusivité";
$t_montant				= "Montant";
$t_redevance			= "Redevance";
$t_HT					= "HT";
$t_info					= "Information";
$t_jetons				= "Jetons";
$t_info1				= "Information 1";
$t_redirect_web			= "Adresse web retour questionnaire";
$t_info2				= "Information 2";
$t_info3				= "Début de redevance";
$t_info4				= "Info contrat";
$t_suppr_abr			= "Suppr.";
$t_suppression			= "Suppression";
$t_supprimer			= "Supprimer";
$t_produit				= "Produit";
$t_comment				= "Informations";
$t_tarif_unit			= "Tarif unitaire";
$t_admin				= "Administrateur";
$t_cert					= "Certifié";
$t_cert2				= "Certifié(e)";
$t_duree				= "Durée";
$t_login				= "Login";
$t_extranet				= "Extranet";
$t_mdp_extranet			= "Mot de passe Extranet";
$t_mdp					= "Mot de passe";
$t_tous					= "Tous";
$t_patientez_charge		= "Patientez durant le chargement";
$t_patientez			= "Patientez";
$t_ope					= "Opération";
$t_nouveau_produit		= "Nouveau produit";
$t_societe				= "Société";
$t_societes_toutes		= "Toutes les sociétés";
$t_opes_toutes			= "Toutes les opérations";
$t_age					= "Age";
$t_sexe					= "Sexe";
$t_dossiers				= "Dossiers";
$t_cands				= "CANDIDATS";
$t_cand_tous			= "Tous les candidats";
$t_cand					= "Candidat";
$t_requetes				= "REQU&Ecirc;TES";
$t_produits				= "PRODUITS";
$t_bienvenue			= "Bienvenue";
$t_deconnexion			= "DECONNEXION";
$t_select				= "Select";
$t_selection			= "Sélection";
$t_fonction				= "Fonction";
$t_langue				= "Langue";
$t_oui					= "Oui";
$t_non					= "Non";
$t_type					= "Type";
$t_type_tous			= "Tous les types";
$t_produits_tous		= "Tous les produits";
$t_fermer				= "FERMER";
$t_select_client		= "Sélectionner le client";
$t_responsable			= "Responsable";
$t_annulation			= "Annulation";
$t_bloque				= "bloqué";
$t_OK					= "OK";
$t_packs_tous			= "Tous les packs";
$t_aller_page_ident		= "Aller à la page d'identification";

$t_recherche_un_produit = "<span class='menu_tarifs'>RECHERCHE</span> <span class='menu_tarifs2'>UN PRODUIT</span>";
$t_generer_produit		= "<span class='menu_tarifs' style='color: red;'>Générer</span> <span class='menu_tarifs2' style='color: red;'>un produit</span>";
 
// Messages
$t_aucun_cert			= "Il n'y a aucun certifié pour ce partenaire. Ajoutez un certifié en";
$t_cand_pas_quest		= "Un ou plusieurs questionnaires doivent avoir été saisis préalablement";
$t_cliquant_ici			= "cliquant ici";
$t_aucun_clt_cert		= "Aucun client pour ce certifié";
$t_aucun_candidat		= "Aucun candidat ne correspond à votre recherche";
$t_aucun_candidat_r		= "Aucun candidat ne correspond aux critères sélectionnés";
$t_pas_de_jeton_dispo	= "Vous n'avez pas de jeton disponible pour ce produit";
$t_choisir_un_part		= "Veuillez choisir un partenaire";
$t_choisir_un_cert		= "Veuillez choisir un certifié";
$t_choisir_un_pdt		= "Veuillez choisir un produit";
$t_choisir_un_type		= "Veuillez choisir un type de jetons";
$t_choisir_un_type_pack	= "Veuillez choisir un type de pack";
$t_aucune_operation_cert= "Aucune opération n'est recensée pour ce certifié";
$t_aucune_operation_type= "Aucune opération recensée pour ce type d'opération";
$t_aucune_operation_cand= "Aucune opération associée à ce candidat";
$t_aucun_pack_pdt		= "Aucun pack n'est disponible pour ce produit";
$t_aucun_contrat_part	= "Aucun contrat valide pour ce partenaire, souhaitez vous continuer ?";
$t_aucun_contrat_part2	= "Aucun contrat valide pour ce partenaire";
$t_contrat_invalide		= "Contrat invalide";
$t_supp_cand_list		= "Voulez-vous vraiment supprimer le candidat de la liste ?";
$t_mod_contrat_part		= "Voulez-vous vraiment modifier ce contrat pour ce partenaire ?";
$t_supp_contrat_part	= "Voulez-vous vraiment supprimer ce contrat pour ce partenaire ?";
$t_sur_de_gen_un_pdt	= "Etes vous sûr de vouloir générer un produit ?";
$t_sur_dannuler_saisie	= "Etes-vous sur de vouloir annuler la saisie de cette opération pour ce candidat ?";
$t_sur_dannuler_aff		= "Etes-vous sûr de vouloir annuler cette affectation ?";
$t_pas_sel_operation	= "Vous n'avez pas sélectionné d'opération";
$t_pas_assez_de_jeton	= "Vous n'avez pas assez de jetons pour valider les affectations";
$t_mauvaise_date_fin	= "La date de fin est incorrecte";
$t_mauvaise_date_deb	= "La date de début est incorrecte";
$t_pb_date_fin_deb		= "La date de fin doit être supérieure à la date de début";
$t_mauvaise_date_naiss	= "La date de naissance est incorrecte";
$t_nom_oblig			= "Le Nom est obligatoire";
$t_design_oblig			= "La Designation est obligatoire";
$t_code_clt_oblig		= "Le Code client est obligatoire";
$t_adresse_oblig		= "L'adresse est obligatoire";
$t_ville_oblig			= "La ville est obligatoire";
$t_cp_oblig				= "Le Code postal est obligatoire (5 caractères)";
$t_couleur_oblig		= "La Couleur de fond est obligatoire";
$t_prenom_oblig			= "Le Prénom est obligatoire";
$t_email_oblig			= "L'email est obligatoire";
$t_tel10_oblig			= "Le téléphone est obligatoire (attention : ne pas mettte d'espace ni de points)";
$t_adr_retour_oblig		= "L'adresse web de retour questionnaire est obligatoire";
$t_login_oblig			= "Le login extranet est obligatoire";
$t_mdp_oblig			= "Le mot de passe extranet est obligatoire";
$t_public_mdp_oblig		= "Le mot de passe est obligatoire";
$t_tel_oblig			= "Le téléphone est obligatoire";
$t_pdt_oblig			= "Le produit est obligatoire";
$t_jet_nbre_oblig		= "Le nombre de jetons est obligatoire";
$t_nbre_jet_inf_egal	= "Le nombre de jetons affectés doit être inférieur ou égal à";
$t_nbre_jet_sup_egal	= "et supérieur ou égal à";
$t_nbre_jet_egal		= "Le nombre de jetons doit être égal à";
$t_nbre_jet_entre		= "Le nombre de jetons doit être compris entre";
$t_et					= "et";
$t_droits_oblig			= "Les droits du certifié doivent être définis";
$t_aucune_cert_cert		= "Aucune certification n'est disponible pour ce certifié.";
$t_nbre_jet_inf			= "Le nombre de jetons affectés doit être inférieur ou égal au nombre de jetons disponibles";
$t_desole_attr_jet		= "Désolé mais vous ne pouvez pas attribuer de jetons";
$t_sur_de_mod_tarif		= "Etes vous sur de vouloir modifier le tarif unitaire ?";
$t_pas_assez_jet		= "Vous n'avez pas suffisamment de jetons pour sélectionner autant de candidats";
$t_aucun_clt_cert_cand	= "Aucun client pour le certifié de ce candidat";
$t_email_format_incor	= "L'email doit être de la forme quelquechose@domain.fr";
$t_email_exist_deja		= "Cet email existe déjà dans notre base de données";
$t_nom_exist_deja		= "Ce nom existe déjà dans notre base de données";
$t_soc_exist_deja		= "Une société du même nom a déjà été créée. Confirmer la création de cette nouvelle société ?";
$t_activite				= "Activité";
$t_categorie_SP			= "Catégorie";
$t_au_moins_un_cert		= "Au moins un certifié par société";
$t_risque_de_perdre		= "En cliquant sur OK, vous risquez de perdre les informations non sauvegardées";
$t_validez_bien_contrat	= "Validez bien vos modifications avant d\'ajouter un contrat.";
$t_mail_OK				= "Le mail à bien été envoyé";
$t_sexe_oblig			= "Le sexe est obligatoire";
$t_confirm_mail			= "Veuillez confirmer votre email";
$t_confirm_mdp			= "Veuillez confirmer votre mot de passe";
$t_date_naiss_oblig		= "La date de naissance est obligatoire";
$t_nom_ent_oblig		= "Le Nom de l\'entreprise est obligatoire";
$t_nom_cont_oblig		= "Le Nom du contact est obligatoire";
$t_prenom_cont_oblig	= "Le Prénom du contact est obligatoire";
$t_cp_fact_oblig		= "Le code postal de facturation est obligatoire";
$t_ville_fact_oblig		= "La ville de facturation est obligatoire";
$t_pays_fact_oblig		= "Le pays de facturation est obligatoire";
$t_lire_cond_vente		= "Vous devez lire et accepter les conditions générales de vente";
$t_erreur				= "Certains paramètres sont nécessaires pour poursuivre";
$lib_sur_la_ligne_des_certifs 	= "Sur la ligne des certifications n°";
$lib_date_perf_incorrect		= ", la date de perfectionnement est incorrecte";
$lib_date_certif_incorrect		= ", la date de certification est incorrecte";

// Jetons
$t_jet_type				= "Type de jetons";
$t_jet_nbre				= "Nombre de jetons";
$t_jet_dispo_attr		= "Jetons disponibles<br>/jetons attribués";
$t_jet_dispo_aff		= "Jetons affectés à des certifiés<br>(jetons restants / jetons affectés)";
$t_jet_dispo_aff2		= "Jetons du certifié<br>(jetons restants / jetons affectés)";
$t_jet_dispo_autres		= "Autre jetons du partenaire<br>(jetons restants / jetons du partenaire)";
$t_jet_actifs_only		= "Affichage des jetons actifs uniquement";
$t_jet_synth_at_part	= "SYNTHESE DES JETONS ATTRIBUES AU PARTENAIRE";
$t_jet_recap_at_part	= "RECAPITULATIF DES ATTRIBUTIONS DE JETONS AU PARTENAIRE";
$t_jet_recap_aff		= "RECAPITULATIF DES JETONS AFFECTES";
$t_jet_attrib_part		= "ATTRIBUTION DE JETONS AU PARTENAIRE";
$t_jet_aff_cert			= "SYNTHESE DES JETONS AFFECTES AU CERTIFIE";
$t_jet_aff_certs		= "SYNTHESE DES JETONS AFFECTES AUX CERTIFIES";
$t_jet_aff_de_jeton		= "AFFECTATION DE JETONS AU CERTIFIE";
$t_jet_modif_aff		= "MODIFICATION DU NOMBRE DE JETONS AFFECTES AU CERTIFIE";
$t_jet_cumul_utilise	= "Cumul utilisé";
$t_jet_nature_pack		= "Nature du pack";


// Partenaires
$t_parts				= "PARTENAIRES";
$t_part_tous			= "TOUS LES PARTENAIRES";
$t_part_opt_saisir 		= "Saisir le nom d'un partenaire";
$t_part_opt_creer 		= "Créer un nouveau partenaire";
$t_synthese_jet_part	= "SYNTHESE DES JETONS ATTRIBUES AU PARTENAIRE";

$t_fiche_part			= "FICHE PARTENAIRE";
$t_fiche_new_part		= "NOUVEAU PARTENAIRE";
$t_fiche_info_part		= "INFORMATIONS PARTENAIRE";
$t_fiche_info_contrat	= "INFORMATIONS CONTRACTUELLES";
$t_fiche_ergonomie		= "ERGONOMIE";
$t_fiche_part_bloq		= "Partenaire bloqué";
$t_fiche_nom_part		= "Nom du partenaire";
$t_fiche_design_part	= "Désignation";
$t_fiche_code_clt		= "Code Client";
$t_fiche_tva_int		= "TVA intracommunautaire";
$t_fiche_siret			= "N° SIRET";
$t_fiche_lang_extra		= "Langue de l'extranet dédié au client VAKOM";
$t_fiche_logo_part1		= "Logo affiché sur le questionnaire (140 * 85 transparent)";
$t_fiche_logo_part2		= "Bandeau de l'extranet (500 * 45)";
$t_fiche_logo_part3		= "Logo affiché sur les documents <br>(175 * 110 transparent)";
$t_fiche_ilogo_part3	= "<span style=\"font-weight: normal;\">(Nota Bene : logo VAKOM par défaut)</span>";

$t_part_ok_only	 		= "Partenaires OK seulement";
$t_part_rs		 		= "Désignation";
$t_part_cert_act		= "Certifiés actifs";

$t_part_typ_contrat		= "Type de contrat";
$t_part_date_debut		= "Signature";
$t_part_date_fin		= "Fin";
$t_part_exclu_terr		= "Exclusivité territoriale";
$t_part_montant_droit_e	= "Montant droit d'entrée";
$t_part_redevance_ann	= "Redevance annuelle";


// Certifiés
$t_certs1				= "Certifiés";
$t_certs				= "CERTIFIES";
$t_cert_tous			= "TOUS LES CERTIFIES";
$t_cert_actifs_only		= "Certifiés actifs seulement";

$t_cert_opt_saisir		= "Et/Ou saisir le nom d'un certifié";

$t_fiche_cert			= "FICHE CERTIFIE";
$t_fiche_cert_inactif	= "Certifié inactif";
$t_fiche_cand_assoc		= "Candidat associé";
$t_fiche_entit_factur	= "Entité de facturation";
$t_fiche_certif_list	= "Liste des Certifications";
$t_fiche_cert_transfert	= "Transférer le certifié vers";
$t_fiche_ne_pas_transf	= "Ne-pas-transférer";
$t_aucun_resultat  		= "Aucun résultat ne correspond à votre demande.";

// Candidats
$t_cand_opt_select_ope	= "Sélectionner une Opération";
$t_cand_opt_select_part	= "Sélectionner un Partenaire";
$t_cand_opt_select_cert	= "Sélectionner un Certifié";
$t_cand_part_tous		= "Tous les partenaires";
$t_cand_cert_tous		= "Tous les certifiés";
$t_cand_rechercher		= "RECHERCHER LES CANDIDATS";
$t_cand_par_soc			= "Par Société";
$t_cand_par_ope			= "Par Opération";
$t_cand_par_cand		= "Par Candidat";
$t_cand_saisir_societe	= "Saisir le nom d'une société";
$t_cand_type_ope		= "Type opération";
$t_cand_saisir_nom		= "Saisir un nom";
$t_cand_actif_only		= "Affichage des candidats actifs seulement";
$t_cand_libre			= "Candidat libre";
$t_cand_libres			= "Candidats commençant par :";
$t_cand_soc				= "Candidats de la société";
$t_cand_recap_dossiers	= "RECAPITULATIF DES DOSSIERS GENERES";
$t_cand_code_acces		= "Relance mail";

$t_fiche_new_clt		= "NOUVEAU CLIENT";
$t_fiche_new_cand		= "NOUVEAU CANDIDAT";
$t_fiche_edit_cand		= "EDITION CANDIDAT";
$t_fiche_new_contact	= "NOUVEAU CONTACT";
$t_fiche_cand			= "FICHE CANDIDAT";
$t_fiche_cand_inactif	= "Candidat inactif";
$t_cand_supp_prod		= "Êtes-vous certain de vouloir supprimer ce produit ?";
// Société
$t_fiche_new_soc		= "NOUVELLE SOCIETE";
$t_fiche_soc			= "FICHE SOCIETE";
$t_fiche_soc_inactive	= "Société inactive";
$t_fiche_clt_actif		= "Client actif";
$t_fiche_ent_nom		= "Nom de l'entreprise";
$t_fiche_soc_nom		= "Nom de la société";
$t_fiche_soc_partage	= "Partage des candidats";

// Produit
$t_produit_a_gen		= "Quel produit souhaitez-vous générer";
$t_produit_ope_part		= "Voulez-vous y associer une opération particulière";
$t_produit_ope_exist	= "Opération existante";
$t_produit_ope_nouvel	= "Ou nouvelle opération";
$t_produit_objet_mail	= "Contenu du mail envoyé aux candidats pour remplir un questionnaire";
$t_produit_mess_perso_1	= "Objet personnalisable";
$t_produit_mess_perso	= "Eventuel message personnalisé à ajouter";
$t_produit_cand_a_sel	= "Pour quel(s) candidat(s) :";
$t_produit_cand_a_sel2	= "Souhaitez-vous ajouter d'autres candidats";
$t_produit_recap_cand	= "Récapitulatif des candidats sélectionnés et questionnaires à (re)saisir";
$t_produit_quest_a_sais	= "Questionnaire à saisir ou ressaisir";
$t_produit_quest_type	= "Type du questionnaire à saisir ou ressaisir";
$t_produit_ajout_cand	= "Ajouter des candidats dans cette liste.";
$t_produit_fin_mail		= "Fin du corps du mail";
$t_produit_quest_niveau   = "Niveau";

$t_part_ajout_contrat	= "AJOUTER UN CONTRAT DU PARTENAIRE";

//Partie public
$t_pub_accepte_cond		= "J'accepte les <a href='#'>conditions générales de vente</a>";
$t_pub_ident			= "MERCI DE BIEN VOULOIR VOUS IDENTIFIER";
$t_pub_deja_inscrit		= "Déjà inscrit :";
$t_pub_mdp_oublie0		= "mot de passe oublié, ";
$t_pub_mdp_oublie1		= "cliquez ICI";
$t_pub_prem_inscrip		= "1ere inscription";
$t_pub_txt_inscrip		= "Pour vous inscrire, veuillez cliquer sur le lien suivant ";
$t_pub_format_date		= "ex : 24/04/1985";
$t_pub_mr				= "Mr";
$t_pub_mme				= "Mme";
$t_pub_bonjour			= "Bonjour";
$t_pub_txt_rens			= "Merci de bien vouloir renseigner le formulaire ci-dessous.<br>* Champs obligatoires";
$t_pub_txt_quest1		= "Le questionnaire a été saisi le ";
$t_pub_txt_quest2		= ". Souhaitez vous le saisir à nouveau";
$t_pub_rensei			= "RENSEIGNEMENTS";
$t_pub_paiement			= "PAIEMENT";
$t_pub_quest			= "QUESTIONNAIRE";
$t_pub_quests			= "QUESTIONNAIRES";
$t_pub_remer			= "REMERCIEMENT";
$t_pub_select_clt		= "Sélectionner le client";
$t_pub_entre			= "Etes-vous une entreprise ?";
$t_pub_confim_mail		= "Confirmer l'email";
$t_pub_confim_mdp		= "Confirmer le mot de passe";
$t_pub_aut_ad			= "Autre adresse de facturation ?";
$t_pub_nom_ent			= "Nom de l'entreprise";
$t_pub_tva				= "N° TVA intracommunautaire";
$t_pub_rep_quest		= "Veuillez repondre à la première question";
$t_pub_txt_paie			= "Cliquez sur le bouton &quot;Poursuivre&quot;, vous serez automatiquement redirigé<br>sur une page sécurisée pour effectuer le règlement.";

// Partie paiement
$t_pub_ref_trans		= "Référence de la transaction";
$t_pub_date_le			= "Date : le";
$t_pub_mont_com			= "Montant de la commande";
$t_paie_ok				= "LA TRANSACTION A ETE CORRECTEMENT EFFECTUEE. NOUS VOUS EN REMERCIONS.";
$t_paie_suite			= "CLIQUEZ SUR LE BOUTON &quot;POURSUIVRE&quot;";
$t_paie_suite2			= "CLIQUEZ SUR LE BOUTON &quot;POURSUIVRE&quot; POUR ACCEDER A LA PAGE DE REMERCIEMENT.";
$t_paie_ko				= "PAIEMENT REFUSE";
$t_paie_annule			= "PAIEMENT ANNULE";
$t_paie_retour			= "Retour sur le site de Vakom";


//Partie mail questionnaire
$t_quest_enreg			= "Votre questionnaire a bien été enregistré";
$t_quest_bonjour		= "Bonjour";
$t_quest_bilan			= "Voici votre bilan";
$t_quest_adobe			= "en pièce jointe, au format PDF. Pour télécharger gratuitement la dernière version d'Adobe Acrobat Reader, <a href=\"http://get.adobe.com/fr/reader/\">cliquer ici</a>.";
$t_quest_lecture		= "Bonne lecture";
$t_quest_note_mdp		= "Notez bien votre mot de passe";
$t_quest_txt_mdp		= "Associé à votre email, il vous permettra de vous identifier la prochaine fois que vous voudrez commander un autre bilan.";
$t_quest_merci			= "Merci de votre confiance";
$t_quest_cord			= "Bien cordialement";
$t_quest_equipe			= "L'Equipe";
$t_quest_envir			= "Avant d'imprimer cet email, réfléchissez à l'impact sur l'environnement";
$t_quest_confirm		= "Nous vous confirmons l'enregistrement de votre questionnaire";
$t_quest_sav_plus		= "Si vous souhaitez en savoir plus sur la méthode OPR, cliquez sur le lien ci-dessous,";
$t_quest_opr_plus		= "En savoir plus sur la méthode OPR";

//partie questionnaire
$t_quest_choix			= "Vos deux choix doivent être différents pour la question";
$t_quest_coche			= "Vous devez cocher une réponse + et - pour la question";
$t_quest_msg_erreur		= "Au moins une question est restée sans réponse. Merci de répondre à toutes les questions.";
$t_quest_reponse		= "Saisissez votre réponse";
$t_quest_chois_choix	= "Choisissez parmi ces choix";
$t_quest_val_ter		= "valider et terminer";

// page remerciement
$t_madame				="Madame";
$t_monsieur				="Monsieur";

// page Requêtes
$t_titre				= "REQUÊTES";
$t_radio1				= "Sélectionner une requête existante";
$t_radio2				= "Créer une nouvelle requête";
$t_radio2_prov			= "Provisoire";
$t_radio3				= "Dupliquer une requête";
$t_valid1				= "VALIDER";
$t_restrict				= "RESTRICTIONS D'ACCÉS";
$t_acces1				= "Accès aux certifiés";
$t_acces2				= "Accès aux administrateurs";
$t_data					= "DONNÉES";
$t_data_table_col1		= "Donnée";
$t_data_table_col2		= "Ordre";
$t_data_table_col3		= "Triée";
$t_data_table_col4		= "Affichée";
$t_data_table_col5		= "Sélect.";
$t_data_table_add		= "Ajouter une donnée";
$t_data_table_del		= "Supprimer";
$t_newData				= "Nouvelle(s) donnée(s)";
$t_newData_radio1		= "SOCIETE";
$t_newData_radio2		= "CANDIDAT";
$t_newData_radio3		= "CERTIFIE";
$t_newData_radio4		= "PARTENAIRE";
$t_newData_radio5		= "PRODUIT";
$t_newData_radio6		= "PI";
$t_newData_radio7		= "REPONSE";
$t_newData_radio8		= "JETON-PARTENAIRE";
$t_newData_radio9		= "JETON-CERTIFIE";
$t_newData_radio10		= "PN";
$t_newData_radio11		= "PR";
$t_newData_radio12		= "PNPI";
$t_newData_radio13		= "PNPR";
$t_newData_radio14		= "PRPI";
$t_newData_table_col1	= "Donnée";
$t_newData_table_col2	= "Sélect.";
$t_newData_table_cancel	= "Annuler";
$t_newData_table_add	= "Ajouter";
$t_filter				= "FILTRES";
$t_filter_table_col1	= "Donnée";
$t_filter_table_col2	= "Opérateur";
$t_filter_table_col3	= "Valeur";
$t_filter_table_col4	= "Modifier";
$t_filter_table_col5	= "Supprimer";
$t_filter_table_add		= "Ajouter un filtre";
$t_newFilter			= "Nouveau filtre / Modification de filtre";
$t_newFilter_table_col1	= "Donnée";
$t_newFilter_table_col2	= "Opérateur";
$t_newFilter_table_col3	= "Valeur";
$t_newFilter_table_cancel	= "Annuler";
$t_newFilter_table_add	= "Ajouter";
$t_newFilter_table_mod	= "Modifier";
$t_valid2				= "VALIDER";

// Mail génération de produit
$t_gen_prod_1			= "Bonjour";
$t_gen_prod_2			= "Merci de votre confiance. La démarche que vous allez entreprendre va être, pour vous, très enrichissante.";
$t_gen_prod_3			= "Nous souhaitons qu'elle vous apporte entière satisfaction.";
$t_gen_prod_4			= "Pour remplir le questionnaire";
$t_gen_prod_5			= "sur notre site internet, merci de bien vouloir";
$t_gen_prod_6			= "cliquer sur le lien ci-dessous";
$t_gen_prod_7			= "Attention : ce lien  est personnel, il ne peut en aucun cas être utilisé par une autre personne.";
$t_gen_prod_8			= "Renseigner le questionnaire";
$t_gen_prod_9			= "Si vous souhaitez en savoir plus, cliquez sur le lien ci-dessous,";
$t_gen_prod_10			= "En savoir plus sur la méthode OPR";
$t_gen_prod_11			= "Bien cordialement,";
$t_gen_prod_12			= "Si ce lien ne fonctionne pas, vous pouvez accéder au questionnaire sur le site <a href=\"http://www.vakom.fr/\">www.vakom.fr</a>, cliquer sur le bouton <font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;	font-weight:700\" color=\"#FF6600\">OPR EN LIGNE</font> puis saisir votre code d'accès : ";

// Mail regénération de produit
$t_gen_prod_renvoi_0			= "Nouvelle invitation à remplir votre questionnaire";
$t_gen_prod_renvoi_1			= "Bonjour";
$t_gen_prod_renvoi_2			= "Nous vous avons proposé de renseigner le questionnaire";
$t_gen_prod_renvoi_2b			= "Nous vous avons proposé de renseigner votre questionnaire";
$t_gen_prod_renvoi_3			= "Sauf erreur de notre part, nous n'avons toujours pas reçu les résultats.";
$t_gen_prod_renvoi_4			= "Afin de le renseigner sur notre site internet, merci de bien vouloir cliquer sur le lien ci-dessous";
$t_gen_prod_renvoi_5			= "Renseigner le questionnaire";
$t_gen_prod_renvoi_6			= "Si toutefois, la saisie de votre questionnaire est en cours, merci de ne pas tenir compte de ce mail.";
$t_gen_prod_renvoi_7			= "Attention : ce lien  est personnel, il ne peut en aucun cas être utilisé par une autre personne.";
$t_gen_prod_renvoi_8			= "Bien cordialement,";

// page Candidats
$t_error_part					= "Veuillez choisir un partenaire";
$t_error_certif_1				= "Veuillez choisir un certifié";
$t_error_certif_2				= "Avant de pouvoir éditer, vous devez choisir un certifié.";
$t_annul_quest_sujet			= "[VAKOM] annulation d'une saisie de questionnaire";
$t_annul_quest_1				= "Bonjour,";
$t_annul_quest_2				= "Nous tenons à vous informer qu'une saisie de questionnaire a été annulée pour le partenaire :";
$t_annul_quest_3				= "Cette annulation est valable pour";
$t_annul_quest_4				= "L'opération créée le";
$t_annul_quest_5				= "sur le code du certifié(e)";
$t_annul_quest_6				= "Code d'accès au questionnaire";
$t_annul_quest_7				= "Produit";
$t_annul_quest_8				= "pour le candidat";
$t_annul_quest_9				= "Nous nous chargeons de recréditer votre produit.";
$t_annul_quest_10				= "S'il y avait un questionnaire à renseigner, n'oubliez pas d'avertir votre candidat que son code d'accès est désormais inactif.";
$t_annul_quest_11				= "Nous restons à votre entière disposition pour tous compléments d'information.";
$t_annul_quest_12				= "Bien cordialement,";
$t_annul_quest_13				= "L'Equipe VAKOM";
$t_annul_quest_14				= "Produit";
$t_annul_quest_15				= "Type de jeton utilisé";
$t_ope_1						= "Aucune opération recensée pour ce type d'opération.";
$t_ope_2						= "Etes-vous sur de vouloir annuler la saisie de cette opération pour ce candidat ?";
$t_ope_3						= "Aucune opération n'est recensée pour ce certifié.";
$t_ope_4						= "Aucune opération associée à ce candidat.";
$t_titre_1						= "CANDIDATS";
$t_titre_2						= "Candidat";
$t_titre_3						= "Tous les candidats";
$t_titre_4						= "Tous les certifiés";
$t_titre_5						= "Société";
$t_titre_6						= "Toutes les sociétés";
$t_titre_7						= "Opération";
$t_titre_8						= "Toutes les opérations";
$t_titre_9						= "RÉCAPITULATIF DES DOSSIERS GÉNÉRÉS";
$t_wait							= "Patientez durant le chargement...";
$t_libel_1						= "Sélectionner un Partenaire";
$t_libel_2						= "Tous les partenaires";
$t_libel_3						= "Sélectionner un Certifié";
$t_libel_4						= "Tous les certifiés";
$t_libel_5						= "Il n'y a aucun certifié pour ce partenaire. Ajoutez un certifié en";
$t_libel_6						= "cliquant ici";
$t_libel_7						= "Par nom";
$t_libel_8						= "Par société";
$t_libel_9						= "Par opération";
$t_libel_10						= "Tous";
$t_libel_11						= "Affichage des candidats actifs seulement";
$t_libel_12						= "Email absent";
$t_libel_13						= "Candidat libre";
$t_libel_14						= "ACTIF";
$t_libel_15						= "INACTIF";
$t_libel_16						= "Consultez";
$t_libel_17						= "Aucun candidat actif ne correspond à votre recherche";
$t_libel_18						= "Aucun candidat ne correspond à votre recherche";
$t_libel_19						= "Editer un Compte-Rendu";
$t_btn_1						= "Créer un candidat";
$t_btn_2						= "Créer une société";
$t_btn_3						= "Rechercher";
$t_btn_4						= "Générer un produit";
$t_column_head_0				= "Date dernier <br>questionnaire";
$t_column_head_1				= "Nouveau <br>produit";
$t_column_head_2				= "Société";
$t_column_head_3				= "Nom";
$t_column_head_4				= "Prénom";
$t_column_head_5				= "Age";
$t_column_head_6				= "Sexe";
$t_column_head_7				= "Dernier profil OPR";
$t_column_head_8				= "Etat";
$t_column_head_9				= "Statut";
$t_column_head_10				= "Dossiers";
$t_column2_head_1				= "Date produit&nbsp;";
$t_column2_head_2				= "Date quest.&nbsp;";
$t_column2_head_3				= "Relance mail&nbsp;";
$t_column2_head_4				= "Dossier&nbsp;";
$t_column2_head_5				= "Opération&nbsp;";
$t_column2_head_6				= "Certifié&nbsp;";
$t_column2_head_7				= "Durée&nbsp;";
$t_column2_head_8				= "Etat&nbsp;";
$t_column2_head_9				= "Suppr.";
$t_invit_remplir				= "Invitation à remplir votre questionnaire";

$doc_de							= "De";
$doc_ent						= "Entretien individuel avec";
$doc_temps						= "Temps de passation";
$doc_a							= "A la demande du certifié OPR®";
$doc_quest						= "Questionnaire renseigné le";
$doc_sommaire					= "SOMMAIRE";
$doc_cadre						= "Dans le cadre de";
$doc_concerne					= "Personnes concernées";
$doc_certif						= "Certifié OPR®";


$t_acn_1 							= "Une erreur est survenue.";
$t_browser_support_error_1 		= "Votre navigateur ne supporte pas les objets XMLHTTPRequest...";
$t_confirm_affectation 			= "ætes-vous sr de vouloir annuler cette affectation ?";
$t_no_client_certifie 			= "Aucun Client Pour ce certifi";
$t_aucune_adr_email 				= "Vous n'avez entr aucune adresse email.Souhaitez-vous tout de mme continuer ?";
$t_sex_obligatoire 				= "Le sexe est obligatoire";
$t_utilisateur_existe 			= "Cette utilisateur existe dj.";
$t_copier_fichier_probleme		= "Impossible de copier le fichier";
$t_fichier_not_image				= "Le fichier n\'est pas une image";
$t_image1_grande					= "L'image1 est trop grande";
$t_jetons_error					= "Le nombre de jetons affects doit tre infrieur ou gal au nombre de jetons disponibles";
$t_jetons_attrubuer_error		= "Dsol mais vous ne pouvez pas attribuer de jetons.";
$t_candidat_sans_email			=	"candidat sans email";
$t_candidat_inactif				=	"candidat inactif";
$t_supprimer_questions_select	=	"Voulez-vous vraiment supprimer les questions slectionnes ?";
$t_supprimer_textes				=	"Etes vous sr de vouloir supprimer les textes correspondant  cette langue ?";
$t_supprimer_question 			= 	"Etes vous sr de vouloir supprimer cette question ? Cette question sera supprime dans toutes les langues.";
$t_supprimer_question_tabeau_1	=	"ætes-vous sr de vouloir supprimer le questionnaire ";
$t_supprimer_question_tabeau_2	=	"ainsi que le tabeau de correspondances associ ?";
$t_supprimer_variables			=	"Etes-vous sr de vouloir supprimer les variables slectionnes ?";
$t_supprimer_regles				=	"Etes-vous sr de vouloir supprimer les rgles slectionnes ?";
$t_somme_obg					=	"Le champ \"somme\" est obligatoire.";
$t_points_obg					=	"Le champ \"points\" est obligatoire.";
$t_valeur_points_obg			=	"Les deux champs de valeur des points sont obligatoires.";
$t_balise_egal_error			=	"La balise haute de valeur des points doit tre suprieure ou gale  la balise basse.";
$t_segments_obg					=	"Le champ \"segments\" est obligatoire.";
$t_blocks_obg					=	"Le champ \"blocs\" est obligatoire.";
$t_doc_deleted_error			=	"Dsol mais ce tableau de texte et image est utilis dans un document, il ne peut tre supprim.";
$t_supprimer_tableau			=	"Voulez-vous vraiment supprimer ce tableau de texte et image ?";
$t_supprimer_regroupement		=	"Voulez-vous vraiment supprimer ce code de regroupement ?";
$t_supprimer_combinaison		=	"ætes-vous sr de vouloir supprimer cette combinaison ?";
$t_manque_questionnaire			=	"Il manque le nom du questionnaire.";
$t_supprimer_document			=	"Voulez-vous vraiment supprimer ce document ?";
$t_supprimer_document_used		=	"Dsol mais ce document est utilis par un produit, il ne peut tre supprim.";
$t_supprimer_prix				=	"ætes vous sr de vouloir supprimer ce prix ?";
$t_date_debut_not_correcte		=	"La date de dbut n'est pas correcte";
$t_quantite_not_renseigne		=	"La quantit doit tre renseigne";
$t_prix_not_renseigne			=	"Le prix doit tre renseign";
$t_non_deja_utilise				=	"Dsol mais un dossier possde dj ce nom.";
$t_supprimer_produit			=	"Voulez-vous vraiment supprimer ce produit ?";
$t_produit_utilise				=	"Dsol mais ce produit a dj t utilis dans une opration, il ne peut tre supprim.";
$t_supprimer_dossiers			=	"Un ou plusieurs dossiers va(ont) tre supprim(s). ætes-vous sr ?";
$t_supprimer_certification		=	"ætes-vous certain de vouloir supprimer ce niveau de certification ?";
$t_supprimer_doc				=	"ætes-vous certain de vouloir supprimer ce document de la base de donnes ?";
$t_supprimer_dos				=	"ætes-vous certain de vouloir supprimer ce dossier de la base de donnes ?";
$t_supprimer_img				=	"ætes vous sr de vouloir supprimer cette image ?";
$t_supprimer_gph				=	"Voulez-vous vraiment supprimer ce graphe ?";
$t_graph_utilise				=	"Dsol mais ce graphe est utilis dans un document, il ne peut tre supprim.";
$t_supprimer_data				=	"ætes vous sr de vouloir supprimer cette donne ?";
$t_supprimer_quest				=	"ætes vous sr de vouloir supprimer ce Questionnaire pour ce Graphe ?";
$t_supprimer_requete			=	"Vous ne pouvez pas supprimer cette requte.";
$t_supprimer_requete_1			=	"Voulez-vous poursuivre la suppression de cette requte ?";
$t_supprimer_suite				=	"Voulez-vous poursuivre la suppression ?";
$t_supprimer_certif				=	"Voulez-vous vraiment supprimer ce certifi ?";
$t_lang_exist					=	"Cette langue existe dj pour le produit slectionn";
$t_opr_recense					=	"Aucune opration recense pour ce type d'opration";
$t_type_opr						=	"Vous devez choisir un type d'opration.";
$t_opr_certif					=	"Aucune opration n\'est recense pour ce certifi";
$t_select_certif				=	"Veuillez choisir un certifi";
$t_select_part  				=	"Veuillez choisir un partenaire";
$t_cand_lib  					=	"Candidat libre";
$t_saisie_annuler				=	"Etes-vous sur de vouloir annuler la saisie de cette opration pour ce candidat ?";
$t_creat_garantie_impo			=	"Cration de la garantie impossible. Le numro de contrat saisi existe dj pour ce collge";
$t_aucune_regle					=	"Pour le moment, aucune rgle n'est dfinie pour le questionnaire slectionn";
$t_aucune_variable				=	"Pour le moment, aucune variable n'est dfinie pour le questionnaire slectionn";
$t_aucune_graph					=	"Aucun Graphe trouv";
$t_mail_envoye					=	"Le mail  bien t renvoy";
$t_quest_renseigne				=	"Questionnaire déjà renseigné";
$t_aucune_combinaison			=	"Aucune combinaison n'est disponible.";
$t_aucune_price					=	"Aucun Prix n'est rfrenc dans la base de donnes";

$t_aucune_type_graph			=	"Aucun type de graphe dans la base";
$t_aucune_profil				=	"Aucun Profil trouvé";
$t_aucune_quest					=	"Aucun Questionnaire trouvé";
$t_param_enreg					=	"Les paramtres ont bien été enregistrés";
$t_acces_interdit				=	"Accès Interdit";

$t_correspondances				=	"CORRESPONDANCES";
$t_price						=	"PRIX";
$t_documents					=	"DOCUMENTS";
$t_setting_general				=	"PARAMETRES GENERAUX";
$t_text_image					=	"TEXTES &amp; IMAGES";
$t_graphes						=	"GRAPHES";

?>