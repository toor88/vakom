<?php
session_start();
	// Si l'utilisateur est un super admin
if ($_SESSION['droit']>5){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);


	switch($_GET['order_cert']){		
		case 'prenom':
			$order_cert = 'ORDER BY CERT_PRENOM ASC, CERT_NOM ASC';
			$order_part = 'ORDER BY PART_NOM,PART_RS ASC';
			break;
		case 'nom':
		default:
			$order_cert = 'ORDER BY CERT_NOM ASC, CERT_PRENOM ASC';
			$order_part = 'ORDER BY PART_NOM,PART_RS ASC';
			break;		
		case 'part':

			$order_part = 'ORDER BY PART_NOM,PART_RS ASC';
			break;
	}
	
	if ($_POST['submit']){
		/* Si la personne a soumis le formulaire */
		if (trim($_POST['search_part'])!='' || trim($_POST['search_cert'])!=''){
			$str_part	= $_POST['search_part'];
			$str_cert	= $_POST['search_cert'];
			
			$where1 	= "AND LOWER(PART_NOM) = '".txt_db(strtolower($str_part))."' ";
			
			$sql_part_list1 = "SELECT * FROM PARTENAIRE, CODE WHERE CODE.CODE_TABLE='NATURE' AND CODE.CODE_ID=PARTENAIRE.PART_NATURE_CODE_ID ".$where1." ".$order_part."";
			$qry_part_list1 = $db->query($sql_part_list1);
			//echo $sql_part_list1;
			if(is_array($qry_part_list1)){
				header('location:admvak_gest_Certifies.php?partid='.$qry_part_list1[0]['part_id']);
			}else{			
				header('location:admvak_gest_Certifies.php?str_part='.$str_part.'&str_cert='.$str_cert);
			}
		}else{
			header('location:admvak_gest_Certifies.php?str_part=&str_cert=');
		}
	}
	
		if ($_GET['actif']=='1' || !isset($_GET['actif'])){
			$sql_contact_actif = " AND ACTIF='1'";
		}
		
		if (isset($_GET['str_part'])){
			$where 		= "AND LOWER(PART_NOM) LIKE '".txt_db(strtolower($_GET['str_part']))."%' ";
			$str_part	= $_GET['str_part'];
			$str_cert	= $_GET['str_cert'];

			$sql_part_list = "SELECT * FROM PARTENAIRE, CODE WHERE CODE.CODE_TABLE='NATURE' AND CODE.CODE_ID=PARTENAIRE.PART_NATURE_CODE_ID ".$where." ".$order_part."";
			//echo $sql_part_list;
			$qry_part_list = $db->query($sql_part_list);
		}
		
		if(is_array($qry_part_list)){
			// Si il n'y a qu'un résultat
			if(!is_array($qry_part_list[1])){
				header('location:admvak_gest_Certifies.php?partid='.$qry_part_list[0]['part_id']);
			}else{
				$multi_rep = true;
			}
		}
	
	if ($_GET['action']=='delete' && $_GET['partid']>0 && $_GET['contact']>0){
		$sql_delete_contact = "UPDATE CERTIFIE SET CERT_DATE_SUPPRESSION=SYSDATE, CERT_USER_SUPPRESSION_ID='".$_SESSION['vak_id']."' WHERE CERT_ID='".txt_db($_GET['contact'])."' ".$order_cert."";
		$qry_delete_contact = $db->query($sql_delete_contact);
		header('location:admvak_gest_Certifies.php?partid='.$_GET['partid']);
	}
	

	if ($_GET['partid']!=''){
		$sql_part = "SELECT * FROM PARTENAIRE WHERE PART_ID='".txt_db($_GET['partid'])."'";
		$qry_part = $db->query($sql_part);
	
		$sql_cert = "SELECT * FROM CERTIFIE WHERE CERT_PART_ID='".txt_db($_GET['partid'])."' AND CERT_DATE_SUPPRESSION IS NULL".$sql_contact_actif." ".$order_cert."";
			//echo $sql_cert;
		$qry_cert = $db->query($sql_cert);
	}else{
		if($multi_rep){
			if (isset($_GET['str_part']) || isset($_GET['str_cert'])){
				$where = "AND LOWER(PART_NOM) LIKE '".txt_db(strtolower($_GET['str_part']))."%' AND LOWER(CERT_NOM) LIKE '".txt_db(strtolower($_GET['str_cert']))."%' ";
			}
			$sql_cert = "SELECT * FROM CERTIFIE, PARTENAIRE WHERE CERT_PART_ID=PART_ID ".$where." AND CERT_DATE_SUPPRESSION IS NULL".$sql_contact_actif." ".$order_cert."";
			//echo $sql_cert;
			$qry_cert = $db->query($sql_cert);
		}
	}
	if(!isset($_GET['actif'])){
		$query_str = $_SERVER['QUERY_STRING'];
	}else{
		$query_str = substr($_SERVER['QUERY_STRING'], 0, (strlen($_SERVER['QUERY_STRING'])-8));
	}
	
	?>
	<html>
	<head>
	<title>Vakom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<link rel="stylesheet" href="../css/style.css" type="text/css">	
	<script language="JavaScript">
	<!--
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}

	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	
	function c_delete(x,y){
		if (confirm('<?php echo $t_supprimer_certif ?>')){
			document.location.href='admvak_gest_Certifies.php?partid='+x+'&action=delete&contact='+y+'&actif=<?php echo intval($_GET['actif']) ?>';
		}
	}
	
	function show_actif(){
		if(document.getElementById('actif').checked==true){
			document.location.href='admvak_gest_Certifies.php?<?php echo $query_str ?>&actif=1';
		}else{
			document.location.href='admvak_gest_Certifies.php?<?php echo $query_str ?>&actif=0';
		}
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
			<?php
			include("menu_top_new.php");
			?>
	  <br>
<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>		  
	 <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Certifies"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;CERTIFI&Eacute;S</td>
		</tr>
	  </table>
	  <form method="post" action="admvak_gest_Certifies.php?idnc=<?php echo $_GET['idnc'] ?>">
	  <table width="961" border="0" cellspacing="0" cellpadding="0" class="fond_tablo_certifies" align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX"> 
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
			  <tr> 
				<td class="TX" align="right">
					Saisir&nbsp;le&nbsp;nom&nbsp;d'un&nbsp;partenaire&nbsp;:&nbsp;
				</td><td class="TX" align="left">
					<input type="text" name="search_part" size="50" class="form_ediht_Certifies" value="<?php echo htmlentities($_GET['str_part']) ?>">
				  <input type="hidden" name="step" value="1">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" align="right">Et/Ou&nbsp;saisir&nbsp;le&nbsp;nom&nbsp;d'un&nbsp;certifi&eacute;&nbsp;:&nbsp;
				</td><td class="TX" align="left">
					<input type="text" name="search_cert" size="50" class="form_ediht_Certifies" value="<?php echo htmlentities($_GET['str_cert']) ?>">
				  <input type="hidden" name="step" value="1">
				</td>
			  </tr>
			 </table>			
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	  <br>
	  <table cellpadding="0" cellspacing="0" width="961" align="center">
		 <tr><td align="center">
			<input type="submit" name="submit" value="RECHERCHER" class="bn_valider_certifie">
		 </td></tr>
	  </table>
	  </form>
	  <br>
		<?php
		if (isset($_GET['str_part']) || isset($_GET['str_cert']) || isset($_GET['partid'])){
		?>
			 <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Certifies"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;
				  <?php
				if($_GET['partid']>0){
					$colspan='9';
					if (is_array($qry_part)){
						echo '<a class="Titre_Certifies" href="#" onClick="MM_openBrWindow(\'admvak_edit_client.php?partid='. $qry_part[0]['part_id'] .'\',\'Vakom_Fiche_Partenaire_'. $qry_part[0]['part_id'] .'\',\'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=800\')">'.htmlentities($qry_part[0]['part_nom']).'</a>';
					}
				}else{
					$colspan='10';
					if($_GET['str_part'] == '' && $_GET['str_cert'] == ''){
						echo 'TOUS LES CERTIFI&Eacute;S';
					}else{
						if($_GET['str_part'] != ''){
							echo $_GET['str_part'];
						}else{
							echo $_GET['str_cert'];
						}
					}
				}
				?></td>
				</tr>
			  </table>
			  <table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="center" class="TX"> 
					<table width="961" border="0" cellspacing="0" cellpadding="2" class="TX">
					  <tr align="left"> 
						<td colspan="<?php echo $colspan ?>" class="TX_Certifies">CONTACTS</td>
					  </tr>
					  <tr align="left">
						<td colspan="<?php echo $colspan ?>" height="1" bgcolor="#666666"> </td>
					  </tr>
					  <tr align="left"> 
						<td colspan="<?php echo $colspan ?>" class="TX_GD"> 
						&nbsp;
						</td>
					  </tr>
					  
					  <tr align="left"> 
						<td colspan="<?php echo ($colspan-3) ?>" class="TX_GD"> 
						<?php
						if ($_GET['partid']>0){
						?>
							<input type="button" name="new_ctact" value="Ajouter un contact" class="bn_ajouter" onClick="MM_openBrWindow('admvak_crea_contactClient.php?partid=<?php echo $_GET['partid'] ?>','Creation_Contact','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">
						<?php
						}
						?>
						</td>
						<td colspan="3" class="TX" style="text-align: right;"> 
						  <input type="checkbox" name="actif" id="actif" value="1" onClick="show_actif();" <?php if ($_GET['actif']=='1' || !isset($_GET['actif'])) echo ' checked="checked"' ?>>Certifiés&nbsp;actifs&nbsp;seulement
						</td>
					 </tr>
						<?php
						if (is_array($qry_cert)){
								?>
							  <tr> 
								<td width='12%' class="TX_bold">
								<a href="admvak_gest_Certifies.php?partid=<?php echo $_GET['partid'] ?>&str_part=<?php echo $_GET['str_part']; ?>&str_cert=<?php echo $_GET['str_cert']; ?>&order_cert=nom">Nom</a></td>
								<td width='12%' class="TX_bold"><a href="admvak_gest_Certifies.php?partid=<?php echo $_GET['partid'] ?>&str_part=<?php echo $_GET['str_part']; ?>&str_cert=<?php echo $_GET['str_cert']; ?>&order_cert=prenom">Pr&eacute;nom</a></td>
								<?php
								if ($_GET['partid']<1){
									?>
									<td width='12%' class="TX_bold"><a href="admvak_gest_Certifies.php?partid=<?php echo $_GET['partid'] ?>&str_part=<?php echo $_GET['str_part']; ?>&str_cert=<?php echo $_GET['str_cert']; ?>&order_cert=part">Partenaire</a></td>
									<?php
								}
								?>
								<td width='12%' align="center" class="TX_bold">Droits</td>
								<td width='12%' align="left" class="TX_bold">Profil <br/>
								  OPR</td>
								<td width='12%'  class="TX_bold">Niveau de <br/>
								  certification</td>
								<td width='12%' class="TX_bold">Date de <br/>
								  certification</td>
								<td width='12%' class="TX_bold" align="center">Etat</td>
								<td width='12%' class="TX_bold" align="center">Jetons</td>
								<td width='12%' class="TX_bold" align="center">Statut</td>
								<!--<td class="TX_bold">&nbsp;</td>-->
							  </tr>
							  <tr> 
								<td colspan="<?php echo $colspan ?>" bgcolor="#CCCCCC" height="1" valign="top"></td>
							  </tr>
							  	<?php
								foreach($qry_cert as $cert){
									if ($cert['cert_droit_certifie']=='1'){
										$sql_certif = "SELECT * FROM CERT_A_CERTIF, CODE WHERE CERT_A_CERTIF.CERTIF_CODE_ID=CODE.CODE_ID AND (CERTIF_CERTIFICATION IS NOT NULL OR CERTIF_FORMATION = 1 OR CERTIF_SUSPENDU= 1 ) AND CODE.CODE_TABLE='CERTIFICATION' AND CERT_A_CERTIF.CERTIF_CERT_ID='".txt_db($cert['cert_id'])."' ORDER BY CODE.CODE_LIBELLE ASC";
										//echo $sql_certif;
										$qry_certif = $db->query($sql_certif);
									}
									else
									{
										$qry_certif ='';
									}
								?>
								  <tr> 
									<td class="TX"><a href="#" onClick="MM_openBrWindow('admvak_edit_contactClient.php?certid=<?php echo $cert['cert_id'] ?>','edit_<?php echo$cert['cert_id']?>','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')"><?php echo strtoupper($cert['cert_nom']) ?></a></td>
									<td class="TX"><?php echo ucfirst($cert['cert_prenom']) ?></td>
									<?php
									if ($_GET['partid']<1){
										?>
										<td class="TX"><a href="#" 
							<?php
if($_SESSION['droit']>5){
?>
onClick="MM_openBrWindow('admvak_edit_client.php?partid=<?php echo $cert['part_id'] ?>','Vakom_Fiche_Partenaire_<?php echo $cert['part_id'] ?>','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=800')"
<?php
}
?>
><?php echo ucfirst($cert['part_nom']) ?>
</a></td>
									<?php
									}
									?>
									<td align="center" class="TX">
									<?php
									if ($cert['cert_droit_admin']=='1'){
										echo 'Administrateur';
										if ($cert['cert_droit_certifie']=='1'){
										echo '/<br>';
										}
									}
									if ($cert['cert_droit_certifie']=='1'){
										echo 'Certifié(e)';
									}
									?>
									</td>
									<td align="left" class="TX">
									<?php
									if($cert['cert_cand_id']!=''){
										$sql_opr = "SELECT LAST_OPR FROM CANDIDAT WHERE CAND_ID='".txt_db($cert['cert_cand_id'])."'";
										$qry_opr = $db->query($sql_opr);
										echo $qry_opr[0]['last_opr'];
									}
									?>
									</td>
									<td class="TX">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												echo $certif['code_libelle'].'<br>';
											}
										}
										?>
									</td>
									<td class="TX">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												echo $certif['certif_certification'].'<br>';
											}
										}
										?>
									</td>
									<td class="TX" align="center">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												if ($certif['certif_formation']=='1'){
													echo 'Formation';
												}elseif($certif['certif_suspendu']=='1'){
													echo 'Suspendu';
												}
												echo '<br>';												
											}
										}
										?>
									</td>
									<td class="TX" align="center"><a href="#" onClick="MM_openBrWindow('admvak_produits_contactClient.php?partid=<?php echo $cert['cert_part_id'] ?>&certid=<?php echo $cert['cert_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes')">Consultez</a></td>
									<td class="TX" align="center">
									<?php
									if ($cert['actif']=='1'){
										echo '<span style="color:#00FF00; font-weight: bold;">ACTIF</span>';
									}else{
										echo '<span style="color:#FF0000; font-weight: bold;">INACTIF</span>';
									}
									?>
									</td>
								  </tr>
								  <tr> 
									<td colspan="<?php echo $colspan ?>" bgcolor="#CCCCCC" height="1" valign="top"></td>
								  </tr>
								<?php
								}
							}else{
							?>
							<tr align="center"> 
								<td colspan="<?php echo $colspan ?>" class="TX"> 
								Aucun résultat ne correspond à votre demande.
								</td>
							</tr>
							<?php
							}
						?>
					</table>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
		<?php
		}
		?>
		</p></div>	</article></div>	</div>	</div>	</div>					
	</body>
	</html>
	<?php
}else{
	include('no_acces.php');
}
?>