<?php
session_start();
/* print_r($_SESSION);
echo "<br>";
print_r($_GET); */
//print_r($_POST);
// Si l'utilisateur est admin
if ($_SESSION['droit']>3){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	/* POUR AV ET SAV */
	if($_SESSION['droit']>5){

		if (isset($_POST['search_part']) || isset($_GET['search_part'])){
			if($_GET['bloque']!=1){
				$and = " AND (PART_BLOQUE = '0' OR PART_BLOQUE IS NULL ) ";
			}
			
			if($_POST['search_part']){
				$search_part = $_POST['search_part'];
			}else{
				$search_part = $_GET['search_part'];
			}
			$where = "AND (LOWER(PART_NOM) LIKE '".txt_db(strtolower($search_part))."%' OR LOWER(PART_RS) LIKE '".txt_db(strtolower($search_part))."%') ";
			
			switch($_GET['order_part']){
				case 'nom':
				default:
					$order_part = 'ORDER BY PARTENAIRE.PART_NOM,PARTENAIRE.PART_RS ASC';
					break;
				case 'rs':
					$order_part = 'ORDER BY PARTENAIRE.PART_RS ASC';
					break;
				case 'cp':
					$order_part = 'ORDER BY PARTENAIRE.PART_CP ASC';
					break;
				case 'ville':
					$order_part = 'ORDER BY PARTENAIRE.PART_VILLE,PARTENAIRE.PART_NOM ASC';
					break;
				case 'nat':
					$order_part = 'ORDER BY CODE.CODE_LIBELLE ASC';
					break;
			}
			
			$sql_part_list = "SELECT * FROM PARTENAIRE, CODE WHERE CODE.CODE_TABLE='NATURE' AND CODE.CODE_ID=PARTENAIRE.PART_NATURE_CODE_ID ".$where." ".$and." ".$order_part."";
			//echo $sql_part_list;
			$qry_part_list = $db->query($sql_part_list);
			
			if(is_array($qry_part_list)){
				// Si il n'y a qu'un résultat
				if(!is_array($qry_part_list[1])){
					header('location:partenaires.php?partid='.$qry_part_list[0]['part_id']);
				}else{
					$multi_rep = true;
				}
			}else{
				if($_GET['stop']!=1){
					header('location:partenaires.php?idnc='.($_GET['idnc']+1).'&search_part='.$search_part.'&bloque=1&stop=1');
				}
			}
		}			
		
	}
	
	if ($_SESSION['droit']>5){
		$part_id = @$_GET['partid'];
	}else{
		$part_id = $_SESSION['part_id'];
	}
	
	if ($part_id!=''){
		$sql_part = "SELECT * FROM PARTENAIRE WHERE PART_ID='".txt_db($part_id)."'";
		$qry_part = $db->query($sql_part);
	}

	if (@$_GET['prodid']!='' && $_GET['typid']!='' && $part_id!='' && !isset($_GET['repart'])){
		switch($_GET['order']){
			case 'date_attrib':
			default:
				$order_jet = 'ORDER BY JET_DATE_ATTRIBUTION DESC';
				break;
			case 'nbre_jet':
				$order_jet = 'ORDER BY JET_NOMBRE';
				break;
			case 'date_deb':
				$order_jet = 'ORDER BY JET_DEB_VALIDITE DESC';
				break;
			case 'date_fin':
				$order_jet = 'ORDER BY JET_FIN_VALIDITE DESC';
				break;
		}
	
		if ($_GET['actif']==1 || $_GET['actif']=='' || !isset($_GET['actif'])){
			$sql_jet = "SELECT JETON.*, 
			CODE.CODE_LIBELLE TYPE_JETON, 
			PRODUIT.* 
			FROM JETON,PARTENAIRE,PRODUIT,CODE CODE 
			WHERE JETON.JET_TYPE_JETON_CODE_ID=CODE.CODE_ID 
			AND CODE.CODE_TABLE='TYPE_JETON' 
			and TO_char(JETON.JET_DEB_VALIDITE,'YYYYMMDD')<=to_char(SYSDATE,'YYYYMMDD') and TO_char(JETON.JET_FIN_VALIDITE,'YYYYMMDD')>=to_char(SYSDATE,'YYYYMMDD')
			AND JETON.JET_PROD_ID=PRODUIT.PROD_ID 
			AND PROD_ID='".txt_db($_GET['prodid'])."'
			AND CODE.CODE_ID='".txt_db($_GET['typid'])."'
			AND JET_PART_ID='".txt_db($part_id)."'
			AND JET_PART_ID=PARTENAIRE.PART_ID ".$order_jet;
		}					
		else
		{
			$sql_jet = "SELECT JETON.*, 
			CODE.CODE_LIBELLE TYPE_JETON, 
			PRODUIT.* 
			FROM JETON,PARTENAIRE,PRODUIT,CODE CODE 
			WHERE JETON.JET_TYPE_JETON_CODE_ID=CODE.CODE_ID 
			AND CODE.CODE_TABLE='TYPE_JETON' 
			AND JETON.JET_PROD_ID=PRODUIT.PROD_ID 
			AND PROD_ID='".txt_db($_GET['prodid'])."'
			AND CODE.CODE_ID='".txt_db($_GET['typid'])."'
			AND JET_PART_ID='".txt_db($part_id)."'
			AND JET_PART_ID=PARTENAIRE.PART_ID ".$order_jet;
		}
		//echo $sql_jet;
		$qry_jet = $db->query($sql_jet);
	}
	if(isset($_GET['repart'])){
		$sql_info_prod = "SELECT PROD_NOM FROM PRODUIT WHERE PROD_ID='".txt_db(intval($_GET['prodid']))."'";
		$qry_info_prod = $db->query($sql_info_prod);
		
		$sql_info_code = "SELECT CODE_LIBELLE FROM CODE WHERE CODE_ID='".txt_db(intval($_GET['typid']))."' AND CODE.CODE_TABLE='TYPE_JETON'";
		$qry_info_code = $db->query($sql_info_code);
		
		$sql_repart = "SELECT JET_ID, CERT_NOM, CERT_PRENOM, AFFECTE, UTILISE, AFFECTE-UTILISE Reste
		FROM CERTIFIE,JETON_CORRESP_CERT,CERT_A_JETON
		WHERE CERTIFIE.CERT_ID = JETON_CORRESP_CERT.CERT_ID AND JETON_CORRESP_CERT.CERT_JET_ID = CERT_A_JETON.CERT_JET_ID
		AND CERT_PART_ID='".txt_db(intval($_GET['partid']))."' AND CERT_JET_PROD_ID='".txt_db(intval($_GET['prodid']))."' AND CERT_JET_TYPE_JETON_CODE_ID='".txt_db(intval($_GET['typid']))."'";
		$qry_repart = $db->query($sql_repart);
	}
	?>
	<html>
	<head>
	<title>Vakom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<link rel="stylesheet" href="../css/style.css" type="text/css">	
	<script language="JavaScript">
	<!--
	function show_bloque(){
		if(document.getElementById('idpart_bloque').checked==true){
			document.location.href='partenaires.php?search_part=<?php echo @$search_part ?>&bloque=0';
		}else{
			document.location.href='partenaires.php?search_part=<?php echo @$search_part ?>&bloque=1';
		}
	}
	
	function valid_form1(){
	 if(document.form1.bn_part[1].checked==true){
		window.open('admvak_crea_client.php','crea_client','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes');
	 }else{
		document.form1.submit();
	 }
	}
	
	function show_actif(){
		if(document.getElementById('jet_actif').checked==true){
			document.location.href='partenaires.php?prodid=<?php echo @$_GET['prodid'] ?>&typid=<?php echo @$_GET['typid'] ?>&partid=<?php echo @$part_id ?>&actif=1#bas';
		}else{
			document.location.href='partenaires.php?prodid=<?php echo @$_GET['prodid'] ?>&typid=<?php echo @$_GET['typid'] ?>&partid=<?php echo @$part_id ?>&actif=0#bas';
		}
	}
	
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  //window.open(theURL,winName,features);
	  window.open(theURL,winName,features).focus();	  
	}

	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
	<?php
		$_GET['menu_selected']=4;
		include("menu_top_new.php");
	?>
	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>
	  <?php
		if ($_SESSION['droit']>5){
	  ?>		
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
			  <td width="20">&nbsp;</td>
			  <td width="250" class="Titre_Partenaires"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $t_parts ?></td>
			  <td align="center">&nbsp;</td>
			</tr>
		  </table>
		  <form method="post" name="form1" action="partenaires.php?idnc=<?php echo ($_GET['idnc']+1) ?>">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="fond_tablo_partenaires" align="center" onsubmit="return valid_form1()" >
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr> 
			<tr> 
			  <td width="14"></td>
			  <td align="center" class="TX"> 
				<table border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td class="TX" align="left">&nbsp;</td>
					<td class="TX" align="left">&nbsp;</td>
				  </tr>
				  <tr> 
					<td class="TX" align="left"> 
					  <input checked="checked" type="radio" name="bn_part" value="select">
					  &nbsp;<?php echo $t_part_opt_saisir ?>&nbsp;:&nbsp;&nbsp; </td>
					<td class="TX" align="left"> 
						<input type="text" name="search_part" size="50" class="form_ediht_Partenaires">
					</td>
				  </tr>
				  <tr> 
					<td class="TX" align="left"> 
					  <input type="radio" name="bn_part" value="create">
					  &nbsp;<?php echo $t_part_opt_creer ?>&nbsp;</td>
					<td class="TX" align="left">&nbsp; </td>
				  </tr>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
		  <br>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr><td align="center">
			<input type="submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_partenaire" onClick="valid_form1();">
			</td></tr>
		  </table>
		  <br>
		</form>
		<?php
		
	}
		
		if (@is_array($qry_part)){ // Si on a choisi un partenaire
			$sql_jeton = "SELECT PROD_ID, PROD_NOM, CODE_LIBELLE, CODE_ID, Sum(JET_NOMBRE) JET_NOMBRE_SUM 
					FROM JETON,PRODUIT,CODE
					WHERE JETON.JET_PROD_ID = PRODUIT.PROD_ID AND JET_TYPE_JETON_CODE_ID = CODE.CODE_ID AND CODE_TABLE='TYPE_JETON' AND JET_PART_ID='".txt_db($part_id)."'
					GROUP BY PROD_ID, PROD_NOM, CODE_LIBELLE, CODE_ID";
			$sql_jeton = "select distinct prod_nom,JET_PROD_ID prod_id,JET_TYPE_JETON_CODE_ID code_id,code_libelle,code_ordre from jeton,produit,code
			where jeton.JET_PROD_ID=produit.prod_id
			and jeton.JET_TYPE_JETON_CODE_ID=code.code_id and code.code_table='TYPE_JETON'
			and jet_part_id='".txt_db($part_id)."' order by prod_nom,code_ordre";				
//			echo $sql_jeton.'<br/>';
			$qry_jeton = $db->query($sql_jeton);
		?>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Partenaires"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<a class="Titre_Partenaires" 
		  <?php
			if($_SESSION['droit']>5){
			?>
			onClick="MM_openBrWindow('admvak_edit_client.php?partid=<?php echo $qry_part[0]['part_id'] ?>','Vakom_Fiche_Partenaire_<?php echo $qry_part[0]['part_id'] ?>','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=800')"
			<?php
			}
			elseif($_SESSION['droit']=4	){
			?>
			onClick="MM_openBrWindow('admvak_edit_client.php?partid=<?php echo $qry_part[0]['part_id'] ?>','Vakom_Fiche_Partenaire_<?php echo $qry_part[0]['part_id'] ?>','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=800')"
			<?php
			}
			?>
			href="#"><?php echo htmlentities($qry_part[0]['part_nom'].' '.$qry_part[0]['part_rs']) ?></a></td>
		</tr>
	  </table>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td class="TX_Partenaires"><?php echo $t_synthese_jet_part ?></td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td bgcolor="#666666" height="1"></td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX">&nbsp;</td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX"> 
			<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
				<tr><td style="text-align: left;" class="TX">
				<?php
				if($_SESSION['droit']>5){
					?>
					<input type="button" name="new_pdt2" value="<?php echo $t_btn_ajouter ?>" class="bn_ajouter" onClick="MM_openBrWindow('admvak_ajout_produit.php?partid=<?php echo addslashes($part_id) ?>','ajout_jeton','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">
					<?php
				}
				?>
				</td><td style="text-align: right;" class="TX">
					<input type="checkbox" name="jet_actif" id="jet_actif" onClick="show_actif();" value="1" <?php 
					if((isset($_GET['actif'])&&($_GET['actif']==1 || $_GET['actif']=='')) || !isset($_GET['actif'])){
						echo ' checked="checked"'; 
					}?>><?php echo $t_jet_actifs_only ?>
				</td></tr>
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
			  <tr align="center"> 
				<td colspan="5" class="TX_GD"></td>
			  </tr>
			  <tr align="center"> 
				<td height="1" colspan="5" bgcolor="#000000" ></td>
			  </tr>
			  <tr> 
				<td class="TX_bold"><?php echo $t_produit ?></td>
				<td class="TX_bold" align="center"><?php echo $t_jet_type ?></td>
				<td class="TX_bold" align="center"><?php echo $t_jet_dispo_attr ?></td>
				<td class="TX_bold" align="center"><?php echo $t_jet_dispo_aff ?></td>
				<td class="TX_bold" align="center"><?php echo $t_jet_dispo_autres ?></td>
			  </tr>
			  <tr> 
				<td height="1" bgcolor="#000000"></td>
				<td height="1" bgcolor="#000000"></td>
				<td height="1" bgcolor="#000000"></td>
				<td height="1" bgcolor="#000000"></td>
				<td height="1" bgcolor="#000000"></td>
			  </tr>
			  <?php
			  if (is_array($qry_jeton)){
				foreach($qry_jeton as $jeton){
					if((isset($_GET['actif'])&&($_GET['actif']==1 || $_GET['actif']=='')) || !isset($_GET['actif'])){
					$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".txt_db($part_id)."'
					and jet_prod_id='".$jeton['prod_id']."'
					and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
					and TO_char(JET_DEB_VALIDITE,'YYYYMMDD')<=to_char(SYSDATE,'YYYYMMDD') and TO_char(JET_FIN_VALIDITE,'YYYYMMDD')>=to_char(SYSDATE,'YYYYMMDD') and cert_id=-1";
					$qry_CJA = $db->query($sql_calcul_jetons_affectes);		
					$qry_CJA_dispo_aff = $qry_CJA[0]['affecte'];				
					$qry_CJA_dispo_uti = $qry_CJA[0]['utilise'];				
					if ($jeton['prod_id'] == 7 and $jeton['code_id'] == 12)
					{
//echo $sql_calcul_jetons_affectes.'<br>';
//echo 'qry_CJA_dispo_aff = '.$qry_CJA_dispo_aff.'<br>';
//echo 'qry_CJA_dispo_uti = '.$qry_CJA_dispo_uti.'<br>';
					}

					$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".txt_db($part_id)."'
					and jet_prod_id='".$jeton['prod_id']."'
					and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
					and TO_char(JET_DEB_VALIDITE,'YYYYMMDD')<=to_char(SYSDATE,'YYYYMMDD') and TO_char(JET_FIN_VALIDITE,'YYYYMMDD')>=to_char(SYSDATE,'YYYYMMDD') and cert_id<>-1";
					$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
					$qry_CJA_aff_aff = $qry_CJA[0]['affecte'];				
					$qry_CJA_aff_uti = $qry_CJA[0]['utilise'];				
					if ($jeton['prod_id'] == 7 and $jeton['code_id'] == 12)
					{
//echo $sql_calcul_jetons_affectes.'<br>';
//echo 'qry_CJA_aff_aff = '.$qry_CJA_aff_aff.'<br>';
//echo 'qry_CJA_aff_uti = '.$qry_CJA_aff_uti.'<br>';
					}
					}
					else
					{
					$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".txt_db($part_id)."'
					and jet_prod_id='".$jeton['prod_id']."'
					and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
					and cert_id=-1";
//echo $sql_calcul_jetons_affectes.'<br>';
					$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
					$qry_CJA_dispo_aff = $qry_CJA[0]['affecte'];				
					$qry_CJA_dispo_uti = $qry_CJA[0]['utilise'];							
					//if ($qry_CJA_dispo_uti > $qry_CJA_dispo_aff)
					//	$qry_CJA_dispo_aff = $qry_CJA_dispo_uti;
					$sql_calcul_jetons_affectes = "select nvl(sum(affecte),0) affecte ,nvl(sum(utilise),0) utilise from v_jeton_synthese where jet_part_id='".txt_db($part_id)."'
					and jet_prod_id='".$jeton['prod_id']."'
					and JET_TYPE_JETON_CODE_ID='".$jeton['code_id']."'
					and cert_id<>-1";
//echo $sql_calcul_jetons_affectes.'<br>';
					$qry_CJA = $db->query($sql_calcul_jetons_affectes);							
					$qry_CJA_aff_aff = $qry_CJA[0]['affecte'];				
					$qry_CJA_aff_uti = $qry_CJA[0]['utilise'];				
					//if ($qry_CJA_aff_uti > $qry_CJA_aff_aff)
					//	$qry_CJA_aff_aff = $qry_CJA_aff_uti;
					}					
					?>
				  <tr> 
					<td><a href="partenaires.php?partid=<?php echo $part_id ?>&prodid=<?php echo $jeton['prod_id'] ?>&typid=<?php echo $jeton['code_id'] ?>&actif=<?php echo $_GET['actif'] ?>#bas"><?php echo ucfirst($jeton['prod_nom']) ?></a></td>
					<td align="center"><?php echo ucfirst($jeton['code_libelle']) ?></td>
					<td align="center"><?php echo $qry_CJA_aff_aff + $qry_CJA_dispo_aff - $qry_CJA_aff_uti - $qry_CJA_dispo_uti ?>/<?php echo $qry_CJA_aff_aff + $qry_CJA_dispo_aff ?></td>					
					<td align="center"><a href="partenaires.php?partid=<?php echo $part_id ?>&prodid=<?php echo $jeton['prod_id'] ?>&typid=<?php echo $jeton['code_id'] ?>&actif=<?php echo $_GET['actif'] ?>&repart=1#bas"><?php echo $qry_CJA_aff_aff - $qry_CJA_aff_uti ?></a>/<?php echo $qry_CJA_aff_aff?></td>					
					<td align="center"><?php echo $qry_CJA_dispo_aff - $qry_CJA_dispo_uti ?>/<?php echo $qry_CJA_dispo_aff ?></td>																		
				  </tr>
				  <tr> 
					<td colspan="5" bgcolor="#CCCCCC" height="1"></td>
				  </tr>
				  <?php
				}
			  }
			  ?>
			  </table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	  <br>
		<?php
		if (@is_array($qry_jet)){ // Si on a cliqué sur un jeton ou si le partenaire n'a encore aucun jeton
		  ?>
		  <a name="#bas"></a>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Partenaires2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_jet[0]['prod_nom'].' - '.$qry_jet[0]['type_jeton'] ?></td>
			</tr>
		  </table>
		  <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="TX_Partenaires"><?php echo $t_jet_recap_at_part ?></td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr>
			  <td width="14"></td>
			  <td class="TX" align="center">&nbsp;</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"><a	name="table_jet"></a></td>
			  <td align="center" valign="top" class="TX"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
				  <tr align="center"> 
					<td height="1" colspan="7" bgcolor="#000000" ></td>
				  </tr>
				  <tr valign="middle"> 
					<td class="TX_bold"><a href="partenaires.php?partid=<?php echo $part_id ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&actif=<?php echo $_GET['actif'] ?>&order=date_attrib#table_jet"><?php echo $t_date_attrib ?></a></td>
					<td class="TX_bold" align="center"><a href="partenaires.php?partid=<?php echo $part_id ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&actif=<?php echo $_GET['actif'] ?>&order=nbre_jet#table_jet"><?php echo $t_jet_nbre ?></a></td>
					<td class="TX_bold" align="center"><a href="partenaires.php?partid=<?php echo $part_id ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&actif=<?php echo $_GET['actif'] ?>&order=date_deb#table_jet"><?php echo $t_date_deb_valid ?></a>
					</td>
					<td class="TX_bold" align="center"><a href="partenaires.php?partid=<?php echo $part_id ?>&prodid=<?php echo $_GET['prodid'] ?>&typid=<?php echo $_GET['typid'] ?>&actif=<?php echo $_GET['actif'] ?>&order=date_fin#table_jet"><?php echo $t_date_fin_valid ?></a></td>
					<td class="TX_bold" align="center"><?php echo $t_comment ?></td>
				  </tr>
				  <tr> 
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
				  </tr>
				  <?php
				  if (is_array($qry_jet)){
					  foreach($qry_jet as $jet){
						?>
						  <tr> 
							<td>
							<?php
							if ($_SESSION['droit']>5){
							?>
								<a href="#" onClick="MM_openBrWindow('admvak_edit_produit.php?jetid=<?php echo addslashes($jet['jet_id']) ?>','edit_jeton','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes')"><?php echo $jet['jet_date_attribution'] ?></a>
							<?php
							}else{
								echo $jet['jet_date_attribution'];
							}
							?>
							</td>
							
							<td align="center"><?php echo $jet['jet_nombre'] ?></td>
							<td align="center"><?php echo $jet['jet_deb_validite'] ?></td>
							<td align="center"><?php echo $jet['jet_fin_validite'] ?></td>
							<td align="center"><?php echo $jet['jet_comm1'].'<br>'.$jet['jet_comm2'] ?></td>
						  </tr>
						  <tr> 
							<td colspan="7" bgcolor="#CCCCCC" height="1"></td>
						  </tr>
						  <?php
					  }
				  }
				  
				  ?>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
		  <?php
		}
		
		if (@is_array($qry_repart)){ // Si on a cliqué sur un jeton ou si le partenaire n'a encore aucun jeton
		  ?>
		  <a name="#bas"></a>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Partenaires2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_info_prod[0]['prod_nom'].' - '.$qry_info_code[0]['code_libelle'] ?></td>
			</tr>
		  </table>
		  <table border="0" cellspacing="0" cellpadding="0"  width="100%" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="TX_Partenaires"><?php echo $t_jet_aff_certs ?></td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr>
			  <td width="14"></td>
			  <td class="TX" align="center">&nbsp;</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"><a	name="table_jet"></a></td>
			  <td align="center" valign="top" class="TX"> 
				<table width="450" border="0" cellspacing="0" cellpadding="2" class="TX">
				  <tr align="center"> 
					<td height="1" colspan="3" bgcolor="#000000" ></td>
				  </tr>
				  <tr valign="middle"> 
					<td class="TX_bold"><?php echo $t_nom ?></td>
					<td class="TX_bold"><?php echo $t_prenom ?></td>
					<td class="TX_bold" align="center"><?php echo $t_jet_dispo_aff2 ?></td>
				  </tr>
				  <tr> 
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
					<td height="1" bgcolor="#000000"></td>
				  </tr>
				  <?php
					  foreach($qry_repart as $repart){
						?>
						  <tr> 
							<td><?php echo $repart['cert_nom'] ?></td>
							<td><?php echo $repart['cert_prenom'] ?></td>
							<td align="center"><?php echo $repart['reste'].'/'. $repart['affecte'] ?></td>
						  </tr>
						  <tr> 
							<td colspan="3" bgcolor="#CCCCCC" height="1"></td>
						  </tr>
						  <?php
					  }
				  ?>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		  </table>
		  <?php
		}
		
	  }else{
		if (@$multi_rep){
			?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Partenaires"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;
			  <?php
			  if ($search_part != ''){
				echo htmlentities($search_part);
			  }else{
				echo $t_part_tous;
			  }
			  ?>
			  </td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="TX_Partenaires"><?php echo $t_parts ?></td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td>&nbsp;</td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="center" class="TX">
			    <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
					<tr><td style="text-align: right;" class="TX">
						<input type="checkbox" name="idpart_bloque" id="idpart_bloque" onClick="show_bloque();" value="1" <?php if($_GET['bloque']==0 || $_GET['bloque']=='' || !isset($_GET['bloque'])){ echo ' checked="checked"'; }?>><?php echo $t_part_ok_only ?>
					</td></tr>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="center" class="TX">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td class="TX_bold"><a href="partenaires.php?search_part=<?php echo $search_part ?>&order_part=nom&bloque=<?php echo $_GET['bloque'] ?>"><?php echo $t_nom ?></a></td>
						<td class="TX_bold"><a href="partenaires.php?search_part=<?php echo $search_part ?>&order_part=rs&bloque=<?php echo $_GET['bloque'] ?>"><?php echo $t_part_rs ?></a></td>
						<td class="TX_bold"><a href="partenaires.php?search_part=<?php echo $search_part ?>&order_part=cp&bloque=<?php echo $_GET['bloque'] ?>"><?php echo $t_cp ?></a></td>
						<td class="TX_bold"><a href="partenaires.php?search_part=<?php echo $search_part ?>&order_part=ville&bloque=<?php echo $_GET['bloque'] ?>"><?php echo $t_ville ?></a></td>
						<td class="TX_bold"><a href="partenaires.php?search_part=<?php echo $search_part ?>&order_part=nat&bloque=<?php echo $_GET['bloque'] ?>"><?php echo $t_nat ?></a></td>
						<td class="TX_bold"><?php echo $t_part_cert_act ?></td>
						<td class="TX_bold"><?php echo $t_etat ?></td>
					</tr>
					<?php
					foreach ($qry_part_list as $elmt){
						echo '<tr>
							<td class="TX"><a href="partenaires.php?partid='.$elmt['part_id'].'">'.$elmt['part_nom'].'</a></td>
							<td class="TX">'.$elmt['part_rs'].'</td>
							<td class="TX">';
							if ($elmt['part_cp']!= 'NULL'){
								echo $elmt['part_cp'];
							}
							echo '</td>
							<td class="TX">';
							if ($elmt['part_ville']!= 'NULL'){
								echo strtoupper($elmt['part_ville']);
							}
							echo '</td>
							<td class="TX">'.$elmt['code_libelle'].'</td>
							<td class="TX">';
							
							$sql_cpte_cert	= "select COUNT(certifie.cert_id) cpt from certifie where cert_part_id='".$elmt['part_id']."' AND ACTIF='1' AND CERT_DATE_SUPPRESSION IS NULL";
							$cpte_cert 		= $db->query($sql_cpte_cert);
							echo intval($cpte_cert[0]['cpt']);
							
							echo '</td>
							<td class="TX">';
							if ($elmt['part_bloque']=='1'){
								echo '<span style="font-weight: bold; color: #FF0000;">'.$t_bloque.'</span>';
							}else{
								echo '<span style="font-weight: bold; color: #99cc00;">'.$t_OK.'</span>';
							}
							echo '</td>
						</tr>';
					}
					?>
				</table>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		</table>
		<table width="100%">
			<tr><td>&nbsp;</td></tr>
		</table>
			<?php
		}
	}
	  ?>	  				
						</p>							
					</div><!-- .entry-content -->
				</article><!-- #post -->
			</div><!-- #content -->
		</div><!-- #primary -->
		</div><!-- #main -->
	</div>	  

	</body>
	</html>
<?php
}else{
	header('location:index.php');
}
?>