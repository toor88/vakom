<?php
session_start();
ob_start();

	// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	if ($_GET['questid']!=''){
		require_once ("../config/lib/connex.php");
		require_once ("../config/lib/db.oracle.php");
		$db = new db($conn);
		$sql_reponse = "SELECT choix_id FROM doc_a_info_reponse WHERE doc_id = '".$_GET['docid']."' AND num_zone =  '".$_GET['zoneid']."' AND doc_lang_id = '".$_GET['langid']."'";
		$query_reponse = $db->query($sql_reponse);
		if(is_array($query_reponse)){
			$choice_array = array();
			foreach($query_reponse as $old_choice){
				$choice_array[$old_choice['choix_id']] = '';
			}
		}
		
		if ($_GET['action']=='del_lang' && $_GET['langid'] >0){

			$sql_del_lang 	= "DELETE FROM QUEST_A_LANG WHERE QUEST_ID='".txt_db($_GET['questid'])."' AND LANG_ID='".txt_db($_GET['langid'])."'";
			$qry_del_lang	= $db->query($sql_del_lang);
			
			header('location:supadmin_crea_questionnaire2.php?questid='.$_GET['questid']).'&choix_lang='.$_GET['choix_lang'];
	
		}
		if ($_GET['action']=='del_question' && $_GET['questionid'] >0){

			$sql_del_question 	= "UPDATE QUEST_A_VAL SET VAL_USER_SUPPRESSION='".$_SESSION['vak_id']."', VAL_DATE_SUPPRESSION=SYSDATE WHERE VAL_CHOIX_ID='".txt_db($_GET['questionid'])."'";
			$qry_del_question	= $db->query($sql_del_question);
			
			header('location:supadmin_crea_questionnaire2.php?questid='.$_GET['questid']).'&choix_lang='.$_GET['choix_lang'];
	
		}
	

	// Selection de la liste des types de question disponibles
	$sql_type_quest_list = "SELECT * FROM CODE WHERE CODE_TABLE='TYPE_QUESTION' ORDER BY CODE_LIBELLE ASC";
	$qry_type_quest_list = $db->query($sql_type_quest_list);
	
	// Selection de la liste des questionnaires existants
	$sql_quest_list		= "SELECT * FROM QUESTIONNAIRE WHERE QUEST_DATE_SUPPRESSION IS NULL ORDER BY QUEST_NOM ASC";
	$qry_quest_list		= $db->query($sql_quest_list);
	
	#############################################################
	########            TRAITEMENT FORM0             ############
	#############################################################
		
		// Selection de la liste des types de question disponibles
		$sql_type_quest_list = "SELECT * FROM CODE WHERE CODE_TABLE='TYPE_QUESTION' ORDER BY CODE_LIBELLE ASC";
		$qry_type_quest_list = $db->query($sql_type_quest_list);
		
		// Sélection des informations de base du questionnaire
		$sql_info_questionnaire	= "SELECT * FROM QUESTIONNAIRE WHERE QUEST_ID='".txt_db($_GET['questid'])."'";
		$qry_info_questionnaire	= $db->query($sql_info_questionnaire);
		
		// Sélection des parametres de langue des textes généraux du questionnaire
		if (is_array($qry_info_questionnaire)){ // Si le questionnaire est trouvé
			$sql_info_lang_questionnaire	= "SELECT * FROM QUEST_A_LANG, LANGUE WHERE LANGUE.LANG_ID=QUEST_A_LANG.LANG_ID AND QUEST_A_LANG.QUEST_ID='".txt_db($_GET['questid'])."'";
			$qry_info_lang_questionnaire	= $db->query($sql_info_lang_questionnaire);
			//echo $sql_info_lang_questionnaire;
			if ($_GET['choix_lang']>0){
				$choix_lang = $_GET['choix_lang'];
			}else{
				$choix_lang = 1;
			}
			$sql_question = "SELECT * FROM CHOIX, QUEST_A_VAL WHERE CHOIX.CHOIX_ID=QUEST_A_VAL.VAL_CHOIX_ID AND VAL_QUEST_ID='".txt_db($_GET['questid'])."' AND CHOIX.CHOIX_LANG_ID='".$choix_lang."' AND QUEST_A_VAL.VAL_USER_SUPPRESSION IS NULL ORDER BY QUEST_A_VAL.VAL_ORDRE ASC, choix_question ASC";
			//echo $sql_question;
			$qry_question = $db->query($sql_question);
			
			$sql_langue = "SELECT LANGUE.LANG_ID, LANGUE.LANG_LIBELLE FROM LANGUE, QUEST_A_LANG WHERE QUEST_A_LANG.LANG_ID=LANGUE.LANG_ID AND QUEST_A_LANG.QUEST_ID='".txt_db($_GET['questid'])."' AND LANGUE.LANG_ID NOT IN ('1')";
			$qry_langue = $db->query($sql_langue);
			
			?>
			<script type="text/javascript">
			function verifier_del_quest(quest_nom){
				if(confirm('<?php echo $t_supprimer_question_tabeau_1 ?> ' + quest_nom + '\r\n<?php echo $t_supprimer_question_tabeau_2 ?>')){
					document.form2.submit();
				}
			}
			
			function gd_public(){
				if (document.form1.grand_public.checked==true){
					document.getElementById('options_quest').style.display='inline';
				}else{
					document.getElementById('options_quest').style.display='none';
				}
			}			function verif_del(){
				if (confirm('<?php echo $t_supprimer_questions_select ?>')){
					document.form1.submit();
				}
			}

			function del_langue(langid){
				if (confirm('<?php echo $t_supprimer_textes ?>')){
					document.location.href='supadmin_crea_questionnaire2.php?questid=<?php echo $_GET['questid']?>&choix_lang<?php echo $_GET['choix_lang']?>&action=del_lang&langid='+langid;
				}
			}

			function delete_question(questionid){
				if (confirm('<?php echo $t_supprimer_question ?>')){
					document.location.href='supadmin_crea_questionnaire2.php?questid=<?php echo $_GET['questid']?>&choix_lang<?php echo $_GET['choix_lang']?>&action=del_question&questionid='+questionid;
				}
			}
						
			function change_langue(choix_lang){
				document.location.href='supadmin_crea_questionnaire2.php?questid=<?php echo $_GET['questid']?>&choix_lang='+choix_lang;
			}
			
			function MM_openBrWindow(theURL,winName,features) { //v2.0
			  window.open(theURL,winName,features);
			}
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			</script>
				
	  <?php
	  if(is_array($qry_info_questionnaire)){
	   ?>
				<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
				
				<?php
				if (is_array($qry_question)){
				?>
					<tr> 
					  <td width="14"></td>
					  <td align="left" class="TX"> 
					  <div style="width:100%;height:200px;overflow-Y:auto;"> 
						<table  cellspacing="0" cellpadding="2" bgcolor="F1F1F1"  border="1" style="width:100%;overflow:hidden;">
							<tr align="center"> 
							<td class="TX_bold">Question </td>
							<td class="TX_bold">Choix 1</td>
							<td class="TX_bold">Choix 2</td>
							<td class="TX_bold">Choix 3</td>
							<td class="TX_bold">Choix 4</td>
							<td class="TX_bold" align="center">Sélection<br><input type="checkbox" id="<?php echo $_GET['zoneid'] .'_'.$_GET['questid'];?>" name="select_all" onclick="che_all('<?php echo txt_db($_GET['questid']).'_'.$_GET['zoneid']; ?>', this)" value="<?php echo $_GET['zoneid'];?>">Tous</td>
						 </tr>
						  
						  <?php
						  $count = 0;
						  foreach($qry_question as $question){
						  $checked = '';
							if($query_reponse){
								if(array_key_exists($question['choix_id'], $choice_array))
									$checked = 'checked="checked"';
							}
							?>
							  <tr>
								<td align="center" class="TX"><?php echo nl2br($question['choix_question'].'<br>'.$question['choix_question_comm']) ?>&nbsp;</td>
								<td class="TX"><?php if (trim($question['choix_1'])!='') echo '<b>'.nl2br($question['choix_1']).'</b><br />'.$question['val_pi_1'].'+ '.$question['val_pn_1'].'-' ?>&nbsp;</td>
								<td class="TX"><?php if (trim($question['choix_2'])!='') echo '<b>'.nl2br($question['choix_2']).'</b><br />'.$question['val_pi_2'].'+ '.$question['val_pn_2'].'-' ?>&nbsp;</td>
								<td class="TX"><?php if (trim($question['choix_3'])!='') echo '<b>'.nl2br($question['choix_3']).'</b><br />'.$question['val_pi_3'].'+ '.$question['val_pn_3'].'-' ?>&nbsp;</td>
								<td class="TX"><?php if (trim($question['choix_4'])!='') echo '<b>'.nl2br($question['choix_4']).'</b><br />'.$question['val_pi_4'].'+ '.$question['val_pn_4'].'-' ?>&nbsp;</td>
								<td class="TX" align="center"><input type="checkbox" <?php echo $checked; ?> id="<?php echo txt_db($_GET['questid']).'_'.$_GET['zoneid'].'_'.$count++; ?>" name="<?php echo txt_db($_GET['questid']).'_'.$_GET['zoneid']; ?>[]" value="<?php echo $question['choix_id']?>" onClick="decoche('<?php echo $_GET['zoneid'].'_'.$_GET['questid']; ?>');">
								<!--<input type="hidden" name="hid_choix_id[]" value="<?php echo $question['choix_id']?>">--></td>
							  </tr>
							<?php
							}
							?>
						</table>
						</div>
					  </td>
					  <td width="14"></td>
					</tr>
				<?php
				}
				?>	
			  </table>
			  <br>  
			  <br>
			  
			  <?php
			  }
			  ?>
		<?php
		}
	}
}else{
	include('no_acces.php');
}

$content = ob_get_contents();

ob_end_clean();

echo $content;

?>