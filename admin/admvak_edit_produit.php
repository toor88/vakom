<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']>5){
	if ($_GET['jetid'] && $_GET['jetid']>0){

		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");

		$db = new db($conn);
		if ($_POST['jetid']!=''){
		
			$sql_update_jet 	= "UPDATE JETON SET JET_NOMBRE = '".txt_db($_POST['nbre'])."', JET_TARIF = '".txt_db($_POST['tarif'])."',
			JET_DEB_VALIDITE = TO_DATE('".txt_db($_POST['JJ'])."/".txt_db($_POST['MM'])."/".txt_db($_POST['AAAA'])."', 'DD/MM/YYYY'), 
			JET_FIN_VALIDITE = TO_DATE('".txt_db($_POST['JJ2'])."/".txt_db($_POST['MM2'])."/".txt_db($_POST['AAAA2'])."', 'DD/MM/YYYY'), 	
			JET_COMM1 = '".txt_db($_POST['comm1'])."', 
			JET_COMM2 = '".txt_db($_POST['comm2'])."'
			WHERE JET_ID = '".$_POST['jetid']."'";
	
			$qry_update_jet			= $db->query($sql_update_jet);
			
			$delta				= (intval($_POST['hid_old_jetons'])-intval($_POST['nbre']));
			
			$sql_update_jet2	= "update jeton_corresp_cert set affecte=affecte-".txt_db($delta)." where jet_id='".txt_db($_GET['jetid'])."' and cert_jet_id=-1";
			$qry_update_jet2	= $db->query($sql_update_jet2);
			//echo $sql_update_jet2;
			//echo $sql_update_jet;
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			<?php
		}
		
		$sql_jeton				= "SELECT JETON.*, PARTENAIRE.PART_NOM, PARTENAIRE.PART_ID, PARTENAIRE.PART_NATURE_CODE_ID,  
		TO_CHAR(JETON.JET_DEB_VALIDITE, 'DD') DD_DEB, TO_CHAR(JETON.JET_DEB_VALIDITE, 'MM') MM_DEB, TO_CHAR(JETON.JET_DEB_VALIDITE, 'YYYY') YYYY_DEB, 
		TO_CHAR(JETON.JET_FIN_VALIDITE, 'DD') DD_FIN, TO_CHAR(JETON.JET_FIN_VALIDITE, 'MM') MM_FIN, TO_CHAR(JETON.JET_FIN_VALIDITE, 'YYYY') YYYY_FIN 
		FROM JETON, PARTENAIRE WHERE JETON.JET_PART_ID=PARTENAIRE.PART_ID AND JET_ID='".txt_db($_GET['jetid'])."'";
		//echo $sql_jeton;
		$qry_jeton 				= $db->query($sql_jeton);
		
		
		// On cherche si le partenaire a un contrat
		$sql_contrat			= "SELECT CONT_PART_ID FROM PART_A_CONTRAT WHERE CONT_PART_ID='".txt_db($qry_jeton[0]['part_id'])."' AND TO_CHAR(CONT_DATE_DEB,'YYYYMMDD')<=TO_CHAR(SYSDATE,'YYYYMMDD') AND TO_CHAR(CONT_DATE_FIN,'YYYYMMDD')>=TO_CHAR(SYSDATE,'YYYYMMDD')";
		//echo $sql_contrat;
		$qry_contrat	 		= $db->query($sql_contrat);
		
		// On check les jetons
		//$sql_chk_jetons 		= "select greatest(sum(affecte),sum(utilise)) utilise from v_jeton_synthese where jet_id='".txt_db($_GET['jetid'])."' and cert_jet_id<>'-1'";
		$sql_chk_jetons 		= "select (select nvl(greatest(sum(affecte),sum(utilise)),0) from v_jeton_synthese where jet_id=".txt_db($_GET['jetid'])." and cert_jet_id<>-1) + (select nvl(sum(utilise),0) from v_jeton_synthese where jet_id=".txt_db($_GET['jetid'])." and cert_jet_id=-1) utilise from dual";
		//echo $sql_chk_jetons;
		$qry_chk_jetons			= $db->query($sql_chk_jetons);
		
		$sql_prod_list 			= "SELECT * FROM PRODUIT ORDER BY PROD_NOM";
		$qry_prod_list 			= $db->query($sql_prod_list);
		
		$sql_type_jet_list 		= "SELECT * FROM CODE WHERE CODE_TABLE='TYPE_JETON'";
		$qry_type_jet_list 		= $db->query($sql_type_jet_list);
		
		if (is_array($qry_jeton)){
		?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--

function DecomposeDate(LeParam1){ 

// Sépare les jours, les mois et les années dans une date de type "22/05/1981"
// Renvoye le tout dans un tableau de taille 3

LeRetour = new Array(3);
LeJour="";
LeMois="";
LeAnnee="";

// Extraction du jour
i=0;
while((LeParam1.charAt(i)!="/")&&(i<10)){
LeJour+=LeParam1.charAt(i);
i++;
}
if(LeJour.charAt(0)=="0"){
LeJour=LeJour.charAt(1);
}
LeParam1=LeParam1.substring(i+1,LeParam1.length);

// Extraction du mois
i=0;
while((LeParam1.charAt(i)!="/")&&(i<10)){
LeMois+=LeParam1.charAt(i);
i++;
}
if(LeMois.charAt(0)=="0"){
LeMois=LeMois.charAt(1);
}
LeParam1=LeParam1.substring(i+1,LeParam1.length);


// Extraction de l'année
LeAnnee=LeParam1;
LeRetour[0]=LeJour;
LeRetour[1]=LeMois;
LeRetour[2]=LeAnnee;
return LeRetour;
}
			
function DateAnglaise(LeParam1,LeParam2){

LaDate = new Array(3);
LaDate = DecomposeDate(LeParam1);

LeRetour = LaDate[2]+LeParam2+LaDate[1]+LeParam2+LaDate[0];
return LeRetour;
}
			
function ComparerDates(LeParam1,LeParam2){
// Compare 2 dates au format jj/mm/aaaa
// Renvoye 0 si égalité, 1 si la première est supérieure, sinon 2
var LeParam1 = DateAnglaise(LeParam1,"/");// Ne pas oublier d'utiliser cette fonction
// pour convertir en date anglaise, sinon le 05/07/2003 sera compris "7 mai 2003" par JavaScript

var LeParam2 = DateAnglaise(LeParam2,"/");

LeParam1 = Date.parse(LeParam1);
LeParam2 = Date.parse(LeParam2);

if (LeParam1 == LeParam2) { 
return 0;
}
if (LeParam1 > LeParam2){
return 1;
}else{
return 2;
}
}
			
			function verif(){
					error = '';
					error1 = '';
					error2 = '';
					error_tarif = false;
					
				if(document.form.hid_contrat_valid.value==0){
					if(confirm("<?php echo $t_aucun_contrat_part ?>")){
						error_contrat = false;
					}
					else
					{
						error += "<?php echo $t_contrat_invalide ?>\r\n";
					}
				}
				
				if(document.form.type.options[document.form.type.selectedIndex].value == 10){
					if(document.form.tarif.value != document.form.hid_tarif.value){
						error_tarif = true;			
					}
				}
				if (document.form.nbre.value<0){
					error += "<?php echo $t_jet_nbre_oblig ?>\n";
				}else{
					if(Number(document.form.hid_old_jetons.value) == Number(document.form.hid_check_jetons.value)){
						if(Number(document.form.nbre.value) < Number(document.form.hid_check_jetons.value) || Number(document.form.nbre.value) > Number(document.form.hid_old_jetons.value)){
						error += "<?php echo $t_nbre_jet_egal ?> "+document.form.hid_check_jetons.value+"\n";
						}
					}else{
						if(Number(document.form.nbre.value) < Number(document.form.hid_check_jetons.value) || Number(document.form.nbre.value) > Number(document.form.hid_old_jetons.value)){
						error += "<?php echo $t_nbre_jet_entre ?> "+document.form.hid_check_jetons.value+" <?php echo $t_et ?> "+ document.form.hid_old_jetons.value +"\n";
						}
					}
					
				}
				
				// verif du format date du début
				if (document.form.JJ.value<1 || document.form.JJ.value>31 || document.form.JJ.value.length<2){
					error1 = true;
				}
				if (document.form.MM.value<1 || document.form.MM.value>12 || document.form.MM.value.length<1){
					error1 = true;
				}
				if (document.form.AAAA.value.length<4){
					error1 = true;
				}
				if (error1==true){
					error +="<?php echo $t_mauvaise_date_deb ?>\n";
				}
				
				// Verif du format date de fin
				if (document.form.JJ2.value<1 || document.form.JJ2.value>31 || document.form.JJ2.value.length<2){
					error2 = true;
				}
				if (document.form.MM2.value<1 || document.form.MM2.value>12 || document.form.MM2.value.length<1){
					error2 = true;
				}
				if (document.form.AAAA2.value.length<4){
					error2 = true;
				}
				if (ComparerDates(document.form.JJ2.value + '/' + document.form.MM2.value + '/' + document.form.AAAA2.value,document.form.JJ.value + '/' + document.form.MM.value + '/' + document.form.AAAA.value) ==2){
					error2 = true;
				}
				
				if (error2==true){
					error +="<?php echo $t_mauvaise_date_fin ?>\n";
				}
				
				if (error!=''){
					alert(error);
				}else{
					if(error_tarif){
						if(confirm("<?php echo $t_sur_de_mod_tarif ?>")){
							document.form.submit();
						}
					}else{
						document.form.submit();
					}
				}
			}
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function pass(champ){
				if (champ==1){
					if (document.form.JJ.value.length==2){
						document.form.MM.value='';
						document.form.MM.focus();
					}
				}
				if (champ==2){
					if (document.form.MM.value.length==2){
						document.form.AAAA.value='';
						document.form.AAAA.focus();
					}
				}
				if (champ==3){
					if (document.form.JJ2.value.length==2){
						document.form.MM2.value='';
						document.form.MM2.focus();
					}
				}
				if (champ==4){
					if (document.form.MM2.value.length==2){
						document.form.AAAA2.value='';
						document.form.AAAA2.focus();
					}
				}
			}
			
			
			//-->
			</script>
			</head>

			<body bgcolor="#FFFFFF" text="#000000">
			<form method="post" action="#" name="form">
			  <table border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td align="left" class="TX">
				  <table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Partenaires2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo strtoupper($qry_jeton[0]['part_nom']) ?></td>
					</tr>
				  </table>
					<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="800">
					  <tr> 
						<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
						<td height="14"></td>
						<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
					  </tr>
						<tr> 
						  <td width="14"></td>
						  <td class="TX_Partenaires"><?php echo $t_jet_attrib_part ?></td>
						  <td width="14"></td>
						</tr>
						<tr> 
						  <td width="14"></td>
						  <td bgcolor="#666666" height="1"></td>
						  <td width="14"></td>
						</tr>
						<tr> 
						  <td width="14"></td>
						  <td align="center" class="TX">&nbsp;</td>
						  <td width="14"></td>
						</tr>
					  <tr> 
						<td width="14"></td>
						<td align="center" valign="top" class="TX"> 
						  <table border="0" cellspacing="0" cellpadding="0">
							<tr> 
							  <td class="TX" height="40"><?php echo $t_date_attrib ?> : </td>
							  <td class="TX"><?php echo $qry_jeton[0]['jet_date_attribution'] ?></td>
							  <td class="TX">&nbsp;</td>
							  <td class="TX">&nbsp;</td>
							</tr>
							<tr> 
							  <td class="TX" height="40"><?php echo $t_produit ?> :</td>
							  <td class="TX"> 
								<select disabled id="liste_prdt" name="liste_prdt" class="form_ediht_Partenaires" onchange="listing_pack(document.getElementById('liste_prdt').options[document.getElementById('liste_prdt').selectedIndex].value, <?php echo $qry_jeton[0]['part_nature_code_id'] ?>,document.getElementById('type').options[document.getElementById('type').selectedIndex].value)">
								  <option value="0" selected="selected"><?php echo $t_produits_tous ?></option>
								  <?php
								  if (is_array($qry_prod_list)){
									foreach($qry_prod_list as $produit){
										unset($selected);
										if ($produit['prod_id'] == $qry_jeton[0]['jet_prod_id']){
											$selected = ' selected="selected" ';
										}
										echo '<option value="'.$produit['prod_id'].'"'.$selected.'>'.$produit['prod_nom'].'</option>';
									}
								  }
								  ?>
								</select>
							  </td>
							  <td class="TX">&nbsp;</td>
							  <td class="TX">&nbsp;</td>
							</tr>
							<tr> 
							  <td class="TX" height="40"><?php echo $t_jet_type ?> : </td>
							  <td  class="TX"> 
								<select disabled name="type" id="type" class="form_ediht_Partenaires" onchange="masque(document.getElementById('type').options[document.getElementById('type').selectedIndex].value)">
								  <?php
								  if (is_array($qry_type_jet_list)){
									foreach($qry_type_jet_list as $type_jet){
										if ($type_jet['code_id'] == $qry_jeton[0]['jet_type_jeton_code_id']){
											$selected = ' selected="selected" ';
										}else{
											$selected=' ';
										}
										echo '<option'.$selected.'value="'.$type_jet['code_id'].'">'.$type_jet['code_libelle'].'</option>';
									}
								  }
								  ?>
								</select>
							  </td>
							  <td  class="TX">&nbsp;</td>
							  <td  class="TX"> 
&nbsp;
							  </td>
							</tr>
							<tr> 
							  <td class="TX" height="40"><?php echo $t_jet_nbre ?> :</td>
							  <td  class="TX"> 
								<input type="text" name="nbre" size="5" class="form_ediht_Partenaires" value="<?php echo $qry_jeton[0]['jet_nombre'] ?>">
							  </td>
							  <td  class="TX"><?php echo $t_tarif_unit ?> : </td>
							  <td  class="TX"> 
								<input type="text" name="tarif" size="5" class="form_ediht_Partenaires" value="<?php echo $qry_jeton[0]['jet_tarif'] ?>">
								<input type="hidden" name="hid_tarif" value="<?php echo $qry_jeton[0]['jet_tarif'] ?>">
								&euro; <?php echo $t_HT ?> </td>
							</tr>
							<tr> 
							  <td class="TX" height="40"><?php echo $t_date_deb_valid ?>
								:&nbsp;&nbsp; </td>
							  <td class="TX"> 
								<input type="text" name="JJ" size="2" onkeyup="pass(1)" class="form_ediht_Partenaires" value="<?php echo $qry_jeton[0]['dd_deb'] ?>" maxlength="2">
								/ 
								<input type="text" name="MM" size="2" onkeyup="pass(2)" class="form_ediht_Partenaires" value="<?php echo $qry_jeton[0]['mm_deb'] ?>" maxlength="2">
								/ 
								<input type="text" name="AAAA" size="4" class="form_ediht_Partenaires" value="<?php echo $qry_jeton[0]['yyyy_deb'] ?>" maxlength="4">
								&nbsp;&nbsp;&nbsp;&nbsp;</td>
							  <td  class="TX"><?php echo $t_date_fin_valid ?> :</td>
							  <td  class="TX"> 
								<input type="text" name="JJ2" size="2" onkeyup="pass(3)" class="form_ediht_Partenaires" value="<?php echo $qry_jeton[0]['dd_fin'] ?>" maxlength="2">
								/ 
								<input type="text" name="MM2" size="2" onkeyup="pass(4)" class="form_ediht_Partenaires" value="<?php echo $qry_jeton[0]['mm_fin'] ?>" maxlength="2">
								/ 
								<input type="text" name="AAAA2" size="4" class="form_ediht_Partenaires" value="<?php echo $qry_jeton[0]['yyyy_fin'] ?>" maxlength="4">
							  </td>
							</tr>
							<tr> 
							  <td class="TX" height="40"><?php echo $t_comment ?> :</td>
							  <td  class="TX" colspan="3"> 
								<input type="text" name="comm1" size="94" class="form_ediht_Partenaires" value="<?php echo $qry_jeton[0]['jet_comm1'] ?>">
							  </td>
							</tr>
							<tr>
							  <td class="TX" height="40">&nbsp;</td>
							  <td  class="TX" colspan="3">
								<input type="text" name="comm2" size="94" class="form_ediht_Partenaires" value="<?php echo $qry_jeton[0]['jet_comm2'] ?>">
							  </td>
							</tr>
						  </table>
						</td>
						<td width="14"></td>
					  </tr>
					  <tr> 
						<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
						<td height="14"></td>
						<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
					  </tr>
					</table>
				  </td>
				</tr>
				<tr> 
				  <td align="center">
					  <?php
					  // Si le partenaire a un contrat en cours de validité, on le fait savoir
					  if(is_array($qry_contrat)){
						echo '<input type="hidden" name="hid_contrat_valid" value="1">';
					  }else{
						echo '<input type="hidden" name="hid_contrat_valid" value="0">';
					  }
					  echo '<input type="hidden" name="hid_old_jetons" value="'.intval($qry_jeton[0]['jet_nombre']).'">';
					  echo '<input type="hidden" name="hid_check_jetons" value="'.intval($qry_chk_jetons[0]['utilise']).'">';
					  ?>
					<input type="hidden" name="jetid" value="<?php echo $qry_jeton[0]['jet_id'] ?>">
					<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_partenaire" onclick="verif();">
				  </td>
				</tr>
				<tr> 
				  <td align="right" width="180">&nbsp; </td>
				</tr>
			  </table>
			</form>
			</body>
			</html>
			<?php
		}
	}
}else{
	include('no_acces.php');
}
?>