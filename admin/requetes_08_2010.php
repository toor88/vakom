<html>
<head>
<title>Vakom - Requetes</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">

<script language="JavaScript">
<!--

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td align="center"> 
			<?php
			include("menu_top.php");
			?>
		  </td>
		</tr>
	  </table>
	  <br>
      <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  
    <td class="Titre_Requetes"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;REQU&Ecirc;TES</td>
		</tr>
	  </table>
	  
<form method="post" action="">
  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="fond_tablo_requetes">
    <tr>
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="center"> 
        <table border="0" cellspacing="0" cellpadding="0" align="center" class="TX">
          <tr> 
            <td align="left" class="TX"> 
              <input type="radio" name="radiobutton" value="radiobutton">
              S&eacute;lectionner une requ&ecirc;te existante :&nbsp;&nbsp;</td>
            <td align="left" class="TX"> 
              <select name="select" class="form_ediht_Requetes">
                <option>Nom Requete 1</option>
                <option>Nom Requete 2</option>
              </select>
            </td>
          </tr>
          <tr> 
            <td align="left" class="TX"> 
              <input type="radio" name="radiobutton" value="radiobutton">
              Cr&eacute;er un nouvelle requ&ecirc;te :&nbsp;&nbsp;</td>
            <td align="left" class="TX"> 
              <input type="text" name="nom_req2" class="form_ediht_Requetes" size="50">
            </td>
          </tr>
          <tr>
            <td align="left" class="TX">
              <input type="radio" name="radiobutton" value="radiobutton">
              Dupliquer une requ&ecirc;te :&nbsp;&nbsp;</td>
            <td align="left" class="TX">
              <input type="text" name="nom_req23" class="form_ediht_Requetes" size="50">
            </td>
          </tr>
        </table>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
    </tr>
  </table>
  <br>
  <table cellpadding="0" cellspacing="0" width="961" align="center">
    <tr>
      <td align="center"> 
        <input type="button" name="submit" value="VALIDER" class="bn_valider_requete">
      </td>
    </tr>
  </table>
</form>
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  
    <td class="Titre_Requetes"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;NOM 
      DE LA REQU&Ecirc;TE</td>
				</tr>
			  </table>
<br>
<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
  <tr> 
    <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
  </tr>
  <tr> 
    <td width="14"></td>
    <td align="center" class="TX"> 
      <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="600">
        <tr align="center"> 
          <td class="TX"> 
            <input type="button" name="VIS" value="VISUALISER" class="bn_valider_requete">
          </td>
          <td class="TX"> 
            <input type="button" name="EXP" value="EXPORTER" class="bn_valider_requete">
          </td>
        </tr>
      </table>
    </td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
  </tr>
</table>
<br>
<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
  <tr> 
    <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
  </tr>
  <tr> 
    <td width="14"></td>
    <td align="left" class="TX_Requetes">DONNEES </td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="1"></td>
    <td align="center" class="TX" height="1" bgcolor="#666666"></td>
    <td width="14" height="1"></td>
  </tr>
  <tr>
    <td width="14"></td>
    <td align="center" class="TX">&nbsp;</td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14"></td>
    <td align="center" class="TX"> 
      <table border="0" cellspacing="1" cellpadding="0" class="TX" bgcolor="#000000" width="754">
        <tr> 
          <td class="TX_bold">Donn&eacute;es</td>
          <td class="TX_bold" align="center">Ordre</td>
          <td class="TX_bold" align="center">Tri&eacute;e</td>
          <td class="TX_bold" align="center">Affich&eacute;e</td>
          <td class="TX_bold" align="center">Select</td>
        </tr>
        <tr bgcolor="F1F1F1"> 
          <td>NOM CERTIFIE</td>
          <td align="center"> 
            <input type="text" name="od1" class="form_ediht_Requetes" size="2">
          </td>
          <td align="center" valign="middle"> 
            <input type="checkbox" name="checkbox17" value="checkbox">
          </td>
          <td align="center" valign="middle"> 
            <input type="checkbox" name="checkbox172" value="checkbox">
          </td>
          <td align="center" valign="middle"> 
            <input type="checkbox" name="checkbox1722" value="checkbox">
          </td>
        </tr>
      </table>
      <br>
      <table width="754" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td>&nbsp;</td>
          <td align="right" width="150"> 
            <input type="button" name="ajout" value="Ajouter une donn&eacute;e" class="bn_ajouter" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
          </td>
          <td align="right" width="100"> 
            <input type="button" name="supp" value="Supprimer" class="bn_ajouter" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
          </td>
        </tr>
      </table>
    </td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
  </tr>
</table>
<br>
<table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
  <tr> 
    <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
  </tr>
  <tr> 
    <td width="14"></td>
    <td align="left" class="TX_Requetes">NOUVELLES DONNEES </td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="1"></td>
    <td align="center" class="TX" height="1" bgcolor="#666666"></td>
    <td width="14" height="1"></td>
  </tr>
  <tr>
    <td width="14"></td>
    <td align="center" class="TX">&nbsp;</td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14"></td>
    <td align="center" class="TX">
<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="450">
        <tr> 
          <td align="left" class="TX"> 
            <input type="radio" name="Bn1" value="1" onClick="change_type(1)" <?php echo $selected1 ?>>
            Candidats&nbsp;&nbsp; 
            <input type="radio" name="Bn2" value="2" onClick="change_type(2)" <?php echo $selected2 ?>>
            R&eacute;ponses&nbsp;&nbsp; 
            <input type="radio" name="Bn3" value="3" onClick="change_type(3)" <?php echo $selected3 ?>>
            Produits&nbsp;&nbsp; 
            <input type="radio" name="Bn4" value="4" onClick="change_type(4)" <?php echo $selected4 ?>>
            Certifi&eacute;s&nbsp; 
            <input type="radio" name="Bn4" value="4" onClick="change_type(4)" <?php echo $selected4 ?>>
            Partenaires&nbsp; &nbsp; </td>
        </tr>
        <tr> 
          <td align="left" class="TX">&nbsp;</td>
        </tr>
        <tr> 
          <td align="left" class="TX"> 
            <table border="0" cellspacing="1" cellpadding="0" class="TX" bgcolor="#000000" width="450">
              <tr> 
                <td class="TX_bold">Donn&eacute;es &agrave; extraire</td>
                <td class="TX_bold" align="center">Select</td>
              </tr>
              <tr bgcolor="F1F1F1"> 
                <td>Nom de la soci&eacute;t&eacute;</td>
                <td align="center"> 
                  <input type="checkbox" name="checkbox" value="checkbox">
                </td>
              </tr>
              <tr bgcolor="F1F1F1"> 
                <td>Nature</td>
                <td align="center"> 
                  <input type="checkbox" name="checkbox2" value="checkbox">
                </td>
              </tr>
              <tr bgcolor="F1F1F1"> 
                <td valign="top">Adresse</td>
                <td valign="top" align="center"> 
                  <input type="checkbox" name="checkbox3" value="checkbox">
                </td>
              </tr>
              <tr bgcolor="F1F1F1"> 
                <td valign="top">Code Postal</td>
                <td valign="top" align="center"> 
                  <input type="checkbox" name="checkbox4" value="checkbox">
                </td>
              </tr>
              <tr bgcolor="F1F1F1"> 
                <td valign="top">Ville</td>
                <td valign="top" align="center"> 
                  <input type="checkbox" name="checkbox5" value="checkbox">
                </td>
              </tr>
              <tr bgcolor="F1F1F1"> 
                <td valign="top">Pays</td>
                <td valign="top" align="center"> 
                  <input type="checkbox" name="checkbox6" value="checkbox">
                </td>
              </tr>
              <tr bgcolor="F1F1F1"> 
                <td valign="top">Client bloqu&eacute;</td>
                <td valign="top" align="center"> 
                  <input type="checkbox" name="checkbox7" value="checkbox">
                </td>
              </tr>
              <tr bgcolor="F1F1F1"> 
                <td valign="top">Sous contrat</td>
                <td valign="top" align="center"> 
                  <input type="checkbox" name="checkbox8" value="checkbox">
                </td>
              </tr>
              <tr bgcolor="F1F1F1"> 
                <td valign="top">Type de contrat</td>
                <td valign="top" align="center"> 
                  <input type="checkbox" name="checkbox9" value="checkbox">
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td align="left" class="TX">&nbsp;</td>
        </tr>
        <tr> 
          <td align="right" class="TX"> 
            <input type="button" name="ajout2" value="Ajouter " class="bn_ajouter" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
            &nbsp;&nbsp; 
            <input type="button" name="annul" value="Annuler" class="bn_ajouter" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
          </td>
        </tr>
      </table>
    </td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
  </tr>
</table>
<br>
<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
  <tr> 
    <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
  </tr>
  <tr> 
    <td width="14"></td>
    <td align="left" class="TX_Requetes">FILTRES</td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="1"></td>
    <td align="center" class="TX" height="1" bgcolor="#666666"></td>
    <td width="14" height="1"></td>
  </tr>
  <tr> 
    <td width="14"></td>
    <td align="center" class="TX">&nbsp;</td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14"></td>
    <td align="center" class="TX"> 
      <table border="0" cellspacing="1" cellpadding="0" class="TX" bgcolor="#000000" width="754">
        <tr> 
          <td class="TX_bold">Donn&eacute;es</td>
          <td class="TX_bold" align="center">Op&eacute;rateurs</td>
          <td class="TX_bold" align="center">Valeurs</td>
          <td class="TX_bold" align="center">Modifier</td>
          <td class="TX_bold" align="center">Suppr</td>
        </tr>
        <tr bgcolor="F1F1F1"> 
          <td bgcolor="F1F1F1">Nom de la soci&eacute;t&eacute;</td>
          <td align="center" bgcolor="F1F1F1"> &lt;</td>
          <td align="center" valign="middle"> R</td>
          <td align="center" valign="middle"><img src="modifier.png" width="12" height="12" border="0"></td>
          <td align="center" valign="middle"> <img src="../images/icon_supp2.gif" width="11" height="12" border="0"></td>
        </tr>
        <tr bgcolor="F1F1F1"> 
          <td valign="top">Produit</td>
          <td valign="top" align="center"> =</td>
          <td valign="middle" align="center"> OPR</td>
          <td valign="middle" align="center">&nbsp;</td>
          <td valign="middle" align="center"> <img src="../images/icon_supp2.gif" width="11" height="12" border="0"></td>
        </tr>
        <tr bgcolor="F1F1F1"> 
          <td valign="top">Montant droit d&#146;entr&eacute;e</td>
          <td valign="top" align="center"> &gt;</td>
          <td valign="middle" align="center"> .....</td>
          <td valign="middle" align="center">&nbsp;</td>
          <td valign="middle" align="center"> <img src="../images/icon_supp2.gif" width="11" height="12" border="0"></td>
        </tr>
      </table>
      <br>
      <table width="754" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right"> 
            <input type="button" name="ajout_F" value="Ajouter un filtre" class="bn_ajouter" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
          </td>
        </tr>
      </table>
    </td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
  </tr>
</table>
<br>
<table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
  <tr> 
    <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
  </tr>
<tr> 
    <td width="14"></td>
    <td align="left" class="TX_Requetes">NOUVEAUX FILTRES</td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="1"></td>
    <td align="center" class="TX" height="1" bgcolor="#666666"></td>
    <td width="14" height="1"></td>
  </tr>
  <tr>
    <td width="14"></td>
    <td align="center" class="TX">&nbsp;</td>
    <td width="14"></td>
  </tr>  <tr> 
    <td width="14"></td>
    <td align="center" class="TX">
<table width="540" border="0" cellspacing="1" cellpadding="0" bgcolor="#000000">
        <tr> 
          <td align="left" class="TX_bold">Donn&eacute;es</td>
          <td align="left" class="TX_bold">&nbsp;&nbsp;Op&eacute;rateurs&nbsp;&nbsp;</td>
          <td align="left" class="TX_bold">Valeurs</td>
        </tr>
        <tr bgcolor="F1F1F1"> 
          <td align="left" class="TX"> 
            <select name="select2" class="form_ediht_Requetes">
              <option>Nom de la soci&eacute;t&eacute;</option>
              <option>Nature</option>
              <option>Code postal</option>
              <option>...</option>
              <option>Mois de facturation de renouvellement</option>
            </select>
          </td>
          <td align="center" class="TX"> 
            <select name="op" class="form_ediht_Requetes">
              <option>&gt;</option>
              <option>&gt; =</option>
              <option>=</option>
              <option>&lt;</option>
              <option>&lt; =</option>
              <option>Contient</option>
              <option>Commence par</option>
              <option>In</option>
            </select>
          </td>
          <td align="left" class="TX"> 
            <input type="text" name="nom_req22" class="form_ediht_Requetes" size="30">
          </td>
        </tr>
        <tr bgcolor="F1F1F1"> 
          <td align="left" class="TX">&nbsp;</td>
          <td align="center" class="TX">&nbsp;</td>
          <td align="left" class="TX">&nbsp;</td>
        </tr>
      </table>
      <br>
      <table width="540" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right">&nbsp; </td>
          <td align="right" width="150">
            <input type="button" name="modif" value="Ajouter/Modifier" class="bn_ajouter" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
          </td>
          <td align="right" width="100">
            <input type="button" name="annul_nF" value="Annuler" class="bn_ajouter" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
          </td>
        </tr>
      </table>
    </td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
  </tr>
</table>
<p>&nbsp;</p>

<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
  <tr> 
    <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
  </tr>
  <tr> 
    <td width="14"></td>
    <td align="left" class="TX_Requetes">RESTRICTIONS D'ACCES</td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="1"></td>
    <td align="center" class="TX" height="1" bgcolor="#666666"></td>
    <td width="14" height="1"></td>
  </tr>
  <tr>
    <td width="14"></td>
    <td align="center" class="TX">&nbsp;</td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14"></td>
    <td align="center" class="TX"> 
      <table width="754" border="0" cellspacing="0" cellpadding="0" class="TX">
        <tr> 
          <td align="left">
            <input type="checkbox" name="checkbox10" value="checkbox">
            Certifi&eacute; :</td>
        </tr>
      </table>
      <br>
      <table width="754" border="0" cellspacing="0" cellpadding="0" class="TX">
        <tr> 
          <td align="left">
            <input type="checkbox" name="checkbox102" value="checkbox">
            Administrateur : </td>
        </tr>
      </table>
    </td>
    <td width="14"></td>
  </tr>
  <tr> 
    <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
    <td height="14"></td>
    <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
  </tr>
</table>
<p><br>
</p>
<table cellpadding="0" cellspacing="0" width="961" align="center">
  <tr> 
    <td align="center"> 
      <input type="button" name="submit2" value="VALIDER" class="bn_valider_requete" onClick="MM_goToURL('parent','admvak_gest_requetes_parten_filtre.php');return document.MM_returnValue">
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
