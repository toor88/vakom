<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo.css" type="text/css">
<link rel="stylesheet" href="../css/general.css" type="text/css">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<script language="JavaScript">
<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
      <?php
	include("menu_top_new.php");
     ?>
<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>	     
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="center" class="menu_Gris">&nbsp;</td>
  </tr>
  <tr> 
    <td align="right"> 
      <table width="961" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="180" valign="top"> 
            <p><font color="EA98AA" class="TX"><b>ENTREPRISE</b></font></p>
          </td>
          <td align="left" valign="top"> 
            <table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="left" class="TX"> 
                  <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr> 
                      <td class="TX" align="center">Selectionner l'entreprise 
                        <select name="select_txt" class="form_ediht">
                          <option selected>RISC GROUP</option>
                          <option>Entreprise 2</option>
                          <option>Entreprise 3</option>
                          <option>etc.</option>
                        </select>
                        &nbsp; 
                        <input type="submit" name="ok" value="Ok" class="bn_ajouter">
                        &nbsp;&nbsp;&nbsp;OU&nbsp;&nbsp;&nbsp; 
                        <input type="submit" name="new_entrep" value="Cr&eacute;er une nouvelle entreprise " class="bn_ajouter" onClick="MM_openBrWindow('gc_crea_entreprise.php','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=540')">
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="14"></td>
              </tr>
              <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td width="180" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
        <tr> 
          <td width="180" valign="top"><font color="EA98AA" class="TX"><b>TABLEAU 
            DES CERTIFIES</b></font></td>
          <td align="left" valign="top"> 
            <table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="center" class="TX"> 
                  <table width="730" border="0" cellspacing="0" cellpadding="2" class="TX">
                    <tr align="center"> 
                      <td colspan="8" class="TX_GD">ENTREPRISE RISC GROUP</td>
                    </tr>
                    <tr align="left"> 
                      <td colspan="8" class="TX_GD"> 
                        <input type="button" name="new_certfie" value="Ajouter" class="bn_ajouter" onClick="MM_openBrWindow('gestion_certifies_ajout.php','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=500')">
                      </td>
                    </tr>
                    <tr align="center" bgcolor="#000000"> 
                      <td height="1" colspan="8" ></td>
                    </tr>
                    <tr> 
                      <td bgcolor="#C4C4C4"><b>Nom</b></td>
                      <td bgcolor="#C4C4C4"><b>Pr&eacute;nom</b></td>
                      <td bgcolor="#C4C4C4" align="center"><b>Code <br>
                        Acc&egrave;s</b></td>
                      <td bgcolor="#C4C4C4" align="center"><b>Fonction</b></td>
                      <td bgcolor="#C4C4C4" align="center"><b>Droits</b></td>
                      <td bgcolor="#C4C4C4"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
                          <tr> 
                            <td><b>Dossiers</b></td>
                            <td align="right"><b>Jetons</b></td>
                          </tr>
                        </table>
                      </td>
                      <td bgcolor="#C4C4C4" align="center"><b>Modifier</b></td>
                      <td bgcolor="#C4C4C4" align="center"><b>Supprimer</b></td>
                    </tr>
                    <tr> 
                      <td height="1" bgcolor="#000000"></td>
                      <td height="1" bgcolor="#000000"></td>
                      <td height="1" bgcolor="#000000"></td>
                      <td height="1" bgcolor="#000000"></td>
                      <td height="1" bgcolor="#000000"></td>
                      <td height="1" bgcolor="#000000"></td>
                      <td height="1" bgcolor="#000000"></td>
                      <td height="1" bgcolor="#000000"></td>
                    </tr>
                    <tr> 
                      <td>BENSIMON</td>
                      <td>Gilles</td>
                      <td align="center">12345</td>
                      <td align="center">Responsable DEI</td>
                      <td align="center">Administrateur</td>
                      <td valign="top"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
                          <tr> 
                            <td>Autoscopie</td>
                            <td align="right">50/50</td>
                          </tr>
                          <tr bgcolor="#EBEBEB"> 
                            <td>Profiloscope</td>
                            <td align="right">20/30</td>
                          </tr>
                        </table>
                      </td>
                      <td align="center"><img src="modifier.png" border="0" width="20"></td>
                      <td align="center"><img src="supprimer.jpg" border="0" width="10"></td>
                    </tr>
                    <tr> 
                      <td colspan="8" bgcolor="#CCCCCC" height="1"></td>
                    </tr>
                    <tr> 
                      <td>BERNARDINI</td>
                      <td>Edith</td>
                      <td align="center">23456</td>
                      <td align="center">Infographiste</td>
                      <td align="center">Utilisateur</td>
                      <td valign="top"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
                          <tr> 
                            <td>Autoscopie</td>
                            <td align="right">50/50</td>
                          </tr>
                        </table>
                      </td>
                      <td align="center"><img src="modifier.png" border="0" width="20"></td>
                      <td align="center"><img src="supprimer.jpg" border="0" width="10"></td>
                    </tr>
                    <tr> 
                      <td colspan="8" bgcolor="#CCCCCC" height="1" valign="top"></td>
                    </tr>
                    <tr> 
                      <td valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                    </tr>
                  </table>
                  <p>&nbsp;</p>
                </td>
                <td width="14"></td>
              </tr>
              <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
            <p>&nbsp;</p>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td align="right" width="180">&nbsp; </td>
  </tr>
</table>
		</p></div>	</article></div>	</div>	</div>	</div>					
</body>
</html>
