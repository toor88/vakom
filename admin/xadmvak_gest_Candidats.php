<?php
session_start();
	// Si l'utilisateur est un super admin
if ($_SESSION['droit']>5){

	function duree( $time ) {
		$tabTemps = array("j" => 86400,
		"h"  => 3600,
		"'" => 60,
		"\"" => 1);

		$result = "";

		foreach($tabTemps as $uniteTemps => $nombreSecondesDansUnite) {
			$$uniteTemps =  floor ($time/$nombreSecondesDansUnite);
			$time = $time%$nombreSecondesDansUnite;

			if($$uniteTemps > 0 ){
				if ($$uniteTemps<10){
					$$uniteTemps = '0'.$$uniteTemps;
				}
				$result .= $$uniteTemps."$uniteTemps";
			}
		}
		return  $result;
		
	} 
								
	if ($_POST['select_part']>0){
		header('location:admvak_gest_Candidats.php?partid='.intval($_POST['select_part']));
	}else{
		$error = 'Veuillez choisir un partenaire';
	}
	
	if (isset($_POST['select_cert'])){
		header('location:admvak_gest_Candidats.php?partid='.intval($_GET['partid']).'&certid='.$_POST['select_cert']);
	}else{
		$error = 'Veuillez choisir un certifié';
	}
	
	/* Si on a cliqué sur le bouton valider de bas de page, celui qui sert a afficher le tableau des candidats */
	if ($_POST['step']==3){
	
		/* On cherche quel mode de recherche a été cliqué */
		switch($_POST['choix_action']){
		
			/* On recherche les candidats par client */
			case 'client':
				/* Si le candidat a bien été choisi dans la liste */
				if (isset($_POST['select_client'])){
					/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
					header('location:admvak_gest_Candidats.php?partid='.intval($_GET['partid']).'&certid='.$_GET['certid'].'&case=client&cliid='.strtolower($_POST['select_client']));
				}
			break;
			
			/* On recherche les candidats libres pour ce certifié */
			case 'libre':
				/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
				header('location:admvak_gest_Candidats.php?partid='.intval($_GET['partid']).'&certid='.$_GET['certid'].'&case=libre');
			break;
			
			/* On recherche les candidats par opération */
			case 'operation':
				/* Si le candidat a bien été choisi dans la liste */
				if ($_POST['select_operation']>0){
					/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
					header('location:admvak_gest_Candidats.php?partid='.intval($_GET['partid']).'&certid='.$_GET['certid'].'&case=operation&opeid='.intval($_POST['select_operation']));
				}
			break;
			
			/* On recherche les candidats par nom */
			case 'nom':
				/* Si le candidat a bien été choisi dans la liste */
				if (strlen($_POST['cand_nom'])>=0){
					/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
					header('location:admvak_gest_Candidats.php?partid='.intval($_GET['partid']).'&certid='.$_GET['certid'].'&case=nom&cand_nom='.$_POST['cand_nom']);
				}
			break;
		}
	}
	
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);

	if ($_GET['action']=='delete' && $_GET['partid']>0 && $_GET['contact']>0){
		$sql_delete_contact = "UPDATE CERTIFIE SET CERT_DATE_SUPPRESSION=SYSDATE, CERT_USER_SUPPRESSION_ID='".$_SESSION['vak_id']."' WHERE CERT_ID='".txt_db($_GET['contact'])."'";
		$qry_delete_contact = $db->query($sql_delete_contact);
		header('location:admvak_gest_Candidats.php?partid='.$_GET['partid']);
	}
	// Sélection de la liste des partenaire
	$sql_part_list = "SELECT * FROM PARTENAIRE ORDER BY PART_NOM,PART_RS ASC";
	$qry_part_list = $db->query($sql_part_list);

	if (isset($_GET['partid'])){
		// Sélection des infos parenaires
		$sql_part = "SELECT PART_ID, PART_NOM FROM PARTENAIRE WHERE PART_ID='".txt_db($_GET['partid'])."'";
		$qry_part = $db->query($sql_part);

		// Sélection de la liste des certifiés
		$sql_cert = "SELECT CERT_ID, CERT_NOM, CERT_PRENOM FROM CERTIFIE WHERE CERT_PART_ID='".txt_db($_GET['partid'])."' AND CERT_DATE_SUPPRESSION IS NULL ORDER BY CERT_NOM ASC";
		$qry_cert = $db->query($sql_cert);
		
		if (isset($_GET['certid'])){
		
			// Selection des infos certifies
			$sql_cert2 = "SELECT CERT_ID, CERT_NOM, CERT_PRENOM FROM CERTIFIE WHERE CERT_ID='".txt_db($_GET['certid'])."' AND CERT_DATE_SUPPRESSION IS NULL";
			$qry_cert2 = $db->query($sql_cert2);

			// Selection de la liste des clients du certifiés
			$sql_liste_clients = "SELECT CLIENT.CLI_ID, CLIENT.CLI_NOM, CLIENT.CLI_VILLE FROM CLIENT, CLIENT_A_CERT WHERE CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND CLIENT_A_CERT.CERT_ID='".txt_db($_GET['certid'])."' ORDER BY CLIENT.CLI_NOM ASC";
			$qry_liste_clients = $db->query($sql_liste_clients);

			// Sélection des candidats libres pour le certifié sélectionné
			$sql_cand_liste = "SELECT CANDIDAT.CAND_ID, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM FROM CANDIDAT, CERTIFIE WHERE CERTIFIE.CERT_ID=CANDIDAT.CAND_CERT_ID AND CANDIDAT.CAND_CERT_ID='".txt_db($_GET['certid'])."' AND CAND_CLI_ID='-1' AND CAND_DATE_SUPPRESSION IS NULL ORDER BY CANDIDAT.CAND_NOM ASC";
			$qry_cand_liste = $db->query($sql_cand_liste);
			
			// Sélection des candidats pour le certifié sélectionné
			$sql_cand_liste2 = "SELECT CANDIDAT.CAND_ID, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM FROM CANDIDAT WHERE CANDIDAT.CAND_CERT_ID='".txt_db($_GET['certid'])."' AND CAND_DATE_SUPPRESSION IS NULL ORDER BY CANDIDAT.CAND_NOM ASC";
			$qry_cand_liste2 = $db->query($sql_cand_liste2);
			
			// Sélection des types d'opérations 
			$sql_type_ope	 = "SELECT CODE_LIBELLE, CODE_ID FROM CODE WHERE CODE.CODE_TABLE='TYPE_OPERATION' ORDER BY CODE_LIBELLE ASC";
			$qry_type_ope	 = $db->query($sql_type_ope);
			
			// On sélectionne les opérations pour le certifié
			$sql_operations	 = "SELECT TYPE_OPERATION.TYPE_OPE_ID, TYPE_OPERATION.TYPE_OPE_LIBELLE FROM TYPE_OPERATION, CODE WHERE CODE.CODE_ID=TYPE_OPERATION.TYPE_OPE_CODE_ID AND CODE.CODE_TABLE='TYPE_OPERATION' AND CERT_ID='".txt_db($_GET['certid'])."' ORDER BY TYPE_OPERATION.TYPE_OPE_LIBELLE ASC";
			$qry_operations	 = $db->query($sql_operations);

			
			// Si le formulaire est posté, on stocke les candidats dans un tableau en session
			if ($_POST['affect']){
				//unset($_SESSION['checked_cand']);
				$_SESSION['checked_cand'] = $_POST['affect_cand'];
/*
				foreach($_SESSION['checked_cand'] as $checked_candidat){
					echo $checked_candidat.'<br>';
				}
	*/			
				?>
				<script type="text/javascript">
					window.open('admvak_certifie_affectCandidat.php?certid=<?php echo $_GET['certid'] ?>','affect_candidat','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=500')
					document.location.href='admvak_gest_Candidats.php?partid=<?php echo$_GET['partid']?>&certid=<?php echo$_GET['certid']?>&case=<?php echo$_GET['case']?>&cliid=<?php echo$_GET['cliid']?>&opeid=<?php echo$_GET['opeid']?>&candid=<?php echo$_GET['candid']?>&idnc=<?php echo($_GET['idnc']+1) ?>';
				</script>
				<?php
				}
		
		
			switch ($_GET['case']){
				case 'client':
					/* Si on cherche des candidats par client */
					if (isset($_GET['cliid'])){
						// Sélection des infos client
						$sql_cli = "SELECT CLI_ID, CLI_NOM, CLIENT.CLI_VILLE FROM CLIENT WHERE LOWER(CLI_NOM) LIKE '".txt_db(strtolower($_GET['cliid']))."%'";
						//echo $sql_cli;
						$qry_cli = $db->query($sql_cli);
						
						if($_GET['certid']!=''){
						 $and = " AND CLIENT_A_CERT.CERT_ID = '".txt_db($_GET['certid'])."'";
						}
						// Sélection des candidats
						$sql_cand = "SELECT CLIENT.CLI_NOM, CANDIDAT.CAND_CLI_ID, CLIENT.CLI_VILLE, CANDIDAT.CAND_ID, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM, CANDIDAT.CAND_SEXE, CANDIDAT.CAND_DNS, CANDIDAT.CAND_FONCTION, CANDIDAT.CAND_ACTIF FROM CERTIFIE, CANDIDAT, CLIENT, CLIENT_A_CERT WHERE CERTIFIE.CERT_ID=CLIENT_A_CERT.CERT_ID AND CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND LOWER(CLIENT.CLI_NOM) LIKE '".txt_db(strtolower($_GET['cliid']))."%' AND CERTIFIE.CERT_PART_ID = '".txt_db(intval($_GET['partid']))."'".$and." AND CANDIDAT.CAND_CLI_ID=CLIENT.CLI_ID AND CAND_DATE_SUPPRESSION IS NULL";
						//echo $sql_cand;
						/* On effectue la requête */
						$qry_cand = $db->query($sql_cand);
					}
				break;
				
				case 'libre':
				/* Si on cherche des candidats libres par nom */
					$sql_cand = "SELECT CANDIDAT.CAND_ID, 
					CANDIDAT.CAND_NOM, 
					CANDIDAT.CAND_PRENOM, 
					CANDIDAT.CAND_CLI_ID, 
					CANDIDAT.CAND_SEXE, 
					CANDIDAT.CAND_DNS, 
					CANDIDAT.CAND_FONCTION, 
					CANDIDAT.CAND_ACTIF
					FROM CANDIDAT   
					WHERE CANDIDAT.CAND_CLI_ID='-1' 
					AND CANDIDAT.CAND_CERT_ID='".txt_db($_GET['certid'])."' 
					AND CANDIDAT.CAND_DATE_SUPPRESSION IS NULL";
					$sql_cand = "SELECT CAND_OPE.*,TO_CHAR(DATE_DEB, 'YYYYMMDDHH24MISS') TIME_DEB, TO_CHAR(DATE_FIN, 'YYYYMMDDHH24MISS') TIME_FIN,CERTIFIE.CERT_NOM FROM CAND_OPE,CERTIFIE WHERE CAND_OPE.CERT_ID_INI=CERTIFIE.CERT_ID AND CLI_ID=-1 AND CAND_OPE.CERT_ID='".txt_db(intval($_GET['certid']))."'  ORDER BY OPE_ID,DOSSIER_ID";
					/* On effectue la requête */
					$qry_cand = $db->query($sql_cand);
				break;
				
				case 'nom':
					/* Si on cherche des candidats par nom */
					if (isset($_GET['cand_nom'])){
						$sql_cand = "SELECT CLIENT.CLI_NOM, 
						CLIENT.CLI_VILLE, 
						CANDIDAT.CAND_ID, 
						CANDIDAT.CAND_NOM, 
						CANDIDAT.CAND_CLI_ID, 
						CANDIDAT.CAND_PRENOM, 
						CANDIDAT.CAND_SEXE, 
						CANDIDAT.CAND_DNS, 
						CANDIDAT.CAND_FONCTION, 
						CANDIDAT.CAND_ACTIF 
						FROM CANDIDAT, CLIENT 
						WHERE UPPER(CANDIDAT.CAND_NOM) LIKE '".txt_db(strtoupper($_GET['cand_nom']))."%' 
						AND CANDIDAT.CAND_CLI_ID=CLIENT.CLI_ID 
						AND CANDIDAT.CAND_CERT_ID='".txt_db($_GET['certid'])."'  
						AND CANDIDAT.CAND_DATE_SUPPRESSION IS NULL";
						/* On effectue la requête */
						$qry_cand = $db->query($sql_cand);
					}
					//$sql_cand = "SELECT CAND_OPE.*,TO_CHAR(DATE_DEB, 'YYYYMMDDHH24MISS') TIME_DEB, TO_CHAR(DATE_FIN, 'YYYYMMDDHH24MISS') TIME_FIN,CERTIFIE.CERT_NOM FROM CAND_OPE,CERTIFIE WHERE CAND_OPE.CERT_ID_INI=CERTIFIE.CERT_ID AND UPPER(CAND_NOM) LIKE '".txt_db(strtoupper($_GET['cand_nom']))."%' AND CAND_OPE.CERT_ID='".txt_db($_GET['certid'])."'  ORDER BY OPE_ID,DOSSIER_ID";
					/* On effectue la requête */
					//$qry_cand = $db->query($sql_cand);
					
				break;
				
				case 'operation':
					/* Si on cherche des candidats par nom */
					if ($_GET['opeid']!=''){
						$sql_cand = "SELECT CANDIDAT.CAND_ID, 
						CANDIDAT.CAND_NOM, 
						CANDIDAT.CAND_CLI_ID, 
						CANDIDAT.CAND_PRENOM, 
						CANDIDAT.CAND_SEXE, 
						CANDIDAT.CAND_DNS, 
						CANDIDAT.CAND_FONCTION, 
						CANDIDAT.CAND_ACTIF
						FROM CANDIDAT, CERTIFIE, CAND_OPE, OPERATION, TYPE_OPERATION 
						WHERE CAND_OPE.OPE_ID = OPERATION.OPE_ID 
						AND CAND_OPE.CAND_ID = CANDIDAT.CAND_ID 
						AND OPERATION.TYPE_OPE_ID = TYPE_OPERATION.TYPE_OPE_ID 
						AND TYPE_OPERATION.TYPE_OPE_ID='".txt_db(intval($_GET['opeid']))."' 
						AND CANDIDAT.CAND_CERT_ID=CERTIFIE.CERT_ID 
						AND CANDIDAT.CAND_CERT_ID='".txt_db($_GET['certid'])."' 
						AND CANDIDAT.CAND_DATE_SUPPRESSION IS NULL";
						/* On effectue la requête */
						//$sql_cand = "SELECT CAND_OPE.*,TO_CHAR(DATE_DEB, 'YYYYMMDDHH24MISS') TIME_DEB, TO_CHAR(DATE_FIN, 'YYYYMMDDHH24MISS') TIME_FIN,CERTIFIE.CERT_NOM FROM CAND_OPE,CERTIFIE WHERE CAND_OPE.CERT_ID_INI=CERTIFIE.CERT_ID AND OPE_ID='".txt_db(intval($_GET['opeid']))."' AND CAND_OPE.CERT_ID='".txt_db($_GET['certid'])."'  ORDER BY OPE_ID,DOSSIER_ID";
						$qry_cand = $db->query($sql_cand);
						
						$sql_infos_ope = "SELECT * FROM TYPE_OPERATION, OPERATION WHERE OPERATION.TYPE_OPE_ID=TYPE_OPERATION.TYPE_OPE_ID AND OPERATION.TYPE_OPE_ID='".txt_db($_GET['opeid'])."'";
						$qry_infos_ope = $db->query($sql_infos_ope);
					}
				break;
			}
			
			if ($_GET['candid']>0){
				$sql_detail_cand 	= "SELECT CAND_OPE.*,TO_CHAR(DATE_DEB, 'YYYYMMDDHH24MISS') TIME_DEB, TO_CHAR(DATE_FIN, 'YYYYMMDDHH24MISS') TIME_FIN,CERTIFIE.CERT_NOM FROM CAND_OPE,CERTIFIE WHERE CAND_OPE.CERT_ID_INI=CERTIFIE.CERT_ID AND CAND_ID='".txt_db(intval($_GET['candid']))."' ORDER BY TO_CHAR(DATE_CREATION,'YYYYMMDD') DESC, OPE_ID,DOSSIER_ID";
				$detail_cand  		= $db->query($sql_detail_cand);
			}
			
		}
		
	}

?>	
<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo.css" type="text/css">
<link rel="stylesheet" href="../css/general.css" type="text/css">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<script language="JavaScript">
<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function change_part(part_id){
	if (part_id>0){
		document.location.href = 'admvak_gest_Candidats.php?partid='+part_id;
	}
}

function change_cert(cert_id){
	document.location.href = 'admvak_gest_Candidats.php?partid=<?php echo $_GET['partid'] ?>&certid='+cert_id;
}

function charge_ope(typeopeid){
	var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
	var filename = "ajax_ope_liste2.php"; // La page qui réceptionne les données
	var data     = null; 
	
	if 	(typeopeid.length>0){ //Si le code postal tapé possède au moins 2 caractères
		document.getElementById("ope_liste").innerHTML='Patientez...';
		var xhr_object = null; 
			 
			if(window.XMLHttpRequest) // Firefox 
			   xhr_object = new XMLHttpRequest(); 
			else if(window.ActiveXObject) // Internet Explorer 
			   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
			else { // XMLHttpRequest non supporté par le navigateur 
			   alert("<?php echo $t_browser_support_error_1 ?>");
			   return; 
			} 
			 
			
			 
			if(typeopeid != ""){
				data = "certid=<?php echo $_GET['certid']?>&type_ope="+typeopeid;
			}
			if(method == "GET" && data != null) {
			   filename += "?"+data;
			   data      = null;
			}
			 
			xhr_object.open(method, filename, true);

			xhr_object.onreadystatechange = function() {
			   if(xhr_object.readyState == 4) {
				  var tmp = xhr_object.responseText.split(":"); 
				  if(typeof(tmp[0]) != "undefined") { 
					 document.getElementById("ope_liste").innerHTML = '';
					 if (tmp[0]!=''){
						document.getElementById("ope_liste").innerHTML = tmp[0];
					 }else{
						document.getElementById("ope_liste").innerHTML = 'Aucune&nbsp;opération&nbsp;recensée&nbsp;pour&nbsp;ce&nbsp;type&nbsp;d\'opération';
					 }
				  }
			   } 
			} 

			xhr_object.send(data); //On envoie les données
	}
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000">
		<?php
			include("menu_top_new.php");
		?>  <br>
<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>	 		
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
<tr> 
  <td width="20">&nbsp;</td>
  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;CANDIDATS</td>
</tr>
</table>
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td align="center"> 
      <table width="961" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="left" valign="top"> 
		  	<form method="post" action="#">
            <table width="961" border="0" cellspacing="0" cellpadding="0" class="fond_tablo_candidats">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="left" class="TX"> 
                  <table border="0" cellspacing="0" cellpadding="0" width="100%" align="left">
                    <tr> 
                      <td class="TX" align="right">S&eacute;lectionner&nbsp;un&nbsp;Partenaire&nbsp;:&nbsp;</td>
					  <td class="TX" align="left" width="500">					  
						<select name="select_part" id="select_part" class="form_ediht_Candidats"  onchange="change_part(document.getElementById('select_part').options[document.getElementById('select_part').selectedIndex].value)">
							<option value="0">Liste des partenaires</option>
							<?php 
							if (is_array($qry_part_list)){
								foreach($qry_part_list as $partenaire){
									unset($selected);
									if ($_GET['partid']==$partenaire['part_id']){
										$selected = ' selected="selected"';
									}
									echo '<option value="'.$partenaire['part_id'].'"'.$selected.'>'.ucfirst($partenaire['part_nom']).'</option>';
								}
							}
							?>
						 </select>
                        &nbsp; 
						  <input type="hidden" name="step" value="1"></td>
                    </tr>
                  </table>
                </td>
                <td width="14"></td>
              </tr>
			  
			<?php
			if (isset($_GET['partid'])){
				if (is_array($qry_cert)){
				?>
				<tr> 
                <td width="14"></td>
                <td align="left" class="TX"> 
                  <table border="0" cellspacing="0" cellpadding="0" width="100%" align="left">
                    <tr> 
                      <td class="TX" align="right">S&eacute;lectionner&nbsp;un&nbsp;Certifi&eacute;&nbsp;:&nbsp;</td>
					  <td class="TX" align="left" width="500">
						<select id="select_cert" id="select_cert" name="select_cert" class="form_ediht_Candidats" onchange="change_cert(document.getElementById('select_cert').options[document.getElementById('select_cert').selectedIndex].value)">
						  <option value="" selected="selected">Liste des certifiés</option>
						  <option value="">Tous</option>
						  <?php
						  foreach ($qry_cert as $cert){
							unset($selected_cert);
							if ($cert['cert_id'] == $_GET['certid']){
								$selected_cert = ' selected="selected"';
							}
							echo '<option value="'.$cert['cert_id'].'"'.$selected_cert.'>'.$cert['cert_nom'].' '.$cert['cert_prenom'].'</option>';
						  }
						  ?>
						</select>
					  </td>
					 </tr>
				  </table>
				</td>
				<td width="14"></td>
			    </tr>
					<?php
						}else{
						?>
						<tr> 
						  <td class="TX" align="center" colspan="3">Il n'y a aucun certifié pour ce partenaire. Ajoutez un certifié en <a href="#" onClick="MM_openBrWindow('admvak_crea_contactClient.php?partid=<?php echo $_GET['partid'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">cliquant ici</a>.</td>
						</tr>
						<?php
						}
			}
		if (isset($_GET['partid']) && isset($_GET['certid'])){
			?>
					<tr> 
					  <td width="14"></td>
					  <td align="left" class="TX_Candidats">RECHERCHER LES CANDIDATS</td>
					  <td width="14"></td>
					</tr>
					<tr> 
					  <td width="14" height="1"></td>
					  <td align="left" bgcolor="#666666"  height="1"></td>
					  <td width="14" height="1"></td>
					</tr>
					<tr> 
					  <td width="14"></td>
					  <td align="center" class="TX"> 
						<table border="0" cellspacing="0" cellpadding="0" width="450">
						  <tr> 
							<td class="TX" align="left" width="55%">&nbsp;</td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left"><input type="radio" name="choix_action" value="client" <?php if (!isset($_GET['case']) || $_GET['case']=='client') echo 'checked="checked"'; ?>>&nbsp;<b>Par Client </b></td>
							<td class="TX" align="left">&nbsp; </td>
						  </tr>
						  <?php
						 #################################################################
						 #######            SI UN CLIENT EST REPERTORIE            #######
						 #################################################################
							?>
							  <tr> 
								<td class="TX" align="left"> 
								  Saisir le nom d'un client :
								<?php
								/*if (is_array($qry_liste_clients)){
									  ?>
									  <select name="select_client" class="form_ediht_Candidats">
										<?php
										foreach($qry_liste_clients as $client_liste){
											unset($selected_cli);
											if($_GET['cliid'] == $client_liste['cli_id']){
												$selected_cli = ' selected="selected"';
											}
											echo '<option value="'.$client_liste['cli_id'].'"'.$selected_cli.'>'.strtoupper($client_liste['cli_nom']).' - '.ucfirst($client_liste['cli_ville']).'</option>';
										}
										?>
									  </select>
									  <?php
								  }else{
									//echo '<tr><td colspan="2" class="TX">Aucun Client n\'est répertorié pour ce certifié.</td></tr>';
								  }
								  */
								  ?>
								  <input type="text" class="form_ediht_Candidats" size="40" maxlength="40" name="select_client">
								</td>
								<td class="TX" align="left" valign="bottom"> 
								  <input class="bn_ajouter" type="button" value="Cr&eacute;er un client" onClick="MM_openBrWindow('admvak_crea_certifieClient.php?certid=<?php echo $_GET['certid'] ?>','crea_client','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">
								 </td>
							  </tr>
						  <tr> 
							<td class="TX" align="left">&nbsp;</td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left"><input type="radio" name="choix_action" value="operation" <?php if ($_GET['case']=='operation') echo 'checked="checked"'; ?>>&nbsp;<b>Par Op&eacute;ration </b></td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						 <?php
						 #################################################################
						 ########       SI UNE OPERATION EST REPERTORIE            #######
						 #################################################################
							 ?>
							 <tr> 
								<td class="TX" align="left" colspan="2">
								<?php
									if (is_array($qry_operations)){
									?>
								  S&eacute;lectionner une op&eacute;ration :&nbsp;<br>
									Type op&eacute;ration :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									  <select name="select_type_op" id="select_type_op" class="form_ediht_Candidats" onchange="charge_ope(document.getElementById('select_type_op').options[document.getElementById('select_type_op').selectedIndex].value);">
										<option value="all" selected>Tous</option>
										<?php
										if (is_array($qry_type_ope)){
											foreach($qry_type_ope as $type_ope){
												echo '<option value="'.$type_ope['code_id'].'">'.$type_ope['code_libelle'].'</option>';
											}
										?>
									  </select>
									  <div style="display: inline;" id="ope_liste" width="200">
										<?php
											echo '<select name="select_operation" id="select_operation" class="form_ediht_Candidats">';
											foreach ($qry_operations as $operations){ // Tant que la requete renvoie un résultat
												unset($selected_opeid);
												if ($_GET['opeid'] == $operations['type_ope_id']){
													$selected_opeid = ' selected="selected"';
												}
												echo '<option value="'. $operations['type_ope_id'].'"'.$selected_opeid.'>'. $operations['type_ope_libelle'] .'</option>';
											}
											echo '</select>';								
										}
										?>
									  </div>
								  <?php
								  }else{
									echo '<?php echo $t_opr_certif ?>';
								  }
								  ?>
									</td>
							  </tr>
						  <tr> 
							<td class="TX" align="left">&nbsp;</td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left"><input type="radio" name="choix_action" value="nom" <?php if ($_GET['case']=='nom') echo 'checked="checked"'; ?>>&nbsp;<b>Par nom</b></td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						<?php
						 #################################################################
						 ########         SI UN CANDIDAT EST REPERTORIE            #######
						 #################################################################
								?>							
							  <tr> 
								<td class="TX" align="left">
								  Saisir un nom :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								  <input type="text" maxlength="55" name="cand_nom" class="form_ediht_Candidats">
								</td>
								<td class="TX" align="left" valign="bottom">
								<input class="bn_ajouter" type="button" value="Cr&eacute;er un candidat" onClick="MM_openBrWindow('admvak_certifie_nvoCandidat.php?certid=<?php echo $_GET['certid'] ?>','crea_cand','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">
								</td>
							  </tr>
						  <tr> 
							<td class="TX" align="left">&nbsp;</td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						</table>
					  </td>
					  <td width="14"></td>
					</tr>  
		<?php
		}
		?>
					<tr> 
					  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
					  <td height="14"></td>
					  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
					</tr>
				  </table>
			  <br>
			</td>
		</tr>
		<?php
		if (isset($_GET['partid']) && isset($_GET['certid'])){
		?>
		<tr>
			<td align="center"><input type="hidden" name="step" value="3">
				<input type="submit" value="Valider" class="bn_valider_candidat">
			</td>
		</tr>
		<?php
		}
		?>
		</form>
		<tr><td>&nbsp;</td></tr>
		<form action="#" method="post">	
		<?php
		if (isset($_GET['case'])){
		?>

			<tr> 
			  <td align="left" valign="top">
				<?php	
				if($_GET['case']=='nom'){
					if($_GET['cand_nom']!=''){
						$str_nom = 'Candidat : '.$_GET['cand_nom'];
					}else{
						$str_nom = 'Tous&nbsp;les&nbsp;candidats';
					}
					if($_GET['certid']>0){
						$str_cert = $qry_cert2[0]['cert_nom'];
					}else{
						$str_cert = 'Tous&nbsp;les&nbsp;certifiés';
					}
					?>
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_part[0]['part_nom']?>&nbsp;>&nbsp;<?php echo $str_cert ?>&nbsp;>&nbsp;<?php echo $str_nom ?></td>
					</tr>
					</table>
					<?php
				}	
				if($_GET['case']=='client'){
					if($_GET['cliid']!=''){
						$str_nom = 'Client : '.$_GET['cliid'];
					}else{
						$str_nom = 'Tous&nbsp;les&nbsp;clients';
					}
					if($_GET['certid']>0){
						$str_cert = $qry_cert2[0]['cert_nom'];
					}else{
						$str_cert = 'Tous&nbsp;les&nbsp;certifiés';
					}
					?>
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_part[0]['part_nom']?>&nbsp;>&nbsp;<?php echo $str_cert ?>&nbsp;>&nbsp;<?php echo $str_nom ?></td>
					</tr>
					</table>
					<?php
				}
				if($_GET['case']=='operation'){
					if($_GET['opeid']>0){
						$str_nom = 'Opération : '.$qry_infos_ope[0]['type_ope_libelle'];
					}else{
						$str_nom = 'Toutes&nbsp;les&nbsp;opérations';
					}
					if($_GET['certid']>0){
						$str_cert = $qry_cert2[0]['cert_nom'];
					}else{
						$str_cert = 'Tous&nbsp;les&nbsp;certifiés';
					}
					?>
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_part[0]['part_nom']?>&nbsp;>&nbsp;<?php echo $str_cert ?>&nbsp;>&nbsp;<?php echo $str_nom ?></td>
					</tr>
					</table>
					<?php
				}
				?>
				<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td class="TX_Candidats">CANDIDATS</td>
					  <td width="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td height="1" bgcolor="#000000"></td>
					  <td width="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td>&nbsp;</td>
					  <td width="14"></td>
				  </tr>
			<?php
			/* On a sélectionné un mode de recherche de candidats */
			switch($_GET['case']){
			
				/* Si on cherche les candidats d'un client */
				case 'client':
				case 'nom':
				case 'operation':
				case 'libre':
				?>
				<a name="bas"></a>
				  <tr> 
					<td width="14"></td>
					<td align="center" class="TX"> 
					  <table width="900" border="0" cellspacing="0" cellpadding="5" class="TX">
						<?php
						if (is_array($qry_cand)){
							?>
							  <tr align="left"> 
								<td colspan="10" class="TX" style="text-align: right;"> 
								  <input type="checkbox" name="box_actif" value="checkbox" checked>
								 Actifs</td>
							  </tr>
							<tr align="center"> 
								<td class="TX_bold" align="center">select</td>
								<td class="TX_bold" align="center">client</td>
								<td class="TX_bold" align="center">nom</td>
								<td class="TX_bold" align="center">prénom</td>
								<td class="TX_bold" align="center">age</td>
								<td class="TX_bold" align="center">sexe</td>
								<td class="TX_bold" align="center">dernier profil OPR</td>
								<td class="TX_bold" align="center">état</td>
								<td class="TX_bold" align="center">statut</td>
								<td class="TX_bold" align="center">d&eacute;tail</td>
							<tr>
						<?php
							foreach($qry_cand as $list_candidat){
							
								$verif_passage = false;
								
								$sql_cand_det 	= "SELECT CAND_OPE.*,TO_CHAR(DATE_DEB, 'YYYYMMDDHH24MISS') TIME_DEB, TO_CHAR(DATE_FIN, 'YYYYMMDDHH24MISS') TIME_FIN,CERTIFIE.CERT_NOM FROM CAND_OPE,CERTIFIE WHERE CAND_OPE.CERT_ID_INI=CERTIFIE.CERT_ID AND CAND_ID='".txt_db(intval($list_candidat['cand_id']))."' ORDER BY OPE_ID,DOSSIER_ID";
								$qry_cand_det  	= $db->query($sql_cand_det);
									
								$bgcolor = "#F1F1F1";
								foreach($qry_cand_det as $ope_cand){
									if(!$verif_passage){
									
										$cand_pn = 'E-:';
										$cand_profil ='';
										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$ope_cand['ope_id']." AND CAND_ID=".$ope_cand['cand_id']." AND REPONSE_PN='E'";
										$qry_cand_pn = $db->query($sql_cand_pn);
										$cand_pn = $cand_pn.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
										$sql_cand_profil ="select distinct CODE_REGROUP_NOM from cand_comb where TXT_IMG_TYPE_PROFIL_CODE_ID=30 and TXT_IMG_QUEST_ID=241 and TXT_IMG_PRECISION_CODE_ID=34 and COMBI_E_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$ope_cand['ope_id']." AND CAND_ID=".$ope_cand['cand_id']." AND REPONSE_PN='C'";
										$qry_cand_pn = $db->query($sql_cand_pn);
										$cand_pn = $cand_pn.'C-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
										$sql_cand_profil = $sql_cand_profil." and COMBI_C_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$ope_cand['ope_id']." AND CAND_ID=".$ope_cand['cand_id']." AND REPONSE_PN='P'";
										$qry_cand_pn = $db->query($sql_cand_pn);
										$sql_cand_profil = $sql_cand_profil." and COMBI_P_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
										$cand_pn = $cand_pn.'P-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$ope_cand['ope_id']." AND CAND_ID=".$ope_cand['cand_id']." AND REPONSE_PN='A'";
										$qry_cand_pn = $db->query($sql_cand_pn);
										$cand_pn = $cand_pn.'A-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
										$sql_cand_profil = $sql_cand_profil." and COMBI_A_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
										$qry_cand_pn = $db->query($sql_cand_profil);
										$cand_profil = $qry_cand_pn[0]['code_regroup_nom'];
										
										$verif_passage = true;
									}
									
									if ($ope_cand['time_deb']!='' && $ope_cand['time_fin']!=''){
										//Vert
										$bgcolor="#00FF00";
										$vert = true;
									}else{
										if ($ope_cand['time_deb']!=''){
											//Orange
											$bgcolor="#FFAA20";
											$orange=true;
										}else{
											//Rouge
											$bgcolor="#FF0000";
											$rouge=true;
										}
									}
								}

								?>
								<tr align="center"> 
									<td valign="top"><input type="checkbox" value="<?php echo $list_candidat['cand_id'] ?>" name="affect_cand[]"></td>
									<td class="TX" align="center"><?php echo '<a href="#bas" onclick="MM_openBrWindow(\'admvak_edit_certifieClient.php?cliid='.$list_candidat['cand_cli_id'].'\',\'fiche_clt_'.$list_candidat['cand_cli_id'].'\',\'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500\')">'.strtoupper($list_candidat['cli_nom']).'</a>' ?></td>
									<td class="TX" valign="top"><?php echo '<a href="#bas" onClick="MM_openBrWindow(\'admvak_certifie_edit_Candidat.php?candid='.$list_candidat['cand_id'].'\',\'fiche_candidat_'.$list_candidat['cand_id'].'\',\'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500\')">'.$list_candidat['cand_nom'].'</a>' ?></td>
									<td class="TX" align="center"><?php echo $list_candidat['cand_prenom'] ?></td>
									<td class="TX" align="center"><?php echo $list_candidat['cand_dns'] ?></td>
									<td class="TX" align="center"><?php echo $list_candidat['cand_sexe'] ?></td>
									<td class="TX" align="center"><?php echo $cand_profil ?></td>
									<td class="TX" valign="top">
									<?php
										if($vert){
											echo '<div style="color:#00FF00; background-color:#00FF00; border-bottom: 1px solid #F1F1F1;">&nbsp;</div>';
										}elseif($orange){
											echo '<div style="color:#FFAA20; background-color:#FFAA20; border-bottom: 1px solid #F1F1F1;">&nbsp;</div>';
										}elseif($rouge){
											echo '<div style="color:#FF0000; background-color:#FF0000; border-bottom: 1px solid #F1F1F1;">&nbsp;</div>';
										}
									?>
								  </td>
									<td class="TX" align="center">
									<?php
									if ($list_candidat['cand_actif']=='1'){
										echo '<span style="color:#00FF00; font-weight: bold;">ACTIF</span>';
									}else{
										echo '<span style="color:#FF0000; font-weight: bold;">INACTIF</span>';
									}
									?>
									</td>
									<td class="TX" align="center"><a href="admvak_gest_Candidats.php?partid=<?php echo $_GET['partid'] ?>&certid=<?php echo $_GET['certid'] ?>&case=<?php echo $_GET['case'] ?>&cliid=<?php echo $_GET['cliid'] ?>&opeid=<?php echo $_GET['opeid'] ?>&candid=<?php echo $list_candidat['cand_id'] ?>#2">détails</a></td>
								<tr>
								<?php
							}
						}else{
						?>
						<tr align="left"> 
							<td colspan="10" class="TX" style="text-align: center;"> 
							  Aucun candidat ne correspond à votre demande.
							 </td>
						</tr>
						<?php
						}
						?>
						<tr align="center"> 
							<td colspan="12" class="TX_GD">&nbsp;</td>
						</tr>
					  </table>
					</td>
					<td width="14"></td>
				  </tr>
		      <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
			<br>
			<?php
			break;
			}
			
			// 2eme TABLEAU DES CANDIDATS
			if($_GET['candid']>0){
			?>
				<table width="740" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Candidats2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo strtoupper($detail_cand[0]['cand_nom']).' '.ucfirst($detail_cand[0]['cand_prenom']) ?></td>
				</tr>
				</table>
				<table width="740" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td class="TX_Candidats">R&Eacute;CAPITULATIF DES DOSSIERS G&Eacute;N&Eacute;R&Eacute;S</td>
					  <td width="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td height="1" bgcolor="#000000"></td>
					  <td width="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td>&nbsp;</td>
					  <td width="14"></td>
				  </tr>
					<tr><td width="14"></td>
					<td class="TX">
					<a name="2"></a>
						<table width="700" border="0" cellspacing="0" cellpadding="0">						
							  <tr align="center"> 
								<td colspan="9" class="TX_GD">&nbsp;</td>
							  </tr>
							  <tr>
								<td  class="TX_bold" align="center">Dates</td>
								<td  class="TX_bold" align="center">Code acc&egrave;s</td>
								<td  class="TX_bold" align="center">P</td>
								<td  class="TX_bold">Produits</td>
								<td  class="TX_bold">Dossiers</td>
								<td  class="TX_bold">Op&eacute;rations</td>
								<td  class="TX_bold">Certifiés</td>
								<td  class="TX_bold">Durée</td>
								<td  class="TX_bold" align="center">Etat</td>
							  </tr>
							  <tr> 
								<td height="1" bgcolor="#000000" colspan="9"> </td>
							  </tr>
							<?php
							$ope_id_sav = '';
							foreach($detail_cand as $candidat){
								$cand_pn = 'E-:';
								$cand_profil ='';
								$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PN='E'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
								$sql_cand_profil ="select distinct CODE_REGROUP_NOM from cand_comb where TXT_IMG_TYPE_PROFIL_CODE_ID=30 and TXT_IMG_QUEST_ID=241 and TXT_IMG_PRECISION_CODE_ID=34 and COMBI_E_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
								$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PN='C'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.'C-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
								$sql_cand_profil = $sql_cand_profil." and COMBI_C_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
								$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PN='P'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$sql_cand_profil = $sql_cand_profil." and COMBI_P_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
								$cand_pn = $cand_pn.'P-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
								$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PN='A'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.'A-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
								$sql_cand_profil = $sql_cand_profil." and COMBI_A_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
								$qry_cand_pn = $db->query($sql_cand_profil);
								$cand_profil = $qry_cand_pn[0]['code_regroup_nom'];
								
								$sql_cand_pn = "select * from cand_pi WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PI='E'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.'E+:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
								$sql_cand_pn = "select * from cand_pi WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PI='C'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.'C+:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
								$sql_cand_pn = "select * from cand_pi WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PI='P'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.'P+:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
								$sql_cand_pn = "select * from cand_pi WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PI='A'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.'A+:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';								

								$sql_cand_pn = "select * from cand_pr WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PR='E'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.'E#:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
								$sql_cand_pn = "select * from cand_pr WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PR='C'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.'C#:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
								$sql_cand_pn = "select * from cand_pr WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PR='P'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.'P#:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
								$sql_cand_pn = "select * from cand_pr WHERE OPE_ID=".$candidat['ope_id']." AND CAND_ID=".$candidat['cand_id']." AND REPONSE_PR='A'";
								$qry_cand_pn = $db->query($sql_cand_pn);
								$cand_pn = $cand_pn.'A#:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';																								
								
								$cand_pn = $cand_pn.'PROFIL : '.$cand_profil;
								?>
								
								<tr>
								
								  <?php
								  if ($ope_id_sav != $candidat['ope_id']){
									$ope_id_sav = $candidat['ope_id'];
								  
								  ?>
								  <td class="TX" valign="top"><?php echo $candidat['date_creation'] ?></td>
								  <td class="TX" valign="top"><?php echo $candidat['code_acces'] ?></td>
								  <td class="TX" valign="top"><?php echo $cand_pn ?></td>
								  <td class="TX" valign="top"><?php echo $candidat['prod_nom'] ?></td>
								  <?php
								  }else{
								  ?>
								  <td class="TX" valign="top"></td>
								  <td class="TX" valign="top"></td>
								  <td class="TX" valign="top"></td>
								  <td class="TX" valign="top"></td>
								  <?php
								  }
								  ?>
								  <td class="TX" valign="top"><a href="gen_doc.php?candid=<?php echo $candidat['cand_id'] ?>&opeid=<?php echo $ope_id_sav ?>&dossid=<?php echo $candidat['dossier_id'] ?>" target="_blank"><?php echo $candidat['dossier_nom'] ?></a></td>
								  <td class="TX" valign="top">
								  <?php
									echo $candidat['type_ope_libelle'];
								  ?>
								  </td>
								  <td class="TX" valign="top">
								  <?php
										echo $candidat['cert_nom'];
								  ?>
								  </td>
								  <td class="TX" valign="top">
								 <?php								
										/* Si la personne a commencé son questionnaire */
										if ($candidat['time_deb']!=''){
											$deb 	= mktime(substr($candidat['time_deb'], 8, 2), substr($candidat['time_deb'], 10, 2), substr($candidat['time_deb'], 12, 2), substr($candidat['time_deb'], 4, 2), substr($candidat['time_deb'], 6, 2), substr($candidat['time_deb'], 0, 4));
											/* Si la date de fin est définie */
											if ($candidat['time_fin']!=''){
												$fin = mktime(substr($candidat['time_fin'], 8, 2), substr($candidat['time_fin'], 10, 2), substr($candidat['time_fin'], 12, 2), substr($candidat['time_fin'], 4, 2), substr($candidat['time_fin'], 6, 2), substr($candidat['time_fin'], 0, 4));
											}else{
												/* Sinon on prend comme référence le time en cours */
												$fin = time();
											}
											$time_rep = $fin - $deb;
											if ($time_rep>0){
												$time_rep = $time_rep;
											}
											echo duree($time_rep);
										}
								 ?>
								  </td>
								  <td class="TX" valign="top"><?php
								  $bgcolor = "#F1F1F1";
									  if ($candidat['time_deb']!='' && $candidat['time_fin']!=''){
										//Vert
										$bgcolor="#00FF00";
									  }else{
										if ($candidat['time_deb']!=''){
											//Orange
											$bgcolor="#FFAA20";
										}else{
											//Rouge
											$bgcolor="#FF0000";
										}
									  }
									
									  echo '<div style="color:'.$bgcolor.'; background-color:'.$bgcolor.'; border-bottom: 1px solid #F1F1F1;">&nbsp;</div>';
								  ?>
								  </td>
								  <!--<td align="center" valign="top"> 
									<input type="checkbox" name="checkbox2" value="checkbox" disabled="disabled" <?php //if ($candidat['cand_actif']!='1') echo 'checked="checked"'; ?>>
								  </td>-->
								</tr>
								<tr> 
								  <td colspan="9" bgcolor="#CCCCCC" height="1" valign="top"></td>
								</tr>
							<?php
							}
						}
						?>
						</table>
					</td>
					<td width="14"></td>
				  </tr>
		      <tr> 
		      <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
			
          </td>
        </tr>		
		<?php
		}
		?>
      </table>
			<br>
			<table width="961">
			<tr><td align="center">
				<?php
				if ($_GET['certid']>0){
				?>
					<center><input type="submit" name="affect" value="G&eacute;n&eacute;rer un produit" class="bn_valider_candidat_big"></center>
				<?php
				}
				?>
				</td></tr>
			</table>
		</form>
    </td>
  </tr>
</table>
		</p></div>	</article></div>	</div>	</div>	</div>					
</body>
</html>
<?php
}else{
	include('no_acces.php');
}
?>
