
function getXhr()
{
	var xhr=null;
	if(window.XMLHttpRequest){ // Firefox et autres	   
		xhr = new XMLHttpRequest();
	}
	else if(window.ActiveXObject){
		try{
			xhr=new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e){
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
	}
	else{
		alert("<?= $t_browser_support_error_1 ?>");
		xhr=false;
	}
	return xhr;
}
function go(obj, zone)
{
	var docid = document.form2.docid.value;
	var langid = document.form2.langid.value;
	
	var selected_option;
	selected_option = obj.options[obj.selectedIndex].value;
	
	var div_name = obj.name+'_div';
	
	var xhr=getXhr();
	xhr.open("GET","ajax_supadmin_crea_questionnaire2.php?questid="+selected_option+"&docid="+docid+"&langid="+langid+"&zoneid="+zone,true);
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200){
		//alert(document.getElementById(div_name).innerHTML);
			var res=xhr.responseText;
			var temp_div = document.getElementById(div_name);
		
			if(temp_div.childNodes.length){
				temp_div.innerHTML = '';
				//temp_div.removeChild(temp_div.childNodes[0]);
			}

			var element = document.createElement('div');
			element.innerHTML = res;

			document.getElementById(div_name).appendChild(element);
		}
	}
	
	xhr.send(null);
}

