<?php
function right($value, $count){
    return substr($value, ($count*-1));
}

function left($string, $count){
    return substr($string, 0, $count);
}

session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']>5){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	$db = new db($conn);
		
	$sql_info_comptable = "select rpad(code_libelle,3) info from code where code_id = 52";
	$qry_info_comptable = $db->query($sql_info_comptable);	
    $code_soc = $qry_info_comptable[0]['info'];
	$sql_info_comptable = "select rpad(code_libelle,3) info from code where code_id = 53";
	$qry_info_comptable = $db->query($sql_info_comptable);	
    $code_etab = $qry_info_comptable[0]['info'];
	$sql_info_comptable = "select rpad(code_libelle,3) info from code where code_id = 54";
	$qry_info_comptable = $db->query($sql_info_comptable);	
    $code_journal = $qry_info_comptable[0]['info'];
	$sql_info_comptable = "select rpad(code_libelle,3) info from code where code_id = 55";
	$qry_info_comptable = $db->query($sql_info_comptable);	
    $code_fac = $qry_info_comptable[0]['info'];
	$sql_info_comptable = "select rpad(code_libelle,8) info from code where code_id = 56";
	$qry_info_comptable = $db->query($sql_info_comptable);	
    $code_ht = $qry_info_comptable[0]['info'];
	$sql_info_comptable = "select rpad(code_libelle,8) info from code where code_id = 57";
	$qry_info_comptable = $db->query($sql_info_comptable);	
    $code_tva = $qry_info_comptable[0]['info'];
	$sql_info_comptable = "select rpad(code_libelle,8) info from code where code_id = 58";
	$qry_info_comptable = $db->query($sql_info_comptable);	
    $code_ttc = $qry_info_comptable[0]['info'];
	
	$sql_export_comptable = "select cand_id, cand_a_ope.ope_id,num_facture,to_char(date_creation,'DDMMYYYY') date_facture,
	rpad(prod_nom,35) titre, to_char(HT_TRANSACTION/100,'99999.99') montant_ht,to_char(TTC_TRANSACTION/100,'99999.99') montant_ttc, to_char((TTC_TRANSACTION-HT_TRANSACTION)/100,'99999.99') montant_tva, rpad(num_transaction,35) num_transaction
	from operation,cand_a_ope,produit 
	where operation.ope_id=cand_a_ope.ope_id and operation.prod_id=produit.prod_id and ht_transaction is not null and flag_export is null and num_facture is not null order by num_facture";
	$qry_export_comptable = $db->query($sql_export_comptable);	
	
	if(is_array($qry_export_comptable)){
		$filename = './exports/OPRVAD'.date('dmYHis').'.txt';
		
		// On ouvre le fichier
		$fp = fopen($filename, "w+"); 

		// On ajoute a la variable data chaque ligne de sortie
		foreach($qry_export_comptable as $ligne){
			$data = $code_soc.$code_etab.$code_journal.$code_fac.$ligne['num_facture'].$ligne['date_facture'].$code_ht.$ligne['titre'].'D'.right('                     '.$ligne['montant_ht'],21).'EURCB '.$ligne['num_transaction'];
			$data  .= "\r\n";
			fputs($fp,$data);
			$data = $code_soc.$code_etab.$code_journal.$code_fac.$ligne['num_facture'].$ligne['date_facture'].$code_tva.$ligne['titre'].'D'.right('                     '.$ligne['montant_tva'],21).'EURCB '.$ligne['num_transaction'];
			$data  .= "\r\n";
			fputs($fp,$data);
			$data = $code_soc.$code_etab.$code_journal.$code_fac.$ligne['num_facture'].$ligne['date_facture'].$code_ttc.$ligne['titre'].'C'.right('                     '.$ligne['montant_ttc'],21).'EURCB '.$ligne['num_transaction'];
			$data  .= "\r\n";
			fputs($fp,$data);
			
			$sql_up_this = "UPDATE CAND_A_OPE SET FLAG_EXPORT=1 WHERE OPE_ID=".intval($ligne['ope_id'])." AND CAND_ID=".intval($ligne['cand_id'])."";
			$qry_up_this = $db->query($sql_up_this);
		}
		// On ferme le fichier
		fclose($fp);
		

		/* Téléchargement du fichier */
		header("Content-Length: " . filesize($filename));
		header('Content-Type: text/plain');
		header('Content-Disposition: attachment; filename=OPRVAD'.date('dmYHis').'.txt');
		header('Content-Transfer-Encoding: binary');
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		readfile($filename);		
		
	}else{
		?>
		<html>
			<head>

			</head>
			<body onLoad="window.close()";>
			</body>
		</html>
		<?php
	}
}
?>