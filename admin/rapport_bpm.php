<?php 

session_start();

include ("../config/lib/connex.php");
include ("../config/lib/db.oracle.php");

$db = new db($conn);

$opeid = $_GET['opeid'];

if (isset($opeid)){

	$manger_nom ="";
	$mangerplusun_nom ="";
	$manger_cand_id = null;
	$nb_personnes_mg=-2;

	$sql_select_question = "SELECT distinct CAND_ID FROM CAND_A_QUEST WHERE OPE_ID = ".txt_db(intval($opeid));
    $condidats   	 = $db->query($sql_select_question);
	    
    foreach($condidats as $condidat){
		$sql_infos_cand = "SELECT CANDIDAT.*,CAND_A_OPE.* FROM CANDIDAT, CAND_A_OPE WHERE CAND_A_OPE.CAND_ID = CANDIDAT.CAND_ID AND CANDIDAT.CAND_ACTIF='1' AND CANDIDAT.CAND_ID='".txt_db(intval($condidat['cand_id']))."'  AND CAND_A_OPE.OPE_ID=".txt_db(intval($opeid))."";
		$infos_candidat = $db->query($sql_infos_cand);            
        if ($infos_candidat[0]['niveau'] == 1){
			$manger_cand_id = $condidat['cand_id'];
        	$manger_nom = $infos_candidat[0]['cand_prenom'] . ' '. $infos_candidat[0]['cand_nom'];
			$cand_fonction = $infos_candidat[0]['cand_fonction'];
        }
        if ($infos_candidat[0]['niveau'] == 2){
        	$mangerplusun_nom = $infos_candidat[0]['cand_prenom'] . ' '. $infos_candidat[0]['cand_nom'];        	
        }        
		$nb_personnes_mg++;
    }
    
}
$deb_page_html = '
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VAKOM | Rapport BPM</title>
    <!-- Bootstrap -->
	<link href="http://www.preprod.bscom.fr/vakom/barometre/rapport/css/bootstrap.css" rel="stylesheet">
	<link href="http://www.preprod.bscom.fr/vakom/barometre/rapport/styles-rapport.css" rel="stylesheet">
  </head>
  <body>
	<script src="http://www.preprod.bscom.fr/vakom/barometre/rapport/js/jquery-1.11.3.min.js"></script>
	<script src="http://www.preprod.bscom.fr/vakom/barometre/rapport/js/bootstrap.js"></script>
  <div class="container">
		<header> 
		  <div class="row">
			<div class="col-lg-2"><img src="http://www.preprod.bscom.fr/vakom/barometre/rapport/img/logo-bpm.png"  alt="Logo"/></div>
			  <div class="col-lg-10 titre-barometre">Baromètre des postures <br>Managériales</div>
			  
		  </div>
	  </header> 
	  
	  
	  <div class="cadre-identification">
		  <div class="nom-fonction">
		  <p>'.$manger_nom.'</p>
		  </div>
		  
		  <div class="nom-fonction">
		  <p>'.$cand_fonction.'</p>
		  </div>
	 </div><!-- FIN cadre-identification -->
	  
	  
	 <div class="cadre-imageFond">
		  
	   <p class="paddingTop60">Date : </p>
		  <p>Entretiens réalisés par : </p>
		  <p>Entretien demandé par : </p>
		
	  </div> 
  </div><!-- FIN CONTAINER -->
  </body>
</html>';
$num_page = 1;
$chemin 		= "./temp/";
$file 			= $opeid.$num_page.".html";
$listfile 		.= $opeid.$num_page.".html,";
$fp = fopen($chemin . $file ,"w+");
fputs($fp,$deb_page_html);
fclose($fp);					
$debut_command= '/usr/local/bin/wkhtmltopdf -O Portrait -B 10 -s A4 ';
$commandline   	= $debut_command.$chemin.$file.' '.$chemin.$opeid.$num_page.'.pdf';	
passthru($commandline);
$commandline   	= $debut_command.$chemin.$file.' '.$chemin.$opeid.'.pdf';	
passthru($commandline);

//P2 : 
$deb_page_html = '
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VAKOM | Rapport BPM</title>
	<link href="http://www.preprod.bscom.fr/vakom/barometre/rapport/css/bootstrap.css" rel="stylesheet">
	<link href="http://www.preprod.bscom.fr/vakom/barometre/rapport/styles-rapport.css" rel="stylesheet">
  </head>
  <body>
	<script src="http://www.preprod.bscom.fr/vakom/barometre/rapport/js/jquery-1.11.3.min.js"></script>
	<script src="http://www.preprod.bscom.fr/vakom/barometre/rapport/js/bootstrap.js"></script>
  <div class="container">
		<div class="entete"><img src="http://www.preprod.bscom.fr/vakom/barometre/rapport/img/logo-bpm.png" width="279" height="90" alt=""/>
	   </div>
	  </div>  
	   <div class="container">
	 <p class="titre">Le management presque parfait</p>
	  
	  <p>Trop souvent le management est réduit par les esprits rationnels et passéistes à un ensemble de techniques, de méthodes. Cest trop souvent considéré comme une « boite à outils ».</p>
	  <p>&nbsp;</p>
		   
	  <p style="text-align: center; font-family: \'MonotypeCorsiva\'; font-size: 20px">« Le monde ne change pas, il a déjà changé »</p>
		   
	  <p>&nbsp;</p>
	  <p>Aujourdhui, à lheure de lentreprise libérée, réinventée, adaptée, libérante, à lheure de la révolution numérique, du virtuel, du 2.0 voire du 3.0 et des réseaux sociaux, la demande de relation humaine na jamais été aussi forte, aussi essentielle pour vivre dans cette société de liens.
Les managers formatés, techniquement armés, mais relationnellement maladroits, absents voire maltraitants continuent dappliquer ce quils ont appris en réduisant parfois le management à une discipline sans âme, sans relation, sans convictions et surtout sans reconnaissance de lautre.
</p>
		   <p>Voilà lenjeu des années à venir et voilà lutilité de notre <span class="orange">B</span>aromètre des <span class="orange">P</span>ostures <span class="orange">M</span>anagériales : <span class="orange">Redonner de lélan et du vivant dans lacte de manager</span> en privilégiant la posture de manager et son leadership.</p>
		   
		   <p>La plupart des dirigeants, des managers ont le sentiment que chez eux « ça va bien ou du moins cela va mieux quailleurs ». Pourquoi ? Sans doute parce quil est difficile dévaluer et daccepter le ressenti des managés et de ceux qui entourent le manager. </p>
		   
		   <p><span class="orange">B</span>aromètre des <span class="orange">P</span>ostures <span class="orange">M</span>anagériale permet au manager de découvrir et dadopter de nouvelles postures, afin de développer son leadership de manager humain et ainsi devenir un <span class="orange">véritable guide accessible</span> pour tous.</p>
		   
		   <p class="signature">Jean-Louis FEL<br>

Dirigeant de VAKOM
</p>
  </div><!-- FIN CONTAINER -->
  </body>
</html>';
$num_page = 2;
$chemin 		= "./temp/";
$file 			= $opeid.$num_page.".html";
$listfile 		.= $opeid.$num_page.".html,";
$fp = fopen($chemin . $file ,"w+");
fputs($fp,$deb_page_html);
fclose($fp);					
$commandline   	= $debut_command.$chemin.$file.' '.$chemin.$opeid.$num_page.'.pdf';	
passthru($commandline);
$commandline   	= $debut_command.$chemin.$file.' '.$chemin.$opeid.'.pdf';	
passthru($commandline);

//P3 :
$deb_page_html = '
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VAKOM | Rapport BPM</title>
    <!-- Bootstrap -->
	<link href="http://www.preprod.bscom.fr/vakom/barometre/rapport/css/bootstrap.css" rel="stylesheet">
	<link href="http://www.preprod.bscom.fr/vakom/barometre/rapport/styles-rapport.css" rel="stylesheet">
  </head>
  <body>
	<script src="http://www.preprod.bscom.fr/vakom/barometre/rapport/js/jquery-1.11.3.min.js"></script>
	<script src="http://www.preprod.bscom.fr/vakom/barometre/rapport/js/bootstrap.js"></script>
  <div class="container">
		  
		<div class="entete"><img src="http://www.preprod.bscom.fr/vakom/barometre/rapport/img/logo-bpm.png" width="279" height="90" alt=""/>
	  
	   </div>
	  </div>  
	 
	   <div class="container">
	  
	 <p class="theme">I. PREAMBULE</p>
	  
	  <ol>
	    <li class="puce">Contexte et finalité</li>
		  <p>Le baromètre des postures managériales est un outil qui permet danalyser les postures dun manager en situation de management dans son entreprise.</p>
		   
	  <p>Un questionnaire a été rempli par : le manager, son N+1, et ses collaborateurs.</p>
		   
	  <p>Il permet de confronter les attentes du management de lentreprise et des salariés avec le ressenti du manager sur 3 chapitres : Piloter une équipe  Piloter une organisation  Se Piloter</p>
		   
		   <p>Cette démarche permet didentifier, grâce à un certain nombre de graphiques les points forts et les marges de progrès du manager en vue de définir des axes de progrès et détablir un plan dactions individualisé. </p>
		  
	    <li class="puce">Démarche </li>
		  
		  <p><img src="http://www.preprod.bscom.fr/vakom/barometre/rapport/img/preambule.jpg" alt=""/></p>
		  
	    <li class="puce">Confidentialité </li>
		  
		  <p>Les contenus du Baromètre des Postures Managériales est strictement confidentiel. Sa diffusion par VAKOM est strictement limitée au manager, à son N+1. Il appartient à lentreprise de préciser une éventuelle utilisation plus large.</p>
		  
	    </ol>
  </div><!-- FIN CONTAINER -->
  </body>
</html> ';
$num_page = 3;
$chemin 		= "./temp/";
$file 			= $opeid.$num_page.".html";
$listfile 		.= $opeid.$num_page.".html,";
$fp = fopen($chemin . $file ,"w+");
fputs($fp,$deb_page_html);
fclose($fp);					

$commandline   	= $debut_command.$chemin.$file.' '.$chemin.$opeid.$num_page.'.pdf';	
passthru($commandline);
$commandline   	= $debut_command.$chemin.$file.' '.$chemin.$opeid.'.pdf';	
passthru($commandline);
echo "ok";
?>