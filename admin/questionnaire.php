<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo.css" type="text/css">
<link rel="stylesheet" href="../css/general.css" type="text/css">
<script language="JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
       <table width="961" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="360"><img src="../images/top2.jpg" width="389" height="121"></td>
		  <td>&nbsp;&nbsp;</td>
          <td><img src="../images/pap1.jpg" width="30" height="30" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="questionnaire.php" class="menu_Gris">Créer les questionnaires</a><br>
            <img src="../images/pap2.jpg" width="30" height="50" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="regles.php" class="menu_Gris">Créer les règles</a> <br>
			<img src="../images/pap4.jpg" width="30" height="30" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="documents.php" class="menu_Gris">Créer vos documents</a><br></td>
          <td valign="middle">
          </td>
          <td valign="middle"><img src="../images/pap5.jpg" width="30" height="30" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="question.php" class="menu_Gris">Diffuser les questionnaires</a><br>
            <img src="../images/pap3.jpg" width="30" height="50" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="resultats.php" class="menu_Gris">Suivre les résultats</a> 
			<br>
            <img src="../images/pap6.jpg" width="30" height="30" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="analyser.php" class="menu_Gris">Analyser les résultats</a></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="center" class="menu_Gris">&nbsp;</td>
  </tr>
  <tr> 
    <td align="right"> 
      <table width="961" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="180" valign="top" align="center"><font color="EA98AA" class="TX"><b>CANDIDAT 
            </b> </font></td>
          <td align="left" valign="top"> 
            <table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="center" class="TX"> 
                  <table width="600" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="TX">Nom du champ</td>
                      <td> 
                        <input type="text" name="nom" size="50" class="form_ediht">
                      </td>
                    </tr>
                    <tr> 
                      <td class="TX">Type du champ</td>
                      <td> 
                         <select name="select" class="form_ediht">
                          <option>Texte</option>
                          <option>Case à cocher</option>
                          <option>Bouton radio</option>
                        </select>
                      </td>
                    </tr>
                    <tr> 
                      <td class="TX">Taille du champ</td>
                      <td> 
                        <select name="select" class="form_ediht">
                          <option>5</option>
                          <option>10</option>
                          <option>15</option>
                          <option selected>20</option>
                          <option>25</option>
                        </select>
                      </td>
                    </tr>
					<tr> 
                      <td class="TX">Emplacement du champ</td>
                      <td> 
                        <select name="select" class="form_ediht">
                          <option>Zone A</option>
                          <option>Zone B</option>
                          <option>Zone C</option>
                          <option selected>Zone D</option>
                          <option>Zone E</option>
                        </select>
                      </td>
                    </tr>
					<tr>
					<td>&nbsp;</td>
					</tr>
					<tr>
					 <td align="center" colspan="2">
      <input type="button" name="Submit" value="Valider" class="BN" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
    </td>
					</tr>
                  </table>
                </td>
                <td width="14"></td>
              </tr>
              <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
          </td>
        </tr>
		 <tr> 
          <td width="180" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
		<tr> 
          <td width="180" valign="top" align="center"><font color="EA98AA" class="TX"><b>CREATION DU <br />QUESTIONNAIRE
            </b> </font><br/><a href="charte.php">Charte graphique</a></td>
          <td align="left" valign="top"> 
            <table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="center" class="TX"> 
                  <table width="600" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="TX">Nom du questionnaire</td>
                      <td> 
                        <input type="text" name="nom" size="50" class="form_ediht">
                      </td>
                    </tr>
                    <tr> 
                      <td class="TX">Type du questionnaire</td>
                      <td> 
                         <select name="select" class="form_ediht">
                          <option>Texte</option>
                          <option>Case à cocher</option>
                          <option>Bouton radio</option>
                        </select>
                      </td>
                    </tr>
					<tr> 
                      <td class="TX">Langue</td>
                      <td> 
                        <select name="langue">
							<option>Allemand</option>
                          <option>Anglais</option>
                          <option>Espagnol</option>
                          <option selected>Fran&ccedil;ais</option>
                          <option>Italien</option>
						</select>
                      </td>
                    </tr>
					<tr>
					<td>&nbsp;</td>
					</tr>
					<tr>
					 <td align="center" colspan="2">
      <input type="button" name="Submit" value="Valider" class="BN" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
    </td>
					</tr>
                  </table>
                </td>
                <td width="14"></td>
              </tr>
              <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td width="180" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
		<tr> 
          <td width="180" valign="top" align="center"><font color="EA98AA" class="TX"><b>CREATION DU <br />GROUPE
            </b> </font><br/><a href="liste.php">Liste des groupes</a></td>
          <td align="left" valign="top"> 
            <table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="center" class="TX"> 
                  <table width="600" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="TX">Nom du groupe</td>
                      <td> 
                        <input type="text" name="nom" size="50" class="form_ediht">
                      </td>
                    </tr>
                    <tr> 
                      <td class="TX">Catégorie du groupe</td>
                      <td> 
                         <select name="select" class="form_ediht">
                          <option>Signalétique</option>
                        </select>
                      </td>
                    </tr>
					<tr> 
                      <td class="TX">Liste des questionnaires</td>
                      <td> 
                        <select name="langue">
						  <option>Questionnaire 1</option>
                          <option>Questionnaire 2</option>
						</select>
                      </td>
                    </tr>
					<tr>
					<td>&nbsp;</td>
					</tr>
					<tr>
					 <td align="center" colspan="2">
      <input type="button" name="Submit" value="Valider" class="BN" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
    </td>
					</tr>
                  </table>
                </td>
                <td width="14"></td>
              </tr>
              <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
          </td>
        </tr>
		<tr> 
          <td width="180" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
		<tr> 
          <td width="180" valign="top" align="center"><font color="EA98AA" class="TX"><b>ADJECTIFS
            </b> </font></td>
          <td align="left" valign="top"> 
            <table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="center" class="TX"> 
                  <table width="600" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="TX">Liste des groupes</td>
                      <td> 
                        <select name="langue">
						  <option>Groupe 1</option>
                          <option>Groupe 2</option>
						</select>
                      </td>
                    </tr>
                    <tr> 
                      <td class="TX">Adjectif 1</td>
                      <td> 
                         <input type="text" name="nom" size="50" class="form_ediht">
                      </td>
                    </tr>
					 <tr> 
                      <td class="TX">Adjectif 2</td>
                      <td> 
                         <input type="text" name="nom" size="50" class="form_ediht">
                      </td>
                    </tr>
					 <tr> 
                      <td class="TX">Adjectif 3</td>
                      <td> 
                         <input type="text" name="nom" size="50" class="form_ediht">
                      </td>
                    </tr>
					 <tr> 
                      <td class="TX">Adjectif 4</td>
                      <td> 
                         <input type="text" name="nom" size="50" class="form_ediht">
                      </td>
                    </tr>
					<tr>
					<td>&nbsp;</td>
					</tr>
					<tr>
					 <td align="center" colspan="2">
      <input type="button" name="Submit" value="Valider" class="BN" onClick="MM_goToURL('parent','#');return document.MM_returnValue">
    </td>
					</tr>
                  </table>
                </td>
                <td width="14"></td>
              </tr>
              <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
          </td>
        </tr>
  <tr> 
    <td align="right" width="180">&nbsp; </td>
  </tr>
</table>
</body>
</html>
