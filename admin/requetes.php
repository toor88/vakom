<?php
session_start();

$delRequestAV = 0;
$delRequest = 0;

if($_SESSION['droit']>4){
	$id_utilisateur = $_SESSION['vak_id'];
	$delRequestAV = 1;
}else{
	$id_utilisateur = $_SESSION['cert_id'];
	$delRequest = $id_utilisateur;
}

if($_SESSION['droit']>5){
	$where_liste_req = "USER_ID=".intval($id_utilisateur);
}else{
	if($_SESSION['droit']==4){
		$where_liste_req = "USER_ID=".intval($id_utilisateur)." OR ((USER_ID=".$_SESSION['part_id']." AND INDIC_ADM=1) OR (USER_ID IN (SELECT VAK_ID FROM USER_VAKOM WHERE VAK_TYPE LIKE '%AV') AND INDIC_ADM=1))";
	}
	if($_SESSION['droit']==2){
		$where_liste_req = "USER_ID=".intval($id_utilisateur)." OR ((USER_ID=".$_SESSION['part_id']." AND INDIC_CERT=1) OR (USER_ID IN (SELECT VAK_ID FROM USER_VAKOM WHERE VAK_TYPE LIKE '%AV') AND INDIC_CERT=1))";
	}
}

include ("../config/lib/connex.php");
include ("../config/lib/db.oracle.php");

if(!isset($_GET['del'])){	// effacement des requetes provisoires
	//echo 'ici';
	$sqlDel1 = "DELETE FROM requete_a_filtre WHERE num_requete IN (SELECT num_requete FROM user_requete WHERE user_id = ".intval($id_utilisateur)." AND provisoire = 1)";
	$sqlDel2 = "DELETE FROM requete_a_chp WHERE num_requete IN (SELECT num_requete FROM user_requete WHERE user_id = ".intval($id_utilisateur)." AND provisoire = 1)";
	$sqlDel3 = "DELETE FROM user_requete WHERE user_id = ".intval($id_utilisateur)." AND provisoire = 1";
//echo $sqlDel3;
	$db = new db($conn);
	$db->query($sqlDel1);
	$db->query($sqlDel2);
	$db->query($sqlDel3);
}
if(isset($_GET['delReq'])){	// effacement de la requete
	$sqlDel1 = "DELETE FROM requete_a_filtre WHERE num_requete = ".$_GET['delReq'];
	$sqlDel2 = "DELETE FROM requete_a_chp WHERE num_requete = ".$_GET['delReq'];
	$sqlDel3 = "DELETE FROM user_requete WHERE num_requete = ".$_GET['delReq'];

	$db = new db($conn);
	$db->query($sqlDel1);
	$db->query($sqlDel2);
	$db->query($sqlDel3);
}
// Si l'utilisateur est un super admin
if ($_SESSION['droit']>0){

	$db = new db($conn);
	
	$tab_operateurs = array(
		1=>'&gt;',
		2=>'&gt; =',
		3=>'=',
		4=>'&lt;',
		5=>'&lt; =',
		6=>'Contient',
		7=>'Commence par',
		8=>'In',
		9=>'&lt;&gt;'
	);
	if($_POST['step']==1){
	###################################
	// Création d'un nouveau libellé de requête
		if($_POST['choix_action'] == 1){ // Sélection
			header('location: requetes.php?req_id='.$_POST['select_req'].'&type=1&del=0');
		}
		if($_POST['choix_action'] == 2 && trim($_POST['txt_nom_req'])!=''){ // Création
			$sql_nb 		= "SELECT SEQ_ID.NEXTVAL NB FROM DUAL";
			$qry_nb 		= $db->query($sql_nb);
			$num_requete 	= $qry_nb[0]['nb'];
//			if($_POST['prov'] == '1')
			if (txt_db($_POST['txt_nom_req']) == 'Provisoire') {
				$prov		= 1;
			}
			else
			{
				$prov		= 0;
			}
			$sql_new_req 	= "INSERT INTO USER_REQUETE (NUM_REQUETE, USER_ID, LIB_REQUETE, provisoire) VALUES (".intval($num_requete).", ".intval($id_utilisateur).", '".txt_db($_POST['txt_nom_req'])."', ".$prov.")";
	//		echo $sql_new_req;
			$qry_new_req	= $db->query($sql_new_req);
			header('location: requetes.php?req_id='.$num_requete.'&type=1&del=0');
		}
		if($_POST['choix_action'] == 3 && $_POST['select_req2']>0 && trim($_POST['txt_nom_req2'])!=''){ // Duplication
			$sql_nb 		= "SELECT SEQ_ID.NEXTVAL NB FROM DUAL";
			$qry_nb 		= $db->query($sql_nb);
			$num_requete 	= $qry_nb[0]['nb'];
			
			// On duplique les filtres
			$sql_ins_filtre	=  "insert into requete_a_filtre (select ".intval($num_requete).",num_ligne,INDIC_TABLE,INDIC_CHP,OPERATEUR,CRITERE,AND_OR from requete_a_filtre where num_requete=".intval($_POST['select_req2']).")";
			$qry_ins_filtre	= $db->query($sql_ins_filtre);
			
			// On duplique les champs
			$sql_ins_chp	=  "insert into requete_a_chp (select seq_id.nextval, ".intval($num_requete).",INDIC_TABLE,INDIC_CHP,ORDRE,TRI,AFFICHE from requete_a_chp where num_requete=".intval($_POST['select_req2']).")";
			$qry_ins_chp	= $db->query($sql_ins_chp);

			// On insère les infos de la requete
			$sql_new_req 	= "INSERT INTO USER_REQUETE (NUM_REQUETE, USER_ID, LIB_REQUETE) VALUES (".intval($num_requete).", ".intval($id_utilisateur).", '".txt_db($_POST['txt_nom_req2'])."')";
			$qry_new_req	= $db->query($sql_new_req);
			
			
			header('location: requetes.php?req_id='.$num_requete.'&type=1&del=0');
		}
		
	###################################
	}
		
	unset($selected1);
	unset($selected2);
	unset($selected3);
	unset($selected4);
	unset($selected5);
	unset($selected6);
	unset($selected7);
	unset($selected8);
	unset($selected9);
	unset($selected10);
	unset($selected11);
	unset($selected12);
	unset($selected13);
	unset($selected14);
	
	switch($_GET['type']){
		case 1:
		default:
			$selected1 	= 'checked="checked"';
			$lib_type 	= 'SOCIETE';
			break;
		case 2:
			$selected2 	= 'checked="checked"';
			$lib_type 	= 'CANDIDAT';
			break;
		case 3:
			$selected3 	= 'checked="checked"';
			$lib_type 	= 'CERTIFIE';
			break;
		case 4:
			$selected4 	= 'checked="checked"';
			$lib_type 	= 'PARTENAIRE';
			break;
		case 5:
			$selected5 	= 'checked="checked"';
			$lib_type 	= 'PRODUIT';
			break;
		case 6:
			$selected6 	= 'checked="checked"';
			$lib_type 	= 'QUESTIONNAIRE';
			break;
		case 7:
			$selected7 	= 'checked="checked"';
			$lib_type 	= 'REPONSE';
			break;
		case 8:
			$selected8 	= 'checked="checked"';
			$lib_type 	= 'JETON-PARTENAIRE';
			break;
		case 9:
			$selected9 	= 'checked="checked"';
			$lib_type 	= 'JETON-CERTIFIE';
			break;
		case 10:
			$selected10 	= 'checked="checked"';
			$lib_type 	= 'PN';
			break;
		case 11:
			$selected11 	= 'checked="checked"';
			$lib_type 	= 'PR';
			break;
		case 12:
			$selected12 	= 'checked="checked"';
			$lib_type 	= 'PNPI';
			break;
		case 13:
			$selected13 	= 'checked="checked"';
			$lib_type 	= 'PNPR';
			break;
		case 14:
			$selected14 	= 'checked="checked"';
			$lib_type 	= 'PRPI';
			break;
	}
	
	if(intval($_GET['req_id'])>0 && intval($_GET['type'])>0){
	$step = 2;
	
	###################################
	// Ajout d'un champ à la requête
	if($_POST['ajouter']){
		if(is_array($_POST['che_champs'])){
			foreach($_POST['che_champs'] as $champ_coche){
				$tab_champ_coche 	= explode('_',$champ_coche);
				$indic_chp			= $tab_champ_coche[0];
				$indic_table		= $tab_champ_coche[1];
				
				// On vérifie que le champ n'existe pas dans la table
				$sql_verif 			= "SELECT * FROM REQUETE_A_CHP WHERE NUM_REQUETE=".intval($_GET['req_id'])." AND INDIC_TABLE=".intval($indic_table)." AND INDIC_CHP=".intval($indic_chp);
				$qry_verif			= $db->query($sql_verif);
				
				if(!is_array($qry_verif)){
					$sql_ins_champ 		= "INSERT INTO REQUETE_A_CHP (NUM_ID, NUM_REQUETE, INDIC_TABLE, INDIC_CHP) VALUES(SEQ_ID.NEXTVAL, ".intval($_GET['req_id']).", ".intval($indic_table).", ".intval($indic_chp).")";
					//echo $sql_ins_champ.'<br>';
					$db->query($sql_ins_champ);
				}
			}
		}
		header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&del=0&idnc='.($_GET['idnc']+1).'#2');
	}
	###################################

	

	###################################
	// Suppression d'un filtre pour la requête sélectionnée
	if($_GET['del']>0){
		$sql_delete 		= "DELETE FROM REQUETE_A_FILTRE WHERE NUM_LIGNE=".intval($_GET['del'])." AND NUM_REQUETE=".intval($_GET['req_id']);
		$qry_delete			= $db->query($sql_delete);
		
		header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&del=0&idnc='.($_GET['idnc']+1).'#3');
	}
	###################################
	
	
	###################################
	// Application d'un nouveau filtre
	if ($_POST['ajout_filtre']) {

		$tab_chp		= explode('_',$_POST['critere']);
		$indic_table 	= intval($tab_chp[0]);
		$indic_chp 		= intval($tab_chp[1]);
		$operateur 		= $_POST['operateur'];
		$valeur 		= txt_db($_POST['valeur']);
		$and_or 		= intval($_POST['and_or']);
		
		/*
		if ($_POST['num_id']){
			$sql  = "Update REQUETE_A_FILTRE SET NUM_REQUETE='".$infos['num_requete']."',INDIC_TABLE='".$infos['indic_table']."',INDIC_CHP='".$infos['indic_chp']."',OPERATEUR='".$infos['operateur']."',CRITERE='".$infos['critere']."', AND_OR='".$infos['and_or']."' Where NUM_LIGNE='".$_POST['num_id']."' ";
			//echo($sql);
			$_GET['mod'] = "";
			$requette = "";
		}*/

		$sql_ins_filtre  = "INSERT INTO REQUETE_A_FILTRE (NUM_REQUETE,NUM_LIGNE,INDIC_TABLE,INDIC_CHP,OPERATEUR,CRITERE, AND_OR) 
		VALUES (".intval($_GET['req_id']).", SEQ_ID.NEXTVAL, ".$indic_table.", ".$indic_chp.", '".$operateur."', '".$valeur."', '".$and_or."')";

		$db->query($sql_ins_filtre);
		
		header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&del=0&idnc='.($_GET['idnc']+1).'#3');
	}
	###################################
	
	
	###################################
	// Sauvegarder les champs de la requête
	if($_POST['sauver']){
		for($i=1; $i<($_POST['nb_champs']+1); $i++){
			unset($num_id);
			unset($tri);
			unset($ordre);
			
			$num_id 			= $_POST["ident$i"];
			$tri				= intval($_POST["tri$i"]);
			$ordre				= intval($_POST["ordre$i"]);
			$affiche			= intval($_POST["affiche$i"]);
			
			$sql_up_champs 		= "UPDATE REQUETE_A_CHP SET TRI=".intval($tri).", AFFICHE=".intval($affiche).", ORDRE=".intval($ordre)." WHERE NUM_ID=".intval($num_id);
			$qry_up_champs		= $db->query($sql_up_champs);	
		}
		
		if($_POST['proprio']==1){
			$sql_up_req 		= "UPDATE USER_REQUETE SET INDIC_CERT=".intval($_POST['che_droit_cert']).", INDIC_ADM=".intval($_POST['che_droit_adm'])." WHERE NUM_REQUETE=".intval($_GET['req_id']);
			$qry_up_req			= $db->query($sql_up_req);
		}
		header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&del=0&idnc='.($_GET['idnc']+1).'#2');
	}
	###################################
	
	
	###################################
	// Suppression d'un champ de la requête
	if($_POST['supprimer']){

 		if(is_array($_POST['che_del'])){
			foreach($_POST['che_del'] as $champ_delete){
				$sql_del_champ = "DELETE FROM REQUETE_A_CHP WHERE NUM_ID=".intval($champ_delete);
				//echo $sql_ins_champ.'<br>';
				$db->query($sql_del_champ);
			}
		}
		header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&del=0&idnc='.($_GET['idnc']+1).'#2');
	}	
	###################################
	
	
	###################################
	// Génération de la liste des champs disponibles
		$sql_champs = "SELECT * FROM LISTE_CHP WHERE INDIC_TABLE=".intval($_GET['type'])." AND CONCAT(LISTE_CHP.INDIC_TABLE, LISTE_CHP.INDIC_CHP) NOT IN (SELECT CONCAT(REQUETE_A_CHP.INDIC_TABLE, REQUETE_A_CHP.INDIC_CHP) FROM REQUETE_A_CHP WHERE NUM_REQUETE=".intval($_GET['req_id']).") AND LIB_CHAMP IS NOT NULL ORDER BY INDIC_CHP";
		$qry_champs = $db->query($sql_champs);
		
		if(is_array($qry_champs)){
			foreach($qry_champs as $data_champs){
				$liste_champs .= '<tr bgcolor="F1F1F1">
				  <td>'.htmlentities($data_champs['lib_champ']).'</td>
				  <td align="center"> 
					<input type="checkbox" name="che_champs[]" value="'.$data_champs['indic_chp'].'_'.$data_champs['indic_table'].'">
				  </td>
				</tr>';
			}
		}else{
			$liste_champs = '<tr bgcolor="F1F1F1"><td align="center" colspan="4">Aucun champ à sélectionner.</td></tr>';
		}
		
		
	// Informations sur la requête sélectionnée
		if($_SESSION['vak_id'] != 0)
			$sql_infos_req = "SELECT * FROM USER_REQUETE WHERE USER_REQUETE.NUM_REQUETE=".intval($_GET['req_id'])." AND (".$where_liste_req.")";
		else
			$sql_infos_req = "SELECT user_requete.*, cert_nom, cert_prenom FROM USER_REQUETE, (SELECT cert_nom, cert_prenom FROM certifie WHERE cert_id = (SELECT user_id FROM user_requete WHERE USER_REQUETE.NUM_REQUETE=".intval($_GET['req_id']).")) WHERE USER_REQUETE.NUM_REQUETE=".intval($_GET['req_id'])." AND (".$where_liste_req.")";
//		echo $sql_infos_req;
		$infos_req 		= $db->query($sql_infos_req);
		$lib_requete	= $infos_req[0]['lib_requete'];
		
		if ($infos_req[0]['user_id'] == $id_utilisateur){
			$proprietaire = 1;
		}else{
			$proprietaire = 0;
		}

	if($_GET['edit']){
		if ($_POST['edit_filtre']){

			$tab_chp		= explode('_',$_POST['critere']);
			$indic_table 	= intval($tab_chp[0]);
			$indic_chp 		= intval($tab_chp[1]);
			$operateur 		= $_POST['operateur'];
			$valeur 		= txt_db($_POST['valeur']);
			$and_or 		= intval($_POST['and_or']);

			$sql_up_filtre  = "UPDATE REQUETE_A_FILTRE SET INDIC_TABLE = ".$indic_table.", INDIC_CHP=".$indic_chp.",OPERATEUR='".$operateur."',CRITERE='".$valeur."', AND_OR='".$and_or."' WHERE NUM_LIGNE=".intval($_GET['edit']);
			//echo $sql_up_filtre;
			$db->query($sql_up_filtre);
			
			header('location:requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&del=0&idnc='.($_GET['idnc']+1).'#3');
		}	
	
		$sql_champs_a_modifier 		= "SELECT * FROM LISTE_CHP, USER_REQUETE, REQUETE_A_FILTRE 
		WHERE LISTE_CHP.INDIC_CHP=REQUETE_A_FILTRE.INDIC_CHP 
		AND LISTE_CHP.INDIC_TABLE=REQUETE_A_FILTRE.INDIC_TABLE 
		AND USER_REQUETE.NUM_REQUETE=REQUETE_A_FILTRE.NUM_REQUETE AND 
		USER_REQUETE.NUM_REQUETE=REQUETE_A_FILTRE.NUM_REQUETE 
		AND USER_REQUETE.NUM_REQUETE=".intval($_GET['req_id'])." AND (".$where_liste_req.") AND NUM_LIGNE=".intval($_GET['edit']);
		$qry_champs_req_a_modifier = $db->query($sql_champs_a_modifier);
		$valeur_a_modifier 		   = $qry_champs_req_a_modifier[0]['critere'];
		$operateur_a_modifier 	   = $qry_champs_req_a_modifier[0]['operateur'];
		
		$critere_a_modifier		   = $qry_champs_req_a_modifier[0]['indic_table'].'_'. $qry_champs_req_a_modifier[0]['indic_chp'];		
		
	}
		
		
	// Génération de la liste des champs pour la requête sélectionnée
		$sql_champs_req = "SELECT * FROM USER_REQUETE, REQUETE_A_CHP, LISTE_CHP 
		WHERE LISTE_CHP.INDIC_CHP=REQUETE_A_CHP.INDIC_CHP 
		AND LISTE_CHP.INDIC_TABLE=REQUETE_A_CHP.INDIC_TABLE 
		AND USER_REQUETE.NUM_REQUETE=REQUETE_A_CHP.NUM_REQUETE 
		AND USER_REQUETE.NUM_REQUETE=".intval($_GET['req_id'])." AND (".$where_liste_req.") ORDER BY ORDRE";
		$qry_champs_req = $db->query($sql_champs_req);
		
		if(is_array($qry_champs_req)){
			$j=0;
			foreach($qry_champs_req as $data_champs_req){
				unset($checked_tri);
				unset($checked_affich);
				if(intval($data_champs_req['tri'])==1){
					$checked_tri = ' checked="checked"';
				}
				
				if(intval($data_champs_req['affiche'])==1){
					$checked_affich = ' checked="checked"';
				}
				$j++;
				$champs_req .= '<tr bgcolor="FFFFFF">
				  <td>'.htmlentities($data_champs_req['lib_champ']).'</td>
				  <td align="center"> 
					<input type="hidden" name="ident'.$j.'" value="'.intval($data_champs_req['num_id']).'"><input type="text" name="ordre'.$j.'" value="'.intval($data_champs_req['ordre']).'" class="form_ediht_Requetes" size="2">
				  </td>
				  <td align="center" valign="middle"> 
					<input type="checkbox" name="tri'.$j.'" value="1"'.$checked_tri.'>
				  </td>
				  <td align="center" valign="middle"> 
					<input type="checkbox" name="affiche'.$j.'" value="1"'.$checked_affich.'>
				  </td>
				  <td align="center" valign="middle"><input type="checkbox" name="che_del[]" value="'.intval($data_champs_req['num_id']).'"></td>
				</tr>';
				unset($champ_a_modifier);
				if($critere_a_modifier == intval($data_champs_req['indic_table']).'_'.intval($data_champs_req['indic_chp'])){
					$champ_a_modifier=' selected="selected"';
				}
				$options_req .= '<option value="'.intval($data_champs_req['indic_table']).'_'.intval($data_champs_req['indic_chp']).'"'.$champ_a_modifier.'>'.htmlentities($data_champs_req['lib_champ']).'</option>';
			}
			$champs_req .= '<input type="hidden" name="nb_champs" value="'.$j.'"';
		}else{
			$champs_req = '<tr bgcolor="F1F1F1"><td align="center" colspan="4">Aucun champ affecté à cette requête.</td></tr>';
		}
	
	
	// Génération de la liste des filtres enregistrés pour la requête sélectionnée
		$sql_champs_req = "SELECT * FROM LISTE_CHP, USER_REQUETE, REQUETE_A_FILTRE 
		WHERE LISTE_CHP.INDIC_CHP=REQUETE_A_FILTRE.INDIC_CHP 
		AND LISTE_CHP.INDIC_TABLE=REQUETE_A_FILTRE.INDIC_TABLE 
		AND USER_REQUETE.NUM_REQUETE=REQUETE_A_FILTRE.NUM_REQUETE AND 
		USER_REQUETE.NUM_REQUETE=REQUETE_A_FILTRE.NUM_REQUETE 
		AND USER_REQUETE.NUM_REQUETE=".intval($_GET['req_id'])." AND (".$where_liste_req.")";
		$qry_champs_req = $db->query($sql_champs_req);
		
		if($proprietaire==1){
			$nb_col = 4;
		}else{
			$nb_col = 3;
		}
		if(is_array($qry_champs_req)){
			foreach($qry_champs_req as $filtre){
				if($filtre['num_ligne'] != $_GET['edit']){
				$tableau_filtres	.= '<tr bgcolor="F1F1F1"> 
					<td bgcolor="F1F1F1">'.htmlentities($filtre['lib_champ']).'</td>
					<td align="center">'.$tab_operateurs[$filtre['operateur']].'</td>
					<td align="center" valign="middle">'.$filtre['critere'].'</td>';
					if($proprietaire==1){
						$tableau_filtres	.='<td align="center" valign="middle"><a href="requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&edit='.$filtre['num_ligne'].'&del=0#3"><img src="./modifier.png" height="12" border="0"></a><td align="center" valign="middle"><a href="javascript:void(0);" onclick="return confirmAction(\'requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&del='.$filtre['num_ligne'].'#3\');"><img src="../images/icon_supp2.gif" width="11" height="12" border="0"></a></td></td>';
					}
					$tableau_filtres	.='</tr>';
				}
			}
		}else{
			$tableau_filtres = '<tr bgcolor="F1F1F1"><td colspan="'.$nb_col.'" align="center">Aucun filtre n\'est enregistré pour cette requête.</td></tr>';
		}
		
	###################################
	}
	
	
	###################################
	// génération de la liste des requêtes enregistrées
	
	$sql_sel_req = "SELECT DISTINCT * FROM USER_REQUETE WHERE ".$where_liste_req." ORDER BY LIB_REQUETE";
	$qry_sel_req = $db->query($sql_sel_req);
	$js_array_content = '';
	if(is_array($qry_sel_req)){
		$js_array_content .= 'var requestOwner = new Array(';
		foreach($qry_sel_req as $sav_req){
			unset($selected_requete);
			if($sav_req['num_requete'] == $_GET['req_id']){
				$selected_requete = 'selected="selected"';
			}
			$options_sav_req .= '<option value="'.$sav_req['num_requete'].'"'.$selected_requete.'>'.htmlentities($sav_req['lib_requete']).'</option>';
			$js_array_content .= "'".$sav_req['user_id']."',";
		}
		$js_array_content = substr($js_array_content,0, strlen($js_array_content)-1);
		$js_array_content .= ");"."\n";
	}
	###################################
	?>
	
<html>
<head>
<title>Vakom - Requetes</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<link rel="stylesheet" href="../css/style.css" type="text/css">
<script language="JavaScript">
<!--
<?php echo $js_array_content; ?>
<?php echo 'var av = '.$delRequestAV.';'."\n"; ?>
<?php echo 'var userID = '.$delRequest.';'."\n"; ?>

function change_type(typid){
	<?php if(isset($_GET['del']))
		echo "document.location.href='requetes.php?req_id=".intval($_GET['req_id'])."&showDiv=1&type='+typid+'&del=0#2'";
	else
		echo "document.location.href='requetes.php?req_id=".intval($_GET['req_id'])."&showDiv=1&type='+typid+'#2'";
	 ?>
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function MM_openBrWindow(theURL,winName) { //v2.0
  window.open(theURL,winName);
}
function confirmAction(which){
	var msg = '<?php echo $t_supprimer_suite ?>';
	if(confirm(msg)){
		if(which == 2)
			document.forms['form'+which].submit();
		else
			window.location = which;
	}
	else
		return false;
}
function supRequest(){
 	if(av != 0){
		if(confirm('<?php echo $t_supprimer_requete_1 ?>'))
			delRequest(document.form1.select_req.options[document.form1.select_req.options.selectedIndex].value);
	}
	else if(userID != 0){
		if(requestOwner[document.form1.select_req.options.selectedIndex] == userID){
			if(confirm('<?php echo $t_supprimer_requete_1 ?>')){
				delRequest(document.form1.select_req.options[document.form1.select_req.options.selectedIndex].value);
				}
		}
		else
			alert('<?php echo $t_supprimer_requete ?>');
	}
}
function delRequest(request){
	window.location = "requetes.php?delReq="+request;
}

function bloque () {
document.getElementById('choix2').checked=true;
	if (document.getElementById('prov').checked==true)
	{
		document.getElementById('txt_nom_req').readOnly = true;
		document.getElementById('txt_nom_req').value='Provisoire';
	}
	if (document.getElementById('prov').checked==false)
	{
		document.getElementById('txt_nom_req').readOnly = false;
		document.getElementById('txt_nom_req').value='';
	}
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
<?php //print_r($_SESSION);?>
	<?php
		$_GET['menu_selected']=6;
		include("menu_top_new.php");
	?>
	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
								
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  
    <td class="Titre_Requetes"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $t_titre ?></td>
		</tr>
	  </table>
	  
<form method="post" action="#" name="form1">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="fond_tablo_requetes">
    <tr>
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="center"> 
        <table border="0" cellspacing="0" cellpadding="0" align="center" class="TX">
		<?php
		if($options_sav_req!=''){
		 ?>
		 <tr> 
            <td align="left" class="TX"><input type="radio" name="choix_action" value="1" id="choix1" checked="checked"/>&nbsp;<?php if($options_sav_req!=''){ echo $t_radio1.' :&nbsp;&nbsp;'; } ?></td>
            <td align="left" class="TX"> 
				<?php
					echo '<select onfocus="document.getElementById(\'choix1\').checked=true;" name="select_req" class="form_ediht_Requetes">';
					echo $options_sav_req;
					echo '</select>';
				?>
				<input type="button" value="Supprimer" class="bn_ajouter" onClick="supRequest();">				
            </td>
          </tr>
		  <?php
		}
		?>
          <tr> 
            <td align="left" class="TX">
			<input type="radio" name="choix_action" value="2" id="choix2" />&nbsp;<?php echo $t_radio2 ?> :&nbsp;&nbsp;</td>
            <td align="left" class="TX"> 
              <input type="text" onFocus="document.getElementById('choix2').checked=true;" id="txt_nom_req" name="txt_nom_req" class="form_ediht_Requetes" size="50">
			  <input type="checkbox" value="1" id="prov" onClick="bloque();"> <?php echo $t_radio2_prov ?>
            </td>
          </tr>
          <tr> 
            <td align="left" class="TX">
			<input type="radio" name="choix_action" value="3" id="choix3" />&nbsp;<?php echo $t_radio3 ?> :&nbsp;&nbsp;</td>
            <td align="left" class="TX"> 
				<?php
				if($options_sav_req!=''){
					echo '<select onfocus="document.getElementById(\'choix3\').checked=true;" name="select_req2" class="form_ediht_Requetes">';
					echo $options_sav_req;
					echo '</select>';
				}
				?>		
              <input type="text" onFocus="document.getElementById('choix3').checked=true;" name="txt_nom_req2" class="form_ediht_Requetes" size="50">
            </td>
          </tr>
        </table>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
    </tr>
  </table>
  <br>
  <table cellpadding="0" cellspacing="0" width="100%" align="center">
    <tr>
      <td align="center">
		<input type="hidden" name="step" value="1">
		<input type="submit" name="valide" value="<?php echo $t_valid1 ?>" class="bn_valider_requete">
      </td>
    </tr>
  </table>
</form>
<br>
<?php
if($step>1){
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td width="20">&nbsp;</td>				  
			<td class="Titre_Requetes"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $lib_requete ?></td>
		</tr>
	 </table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="fond_tablo_requetes">
	  <tr> 
		<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
	  </tr>
		<tr> 
		<td width="14"></td>
		<td align="center" class="TX">
		  <input type="button" name="ap" value="VISUALISER" class="bn_valider_requete" onClick="MM_openBrWindow('requetes_apercu.php?req_id=<?php echo $_GET['req_id'] ?>','')">
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="export" value="EXPORTER" class="bn_valider_requete" onClick="window.location='requetes_apercu.php?req_id=<?php echo $_GET['req_id'] ?>&export=1'"> <!--onClick="MM_goToURL('parent','#');return document.MM_returnValue">-->
		</td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
	  </tr>
	</table>
	<p>&nbsp;</p>
	<form action="#" method="post" name="form2" id="formData">
	<input type="hidden" name="sauver" value="1" />
	<a name="2"></a>
		  <?php
	  if($proprietaire==1){
		?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
		  <tr> 
			<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			<td height="14"></td>
			<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		  </tr>
		  <tr> 
			<td width="14"></td>
			<td class="TX_Requetes" align="left"><?php echo $t_restrict ?> (<?php  echo $_SESSION['nom'].' '.$_SESSION['prenom'].', ';
			if($_SESSION['vak_id'] == 1)
				echo 'Administrateur Super';
			elseif($_SESSION['vak_id'] == 2)
				echo 'VAKOM Administrateur';
			else
				echo $infos_req[0]['cert_nom'].' '.$infos_req[0]['cert_prenom'];
			?>)</td>
			<td width="14"></td>
		  </tr>
		  <tr> 
			<td width="14" height="1"></td>
			<td bgcolor="#666666" height="1"></td>
			<td width="14" height="1"></td>
		  </tr>
		  <tr>
		  <td colspan="3">&nbsp;</td>
		  </tr>
	  		<?php
			  unset($checked_1);
			  unset($checked_2);
			  if($infos_req[0]['indic_cert']==1){
				$checked_1 = ' checked="checked"';
			  }
			  if($infos_req[0]['indic_adm']==1){
				$checked_2 = ' checked="checked"';
			  }
			  ?>
			<tr>
			  <td width="14"></td>
			  <td class="TX">
				  <input type="checkbox" value="1" name="che_droit_cert"<?php echo $checked_1?>/>&nbsp;<?php echo $t_acces1 ?><br>
				  <input type="checkbox" value="1" name="che_droit_adm"<?php echo $checked_2?>/>&nbsp;<?php echo $t_acces2 ?>
			  <input type="hidden" name="proprio" value="<?php echo $proprietaire ?>" />
			  </td>
			  <td width="14"></td>
			</tr>
		</td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
	  </tr>
	</table>
		<?php
	  }
	  ?>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="0"  >
	  <tr> 
		<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
	  </tr>
	  <tr> 
		<td width="14"></td>
		<td class="TX_Requetes" align="left"><?php echo $t_data ?></td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="1"></td>
		<td bgcolor="#666666" height="1"></td>
		<td width="14" height="1"></td>
	  </tr>
	  <tr> 
		<td width="14"></td>
		<td class="TX"> 
		  <table border="0" cellspacing="0" cellpadding="0" style="width:800px" >
			<tr> 
			  <td height="14"></td>
			</tr>
			<tr> 
			  <td align="left" class="TX">
				<table class="tabloGeneral">
					<tr> 
					  <td class="TX_bold" width="400"><?php echo $t_data_table_col1 ?></td>
					  <td class="TX_bold" align="center"><?php echo $t_data_table_col2 ?></td>
					  <td class="TX_bold" align="center"><?php echo $t_data_table_col3 ?></td>
					  <td class="TX_bold" align="center"><?php echo $t_data_table_col4 ?></td>
					  <td class="TX_bold" align="center"><?php echo $t_data_table_col5 ?></td>
					</tr>
					<?php
					if($champs_req!=''){
						echo $champs_req;
					}
					?>
				</table>
				<table align="right">
					<tr><td class="TX">
					<?php if($champs_req != '' && $proprietaire==1) {
					?>
					<input type="button" class="bn_ajouter" onClick="document.getElementById('div_ajout_donnee').style.display='block';window.location=window.location.pathname + window.location.search+'#add_data';" value="<?php echo$t_data_table_add?>">&nbsp;
					<input type="submit" name="supprimer" value="<?php echo$t_data_table_del?>" class="bn_ajouter" onClick="return confirmAction(2);">
					<?php } ?>
					</td></tr>
				</table>
			 </td>
			</tr>
		   </table>
		</td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
	  </tr>
	</table>
	<a name="add_data"></a>
	<div id="div_ajout_donnee" <?php if($_GET['showDiv'] == 1 ) echo 'style="display: block;"'; ?>>
	<table width="800" border="0" cellspacing="0" cellpadding="0"  align="center">
	  <tr> 
		<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
	  </tr>
	  <tr> 
		<td width="14"></td>
		<td class="TX_Requetes" align="left"><?php echo $t_newData ?></td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="1"></td>
		<td bgcolor="#666666" height="1"></td>
		<td width="14" height="1"></td>
	  </tr>
	  <tr> 
		<td width="14"></td>
		<td align="center" class="TX"> 
		  <table border="0" cellspacing="0" cellpadding="0" >
			<tr> 
			  <td height="14"></td>
			</tr>
			<tr> 
			  <td align="left" class="TX">
					<table style="width:800px; border:0" cellspacing="0" cellpadding="0">
					  <tr><td class="TX" align="center">							
						<input type="radio" name="Bn4" value="4" onClick="change_type(4)" <?php echo $selected4 ?>> <?php echo $t_newData_radio4 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn8" value="8" onClick="change_type(8)" <?php echo $selected8 ?>> <?php echo $t_newData_radio8 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn3" value="3" onClick="change_type(3)" <?php echo $selected3 ?>> <?php echo $t_newData_radio3 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn9" value="9" onClick="change_type(9)" <?php echo $selected9 ?>> <?php echo $t_newData_radio9 ?>&nbsp;
						<input type="radio" name="Bn1" value="1" onClick="change_type(1)" <?php echo $selected1 ?>> <?php echo $t_newData_radio1 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn2" value="2" onClick="change_type(2)" <?php echo $selected2 ?>> <?php echo $t_newData_radio2 ?>&nbsp;&nbsp;
						</td>
					  </tr>
					  <tr>
                      <td class="TX" align="center">							
						<input type="radio" name="Bn5" value="5" onClick="change_type(5)" <?php echo $selected5 ?>> <?php echo $t_newData_radio5 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn6" value="6" onClick="change_type(6)" <?php echo $selected6 ?>> <?php echo $t_newData_radio6 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn10" value="10" onClick="change_type(10)" <?php echo $selected10 ?>> <?php echo $t_newData_radio10 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn11" value="11" onClick="change_type(11)" <?php echo $selected11 ?>> <?php echo $t_newData_radio11 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn12" value="12" onClick="change_type(12)" <?php echo $selected12 ?>><?php echo $t_newData_radio12 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn13" value="13" onClick="change_type(13)" <?php echo $selected13 ?>> <?php echo $t_newData_radio13 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn14" value="14" onClick="change_type(14)" <?php echo $selected14 ?>> <?php echo $t_newData_radio14 ?>&nbsp;&nbsp;
						<input type="radio" name="Bn7" value="7" onClick="change_type(7)" <?php echo $selected7 ?>> <?php echo $t_newData_radio7 ?>&nbsp;&nbsp;
						</td>
					  </tr>
					  <tr> 
						<td width="700" valign="top"> 
						  <table class="tabloGeneral">
							<tr> 
							  <td class="TX_bold"><?php echo $t_newData_table_col1 ?></td>
							  <td class="TX_bold" align="center" width="55"><?php echo $t_newData_table_col2 ?></td>
							</tr>
							<?php
							if($liste_champs != ''){
								echo $liste_champs;
							}
							?>
						  </table>
						</td>
					  </tr>
					  <tr><td align="right"><?php if($liste_champs != '' && $proprietaire==1) echo '<input type="button" style="margin-top:2px" value="'.$t_newData_table_cancel.'" class="bn_ajouter" onclick="document.getElementById(\'div_ajout_donnee\').style.display=\'none\'">&nbsp;<input type="submit" name="ajouter" value="'.$t_newData_table_add.'" class="bn_ajouter">'; ?></td></tr>
					</table>
			  </td>
			</tr>
			<tr> 
			  <td align="left" class="TX">&nbsp;</td>
			</tr>
		  </table>
		</td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
	  </tr>
	</table>
	<br>
	</div>
<script language="Javascript">
	var ancre=window.location.hash;
	if (ancre=='#add_data')
	{
	document.getElementById('div_ajout_donnee').style.display='block';
	}
</script>
<!-- 	  <?php
	 // if($proprietaire==1){
		?>
		<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
		  <tr> 
			<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			<td height="14"></td>
			<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		  </tr>
		  <tr> 
			<td width="14"></td>
			<td class="TX_Requetes" align="left">RESTRICTIONS D'ACCES</td>
			<td width="14"></td>
		  </tr>
		  <tr> 
			<td width="14" height="1"></td>
			<td class="TX" align="center" bgcolor="#666666" height="1"></td>
			<td width="14" height="1"></td>
		  </tr>
		  <tr>
		  <td colspan="3">&nbsp;</td>
		  </tr>
	  		<?php
			/*  unset($checked_1);
			  unset($checked_2);
			  if($infos_req[0]['indic_cert']==1){
				$checked_1 = ' checked="checked"';
			  }
			  if($infos_req[0]['indic_adm']==1){
				$checked_2 = ' checked="checked"';
			  }*/
			  ?>
			<tr>
			  <td width="14"></td>
			  <td class="TX">
				  <input type="checkbox" value="1" name="che_droit_cert"<?php echo $checked_1?>/>&nbsp;Accès&nbsp;aux&nbsp;certifiés<br>
				  <input type="checkbox" value="1" name="che_droit_adm"<?php echo $checked_2?>/>&nbsp;Accès&nbsp;aux&nbsp;administrateurs
			  <input type="hidden" name="proprio" value="<?php echo $proprietaire ?>" />
			  </td>
			  <td width="14"></td>
			</tr>
		</td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
	  </tr>
	</table>
		<?php
	  //}
	  ?>-->
</form>
	<p>&nbsp;</p>
	<?php
}

if($step>1){
?>
	<a name="3"></a>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	  <tr> 
		<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
	  </tr>
	  <tr> 
		<td width="14"></td>
		<td class="TX_Requetes" align="left"><?php echo $t_filter ?></td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="1"></td>
		<td bgcolor="#666666" height="1"></td>
		<td width="14" height="1"></td>
	  </tr>
	  <tr><td colspan="3">&nbsp;</td></tr>
	  <tr> 
		<td width="14"></td>
		<td class="TX">
		  <table style="width:800px; border:0"  cellspacing="0" cellpadding="0" >
			<tr> 
			  <td height="14">
			  	<table class="tabloGeneral">
				  <tr> 
					<td class="TX_bold"><?php echo $t_filter_table_col1 ?></td>
					<td class="TX_bold" align="center"><?php echo $t_filter_table_col2 ?></td>
					<td class="TX_bold" align="center"><?php echo $t_filter_table_col3 ?></td>
					<?php
					if($proprietaire==1){
						?>
						<td class="TX_bold" align="center"><?php echo $t_filter_table_col4 ?></td>
						<td class="TX_bold" align="center"><?php echo $t_filter_table_col5 ?></td>
						<?php
					}
					?>
				  </tr>
				  <?php
				  echo $tableau_filtres;
				  ?>
				</table>
			  </td>
			</tr>
			</table>
			</td>
			<td width="14"></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td><table style="width:800px; border:0">
		      <tbody>
		        <tr>
		          <td style="text-align:right"> <?php if($champs_req != '' && $proprietaire==1) echo '<input type="button" class="bn_ajouter" onclick="document.getElementById(\'newFilters\').style.display=\'block\';window.location=window.location.pathname + window.location.search+\'#lesfiltres\';" value="'.$t_filter_table_add.'">'; ?></td>
		          </tr>
		        </tbody>
		      </table></td>
		    <td>&nbsp;</td>
		    <tr> 
			<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			<td height="14"></td>
			<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		  </tr>
		</table>
		<br>
			<?php
			if($proprietaire==1){
				?>
				<a name="lesfiltres"></a>
			<table id="newFilters" width="800" border="0" cellspacing="0" cellpadding="0"  <?php if($valeur_a_modifier != '') echo 'style="display: block;"';?>>
			  <tr> 
				<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				<td height="14"></td>
				<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			  </tr>
			  <tr> 
				<td width="14"></td>
				<td class="TX_Requetes" align="left"><?php echo $t_newFilter ?></td>
				<td width="14"></td>
			  </tr>
			  <tr> 
				<td width="14" height="1"></td>
				<td bgcolor="#666666" height="1"></td>
				<td width="14" height="1"></td>
			  </tr>
			  <tr><td colspan="3">&nbsp;</td></tr>
			  <tr> 
				<td width="14"></td>
				<td align="center" class="TX"> 
				<form action="#" method="post" name="form3">
					<table>
					<tr> 
					  <td align="left" class="TX_bold"><?php echo $t_newFilter_table_col1 ?></td>
					  <td align="left" class="TX_bold"><?php echo $t_newFilter_table_col2 ?></td>
					  <td align="left" class="TX_bold"><?php echo $t_newFilter_table_col3 ?></td>
					  <td align="left" class="TX">&nbsp;</td>
					  <td align="left" class="TX">&nbsp;</td>
					</tr>
					<tr> 
					  <td align="left" class="TX"> 
						<select name="critere" class="form_ediht_Requetes">
							<?php
							echo $options_req;
							?>
						</select>
					  </td>
					  <td align="center" class="TX"> 
						<select name="operateur" class="form_ediht_Requetes">
							<?php
							foreach($tab_operateurs as $key=>$value){
								unset($selected_a_valider);
								if($operateur_a_modifier == $key){
									$selected_a_valider = ' selected="selected"';
								}
								echo '<option value="'.$key.'"'.$selected_a_valider.'>'.$value.'</option>';
							}
							?>
						</select>
					  </td>
					  <td align="left" class="TX"> 
						<input type="text" name="valeur" class="form_ediht_Requetes" size="30" value="<?php echo htmlentities($valeur_a_modifier) ?>">
					  </td>
					  <td align="left" class="TX" valign="middle"> 
					  </td>
					  <td align="center" class="TX"> 
							<?php
							/* if($valeur_a_modifier != ''){
								echo '<input type="submit" name="edit_filtre" value="Edit" class="bn_ajouter">';
							}else{
								echo '<input type="submit" name="ajout_filtre" value="ok" class="bn_ajouter">';
							} */
							?>
							
					  </td>
					</tr>
				<tr> 
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="center" class="TX">&nbsp;</td>
				  <td align="right" class="TX">
				  <?php
					if($valeur_a_modifier != ''){
						echo '<input type="button" value="'.$t_newFilter_table_cancel.'" class="bn_ajouter" onclick="window.location = \'requetes.php?req_id='.intval($_GET['req_id']).'&type='.intval($_GET['type']).'&del=0#3\';">';
						echo '<input type="submit" name="edit_filtre" value="'.$t_newFilter_table_mod.'" class="bn_ajouter">';
					}else{
						echo '<input type="button" onclick="document.getElementById(\'newFilters\').style.display=\'none\'" class="bn_ajouter" value="'.$t_newFilter_table_cancel.'">&nbsp;<input type="submit" name="ajout_filtre" value="'.$t_newFilter_table_add.'" class="bn_ajouter">';
	
					}
				?>
				  </td>
				  <td align="left" class="TX" valign="middle">&nbsp;</td>
				  <td align="center" class="TX">&nbsp;</td>
				</tr>
				<?php
			}
			?>
			</form>
			<tr> 
			  <td align="left" class="TX" colspan="5"> 

			  </td>
			</tr>
		  </table>
		</td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
	  </tr>
	</table>
	<script language="Javascript">
			var ancre=window.location.hash;
			if (ancre=='#lesfiltres')
			{
			document.getElementById('newFilters').style.display='block';
			}
		</script>
	<p>&nbsp;</p>
	<?php
}
?>
<?php
if($step>1){
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	  <tr> 
		<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
	  </tr>
		<tr> 
		<td width="14"></td>
		<td align="center" class="TX">
		<input type="button" name="ap" value="VISUALISER" class="bn_valider_requete" onClick="MM_openBrWindow('requetes_apercu.php?req_id=<?php echo $_GET['req_id'] ?>','')">
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  <input type="button" name="export" value="EXPORTER" class="bn_valider_requete" onClick="window.location='requetes_apercu.php?req_id=<?php echo $_GET['req_id'] ?>&export=1'"> <!--onClick="MM_goToURL('parent','#');return document.MM_returnValue">-->
		<?php
		if($proprietaire==1){
		?>
		  	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" name="valide2" value="<?php echo $t_valid2 ?>" onClick="document.form2.submit();" class="bn_valider_requete">
		<?php
		}
		?>
		  
		</td>
		<td width="14"></td>
	  </tr>
	  <tr> 
		<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		<td height="14"></td>
		<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
	  </tr>
	</table>
<?php } ?>
<p>&nbsp;</p>
</p></div>	</article></div>	</div>	</div>	</div>
</body>
</html>
<?php
}else{
	include('no_acces.php');
}
?>