<?php
session_start();
if ($_SESSION['droit']!=''){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	
        include ("../Artichow/LinePlot.class.php");
        include ("../Artichow/ScatterPlot.class.php");
	
	$db = new db($conn);
	
	function rgb2hex($rgb){ 
		 if(!is_array($rgb)) { 
			  echo "Error : input must be an array"; 
			  return 0; 
		  } 
		  
		  $hex = ""; 
		 for($i=0; $i<3; $i++) { 
			  if( ($rgb[$i] > 255) || ($rgb[$i] < 0) ) { 
			  echo "Error : input must be between 0 and 255"; 
			  return 0; 
		  } 
		  $tmp = dechex($rgb[$i]); 
		  if(strlen($tmp) < 2) $hex .= "0". $tmp; 
			else $hex .= $tmp; 
		  } 
		  
		  return $hex; 
	} 	
	if ($_GET['candid'] && $_GET['opeid'] && $_GET['dossid']){
		$sql_infos_prod_doss = "SELECT * FROM PRODUIT_A_DOSSIER,OPERATION WHERE PRODUIT_A_DOSSIER.PROD_ID=OPERATION.PROD_ID AND OPERATION.OPE_ID='".txt_db(intval($_GET['opeid']))."' and DOSSIER_ID='".txt_db(intval($_GET['dossid']))."'";
		$qry_infos_prod_doss = $db->query($sql_infos_prod_doss);
	//echo $sql_infos_prod_doss;
		/* Si le candidat est sélectionné */
		$sql_info_candidat = "select CAND_NOM,CAND_PRENOM,CAND_SEXE,CLI_NOM,CAND_LANG_ID
								from candidat,client
								where candidat.cand_cli_id=client.cli_id(+)
								and cand_id='".txt_db(intval($_GET['candid']))."'";
		$qry_info_candidat = $db->query($sql_info_candidat);
	//echo $sql_info_candidat.'<br>';	
		/* On sélectionne les informations sur le dossier pour le candidat et l'opération sélectionnés */	
		$sql_infos_doss = "SELECT DATE_FIN, DOSSIER_ID,CAND_ID,OPE_ID,PROD_NOM,TITRE,DOC_ID,DOC_NOM,TYPE_GABARIT, TRI FROM CAND_DOC 
			WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' 
				AND OPE_ID='".txt_db(intval($_GET['opeid']))."' 
				AND DOSSIER_ID='".txt_db(intval($_GET['dossid']))."' 
			GROUP BY DATE_FIN, DOSSIER_ID,CAND_ID,OPE_ID,PROD_NOM,TITRE,DOC_ID,DOC_NOM,TYPE_GABARIT, TRI ORDER BY DOSSIER_ID,TRI";
		$qry_infos_doss = $db->query($sql_infos_doss);
//echo $sql_infos_doss.'<br>';		

		$sql_cert_info = "SELECT CERTIFIE.* FROM CERTIFIE, CANDIDAT WHERE CANDIDAT.CAND_ID='".$qry_infos_doss[0]['cand_id']."' AND CANDIDAT.CAND_CERT_ID=CERTIFIE.CERT_ID";
		$qry_cert_info = $db->query($sql_cert_info);

		$deb_page_html = '
<html>
	<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="http://www.vakom.ornis.net/pre-prod/css/nvo.css" type="text/css">
		<link rel="stylesheet" href="http://www.vakom.ornis.net/pre-prod/css/general.css" type="text/css">
		<link rel="stylesheet" href="http://www.vakom.ornis.net/pre-prod/css/import.css" type="text/css">
	</head>
	<body bgcolor="#FFFFFF" text="#000000" width="860">
	';
	$mid_page_html = '
	<center>
		<table width="890" style="page-break-before:always" align="center" height="1100">
		<tr>
			<td valign="top" height="1280" width="886" style="background-image: url(\'http://www.vakom.ornis.net/pre-prod/images/fond_page_garde.jpg\'); background-position: bottom center; background-repeat: no-repeat;">
			<table width="861" height="1000">
				<tr><td width="857"><h1 class="titre_doss">'.$qry_infos_doss[0]['titre'].'</h1><br><br><h2 class="stitre_doss">
					de '.$qry_info_candidat[0]['cand_prenom'].' '.$qry_info_candidat[0]['cand_nom'].'</h2><h2 class="stitre_doss2">'.$qry_info_candidat[0]['cli_nom'].'</h2>
					<br />';
					if ($qry_infos_prod_doss[0]['operation']==1){
						$sql_info_ope = "SELECT TYPE_OPE_LIBELLE FROM TYPE_OPERATION WHERE TYPE_OPE_ID=".intval($_GET['opeid'])."";
						$qry_info_ope = $db->query($sql_info_ope);
						$mid_page_html.= '<span class="bold_garde">Opération : </span>'.$qry_info_ope[0]['type_ope_libelle'].'<br>';
					}
					$mid_page_html.= '
					<span class="bold_garde">
					Fait le : </span>'.date('d/m/Y').'<br><span class="bold_garde">
					Temps de passation : </span><br>
				</td></tr>
			</table>
			<table width="861" height="250" ><tr>
			<td width="350">&nbsp;</td>
			<td style="color:white;">
			VAKOM<br>
			38 rue Bouquet - 76108 Rouen Cedex 1<br>
			Tél. : 02 32 10 59 20 - Fax : 02 32 10 59 21<br>
			<span class="jaune">http://www.vakom.fr</span>   - accueil@vakom.fr<br>
			RCS Rouen 438 715 252
			</td></tr>
			</table>
			</td>
		</tr>
	';
$fin_page_html = '	
		</table>
	</center>
	</body>
</html>
';
$chemin 		= "./temp/";
$num_page = 1;
// On ouvre un nouveau fichier
$file 			= $num_page.".html";
$fp = fopen($chemin . $file ,"w+");
fputs($fp,$deb_page_html.$mid_page_html.$fin_page_html);
fclose($fp);
flush();
$commandline= '/usr/local/bin/wkhtmltopdf -O Portrait -s A4';
if ($qry_infos_prod_doss[0]['page_garde'] == 1){
$commandline .= ' '.$chemin . $file;
}
/*
		echo '<html>
		<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<link rel="stylesheet" href="../css/import.css" type="text/css">
		</head>
		<body bgcolor="#FFFFFF" text="#000000" width="860">
		<center><table><tr><td valign="top" height="1200" width="860" style="background-image: url(\'http://www.vakom.ornis.net/pre-prod/images/fond_page_garde.jpg\'); background-position: bottom center; background-repeat: no-repeat;">';
		echo '<table width="720">
				<tr><td width="720">';
		echo '<h1 class="titre_doss">'.$qry_infos_doss[0]['titre'].'</h1><br><br>';
		echo '<h2 class="stitre_doss">de '.$qry_info_candidat[0]['cand_prenom'].' '.$qry_info_candidat[0]['cand_nom'].'</h2>';
		echo '<h2 class="stitre_doss2">'.$qry_info_candidat[0]['cli_nom'].'</h3>';
		echo '<span class="bold_garde">Fait le : </span>'.date('d/m/Y').'<br>';
		echo '<span class="bold_garde">Temps de passation : </span><br>';
		if($qry_cert_info[0]['cert_nom']!=''){
			echo '<span class="bold_garde">Certifié OPR&reg; : </span>'.$qry_cert_info[0]['cert_prenom'].' '.$qry_cert_info[0]['cert_nom'];
		}
		echo '
		<div class="infos_garde">
		VAKOM<br>
		38 rue Bouquet - 76108 Rouen Cedex 1<br>
		Tél. : 02 32 10 59 20 - Fax : 02 32 10 59 21<br>
		<span class="jaune">http://www.vakom.fr</span>   -   accueil@vakom.fr<br>
		RCS Rouen 438 715 252</div>
		</td></tr>
	</table>
</td></tr>
		</table></center>';*/
		if (is_array($qry_infos_doss)) {
			/* Pour chaque document du dossier */
			/*			
			echo '<center><table style="page-break-before:always" align="center" width="810">
			<tr><td style="text-align: left;" valign="top" height="1150" width="810"><span class="titre1Car0"><span style="font-size: 28pt;">SOMMAIRE</span></span><br>';
			*/
			$mid_page_html = '<center><table style="page-break-before:always" align="center" width="810">
			<tr><td style="text-align: left;" valign="top" height="1030" width="810"><span class="titre1Car0"><span style="font-size: 28pt;">SOMMAIRE</span></span><br>';
			$mid_page_html .= '<table border="0" width="100%" id="table1">';
			$nb_page_som = 1;
			foreach($qry_infos_doss as $infos_document){
				$nu++;
				$nb_page_som++;
				//echo '<a href="#'.$nu.'" class="titre3car0" style="font-size: 18pt;">'.$infos_document['doc_nom'].'</a><br>';
				$mid_page_html .= '<tr>
				<td><a href="#" class="titre3car0" style="font-size: 18pt;">'.$infos_document['doc_nom'].'</a></td>
				<td style="font-size: 18pt;" class="titre3car0" width="56">'.$nb_page_som.'</td>
				</tr>';
			}
			$mid_page_html .= '<table>';
			//echo '</td></tr></table></center>';
		}
		$mid_page_html .= '</td></tr>';
		// On ouvre un nouveau fichier
		$num_page = $num_page+1;
		$file 			= $num_page.".html";
		$fp = fopen($chemin . $file ,"w+");
		fputs($fp,$deb_page_html.$mid_page_html.$fin_page_html);
		fclose($fp);				
		if ($qry_infos_prod_doss[0]['sommaire'] == 1){		
			$commandline .= ' '.$chemin . $file;
		}
		
		if (is_array($qry_infos_doss)) {
		/* Pour chaque document du dossier */
		foreach($qry_infos_doss as $infos_document){
		$j++;
		//echo '<a name="'.$j.'"></a><center><table width="810" style="page-break-before:always" align="center"><tr><td height="1100" valign="top" width="810" align="center">';
			/* On regarde le type de gabarit pour ce document, on définit dans quelle(s) zone(s) on doit chercher */
			switch ($infos_document['type_gabarit']){
				case 1:
				default:
					$nb_zones = 1;
				break;
				case 2:
					$nb_zones = 2;
				break;
				case 3:
				case 4:
					$nb_zones = 3;
				break;
				case 5:
					$nb_zones = 4;
				break;
			}
			
			/* On recherche dans les zones du document */
			for ($zone_id=1;$zone_id<=$nb_zones;$zone_id++){
				$sql_infos_zones = "SELECT CAND_DOC.* FROM CAND_DOC 
				WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' 
					AND OPE_ID='".txt_db(intval($_GET['opeid']))."' 
					AND DOSSIER_ID='".txt_db(intval($_GET['dossid']))."' 
					AND DOC_ID='".txt_db(intval($infos_document['doc_id']))."' 
					AND NUM_ZONE='".$zone_id."' 
				ORDER BY DOSSIER_ID,TRI";
//echo $sql_infos_zones.'<br>';	
				$infos_zones = $db->query($sql_infos_zones);
				switch($infos_zones[0]['type_zone'.$zone_id]){
					case 1:
					
						$align='center';
						
						$sql_select_graphe = "SELECT * FROM GRAPHE_A_QUEST WHERE GRAPHE_ID='".txt_db(intval($infos_zones[0]['doc_graphe_id']))."'";
						//echo $sql_select_graphe.'<br>';
						$qry_select_graphe = $db->query($sql_select_graphe);
					   					
						$sql_select_detail_graphe = "SELECT DOC_GRAPHE_MULTI FROM DOC_A_INFO WHERE DOC_GRAPHE_ID='".txt_db(intval($infos_zones[0]['doc_graphe_id']))."'";
						$qry_select_detail_graphe = $db->query($sql_select_detail_graphe);
						
						/* Si ce n'est pas un graphe multi utilisateur */
						if(intval($qry_select_detail_graphe[0]['doc_graphe_multi'])<1){
							
							// Les valeurs à afficher sur la courbe
							$sql_select_graphe_points = "SELECT TO_CHAR(COR_VAR_POINTS) RES FROM GRAPHE_PN WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['opeid']))."' AND TYPE='".$qry_select_graphe[0]['type_profil_code_id']."' ORDER BY ID";
							$qry_select_graphe_points = $db->query($sql_select_graphe_points);
							
							if ($qry_select_graphe[0]['type_profil_code_id'] == "29") { //PI
								$col1 = new Color(58, 170, 220);
							}
							elseif ($qry_select_graphe[0]['type_profil_code_id'] == "30") { //PN
								$col1 = new Color(218, 95, 155);
							}
							elseif ($qry_select_graphe[0]['type_profil_code_id'] == "32") { //PNPI (PR ??)
								$col1 = new Color(122, 176, 49);
							}
							if (is_array($qry_select_graphe_points)) {
								$values = array(str_replace(',','.',$qry_select_graphe_points[0]['res']),str_replace(',','.',$qry_select_graphe_points[1]['res']),str_replace(',','.',$qry_select_graphe_points[2]['res']),str_replace(',','.',$qry_select_graphe_points[3]['res']));							
							}
							else{
								$values = array(0,0,0,0);
							}
							//echo str_replace(',','.',$qry_select_graphe_points[0]['res']).' '.str_replace(',','.',$qry_select_graphe_points[1]['res']).' '.str_replace(',','.',$qry_select_graphe_points[2]['res']).' '.str_replace(',','.',$qry_select_graphe_points[3]['res']);
							
							// Taille du graphe par défaut
							$width_grille	= 635;
							$height_grille	= 622;
							
							$sql_images = "SELECT TITRE, TOP_GRAPHE, MID_GRAPHE, BOTTOM_GRAPHE, AFFICHE_NOM FROM GRAPHE_A_TEXTE WHERE LANG_ID='".$qry_info_candidat[0]['cand_lang_id']."' AND GRAPHE_ID='".intval($qry_select_graphe[0]['graphe_id'])."'";
							//echo $sql_images;
							$qry_images = $db->query($sql_images);
							
							###############################
							###############################
								// Définition de la taille de la grille si la grille est enregistrée sur le serveur
								if ($qry_images[0]['mid_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['mid_graphe'])){
									$grille_exists 	= true;
									$grille_link	= 'http://www.vakom.ornis.net/pre-prod/graphes/'.$qry_images[0]['mid_graphe'];
									list($width_grille, $height_grille) = getimagesize($grille_link);
								}else{
									$grille_exists = false;
									$grille_link	= 'http://www.vakom.ornis.net/pre-prod/graphes/mid_graphes0.jpg';
								}
								
								// Définition de la taille du top si il est enregistré sur le serveur
								if ($qry_images[0]['top_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['top_graphe'])){
									$top_exists = true;
									$top_link   = 'http://www.vakom.ornis.net/pre-prod/graphes/'.$qry_images[0]['top_graphe'];
									list($width_top, $height_top) = getimagesize($top_link);
								}else{
									$top_exists = false;
								}
								// Définition de la taille du bottom si il est enregistré sur le serveur
								if ($qry_images[0]['bottom_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['bottom_graphe'])){
									$bottom_exists = true;
									$bottom_link   = 'http://www.vakom.ornis.net/pre-prod/graphes/'.$qry_images[0]['bottom_graphe'];
									list($width_bottom, $height_bottom) = getimagesize($bottom_link);
								}else{
									$bottom_exists = false;
								}
							
							
							###############################
							###############################
							
							// Ici, le graphique mesurera X x Y pixels.
							$graph = new Graph($width_grille, $height_grille);
						   
							// L'anti-aliasing permet d'afficher des courbes plus naturelles,
							// mais cette option consomme beaucoup de ressources sur le serveur.
							$graph->setAntiAliasing(TRUE);
							
							// Masquer la bordure
							$graph->border->hide();

							$col = new Color(210, 200, 34,100);
							$graph->setFormat(2); // format PNG pour gerer la transparence
							$graph->setBackgroundColor($col);
							
							$group = new PlotGroup();
							
							$group->setPadding($width_grille/8, $width_grille/8, 0, 0);
							$group->setBackgroundColor($col);
							$group->setYMin(0);
							$group->setYMax(29);
							//$group->yAxis->setLabelPrecision(1);
							$group->grid->hide();
							$group->axis->bottom->hide();
							$group->axis->left->hide();
							
							//echo $qry_select_graphe_points[0]['res'].','.$qry_select_graphe_points[1]['res'].','.$qry_select_graphe_points[2]['res'].','.$qry_select_graphe_points[3]['res'];
							//$values = array(21,24.5,26.5,28);
							// On créé la courbe
							
							$plot = new LinePlot($values);
							
							//$plot->setType(LINE_DASHED);
							
							if($qry_select_graphe[0]['style']==0){
								//0 Pointillé
								$plot->setStyle(LINE_DASHED);
							}else{
								$plot->setStyle(LINE_SOLID);
							}
							
							$plot->mark->setType(MARK_CIRCLE);
							$plot->mark->setFill($col1);
							$plot->mark->setSize(6);

							//$plot->setYAxis(0,29);
							$plot->yAxis->setLabelPrecision(1);
							$plot->grid->hide();
							$plot->yAxis->hide();
							$plot->xAxis->hide();
							$plot->xAxis->hideTicks();
							$plot->xAxis->setLabelInterval(1);
							
							$plot->setColor($col1);

							// On ne change pas l'espace du haut et du bas de la courbe.
							   $plot->setSpace(
								  -10, // Gauche
								  -5, // Droite
								  0, // Haut 
								  4 // Bas
							   );
							   
							$group->add($plot);	
							/*
							$x = array(0, 1, 2, 3);

							$sc_plot = new ScatterPlot($values, $x);
							
							$sc_plot->mark->setType(MARK_CIRCLE);
							$sc_plot->mark->setFill($col1);
							$sc_plot->mark->setSize(6);

							//$sc_plot->setYAxis(0,29);
							$sc_plot->yAxis->setLabelPrecision(1);
							$sc_plot->grid->hide();
							$sc_plot->yAxis->hide();
							$sc_plot->xAxis->hide();
							$sc_plot->xAxis->hideTicks();
							$sc_plot->xAxis->setLabelInterval(1);
							
							$sc_plot->setColor($col1);

							// On ne change pas l'espace du haut et du bas de la courbe.
							   $sc_plot->setSpace(
								  -10, // Gauche 
								  -5, // Droite 
								  0, // Haut 
								  4 // Bas 
							   );
							   
							$group->add($sc_plot);
							*/
							// 2 COURBES ??
							if(is_array($qry_select_graphe[1])){
							
								// Les valeurs à afficher sur la courbe
								if ($qry_select_graphe[1]['type_profil_code_id'] == "29") { //PI
									$sql_select_graphe_points = "SELECT TO_CHAR(PI) RES FROM GRAPHE_PNPI WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['opeid']))."' ORDER BY ID";
									$qry_select_graphe_points = $db->query($sql_select_graphe_points);
									$col2 = new Color(58, 170, 220);
								}
								elseif ($qry_select_graphe[1]['type_profil_code_id'] == "30") { //PN
									$sql_select_graphe_points = "SELECT TO_CHAR(PN) RES FROM GRAPHE_PNPI WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['opeid']))."' ORDER BY ID";
									$qry_select_graphe_points = $db->query($sql_select_graphe_points);
									$col2 = new Color(218, 95, 155);
								}
								elseif ($qry_select_graphe[1]['type_profil_code_id'] == "32") { //PNPI (PR ??)
									$sql_select_graphe_points = "SELECT TO_CHAR(PI-PN) RES FROM GRAPHE_PNPI WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['opeid']))."' ORDER BY ID";
									$qry_select_graphe_points = $db->query($sql_select_graphe_points);
									$col2 = new Color(122, 176, 49);
								}
								if (is_array($qry_select_graphe_points)) {
									$values2 = array(str_replace(',','.',$qry_select_graphe_points[0]['res']),str_replace(',','.',$qry_select_graphe_points[1]['res']),str_replace(',','.',$qry_select_graphe_points[2]['res']),str_replace(',','.',$qry_select_graphe_points[3]['res']));							
								}
								else{
									$values2 = array(0,0,0,0);
								}
								
								//echo '<br>2>>>'.str_replace(',','.',$qry_select_graphe_points[0]['res']).' '.str_replace(',','.',$qry_select_graphe_points[1]['res']).' '.str_replace(',','.',$qry_select_graphe_points[2]['res']).' '.str_replace(',','.',$qry_select_graphe_points[3]['res']);
												
								$plot2 = new LinePlot($values2);
								
								//var_dump($values2);
								
								if($qry_select_graphe[1]['style']==0){
									//0 Pointillé
									$plot2->setStyle(LINE_DASHED);						
								}else{
									$plot2->setStyle(LINE_SOLID);
								}
							
								$plot2->mark->setType(MARK_CIRCLE);
								$plot2->mark->setFill($col2);
								$plot2->mark->setSize(6);
								
								//$plot2->setYAxis(0,29);
								$plot2->yAxis->setLabelPrecision(1);
								$plot2->grid->hide();
								$plot2->yAxis->hide();
								$plot2->xAxis->hide();
								$plot2->xAxis->hideTicks();
								$plot2->xAxis->setLabelInterval(1);
								
								$plot2->setColor($col2);

								// On ne change pas l'espace du haut et du bas de la courbe.
								   $plot2->setSpace(
									  -10, /* Gauche */
									  -5, /* Droite */
									  0, /* Haut */
									  4 /* Bas */
								   );

								$group->add($plot2);
							}
							
						}else{
							/* Si le graphe est mutli utilisateur */
							###############################
							###############################
							
							$sql_select_list_cand = "SELECT CAND_A_OPE.CAND_ID, CAND_NOM, CAND_PRENOM FROM CAND_A_OPE, CANDIDAT WHERE CANDIDAT.CAND_ID=CAND_A_OPE.CAND_ID AND OPE_ID=".intval($_GET['opeid'])."";
							$qry_select_list_cand = $db->query($sql_select_list_cand);
							
							
							// Taille du graphe par défaut
							$width_grille	= 635;
							$height_grille	= 622;
							
							$sql_images = "SELECT TITRE, TOP_GRAPHE, MID_GRAPHE, BOTTOM_GRAPHE, AFFICHE_NOM FROM GRAPHE_A_TEXTE WHERE LANG_ID='".$qry_info_candidat[0]['cand_lang_id']."' AND GRAPHE_ID='".intval($qry_select_graphe[0]['graphe_id'])."'";
							//echo $sql_images;
							$qry_images = $db->query($sql_images);
							
							###############################
							###############################
								// Définition de la taille de la grille si la grille est enregistrée sur le serveur
								if ($qry_images[0]['mid_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['mid_graphe'])){
									$grille_exists 	= true;
									$grille_link	= 'http://www.vakom.ornis.net/pre-prod/graphes/'.$qry_images[0]['mid_graphe'];
									list($width_grille, $height_grille) = getimagesize($grille_link);
								}else{
									$grille_exists = false;
									$grille_link	= 'http://www.vakom.ornis.net/pre-prod/graphes/mid_graphes0.jpg';
								}
								
								// Définition de la taille du top si il est enregistré sur le serveur
								if ($qry_images[0]['top_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['top_graphe'])){
									$top_exists = true;
									$top_link   = 'http://www.vakom.ornis.net/pre-prod/graphes/'.$qry_images[0]['top_graphe'];
									list($width_top, $height_top) = getimagesize($top_link);
								}else{
									$top_exists = false;
								}
								// Définition de la taille du bottom si il est enregistré sur le serveur
								if ($qry_images[0]['bottom_graphe']!='' && file_exists('../graphes/'.$qry_images[0]['bottom_graphe'])){
									$bottom_exists = true;
									$bottom_link   = 'http://www.vakom.ornis.net/pre-prod/graphes/'.$qry_images[0]['bottom_graphe'];
									list($width_bottom, $height_bottom) = getimagesize($bottom_link);
								}else{
									$bottom_exists = false;
								}
							
							
							###############################
							###############################
							
							// Ici, le graphique mesurera X x Y pixels.
							$graph = new Graph($width_grille, $height_grille);
						   
							// L'anti-aliasing permet d'afficher des courbes plus naturelles,
							// mais cette option consomme beaucoup de ressources sur le serveur.
							$graph->setAntiAliasing(TRUE);
							
							// Masquer la bordure
							$graph->border->hide();

							$col = new Color(210, 200, 34,100);
							$graph->setFormat(2); // format PNG pour gerer la transparence
							$graph->setBackgroundColor($col);
							
							$group = new PlotGroup();
							
							$group->setPadding($width_grille/8, $width_grille/8, 0, 0);
							$group->setBackgroundColor($col);
							$group->setYMin(0);
							$group->setYMax(29);
							//$group->yAxis->setLabelPrecision(1);
							$group->grid->hide();
							$group->axis->bottom->hide();
							$group->axis->left->hide();
								
							$tab_color = array(
							0=>'255,50,50',
							1=>'50,50,255',
							2=>'50,255,50',
							3=>'255,50,200',
							4=>'255,255,0',
							5=>'150,0,255',
							6=>'0,0,200',
							7=>'0,255,255',
							8=>'200,0,50',
							9=>'255,50,0',
							10=>'50,0,200',
							11=>'200,0,0');
							
							$numcand=0;
							foreach($qry_select_list_cand as $list_cand){
							

								$setting_cand[$numcand]['cand_nom'] 	= $list_cand['cand_nom'];
								$setting_cand[$numcand]['cand_prenom'] 	= $list_cand['cand_prenom'];
								$setting_cand[$numcand]['cand_color'] 	= $tab_color[$numcand];
								//echo $setting_cand[$numcand]['cand_color'].'<br>';
								$stab_color = explode(',',$tab_color[$numcand]);
								$scol1	= $stab_color[0];
								$scol2	= $stab_color[1];
								$scol3	= $stab_color[2];
								$col1 = new Color($scol1,$scol2,$scol3);
								
								$numcand++;
								
								// Les valeurs à afficher sur la courbe
								$sql_select_graphe_points = "SELECT TO_CHAR(COR_VAR_POINTS) RES FROM GRAPHE_PN WHERE CAND_ID='".txt_db(intval($list_cand['cand_id']))."' AND OPE_ID='".txt_db(intval($_GET['opeid']))."' AND TYPE='".$qry_select_graphe[0]['type_profil_code_id']."' ORDER BY ID";
								//echo $sql_select_graphe_points;
								$qry_select_graphe_points = $db->query($sql_select_graphe_points);
								
								if (is_array($qry_select_graphe_points)) {
									$values = array(str_replace(',','.',$qry_select_graphe_points[0]['res']),str_replace(',','.',$qry_select_graphe_points[1]['res']),str_replace(',','.',$qry_select_graphe_points[2]['res']),str_replace(',','.',$qry_select_graphe_points[3]['res']));							
								}
								else{
									$values = array(0,0,0,0);
								}

								// On créé la courbe								
								$plot = new LinePlot($values);

								$plot->setStyle(LINE_SOLID);								
								
								$plot->mark->setType(MARK_CIRCLE);
								$plot->mark->setFill($col1);
								$plot->mark->setSize(6);

								//$plot->setYAxis(0,29);
								$plot->yAxis->setLabelPrecision(1);
								$plot->grid->hide();
								$plot->yAxis->hide();
								$plot->xAxis->hide();
								$plot->xAxis->hideTicks();
								$plot->xAxis->setLabelInterval(1);
								
								$plot->setColor($col1);

								// On ne change pas l'espace du haut et du bas de la courbe.
								   $plot->setSpace(
									  -10, // Gauche
									  -5, // Droite
									  0, // Haut 
									  4 // Bas
								   );
								   
								$group->add($plot);	
							}
						}
						
						$graph->add($group);
						
						$graph->setFormat(Image::PNG); 

						$graph->draw("../graphes/graphe.png");
						
						$picture = imagecreatefrompng("../graphes/graphe.png");
						$color = imagecolorexact ($picture,255,255,255);
						imagecolortransparent($picture, $color);
						imagepng($picture, "../graphes/graphe".$infos_document['doc_id']."_".$zone_id.".png"); 
						
						@unlink("../graphes/graphe.png");
						
						$contenu[$zone_id] = '<div style="display: inline; margin-bottom: 50px;">';
						
							if(intval($qry_select_detail_graphe[0]['doc_graphe_multi'])<1){
								/* Si on doit afficher le nom du candidat */
								if($qry_images[0]['affiche_nom']==1){
									$title_graphe = $qry_images[0]['titre'];
									$title_graphe = str_replace ( "[NOM]" , $qry_info_candidat[0]['cand_prenom'] , $title_graphe );
									$title_graphe = str_replace ( "[PRENOM]" , $qry_info_candidat[0]['cand_nom'] , $title_graphe );
//									$title_graphe = $qry_images[0]['titre'];
//									$ident_candidat = '&nbsp;'.$qry_info_candidat[0]['cand_prenom'].' '.$qry_info_candidat[0]['cand_nom'];
									$contenu[$zone_id] .= '<div style="width: '.$width_top.'px;">'.$title_graphe.'</div>';
								}
							}
						
							
						
							if($top_exists){
								$contenu[$zone_id] .= '<div style="overflow: hidden;"><img style="width: '.$width_top.'px;" src="'.$top_link.'" alt="" /></div>';
							}
							/*
							echo 'wg = '.$width_grille.'<br>';
							echo 'hg = '.$height_grille.'<br>';
							echo 'gl = '.$grille_link.'<br>';
							*/							
							$contenu[$zone_id] .='<div style="overflow: hidden; width: '.$width_grille.'px; height: '.$height_grille.'px; display: block; text-align: center; background-image: url('.$grille_link.');">
							<img style="width: '.$width_grille.'px;" src="http://www.vakom.ornis.net/pre-prod/graphes/graphe'.$infos_document['doc_id'].'_'.$zone_id.'.png" alt="" />
							</div>';
							
							if($bottom_exists){
								$contenu[$zone_id] .='<div style="overflow: hidden; display: block;">
								<img style="width: '.$width_bottom.'px;" src="'.$bottom_link.'" alt="" />
								</div>';
							}
							/* Si on doit afficher le nom du candidat */
							if(intval($qry_select_detail_graphe[0]['doc_graphe_multi'])==1){
								/* Si c'est un graphe multi */
								if($qry_images[0]['affiche_nom']==1){
									unset($ident_candidat2);
									$numcand=0;
									foreach($qry_select_list_cand as $list_cand){
										$ident_candidat2 .= '<div style="float: left;"><table style="display: inline"><tr><td height="5" width="5" style="line-height: 5px;" bgcolor="#'.rgb2hex(explode(',',$setting_cand[$numcand]['cand_color'])).'">&nbsp;</td><td>'.$setting_cand[$numcand]['cand_prenom'].' '.$setting_cand[$numcand]['cand_nom'].'&nbsp;&nbsp;</td></tr></table></div>';
										$numcand++;
									}
								}
								$contenu[$zone_id] .= '<div style="width: '.$width_top.'px;">'.$ident_candidat2.'<div style="clear: both;"></div></div>';
							}
							
						$contenu[$zone_id] .= '</div>';
						break;
					case 2:
						$align='left';
						
						$contenu[$zone_id] = $infos_zones[0]['doc_texte_fixe'];
					break;
					case 4:
						$align='center';
						$contenu[$zone_id] = '<img src="http://www.vakom.ornis.net/pre-prod/images/docs/'.$infos_zones[0]['doc_image_fixe'].'" alt="">';
					break;
					case 3:
					
						$align='left';
					
						$sql_select_txt_img = "SELECT * FROM TEXTE_IMAGE WHERE TXT_IMG_ID='".txt_db(intval($infos_zones[0]['doc_txt_img_id']))."'";
						//echo $sql_select_txt_img.'<br>';
						$qry_select_txt_img = $db->query($sql_select_txt_img);
						
						$tab_lettres = array(
							1=>'E',
							2=>'C',
							3=>'P',
							4=>'A');
						$tab_profils = array(
							29=>'PI',
							30=>'PN',
							32=>'PR');
						unset($combi);
						foreach($tab_lettres as $key => $value){
							/* boucle pour chaque lettre */
							switch($qry_select_txt_img[0]['txt_img_precision_code_id']){
								case 33:
									/* Par points */
									$vars = 'COR_VAR_POINTS';
								break;
								case 34:
									/* Par segment */
									$vars = 'COR_REGLE_SEGMENT';
								break;
								case 35:
									/* Par bloc */
									$vars = 'COR_REGLE_BLOC';
								break;
							}
							
							$sql_combi = "SELECT ".$vars." VAR_".$value." FROM CAND_".$tab_profils[$qry_select_txt_img[0]['txt_img_type_profil_code_id']]." WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['opeid']))."' AND REPONSE_".$tab_profils[$qry_select_txt_img[0]['txt_img_type_profil_code_id']]." = '".$value."'";
							//echo $sql_combi.'<br>';
							$qry_combi = $db->query($sql_combi);
							$combi[$key] = $qry_combi[0]['var_'.strtolower($value)];
							$valeur_combi .= $combi[$key];
							
							//echo $sql.'<br>';							
						}
						
						$sql_lib_textes = "select * from cand_comb
						where txt_img_id='".txt_db(intval($infos_zones[0]['doc_txt_img_id']))."'
						and COMBI_E_INF<='".$combi[1]."'
						and COMBI_C_INF<='".$combi[2]."'
						and COMBI_P_INF<='".$combi[3]."'
						and COMBI_A_INF<='".$combi[4]."'
						and COMBI_E_SUP>='".$combi[1]."'
						and COMBI_C_SUP>='".$combi[2]."'
						and COMBI_P_SUP>='".$combi[3]."'
						and COMBI_A_SUP>='".$combi[4]."'";
						$sql_lib_textes = $db->query($sql_lib_textes);
						/* SI HOMME */
						if($qry_info_candidat[0]['cand_sexe']!='F'){
							$sql_export_h = "select txt_homme from texte_a_langue_h where TXT_CODE_ID=".$sql_lib_textes[0]['txt_code_id'];
							$qry_export_h = $db->query($sql_export_h);
							$contenu[$zone_id] = $qry_export_h[0]['txt_homme'];
						}
						/* SI FEMMME */
						else{
							$sql_export_f = "select txt_femme from texte_a_langue_f where TXT_CODE_ID=".$sql_lib_textes[0]['txt_code_id'];
							$qry_export_f = $db->query($sql_export_f);
							$contenu[$zone_id] = $qry_export_f[0]['txt_femme'];
						}
					break;
					case 5:
					
						$align='center';
					
						$sql_select_txt_img = "SELECT * FROM TEXTE_IMAGE WHERE TXT_IMG_ID IN ('".txt_db(intval($infos_zones[0]['doc_img1_txt_img_id']))."','".txt_db(intval($infos_zones[0]['doc_img2_txt_img_id']))."')";
						//echo $sql_select_txt_img.'<br>';
						$qry_select_txt_img = $db->query($sql_select_txt_img);
						$tab_lettres = array(
							1=>'E',
							2=>'C',
							3=>'P',
							4=>'A');
						$tab_profils = array(
							29=>'PI',
							30=>'PN',
							32=>'PR');
						unset($combi);
						foreach($tab_lettres as $key => $value){
							/* boucle pour chaque lettre */
							switch($qry_select_txt_img[0]['txt_img_precision_code_id']){
								case 33:
									/* Par points */
									$vars = 'COR_VAR_POINTS';
								break;
								case 34:
									/* Par segment */
									$vars = 'COR_REGLE_SEGMENT';
								break;
								case 35:
									/* Par bloc */
									$vars = 'COR_REGLE_BLOC';
								break;
							}
							
							$sql_combi = "SELECT ".$vars." VAR_".$value." FROM CAND_".$tab_profils[$qry_select_txt_img[0]['txt_img_type_profil_code_id']]." WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['opeid']))."' AND REPONSE_".$tab_profils[$qry_select_txt_img[0]['txt_img_type_profil_code_id']]." = '".$value."'";
							//echo $sql_combi.'<br>';
							$qry_combi = $db->query($sql_combi);
							$combi[$key] = $qry_combi[0]['var_'.strtolower($value)];
							$valeur_combi .= $combi[$key];
							
							//echo $sql.'<br>';							
						}
						
						$sql_lib_textes = "select * from cand_comb,texte_image
						where cand_comb.txt_img_id=texte_image.txt_img_id
						and cand_comb.txt_img_quest_id=texte_image.txt_img_quest_id
						and cand_comb.TXT_IMG_TYPE_PROFIL_CODE_ID=texte_image.TXT_IMG_TYPE_PROFIL_CODE_ID
						and cand_comb.TXT_IMG_PRECISION_CODE_ID=texte_image.TXT_IMG_PRECISION_CODE_ID
						and texte_image.txt_img_id IN ('".txt_db(intval($infos_zones[0]['doc_img1_txt_img_id']))."','".txt_db(intval($infos_zones[0]['doc_img2_txt_img_id']))."')
						and COMBI_E_INF<='".$combi[1]."'
						and COMBI_C_INF<='".$combi[2]."'
						and COMBI_P_INF<='".$combi[3]."'
						and COMBI_A_INF<='".$combi[4]."'
						and COMBI_E_SUP>='".$combi[1]."'
						and COMBI_C_SUP>='".$combi[2]."'
						and COMBI_P_SUP>='".$combi[3]."'
						and COMBI_A_SUP>='".$combi[4]."'";
						//echo $sql_lib_textes;
						$sql_lib_textes = $db->query($sql_lib_textes);
						/* SI HOMME */
						if($qry_info_candidat[0]['cand_sexe']!='F'){
							$contenu[$zone_id] = '<img src="http://www.vakom.ornis.net/pre-prod/admin/images_txtimg/'.$sql_lib_textes[0]['img_homme'].'" alt="">';
						}
						/* SI FEMMME */
						else{
							$contenu[$zone_id] = '<img src="http://www.vakom.ornis.net/pre-prod/admin/images_txtimg/'.$sql_lib_textes[0]['img_femme'].'" alt="">';						
						}
					break;
					case 6:
						$align='left';
						/* Sélection du CR */
						$sql_sel_cr = "SELECT TXT_CR FROM CAND_A_CR WHERE OPE_ID=".intval($_GET['opeid'])." AND CAND_ID=".$_GET['candid']." AND DOC_ID=".$infos_document['doc_id']." AND NUM_ZONE=".$zone_id."";
						$qry_sel_cr = $db->query($sql_sel_cr);
						//echo $sql_sel_cr;
						$contenu[$zone_id] = $qry_sel_cr[0]['txt_cr'];
					break;
				}
			}
			
			switch($infos_document['type_gabarit']){
				case 1:
					/*
					echo '
					<table width="720" align="center" cellpadding="5" cellspacing="0" height="800">
						<tr><td width="720" valign="top" align="'.$align.'">'.$contenu[1].'</td></tr>
					</table>';
					*/
					$mid_page_html = '
					<center><table width="810" style="page-break-before:always" align="center"><tr><td height="1030" valign="top" width="810" align="center">
					<table width="720" align="center" cellpadding="5" cellspacing="0" height="800">
						<tr><td width="720" valign="top" align="'.$align.'">'.$contenu[1].'</td></tr>
					</table>';
				break;
				case 2:
					/*echo '
					<table width="720" align="center" cellpadding="5" cellspacing="0" height="800">
						<tr><td width="720" height="1" valign="top" align="'.$align.'">'.$contenu[1].'</td></tr>
						<tr><td width="720" valign="top" align="'.$align.'">'.$contenu[2].'</td></tr>
					</table>';
					*/
					$mid_page_html = '
					<center><table width="810" style="page-break-before:always" align="center"><tr><td height="1030" valign="top" width="810" align="center">
					<table width="720" align="center" cellpadding="5" cellspacing="0" height="800">
						<tr><td width="720" height="1" valign="top" align="'.$align.'">'.$contenu[1].'</td></tr>
						<tr><td width="720" valign="top" align="'.$align.'">'.$contenu[2].'</td></tr>
					</table>';
				break;
				case 3:
					/*
					echo '
					<table width="720" align="center" cellpadding="5" cellspacing="0" height="800">
						<tr><td width="360" height="1" valign="top" align="'.$align.'">'.$contenu[1].'</td>
							<td width="360" valign="top" align="'.$align.'">'.$contenu[2].'</td></tr>
						<tr><td colspan="2" valign="top" width="720" align="'.$align.'">'.$contenu[3].'</td></tr>
					</table>';
					*/
					$mid_page_html = '
					<center><table width="810" style="page-break-before:always" align="center"><tr><td height="1030" valign="top" width="810" align="center">
					<table width="720" align="center" cellpadding="5" cellspacing="0" height="800">
						<tr><td width="360" height="1" valign="top" align="left">'.$contenu[1].'</td>
							<td width="360" valign="top" align="right">'.$contenu[2].'</td></tr>
						<tr><td colspan="2" valign="top" width="720" align="center">'.$contenu[3].'</td></tr>
					</table>';
				break;
				case 4:
					/*
					echo '
					<table width="720" align="center" cellpadding="5" cellspacing="0" height="800">
						<tr><td width="720" height="1" valign="top" align="'.$align.'">'.$contenu[1].'</td></tr>
						<tr><td width="720" height="1" valign="top" align="'.$align.'">'.$contenu[2].'</td></tr>
						<tr><td width="720" valign="top" align="'.$align.'">'.$contenu[3].'</td></tr>
					</table>';
					*/
					$mid_page_html = '
					<center><table width="810" style="page-break-before:always" align="center"><tr><td height="1030" valign="top" width="810" align="center">
					<table width="720" align="center" cellpadding="5" cellspacing="0" height="800">
						<tr><td width="720" height="1" valign="top" align="'.$align.'">'.$contenu[1].'</td></tr>
						<tr><td width="720" height="1" valign="top" align="'.$align.'">'.$contenu[2].'</td></tr>
						<tr><td width="720" valign="top" align="'.$align.'">'.$contenu[3].'</td></tr>
					</table>';
				break;
				case 5:
					/*
					echo '
					<table border="0" width="720" align="center" cellpadding="5" cellspacing="0" height="800">
						<tr><td width="360" height="1" valign="top" align="'.$align.'">'.$contenu[1].'</td>
							<td rowspan="2" width="360" valign="top" align="'.$align.'">'.$contenu[3].'</td>
						</tr>
						<tr>
							<td width="360" height="1" valign="top" align="'.$align.'">'.$contenu[2].' </td>
						</tr>
						<tr><td width="720" valign="top" colspan="2" align="'.$align.'">'.$contenu[4].'</td></tr>
					</table>';
					*/
					$mid_page_html = '
					<center><table width="810" style="page-break-before:always" align="center"><tr><td height="1030" valign="top" width="810" align="center">
					<table border="0" width="720" align="center" cellpadding="5" cellspacing="0" height="800">
						<tr><td width="360" height="1" valign="top" align="'.$align.'">'.$contenu[1].'</td>
							<td rowspan="2" width="360" valign="top" align="'.$align.'">'.$contenu[3].'</td>
						</tr>
						<tr>
							<td width="360" height="1" valign="top" align="'.$align.'">'.$contenu[2].' </td>
						</tr>
						<tr><td width="720" valign="top" colspan="2" align="'.$align.'">'.$contenu[4].'</td></tr>
					</table>';
				break;
			}
			$mid_page_html .= '</td></tr>';
			if ($qry_infos_prod_doss[0]['pied_page'] == 1){
			$mid_page_html .= '
			<tr>
				<td height="80" align="center" style="background-image: url(\'http://www.vakom.ornis.net/pre-prod/images/pied_page.jpg\'); background-position: bottom center; background-repeat: no-repeat;">
					<table width="720">
					<tr><td height="1" width="250">&nbsp;</td><td style="text-align: left;font-weight: bold; color: #FFFFFF; ">'.$qry_infos_doss[0]['titre'].'<br>'.$qry_info_candidat[0]['cand_prenom'].' '.$qry_info_candidat[0]['cand_nom'].' - '.$qry_infos_doss[0]['date_fin'].'
					</td>
					<td width="50" height="1">&nbsp;</td><td style="text-align: left;font-weight: bold; color: #FFFFFF; ">Page '.$num_page.'</td>
					</tr>
					</table>
				</td>
			</tr>';
			}
			$num_page = $num_page+1;
			// On ouvre un nouveau fichier
			$file 			= $num_page.".html";
			$fp = fopen($chemin . $file ,"w+");
			fputs($fp,$deb_page_html.$mid_page_html.$fin_page_html);
			fclose($fp);					
			$commandline .= ' '.$chemin . $file;
			##############################################################
			#############   FIN DE LA BOUCLE DOCUMENT   ##################
			##############################################################
			/*
			echo '</td></tr>
			<tr>
				<td height="80" align="center" style="background-image: url(\'http://www.vakom.ornis.net/pre-prod/images/pied_page.jpg\'); background-position: bottom center; background-repeat: no-repeat;">
					<table width="720">
					<tr>
					<td height="1" width="250">&nbsp;</td><td style="text-align: left;font-weight: bold; color: #FFFFFF; ">'.$qry_infos_doss[0]['titre'].'<br>'.$qry_info_candidat[0]['cand_prenom'].' '.$qry_info_candidat[0]['cand_nom'].' / '.$qry_infos_doss[0]['date_fin'].'
					</td>
					</tr>
					</table>
				</td>
			</tr>
			</table></center><hr/>';
			*/
		}
	}

	}
	$fname		  	= intval($_GET['candid']).intval($_GET['opeid']).intval($_GET['dossid']);
	$ftype 			= "application/pdf";
	$commandline   .= ' '. $chemin . $fname . '.pdf';
	passthru($commandline);

	?>
	
	<meta http-equiv="refresh" content="0; URL=<?php echo $chemin . $fname . '.pdf' ?>">
	
	<?php
	//echo '<td> <a href="http://www.vakom.ornis.net/pre-prod/admin/temp/test.pdf" target="_blank">Version imprimable (PDF)</a></td>';

}else{
	include('no_acces.php');
}
?>
