<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
	if (is_uploaded_file($_FILES['doc']['tmp_name'])){

		$error = '';
		$ftmp  = $_FILES['doc']['tmp_name'];
		$fname = str_replace(' ','_', $_FILES['doc']['name']);	
		$fname = strtr($fname,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
		$fname = strtolower($fname);

		//extension
		$extension= strrchr($fname,'.');
		
		//lien des fichiers
		$lien  		= 'imports/'.$fname;
	
		// Le fichier est déplacé dans le dossier normal
		if (!move_uploaded_file($ftmp,$lien)){
			$error .= '<?php echo $t_copier_fichier_probleme  ?>';
		}
		?>
		<script type="text/javascript">
			window.close();
		</script>
		<?php
		
	}else{
		?>
		<html>
			<head>
				<title>Vakom - IMPORTS</title>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<link rel="stylesheet" href="../css/nvo.css" type="text/css">
				<link rel="stylesheet" href="../css/general.css" type="text/css">
			</head>
			<body bgcolor="#FFFFFF" text="#000000">

			  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr><td align="center" class="TX">&nbsp;</td></tr>
				<tr><td align="center" class="TX">&nbsp;</td></tr>
				<tr> 
				  <td align="center" class="TX"> 
					<form action="#" method="post" enctype="multipart/form-data">
						Importez&nbsp;votre&nbsp;fichier&nbsp; : <input type="file" name="doc" />&nbsp;
						<input type="submit" value="VALIDER" class="BN" />
					</form>
				  </td>
				</tr>
			  </table>

			</body>
		</html>
		<?php
	}
}