<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
		
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	$choix_zone = array(
	1 => 'Graphe',
	2 => 'Texte Fixe',
	3 => 'Texte Dynamique',
	4 => 'Image Fixe',
	5 => 'Images Dynamiques',
	6 => 'Compte-rendu',
	7 => 'Réponse');
		
	if ($_POST['step']=='2' && $_GET['docid']>0){
		switch ($_POST['gabarit']){
			case 1:
				$sql_zone = "TYPE_ZONE1='".$_POST['1-ZONE1']."', TYPE_ZONE2='', TYPE_ZONE3='', TYPE_ZONE4=''";
			break;
			case 2:
				$sql_zone = "TYPE_ZONE1='".$_POST['2-ZONE1']."', TYPE_ZONE2='".$_POST['2-ZONE2']."', TYPE_ZONE3='', TYPE_ZONE4=''";
			break;
			case 3:
				$sql_zone = "TYPE_ZONE1='".$_POST['3-ZONE1']."', TYPE_ZONE2='".$_POST['3-ZONE2']."', TYPE_ZONE3='".$_POST['3-ZONE3']."', TYPE_ZONE4=''";
			break;
			case 4:
				$sql_zone = "TYPE_ZONE1='".$_POST['4-ZONE1']."', TYPE_ZONE2='".$_POST['4-ZONE2']."', TYPE_ZONE3='".$_POST['4-ZONE3']."', TYPE_ZONE4=''";
			break;
			case 5:
				$sql_zone = "TYPE_ZONE1='".$_POST['5-ZONE1']."', TYPE_ZONE2='".$_POST['5-ZONE2']."', TYPE_ZONE3='".$_POST['5-ZONE3']."', TYPE_ZONE4='".$_POST['5-ZONE4']."'";
			break;
		}
		
		$sql_update = "UPDATE DOCUMENT SET TYPE_GABARIT='".intval($_POST['gabarit'])."', ".$sql_zone." WHERE DOC_ID='".txt_db(intval($_GET['docid']))."'";
		//echo $sql_update;
		$qry_update = $db->query($sql_update);
		header('location:gestion_docs_gabarit.php?docid='.$_GET['docid'].'&langid=1');
	}else{
		if ($_POST['choix']=='selection' && $_POST['select_doc']>0){
			header('location:gestion_docs.php?docid='.intval($_POST['select_doc']));
		}	
	}
	
	
		if ($_POST['choix']=='creation' && $_POST['gabarit']>0 && strlen(trim($_POST['doc_nom']))>0){
		
			switch ($_POST['gabarit']){
				case 1:
					$sql_zone = "'".$_POST['1-ZONE1']."', '', '', ''";
				break;
				case 2:
					$sql_zone = "'".$_POST['2-ZONE1']."', '".$_POST['2-ZONE2']."', '', ''";
				break;
				case 3:
					$sql_zone = "'".$_POST['3-ZONE1']."', '".$_POST['3-ZONE2']."', '".$_POST['3-ZONE3']."', ''";
				break;
				case 4:
					$sql_zone = "'".$_POST['4-ZONE1']."', '".$_POST['4-ZONE2']."', '".$_POST['4-ZONE3']."', ''";
				break;
				case 5:
					$sql_zone = "'".$_POST['5-ZONE1']."', '".$_POST['5-ZONE2']."', '".$_POST['5-ZONE3']."', '".$_POST['5-ZONE4']."'";
				break;
			}
			
			// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
			$sql_seq_num 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
			$qry_seq_num 		= $db->query($sql_seq_num);
			$seq_num			= intval($qry_seq_num[0]['seq_num']);
			
			$sql_insert = "INSERT INTO DOCUMENT VALUES('".$seq_num."', '".txt_db($_POST['doc_nom'])."', '".intval($_POST['gabarit'])."', ".$sql_zone.")";
			$qry_insert = $db->query($sql_insert);
			header('location:gestion_docs_gabarit.php?docid='.$seq_num);
		}
	
	/* Selection des différents documents déjà existants */
	$sql_list_doc = "SELECT DOC_ID, DOC_NOM FROM DOCUMENT ORDER BY DOC_NOM ASC";
	$qry_list_doc = $db->query($sql_list_doc);
	
	if ($_GET['docid']>0){
		/* Selection du document sélectionné */
		$sql_doc = "SELECT * FROM DOCUMENT WHERE DOC_ID='".txt_db(intval($_GET['docid']))."'";
		$qry_doc = $db->query($sql_doc);
		
		/* Si on a demandé de supprimer le graphe */
		if($_GET['action']=='delete_doc'){

			/* On fait un DELETE dans la table */
			$sql_del_doc  = "DELETE FROM DOCUMENT WHERE DOC_ID='".txt_db(intval($_GET['docid']))."'";
			$qry_del_doc  = $db->query($sql_del_doc);
			
			/* On redirige la personne sur la page d'accueil des docs */
			header('location:gestion_docs.php?docid='.intval($_GET['docid']));
		}
	}
	
		if ($_POST['choix']=='duplication'){
			if(trim($_POST['nvo_nom_doc'])!='' && $_POST['select_duplic']>0){
			
				/* Selection du document sélectionné */
				$sql_doc = "SELECT * FROM DOCUMENT WHERE DOC_ID='".txt_db(intval($_POST['select_duplic']))."'";
				$qry_doc = $db->query($sql_doc);
				
				/* Selection des détails du document sélectionné */
				$sql_doc_info = "SELECT * FROM DOC_A_INFO WHERE DOC_ID='".txt_db(intval($_POST['select_duplic']))."'";
				$qry_doc_info = $db->query($sql_doc_info);				

			
				// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
				$sql_seq_num 		= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
				$qry_seq_num 		= $db->query($sql_seq_num);
				$seq_num			= intval($qry_seq_num[0]['seq_num']);
				
				if(is_array($qry_doc_info)){
					foreach($qry_doc_info as $res_info){

						/* On insert les détails du document sélectionné pour le document dupliqué */
						$sql_ins_doc_info = "INSERT INTO DOC_A_INFO (DOC_ID,
							NUM_ZONE,
							DOC_GRAPHE_ID,
							DOC_GRAPHE_MULTI,
							DOC_TEXTE_FIXE,
							DOC_IMAGE_FIXE,
							DOC_TXT_IMG_ID,
							DOC_NOM_CR,
							DOC_IMG1_TXT_IMG_ID,
							DOC_IMG2_TXT_IMG_ID,
							DOC_IMG_MULTI,
							DOC_LANG_ID,
							DOC_TEXTE_FIXE_2,
							DOC_TEXTE_FIXE_3,
							DOC_TEXTE_FIXE_4,
							DOC_TEXTE_FIXE_5,
							DOC_TYPE_CR,
							DOC_TEXTE_FIXE_6,
							DOC_TEXTE_FIXE_7,
							DOC_TEXTE_FIXE_8) VALUES('".$seq_num."', 
							'".txt_db($res_info['num_zone'])."', 
							'".txt_db($res_info['doc_graphe_id'])."', 
							'".txt_db($res_info['doc_graphe_multi'])."', 
							'".txt_db($res_info['doc_texte_fixe'])."', 
							'".txt_db($res_info['doc_image_fixe'])."', 
							'".txt_db($res_info['doc_txt_img_id'])."', 
							'".txt_db($res_info['doc_nom_cr'])."', 
							'".txt_db($res_info['doc_img1_txt_img_id'])."', 
							'".txt_db($res_info['doc_img2_txt_img_id'])."', 
							'".txt_db($res_info['doc_img_multi'])."', 
							'".txt_db($res_info['doc_lang_id'])."',
							'".txt_db($res_info['doc_texte_fixe_2'])."',
							'".txt_db($res_info['doc_texte_fixe_3'])."',
							'".txt_db($res_info['doc_texte_fixe_4'])."',
							'".txt_db($res_info['doc_texte_fixe_5'])."',
							'".txt_db($res_info['doc_type_cr'])."',
							'".txt_db($res_info['doc_texte_fixe_6'])."',
							'".txt_db($res_info['doc_texte_fixe_7'])."',
							'".txt_db($res_info['doc_texte_fixe_8'])."')";
						$qry_ins_doc_info = $db->query($sql_ins_doc_info);
					}
				}
				
				$sql_insert = "INSERT INTO DOCUMENT VALUES('".$seq_num."', '".txt_db($_POST['nvo_nom_doc'])."', '".$qry_doc[0]['type_gabarit']."', '".$qry_doc[0]['type_zone1']."', '".$qry_doc[0]['type_zone2']."', '".$qry_doc[0]['type_zone3']."', '".$qry_doc[0]['type_zone4']."')";
				$qry_insert = $db->query($sql_insert);
			
				
				/* On redirige */
				header('location:gestion_docs.php?docid='.intval($seq_num));
			}else{
				$error = '<?php echo $t_manque_questionnaire ?>';
			}
		}
	?>
	<html>
	<head>
	<title>Vakom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<link rel="stylesheet" href="../css/style.css" type="text/css">
	<script language="JavaScript">
	<!--
	function supprimer_doc(docid){
		if (docid>0){
			var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
			var filename = "ajax_supp_doc.php"; // La page qui réceptionne les données
			var data     = null; 

			var xhr_object = null; 
				 
			if(window.XMLHttpRequest) // Firefox 
			   xhr_object = new XMLHttpRequest(); 
			else if(window.ActiveXObject) // Internet Explorer 
			   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
			else { // XMLHttpRequest non supporté par le navigateur 
			   alert("<?php echo $t_browser_support_error_1 ?>");
			   return; 
			} 
			 

			data = "docid="+docid;
			
			if(method == "GET" && data != null) {
			   filename += "?"+data;
			   data      = null;
			}
			 
			xhr_object.open(method, filename, true);

			xhr_object.onreadystatechange = function() {
			   if(xhr_object.readyState == 4) {
				  var tmp = xhr_object.responseText.split(":"); 
				  if(typeof(tmp[0]) != "undefined") { 
					if(tmp[0]==0){
						if(confirm('<?php echo $t_supprimer_document ?>')){
							document.location.href='gestion_docs.php?docid='+docid+'&action=delete_doc';
						}
					}else{
						alert('<?php echo $t_supprimer_document_used ?>');
					}
				  }
			   } 
			} 

			xhr_object.send(data); //On envoie les données
		}
		
	}
	
	function cache(xxid){
		document.getElementById(xxid).style.display='none';
	}
	function montre(xxid){
		document.getElementById(xxid).style.display='block';
	}
	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
	<?php
		include('menu_top_new.php');
	?>

	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
							  
		<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;DOCUMENTS</td>
		</tr>
		</table>
	<form method="post" name="form1" action="#">
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX">
			<table border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr> 
				<td align="left" class="menu_Gris"> 
				  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
					<?php
					if (is_array($qry_list_doc)){
						?>
						<tr> 
						  <td align="left" class="TX"> 
						  <?php
						  if (is_array($qry_doc)){
							$checked = ' checked="checked"';
						  }else{
							$checked = '';	
						  }
						  ?>
							<input type="radio" id="choix1" name="choix" checked="checked" value="selection"<?php echo $checked ?>>
							S&eacute;lectionner un document existant :&nbsp; </td>
						  <td align="left" class="TX"> 
							<select name="select_doc" id="sel_doc" class="form_ediht_Tarifs" onclick="document.getElementById('choix1').checked=true;">
							<?php
								foreach($qry_list_doc as $doc_list){
									if (is_array($qry_doc)){
										if ($doc_list['doc_id'] == $qry_doc[0]['doc_id']){
											$selected= ' selected="selected"';
										}else{
											$selected = '';
										}
									}
									echo '<option value="'.$doc_list['doc_id'].'"'.$selected.'>'.htmlentities($doc_list['doc_nom']).'</option>';
								}
							?>								
							</select>&nbsp;<input type="button" class="bn_ajouter" value="Supprimer" onClick="supprimer_doc(document.getElementById('sel_doc').options[document.getElementById('sel_doc').selectedIndex].value);">
						  </td>
						</tr>
						<?php
					}
					?>
					<tr> 
					  <td class="TX"> 
						<input onClick="montre('gabarits');" type="radio" id="choix2" name="choix" value="creation">
						Cr&eacute;er un nouveau document :</td>
					  <td class="TX"> 
						<input type="text" name="doc_nom" class="form_ediht_Tarifs" size="30" maxlength="120" onFocus="montre('gabarits'); document.getElementById('choix2').checked=true;">
					  </td>
					</tr>
			<?php
			  /* S'il y a au moins un document dans la base de données */
			  if (is_array($qry_list_doc)){
			  ?>
			  <tr> 
				<td align="left" class="TX"> 
				  <input type="radio" id="choix_action3" name="choix" value="duplication">
				  Dupliquer un document :&nbsp; &nbsp;</td>
				<td align="left" class="TX"> 
				  <select name="select_duplic" class="form_ediht_Tarifs" onclick="document.getElementById('choix_action3').checked=true;">
					<?php
					foreach($qry_list_doc as $doc_liste2){
						if (is_array($qry_doc)){
							if ($doc_liste2['doc_id'] == $qry_doc[0]['doc_id']){
								$selected= ' selected="selected"';
							}else{
								$selected = '';
							}
						}
						echo '<option value="'.$doc_liste2['doc_id'].'"'.$selected.'>'.htmlentities($doc_liste2['doc_nom']).'</option>';
					}
					?>
				  </select>
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nouveau&nbsp;libellé&nbsp;:&nbsp;<input type="text" name="nvo_nom_doc" class="form_ediht_Tarifs" size="30" maxlength="120" onClick="document.getElementById('choix_action3').checked=true;">
				</td>
			  </tr>
			  <?php
			  }
			  ?>
				  </table>
				</td>
			  </tr>
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14" class="TX"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	  	<br>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr><td align="center">
			<input type="hidden" name="step" value="1">
			<input type="submit" value="Valider" class="BN">
		</td></tr>
		</table>
	  <br>

	<div id="gabarits" <?php if (!is_array($qry_doc)){ echo 'style="display: none;"'; } ?>>
	<?php
	if (is_array($qry_doc)){
		?>
		<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo ucfirst($qry_doc[0]['doc_nom']) ?></td>
		</tr>
		</table>
		<?php
	}
	?>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
		<tr> 
		  <td width="14">&nbsp;</td>
		  <td class="TX_Tarifs">CONTENU DU DOCUMENT</td>
		  <td width="14">&nbsp;</td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td bgcolor="#666666" height="1"></td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14">&nbsp;</td>
		  <td></td>
		  <td width="14">&nbsp;</td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX"> 
			<table width="750" border="0" cellspacing="0" cellpadding="2">
			  <tr align="center"> 
				<td class="TX" valign="top"> 
				  <input type="radio" name="gabarit" value="1" <?php if ($qry_doc[0]['type_gabarit'] == '1' || !is_array($qry_doc)) echo 'checked="checked"';{}?>>
				  Gabarit 1</td>
				<td class="TX" valign="top"> 
				  <input type="radio" name="gabarit" value="2" <?php if ($qry_doc[0]['type_gabarit'] == '2') echo 'checked="checked"';{}?>>
				  Gabarit 2</td>
				<td class="TX" valign="top"> 
				  <input type="radio" name="gabarit" value="3" <?php if ($qry_doc[0]['type_gabarit'] == '3') echo 'checked="checked"';{}?>>
				  Gabarit 3</td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top" align="center"> 
				  <table width="180" border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000" height="200">
					<tr bgcolor="#C4C4C4"> 
					  <td align="center"> 
						<p><b>Zone 1</b></p>
						<p> 
						  <select name="1-ZONE1" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '1'){
									if ($qry_doc[0]['type_zone1']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 1){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					</tr>
				  </table>
				</td>
				<td class="TX" align="center" valign="top"> 
				  <table width="180" border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000" height="200">
					<tr bgcolor="#C4C4C4"> 
					  <td align="center" height="100"> 
						<p><b>Zone 1</b></p>
						<p> 
						  <select name="2-ZONE1" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '2'){
									if ($qry_doc[0]['type_zone1']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 2){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					</tr>
					<tr bgcolor="#C4C4C4"> 
					  <td align="center"> 
						<p><b>Zone 2</b></p>
						<p> 
						  <select name="2-ZONE2" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '2'){
									if ($qry_doc[0]['type_zone2']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 6){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					</tr>
				  </table>
				</td>
				<td class="TX" align="center" valign="top"> 
				  <table border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000" height="200">
					<tr bgcolor="#C4C4C4"> 
					  <td align="center" height="100"> 
						<p><b>Zone 1</b></p>
						<p> 
						  <select name="3-ZONE1" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '3'){
									if ($qry_doc[0]['type_zone1']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 4){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					  <td align="center" height="100"> 
						<p><b>Zone 2</b></p>
						<p> 
						  <select name="3-ZONE2" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '3'){
									if ($qry_doc[0]['type_zone2']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 3){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					</tr>
					<tr bgcolor="#C4C4C4"> 
					  <td align="center" colspan="2"> 
						<p><b>Zone 3</b></p>
						<p> 
						  <select name="3-ZONE3" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '3'){
									if ($qry_doc[0]['type_zone3']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 5){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					</tr>
				  </table>
				</td>
			  </tr>
			  <tr align="center"> 
				<td height="4"></td>
			  </tr>
			  <tr align="center">
				<td class="TX" valign="top" colspan="3">&nbsp; </td>
			  </tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="2">
			  <tr align="center"> 
				<td class="TX" valign="top"> 
				  <input type="radio" name="gabarit" value="4" <?php if ($qry_doc[0]['type_gabarit'] == '4') echo 'checked="checked"';{}?>>
				  Gabarit 4</td>
				<td class="TX" valign="top"> 
				  <input type="radio" name="gabarit" value="5" <?php if ($qry_doc[0]['type_gabarit'] == '5') echo 'checked="checked"';{}?>>
				  Gabarit 5</td>
			  </tr>
			  <tr> 
				<td class="TX" align="center" valign="top"> 
				  <table border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000" width="180">
					<tr bgcolor="#C4C4C4"> 
					  <td align="center" height="80" valign="middle"> <b>Zone 1</b> 
						<p> 
						  <select name="4-ZONE1" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '4'){
									if ($qry_doc[0]['type_zone1']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 2){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					</tr>
					<tr bgcolor="#C4C4C4"> 
					  <td align="center" height="80" valign="middle"> <b>Zone 2</b> 
						<p> 
						  <select name="4-ZONE2" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '4'){
									if ($qry_doc[0]['type_zone2']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 1){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					</tr>
					<tr bgcolor="#C4C4C4"> 
					  <td align="center" height="80" valign="middle"> <b>Zone 3</b> 
						<p> 
						  <select name="4-ZONE3" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '4'){
									if ($qry_doc[0]['type_zone3']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 2){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					</tr>
				  </table>
				</td>
				<td class="TX" align="center" valign="top"> 
				  <table border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000">
					<tr bgcolor="#C4C4C4"> 
					  <td align="center" height="50" width="120"> 
						<p><b>Zone 1</b></p>
						<p> 
						  <select name="5-ZONE1" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '5'){
									if ($qry_doc[0]['type_zone1']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 1){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					  <td align="center" height="100" rowspan="2" width="120"> 
						<p><b>Zone 3</b></p>
						<p> 
						  <select name="5-ZONE3" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '5'){
									if ($qry_doc[0]['type_zone3']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 2){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					  </tr>
					  <tr bgcolor="#C4C4C4"> 
					  <td align="center" height="50"> 
						<p><b>Zone 2</b></p>
						<p> 
						  <select name="5-ZONE2" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '5'){
									if ($qry_doc[0]['type_zone2']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 6){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					</tr>
					<tr bgcolor="#C4C4C4"> 
					  <td align="center" colspan="2" height="140" width="240"> 
						<p><b>Zone 4</b></p>
						<p> 
						  <select name="5-ZONE4" class="form_gabarit">
							<?php
							for ($i=1; $i<=count($choix_zone); $i++){
								unset($selected);
								if (is_array($qry_doc) && $qry_doc[0]['type_gabarit'] == '5'){
									if ($qry_doc[0]['type_zone4']==$i){
										$selected = ' selected="selected"';
									}
								}else{
									if ($i == 4){
										$selected = ' selected="selected"';
									}
								}
								echo '<option value="'.$i.'"'.$selected.'>'.$choix_zone[$i].'</option>';
							}
							unset($i);
							?>
						  </select>
						</p>
					  </td>
					</tr>
				  </table>
				</td>
			  </tr>
			  <tr align="center"> 
				<td height="4"></td>
			  </tr>
			</table>
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	  <br>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr><td align="center">
			<input type="hidden" name="step" value="2">
			<input type="submit" value="Valider" class="BN">
		</td></tr>
		</table>
	  </form>
	</div>
</p></div>	</article></div>	</div>	</div>	</div>	  			
	</body>
	</html>
<?php
}else{
	include('no_acces.php');
}
?>