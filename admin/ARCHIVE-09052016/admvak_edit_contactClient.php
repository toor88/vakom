<?php
session_start();
	// Si l'utilisateur est un super admin
if ($_SESSION['droit']>5){
	if ($_GET['certid']!=''){
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);

		if ($_POST['certid']!=''){
			$num = $_POST['certid'];
		
			if ($_POST['inactif']=='1'){
				$actif='0';
			}else{
				$actif='1';
			}	
			
			/* Si le pays selectionné n'est pas la france */
			if ($_POST['select_pays']!='5'){
				/* Le CP a pour valeur le champ cp_etr et la ville a pour valeur le champ ville_etr*/
				$cp 	= $_POST['cp_etr'];
				$ville 	= $_POST['ville_etr'];
			}else{
			/* Si le pays selectionné est la france, le CP porte la valeur du champ cp */
				$cp 	= $_POST['cp'];
				/* Si la ville n'existe pas dans la base, la ville porte la valeur du champ nom_new_ville */
				if ($_POST['new_ville']=='1'){
					$ville 	= $_POST['nom_new_ville'];
				}else{
					/* Si elle existe, la valeur de la ville est celle de la liste de sélection */
					if(preg_match('/_/', $_POST['ville'])){
						$tab_v 	=  explode("_",$_POST['ville']);
						$ville 	= $tab_v[1];
					}
					else
						$ville 	= $_POST['ville'];
				}
			}
			
			$sql_update_cert 	= "UPDATE CERTIFIE SET ACTIF='".$actif."', CERT_NOM = '".txt_db($_POST['nom'])."', 
			CERT_PRENOM = '".txt_db($_POST['prenom'])."', 
			CERT_AUTRE_NOM_PART = '".txt_db($_POST['autre_nom_part'])."', 
			CERT_DROIT_ADMIN = '".txt_db($_POST['droit_admin'])."', 
			CERT_DROIT_CERTIFIE = '".txt_db($_POST['droit_certif'])."', 
			CERT_LOGIN = '".txt_db($_POST['ca'])."', 
			CERT_PWD = '".txt_db($_POST['mp'])."', 
			CERT_TEL = '".txt_db($_POST['tel'])."', 
			CERT_EMAIL = '".txt_db($_POST['mail'])."', 
			CERT_AD1 = '".txt_db($_POST['adr'])."', 
			CERT_AD2 = '".txt_db($_POST['adr2'])."', 
			CERT_CP = '".txt_db($cp)."', 
			CERT_VILLE = '".txt_db($ville)."', 
			CERT_PAYS_CODE_ID = '".txt_db($_POST['select_pays'])."', 
			CERT_FAX = '".txt_db($_POST['fax'])."', 
			CERT_PORTABLE = '".txt_db($_POST['tel_p'])."', 
			CERT_INFO1 = '".txt_db($_POST['comm1'])."', 
			CERT_INFO2 = '".txt_db($_POST['comm2'])."', 
			CERT_CAND_ID = '".txt_db($_POST['select_candidat'])."', 
			CERT_USER_MODIFICATION_ID = '".txt_db($_SESSION['vak_id'])."', 
			CERT_DATE_MODIFICATION = SYSDATE ";

			if (strlen($_POST['JJ_DNS'])==2 && strlen($_POST['MM_DNS'])==2 && strlen($_POST['AAAA_DNS'])==4 && $_POST['JJ_DNS']>00 && $_POST['JJ_DNS']<32 && $_POST['MM_DNS']>00 && $_POST['MM_DNS']<13 && $_POST['AAAA_DNS']>1900 && $_POST['AAAA_DNS']<date('Y')){
				$sql_update_cert .= ", CERT_DNS = TO_DATE('".txt_db($_POST['JJ_DNS'])."/".txt_db($_POST['MM_DNS'])."/".txt_db($_POST['AAAA_DNS'])."', 'DD/MM/YYYY') ";
			}else{
				$sql_update_cert .= ", CERT_DNS = '' ";
			}
			/* Si on veut transférer le certifié à un autre partenaire */
			if ($_POST['select_transfert']>0){
				/* On doit supprimer l'ancienne fiche */
				$sql_update_cert .= ", CERT_USER_SUPPRESSION_ID = '".txt_db($_SESSION['vak_id'])."', 
				CERT_DATE_SUPPRESSION = SYSDATE ";
			}
			
			$sql_update_cert .= "WHERE CERT_ID = '".$_POST['certid']."'";
			
			$qry_update_cert		= $db->query($sql_update_cert);
			
			if (is_array($_POST['liste_certif'])){
				foreach($_POST['liste_certif'] as $certif_a_modifier){
					$sql_up_certif = "UPDATE CERT_A_CERTIF SET CERTIF_FORMATION='".txt_db($_POST[$certif_a_modifier.'_formation'])."', CERTIF_SUSPENDU='".txt_db($_POST[$certif_a_modifier.'_suspendu'])."'";
					if (strlen($_POST[$certif_a_modifier.'_JJ'])==2 && strlen($_POST[$certif_a_modifier.'_MM'])==2 && strlen($_POST[$certif_a_modifier.'_AAAA'])==4){
						$sql_up_certif.= ", CERTIF_PERFECTIONNEMENT = TO_DATE('".txt_db($_POST[$certif_a_modifier.'_JJ'])."/".txt_db($_POST[$certif_a_modifier.'_MM'])."/".txt_db($_POST[$certif_a_modifier.'_AAAA'])."', 'DD/MM/YYYY')"; 
					}else{
						$sql_up_certif.= ", CERTIF_PERFECTIONNEMENT = ''";
					}
					if (strlen($_POST[$certif_a_modifier.'_JJ2'])==2 && strlen($_POST[$certif_a_modifier.'_MM2'])==2 && strlen($_POST[$certif_a_modifier.'_AAAA2'])==4){
						$sql_up_certif.= ", CERTIF_CERTIFICATION = TO_DATE('".txt_db($_POST[$certif_a_modifier.'_JJ2'])."/".txt_db($_POST[$certif_a_modifier.'_MM2'])."/".txt_db($_POST[$certif_a_modifier.'_AAAA2'])."', 'DD/MM/YYYY')"; 
					}else{
						$sql_up_certif.= ", CERTIF_CERTIFICATION = ''";
					}
					$sql_up_certif.= " WHERE CERTIF_ID='".txt_db(intval($certif_a_modifier))."'";
					$qry_up_certif = $db->query($sql_up_certif);
				}			
			}
			
			if ($_POST['select_transfert']>0){
			
				/* On récupère le nouvel id a insérer */
				$sql_num = "SELECT SEQ_ID.NEXTVAL NB FROM DUAL";
				$qry_num = $db->query($sql_num);
				$num = $qry_num[0]['nb'];
				
				/* On insert la nouvelle fiche avec le nouveau partenaire */
				$sql_insert_cert = "INSERT INTO CERTIFIE VALUES('".intval($num)."',
				'".txt_db(intval($_POST['select_transfert']))."',
				'".txt_db($_POST['nom'])."', 
				'".txt_db($_POST['prenom'])."', ";
				if (strlen($_POST['JJ_DNS'])==2 && strlen($_POST['MM_DNS'])==2 && strlen($_POST['AAAA_DNS'])==4 && $_POST['JJ_DNS']>00 && $_POST['JJ_DNS']<32 && $_POST['MM_DNS']>00 && $_POST['MM_DNS']<13 && $_POST['AAAA_DNS']>1900 && $_POST['AAAA_DNS']<date('Y')){
					$sql_insert_cert .= "TO_DATE('".txt_db($_POST['JJ_DNS'])."/".txt_db($_POST['MM_DNS'])."/".txt_db($_POST['AAAA_DNS'])."', 'DD/MM/YYYY'), ";
				}else{
					$sql_insert_cert .= "'', ";
				}	
				$sql_insert_cert .= "'".txt_db($_POST['droit_admin'])."', 
				'".txt_db($_POST['droit_certif'])."', 
				'".txt_db($_POST['ca'])."', 
				'".txt_db($_POST['mp'])."', 
				'".txt_db($_POST['tel'])."', 
				'".txt_db($_POST['mail'])."', 
				'".txt_db($_POST['adr'])."', 
				'".txt_db($_POST['adr2'])."', 
				'".txt_db($_POST['cp'])."', 
				'".txt_db($_POST['ville'])."', 
				'".txt_db(intval($_POST['select_pays']))."', 
				'".txt_db($_POST['fax'])."', 
				'".txt_db($_POST['tel_p'])."', 
				'".txt_db($_POST['select_candidat'])."', 
				'".txt_db($_POST['comm1'])."', 
				'".txt_db($_POST['comm2'])."',
				'".txt_db($_SESSION['vak_id'])."',
				TO_DATE('".txt_db($_POST['cert_date_creation'])."', 'DD/MM/YYYY HH24:MI'),
				'".txt_db($_SESSION['vak_id'])."',
				SYSDATE,
				'',
				'',
				'".$actif."',
				'".txt_db($_POST['autre_nom_part'])."')";
				$qry_insert_cert = $db->query($sql_insert_cert);
				
				/* On duplique toutes les certifications obtenues par ce certifié */
				/* On commence donc par faire le select de toutes les certifs qu'il a obtenues */
				$sql_sel_certif = "SELECT  * FROM CERT_A_CERTIF WHERE CERTIF_CERT_ID='".txt_db(intval($_POST['certid']))."'";
				$qry_sel_certif = $db->query($sql_sel_certif);
				
				/* Pour chaque certification trouvée */
				if (is_array($qry_sel_certif)){
					foreach($qry_sel_certif as $certif_a_dupliquer){
						$sql_insert_new_certif = "INSERT INTO CERT_A_CERTIF VALUES(SEQ_ID.NEXTVAL, 
						'".txt_db(intval($num))."', 
						'".$certif_a_dupliquer['certif_code_id']."', 
						'".$certif_a_dupliquer['certif_perfectionnement']."', 
						'".$certif_a_dupliquer['certif_certification']."', 
						'".$certif_a_dupliquer['certif_formation']."', 
						'".$certif_a_dupliquer['certif_suspendu']."')";
						$qry_insert_new_certif = $db->query($sql_insert_new_certif);
					}
				}
			}
			$sql_insert_new_certif = "UPDATE CERT_A_CERTIF SET CERTIF_FORMATION=0 WHERE CERTIF_SUSPENDU=1 AND CERTIF_CERT_ID='".txt_db(intval($_POST['certid']))."'";
			$qry_insert_new_certif = $db->query($sql_insert_new_certif);
			//echo $sql_update_cert;

		?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
				//document.location.href='admvak_edit_contactClient.php?certid=<?php echo txt_db($num) ?>&idnc=<?php echo ($_GET['idnc']+1) ?>';
			</script>
			<?php
		}
		
		$sql_cert = "SELECT CERTIFIE.*, TO_CHAR(CERTIFIE.CERT_DATE_CREATION, 'DD/MM/YYYY HH24:MI') CERT_DATE_CREATION, TO_CHAR(CERTIFIE.CERT_DATE_MODIFICATION, 'DD/MM/YYYY HH24:MI') CERT_DATE_MODIFICATION, TO_CHAR(CERTIFIE.CERT_DNS, 'DD') JJ_DNS, TO_CHAR(CERTIFIE.CERT_DNS, 'MM') MM_DNS, TO_CHAR(CERTIFIE.CERT_DNS, 'YYYY') AAAA_DNS, PARTENAIRE.PART_NOM, PARTENAIRE.PART_ID FROM CERTIFIE, PARTENAIRE WHERE CERTIFIE.CERT_PART_ID=PARTENAIRE.PART_ID AND CERTIFIE.CERT_ID='".txt_db($_GET['certid'])."'";
		$qry_cert = $db->query($sql_cert);
		
		// Chargement de la liste des partenaires
		$sql_part_list 		= "SELECT * FROM PARTENAIRE ORDER BY PART_NOM ASC";
		$qry_part_list 		= $db->query($sql_part_list);
		
		// Chargement de la liste des pats
		$sql_pays_list 		= "SELECT * FROM CODE WHERE CODE_TABLE='PAYS' ORDER BY CODE_ORDRE";
		$qry_pays_list 		= $db->query($sql_pays_list);
		
		// Chargement de la liste des certifications
		$sql_certif_list 		= "SELECT * FROM CODE WHERE CODE_TABLE='CERTIFICATION' ORDER BY CODE_LIBELLE ASC";
		$qry_certif_list 		= $db->query($sql_certif_list);
		
		// Chargement de la liste des certifications
		$sql_cert_certif_list 		= "SELECT CERT_A_CERTIF.*, CODE.*, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_PERFECTIONNEMENT, 'DD') JJ, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_PERFECTIONNEMENT, 'MM') MM, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_PERFECTIONNEMENT, 'YYYY') AAAA, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_CERTIFICATION, 'DD') JJ2, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_CERTIFICATION, 'MM') MM2, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_CERTIFICATION, 'YYYY') AAAA2 
		FROM CERT_A_CERTIF,CODE WHERE CODE.CODE_TABLE='CERTIFICATION' AND CERT_A_CERTIF.CERTIF_CODE_ID=CODE.CODE_ID AND CERT_A_CERTIF.CERTIF_CERT_ID='".txt_db(intval($_GET['certid']))."' ORDER BY CODE.CODE_LIBELLE ASC";
		$qry_cert_certif_list 		= $db->query($sql_cert_certif_list);
		
		if (is_array($qry_cert)){
		
			$sql_cand_assoc = "select cand_id, cli_nom, cand_nom, cand_prenom, last_opr from candidat,client where candidat.last_opr is not null and candidat.cand_cli_id=client.cli_id and LOWER(cand_nom) = '".txt_db(strtolower($qry_cert[0]['cert_nom']))."' and LOWER(cand_prenom) = '".txt_db(strtolower($qry_cert[0]['cert_prenom']))."' union select cand_id, '' cli_nom, cand_nom, cand_prenom, last_opr from candidat where last_opr is not null and cand_cli_id='-1' and LOWER(cand_nom) = '".txt_db(strtolower($qry_cert[0]['cert_nom']))."' and LOWER(cand_prenom) = '".txt_db(strtolower($qry_cert[0]['cert_prenom']))."'";
			$qry_cand_assoc = $db->query($sql_cand_assoc);
			?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--
			function hid_ville(id_pays){
				if (id_pays != "5"){
					document.getElementById('cp1').style.display='none';
					document.getElementById('cp2').style.display='inline';
					document.getElementById('ville2').style.display='none';
					document.getElementById('ville3').style.display='inline';
				}else{
					document.getElementById('cp1').style.display='inline';
					document.getElementById('cp2').style.display='none';
					document.getElementById('ville2').style.display='inline';
					document.getElementById('ville3').style.display='none';
				}
			}
			
			function charge_code(code){
				var tmp_code = code.split("_");
				document.getElementById('cp').value = tmp_code[0];
			}
		
			function TestVille(){
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_ville.php"; // La page qui réceptionne les données
				var cp = document.getElementById('cp').value; 
				var data     = null; 
				
				if 	(cp.length>1){ //Si le code postal tapé possède au moins 2 caractères
					document.getElementById("ville2").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" />';
					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
						   return; 
						} 
						 
						
						 
						if(cp != ""){
							data = "cp="+cp+"&class=cert";
						}
						if(method == "GET" && data != null) {
						   filename += "?"+data;
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split(":"); 
							  if(typeof(tmp[0]) != "undefined") { 
								 document.getElementById("ville2").innerHTML = '';
								 if (tmp[0]!=''){
									 document.getElementById('saisie_ville_m').style.display='inline';
									 document.getElementById('saisie_ville_m_lab').style.display='inline';
									 document.getElementById('saisie_ville_m').checked = false;								 
									document.getElementById("ville2").innerHTML = tmp[0];
								 }else{
									document.getElementById('saisie_ville_m').style.display='none';
									document.getElementById('saisie_ville_m_lab').style.display='none';
									document.getElementById("ville2").innerHTML = '<input type="hidden" name="new_ville" value="1"><input type="text" size="46" name="nom_new_ville" maxlength="255" class="form_ediht_Certifies" />';
								 }
							  }
						   } 
						} 

						xhr_object.send(data); //On envoie les données
				}
			}
			
			function doSaisieLibre(){
				var saisie_ville_m = document.getElementById('saisie_ville_m');
				if (saisie_ville_m.checked){
					document.getElementById('saisie_ville_m').style.display='none';
					document.getElementById('saisie_ville_m_lab').style.display='none';
					document.getElementById("ville2").innerHTML = '<input type="hidden" name="new_ville" value="1"><input type="text" size="46" name="nom_new_ville" maxlength="255" class="form_ediht_Certifies" />';
				}
			}
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function ajout_certif_dispo(certifid){
			
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_affect_certif.php"; // La page qui réceptionne les données
				var data     = null; 
				
				if 	(certifid.length>0){ //Si le code postal tapé possède au moins 2 caractères
					document.getElementById("tablo_certif_dispo").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" />';
					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
						   return; 
						} 
						 
						
						 
						if(certifid != ""){
							data = "certifid="+certifid;
						}
						if(method == "GET" && data != null) {
						   filename += "?certid= $_GET['certid'] &"+data;
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split(":"); 
							  if(typeof(tmp[0]) != "undefined") { 
								 document.getElementById("tablo_certif_dispo").innerHTML = '';
								 if (tmp[0]!=''){
									document.getElementById("tablo_certif_dispo").innerHTML = tmp[0];
								 }
							  }
						   } 
						} 

						xhr_object.send(data); //On envoie les données
				}
				
			}
			
			function verif(){
				error = '';
				error2 = '';
					
				if (document.form.nom.value<1){
					error += "<?php echo $t_nom_oblig ?>\n";
				}				
				if (document.form.prenom.value<1){
					error += "<?php echo $t_prenom_oblig ?>\n";
				}
				if (document.form.ca.value<1){
					error += "<?php echo $t_login_oblig ?>\n";
				}
				if (document.form.mp.value<1){
					error += "<?php echo $t_mdp_oblig ?>\n";
				}
				if (document.form.cp.value == ''){
							error += "<?php echo $t_cp_oblig ?>\n";
				}
				if (document.form.ville.value == ''){
					error += "<?php echo $t_ville_oblig ?>\n";
				}
				if (document.form.tel.value.length<10){
					error += "<?php echo $t_tel_oblig ?>\n";
				}
				if (document.form.mail.value<1){
					error += "<?php echo $t_email_oblig ?>\n";
				}
				if (document.form.droit_admin.checked!="1" && document.form.droit_certif.checked!="1"){
					error += "<?php echo $t_droits_oblig ?>\n";
				}
				
				if (document.form.JJ_DNS.value!='' || document.form.MM_DNS.value!='' || document.form.AAAA_DNS.value!=''){
					// Verif du format date de fin
					if (document.form.JJ_DNS.value<1 || document.form.JJ_DNS.value>31 || document.form.JJ_DNS.value.length<2){
						error2 = true;
					}
					if (document.form.MM_DNS.value<1 || document.form.MM_DNS.value>12 || document.form.MM_DNS.value.length<1){
						error2 = true;
					}
					if (document.form.AAAA_DNS.value<1910 || document.form.AAAA_DNS.value.length<4){
						error2 = true;
					}
					if (error2==true){
						error +="<?php echo $t_mauvaise_date_naiss ?>\n";
					}
				}
				
				
				/* Vérification des dates saisies pour les certifications */
			//	var compte_certif = 5;
			//	for(i=0; i<5; i++){
					
			//	}

			
			
				function verifdt(mydate){
				
					verdat=mydate;
					datej= new Date()
					anneej=datej.getFullYear()+"*";
					anneej=anneej.substring(0,2)

					if (verdat.length ==6) 
					verdat=verdat.substring(0,2)+"/"+verdat.substring(4,2)+"/"+anneej+verdat.substring(6,4);

					if (verdat.length ==8) 
					verdat=verdat.substring(0,2)+"/"+verdat.substring(4,2)+"/"+verdat.substring(8,4);


					if(!isValidDate(verdat))
					return false;
					else
					return true;

				}

				function isValidDate(d) {
					var dateRegEx = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
					return d.match(dateRegEx);
				} 
			
			
				var elmt = '';
				var count_certif = document.getElementsByName('liste_certif[]').length;
				for (i=0; i<count_certif; i++){
					j = Number(i)+Number(1);
					/* Pour chaque ligne de certification présente */
					elmt = document.getElementsByName('liste_certif[]')[i].value;
					
					if(document.getElementById(elmt+'_JJ').value!='' || document.getElementById(elmt+'_MM').value!='' || document.getElementById(elmt+'_AAAA').value!=''){
						if(!verifdt(document.getElementById(elmt+'_JJ').value + '/' + document.getElementById(elmt+'_MM').value  + '/' + document.getElementById(elmt+'_AAAA').value)){
							error += '<?php echo $lib_sur_la_ligne_des_certifs ?>'+j+'<?php echo $lib_date_perf_incorrect ?>\r\n';
						}
					}
					if(document.getElementById(elmt+'_JJ2').value!='' || document.getElementById(elmt+'_MM2').value!='' || document.getElementById(elmt+'_AAAA2').value!=''){
						if(!verifdt(document.getElementById(elmt+'_JJ2').value + '/' + document.getElementById(elmt+'_MM2').value  + '/' + document.getElementById(elmt+'_AAAA2').value)){
							error += '<?php echo $lib_sur_la_ligne_des_certifs ?>'+j+'<?php echo $lib_date_certif_incorrect ?>\r\n';
						}
					}
				}
				 

				if (error!=''){
					alert(error);
				}else{
					document.form.submit();
				}
			}
			
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}

			function pasdate1(){
				var obj=document.form.JJ_DNS.value.length;
				if (obj==2){
					document.form.MM_DNS.value='';
					document.form.MM_DNS.focus();
				}

			}
			function pasdate2(){
				var obj=document.form.MM_DNS.value.length;
				if (obj==2){
					document.form.AAAA_DNS.value='';
					document.form.AAAA_DNS.focus();
				}
			}
			
			function pass(champ, certifid){
				if (champ==1){
					if (document.getElementById(certifid+'_JJ').value.length==2){
						document.getElementById(certifid+'_MM').value='';
						document.getElementById(certifid+'_MM').focus();
					}
				}
				if (champ==2){
					if (document.getElementById(certifid+'_MM').value.length==2){
						document.getElementById(certifid+'_AAAA').value='';
						document.getElementById(certifid+'_AAAA').focus();
					}
				}

				if (champ==4){
					if (document.getElementById(certifid+'_JJ2').value.length==2){
						document.getElementById(certifid+'_MM2').value='';
						document.getElementById(certifid+'_MM2').focus();
					}
				}
				if (champ==5){
					if (document.getElementById(certifid+'_MM2').value.length==2){
						document.getElementById(certifid+'_AAAA2').value='';
						document.getElementById(certifid+'_AAAA2').focus();
					}
				}
			}
			
			function envoi_mail(){
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_edit_contactClient_mail.php"; // La page qui réceptionne les données
				var data     = null; 

					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
						   return; 
						} 
						 
						
						 
						
						data = "certid=<?php echo $_GET['certid'] ?>";
						
						if(method == "GET" && data != null) {
						   filename += "?"+data;
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split(":"); 
							  if(typeof(tmp[0]) != "undefined") { 
								 document.getElementById("temp_mail_ok").innerHTML = '';
								 if (tmp[0]!=''){
									document.getElementById("temp_mail_ok").innerHTML = '&nbsp;<?php echo $t_mail_OK ?>';
								 }
							  }
						   } 
						} 

					xhr_object.send(data); //On envoie les données
				
			}
			//-->
			</script>
			</head>

			<body bgcolor="#FFFFFF" text="#000000">
			<form method="post" action="#" name="form">
			 <table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Certifies"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp; <?php echo htmlentities($qry_cert[0]['part_nom']).' > '.htmlentities($qry_cert[0]['cert_prenom']).' '.htmlentities($qry_cert[0]['cert_nom'])?></td>
				</tr>
			  </table>
			  <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="900" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td width="150" class="fond_tablo_certifies"><?php echo $t_fiche_cert ?></td>
				  <td align="left" class="fond_tablo_certifies2"><?php echo $t_date_creation ?> : le <?php echo $qry_cert[0]['cert_date_creation']?>
					<input type="hidden" name="cert_date_creation" value="<?php echo $qry_cert[0]['cert_date_creation']?>">
					<?php
					if ($qry_cert[0]['cert_date_modification'] != ''){
						echo '<br>'.$t_date_modif.' : le '.$qry_cert[0]['cert_date_modification'];
					}
					?>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="center" colspan="2" valign="top" class="TX"> 
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
					  <tr> 
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
						<td class="champsoblig" valign="middle" align="right"><?php echo $t_champs_oblig ?> 
						  * </td>
					  </tr>
					  <tr> 
						<td colspan="2" class="TX" height="40"><span  <?php if($qry_cert[0]['actif']=="0") echo ' style="color:#FF0000; font-weight: bold; font-size: 20px;"'?>>Certifié&nbsp;Inactif*&nbsp;:</span>
						  <input type="checkbox" name="inactif" value="1" <?php if($qry_cert[0]['actif']=="0") echo ' checked="checked"'?>>
						</td>	
						<td class="TX"> </td>
						<td class="TX"> </td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_part ?>&nbsp;:</td>
						<td class="TX"> 
						  <?php echo htmlentities($qry_cert[0]['part_nom']) ?>
						</td>
						<td class="TX" height="40"><?php echo $t_autre_nom_part ?>&nbsp;:</td>
						<td class="TX"> 
						  <input type="text" name="autre_nom_part" size="40" maxlength="80" class="form_ediht_Certifies" value="<?php echo htmlentities($qry_cert[0]['cert_autre_nom_part']) ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_nom ?>*&nbsp;:</td>
						<td class="TX"> 
						  <input type="text" name="nom" size="40" maxlength="40" class="form_ediht_Certifies" value="<?php echo htmlentities($qry_cert[0]['cert_nom']) ?>">
						</td>
						<td class="TX">&nbsp;<?php echo $t_prenom ?>*&nbsp;:</td>
						<td class="TX"> 
						  <input type="text" name="prenom" size="40" maxlength="40" class="form_ediht_Certifies" value="<?php echo ucfirst($qry_cert[0]['cert_prenom']) ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_date_naissance ?>&nbsp;:</td>
						<td class="TX"> 
						  <input type="text" name="JJ_DNS" size="2" class="form_ediht_Certifies" maxlength="2" value="<?php echo $qry_cert[0]['jj_dns'] ?>" onkeyup="pasdate1();">
						  / 
						  <input type="text" name="MM_DNS" size="2" class="form_ediht_Certifies" maxlength="2" value="<?php echo $qry_cert[0]['mm_dns'] ?>" onkeyup="pasdate2();">
						  / 
						  <input type="text" name="AAAA_DNS" size="4" class="form_ediht_Certifies" maxlength="4" value="<?php echo $qry_cert[0]['aaaa_dns'] ?>">
						</td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  <tr>
						<td class="TX" height="40"><?php echo $t_droits ?>*&nbsp;:</td>
						<td class="TX">
						<?php
						if ($qry_cert[0]['cert_droit_admin']=='1'){
							$checked1	= ' checked="checked" ';
						}else{
							$checked1	= ' ';
						}
						if ($qry_cert[0]['cert_droit_certifie']=='1'){
							$checked2	= ' checked="checked" ';
						}else{
							$checked2	= ' ';
						}
						?>
						  <input type="checkbox" name="droit_admin" value="1"<?php echo $checked1 ?>>
						  <?php echo $t_admin ?>&nbsp;&nbsp;&nbsp; 
						  <input type="checkbox" name="droit_certif" value="1"<?php echo $checked2 ?>>
						  <?php echo $t_cert ?></td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><a href="#" onclick="envoi_mail();"><?php echo $t_login ?></a>&nbsp;<?php echo $t_extranet ?>* :</td>
						<td class="TX"> 
						  <input type="text" name="ca" size="40" class="form_ediht_Certifies" maxlength="30" value="<?php echo htmlentities($qry_cert[0]['cert_login']) ?>"><div style="display: block; color: #00AA00; font-weight: bold;" id="temp_mail_ok"></div>
						</td>
						<td class="TX"><?php echo $t_mdp_extranet ?>*&nbsp;:</td>
						<td class="TX"> 
						  <input type="text" name="mp" size="40" class="form_ediht_Certifies" maxlength="40" value="<?php echo htmlentities($qry_cert[0]['cert_pwd']) ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_tel_fix ?>*&nbsp;:</td>
						<td> 
						  <input type="text" name="tel" size="40" class="form_ediht_Certifies" maxlength="25" value="<?php echo htmlentities($qry_cert[0]['cert_tel']) ?>">
						</td>
						<td class="TX"> </td>
						<td class="TX"> </td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_email ?>*&nbsp;:</td>
						<td colspan="3"> 
						  <input type="text" name="mail" size="40" class="form_ediht_Certifies" maxlength="130" value="<?php echo htmlentities($qry_cert[0]['cert_email']) ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_adresse ?>&nbsp;:
						<br>&nbsp;
						</td>
						<td colspan="3"> 
						  <input type="text" name="adr" size="134" class="form_ediht_Certifies" maxlength="130" value="<?php echo htmlentities($qry_cert[0]['cert_ad1']) ?>">
						  <br>
						  <input type="text" name="adr2" size="134" class="form_ediht_Certifies" maxlength="130" value="<?php echo htmlentities($qry_cert[0]['cert_ad2']) ?>">
						  &nbsp;&nbsp; </td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_pays ?>&nbsp;:</td>
						<td> 
						  <select name="select_pays" id="pays" class="form_ediht_Certifies" onchange="hid_ville(document.getElementById('pays').options[document.getElementById('pays').selectedIndex].value)">
						<?php
						  if (is_array($qry_pays_list)){
							foreach($qry_pays_list as $pays){
								if ($pays['code_id'] == $qry_cert[0]['cert_pays_code_id']){
									$selected = ' selected="selected" ';
								}else{
									$selected = ' ';
								}
								echo '<option'.$selected.'value="'.$pays['code_id'].'">'.$pays['code_libelle'].'</option>';
							}
						  }
						?>
						  </select>
						</td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_cp ?>*&nbsp;:</td>
						<td class="TX"> 
						  <div id="cp1" width="1%" <?php if($qry_cert[0]['cert_pays_code_id']==5 || !$qry_cert[0]['cert_pays_code_id']){ echo 'style="display: inline;"'; } else{ echo 'style="display: none;"'; } ?>>
						  <input type="text" name="cp" id="cp" value="<?php echo htmlentities($qry_cert[0]['cert_cp']) ?>" size="5" maxlength="5" class="form_ediht_Certifies" onkeyUp="TestVille()" onblur="charge_code(document.getElementById('ville').options[document.getElementById('ville').selectedIndex].value)">
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						  </div>
						  <div id="cp2" width="1%" <?php if($qry_cert[0]['cert_pays_code_id']==5){ echo 'style="display: none;"'; } else{ echo 'style="display: inline;"'; } ?>>
						  <input type="text" name="cp_etr" id="cp_etr" value="<?php echo htmlentities($qry_cert[0]['cert_cp']) ?>" size="5" maxlength="5" class="form_ediht_Certifies">
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						  </div></td>
						  <td class="TX"><?php echo $t_ville ?>*&nbsp;:&nbsp;</td>
						  <td class="TX">
						 <div id="ville2" width="1%" style="float: left;display: inline;"><input type="hidden" id="ville" name="ville" class="form_ediht_Certifies" value="<?php echo htmlentities($qry_cert[0]['cert_ville']) ?>"><?php echo htmlentities($qry_cert[0]['cert_ville']) ?></div>
						  <div>
							  <input type="checkbox" name="saisie_ville_m" id="saisie_ville_m"  onclick="doSaisieLibre()" style="display: none;float: left;    margin-left: 25px;margin-top: 5px;">
							  <span id="saisie_ville_m_lab" style="display: none;line-height: 20px;" >&nbsp;&nbsp;Saisie libre</span>
						  </div>
						 
						<div id="ville3" style="display: none;">
							<input type="text" name="ville_etr" value="<?php echo htmlentities($qry_cert[0]['cert_ville']) ?>" maxlength="255" class="form_ediht_Certifies">
						</div>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_fax ?> : </td>
						<td class="TX"> 
						  <input type="text" name="fax" size="40" class="form_ediht_Certifies" maxlength="25" value="<?php echo htmlentities($qry_cert[0]['cert_fax']) ?>">
						</td>
						<td class="TX"><?php echo $t_tel_port ?> :</td>
						<td class="TX"> 
						  <input type="text" name="tel_p" size="40" class="form_ediht_Certifies" maxlength="25" value="<?php echo htmlentities($qry_cert[0]['cert_portable']) ?>">
						</td>
					  </tr>

					  <?php
					  if($qry_cert[0]['cert_cand_id']!=''){
						$sql_opr = "SELECT LAST_OPR FROM CANDIDAT WHERE CAND_ID='".txt_db($qry_cert[0]['cert_cand_id'])."'";
						$qry_opr = $db->query($sql_opr);
						  ?>
						  <tr> 
							<td class="TX" height="40"><?php echo $t_last_opr ?> :</td>
							<td class="TX"><?php echo $qry_opr[0]['last_opr'] ?></td>
							<td class="TX">&nbsp;</td>
							<td class="TX">&nbsp; </td>
						  </tr>
						  <?php
					  }
					  ?>
					  <tr> 
						<td class="TX" height="40"><?php echo $t_fiche_cand_assoc ?> : </td>
						<td class="TX"> 
						  <select name="select_candidat" class="form_ediht_Certifies">
						  <?php
						  /* Si  */
						  if(is_array($qry_cand_assoc)){
						  echo '<option value="">---</option>';
							  foreach($qry_cand_assoc as $cand_assoc){
								unset ($selected_cand_assoc);
								if($cand_assoc['cand_id'] == $qry_cert[0]['cert_cand_id']){
									$selected_cand_assoc = ' selected="selected"';
								}
								echo '<option value="'.$cand_assoc['cand_id'].'"'.$selected_cand_assoc.'>'.ucfirst($cand_assoc['cli_nom']).'> '.htmlentities($cand_assoc['cand_nom']).' '.htmlentities($cand_assoc['cand_prenom']).'</option>';
							  }
						  }
						  ?>
						  </select>
						</td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp; </td>
					  </tr>
	
	
					  <tr> 
						<td class="TX" height="40"><?php echo $t_info1 ?> :</td>
						<td class="TX"> 
						  <input type="text" name="comm1" size="40" class="form_ediht_Certifies" maxlength="40" value="<?php echo htmlentities($qry_cert[0]['cert_info1']) ?>">
						</td>
						<td class="TX"><?php echo $t_info2 ?> :</td>
						<td class="TX"> 
						  <input type="text" name="comm2" size="40" class="form_ediht_Certifies" maxlength="40" value="<?php echo htmlentities($qry_cert[0]['cert_info2']) ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp; </td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					</table>
				  </td>
				  <td width="14"></td>
				</tr>

				<tr> 
				  <td width="14"></td>
				  <td align="center" colspan="2" valign="top" class="TX">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				
				<tr> 
				  <td width="14"></td>
				  <td colspan="2" align="left" valign="top" class="TX"> 
					<table border="0" cellspacing="0" cellpadding="0">
					<?php
					if (is_array($qry_certif_list)){
						?>
						<tr>
							<td colspan="2"> 
							<div id="tablo_certif_dispo">
							  <table width="600" border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000">
								<tr> 
								  <td class="TX_bold">&nbsp;<?php echo $t_fiche_certif_list ?></td>
								  <td class="TX_bold" align="center"><?php echo $t_date_perfection ?></td>
								  <td class="TX_bold" align="center"><?php echo $t_date_certif ?></td>
								  <td class="TX_bold" align="center"><?php echo $t_en_formation ?></td>
								  <td class="TX_bold" align="center"><?php echo $t_suspendu ?></td>
								</tr>
								<?php
								if (is_array($qry_cert_certif_list)){
									foreach ($qry_cert_certif_list as $cert_certif_list){
									?>
									<tr bgcolor="F1F1F1"> 
									  <td><input type="hidden" name="liste_certif[]" value="<?php echo $cert_certif_list['certif_id'] ?>"><?php echo $cert_certif_list['code_libelle'] ?></td>
									  <td align="center"> 
										<input type="text" name="<?php echo $cert_certif_list['certif_id'] ?>_JJ" id="<?php echo $cert_certif_list['certif_id'] ?>_JJ" size="2" maxlength="2" class="form_ediht_Certifies" value="<?php echo $cert_certif_list['jj'] ?>" onkeyup="pass(1, <?php echo $cert_certif_list['certif_id'] ?>)">
										/ 
										<input type="text" name="<?php echo $cert_certif_list['certif_id'] ?>_MM" id="<?php echo $cert_certif_list['certif_id'] ?>_MM" size="2" maxlength="2" class="form_ediht_Certifies" value="<?php echo $cert_certif_list['mm'] ?>" onkeyup="pass(2, <?php echo $cert_certif_list['certif_id'] ?>)">
										/ 
										<input type="text" name="<?php echo $cert_certif_list['certif_id'] ?>_AAAA" id="<?php echo $cert_certif_list['certif_id'] ?>_AAAA" size="4" maxlength="4" class="form_ediht_Certifies" value="<?php echo $cert_certif_list['aaaa'] ?>" onkeyup="pass(3, <?php echo $cert_certif_list['certif_id'] ?>)">
									  </td>
									  <td align="center"> 
										<input type="text" name="<?php echo $cert_certif_list['certif_id'] ?>_JJ2" id="<?php echo $cert_certif_list['certif_id'] ?>_JJ2" size="2" maxlength="2" class="form_ediht_Certifies" value="<?php echo $cert_certif_list['jj2'] ?>" onkeyup="pass(4, <?php echo $cert_certif_list['certif_id'] ?>)">
										/ 
										<input type="text" name="<?php echo $cert_certif_list['certif_id'] ?>_MM2" id="<?php echo $cert_certif_list['certif_id'] ?>_MM2" size="2" maxlength="2" class="form_ediht_Certifies" value="<?php echo $cert_certif_list['mm2'] ?>" onkeyup="pass(5, <?php echo $cert_certif_list['certif_id'] ?>)">
										/ 
										<input type="text" name="<?php echo $cert_certif_list['certif_id'] ?>_AAAA2" id="<?php echo $cert_certif_list['certif_id'] ?>_AAAA2" size="4" maxlength="4" class="form_ediht_Certifies" value="<?php echo $cert_certif_list['aaaa2'] ?>">
									  </td>
									  <td align="center"> 
										<input type="checkbox" name="<?php echo $cert_certif_list['certif_id'] ?>_formation" value="1" <?php if ($cert_certif_list['certif_formation']=='1'){ echo 'checked="checked"';}?>>
									  </td>
									  <td align="center"> 
										<input type="checkbox" name="<?php echo $cert_certif_list['certif_id'] ?>_suspendu" value="1" <?php if ($cert_certif_list['certif_suspendu']=='1'){ echo 'checked="checked"';}?>>
									  </td>
									</tr>
									<?php
									}
								}else{
								echo '<tr bgcolor="F1F1F1"><td colspan="5" align="center" class="TX">'.$t_aucune_operation_cert.'</td></tr>';
								}
								?>
							  </table>
							  </div>
							</td>
						  </tr>
						  <?php
					  }
					  ?>
					  <tr> 
						<td class="TX" valign="top">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  <?php
					  if (is_array($qry_part_list)){
						  ?>
						  <tr> 
							<td class="TX" valign="top"><?php echo $t_fiche_cert_transfert ?> 
							  :&nbsp;&nbsp;&nbsp; </td>
							<td class="TX"> 
							  <select name="select_transfert" class="form_ediht_Certifies">
								<option value="0">---<?php echo $t_fiche_ne_pas_transf ?>---</option>
								<?php
								foreach($qry_part_list as $part){
									echo '<option value="'.$part['part_id'].'">'.htmlentities($part['part_nom']).' '.htmlentities($part['part_rs']).'</option>';
								}
								?>
							  </select>
							</td>
						  </tr>
						  <?php
					  }
					  ?>
					</table>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
			  <br>
			  <table border="0" cellspacing="0" cellpadding="0" align="center" width="900">
				<tr> 
				  <td align="center"> 
					<input type="hidden" name="certid" value="<?php echo $qry_cert[0]['cert_id'] ?>">
					<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_certifie" onClick="verif();">
				  </td>
				</tr>
			  </table>
			</form>
			</body>
			</html>
		<?php
		}
	}
}else{
	include('no_acces.php');
}
?>
