<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']>1){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	/* Params a faire passer : cand_id, ope_id, dossier_id(396099) et prod_id(395574) */
	$db = new db($conn);
	
	if($_GET['candid']>0 && $_GET['opeid']>0 && $_GET['prodid']>0 && $_GET['dossid']>0 && $_GET['certid']>0){
		/* On génère la liste de tous les certifiés du partenaire */
		if($_SESSION['droit']>5){
			$sql_liste_certifies = "SELECT CERTIFIE.CERT_ID, CERTIFIE.CERT_NOM, CERTIFIE.CERT_PRENOM FROM CERTIFIE WHERE CERTIFIE.CERT_DATE_SUPPRESSION IS NULL ORDER BY CERTIFIE.CERT_NOM, CERTIFIE.CERT_PRENOM";
		}else{
			$sql_liste_certifies = "SELECT CERTIFIE.CERT_ID, CERTIFIE.CERT_NOM, CERTIFIE.CERT_PRENOM FROM CERTIFIE WHERE CERTIFIE.CERT_PART_ID=".$_SESSION['part_id']." AND CERTIFIE.CERT_DATE_SUPPRESSION IS NULL AND ORDER BY CERTIFIE.CERT_NOM, CERTIFIE.CERT_PRENOM";
		}
		//echo $sql_liste_certifies;
		$qry_liste_certifies = $db->query($sql_liste_certifies);
	
		/* On sélectionne les infos de l'opération */
		$sql_info_ope = "SELECT TYPE_OPE_LIBELLE FROM TYPE_OPERATION WHERE TYPE_OPE_ID=".intval($_GET['opeid'])."";
		$qry_info_ope = $db->query($sql_info_ope);
		
		/* On sélectionne les infos du candidat */
		$sql_info_cand = "SELECT CAND_PRENOM, CAND_NOM FROM CANDIDAT WHERE CAND_ID=".intval($_GET['candid'])."";
		$qry_info_cand = $db->query($sql_info_cand);
		
		/* On sélectionne les infos du dossier */
		$sql_info_doss = "SELECT DOSSIER_NOM FROM DOSSIER WHERE DOSSIER_ID=".intval($_GET['dossid'])."";
		$qry_info_doss = $db->query($sql_info_doss);
		
		/* On génère la liste des documents qui contiennent au moins un compte rendu à saisir */
		$sql_sel_doc = "SELECT DOCUMENT.DOC_ID, PRODUIT_A_DOC.TRI, DOC_A_INFO.NUM_ZONE, DOC_A_INFO.DOC_NOM_CR,DOC_A_INFO.DOC_TYPE_CR
FROM PRODUIT_A_DOC,DOCUMENT,DOC_A_INFO
WHERE PRODUIT_A_DOC.DOC_ID = DOCUMENT.DOC_ID AND DOCUMENT.DOC_ID = DOC_A_INFO.DOC_ID
and prod_id=".intval($_GET['prodid'])." and dossier_id=".intval($_GET['dossid'])." AND (DOCUMENT.TYPE_ZONE1=6 OR DOCUMENT.TYPE_ZONE2=6 OR DOCUMENT.TYPE_ZONE3=6 OR DOCUMENT.TYPE_ZONE4=6)";
		//echo $sql_sel_doc;
		$qry_sel_doc = $db->query($sql_sel_doc);
		
		if($_GET['docid']>0 && $_GET['num_zone']>0 && $_GET['certid']>0){
			/* Sélection du type de cr à saisir */
			$sql_type_cr = "SELECT DOC_TYPE_CR FROM DOC_A_INFO WHERE DOC_ID='".intval($_GET['docid'])."' AND NUM_ZONE='".intval($_GET['num_zone'])."'";
			$qry_type_cr = $db->query($sql_type_cr);
			
			/* On sélectionne le compte-rendu saisi */
			$sql_sel_txt_cr = "SELECT TXT_CR FROM CAND_A_CR WHERE CAND_ID=".$_GET['candid']." AND OPE_ID=".$_GET['opeid']." AND DOC_ID=".$_GET['docid']." AND NUM_ZONE=".$_GET['num_zone']."";
			$qry_sel_txt_cr = $db->query($sql_sel_txt_cr);
		}
	}
	
	if($_POST['step']>0){
	
			$temp_val = explode('_',$_POST['select_doc']);
			$temp_docid = $temp_val[0];
			$temp_numzone = $temp_val[1];
		
		switch ($_POST['step']){
			case 1:
				/* On a choisi le document */
				header('location:edit_cr.php?candid='.$_GET['candid'].'&opeid='.$_GET['opeid'].'&prodid='.$_GET['prodid'].'&certid='.$_POST['select_certifie'].'&JJ_cr='.$_POST['JJ_cr'].'&MM_cr='.$_POST['MM_cr'].'&AAAA_cr='.$_POST['AAAA_cr'].'&dossid='.$_GET['dossid'].'&docid='.$temp_docid.'&num_zone='.$temp_numzone);
			break;					
			case 3:
			/* On a posté le compte rendu */
				/* On insère le compte-rendu en base */
				
				$sql_del_cr = "DELETE FROM CAND_A_CR WHERE CAND_ID=".intval($_GET['candid'])." AND OPE_ID=".intval($_GET['opeid'])." AND DOC_ID=".intval($_GET['docid'])." AND NUM_ZONE=".intval($_GET['num_zone'])."";
				$qry_del_cr=$db->query($sql_del_cr);
				
				$sql_insert_cr = "INSERT INTO CAND_A_CR (CAND_ID, OPE_ID, DOC_ID, NUM_ZONE, NUM_LIGNE, THEME, TXT_CR, CERT_ID, DATE_CR) 
				VALUES(".intval($_GET['candid']).", 
				".intval($_GET['opeid']).", 
				".intval($_POST['select_doc']).", 
				".intval($_POST['select_zone']).", 
				SEQ_ID.NEXTVAL, 
				".intval($_POST['theme']).", 
				'".txt_db($_POST['txt_cr'])."',
				'".intval($_GET['certid'])."',
				TO_DATE('".txt_db($_GET['JJ_cr']).txt_db($_GET['MM_cr']).txt_db($_GET['AAAA_cr'])."','DDMMYYYY'))";
				$qry_insert_cr=$db->query($sql_insert_cr);
				echo $sql_insert_cr;
				//header('location:edit_cr.php?candid='.$_GET['candid'].'&opeid='.$_GET['opeid'].'&prodid='.$_GET['prodid'].'&certid='.$_GET['certid'].'&JJ_cr='.$_GET['JJ_cr'].'&MM_cr='.$_GET['MM_cr'].'&AAAA_cr='.$_GET['AAAA_cr'].'&dossid='.$_GET['dossid'].'&docid='.$_GET['docid'].'&num_zone='.$_GET['num_zone'].'&idnc='.($_GET['idnc']+1));
			break;
		}
	}

	?>
	<html>
		<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>
			<script language="JavaScript">
			<!--
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			//-->
			
			window.onload = function(){
				tinyMCE.init({
						// General options
						mode : "exact",
						elements : "txt_cr_libre",						
						theme : "advanced",
						plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

						// Theme options
						theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
						theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
						theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
						theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						theme_advanced_resizing : false,

						// Example content CSS (should be your site CSS)
						content_css : "../css/import.css",

						// Drop lists for link/image/media/template dialogs
						template_external_list_url : "lists/template_list.js",
						external_link_list_url : "lists/link_list.js",
						external_image_list_url : "lists/image_list.js",
						media_external_list_url : "lists/media_list.js",

						// Replace values for the template plugin
						template_replace_values : {
							username : "Some User",
							staffid : "991234"
						}
					});
			}
			function verif(){
				error = '';
				if (parseInt(document.form.nbre.value) > parseInt(document.form.max.value) || parseInt(document.form.nbre.value) < parseInt(document.form.min.value)){
					error += "<?php echo $t_nbre_jet_inf_egal ?> "+ parseInt(document.form.max.value) +" <?php echo $t_nbre_jet_sup_egal ?> "+ parseInt(document.form.min.value) +"\n";
				}
				
				if (error!=''){
					alert(error);
				}else{
					document.form.submit();
				}
			}
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			//-->
			</script>
		</head>
		<body bgcolor="#FFFFFF" text="#000000">
		<table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;SAISIE D'UN COMPTE-RENDU</td>
			</tr>
		</table>
		<form method="post" action="#" name="form1">
		  <table width="700" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr>
			<td width="14"></td>
			<td class="TX_Candidats">
				<table width="600" cellpadding="2" cellspacing="0" border="0">
				<tr><td class="TX" width="150">Date&nbsp;de&nbsp;compte-rendu&nbsp;:&nbsp;</td><td style="text-align: left;"><input class="form_ediht_Candidats" type="text" name="JJ_cr" size="2" maxlength="2" value="<?php echo date('d') ?>">&nbsp;<input class="form_ediht_Candidats" type="text" name="MM_cr" size="2" maxlength="2" value="<?php echo date('m') ?>">&nbsp;<input class="form_ediht_Candidats" type="text" name="AAAA_cr" size="4" maxlength="4" value="<?php echo date('Y') ?>"></td></tr>
					<tr><td class="TX" style="text-align: left;" colspan="2">&nbsp;</td></tr>
					<tr><td class="TX" style="text-align: left;">Certifi&eacute;s&nbsp;:&nbsp;</td><td style="text-align: left;">
						<select name="select_certifie" class="form_ediht_Candidats">
							<?php
							foreach($qry_liste_certifies as $elmt_certifie){
								unset($selected_certifie);
								if($elmt_certifie['cert_id']==$_GET['certid']){
									$selected_certifie = ' selected="selected"'; 
								}
								echo '<option value="'.$elmt_certifie['cert_id'].'"'.$selected_certifie.'>'.htmlentities($elmt_certifie['cert_nom']).' '.htmlentities($elmt_certifie['cert_prenom']).'</option>';
							}
							?>					
						</select>
					</td></tr>
				</table>
			</td>
			<td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
			</table>
			<br><br>
			
			<table width="700" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="TX">
			  <table width="600" cellpadding="2" cellspacing="0" border="0">
				<tr><td class="TX" width="150">Choix du document&nbsp;:&nbsp;</td>
				<td style="text-align: left;">
				<?php
				/* Si on a trouvé au moins un document qui contient au moins un compte-rendu à saisir */
				if(is_array($qry_sel_doc)){
					?>
						<input type="hidden" name="step" value="1"><select name="select_doc" class="form_ediht_Candidats">
							<?php 
							foreach($qry_sel_doc as $doc_liste){
									unset($selected_doc);
									$temp_str = $_GET['docid'].'_'.$_GET['num_zone'];
									if($temp_str==$doc_liste['doc_id'].'_'.$doc_liste['num_zone']){
										$selected_doc = ' selected="selected"';
									}
								echo '<option value="'.$doc_liste['doc_id'].'_'.$doc_liste['num_zone'].'"'.$selected_doc.'>'.htmlentities($doc_liste['doc_nom_cr']).'</option>';
							}
							?>
						</select>&nbsp;<input type="submit" class="bn_ajouter" value="Sélectionner">
					<?php
				}
				?>
				</td>
				</tr>
			  </table>
			  </td>
			  <td width="14"></td>
			</tr>
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		</table>
		</form>
			<br><br>
		<?php
		if($_GET['docid']>0 && $_GET['num_zone']>0){
			?>
		  <table width="700" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="TX_Candidats">
			  Compte rendu du candidat <?php echo htmlentities($qry_info_cand[0]['cand_prenom']). ' ' . htmlentities($qry_info_cand[0]['cand_nom']) ?> 
			  <?php
			  if(is_array($qry_info_ope)){
				?>
				pour l'op&eacute;ration <?php echo htmlentities($qry_info_ope[0]['type_ope_libelle']) ?> 
				<?php
			  }
			  ?>
			  du dossier <?php echo htmlentities($qry_info_doss[0]['dossier_nom']) ?></td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="center" valign="middle" class="TX">&nbsp;</td>
			  <td width="14"></td>
			</tr>
				<form action="#" method="post" name="form3">
					<tr> 
					  <td width="14"></td>
					  <td align="center" valign="middle" class="TX">&nbsp;</td>
					  <td width="14"></td>
					</tr>
					<tr> 
					  <td width="14"></td>
					  <td align="center" valign="middle" class="TX">
						  <input type="hidden" name="select_doc" value="<?php echo $_GET['docid'] ?>">
						  <input type="hidden" name="select_zone" value="<?php echo $_GET['num_zone'] ?>">
						  <input type="hidden" name="step" value="3">
					  <?php
						switch ($qry_type_cr[0]['doc_type_cr']){
							
							case 47 : //Themes
							case 48 : //Themes et Images					
							case 49 : //Images
							?>
							<table align="left" cellpadding="0" cellspacing="0" border="0">
							  <tr>
								<td class="TX" valign="top" width="150">
								  Thème&nbsp;:&nbsp;</td><td class="TX"><textarea cols="70" rows="3" name="txt_cr_theme" class="form_ediht_Candidats" style="height: 50px;"><?php echo htmlentities($qry_sel_txt_cr[0]['txt_cr']) ?></textarea>
								</td>
							  </tr>
							  <tr><td colspan="2">&nbsp;</td></tr>
							  <tr>
								<td class="TX" valign="top">
								Commentaire&nbsp;:&nbsp;</td><td class="TX"><textarea cols="70" rows="10" name="txt_cr_comm" class="form_ediht_Candidats" style="height: 250px;"><?php echo htmlentities($qry_sel_txt_cr[0]['txt_cr']) ?></textarea>
								</td>
							  </tr>
							</table>
							<?php
							break;
							case 50 : //Libre
							  ?>
							  <textarea name="txt_cr_comm" id="txt_cr_libre"><?php echo htmlentities($qry_sel_txt_cr[0]['txt_cr']) ?></textarea>
							<?php
							break;
						}
						?>
					  </td>
					  <td width="14"></td>
					</tr>
					<tr> 
					  <td width="14"></td>
					  <td align="center" valign="top" class="TX">&nbsp;</td>
					  <td width="14"></td>
					</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
		</table>
		<br>
		<table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td class="TX" align="center">
				<input type="submit" value="AJOUTER" class="bn_valider_candidat">
			  </td>
			</tr>
		</table>
		</form>
		<?php
		}
		?>
	</body>
	</html>
	<?php
	}else{
	include('no_acces.php');
}
?>
