<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VAKOM | Rapport BPM</title>
    <!-- Bootstrap -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="styles-rapport.css" rel="stylesheet">	  	  
  </head>
  <body>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
	<script src="js/jquery-1.11.3.min.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed --> 
	<script src="js/bootstrap.js"></script>
  	<div class="container">		  
		<div class="entete"><img src="img/logo-bpm.png" width="279" height="90" alt=""/></div>			  
	</div>  
<?php 

session_start();

include ("../config/lib/connex.php");
include ("../config/lib/db.oracle.php");

$db = new db($conn);

$opeid = $_GET['opeid'];

if (isset($opeid)){

	$manger_nom ="";
	$mangerplusun_nom ="";
	$manger_cand_id = null;
	$nb_personnes_mg=-2;

	$sql_select_question = "SELECT distinct CAND_ID FROM CAND_A_QUEST WHERE OPE_ID = ".txt_db(intval($opeid));
    $condidats   	 = $db->query($sql_select_question);
	    
    foreach($condidats as $condidat){
		$sql_infos_cand = "SELECT CANDIDAT.*,CAND_A_OPE.* FROM CANDIDAT, CAND_A_OPE WHERE CAND_A_OPE.CAND_ID = CANDIDAT.CAND_ID AND CANDIDAT.CAND_ACTIF='1' AND CANDIDAT.CAND_ID='".txt_db(intval($condidat['cand_id']))."'  AND CAND_A_OPE.OPE_ID=".txt_db(intval($opeid))."";
		$infos_candidat = $db->query($sql_infos_cand);            
        if ($infos_candidat[0]['niveau'] == 1){
			$manger_cand_id = $condidat['cand_id'];
        	$manger_nom = $infos_candidat[0]['cand_prenom'] . ' '. $infos_candidat[0]['cand_nom'];
			$cand_fonction = $infos_candidat[0]['cand_fonction'];
        }
        if ($infos_candidat[0]['niveau'] == 2){
        	$mangerplusun_nom = $infos_candidat[0]['cand_prenom'] . ' '. $infos_candidat[0]['cand_nom'];        	
        }        
		$nb_personnes_mg++;
    }
    
	$tab = getData($db,$opeid,0, null);
	getData($db,$opeid,1, $tab);
	getData($db,$opeid,2, $tab);
		
}

function getData($db,$opeid,$level,$tab)
{
	$sql_query = "select theme,stheme,round((sum(to_number(txt_libre))/count(choix_id))*10,2) note from reponse_bpm where ope_id=".$opeid." and niveau=".$level." group by theme,stheme order by theme,stheme";
	$qry_query = $db->query($sql_query);

	$themes = array();
	$sthemes = array();
	$objects = array();

	$index = 0;

	foreach($qry_query as $query)
	{
		$theme = $query['theme'];
		$stheme = $query['stheme'];
		$note = $query['note']; 
	
		if (in_array($theme, $themes)) 
		{
			if($level != 0){
				$sth = getObjectStheme($tab, $stheme);				
			} else {
				$sth = new stdClass;
				$sth->stheme = $stheme;			
			}
			
			if ($level == 0)	$sth->collaborateur = $note;
			if ($level == 1)	$sth->manager = $note;
			if ($level == 2)	$sth->nplus1 = $note;			
			
			array_push($sthemes, $sth);
		
			if($qry_query[$index+1]['theme'] != $theme)
			{			
				$std = new stdClass;
				$std->theme = $theme;
				$std->max = count($sthemes);
				$std->sthemes = $sthemes;
				array_push($objects, $std);
			}
		} else {				
			$sthemes = array();
			array_push($themes, $theme);
			if($level != 0){
				$sth = getObjectStheme($tab, $stheme);				
			} else {
				$sth = new stdClass;
				$sth->stheme = $stheme;			
			}
	
			if ($level == 0)	$sth->collaborateur = $note;
			if ($level == 1)	$sth->manager = $note;
			if ($level == 2)	$sth->nplus1 = $note;

			array_push($sthemes, $sth);
		}
		$index++;	
	}
	return $objects;
} 

function getObjectStheme($tab, $stheme)
{
	foreach($tab as $ligne)
 	{
	foreach($ligne->sthemes as $obj)
	{
		if($obj->stheme == $stheme)
		{
			return $obj;
		}	
	}	
 }
}
?>	 
<div class="container">
	  
  <p class="theme">Document de pr&Eacute;paration &Agrave; l'entretien</p>		   
  <table class="tableAnalyse">
  <tbody>
    <tr>
      <td>Nom et pr&eacute;nom de la personne &eacute;valu&eacute;e</td>
      <td><?php echo$manger_nom?></td>
      </tr>
    <tr>
      <td>Fonction</td>
      <td><?php echo$cand_fonction?></td>
      </tr>
    <tr>
      <td  >Nombre personnes manag&eacute;es</td>
      <td ><?php echo$nb_personnes_mg?></td>
      </tr>
    <tr>
      <td >Nom du manager de la personne &eacute;valu&eacute;e</td>
      <td ><?php echo$mangerplusun_nom?></td>
      </tr>
    </tbody>
</table>

	  <p class="item strong text-center">ITEMS</p>
		<p style="text-align: center;border: 1px solid #000"><img style="width: 100%;" src="http://www.lesensdelhumain.com/preprod/public/graph_generator/<?php echo$opeid?>_1.png"  alt=""/></p>   
		   
	 <p class="item strong text-center">Sous-th&egrave;mes</p>
		 <p style="text-align: center;border: 1px solid #000 "><img style="width: 100%;" src="http://www.lesensdelhumain.com/preprod/public/graph_generator/<?php echo$opeid?>_2.png"  alt=""/></p> 
		 		   
	     <p>&nbsp;</p>
		   
<?php		   

	$table = "<table class='tableResultAnalyse'><tbody><tr>";
    $table .= "<th>Th&egrave;me</th>";
	$table .= "<th>Sous-Th&egrave;mes</th>";
    $table .= "<th style='background-color: #0b2e43; width: 12%' >Auto &eacute;valuation</th>";
	$table .= "<th style='background-color: #3785b3; width: 12%' >N+1</th>";
	$table .= "<th style='background-color: #46b0ef; width: 12%'>Ensemble collaborateurs</th></tr>";
	
    foreach($tab as $obj){
    	$index_theme = 0;    
	    foreach($obj->sthemes as $stheme)
	    {
	    	//rowspan="$obj->max"
	    	$table .= '<tr>';
		    if ($index_theme == 0){
	    	  	$table .= '<td rowspan="'.$obj->max.'" style="width: 22.7%;" class="center">'.$obj->theme.'</td>';		    
		    } 
		    
		    $table .= '<td>'.$stheme->stheme.'</td>';
		    $table .= '<td class="center" >'.$stheme->manager.'%</td>';
		    $table .= '<td class="center" >'.$stheme->nplus1.'%</td>';
    		$table .= '<td class="center" >'.$stheme->collaborateur.'%</td>';		    
	      	$table .= '</tr>';
	      	$index_theme++;
	    }
	}
	      $table .= '</tbody></table>';	
?>	    
<?php echo$table?>
  </div><!-- FIN CONTAINER -->
  </body>
</html>