<?php
session_start();
	// Si l'utilisateur est un super admin

if ($_SESSION['droit']>1){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);

	switch($_SESSION['droit']){
		case 2:
			$prefixe		= 'admcertif';
			$str_colspan	= 1;
			$str_part_id 	= $_SESSION['part_id'];
			$where_cert 	= " AND CERT_ID=".intval($_SESSION['cert_id'])." ";
		break;
		case 4:
			$prefixe		= 'admsocvak';
			$str_part_id 	= $_SESSION['part_id'];
			$where_cert 	= '';
		break;
		case 6:
		case 9:
			$prefixe		= 'admvak';
			$str_part_id 	= $_GET['partid'];
			$where_cert 	= '';
		break;
	}
	
	switch($_GET['order_cert']){		
		case 'prenom':
			$order_cert = 'ORDER BY CERT_PRENOM ASC, CERT_NOM ASC';
			$order_part = 'ORDER BY PART_NOM,PART_RS ASC';
			break;
		case 'nom':
		default:
			$order_cert = 'ORDER BY CERT_NOM ASC, CERT_PRENOM ASC';
			$order_part = 'ORDER BY PART_NOM,PART_RS ASC';
			break;		
		case 'part':
			$order_cert = 'ORDER BY PART_NOM , CERT_NOM ASC';
			$order_part = 'ORDER BY PART_NOM,PART_RS ASC';
			break;
	}
	
	if ($_POST['submit']){
		/* Si la personne a soumis le formulaire */
		if (trim($_POST['search_part'])!='' || trim($_POST['search_cert'])!=''){
			$str_part	= stripslashes($_POST['search_part']);
			$str_cert	= stripslashes($_POST['search_cert']);
			//echo $str_cert;
			$where1 	= "AND LOWER(concat(concat(PART_NOM,' '),PART_RS)) = '".txt_db(strtolower($str_part))."' ";
			
			$sql_part_list1 = "SELECT * FROM PARTENAIRE, CODE WHERE CODE.CODE_TABLE='NATURE' AND CODE.CODE_ID=PARTENAIRE.PART_NATURE_CODE_ID ".$where1." ".$order_part."";
			$qry_part_list1 = $db->query($sql_part_list1);
			//echo $sql_part_list1;
			if(is_array($qry_part_list1)){
				header('location:certifies.php?partid='.$qry_part_list1[0]['part_id']);
			}else{			
				header('location:certifies.php?str_part='.$str_part.'&str_cert='.$str_cert);
			}
		}else{
			header('location:certifies.php?str_part=&str_cert=');
		}
	}
	
		if ($_GET['actif']=='1' || !isset($_GET['actif'])){
			$sql_contact_actif = " AND ACTIF='1'";
		}
		
		if($_SESSION['droit']>5){
			if (isset($_GET['str_part'])){
				$where 		= "AND LOWER(concat(concat(PART_NOM,' '),PART_RS)) LIKE '".txt_db(strtolower($_GET['str_part']))."%' ";
				$str_part	= $_GET['str_part'];
				$str_cert	= $_GET['str_cert'];

				$sql_part_list = "SELECT * FROM PARTENAIRE, CODE WHERE CODE.CODE_TABLE='NATURE' AND CODE.CODE_ID=PARTENAIRE.PART_NATURE_CODE_ID ".$where." ".$order_part."";
				//echo $sql_part_list;
				$qry_part_list = $db->query($sql_part_list);
			}
		}else{
			$sql_part_list = "SELECT * FROM PARTENAIRE, CODE WHERE CODE.CODE_TABLE='NATURE' AND CODE.CODE_ID=PARTENAIRE.PART_NATURE_CODE_ID WHERE PART_ID=".intval($_SESSION['part_id']);
			//echo $sql_part_list;
			$qry_part_list = $db->query($sql_part_list);
		}
		
		if(is_array($qry_part_list)){
			// Si il n'y a qu'un résultat
			if(!is_array($qry_part_list[1])){
				header('location:certifies.php?partid='.$qry_part_list[0]['part_id']);
			}else{
				$multi_rep = true;
			}
		}
	
	if ($_GET['action']=='delete' && $str_part_id>0 && $_GET['contact']>0 && $_SESSION['droit']>5){
		$sql_delete_contact = "UPDATE CERTIFIE SET CERT_DATE_SUPPRESSION=SYSDATE, CERT_USER_SUPPRESSION_ID='".$_SESSION['vak_id']."' WHERE CERT_ID='".txt_db($_GET['contact'])."' ";
		$qry_delete_contact = $db->query($sql_delete_contact);
		header('location:certifies.php?partid='.$str_part_id);
	}
	

	if ($str_part_id!=''){
		$sql_part = "SELECT * FROM PARTENAIRE WHERE PART_ID='".txt_db($str_part_id)."'";
		$qry_part = $db->query($sql_part);
	
		$sql_cert = "SELECT * FROM CERTIFIE WHERE CERT_PART_ID='".txt_db($str_part_id)."' ".$where_cert." AND CERT_DATE_SUPPRESSION IS NULL".$sql_contact_actif." ".$order_cert."";
			//echo $sql_cert;
		$qry_cert = $db->query($sql_cert);
	}else{
		if($multi_rep){
			if (isset($_GET['str_part']) || isset($_GET['str_cert'])){
				$where = "AND LOWER(concat(concat(PART_NOM,' '),PART_RS)) LIKE '".txt_db(strtolower($_GET['str_part']))."%' AND LOWER(CERT_NOM) LIKE '".txt_db(strtolower($_GET['str_cert']))."%' ";
			}
			$sql_cert = "SELECT * FROM CERTIFIE, PARTENAIRE WHERE CERT_PART_ID=PART_ID ".$where." AND CERT_DATE_SUPPRESSION IS NULL".$sql_contact_actif." ".$order_cert."";
			//echo $sql_cert;
			$qry_cert = $db->query($sql_cert);
		}
	}
	//echo $sql_cert;
	if(!isset($_GET['actif'])){
		$query_str = $_SERVER['QUERY_STRING'];
	}else{
		$query_str = substr($_SERVER['QUERY_STRING'], 0, (strlen($_SERVER['QUERY_STRING'])-8));
	}
	?>
	<html>
	<head>
	<title>Vakom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<link rel="stylesheet" href="../css/style.css" type="text/css">	
	<script language="JavaScript">
	<!--
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}

	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	
	function c_delete(x,y){
		if (confirm('<?php echo $t_supprimer_certif ?>')){
			document.location.href='certifies.php?partid='+x+'&action=delete&contact='+y+'&actif=<?php echo intval($_GET['actif']) ?>';
		}
	}
	
	function show_actif(){
		if(document.getElementById('actif').checked==true){
			document.location.href='certifies.php?<?php echo $query_str ?>&actif=1';
		}else{
			document.location.href='certifies.php?<?php echo $query_str ?>&actif=0';
		}
	}
	//-->
	</script>
	</head>

	<body bgcolor="#FFFFFF" text="#000000">
		<?php
			$_GET['menu_selected']=3;		
			include("menu_top_new.php");
		?>	
	<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>			
	  <?php
	  if($_SESSION['droit']>5){
	  ?>
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Certifies"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $t_certs1 ?></td>
		</tr>
	  </table>
	  <form method="post" action="certifies.php?idnc=<?php echo $_GET['idnc'] ?>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="fond_tablo_certifies" align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="center" class="TX"> 
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
			  <tr> 
				<td class="TX" align="right">
					<?php echo $t_part_opt_saisir ?>
				</td><td class="TX" align="left">
					<input type="text" name="search_part" size="50" class="form_ediht_Certifies" value="<?php echo htmlentities($_GET['str_part']) ?>">
				  <input type="hidden" name="step" value="1">
				</td>
			  </tr>
			  <tr> 
				<td class="TX" align="right">
					<?php echo $t_cert_opt_saisir ?>			
				</td><td class="TX" align="left">
					<input type="text" name="search_cert" size="50" class="form_ediht_Certifies" value="<?php echo stripslashes($_GET['str_cert']); ?>">
				  <input type="hidden" name="step" value="1">
				</td>
			  </tr>
			 </table>			
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
	  <br>
	  <table cellpadding="0" cellspacing="0" width="961" align="center">
		 <tr><td align="center">
			<input type="submit" name="submit" value="<?php echo $t_btn_3 ?>" class="bn_valider_certifie">
		 </td></tr>
	  </table>
	  </form>
	  <br>
		<?php
		}
		if (isset($_GET['str_part']) || isset($_GET['str_cert']) || isset($str_part_id)){
		?>
			 <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Certifies"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;
				  <?php
				if($str_part_id>0){
					$colspan=(9-$str_colspan);
					if (is_array($qry_part)){

						if($_SESSION['droit']>5){
							echo '<a class="Titre_Certifies" href="#" onClick="MM_openBrWindow(\''. $prefixe .'_edit_client.php?partid='. $qry_part[0]['part_id'] .'\',\'Vakom_Fiche_Partenaire_'. $qry_part[0]['part_id'] .'\',\'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=850,height=800\')">';
						}
						echo htmlentities($qry_part[0]['part_nom'].' '.$qry_part[0]['part_rs']);
						if($_SESSION['droit']>5){
							echo '</a>';
						}
					}
				}else{
					$colspan=(10-$str_colspan);
					if($_GET['str_part'] == '' && $_GET['str_cert'] == ''){
						echo 'TOUS LES CERTIFI&Eacute;S';
					}else{
						if($_GET['str_part'] != ''){
							echo stripslashes($_GET['str_part']);
						}else{
							echo stripslashes($_GET['str_cert']);
						}
					}
				}
				?></td>
				</tr>
			  </table>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="center" class="TX"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="2" class="TX">
					  <tr align="left"> 
						<td colspan="<?php echo $colspan ?>" class="TX_Certifies">CONTACTS</td>
					  </tr>
					  <tr align="left">
						<td colspan="<?php echo $colspan ?>" height="1" bgcolor="#666666"> </td>
					  </tr>
					  <tr align="left"> 
						<td colspan="<?php echo $colspan ?>" class="TX_GD">&nbsp; 
						
						</td>
					  </tr>
					  
					  <tr align="left"> 
						<td colspan="<?php echo ($colspan-3) ?>" class="TX_GD"> 
						<?php
						if ($str_part_id>0 && $_SESSION['droit']>2){
						?>
							<input type="button" name="new_ctact" value="<?php echo $t_btn_ajouter_contact ?>" class="bn_ajouter" onClick="MM_openBrWindow('<?php echo $prefixe ?>_crea_contactClient.php?partid=<?php echo $str_part_id ?>','Creation_Contact','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=600')">
						<?php
						}
						?>
						</td>
						<td colspan="3" class="TX" style="text-align: right;"> 
						<?php
						if($_SESSION['droit']>2){
							?>
							<input type="checkbox" name="actif" id="actif" value="1" onClick="show_actif();" <?php if ($_GET['actif']=='1' || !isset($_GET['actif'])) echo ' checked="checked"' ?>> Certifiés&nbsp;actifs&nbsp;seulement
							<?php
						}	
						?>
						</td>
					 </tr>
						<?php
						if (is_array($qry_cert)){
								?>
							  <tr> 
								<td width='12%' class="TX_bold">
								<a href="certifies.php?partid=<?php echo $str_part_id ?>&str_part=<?php echo $_GET['str_part']; ?>&str_cert=<?php echo $_GET['str_cert']; ?>&order_cert=nom">Nom</a></td>
								<td width='12%' class="TX_bold"><a href="certifies.php?partid=<?php echo $str_part_id ?>&str_part=<?php echo $_GET['str_part']; ?>&str_cert=<?php echo $_GET['str_cert']; ?>&order_cert=prenom">Pr&eacute;nom</a></td>
								<?php
								if ($str_part_id<1){
									?>
									<td width='12%' class="TX_bold"><a href="certifies.php?partid=<?php echo $str_part_id ?>&str_part=<?php echo $_GET['str_part']; ?>&str_cert=<?php echo $_GET['str_cert']; ?>&order_cert=part">Partenaire</a></td>
									<?php
								}
								?>
								<td width='12%' align="center" class="TX_bold">Droits</td>
								<td width='12%' align="left" class="TX_bold">Profil <br/>
								  OPR</td>
								<td width='12%'  class="TX_bold">Niveau de <br/>
								  certification</td>
								<td width='12%' class="TX_bold">Date de <br/>
								  certification</td>
								<td width='12%' class="TX_bold" align="center">Etat</td>
								<td width='12%' class="TX_bold" align="center">Jetons</td>
								<?php
								if($_SESSION['droit']>2){
									?>
									<td width='12%' class="TX_bold" align="center">Statut</td>
									<?php
								}
								?>
								<!--<td class="TX_bold">&nbsp;</td>-->
							  </tr>
							  <tr> 
								<td colspan="<?php echo $colspan ?>" bgcolor="#CCCCCC" height="1" valign="top"></td>
							  </tr>
							  	<?php
								foreach($qry_cert as $cert){
									if ($cert['cert_droit_certifie']=='1'){
										$sql_certif = "SELECT * FROM CERT_A_CERTIF, CODE WHERE CERT_A_CERTIF.CERTIF_CODE_ID=CODE.CODE_ID AND (CERTIF_CERTIFICATION IS NOT NULL OR CERTIF_FORMATION = 1 OR CERTIF_SUSPENDU= 1 ) AND CODE.CODE_TABLE='CERTIFICATION' AND CERT_A_CERTIF.CERTIF_CERT_ID='".txt_db($cert['cert_id'])."' ORDER BY CODE.CODE_LIBELLE ASC";
										//echo $sql_certif;
										$qry_certif = $db->query($sql_certif);
									}
									else
									{
										$qry_certif ='';
									}
								?>
								  <tr> 
									<td class="TX"><a href="#" onClick="MM_openBrWindow('<?php echo $prefixe ?>_edit_contactClient.php?certid=<?php echo $cert['cert_id'] ?>','edit_<?php echo$cert['cert_id']?>','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=1000,height=700')"><?php echo strtoupper($cert['cert_nom']) ?></a></td>
									<td class="TX"><?php echo ucfirst($cert['cert_prenom']) ?></td>
									<?php
									if ($str_part_id<1){
										?>
										<td class="TX"><a href="#" 
							<?php
if($_SESSION['droit']>5){
?>
onClick="MM_openBrWindow('<?php echo $prefixe ?>_edit_client.php?partid=<?php echo $cert['part_id'] ?>','Vakom_Fiche_Partenaire_<?php echo $cert['part_id'] ?>','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=950,height=800')"
<?php
}
?>
><?php echo ucfirst($cert['part_nom'].' '.$cert['part_rs']) ?>
</a></td>
									<?php
									}
									?>
									<td align="center" class="TX">
									<?php
									if ($cert['cert_droit_admin']=='1'){
										echo 'Administrateur';
										if ($cert['cert_droit_certifie']=='1'){
										echo '/<br>';
										}
									}
									if ($cert['cert_droit_certifie']=='1'){
										echo 'Certifié(e)';
									}
									?>
									</td>
									<td align="left" class="TX">
									<?php
									if($cert['cert_cand_id']!=''){
										$sql_opr = "SELECT LAST_OPR FROM CANDIDAT WHERE CAND_ID='".txt_db($cert['cert_cand_id'])."'";
										$qry_opr = $db->query($sql_opr);
										echo $qry_opr[0]['last_opr'];
									}
									?>
									</td>
									<td class="TX">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												echo $certif['code_libelle'].'<br>';
											}
										}
										?>
									</td>
									<td class="TX">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												echo $certif['certif_certification'].'<br>';
											}
										}
										?>
									</td>
									<td class="TX" align="center">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												if ($certif['certif_formation']=='1'){
													echo $t_formation;
												}elseif($certif['certif_suspendu']=='1'){
													echo $t_suspendu;
												}
												echo '<br>';												
											}
										}
										?>
									</td>
									<td class="TX" align="center"><a href="#" onClick="MM_openBrWindow('<?php echo $prefixe ?>_produits_contactClient.php?partid=<?php echo $cert['cert_part_id'] ?>&certid=<?php echo $cert['cert_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes')">Consultez</a></td>
									<?php
									if($_SESSION['droit']>2){
										?>
										<td class="TX" align="center">
										<?php
										if ($cert['actif']=='1'){
											echo '<span style="color:#99cc00; font-weight: bold;">'.$t_actif.'</span>';
										}else{
											echo '<span style="color:#FF0000; font-weight: bold;">'.$t_inactif.'</span>';
										}
										?>
										</td>
										<?php
									}
									?>
								  </tr>
								  <tr> 
									<td colspan="<?php echo $colspan ?>" bgcolor="#CCCCCC" height="1" valign="top"></td>
								  </tr>
								<?php
								}
							}else{
							?>
							<tr align="center"> 
								<td colspan="<?php echo $colspan ?>" class="TX"> 
								<?php echo $t_aucun_resultat ?>
								</td>
							</tr>
							<?php
							}
						?>
					</table>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
		<?php
		}
		?>
</p></div>	</article></div>	</div>	</div>	</div>		
	</body>
	</html>
	<?php
}else{
	include('no_acces.php');
}
?>