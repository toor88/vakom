<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']>5){
	if ($_GET['partid'] && $_GET['partid']>0){

		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");

		$db = new db($conn);
		if (@$_POST['partid']!=''){
			$sql_insert_jet 	= "SELECT SEQ_ID.NEXTVAL SEQ_JET FROM DUAL";
			$qry_insert_jet		= $db->query($sql_insert_jet);
			$set_jet_tmp = $qry_insert_jet[0]['seq_jet'];
			$sql_insert_jet 	= "INSERT INTO JETON VALUES (".$set_jet_tmp.", 
			'".txt_db($_POST['partid'])."', 
			'".txt_db($_POST['liste_prdt'])."', 
			SYSDATE, 
			'".txt_db($_POST['type'])."', 
			'".txt_db($_POST['pack'])."', 
			'".txt_db($_POST['nbre'])."', 
			'".txt_db($_POST['tarif'])."',
			TO_DATE('".txt_db($_POST['JJ'])."/".txt_db($_POST['MM'])."/".txt_db($_POST['AAAA'])."', 'DD/MM/YYYY'), 
			TO_DATE('".txt_db($_POST['JJ2'])."/".txt_db($_POST['MM2'])."/".txt_db($_POST['AAAA2'])."', 'DD/MM/YYYY'), 	
			'".txt_db($_POST['comm1'])."', 
			'".txt_db($_POST['comm2'])."')";
			//echo $sql_insert_jet;
			$qry_insert_jet		= $db->query($sql_insert_jet);
			
			$sql_insert_jet 	= "INSERT INTO JETON_CORRESP_CERT (JETON_CORRESP_ID,JET_ID,CERT_ID,CERT_JET_ID,AFFECTE,UTILISE) VALUES (SEQ_ID.NEXTVAL, 
			'".$set_jet_tmp."', 
			'-1','-1', 
			'".txt_db($_POST['nbre'])."', 
			'0')";
			//echo $sql_insert_jet;
			$qry_insert_jet		= $db->query($sql_insert_jet);
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			<?php
		}
		
		// Chargement des infos du partenaire
		$sql_partenaire			= "SELECT PART_ID, PART_NOM, PART_NATURE_CODE_ID FROM PARTENAIRE WHERE PART_ID='".txt_db($_GET['partid'])."'";
		$qry_partenaire 		= $db->query($sql_partenaire);
		
		// On cherche si le partenaire a un contrat
		$sql_contrat			= "SELECT CONT_PART_ID FROM PART_A_CONTRAT WHERE CONT_PART_ID='".txt_db($_GET['partid'])."' AND TO_CHAR(CONT_DATE_DEB,'YYYYMMDD')<=TO_CHAR(SYSDATE,'YYYYMMDD') AND TO_CHAR(CONT_DATE_FIN,'YYYYMMDD')>=TO_CHAR(SYSDATE,'YYYYMMDD')";
		$qry_contrat	 		= $db->query($sql_contrat);
		
		// Chargement de la liste des produits
		$sql_prod_list 			= "SELECT * FROM PRODUIT ORDER BY PROD_NOM";
		$qry_prod_list 			= $db->query($sql_prod_list);
		
		// Chargement de la liste des types de jeton
		$sql_type_jet_list 		= "SELECT * FROM CODE WHERE CODE_TABLE='TYPE_JETON'";
		$qry_type_jet_list 		= $db->query($sql_type_jet_list);
		
		// Chargement de la liste des natures de pack
		$sql_nature_pack_list 	= "SELECT * FROM CODE WHERE CODE_TABLE='NATURE_PACK'";
		$qry_nature_pack_list 	= $db->query($sql_nature_pack_list);
		
		if (is_array($qry_partenaire)){
		?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--
			function change_pack(quantite_prix){
				var dub = quantite_prix.split("__"); 
				quantite = dub[0];
				prix = dub[1];
				if(quantite>0){
					document.form.nbre.value = quantite;
				}else{
					document.form.nbre.value = 0;
				}
				if(prix>0){
					document.form.tarif.value = prix;
					document.form.hid_tarif.value = prix;
				}else{
					document.form.tarif.value = 0;
					document.form.hid_tarif.value = 0;
				}
			}
			
			function masque(typid){
				if (typid==11 || typid==12){ 
					document.getElementById("pack_liste").innerHTML='';
					document.getElementById("lib_pack").innerHTML='';
					document.form.nbre.readOnly = false;
					document.form.nbre.value = 0;
					document.form.tarif.value = 0;
					if(typid==12){
						document.form.tarif.readOnly = true;
					}else{
						document.form.tarif.readOnly = false;
					}
				}else{
					//alert(typid);
					document.form.tarif.readOnly = false;
					document.form.nbre.value = 0;
					document.form.tarif.value = 0;
					document.form.nbre.readOnly = false;
					document.getElementById("lib_pack").innerHTML = "<?php echo $t_jet_nature_pack ?> :";
					listing_pack(document.getElementById('liste_prdt').options[document.getElementById('liste_prdt').selectedIndex].value, <?php echo $qry_partenaire[0]['part_nature_code_id'] ?>, document.form.type.options[document.form.type.selectedIndex].value);
				}
			}
			
			function listing_pack(prodid, natid, typid){
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_liste_packs.php"; // La page qui réceptionne les données
				var data     = null; 
				
				if 	(prodid>0){ //Si le code postal tapé possède au moins 2 caractères
					//alert(prodid);
					document.form.nbre.value = 0;
					document.form.tarif.value = 0;
					document.form.nbre.readOnly = false;
					document.getElementById("pack_liste").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="">';
					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("<?php echo $t_browser_support_error_1 ?>");
						   return; 
						} 
						 
						
						 
						if(prodid != "" && natid != ""){
							data = "prodid="+prodid+"&natid="+natid;
						}
						if(method == "GET" && data != null) {
						   filename += "?"+data;
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
						   document.getElementById("pack_liste").innerHTML = '';
							  var tmp = xhr_object.responseText.split(":"); 
							 if (tmp[0]!=''){
								if(typid==10){
								document.getElementById("pack_liste").innerHTML = tmp[0];
								}else{
									document.form.nbre.readOnly=false;
								}
							 }else{
								if(typid==10){
								document.getElementById("pack_liste").innerHTML = "<?php echo $t_aucun_pack_pdt ?>";
								}else{
									document.form.nbre.readOnly=false;
								}
							 }
						   } 
						} 

						xhr_object.send(data); //On envoie les données
				}
			}
			
			function verif(){
					error = '';
					error1 = '';
					error2 = '';
					error_tarif = false;
				
				if(document.form.hid_contrat_valid.value==0 && document.form.type.options[document.form.type.selectedIndex].value == 10){
					if(confirm("<?php echo $t_aucun_contrat_part ?>")){
						error_contrat = false;
					}
					else
					{
						error += "<?php echo $t_contrat_invalide ?>\r\n";
					}
				}
				
				if(document.form.type.options[document.form.type.selectedIndex].value == 10){
					if(document.form.tarif.value != document.form.hid_tarif.value){
						error_tarif = true;
					}
					if(document.form.packs){
						if(document.form.packs.options[document.form.packs.selectedIndex].value<1){
							error += "<?php echo $t_choisir_un_type_pack ?>\r\n";
						}
					}else{
						error += "<?php echo $t_aucun_pack_pdt ?>\r\n";
					}
				}				
				
				if (document.getElementById('liste_prdt').options[document.getElementById('liste_prdt').selectedIndex].value<1){
					error += "<?php echo $t_pdt_oblig ?>\n";
				}
				
				if (document.form.nbre.value<1){
					error += "<?php echo $t_jet_nbre_oblig ?>\n";
				}
				// verif du format date du début
				if (document.form.JJ.value<1 || document.form.JJ.value>31 || document.form.JJ.value.length<2){
					error1 = true;
				}
				if (document.form.MM.value<1 || document.form.MM.value>12 || document.form.MM.value.length<1){
					error1 = true;
				}
				if (document.form.AAAA.value<2009 || document.form.AAAA.value.length<4){
					error1 = true;
				}
				if (error1==true){
					error +="<?php echo $t_mauvaise_date_deb ?>\n";
				}
				
				// Verif du format date de fin
				if (document.form.JJ2.value<1 || document.form.JJ2.value>31 || document.form.JJ2.value.length<2){
					error2 = true;
				}
				if (document.form.MM2.value<1 || document.form.MM2.value>12 || document.form.MM2.value.length<1){
					error2 = true;
				}
				if (document.form.AAAA2.value<2009 || document.form.AAAA2.value.length<4){
					error2 = true;
				}
				if (error2==true){
					error +="<?php echo $t_mauvaise_date_fin ?>\n";
				}
				else
				{
					if (document.form.AAAA2.value + document.form.MM2.value + document.form.JJ2.value < document.form.AAAA.value + document.form.MM.value + document.form.JJ.value)
					{
						error +="<?php echo $t_pb_date_fin_deb ?>\n";
					}
				}
				if (error!=''){
					alert(error);
				}else{
					if(error_tarif){
						if(confirm("<?php echo $t_sur_de_mod_tarif ?>")){
							document.form.submit();
						}
					}else{
						document.form.submit();
					}
				}
			}
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function pass(champ){
				if(champ==1){
					if(document.form.JJ.value.length==2){
						document.form.MM.value='';
						document.form.MM.focus();
					}
				}
				if(champ==2){
					if(document.form.MM.value.length==2){
						document.form.AAAA.value='';
						document.form.AAAA.focus();
					}
				}
				if(champ==3){
					if(document.form.JJ2.value.length==2){
						document.form.MM2.value='';
						document.form.MM2.focus();
					}
				}
				if(champ==4){
					if(document.form.MM2.value.length==2){
						document.form.AAAA2.value='';
						document.form.AAAA2.focus();
					}
				}
				
			}
			//-->
			</script>
			</head>

			<body bgcolor="#FFFFFF" text="#000000">
			<form method="post" action="#" name="form">
            
            <table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Partenaires2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo strtoupper($qry_partenaire[0]['part_nom']) ?></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td ><table border="0" cellspacing="0" cellpadding="0"  width="100%">
					  <tr> 
						<td height="14"></td>
						</tr>
					<tr> 
						  <td class="TX_Partenaires"><?php echo $t_jet_attrib_part ?></td>
					    </tr>
						<tr> 
						  <td bgcolor="#666666" height="1"></td>
					    </tr>
						<tr> 
						  <td align="center" class="TX">&nbsp;</td>
					    </tr>
					  <tr> 
						<td align="center" valign="top" class="TX"> 
						  <table border="0" cellspacing="0" cellpadding="0">
							<tr> 
							  <td class="TX" ><?php echo $t_date_attrib ?> : </td>
							  <td class="TX"><?php echo date('d/m/Y') ?></td>
							  <td class="TX">&nbsp;</td>
							  <td class="TX">&nbsp;</td>
							</tr>
							<tr> 
							  <td class="TX" ><?php echo $t_produit ?> :</td>
							  <td class="TX"> 
								<select id="liste_prdt" name="liste_prdt" class="form_ediht_Partenaires" onChange="listing_pack(document.getElementById('liste_prdt').options[document.getElementById('liste_prdt').selectedIndex].value, <?php echo $qry_partenaire[0]['part_nature_code_id'] ?>, document.form.type.options[document.form.type.selectedIndex].value)">
								  <option value="0" selected="selected"><?php echo $t_produits_tous ?></option>
								  <?php
								  if (is_array($qry_prod_list)){
									foreach($qry_prod_list as $produit){
										echo '<option value="'.$produit['prod_id'].'">'.$produit['prod_nom'].'</option>';
									}
								  }
								  ?>
								</select>
							  </td>
							  <td class="TX">&nbsp;</td>
							  <td class="TX">&nbsp;</td>
							</tr>
							<tr> 
							  <td class="TX" ><?php echo $t_jet_type ?> : </td>
							  <td  class="TX"> 
								<select name="type" onChange="masque(document.form.type.options[document.form.type.selectedIndex].value)" class="form_ediht_Partenaires">
								  <?php
								  if (is_array($qry_type_jet_list)){
									foreach($qry_type_jet_list as $type_jet){
										echo '<option value="'.$type_jet['code_id'].'">'.$type_jet['code_libelle'].'</option>';
									}
								  }
								  ?>
								</select>
							  </td>
							  <td  class="TX"><div id="lib_pack"><?php echo $t_jet_nature_pack ?> :</div></td>
							  <td  class="TX"> 
								<div id="pack_liste"></div>
								
							  </td>
							</tr>
							<tr> 
							  <td class="TX" ><?php echo $t_jet_nbre ?> :</td>
							  <td  class="TX"> 
								<input type="text" name="nbre" size="5" class="form_ediht_Partenaires" >
							  </td>
							  <td  class="TX"><?php echo $t_tarif_unit ?> : </td>
							  <td  class="TX"> 
								<input type="text" name="tarif" size="5" class="form_ediht_Partenaires" >
								<input type="hidden" name="hid_tarif">
								&euro; <?php echo $t_HT ?> </td>
							</tr>
							<tr> 
							  <td class="TX" ><?php echo $t_date_deb_valid2 ?> 
								:&nbsp;&nbsp; </td>
							  <td class="TX"> 
								<input type="text" name="JJ" onKeyUp="pass(1)" size="2" class="form_ediht_Partenaires" value="<?php echo date('d') ?>" maxlength="2">
								/ 
								<input type="text" name="MM" onKeyUp="pass(2)" size="2" class="form_ediht_Partenaires" value="<?php echo date('m') ?>" maxlength="2">
								/ 
								<input type="text" name="AAAA" size="4" class="form_ediht_Partenaires" value="<?php echo date('Y') ?>" maxlength="4">
								&nbsp;&nbsp;&nbsp;&nbsp;</td>
							  <td  class="TX"><?php echo $t_date_fin_valid2 ?> :</td>
							  <td  class="TX"> 
								<input type="text" name="JJ2" onKeyUp="pass(3)" size="2" class="form_ediht_Partenaires" maxlength="2">
								/ 
								<input type="text" name="MM2" onKeyUp="pass(4)" size="2" class="form_ediht_Partenaires" maxlength="2">
								/ 
								<input type="text" name="AAAA2" size="4" class="form_ediht_Partenaires" maxlength="4">
							  </td>
							</tr>
							<tr> 
							  <td class="TX" ><?php echo $t_comment ?> :</td>
							  <td  class="TX" colspan="3"> 
								<input type="text" name="comm1" style="width:99%" class="form_ediht_Partenaires" >
							  </td>
							</tr>
							<tr>
							  <td class="TX" >&nbsp;</td>
							  <td  class="TX" colspan="3">
								<input type="text" name="comm2" style="width:99%"" class="form_ediht_Partenaires" >
							  </td>
							</tr>
						  </table>
						</td>
						</tr>
				    </table></td>
				    </tr>
				  </table>
            
			  <br>
              
			  <p style="text-align:center">
				  <?php
				  // Si le partenaire a un contrat en cours de validité, on le fait savoir
				  if(is_array($qry_contrat)){
					echo '<input type="hidden" name="hid_contrat_valid" value="1">';
				  }else{
					echo '<input type="hidden" name="hid_contrat_valid" value="0">';
				  }
				  ?>
					<input type="hidden" name="partid" value="<?php echo $qry_partenaire[0]['part_id'] ?>" class="BN">
					<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_partenaire" onClick="verif();">
				  </p>
			</form>
			</body>
			</html>
			<?php
		}
	}
}else{
	include('no_acces.php');
}
?>