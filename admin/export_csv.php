<?php
set_time_limit(200);
if ($_SESSION['droit']=='9'){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	if ($_GET['txtimgid']){
		/* Requete de l'export */
		
		$sql_nom = "select txt_img_nom from texte_image where TXT_IMG_ID='".txt_db(intval($_GET['txtimgid']))."'";
		$qry_nom = $db->query($sql_nom);
		$sql_export_nom = "select txt_img_nom from texte_image where TXT_IMG_ID='".txt_db(intval($_GET['txtimgid']))."' ";
		$qry_export_nom = $db->query($sql_export_nom);

		
		$sql_regroup = "select distinct CODE_REGROUP_ID,CODE_REGROUP_NOM,TXT_NOM from CODE_REGROUPEMENT,TEXTE where CODE_REGROUPEMENT.TXT_CODE_ID=TEXTE.TXT_CODE_ID AND TXT_IMG_ID='".intval($_GET['txtimgid'])."' ORDER BY CODE_REGROUP_NOM,TXT_NOM";
		$qry_regroup = $db->query($sql_regroup);
				
		function format_sortie($string){
			$string=str_replace(chr(10).chr(13), ' ', $string);
			$string=str_replace(chr(10), ' ', $string);
			$string=str_replace(chr(13), ' ', $string);
			$string=strip_tags($string);
			$string=preg_replace("/(\r\n|\n|\r)/", " ",$string);
			$string=html_entity_decode($string);
			$string=str_replace(';',',',$string);
			return $string;
		}
			
			$chemin = "./temp/";
			$file = $qry_export_nom[0]['txt_img_nom'];
			$file = str_replace(' ','_', $file);
			$file = strtr($file,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
			$file = strtolower($file).'.csv';

			$contenu  = "E_INF".';';
			$contenu .= "E_SUP".';';
			$contenu .= "C_INF".';';
			$contenu .= "C_SUP".';';
			$contenu .= "P_INF".';';
			$contenu .= "P_SUP".';';
			$contenu .= "A_INF".';';
			$contenu .= "A_SUP".';';
			$contenu .= "CODE_REGROUP_NOM".';';
			$contenu .= "TXT_NOM".';';
			$contenu .= "\r\n";
			$fp = fopen($chemin . $file ,"w+");
			fputs($fp,$contenu);
			fclose($fp);
			if(is_array($qry_regroup )){
				foreach($qry_regroup as $data_gp){
				$sql_export = "select * from TEXTE_IMAGE_A_COMBI where combi_txt_img_id=".intval($_GET['txtimgid'])." and combi_code_regroup_id=".$data_gp['code_regroup_id']."";
				//echo $sql_export;
				$qry_export = $db->query($sql_export);
				if(is_array($qry_export )){
					foreach($qry_export as $data){
						$contenu = "";
						$contenu  .= format_sortie($data['combi_e_inf']).';';
						$contenu  .= format_sortie($data['combi_e_sup']).';';
						$contenu  .= format_sortie($data['combi_c_inf']).';';
						$contenu  .= format_sortie($data['combi_c_sup']).';';
						$contenu  .= format_sortie($data['combi_p_inf']).';';
						$contenu  .= format_sortie($data['combi_p_sup']).';';
						$contenu  .= format_sortie($data['combi_a_inf']).';';
						$contenu  .= format_sortie($data['combi_a_sup']).';';
						$contenu  .= format_sortie($data_gp['code_regroup_nom']).';';
						$contenu  .= format_sortie($data_gp['txt_nom']).';';
						$contenu  .= "\r\n";
						$fp = fopen($chemin . $file ,"a+");
						fputs($fp,$contenu);
						fclose($fp);			
					}
				}
				else
				{
						$contenu = ";;;;;;;;";
						$contenu  .= format_sortie($data_gp['code_regroup_nom']).';';
						$contenu  .= format_sortie($data_gp['txt_nom']).';';
						$contenu  .= "\r\n";
						$fp = fopen($chemin . $file ,"a+");
						fputs($fp,$contenu);
						fclose($fp);			
				}
				}
			}
			// On ouvre un nouveau fichier
			
			$type = "text/csv";
			header("Content-disposition: attachment; filename=$file");
			header("Content-Type: application/force-download");
			header("Content-Transfer-Encoding: $type\n");
			header("Content-Length: ".filesize($chemin . $file));
			header("Pragma: no-cache");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
			header("Expires: 0");
			readfile($chemin . $file);
	}
	
}else{
	include('no_acces.php');
}
?>