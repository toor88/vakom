<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VAKOM | Rapport BPM</title>
    <!-- Bootstrap -->
	<link href="styles-rapport.css" rel="stylesheet">	  	  
	<script src="http://www.lesensdelhumain.com/public/charts2/Chart.bundle.js"></script>
	<style type="text/css">/* Chart.js */
		@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style>
	<script src="http://www.lesensdelhumain.com/public/charts2/analyser.js"></script>	
  </head>
  <body>

<?php 

session_start();

include ("../config/lib/connex.php");
include ("../config/lib/db.oracle.php");

$db = new db($conn);

$opeid = $_GET['opeid'];

if (isset($opeid)){

	$manger_nom ="";
	$mangerplusun_nom ="";
	$manger_cand_id = null;
	$nb_personnes_mg=-2;

	$sql_select_question = "SELECT distinct CAND_ID FROM CAND_A_QUEST WHERE OPE_ID = ".txt_db(intval($opeid));
    $condidats   	 = $db->query($sql_select_question);
	    
    foreach($condidats as $condidat){
		$sql_infos_cand = "SELECT CANDIDAT.*,CAND_A_OPE.* FROM CANDIDAT, CAND_A_OPE WHERE CAND_A_OPE.CAND_ID = CANDIDAT.CAND_ID AND CANDIDAT.CAND_ACTIF='1' AND CANDIDAT.CAND_ID='".txt_db(intval($condidat['cand_id']))."'  AND CAND_A_OPE.OPE_ID=".txt_db(intval($opeid))."";
		$infos_candidat = $db->query($sql_infos_cand);            
        if ($infos_candidat[0]['niveau'] == 1){
			$manger_cand_id = $condidat['cand_id'];
        	$manger_nom = $infos_candidat[0]['cand_prenom'] . ' '. $infos_candidat[0]['cand_nom'];
			$cand_fonction = $infos_candidat[0]['cand_fonction'];
        }
        if ($infos_candidat[0]['niveau'] == 2){
        	$mangerplusun_nom = $infos_candidat[0]['cand_prenom'] . ' '. $infos_candidat[0]['cand_nom'];        	
        }        
		$nb_personnes_mg++;
    }
    
	$tab = getData($db,$opeid,0, null);
	getData($db,$opeid,1, $tab);
	getData($db,$opeid,2, $tab);
		
}

function getData($db,$opeid,$level,$tab)
{
	$sql_query = "select theme,stheme,round((sum(to_number(txt_libre))/count(choix_id))*10,2) note from reponse_bpm where ope_id=".$opeid." and niveau=".$level." group by theme,stheme order by theme,stheme";
	$qry_query = $db->query($sql_query);

	$themes = array();
	$sthemes = array();
	$objects = array();

	$index = 0;

	foreach($qry_query as $query)
	{
		$theme = $query['theme'];
		$stheme = $query['stheme'];
		$note = $query['note']; 
	
		if (in_array($theme, $themes)) 
		{
			if($level != 0){
				$sth = getObjectStheme($tab, $stheme);				
			} else {
				$sth = new stdClass;
				$sth->stheme = $stheme;			
			}
			
			if ($level == 0)	$sth->collaborateur = $note;
			if ($level == 1)	$sth->manager = $note;
			if ($level == 2)	$sth->nplus1 = $note;			
			
			array_push($sthemes, $sth);
		
			if($qry_query[$index+1]['theme'] != $theme)
			{			
				$std = new stdClass;
				$std->theme = $theme;
				$std->max = count($sthemes);
				$std->sthemes = $sthemes;
				array_push($objects, $std);
			}
		} else {				
			$sthemes = array();
			array_push($themes, $theme);
			if($level != 0){
				$sth = getObjectStheme($tab, $stheme);				
			} else {
				$sth = new stdClass;
				$sth->stheme = $stheme;			
			}
	
			if ($level == 0)	$sth->collaborateur = $note;
			if ($level == 1)	$sth->manager = $note;
			if ($level == 2)	$sth->nplus1 = $note;

			array_push($sthemes, $sth);
		}
		$index++;	
	}
	return $objects;
} 

function getObjectStheme($tab, $stheme)
{
	foreach($tab as $ligne)
 	{
	foreach($ligne->sthemes as $obj)
	{
		if($obj->stheme == $stheme)
		{
			return $obj;
		}	
	}	
 }
}

	$type="radar";		
	$sql_graph ="select stheme_code_id,stheme,round((sum(to_number(txt_libre))/count(choix_id))*10,2) note from reponse_bpm where ope_id=".$opeid." and niveau=0 group by stheme_code_id,stheme order by stheme_code_id";
	$qry_graph = $db->query($sql_graph);

	foreach($qry_graph as $data )
	{
		$themesGraph1[] = str_replace("'","\'",$data['stheme']);
		$note1[] = str_replace(",",".",$data['note']);		
	}
	$counter1 = count($themesGraph1);		
	$noteGraph1[] = $note1;
			
	$sql_graph ="select stheme_code_id,stheme,round((sum(to_number(txt_libre))/count(choix_id))*10,2) note from reponse_bpm where ope_id=".$opeid." and niveau=1 group by stheme_code_id,stheme order by stheme_code_id";
	$qry_graph = $db->query($sql_graph);

	$note1 = array();
	foreach($qry_graph as $data)
	{
		$note1[] = str_replace(",",".",$data['note']);
	}
	$noteGraph1[] = $note1;
		
	$sql_graph ="select stheme_code_id,stheme,round((sum(to_number(txt_libre))/count(choix_id))*10,2) note from reponse_bpm where ope_id=".$opeid." and niveau=2 group by stheme_code_id,stheme order by stheme_code_id";
	$qry_graph = $db->query($sql_graph);

	$note1 = array();
	foreach($qry_graph as $data )
	{	
		$note1[] = str_replace(",",".",$data['note']);
	}

	$noteGraph1[] = $note1;	
	
	$note1 = array();	
	$sql_graph ="select code_id,itheme,round((sum(to_number(txt_libre))/count(choix_id))*10,2) note from reponse_bpm where ope_id=".$opeid." and niveau=0 group by code_id,itheme order by code_id";
	$qry_graph = $db->query($sql_graph);
		
	foreach($qry_graph as $data )
	{
		$themesGraph2[] = str_replace("'","\'",$data['itheme']);			
		$note1[] = str_replace(",",".",$data['note']);		
	}
	$counter2 = count($themesGraph2);
	$noteGraph2[] = $note1;

	$sql_graph ="select code_id,itheme,round((sum(to_number(txt_libre))/count(choix_id))*10,2) note from reponse_bpm where ope_id=".$opeid." and niveau=1 group by code_id,itheme order by code_id";
	$qry_graph = $db->query($sql_graph);

	$note1 = array();
	foreach($qry_graph as $data)
	{
		$note1[] = str_replace(",",".",$data['note']);
	}
	$noteGraph2[] = $note1;
		
	$sql_graph ="select code_id,itheme,round((sum(to_number(txt_libre))/count(choix_id))*10,2) note from reponse_bpm where ope_id=".$opeid." and niveau=2 group by code_id,itheme order by code_id";
	$qry_graph = $db->query($sql_graph);

	$note1 = array();
	foreach($qry_graph as $data )
	{	
		$note1[] = str_replace(",",".",$data['note']);
	}
	
	$noteGraph2[] = $note1;	

	$note1 = array();
	$themesGraph1= 		array();
	$sql_stheme_graph ="select distinct stheme, stheme_code_id from reponse_bpm where ope_id=".$opeid;
	$qry_stheme_graph = $db->query($sql_stheme_graph);
	
	$note1 = "";
	$note2 = "";
	$note3 = "";
	$themesGraph1 = array();
	
	foreach($tab as $data )
	{
		foreach($data->sthemes as $data2 )	{
			$themesGraph1[] = str_replace("'","\'",$data2->stheme);		 	
			$note1 .= "'".str_replace(",",".",$data2->collaborateur) ."',";
			$note2 .= "'".str_replace(",",".",$data2->manager) ."',";
			$note3 .= "'".str_replace(",",".",$data2->nplus1) ."',";						
		}		
	}
	$counter1 = count($themesGraph1);
	$noteGraph1 = array(substr($note1, 0, -1));
	$noteGraph1[] = substr($note2, 0, -1);
	$noteGraph1[] = substr($note3, 0, -1);
		

?>	

<div id="entretien" >
	<div class="container">		  
			<div class="entete"><img src="img/logo-bpm.png" width="279" height="90" alt=""/></div>			  
	</div>   
	<div class="container" >
	  
  <p class="theme">Document de pr&Eacute;paration &Agrave; l'entretien</p>		   
  <table class="tableAnalyse">
  <tbody>
    <tr>
      <td>Nom et pr&eacute;nom de la personne &eacute;valu&eacute;e</td>
      <td><?php echo$manger_nom?></td>
      </tr>
    <tr>
      <td>Fonction</td>
      <td><?php echo$cand_fonction?></td>
      </tr>
    <tr>
      <td  >Nombre de collaborateurs inscrits</td>
      <td ><?php echo$nb_personnes_mg?></td>
      </tr>
    <tr>
      <td >Nom du manager de la personne &eacute;valu&eacute;e</td>
      <td ><?php echo$mangerplusun_nom?></td>
      </tr>
    </tbody>
</table>
	  <p class="item strong text-center">ITEMS</p>				
		<p style="text-align: center;border: 1px solid #000" class="wrapper1">
			<canvas id="chart-1" width="1024" height="750" class="chartjs-render-monitor" ></canvas>
		</p>   
	 <p class="item strong text-center">Sous-th&egrave;mes</p>
		 <p style="text-align: center;border: 1px solid #000;width: 700px;" class="wrapper2">
			<canvas id="chart-2" width="750" class="chartjs-render-monitor" ></canvas>
		 </p>
		 <!--<p style="text-align: center;border: 1px solid #000;width: 1500px;" class="wrapper2">
			<canvas id="chart-2" width="750" class="chartjs-render-monitor" ></canvas>
		 </p> --> 		 		   
	     <p>&nbsp;</p>
		   
<?php		   

	$table = "<table class='tableResultAnalyse'><tbody><tr>";
    $table .= "<th>Th&egrave;me</th>";
	$table .= "<th>Sous-Th&egrave;mes</th>";
    $table .= "<th style='background-color: #0b2e43; width: 12%' >Auto &eacute;valuation</th>";
	$table .= "<th style='background-color: #3785b3; width: 12%' >N+1</th>";
	$table .= "<th style='background-color: #46b0ef; width: 12%'>Ensemble collaborateurs</th></tr>";
	
    foreach($tab as $obj){
    	$index_theme = 0;    
	    foreach($obj->sthemes as $stheme)
	    {
	    	//rowspan="$obj->max"
	    	$table .= '<tr>';
		    if ($index_theme == 0){
	    	  	$table .= '<td rowspan="'.$obj->max.'" style="width: 22.7%;" class="center">'.$obj->theme.'</td>';		    
		    } 
		    
		    $table .= '<td>'.$stheme->stheme.'</td>';
		    $table .= '<td class="center" >'.$stheme->manager.'%</td>';
		    $table .= '<td class="center" >'.$stheme->nplus1.'%</td>';
    		$table .= '<td class="center" >'.$stheme->collaborateur.'%</td>';		    
	      	$table .= '</tr>';
	      	$index_theme++;
	    }
	}
	      $table .= '</tbody></table>';	
?>	    
<?php echo$table?>
  </div><!-- FIN CONTAINER -->
</div>
</body>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>	
<script src="http://www.lesensdelhumain.com/public/html2canvas.min.js"></script>
<script>

		window.chartColors = {
			red: 'rgb(255, 99, 132)',
			orange: 'rgb(255, 159, 64)',	
			yellow: 'rgb(255, 205, 86)',
			green: 'rgb(75, 192, 192)',
			blue: 'rgb(54, 162, 235)',
			purple: 'rgb(153, 102, 255)',
			grey: 'rgb(201, 203, 207)'
		};	
		var presets = window.chartColors;	
		var inputs1 = {
			min: 8,
			max: 16,
			count: <?php echo$counter1?>,
			decimals: 2,
			continuity: 1
		};
		var inputs2 = {
			min: 8,
			max: 16,
			count: <?php echo$counter2?>,
			decimals: 2,
			continuity: 1
		};		
		var COLORS = [
			'#4dc9f6',
			'#f67019',
			'#f53794',
			'#537bc4',
			'#acc236',
			'#166a8f',
			'#00a950',
			'#58595b',
			'#8549ba'		
		];
 	 
		var Graph1 = ['<?php echoimplode("', '", $themesGraph1)?>'];
		var Graph2 = ['<?php echoimplode("', '", $themesGraph2)?>'];
		
		var note1 = [<?php echo$noteGraph1[0]?>];
		var note2 = [<?php echo$noteGraph1[1]?>];
		var note3 = [<?php echo$noteGraph1[2]?>];	

		var noteGraph2_1 = ['<?php echoimplode("', '", $noteGraph2[0])?>'];
		var noteGraph2_2 = ['<?php echoimplode("', '", $noteGraph2[1])?>'];
		var noteGraph2_3 = ['<?php echoimplode("', '", $noteGraph2[2])?>'];	
		

		function numbers(config){
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 1;
			var from = cfg.from || [];
			var count = cfg.count || 6;
			var decimals = cfg.decimals || 8;
			var continuity = cfg.continuity || 1;
			var dfactor = Math.pow(10, decimals) || 0;
			var data = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = (from[i] || 0) + this.rand(min, max);
				if (this.rand() <= continuity) {
					data.push(Math.round(dfactor * value) / dfactor);
				} else {
					data.push(null);
				}
			}

			return data;
		}
		function criteria1(config){
			var cfg = config || {};
			var count = cfg.count || 6;
			var section = cfg.section;
			var values = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = Graph1[Math.ceil(i) % cfg.count];
				if (value != undefined)
					values.push(value.substring(0, section));
			}

			return values;
		}	
		function criteria2(config){
			var cfg = config || {};
			var count = cfg.count || 6;
			var section = cfg.section;
			var values = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = Graph2[Math.ceil(i) % cfg.count];
				if (value != undefined)
					values.push(value.substring(0, section));
			}

			return values;
		}				
		function transparentize(color, opacity) {
			var alpha = opacity === undefined ? 0.1 : 1 - opacity;
			return Color(color).alpha(alpha).rgbString();
		}
		function generateData1() {		
			var values = numbers(inputs1);
			inputs1.from = values;
			return values;
		}
		function generateData2() {		
			var values = numbers(inputs2);
			inputs2.from = values;
			return values;
		}	

		function generateLabels1(config) {
			return criteria1({count: inputs1.count});
		}
		function generateLabels2(config) {
			return criteria2({count: inputs2.count});
		}
		
		var notations = {
  			  0: "0%",
  			 20: "20%",
  			 40: "40%",
			 60: "60%",
  			 80: "80%",
  			100: "100%"
  		}  		

		var data1 = {
				labels: generateLabels1(),
				datasets: [{
					backgroundColor: transparentize(presets.blue),
					borderColor: presets.blue,			
					data: note2,		
					label: 'Manager',
					fill: '-1'
				}, {
					backgroundColor: transparentize(presets.orange),
					borderColor: presets.orange,
					data: note3,
					label: 'N+1',
					fill: 1
				},{
					backgroundColor: transparentize(presets.grey),
					borderColor: presets.grey,
					data: note1,
					label: 'Ensemble collaborateur'				
				}]
			};
		var data2 = {
				labels: generateLabels2(),
				datasets: [{
					backgroundColor: transparentize(presets.blue),
					borderColor: presets.blue,			
					data: noteGraph2_2,		
					label: 'Manager',
					fill: '-1'
				}, {
					backgroundColor: transparentize(presets.orange),
					borderColor: presets.orange,
					data: noteGraph2_3,
					label: 'N+1',
					fill: 1
				},{
					backgroundColor: transparentize(presets.grey),
					borderColor: presets.grey,
					data: noteGraph2_1,
					label: 'Ensemble collaborateur'				
				}]
			};
						
		var options1 = {
			maintainAspectRatio: true,
			spanGaps: false,					
			elements: {
				line: {
					tension: 0.000001
				}
			},			
			scale:{
				pointLabels: {
      				fontSize: 9
	    		},
				ticks: {
		            beginAtZero: true,
        		    max: 100,
        		    padding: 125,
        		    fontSize: 9,
        			autoSkip: true,
        		    stepSize: 20,
					userCallback: function(value, index, values) {
			          return notations[value];
			        }      		    
		        },
                gridLines:{
					tickMarkLength: 10
                }
			},
			tooltips:{
              enabled:true
            },
			legend: {
			    position: 'bottom'
			}
			,plugins: {
				filler: {
					propagate: false
				},
				samples_filler_analyser: {
					target: 'chart-analyser'
				}
			},  
			animation: {
    			  onComplete () {
						generatePic(1,".wrapper1");
					  }
    		}
		};					

		var options2 = {
			maintainAspectRatio: true,
			spanGaps: false,					
			elements: {
				line: {
					tension: 0.000001
				}
			},			
			scale:{
				pointLabels: {
      				fontSize: 9
	    		},
				ticks: {
		            beginAtZero: true,
        		    max: 100,
        		    padding: 125,
        		    fontSize: 9,
        			autoSkip: true,
        		    stepSize: 20,
					userCallback: function(value, index, values) {
			          return notations[value];
			        }      		    
		        },
                gridLines:{
					tickMarkLength: 10
                }
			},
			tooltips:{
              enabled:true
            },
			legend: {
			    position: 'bottom'
			}
			,plugins: {
				filler: {
					propagate: false
				},
				samples_filler_analyser: {
					target: 'chart-analyser'
				}
			},  
			animation: {
    			  onComplete () {
						//generatePic(2, '.wrapper2');
					  }
    		}
		};				
		
		var chart1 = new Chart('chart-1', {
			type: '<?php echo$type?>',
			data: data1,
			options: options1
		});	
		var chart2 = new Chart('chart-2', {
			type: '<?php echo$type?>',
			data: data2,
			options: options2
		});		
		
		function togglePropagate(btn) {
			var value = btn.classList.toggle('btn-on');
			chart1.options.plugins.filler.propagate = value;
			chart1.update();
			
			chart2.options.plugins.filler.propagate = value;
			chart2.update();			
		}
		function toggleSmooth(btn) {
			var value = btn.classList.toggle('btn-on');
			chart1.options.elements.line.tension = value? 0.1 : 0.000001;
			chart1.update();

			chart2.options.elements.line.tension = value? 0.1 : 0.000001;
			chart2.update();			
		}
		function randomize() {
			inputs1.from = [];
			chart1.data.datasets.forEach(function(dataset) {
				dataset.data = generateData1();
			});
			chart1.update();
			inputs2.from = [];			
			chart2.data.datasets.forEach(function(dataset) {
				dataset.data = generateData2();
			});
			chart2.update();			
		}	
		
		function generatePic(type, classSelector) {
			div_content = document.querySelector(classSelector)

				html2canvas(div_content).then(function(canvas) {
					//change the canvas to jpeg image
					data = canvas.toDataURL('image/png');
					
					//then call a super hero php to save the image
					save_img(data,type);
				});
		}
		
		function save_img(data, type){
			var ope_id = '<?php echo$opeid?>';
			
			$.post('http://www.lesensdelhumain.com/public/graph_generator.php', {data: data,type: type,ope_id: ope_id}, function(res){				
			});
		}			
  
</script>
</html>
