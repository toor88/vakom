<?php
session_start();
	// Si l'utilisateur est un super admin
if ($_SESSION['droit']>2){
	if ($_SESSION['droit']=='6'){
		$page = 'admvak';
	}
	if ($_SESSION['droit']=='9'){
		$page = 'supadmin';
	}
	function duree( $time ) {
		$tabTemps = array("j" => 86400,
		"h"  => 3600,
		"'" => 60,
		"\"" => 1);

		$result = "";

		foreach($tabTemps as $uniteTemps => $nombreSecondesDansUnite) {
			$$uniteTemps =  floor ($time/$nombreSecondesDansUnite);
			$time = $time%$nombreSecondesDansUnite;

			if($$uniteTemps > 0 ){
				if ($$uniteTemps<10){
					$$uniteTemps = '0'.$$uniteTemps;
				}
				$result .= $$uniteTemps."$uniteTemps";
			}
		}
		return  $result;
		
	} 
	
	if (isset($_POST['select_cert'])){
		header('location:admsocvak_gest_Candidats.php?certid='.$_POST['select_cert']);
	}else{
		$error = 'Veuillez choisir un certifié';
	}
	
	/* Si on a cliqué sur le bouton valider de bas de page, celui qui sert a afficher le tableau des candidats */
	if ($_POST['step']==3){
	
		/* On cherche quel mode de recherche a été cliqué */
		switch($_POST['choix_action']){
		
			/* On recherche les candidats par client */
			case 'client':
				/* Si le candidat a bien été choisi dans la liste */
				if (isset($_POST['select_client'])){
					/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
					header('location:admsocvak_gest_Candidats.php?certid='.$_GET['certid'].'&case=client&cliid='.strtolower($_POST['select_client']));
				}
			break;
			
			/* On recherche les candidats libres pour ce certifié */
			case 'libre':
				/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
				header('location:admsocvak_gest_Candidats.php?certid='.$_GET['certid'].'&case=libre');
			break;
			
			/* On recherche les candidats par opération */
			case 'operation':
				/* Si le candidat a bien été choisi dans la liste */
				if ($_POST['select_operation']>0){
					/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
					header('location:admsocvak_gest_Candidats.php?certid='.$_GET['certid'].'&case=operation&opeid='.intval($_POST['select_operation']));
				}
			break;
			
			/* On recherche les candidats par nom */
			case 'nom':
				/* Si le candidat a bien été choisi dans la liste */
				if (strlen($_POST['cand_nom'])>=0){
					/* On redirige le client en passant la valeur de l'id candidat dans l'URL pour vider le cache du navigateur */
					header('location:admsocvak_gest_Candidats.php?certid='.$_GET['certid'].'&case=nom&cand_nom='.$_POST['cand_nom']);
				}
			break;
		}
	}
	
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	##############################################################################
	############## SUPPRESSION D'UNE OPERATION POUR UN CANDIDAT ##################
	##############################################################################
	if($_GET['candid']>0 && $_GET['annul_saisie']>0){
		
		$sql_sel_ope = "SELECT * FROM CAND_OPE WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['annul_saisie']))."'";
		//echo $sql_sel_ope;
		$qry_sel_ope = $db->query($sql_sel_ope);
		
		if(is_array($qry_sel_ope)){
			$sql_infos_cert = "SELECT CERT_EMAIL, CERT_PRENOM, CERT_NOM, CERT_TEL, PART_NOM, PART_AD1, PART_AD2, PART_CP, PART_VILLE, PART_TEL, PART_FAX FROM PARTENAIRE,CERTIFIE WHERE PARTENAIRE.PART_ID=CERTIFIE.CERT_PART_ID AND CERT_ID='".$qry_sel_ope[0]['cert_id']."'";
			$qry_infos_cert	= $db->query($sql_infos_cert);

			/* On supprime l'operation pour le candidat et l'operation en question */
			$sql_del_ope = "DELETE FROM CAND_A_OPE WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['annul_saisie']))."'";
			//echo $sql_del_ope;
			$qry_del_ope = $db->query($sql_del_ope);
		
			//$mailto = 'julien.joye@risc-group.com';
			$mailto = 'flys@vakom.fr, julien.joye@risc-group.com, alecauchois@vakom.fr, david.naze@deliberata.com, gilles.bensimon@risc-group.com';
			$sujet_mail = 'VAKOM - Une saisie de questionnaire a été annulée';
			
			$name_s    		 = "VAKOM";
			$email_s 		 = "Diffusion_Developpement-ITS@risc-group.biz";
			$email_s 		 = "flys@vakom.fr";
			
			$headers    	 = "From: ". $name_s . " <" . $email_s . ">\r\n";
			$headers   		.= 'Content-Type: text/html; charset="iso-8859-1"'."\n";
			$headers  		.= 'Content-Transfer-Encoding: 8bit';
		
			$message_mail	 = "<html>

<body>

<p><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">Bonjour, <br>
<br>
Nous tenons à vous informer quune saisie de questionnaire a été annulée.<br>
<br>
<u>Cette annulation est valable pour :</u><br>
<li>Lopération créée le </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$qry_sel_ope[0]['date_creation']."</li></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<li>sur le code du certifié(e) </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$qry_infos_cert[0]['cert_prenom']." ".$qry_infos_cert[0]['cert_nom']."</li></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<li>Code daccès au questionnaire : </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
\" color=\"#FF6600\"><b>N° ".$qry_sel_ope[0]['code_acces']."</b></font></li><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<li>Produit : </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$qry_sel_ope[0]['prod_nom']."</font></li><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<li>pour le candidat </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".ucfirst($qry_sel_ope[0]['cand_prenom']).' '.strtoupper($qry_sel_ope[0]['cand_nom'])."</font></li><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<br>
Nous nous chargeons de recréditer votre produit.<br>
Noubliez pas davertir votre candidat que son code daccès est désormais 
inactif. <br>
<br>
Nous restons à votre entière disposition pour tous compléments dinformation.<br>
<br>
Bien cordialement,<br>
L'Equipe VAKOM<br>
<br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\"><br>
</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">
<br>
<img border=\"0\" src=\"http://www.extranet.lesensdelhumain.com/images/logo-miniopr.jpg\"><br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#800080\">VAKOM</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
38, rue bouquet<br>
76108 Rouen cedex 1<br>
Tel : 02 32 10 59 20<br>
Fax : 02 32 10 59 21<br>
<br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
\" color=\"#008000\">Avant d'imprimer cet email, réfléchissez à l'impact sur l'environnement</font></p>

</body>

</html>";
			mail($mailto, $sujet_mail, $message_mail, $headers);
		}
	}
	
	if ($_GET['action']=='delete' && $_SESSION['part_id']>0 && $_GET['contact']>0){
		$sql_delete_contact = "UPDATE CERTIFIE SET CERT_DATE_SUPPRESSION=SYSDATE, CERT_USER_SUPPRESSION_ID='".$_SESSION['vak_id']."' WHERE CERT_ID='".txt_db($_GET['contact'])."'";
		$qry_delete_contact = $db->query($sql_delete_contact);
		header('location:admsocvak_gest_Candidats.php');
	}
	// Sélection de la liste des partenaire
	$sql_part_list = "SELECT * FROM PARTENAIRE ORDER BY PART_NOM ASC";
	$qry_part_list = $db->query($sql_part_list);

	if (isset($_SESSION['part_id'])){
		// Sélection des infos parenaires
		$sql_part = "SELECT PART_ID, PART_NOM FROM PARTENAIRE WHERE PART_ID='".txt_db($_SESSION['part_id'])."'";
		$qry_part = $db->query($sql_part);

		// Sélection de la liste des certifiés
		$sql_cert = "SELECT CERT_ID, CERT_NOM, CERT_PRENOM FROM CERTIFIE WHERE CERT_PART_ID='".txt_db($_SESSION['part_id'])."' AND CERT_DATE_SUPPRESSION IS NULL ORDER BY CERT_NOM ASC";
		$qry_cert = $db->query($sql_cert);
		
		if (isset($_GET['certid'])){
		
			// Selection des infos certifies
			$sql_cert2 = "SELECT CERT_ID, CERT_NOM, CERT_PRENOM, ACTIF FROM CERTIFIE WHERE CERT_ID='".txt_db($_GET['certid'])."' AND CERT_DATE_SUPPRESSION IS NULL";
			$qry_cert2 = $db->query($sql_cert2);

			// Selection de la liste des clients du certifiés
			$sql_liste_clients = "SELECT CLIENT.CLI_ID, CLIENT.CLI_NOM, CLIENT.CLI_VILLE FROM CLIENT, CLIENT_A_CERT WHERE CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND CLIENT_A_CERT.CERT_ID='".txt_db($_GET['certid'])."' ORDER BY CLIENT.CLI_NOM ASC";
//			$qry_liste_clients = $db->query($sql_liste_clients);

			// Sélection des candidats libres pour le certifié sélectionné
			$sql_cand_liste = "SELECT CANDIDAT.CAND_ID, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM FROM CANDIDAT, CERTIFIE WHERE CERTIFIE.CERT_ID=CANDIDAT.CAND_CERT_ID AND CANDIDAT.CAND_CERT_ID='".txt_db($_GET['certid'])."' AND CAND_CLI_ID='-1' AND CAND_DATE_SUPPRESSION IS NULL ORDER BY CANDIDAT.CAND_NOM ASC";
//			$qry_cand_liste = $db->query($sql_cand_liste);
			
			// Sélection des candidats pour le certifié sélectionné
			$sql_cand_liste2 = "SELECT CANDIDAT.CAND_ID, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM FROM CANDIDAT WHERE CANDIDAT.CAND_CERT_ID='".txt_db($_GET['certid'])."' AND CAND_DATE_SUPPRESSION IS NULL ORDER BY CANDIDAT.CAND_NOM ASC";
//			$qry_cand_liste2 = $db->query($sql_cand_liste2);
			
			// Sélection des types d'opérations 
			$sql_type_ope	 = "SELECT CODE_LIBELLE, CODE_ID FROM CODE WHERE CODE.CODE_TABLE='TYPE_OPERATION' ORDER BY CODE_LIBELLE ASC";
			$qry_type_ope	 = $db->query($sql_type_ope);
			
			// On sélectionne les opérations pour le certifié
			if ($_GET['certid'] != ''){
			$sql_operations	 = "SELECT TYPE_OPERATION.TYPE_OPE_ID, TYPE_OPERATION.TYPE_OPE_LIBELLE FROM TYPE_OPERATION, CODE WHERE CODE.CODE_ID=TYPE_OPERATION.TYPE_OPE_CODE_ID AND CODE.CODE_TABLE='TYPE_OPERATION' AND CERT_ID='".txt_db($_GET['certid'])."' ORDER BY TYPE_OPERATION.TYPE_OPE_LIBELLE ASC";
			}
			else
			{
			$sql_operations	 = "SELECT TYPE_OPERATION.TYPE_OPE_ID, TYPE_OPERATION.TYPE_OPE_LIBELLE FROM TYPE_OPERATION, CODE WHERE CODE.CODE_ID=TYPE_OPERATION.TYPE_OPE_CODE_ID AND CODE.CODE_TABLE='TYPE_OPERATION' AND CERT_ID IN (SELECT CERT_ID FROM CERTIFIE WHERE CERT_PART_ID='".txt_db($_SESSION['part_id'])."') ORDER BY TYPE_OPERATION.TYPE_OPE_LIBELLE ASC";
			}
			$qry_operations	 = $db->query($sql_operations);

			
			// Si le formulaire est posté, on stocke les candidats dans un tableau en session
			if ($_POST['affect']){
				//unset($_SESSION['checked_cand']);
				$_SESSION['checked_cand'] = $_POST['affect_cand'];
				header('location:admvak_certifie_affectCandidat.php?certid='. $_GET['certid']);
/*
				foreach($_SESSION['checked_cand'] as $checked_candidat){
					echo $checked_candidat.'<br>';
				}
	*/			
				}
				
			if ($_GET['actif']=='1' || !isset($_GET['actif'])){
				$sql_contact_actif = " AND CAND_ACTIF='1'";
			}
		
			switch ($_GET['case']){
				case 'client':
					/* Si on cherche des candidats par client */
					if (isset($_GET['cliid'])){
						// Sélection des infos client
						$sql_cli = "SELECT CLI_ID, CLI_NOM, CLIENT.CLI_VILLE FROM CLIENT WHERE LOWER(CLI_NOM) LIKE '".txt_db(strtolower($_GET['cliid']))."%'";
						//echo $sql_cli;
						//$qry_cli = $db->query($sql_cli);
						$and = "";
						if($_GET['certid']==''){
							$and = " AND CERT_ID IN (SELECT CERT_ID FROM CERTIFIE WHERE CERT_PART_ID='".txt_db($_SESSION['part_id'])."') ";
						}
						else{
							$and = " AND CERT_ID = '".txt_db($_GET['certid'])."'";
						}
						// Sélection des candidats
						$sql_cand = "SELECT CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM, MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, ";
						$sql_cand .= " MAX(CAND_ACTIF) CAND_ACTIF , MAX(AGE) AGE, MAX(DATE_DEB) DATE_DEB,  MAX(DATE_FIN) DATE_FIN, MAX(OPE_ID) OPE_ID FROM CAND_OPE WHERE LOWER(CLI_NOM) LIKE '".txt_db(strtolower($_GET['cliid']))."%' ".$and.$sql_contact_actif." GROUP BY CAND_ID ORDER BY CLI_NOM,CAND_NOM,CAND_PRENOM";
						//echo $sql_cand;
						/* On effectue la requête */
						$sql_cand = "SELECT CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM, MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, ";
						$sql_cand .= " MAX(CAND_ACTIF) CAND_ACTIF , MAX(AGE) AGE, MAX(LAST_OPR) LAST_OPR, '' DATE_DEB,  '' DATE_FIN, '' OPE_ID FROM CAND_CERT WHERE CLI_ID>0 AND LOWER(CLI_NOM) LIKE '".txt_db(strtolower($_GET['cliid']))."%' ".$and.$sql_contact_actif." GROUP BY CAND_ID ORDER BY CLI_NOM,CAND_NOM,CAND_PRENOM";
						
						//echo $sql_cand;
						$qry_cand = $db->query($sql_cand);
					}
				break;
				
				case 'nom':
					/* Si on cherche des candidats par nom */						
					if (isset($_GET['cand_nom'])){
					
						if($_GET['certid']==''){
							$and = " AND CERT_ID IN (SELECT -1 FROM DUAL UNION SELECT CERT_ID FROM CERTIFIE WHERE CERT_PART_ID=".txt_db($_SESSION['part_id']).") ";
						}
						else{
							$and = " AND CERT_ID IN(".txt_db($_GET['certid']).",-1)";
						}
						// Sélection des candidats
						$sql_cand = "SELECT CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM, MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, ";
						$sql_cand .= " MAX(CAND_ACTIF) CAND_ACTIF , MAX(AGE) AGE, MAX(DATE_DEB) DATE_DEB,  MAX(DATE_FIN) DATE_FIN, MAX(OPE_ID) OPE_ID FROM CAND_OPE WHERE LOWER(CAND_NOM) LIKE '".txt_db(strtolower($_GET['cand_nom']))."%' ".$and.$sql_contact_actif." GROUP BY CAND_ID ORDER BY CLI_NOM,CAND_NOM,CAND_PRENOM";
						$sql_cand = "SELECT CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM, MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, ";
						$sql_cand .= " MAX(CAND_ACTIF) CAND_ACTIF , MAX(AGE) AGE, MAX(LAST_OPR) LAST_OPR, '' DATE_DEB,  '' DATE_FIN, '' OPE_ID FROM CAND_CERT WHERE LOWER(CAND_NOM) LIKE '".txt_db(strtolower($_GET['cand_nom']))."%' ".$and.$sql_contact_actif." GROUP BY CAND_ID ORDER BY CLI_NOM,CAND_NOM,CAND_PRENOM";
						//echo $sql_cand;
						/* On effectue la requête */
						$qry_cand = $db->query($sql_cand);
					}
					
				break;
				
				case 'operation':
					/* Si on cherche des candidats par nom */
					if ($_GET['opeid']!=''){
					
						$sql_libelle_ope 	= "SELECT TYPE_OPE_LIBELLE FROM TYPE_OPERATION, OPERATION WHERE TYPE_OPERATION.TYPE_OPE_ID=OPERATION.TYPE_OPE_ID AND TYPE_OPERATION.TYPE_OPE_ID='".txt_db($_GET['opeid'])."'";
						$libelle_ope		= $db->query($sql_libelle_ope);
						
						if($_GET['certid']==''){
							$and = " AND CERT_ID IN (SELECT CERT_ID FROM CERTIFIE WHERE CERT_PART_ID='".txt_db($_SESSION['part_id'])."') ";
						}
						else{
							$and = " AND CERT_ID = '".txt_db($_GET['certid'])."'";
						}
						// Sélection des candidats
						$sql_cand = "SELECT CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM, MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, ";
						$sql_cand .= " MAX(CAND_ACTIF) CAND_ACTIF , MAX(AGE) AGE, MAX(DATE_DEB) DATE_DEB,  MAX(LAST_OPR) LAST_OPR, MAX(DATE_FIN) DATE_FIN, MAX(CAND_OPE.OPE_ID) OPE_ID FROM CAND_OPE, OPERATION WHERE CAND_OPE.OPE_ID=OPERATION.OPE_ID AND OPERATION.TYPE_OPE_ID='".txt_db(intval($_GET['opeid']))."' ".$and.$sql_contact_actif." GROUP BY CAND_ID ORDER BY CLI_NOM,CAND_NOM,CAND_PRENOM";
						//echo $sql_cand;
						/* On effectue la requête */
						$qry_cand = $db->query($sql_cand);
						
					}
				break;
			}
			
			if ($_GET['candid']>0){
				$sql_detail_cand = "SELECT CAND_OPE.*,TO_CHAR(DATE_DEB, 'YYYYMMDDHH24MISS') TIME_DEB, TO_CHAR(DATE_FIN, 'YYYYMMDDHH24MISS') TIME_FIN,CERTIFIE.CERT_NOM FROM CAND_OPE,CERTIFIE WHERE CAND_OPE.CERT_ID=CERTIFIE.CERT_ID AND CAND_OPE.CERT_ID_INI=CERTIFIE.CERT_ID AND CAND_ID='".txt_db(intval($_GET['candid']))."' ORDER BY TO_CHAR(DATE_CREATION,'YYYYMMDD') DESC, OPE_ID DESC,DOSSIER_ID";
				//echo $sql_detail_cand ;
				$detail_cand  		= $db->query($sql_detail_cand);
			}
			
		}
		
	}
	if(!isset($_GET['actif'])){
		$query_str = $_SERVER['QUERY_STRING'];
	}else{
		$query_str = substr($_SERVER['QUERY_STRING'], 0, (strlen($_SERVER['QUERY_STRING'])-8));
	}
?>	
<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo.css" type="text/css">
<link rel="stylesheet" href="../css/general.css" type="text/css">
<script language="JavaScript">
<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function change_cert(cert_id){
	document.location.href = 'admsocvak_gest_Candidats.php?certid='+cert_id;
}

function charge_ope(typeopeid){
	var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
	var filename = "ajax_ope_liste2.php"; // La page qui réceptionne les données
	var data     = null; 
	
	if 	(typeopeid.length>0){ //Si le code postal tapé possède au moins 2 caractères
		document.getElementById("ope_liste").innerHTML='<?php echo $t_patientez ?>...';
		var xhr_object = null; 
			 
			if(window.XMLHttpRequest) // Firefox 
			   xhr_object = new XMLHttpRequest(); 
			else if(window.ActiveXObject) // Internet Explorer 
			   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
			else { // XMLHttpRequest non supporté par le navigateur 
			   alert("<?php echo $t_browser_support_error_1 ?>");
			   return; 
			} 
			 
			
			 
			if(typeopeid != ""){
				data = "certid=<?php echo $_GET['certid']?>&partid=<?php echo $_SESSION['part_id']?>&type_ope="+typeopeid;
			}
			if(method == "GET" && data != null) {
			   filename += "?"+data;
			   data      = null;
			}
			 
			xhr_object.open(method, filename, true);

			xhr_object.onreadystatechange = function() {
			   if(xhr_object.readyState == 4) {
				  var tmp = xhr_object.responseText.split(":"); 
				  if(typeof(tmp[0]) != "undefined") { 
					 document.getElementById("ope_liste").innerHTML = '';
					 if (tmp[0]!=''){
						document.getElementById("ope_liste").innerHTML = tmp[0];
						document.getElementById('action_ope').disabled=false;
						document.getElementById('action_ope').checked=true;
					 }else{
						document.getElementById('action_ope').disabled=true;
						document.getElementById('action_ope').checked=false;
						document.getElementById("ope_liste").innerHTML = "<?php echo $t_aucune_operation_type ?>";
					}
				  }
			   } 
			} 

			xhr_object.send(data); //On envoie les données
	}
}

function show_actif(){
	if(document.getElementById('actif').checked==true){
		document.location.href='admsocvak_gest_Candidats.php?<?php echo $query_str ?>&actif=1';
	}else{
		document.location.href='admsocvak_gest_Candidats.php?<?php echo $query_str ?>&actif=0';
	}
}

function coche(field) {
	if (document.getElementById('tous').checked == true ){
		for (i=0;i<field.length;i++)
		{
		field[i].checked = true;
		}
	}else{
		for (i=0;i<field.length;i++)
		{
		field[i].checked = false;
		}
	}
}

function annul_saisie(opeid){
	if(confirm("<?php echo $t_sur_dannuler_saisie ?>")){
		document.location.href='admsocvak_gest_Candidats.php?<?php echo $query_str ?>&annul_saisie='+opeid+'&actif=<?php echo $_GET['actif'] ?>';
	}
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000">
  <table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
	  <td align="center"> 
		<?php
		include("menu_top.php");
		?>
	  </td>
	</tr>
  </table>
  <br>
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
<tr> 
  <td width="20">&nbsp;</td>
  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $t_cands ?></td>
</tr>
</table>
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td align="center"> 
      <table width="961" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="left" valign="top"> 
		  	<form method="post" action="#">
            <table width="961" border="0" cellspacing="0" cellpadding="0" class="fond_tablo_candidats">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="left" class="TX"> 
                  <table border="0" cellspacing="0" cellpadding="0" width="100%" align="left">
                    <tr> 
                      <td class="TX" align="right"><?php echo $t_cand_opt_select_part ?>&nbsp;:&nbsp;</td>
					  <td class="TX" align="left" width="500">					  
						<select disabled name="select_part" id="select_part" class="form_ediht_Candidats"  onchange="change_part(document.getElementById('select_part').options[document.getElementById('select_part').selectedIndex].value)">
							<option value="0"><?php echo $t_cand_part_tous ?></option>
							<?php 
							if (is_array($qry_part_list)){
								foreach($qry_part_list as $partenaire){
									unset($selected);
									if ($_SESSION['part_id']==$partenaire['part_id']){
										$selected = ' selected="selected"';
									}
									echo '<option value="'.$partenaire['part_id'].'"'.$selected.'>'.ucfirst($partenaire['part_nom']).'</option>';
								}
							}
							?>
						 </select>
                        &nbsp; 
						  <input type="hidden" name="step" value="1"></td>
                    </tr>
                  </table>
                </td>
                <td width="14"></td>
              </tr>
			  
			<?php
			if (isset($_SESSION['part_id'])){
				if (is_array($qry_cert)){
				?>
				<tr> 
                <td width="14"></td>
                <td align="left" class="TX"> 
                  <table border="0" cellspacing="0" cellpadding="0" width="100%" align="left">
                    <tr> 
                      <td class="TX" align="right"><?php echo $t_cand_opt_select_cert ?>&nbsp;:&nbsp;</td>
					  <td class="TX" align="left" width="500">
						<select id="select_cert" id="select_cert" name="select_cert" class="form_ediht_Candidats" onchange="change_cert(document.getElementById('select_cert').options[document.getElementById('select_cert').selectedIndex].value)">
						  <option value=""><?php echo $t_cand_cert_tous ?></option>
						  <?php
						  foreach ($qry_cert as $cert){
							unset($selected_cert);
							if ($cert['cert_id'] == $_GET['certid']){
								$selected_cert = ' selected="selected"';
							}
							echo '<option value="'.$cert['cert_id'].'"'.$selected_cert.'>'.$cert['cert_nom'].' '.$cert['cert_prenom'].'</option>';
						  }
						  ?>
						</select>
					  </td>
					 </tr>
				  </table>
				</td>
				<td width="14"></td>
			    </tr>
					<?php
						}else{
						?>
						<tr> 
						  <td class="TX" align="center" colspan="3"><?php echo $t_aucun_certifie ?> <a href="#" onClick="MM_openBrWindow('admvak_crea_contactClient.php?partid=<?php echo $_SESSION['part_id'] ?>','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')"><?php echo $t_cliquant_ici ?></a>.</td>
						</tr>
						<?php
						}
			}
		if (isset($_SESSION['part_id']) && isset($_GET['certid'])){
			?>
					<tr> 
					  <td width="14"></td>
					  <td align="left" class="TX_Candidats"><?php echo $t_cand_rechercher ?></td>
					  <td width="14"></td>
					</tr>
					<tr> 
					  <td width="14" height="1"></td>
					  <td align="left" bgcolor="#666666"  height="1"></td>
					  <td width="14" height="1"></td>
					</tr>
					<tr> 
					  <td width="14"></td>
					  <td align="center" class="TX"> 
						<table border="0" cellspacing="0" cellpadding="0" width="450">
						  <tr> 
							<td class="TX" align="left" width="55%">&nbsp;</td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left"><input type="radio" id="action_client" name="choix_action" value="client" <?php if (!isset($_GET['case']) || $_GET['case']=='client') echo 'checked="checked"'; ?>>&nbsp;<b><?php echo $t_cand_par_soc ?> </b></td>
							<td class="TX" align="left">&nbsp; </td>
						  </tr>
						  <?php
						 #################################################################
						 #######            SI UN CLIENT EST REPERTORIE            #######
						 #################################################################
							?>
							  <tr> 
								<td class="TX" align="left"> 
								  <?php echo $t_cand_saisir_societe ?> :
								<?php
								/*if (is_array($qry_liste_clients)){
									  ?>
									  <select name="select_client" class="form_ediht_Candidats">
										<?php
										foreach($qry_liste_clients as $client_liste){
											unset($selected_cli);
											if($_GET['cliid'] == $client_liste['cli_id']){
												$selected_cli = ' selected="selected"';
											}
											echo '<option value="'.$client_liste['cli_id'].'"'.$selected_cli.'>'.strtoupper($client_liste['cli_nom']).' - '.ucfirst($client_liste['cli_ville']).'</option>';
										}
										?>
									  </select>
									  <?php
								  }else{
									//echo '<tr><td colspan="2" class="TX">Aucun Client n\'est répertorié pour ce certifié.</td></tr>';
								  }
								  */
								  ?>
								  <input onclick="document.getElementById('action_client').checked=true;" type="text" class="form_ediht_Candidats" size="40" maxlength="40" name="select_client">
								</td>
								<td class="TX" align="left" valign="bottom"> 
								<?php
								if($_GET['certid']>0){
								?>
								  <input class="bn_ajouter" type="button" value="<?php echo $t_btn_creer_societe ?>" onClick="MM_openBrWindow('admvak_crea_certifieClient.php?certid=<?php echo $_GET['certid'] ?>','crea_client','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')"></td>
								 <?php
								 }
								 ?>
								 </td>
							  </tr>
						  <tr> 
							<td class="TX" align="left">&nbsp;</td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left"><input <?php if (!is_array($qry_operations) || !is_array($qry_type_ope)){ echo 'disabled'; }else{ if ($_GET['case']=='operation') echo 'checked="checked"'; }?> type="radio" id="action_ope" name="choix_action" value="operation">&nbsp;<b><?php echo $t_cand_par_ope ?> </b></td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						 <?php
						 #################################################################
						 ########       SI UNE OPERATION EST REPERTORIE            #######
						 #################################################################
							 ?>
							 <tr> 
								<td class="TX" align="left" colspan="2">
								<?php
									if (is_array($qry_operations)){
									?>
								  <?php echo $t_cand_opt_select_ope ?> :&nbsp;<br>
									<?php echo $t_cand_type_ope ?> :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									  <select name="select_type_op" id="select_type_op" class="form_ediht_Candidats" onchange="charge_ope(document.getElementById('select_type_op').options[document.getElementById('select_type_op').selectedIndex].value);">
										<option value="all" selected><?php echo $t_tous ?></option>
										<?php
										if (is_array($qry_type_ope)){
											foreach($qry_type_ope as $type_ope){
												echo '<option value="'.$type_ope['code_id'].'">'.$type_ope['code_libelle'].'</option>';
											}
										?>
									  </select>
									  <div style="display: inline;" id="ope_liste" width="200">
										<?php
											echo '<select onclick="document.getElementById(\'action_ope\').checked=true;" name="select_operation" id="select_operation" class="form_ediht_Candidats">';
											foreach ($qry_operations as $operations){ // Tant que la requete renvoie un résultat
												unset($selected_opeid);
												if ($_GET['opeid'] == $operations['type_ope_id']){
													$selected_opeid = ' selected="selected"';
												}
												echo '<option value="'. $operations['type_ope_id'].'"'.$selected_opeid.'>'. $operations['type_ope_libelle'] .'</option>';
											}
											echo '</select>';								
										}
										?>
									  </div>
								  <?php
								  }else{
									echo $t_aucune_operation_cert;
								  }
								  ?>
									</td>
							  </tr>
						  <tr> 
							<td class="TX" align="left">&nbsp;</td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						  <tr> 
							<td class="TX" align="left"><input type="radio" id="action_nom" name="choix_action" value="nom" <?php if ($_GET['case']=='nom') echo 'checked="checked"'; ?>>&nbsp;<b><?php echo $t_cand_par_cand ?></b></td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						<?php
						 #################################################################
						 ########         SI UN CANDIDAT EST REPERTORIE            #######
						 #################################################################
								?>							
							  <tr> 
								<td class="TX" align="left">
								  <?php echo $t_cand_saisir_nom ?> :<br>
								  <input type="text" onclick="document.getElementById('action_nom').checked=true;" maxlength="55" size="40" name="cand_nom" class="form_ediht_Candidats">
								</td>
								<td class="TX" align="left" valign="bottom">
								<?php
								if($_GET['certid']>0){
									?>
									<input class="bn_ajouter" type="button" value="<?php echo $t_btn_creer_candidat ?>" onClick="MM_openBrWindow('admvak_certifie_nvoCandidat.php?certid=<?php echo $_GET['certid'] ?>','crea_cand','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')">
									<?php
								}
								?>
								</td>
							  </tr>
						  <tr> 
							<td class="TX" align="left">&nbsp;</td>
							<td class="TX" align="left">&nbsp;</td>
						  </tr>
						</table>
					  </td>
					  <td width="14"></td>
					</tr>  
		<?php
		}
		?>
					<tr> 
					  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
					  <td height="14"></td>
					  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
					</tr>
				  </table>
			  <br>
			</td>
		</tr>
		<?php
		if (isset($_SESSION['part_id']) && isset($_GET['certid'])){
		?>
		<tr>
			<td align="center"><input type="hidden" name="step" value="3">
				<input type="submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_candidat">
			</td>
		</tr>
		<?php
		}
		?>
		</form>
		<form action="#" method="post" name="form">	
		<tr><td>&nbsp;</td></tr>	
		<?php
		if (isset($_GET['case'])){
		?>

			<tr> 
			  <td align="left" valign="top">
				<?php	
				if($_GET['case']=='nom'){
					if($_GET['cand_nom']!=''){
						$str_nom = $t_cand.' : '.$_GET['cand_nom'];
					}else{
						$str_nom = $t_cand_tous;
					}
					if($_GET['certid']>0){
						$str_cert = $qry_cert2[0]['cert_nom'];
					}else{
						$str_cert = $t_cert_tous;
					}
					?>
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_part[0]['part_nom']?>&nbsp;>&nbsp;<?php echo $str_cert ?>&nbsp;>&nbsp;<?php echo $str_nom ?></td>
					</tr>
					</table>
					<?php
				}	
				if($_GET['case']=='client'){
					if($_GET['cliid']!=''){
						$str_nom = $t_societe.' : '.$_GET['cliid'];
					}else{
						$str_nom = $t_societes_toutes;
					}
					if($_GET['certid']>0){
						$str_cert = $qry_cert2[0]['cert_nom'];
					}else{
						$str_cert = $t_cert_tous;
					}
					?>
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_part[0]['part_nom']?>&nbsp;>&nbsp;<?php echo $str_cert ?>&nbsp;>&nbsp;<?php echo $str_nom ?></td>
					</tr>
					</table>
					<?php
				}
				if($_GET['case']=='operation'){
					if($_GET['opeid']>0){
						
						$str_nom = $t_ope.' : '.$libelle_ope[0]['type_ope_libelle'];
					}else{
						$str_nom = $t_opes_toutes;
					}

					if($_GET['certid']>0){
						$str_cert = $qry_cert2[0]['cert_nom'];
					}else{
						$str_cert = $t_cert_tous;
					}
					?>
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $qry_part[0]['part_nom']?>&nbsp;>&nbsp;<?php echo $str_cert ?>&nbsp;>&nbsp;<?php echo $str_nom ?></td>
					</tr>
					</table>
					<?php
				}
				?>
				<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td class="TX_Candidats"><?php echo $t_cands ?></td>
					  <td width="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td height="1" bgcolor="#000000"></td>
					  <td width="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td>&nbsp;</td>
					  <td width="14"></td>
				  </tr>
			<?php
			/* On a sélectionné un mode de recherche de candidats */
			switch($_GET['case']){
			
				/* Si on cherche les candidats d'un client */
				case 'client':
				case 'nom':
				case 'operation':
				?>
				<a name="bas"></a>
				  <tr> 
					<td width="14"></td>
					<td align="center" class="TX"> 
					  <table width="900" border="0" cellspacing="0" cellpadding="5" class="TX">
						<?php
						if (is_array($qry_cand)){
							?>
						  <tr align="left"> 
							<td colspan="10" class="TX" style="text-align: right;"> 
							  <input type="checkbox" id="actif" name="actif" onclick="show_actif()" value="1" <?php if($_GET['actif']=='1' || !isset($_GET['actif'])){ echo ' checked="checked"'; } ?>>
							<?php echo $t_cand_actif_only ?></td>
						  </tr>
							<tr align="center"> 
								<td class="TX_bold" align="center"><input type="checkbox" value="1" name="tout_coche" id="tous" onclick="coche(document.form.affect_cand)"><?php echo $t_select ?></td>
								<td class="TX_bold" align="center"><?php echo $t_societe ?></td>
								<td class="TX_bold" align="center"><?php echo $t_nom ?></td>
								<td class="TX_bold" align="center"><?php echo $t_prenom ?></td>
								<td class="TX_bold" align="center"><?php echo $t_age ?></td>
								<td class="TX_bold" align="center"><?php echo $t_sexe ?></td>
								<td class="TX_bold" align="center"><?php echo $t_last_OPR?></td>
								<td class="TX_bold" align="center"><?php echo $t_etat ?></td>
								<td class="TX_bold" align="center"><?php echo $t_statut ?></td>
								<td class="TX_bold" align="center"><?php echo $t_dossiers ?></td>
							<tr>
						<?php
							foreach($qry_cand as $list_candidat){
							
								$verif_passage = false;
								
								$sql_cand_det 	= "SELECT MAX(OPE_ID) OPE_ID FROM cand_a_ope where cand_id='".txt_db(intval($list_candidat['cand_id']))."'";
								$qry_cand_det  	= $db->query($sql_cand_det);
								
								$sql_cand_det_date = "SELECT TO_CHAR(DATE_DEB, 'YYYYMMDDHH24MISS') TIME_DEB, TO_CHAR(DATE_FIN, 'YYYYMMDDHH24MISS') TIME_FIN FROM CAND_A_OPE WHERE CAND_ID='".txt_db(intval($list_candidat['cand_id']))."' AND OPE_ID='".$qry_cand_det[0]['ope_id']."'";
								$qry_cand_det  	= $db->query($sql_cand_det_date);
									
								$bgcolor = "#F1F1F1";
//								foreach($qry_cand_det as $ope_cand){
//									if(!$verif_passage){									
										$cand_pn = 'E-:';
										$cand_profil ='';
//										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$list_candidat['ope_id']." AND CAND_ID=".$list_candidat['cand_id']." AND REPONSE_PN='E'";
//										$qry_cand_pn = $db->query($sql_cand_pn);
//										$cand_pn = $cand_pn.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
//										$sql_cand_profil ="select distinct CODE_REGROUP_NOM from cand_comb where TXT_IMG_TYPE_PROFIL_CODE_ID=30 and TXT_IMG_QUEST_ID=241 and TXT_IMG_PRECISION_CODE_ID=34 and COMBI_E_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
//										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$list_candidat['ope_id']." AND CAND_ID=".$list_candidat['cand_id']." AND REPONSE_PN='C'";
//										$qry_cand_pn = $db->query($sql_cand_pn);
//										$cand_pn = $cand_pn.'C-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
//										$sql_cand_profil = $sql_cand_profil." and COMBI_C_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
//										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$list_candidat['ope_id']." AND CAND_ID=".$list_candidat['cand_id']." AND REPONSE_PN='P'";
//										$qry_cand_pn = $db->query($sql_cand_pn);
//										$sql_cand_profil = $sql_cand_profil." and COMBI_P_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
//										$cand_pn = $cand_pn.'P-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
//										$sql_cand_pn = "select * from cand_pn WHERE OPE_ID=".$list_candidat['ope_id']." AND CAND_ID=".$list_candidat['cand_id']." AND REPONSE_PN='A'";
//										$qry_cand_pn = $db->query($sql_cand_pn);
//										$cand_pn = $cand_pn.'A-:'.$qry_cand_pn[0]['cpt'].'-'.$qry_cand_pn[0]['cor_var_points'].'-'.$qry_cand_pn[0]['cor_regle_segment'].'-'.$qry_cand_pn[0]['cor_regle_bloc'].'<br>';
//										$sql_cand_profil = $sql_cand_profil." and COMBI_A_INF='".$qry_cand_pn[0]['cor_regle_segment']."'";
//										$qry_cand_pn = $db->query($sql_cand_profil);
//										$cand_profil = $qry_cand_pn[0]['code_regroup_nom'];
										
//										$verif_passage = true;
//									}
									
									if(is_array($qry_cand_det)){
										if ($qry_cand_det[0]['time_deb']!='' && $qry_cand_det[0]['time_fin']!=''){
											//Vert
											$bgcolor="#00FF00";
										}else{
											if ($qry_cand_det[0]['time_deb']!=''){
												//Orange
												$bgcolor="#FFAA20";
											}else{
												//Rouge
												$bgcolor="#FF0000";
											}
										}
									}
//								}
							
								?>
								<tr align="center"> 
									<td valign="top">
									<?php
									if ($list_candidat['cand_actif']=='1'){
										?>
										<input type="checkbox" value="<?php echo $list_candidat['cand_id'] ?>" id="affect_cand" name="affect_cand[]">
										<?php
									}
									?>
									</td>
									<td class="TX" align="center">
									<?php 
									if($list_candidat['cli_id']>0){
										echo '<a href="#bas" onclick="MM_openBrWindow(\'admvak_edit_certifieClient.php?certid='.$list_candidat['cert_id'].'&cliid='.$list_candidat['cli_id'].'\',\'fiche_clt_'.$list_candidat['cli_id'].'\',\'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500\')">'.strtoupper($list_candidat['cli_nom']).'</a>'; 
									}else{
										echo $t_cand_libre;
									}
									?></td>
									 <td class="TX" valign="top">
									 <?php 
									 
									 echo '<a href="#bas" onClick="MM_openBrWindow(\'admvak_certifie_edit_Candidat.php?candid='.$list_candidat['cand_id'].'\',\'fiche_candidat_'.$list_candidat['cand_id'].'\',\'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500\')">'.$list_candidat['cand_nom'].'</a>';
									 
									 ?>
									 
									 </td>
									<td class="TX" align="center"><?php echo $list_candidat['cand_prenom'] ?></td>
									<td class="TX" align="center"><?php echo $list_candidat['age'] ?></td>
									<td class="TX" align="center"><?php echo $list_candidat['cand_sexe'] ?></td>
									<td class="TX" align="center"><?php echo $list_candidat['last_opr'] ?></td>
									<td class="TX" valign="top">
									<?php
										echo '<div style="color:'.$bgcolor.'; background-color:'.$bgcolor.'; border-bottom: 1px solid #F1F1F1;">&nbsp;</div>';
									?>
								  </td>
									<td class="TX" align="center">
									<?php
									if ($list_candidat['cand_actif']=='1'){
										echo '<span style="color:#00FF00; font-weight: bold;">ACTIF</span>';
									}else{
										echo '<span style="color:#FF0000; font-weight: bold;">INACTIF</span>';
									}
									?>
									</td>
									<td class="TX" align="center"><a href="admsocvak_gest_Candidats.php?certid=<?php echo $_GET['certid'] ?>&case=<?php echo $_GET['case'] ?>&cliid=<?php echo $_GET['cliid'] ?>&opeid=<?php echo $_GET['opeid'] ?>&candid=<?php echo $list_candidat['cand_id'] ?>&cand_nom=<?php echo $_GET['cand_nom'] ?>&cliid=<?php echo $_GET['cliid'] ?>&opeid=<?php echo $_GET['opeid'] ?>&actif=<?php echo intval($_GET['actif']) ?>#2"><?php echo $t_consultez ?></a></td>
								<tr>
								<?php
							}
						}else{
						?>
						<tr align="left"> 
							<td colspan="10" class="TX" style="text-align: center;"> 
							  <?php echo $t_aucun_candidat ?>
							 </td>
						</tr>
						<?php
						}
						?>
						<tr align="center"> 
							<td colspan="12" class="TX_GD">&nbsp;</td>
						</tr>
					  </table>
					</td>
					<td width="14"></td>
				  </tr>
		      <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
			<br>
			<?php
			break;
			}
			
			// 2eme TABLEAU DES CANDIDATS
			if($_GET['candid']>0){
				?>
				<a name="2"></a>
				<?php
				if(is_array($detail_cand)){
				if($detail_cand[0]['cli_nom']!=''){
					$client = ', '.ucfirst($detail_cand[0]['cli_nom']);
				}else{
					$client = ', '.$t_cand_libre;
				}
			?>
				<table width="740" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Candidats2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo strtoupper($detail_cand[0]['cand_nom']).' '.ucfirst($detail_cand[0]['cand_prenom']).$client ?></td>
				</tr>
				</table>
				<table width="740" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td class="TX_Candidats"><?php echo $t_cand_recap_dossiers ?></td>
					  <td width="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td height="1" bgcolor="#000000"></td>
					  <td width="14"></td>
				  </tr>
				  <tr>
					  <td width="14"></td>
					  <td>&nbsp;</td>
					  <td width="14"></td>
				  </tr>
					<tr><td width="14"></td>
					<td class="TX">
						<table width="700" border="0" cellspacing="0" cellpadding="0">						
							  <tr align="center"> 
								<td colspan="10" class="TX_GD">&nbsp;</td>
							  </tr>
							  <tr>
								<td  class="TX_bold"><?php echo $t_date_produit ?></td>
								<td  class="TX_bold"><?php echo $t_date_quest ?></td>
								<td  class="TX_bold"><?php echo $t_cand_code_acces ?></td>
								<td  class="TX_bold"><?php echo $t_produit ?></td>
								<td  class="TX_bold"><?php echo $t_dossiers ?></td>
								<td  class="TX_bold"><?php echo $t_ope ?></td>
								<td  class="TX_bold"><?php echo $t_cert ?></td>
								<td  class="TX_bold"><?php echo $t_duree ?></td>
								<td  class="TX_bold" align="center"><?php echo $t_etat ?></td>
								<td  class="TX_bold" align="center"><?php echo $t_suppression ?></td>
							  </tr>
							  <tr> 
								<td height="1" bgcolor="#000000" colspan="10"> </td>
							  </tr>
							<?php
							$ope_id_sav = '';
							foreach($detail_cand as $candidat){
								$sql_verif_droit = "SELECT * FROM PRODUIT, CERT_A_CERTIF, PRODUIT_A_CERTIF 
								WHERE CERT_A_CERTIF.CERTIF_CERT_ID = '".txt_db($_GET['certid'])."' 
								AND (CERT_A_CERTIF.CERTIF_CERTIFICATION IS NOT NULL 
								AND (CERT_A_CERTIF.CERTIF_SUSPENDU<>'1' OR CERT_A_CERTIF.CERTIF_SUSPENDU IS NULL)
								AND (CERTIF_CERTIFICATION IS NOT NULL AND TO_CHAR(CERTIF_CERTIFICATION, 'YYYYMMDD')<=TO_CHAR(SYSDATE, 'YYYYMMDD')) OR CERTIF_FORMATION = 1) 
								AND PRODUIT_A_CERTIF.PROD_ID =  PRODUIT.PROD_ID 
								AND PRODUIT_A_CERTIF.CODE_ID = CERT_A_CERTIF.CERTIF_CODE_ID 
								AND PRODUIT.PROD_ID='".intval($candidat['prod_id'])."' 
								ORDER BY PROD_NOM";
							/*
								$sql_verif_droit = "SELECT * FROM PRODUIT, CERT_A_CERTIF, PRODUIT_A_CERTIF 
									WHERE CERT_A_CERTIF.CERTIF_CERT_ID = '".intval($_GET['certid'])."' 
									AND CERT_A_CERTIF.CERTIF_CERTIFICATION IS NOT NULL 
									AND (CERT_A_CERTIF.CERTIF_SUSPENDU<>'1' OR CERT_A_CERTIF.CERTIF_SUSPENDU IS NULL)
									AND ((CERTIF_CERTIFICATION IS NOT NULL AND TO_CHAR(CERTIF_CERTIFICATION, 'YYYYMMDD')<=TO_CHAR(SYSDATE, 'YYYYMMDD')) OR CERTIF_FORMATION = 1) 
									AND PRODUIT_A_CERTIF.PROD_ID =  PRODUIT.PROD_ID 
									AND PRODUIT_A_CERTIF.CODE_ID = CERT_A_CERTIF.CERTIF_CODE_ID 
									AND PRODUIT.PROD_ID='".intval($candidat['prod_id'])."' ";
							*/
								$qry_verif_droit = $db->query($sql_verif_droit);
							
								?>
								
								<tr>
								<?php
								  if ($ope_id_sav != $candidat['ope_id']){
									$ope_id_sav = $candidat['ope_id'];
								  
								  ?>
								  <td class="TX" valign="top"><?php echo $candidat['date_creation'] ?></td>
								  <td class="TX" valign="top"><?php echo $candidat['date_creation'] ?></td>
								  <td class="TX" valign="top">
								  <?php
								  if($candidat['quest_a_saisir']=='1'){
									echo $candidat['code_acces'];
								  }
								  ?>
								  </td>
								  <td class="TX" valign="top"><?php echo $candidat['prod_nom'] ?></td>
								  <?php
								  }else{
								  ?>
								  <td class="TX" valign="top"></td>
								  <td class="TX" valign="top"></td>
								  <td class="TX" valign="top"></td>
								  <td class="TX" valign="top"></td>
								  <?php
								  }
								  ?>
								  <td class="TX" valign="top">
								  <?php
								  if ($candidat['time_fin']!='' && is_array($qry_verif_droit)){
									  ?>
									  <a href="gen_doc.php?candid=<?php echo $candidat['cand_id'] ?>&opeid=<?php echo $ope_id_sav ?>&dossid=<?php echo $candidat['dossier_id'] ?>" target="_blank">
									  <?php 
								  }
									  echo $candidat['dossier_nom']; ?>
								  <?php
								  if ($candidat['time_fin']!='' && is_array($qry_verif_droit)) echo '</a>';
								  ?>									  
								  </td>
								  <td class="TX" valign="top">
								  <?php
									echo $candidat['type_ope_libelle'];
								  ?>
								  </td>
								  <td class="TX" valign="top">
								  <?php
										echo $candidat['cert_nom'];
								  ?>
								  </td>
								  <td class="TX" valign="top">
								 <?php								
										/* Si la personne a commencé son questionnaire */
										if ($candidat['time_deb']!=''){
											$deb 	= mktime(substr($candidat['time_deb'], 8, 2), substr($candidat['time_deb'], 10, 2), substr($candidat['time_deb'], 12, 2), substr($candidat['time_deb'], 4, 2), substr($candidat['time_deb'], 6, 2), substr($candidat['time_deb'], 0, 4));
											/* Si la date de fin est définie */
											if ($candidat['time_fin']!=''){
												$fin = mktime(substr($candidat['time_fin'], 8, 2), substr($candidat['time_fin'], 10, 2), substr($candidat['time_fin'], 12, 2), substr($candidat['time_fin'], 4, 2), substr($candidat['time_fin'], 6, 2), substr($candidat['time_fin'], 0, 4));
											}else{
												/* Sinon on prend comme référence le time en cours */
												$fin = time();
											}
											$time_rep = $fin - $deb;
											if ($time_rep>0){
												$time_rep = $time_rep;
											}
											echo duree($time_rep);
										}
								 ?>
								  </td>
								  <td class="TX" valign="top"><?php
								  $bgcolor = "#F1F1F1";
									  if ($candidat['time_deb']!='' && $candidat['time_fin']!=''){
										//Vert
										$bgcolor="#00FF00";
									  }else{
										if ($candidat['time_deb']!=''){
											//Orange
											$bgcolor="#FFAA20";
										}else{
											//Rouge
											$bgcolor="#FF0000";
										}
									  }
									
									  echo '<div style="color:'.$bgcolor.'; background-color:'.$bgcolor.'; border-bottom: 1px solid #F1F1F1;">&nbsp;</div>';
								  ?>
								  </td>
								  <td align="center"> 
								  <?php
								  if($candidat['ope_id'] && $candidat['time_deb']==''){
									  if ($ope_id_sav2 != $candidat['ope_id']){
									  $ope_id_sav2 = $candidat['ope_id'];
									  ?>
										<img src="../images/icon_supp2.gif" onmouseover="this.style.cursor='pointer'" onclick="annul_saisie(<?php echo $candidat['ope_id'] ?>)">
									  <?php
									  }
								  }
								  ?>
								  </td>
								</tr>
								<tr> 
								  <td colspan="10" bgcolor="#CCCCCC" height="1" valign="top"></td>
								</tr>
							<?php
							}
						}else{
								?>
									<table width="740" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr> 
									  <td width="20">&nbsp;</td>
									  <td class="Titre_Candidats2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $t_aucune_operation_cand ?></td>
									</tr>
									</table>
								<?php
							}
						
						}
						?>
						</table>
					</td>
					<td width="14"></td>
				  </tr>
		      <tr> 
		      <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
          </td>
        </tr>		
		<?php
		}
		?>
      </table>
			<br>
			<table width="961">
			<tr><td align="center">
				<?php
				if ($_GET['certid']>0 && $qry_cert2[0]['actif']=='1'){
				?>
					<center><input type="submit" name="affect" value="<?php echo $t_btn_generer_produit ?>" class="bn_valider_candidat_big"></center>
				<?php
				}
				?>
				</td></tr>
			</table>
		</form>
    </td>
  </tr>
</table>
</body>
</html>
<?php
}else{
	include('no_acces.php');
}
?>
