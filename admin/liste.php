<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo.css" type="text/css">
<link rel="stylesheet" href="../css/general.css" type="text/css">
<script language="JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
        <table width="961" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="360"><img src="../images/top2.jpg" width="389" height="121"></td>
		  <td>&nbsp;&nbsp;</td>
          <td><img src="../images/pap1.jpg" width="30" height="30" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="questionnaire.php" class="menu_Gris">Créer les questionnaires</a><br>
            <img src="../images/pap2.jpg" width="30" height="50" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="regles.php" class="menu_Gris">Créer les règles</a> <br>
			<img src="../images/pap4.jpg" width="30" height="30" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="documents.php" class="menu_Gris">Créer vos documents</a><br></td>
          <td valign="middle">
          </td>
          <td valign="middle"><img src="../images/pap5.jpg" width="30" height="30" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="question.php" class="menu_Gris">Diffuser les questionnaires</a><br>
            <img src="../images/pap3.jpg" width="30" height="50" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="resultats.php" class="menu_Gris">Suivre les résultats</a> 
			<br>
            <img src="../images/pap6.jpg" width="30" height="30" border="0" align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="analyser.php" class="menu_Gris">Analyser les résultats</a></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="center" class="menu_Gris">&nbsp;</td>
  </tr>
  <tr> 
    <td align="right"> 
      <table width="961" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="180" valign="top" align="center"><font color="EA98AA" class="TX"><b>LISTE DES GROUPES ET ADJECTIFS</b> 
            </font></td>
          <td align="left" valign="top"> 
            <table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="left" class="TX"> 
                  <table border="1" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="100%">
                    <tr align="center" bgcolor="#C4C4C4"> 
					  <td class="TX">Groupe</td>
                      <td class="TX">Attribut 1</td>
                      <td class="TX">Attribut 2</td>
                      <td class="TX">Attribut 3</td>
                      <td class="TX">Attribut 4</td>
                      <td class="TX" align="center">Modifier</td>
                      <td class="TX" align="center">Supprimer</td>
                    </tr>
                    <tr > 
					  <td align="center">1</td>
					  <td class="TX">Précis</td>
                      <td class="TX">Pondéré</td>
					  <td class="TX">Sociable</td>
                      <td class="TX">Esprit de pionnier</td>
					  <td class="TX" align="center"><img src="modifier.png" border="0" width="20"></td>
                      <td class="TX" align="center"><img src="supprimer.jpg" border="0" width="10"></td>
                    </tr>
					 <tr > 
					  <td align="center">2</td>
					  <td class="TX">Persuasif</td>
                      <td class="TX">Bon caractère</td>
					  <td class="TX">Direct</td>
                      <td class="TX">Décisif</td>
					  <td class="TX" align="center"><img src="modifier.png" border="0" width="20"></td>
                      <td class="TX" align="center"><img src="supprimer.jpg" border="0" width="10"></td>
                    </tr>
                    <tr > 
					  <td align="center">3</td>
					  <td class="TX">Bienveillant</td>
                      <td class="TX">Pondéré</td>
					  <td class="TX">Ne reste pas en place</td>
                      <td class="TX">Prudent</td>
					  <td class="TX" align="center"><img src="modifier.png" border="0" width="20"></td>
                      <td class="TX" align="center"><img src="supprimer.jpg" border="0" width="10"></td>
                    </tr>
                  </table>
                </td>
                <td width="14"></td>
              </tr>
              <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
            <p>&nbsp;</p>
          </td>
        </tr>
  <tr> 
    <td align="right" width="180">&nbsp; </td>
  </tr>
</table>
</body>
</html>
