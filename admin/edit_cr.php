<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']>1){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	/* Params a faire passer : cand_id, ope_id, dossier_id(396099) et prod_id(395574) */
	$db = new db($conn);
	
	if($_GET['candid']>0 && $_GET['opeid']>0 && $_GET['prodid']>0 && $_GET['dossid']>0 && $_GET['certid']>0){
	
		/* Si le formulaire est posté */
		if($_POST['step']>0){
		
			$temp_val = explode('_',$_POST['select_doc']);
			$temp_docid = $temp_val[0];
			$temp_numzone = $temp_val[1];

			/* On a posté le compte rendu */
			
				if (strlen(trim($_POST['txt_cr_theme']))>0 || strlen(trim($_POST['txt_cr_comm']))>0){
					$seq_num_ligne = "SEQ_ID.NEXTVAL";
					/* On supprime l'ancien compte-rendu pour les types LIBRE et pour les mises à jour des autres types*/
					if($_POST['hid_type_cr']==50){
						$sql_del_cr = "DELETE FROM CAND_A_CR WHERE CAND_ID=".intval($_GET['candid'])." AND OPE_ID=".intval($_GET['opeid'])." AND DOC_ID=".intval($temp_docid)." AND NUM_ZONE=".intval($temp_numzone)."";
						$qry_del_cr=$db->query($sql_del_cr);
					}else{
						if($_GET['action']=='edit' && $_GET['numligne']>0){
							$sql_del_cr = "DELETE FROM CAND_A_CR WHERE CAND_ID=".intval($_GET['candid'])." AND OPE_ID=".intval($_GET['opeid'])." AND DOC_ID=".intval($temp_docid)." AND NUM_ZONE=".intval($temp_numzone)." AND NUM_LIGNE=".intval($_GET['numligne'])."";
							$qry_del_cr=$db->query($sql_del_cr);
							$seq_num_ligne = $_GET['numligne'];
						}
					}
					//echo 'GBE1:'.$sql_del_cr.'<br>';
					/* On insère les nouvelles valeurs */
					$sql_insert_cr = "INSERT INTO CAND_A_CR (CAND_ID, OPE_ID, DOC_ID, NUM_ZONE, NUM_LIGNE, THEME, TXT_CR, CERT_ID, DATE_CR) 
					VALUES(".intval($_GET['candid']).", 
					".intval($_GET['opeid']).", 
					".intval($temp_docid).", 
					".intval($temp_numzone).", 
					".$seq_num_ligne.", 
					'".txt_db($_POST['txt_cr_theme'])."', 
					'".txt_db($_POST['txt_cr_comm'])."',
					'".intval($_POST['select_certifie'])."',
					TO_DATE('".txt_db($_POST['JJ_cr']).txt_db($_POST['MM_cr']).txt_db($_POST['AAAA_cr'])."','DDMMYYYY'))";
					$qry_insert_cr=$db->query($sql_insert_cr);
					//echo 'GBE2:'.$sql_insert_cr.'<br>';
					/* On met à jour la date et le certifié pour toutes les zones de tous les documents de l'opération et du candidat sélectionnés */
					$sql_update_date_cert = "UPDATE CAND_A_CR SET DATE_CR=TO_DATE('".txt_db($_POST['JJ_cr']).txt_db($_POST['MM_cr']).txt_db($_POST['AAAA_cr'])."','DDMMYYYY'), 
					CERT_ID='".intval($_POST['select_certifie'])."' WHERE CAND_ID='".intval($_GET['candid'])."' AND OPE_ID='".intval($_GET['opeid'])."'";
					$qry_update_date_cert=$db->query($sql_update_date_cert);
					//echo 'GBE3:'.$sql_update_date_cert.'<br>';
				}
				if($_POST['hid_type_cr']==50){
					$str_complement = '&action=edit&numligne=-1';
				}
				header('location:edit_cr.php?candid='.$_GET['candid'].'&opeid='.$_GET['opeid'].'&partid='.$_GET['partid'].'&prodid='.$_GET['prodid'].'&certid='.$_POST['select_certifie'].'&JJ_cr='.$_POST['JJ_cr'].'&MM_cr='.$_POST['MM_cr'].'&AAAA_cr='.$_POST['AAAA_cr'].'&dossid='.$_GET['dossid'].'&docid='.$temp_docid.'&num_zone='.$temp_numzone.$str_complement.'&idnc='.($_GET['idnc']+1));

		}
		// FIN DU TRAITEMENT DU FORMULAIRE
		########################################################################
		########################################################################
	
		/* On génère la liste de tous les certifiés du partenaire */
		if($_SESSION['droit']>5){
			$sql_liste_certifies = "SELECT CERTIFIE.CERT_ID, CERTIFIE.CERT_NOM, CERTIFIE.CERT_PRENOM FROM CERTIFIE WHERE CERTIFIE.CERT_DATE_SUPPRESSION IS NULL AND actif = 1 ORDER BY CERTIFIE.CERT_NOM, CERTIFIE.CERT_PRENOM";
		}else{
			$sql_liste_certifies = "SELECT CERTIFIE.CERT_ID, CERTIFIE.CERT_NOM, CERTIFIE.CERT_PRENOM FROM CERTIFIE WHERE CERTIFIE.CERT_PART_ID=".$_GET['partid']." AND CERTIFIE.CERT_DATE_SUPPRESSION IS NULL AND actif = 1 ORDER BY CERTIFIE.CERT_NOM, CERTIFIE.CERT_PRENOM";
		}
		//echo $sql_liste_certifies;
		$qry_liste_certifies = $db->query($sql_liste_certifies);
	
		/* On sélectionne les infos de l'opération */
		$sql_info_ope = "SELECT TYPE_OPE_LIBELLE FROM TYPE_OPERATION WHERE TYPE_OPE_ID=".intval($_GET['opeid'])."";
		$qry_info_ope = $db->query($sql_info_ope);
		
		/* On sélectionne les infos du candidat */
		$sql_info_cand = "SELECT CAND_PRENOM, CAND_NOM,CAND_LANG_ID FROM CANDIDAT WHERE CAND_ID=".intval($_GET['candid'])."";
		$qry_info_cand = $db->query($sql_info_cand);
		
		/* On sélectionne les infos du dossier */
		$sql_info_doss = "SELECT DOSSIER_NOM FROM DOSSIER WHERE DOSSIER_ID=".intval($_GET['dossid'])."";
		$qry_info_doss = $db->query($sql_info_doss);
		
		/* On génère la liste des documents qui contiennent au moins un compte rendu à saisir */
		$sql_sel_doc = "SELECT DOCUMENT.DOC_ID, PRODUIT_A_DOC.TRI, DOC_A_INFO.NUM_ZONE, DOC_A_INFO.DOC_NOM_CR,DOC_A_INFO.DOC_TYPE_CR
FROM PRODUIT_A_DOC,DOCUMENT,DOC_A_INFO
WHERE PRODUIT_A_DOC.DOC_ID = DOCUMENT.DOC_ID AND DOCUMENT.DOC_ID = DOC_A_INFO.DOC_ID
and DOC_LANG_ID=".$qry_info_cand[0]['cand_lang_id']." and prod_id=".intval($_GET['prodid'])." and dossier_id=".intval($_GET['dossid'])." AND (DOCUMENT.TYPE_ZONE1=6 OR DOCUMENT.TYPE_ZONE2=6 OR DOCUMENT.TYPE_ZONE3=6 OR DOCUMENT.TYPE_ZONE4=6) AND DOC_NOM_CR IS NOT NULL ORDER BY tri ";
		//echo 'GBE4:'.$sql_sel_doc.'<br>';
		$qry_sel_doc = $db->query($sql_sel_doc);

		if($_GET['docid']>0 && $_GET['num_zone']>0 && $_GET['certid']>0){
			
			if($_GET['action']=='del' && $_GET['numligne']>=0){
				$sql_del_cr = "DELETE FROM CAND_A_CR WHERE CAND_ID=".intval($_GET['candid'])." AND OPE_ID=".intval($_GET['opeid'])." AND DOC_ID=".intval($_GET['docid'])." AND NUM_ZONE=".intval($_GET['num_zone'])." AND NUM_LIGNE=".intval($_GET['numligne'])."";
				$qry_del_cr=$db->query($sql_del_cr);
				//echo 'GBE5:'.$sql_del_cr.'<br>';
			}
			
			/* Sélection du type de cr à saisir */
			$sql_type_cr = "SELECT DOC_TYPE_CR FROM DOC_A_INFO WHERE DOC_ID='".intval($_GET['docid'])."' AND NUM_ZONE='".intval($_GET['num_zone'])."'";
			$qry_type_cr = $db->query($sql_type_cr);
			
			if($_GET['action']=='edit' && isset($_GET['numligne'])){
				if($_GET['numligne']>0){
					/* On sélectionne le compte-rendu saisi */
					$sql_sel_txt_cr = "SELECT * FROM CAND_A_CR WHERE CAND_ID=".$_GET['candid']." AND OPE_ID=".$_GET['opeid']." AND DOC_ID=".$_GET['docid']." AND NUM_ZONE=".$_GET['num_zone']." AND NUM_LIGNE=".intval($_GET['numligne'])."";
					$qry_sel_txt_cr = $db->query($sql_sel_txt_cr);
				}else{
					$sql_sel_txt_cr = "SELECT * FROM CAND_A_CR WHERE CAND_ID=".$_GET['candid']." AND OPE_ID=".$_GET['opeid']." AND DOC_ID=".$_GET['docid']." AND NUM_ZONE=".$_GET['num_zone']."";
					$qry_sel_txt_cr = $db->query($sql_sel_txt_cr);
				}
				//echo 'GBE6:'.$sql_sel_txt_cr.'<br>';
			}
			
			$sql_nom_doc = "SELECT DOC_NOM_CR FROM DOC_A_INFO WHERE DOC_ID='".intval($_GET['docid'])."' AND NUM_ZONE='".intval($_GET['num_zone'])."'";
			//echo 'GBE7:'.$sql_nom_doc;
			$qry_nom_doc = $db->query($sql_nom_doc);
			
		}
		
		/* Par défaut, les champs sélectionnés sont la date du jour et le certifié annoncé dans l'URL */
		$date_d = date('d');
		$date_m = date('m');				
		$date_Y = date('Y');
		$cert_id_cr = intval($_GET['certid']);
		
		/* S'il y a des documents dans la base */
		if(is_array($qry_sel_doc)){
			/* On sélection l'id du certifié et la date des CR enregistrés */
			$sql_last_date = "SELECT CERT_ID, TO_CHAR(DATE_CR, 'DD') JJ_cr, TO_CHAR(DATE_CR, 'MM') MM_cr, TO_CHAR(DATE_CR, 'YYYY') AAAA_cr FROM CAND_A_CR WHERE OPE_ID='".intval($_GET['opeid'])."' AND DOC_ID='".$qry_sel_doc[0]['doc_id']."' AND CAND_ID='".intval($_GET['candid'])."'";
			$qry_last_date = $db->query($sql_last_date);
		}
		
		/* Si un CR a déjà été saisi, on récupère ses champs et on les met par défaut */
		if(is_array($qry_last_date) && $_GET['docid']<1){
			$date_d 	= $qry_last_date[0]['jj_cr'];
			$date_m		= $qry_last_date[0]['mm_cr'];
			$date_y		= $qry_last_date[0]['yyyy_cr'];
			$cert_id_cr	= $qry_last_date[0]['cert_id'];
		}else{
			if($_GET['JJ_cr']>0){
				$date_d = $_GET['JJ_cr'];
			}
			if($_GET['MM_cr']>0){
				$date_m = $_GET['MM_cr'];
			}
			if($_GET['AAAA_cr']>0){
				$date_Y = $_GET['AAAA_cr'];
			}
			if($_GET['certid']>0){
				$cert_id_cr = intval($_GET['certid']);
			}
		}
		/* On génere la liste des themes du compte rendu pour le candidat, le dossier, le document, la zone choisie */
		$sql_liste_cr = "SELECT * FROM CAND_A_CR WHERE CAND_ID=".intval($_GET['candid'])." AND OPE_ID=".intval($_GET['opeid'])." AND DOC_ID=".intval($_GET['docid'])." AND NUM_ZONE=".intval($_GET['num_zone'])." ORDER BY NUM_LIGNE ASC";
		//echo $sql_liste_cr;
		$qry_liste_cr = $db->query($sql_liste_cr);
	}
		

	?>
	<html>
		<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>
			<script language="JavaScript">
			<!--
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			//-->
			
			window.onload = function(){
				tinyMCE.init({
						// General options
						mode : "exact",
						elements : "txt_cr_libre",						
						theme : "advanced",
						plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
						height: "1000",
						// Theme options
						theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
						theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
						theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
						theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						theme_advanced_resizing : false,

						// Example content CSS (should be your site CSS)
						content_css : "../css/import.css",

						// Drop lists for link/image/media/template dialogs
						template_external_list_url : "lists/template_list.js",
						external_link_list_url : "lists/link_list.js",
						external_image_list_url : "lists/image_list.js",
						media_external_list_url : "lists/media_list.js",

						// Replace values for the template plugin
						template_replace_values : {
							username : "Some User",
							staffid : "991234"
						}
					});
			}
			function verif(){
				error = '';
				if (parseInt(document.form.nbre.value) > parseInt(document.form.max.value) || parseInt(document.form.nbre.value) < parseInt(document.form.min.value)){
					error += "<?php echo $t_nbre_jet_inf_egal ?> "+ parseInt(document.form.max.value) +" <?php echo $t_nbre_jet_sup_egal ?> "+ parseInt(document.form.min.value) +"\n";
				}
				
				if (error!=''){
					alert(error);
				}else{
					document.form.submit();
				}
			}
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function change(){
				var tmp = document.form.select_doc.options[document.form.select_doc.selectedIndex].value.split("_");
				docid = tmp[0];
				numzone = tmp[1];
				if(tmp[2]==50){
					document.location.href='edit_cr.php?candid=<?php echo $_GET['candid'] ?>&opeid=<?php echo $_GET['opeid'] ?>&partid=<?php echo $_GET['partid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&dossid=<?php echo $_GET['dossid'] ?>&certid='+document.form.select_certifie.options[document.form.select_certifie.selectedIndex].value+'&JJ_cr='+document.form.JJ_cr.value+'&MM_cr='+document.form.MM_cr.value+'&AAAA_cr='+document.form.AAAA_cr.value+'&docid='+docid+'&num_zone='+numzone+'&action=edit&numligne=-1';
				}else{
					document.location.href='edit_cr.php?candid=<?php echo $_GET['candid'] ?>&opeid=<?php echo $_GET['opeid'] ?>&partid=<?php echo $_GET['partid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&dossid=<?php echo $_GET['dossid'] ?>&certid='+document.form.select_certifie.options[document.form.select_certifie.selectedIndex].value+'&JJ_cr='+document.form.JJ_cr.value+'&MM_cr='+document.form.MM_cr.value+'&AAAA_cr='+document.form.AAAA_cr.value+'&docid='+docid+'&num_zone='+numzone
				}
			}
			
			function fct_delete(numligne){
				if(confirm('Voulez-vous vraiment supprimer cet enregistrement ?')){
					var tmp2 = document.form.select_doc.options[document.form.select_doc.selectedIndex].value.split("_");
					docid2 = tmp2[0];
					numzone2 = tmp2[1];
		
					document.location.href='edit_cr.php?candid=<?php echo $_GET['candid'] ?>&opeid=<?php echo $_GET['opeid'] ?>&partid=<?php echo $_GET['partid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&dossid=<?php echo $_GET['dossid'] ?>&certid='+document.form.select_certifie.options[document.form.select_certifie.selectedIndex].value+'&JJ_cr='+document.form.JJ_cr.value+'&MM_cr='+document.form.MM_cr.value+'&AAAA_cr='+document.form.AAAA_cr.value+'&docid='+docid2+'&num_zone='+numzone2+'&action=del&numligne='+numligne;
				}
			}
			function fct_edit(numligne){

				var tmp2 = document.form.select_doc.options[document.form.select_doc.selectedIndex].value.split("_");
				docid2 = tmp2[0];
				numzone2 = tmp2[1];
	
				document.location.href='edit_cr.php?candid=<?php echo $_GET['candid'] ?>&opeid=<?php echo $_GET['opeid'] ?>&partid=<?php echo $_GET['partid'] ?>&prodid=<?php echo $_GET['prodid'] ?>&dossid=<?php echo $_GET['dossid'] ?>&certid='+document.form.select_certifie.options[document.form.select_certifie.selectedIndex].value+'&JJ_cr='+document.form.JJ_cr.value+'&MM_cr='+document.form.MM_cr.value+'&AAAA_cr='+document.form.AAAA_cr.value+'&docid='+docid2+'&num_zone='+numzone2+'&action=edit&numligne='+numligne;
			
			}
			//-->
			</script>
		</head>
		<body bgcolor="#FFFFFF" text="#000000">
		<table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo htmlentities($qry_info_cand[0]['cand_prenom']). ' ' . htmlentities($qry_info_cand[0]['cand_nom']) ?>, <?php echo htmlentities($qry_info_doss[0]['dossier_nom']) ?></td>
			</tr>
		</table>
		<form method="post" action="#" name="form" id="form">
		  <table width="700" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr>
			<td width="14"></td>
			<td class="TX_Candidats">
				<table width="600" cellpadding="2" cellspacing="0" border="0">
				<tr><td class="TX" width="150">Date&nbsp;de&nbsp;lentretien&nbsp;:&nbsp;</td>
				<td style="text-align: left;">
				<input class="form_ediht_Candidats" type="text" name="JJ_cr" size="2" maxlength="2" value="<?php echo $date_d ?>">&nbsp;
				<input class="form_ediht_Candidats" type="text" name="MM_cr" size="2" maxlength="2" value="<?php echo $date_m ?>">&nbsp;
				<input class="form_ediht_Candidats" type="text" name="AAAA_cr" size="4" maxlength="4" value="<?php echo $date_Y ?>"></td></tr>
					
					<tr><td class="TX" style="text-align: left;">Certifi&eacute;&nbsp;:&nbsp;</td><td style="text-align: left;">
						<select name="select_certifie" class="form_ediht_Candidats">
							<?php
							foreach($qry_liste_certifies as $elmt_certifie){
								unset($selected_certifie);
								if($elmt_certifie['cert_id']==$cert_id_cr){
									$selected_certifie = ' selected="selected"'; 
								}
								echo '<option value="'.$elmt_certifie['cert_id'].'"'.$selected_certifie.'>'.htmlentities($elmt_certifie['cert_nom']).' '.htmlentities($elmt_certifie['cert_prenom']).'</option>';
							}
							?>					
						</select>
					</td></tr>
						<tr>
							<td></td>
							<td style="text-align: center;"><input style="margin-top: 10px;margin-right: 80px;" type="button" value="Enregistrer" class="bn_valider_candidat" onclick="window.close();"></td>
						</tr>
				</table>
			</td>
			<td width="14"></td>
			</tr>
			<tr> 
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			 </tr>
			</table>
			<br>
			<table width="700" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="TX">
			  <table width="600" cellpadding="2" cellspacing="0" border="0">
				<tr><td class="TX" width="150">Choix du document&nbsp;:&nbsp;</td>
				<td style="text-align: left;">
				<?php
				/* Si on a trouvé au moins un document qui contient au moins un compte-rendu à saisir */
				if(is_array($qry_sel_doc)){
					?>
						<input type="hidden" name="step" value="1">
						<select name="select_doc" class="form_ediht_Candidats">
							<?php 
							foreach($qry_sel_doc as $doc_liste){
									unset($selected_doc);
									$temp_str = $_GET['docid'].'_'.$_GET['num_zone'];
									if($temp_str==$doc_liste['doc_id'].'_'.$doc_liste['num_zone']){
										$selected_doc = ' selected="selected"';
									}
								echo '<option value="'.$doc_liste['doc_id'].'_'.$doc_liste['num_zone'].'_'.$doc_liste['doc_type_cr'].'"'.$selected_doc.'>'.htmlentities($doc_liste['doc_nom_cr']).'</option>';
							}
							?>
						</select>&nbsp;<input type="button" name="valid1" class="bn_ajouter" value="Sélectionner" onclick="change();">
					<?php
				}
				?>
				</td>
				</tr>
			  </table>
			  </td>
			  <td width="14"></td>
			</tr>
			  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
			</tr>
		</table>
			<br>
		<?php
		if($_GET['docid']>0 && strlen($_GET['num_zone'])>0){
			?>
		  <table width="700" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
			<tr> 
			  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
			  <td height="14"></td>
			  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td class="TX_Candidats_UP">
			  SAISIE D'UNE LIGNE D'ENTRETIEN&nbsp;:&nbsp;<?php echo htmlentities($qry_nom_doc[0]['doc_nom_cr']) ?>
			  </td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td width="14"></td>
			</tr>
			<tr> 
			  <td width="14"></td>
			  <td align="center" valign="middle" class="TX">&nbsp;</td>
			  <td width="14"></td>
			</tr>
					<tr> 
					  <td width="14"></td>
					  <td align="center" valign="middle" class="TX">
					  <input type="hidden" name="hid_type_cr" value="<?php echo $qry_type_cr[0]['doc_type_cr'] ?>">
					  <?php
						switch ($qry_type_cr[0]['doc_type_cr']){
							
							case 47 : //Themes
							case 48 : //Themes et Images					
							?>
							<table align="left" cellpadding="0" cellspacing="0" border="0">
							  <tr>
								<td class="TX" valign="top" width="150">
								  Thème&nbsp;:&nbsp;</td><td class="TX"><textarea cols="70" rows="3" name="txt_cr_theme" class="form_ediht_Candidats" style="height: 50px;"><?php echo $qry_sel_txt_cr[0]['theme'] ?></textarea>
								</td>
							  </tr>
							  <tr><td colspan="2">&nbsp;</td></tr>
							  <tr>
								<td class="TX" valign="top">
								Commentaire&nbsp;:&nbsp;</td><td class="TX"><textarea cols="70" rows="8" name="txt_cr_comm" class="form_ediht_Candidats" style="height: 200px;"><?php echo $qry_sel_txt_cr[0]['txt_cr'] ?></textarea>
								</td>
							  </tr>
							</table>
							<?php
							break;
							case 49 : //Images
							case 50 : //Libre
							  ?>
							  <textarea cols="70" rows="8" name="txt_cr_comm" class="form_ediht_Candidats" style="height: 200px;"><?php echo $qry_sel_txt_cr[0]['txt_cr'] ?></textarea>
							<?php
							break;
						}
						?>
					  </td>
					  <td width="14"></td>
					</tr>
					<tr> 
					  <td width="14"></td>
					  <td align="center" valign="top" class="TX">&nbsp;</td>
					  <td width="14"></td>
					</tr>
					<tr> 
					  <td width="14"></td>
					  <td align="center" valign="top" class="TX">
						<?php
						if($qry_type_cr[0]['doc_type_cr']==50 || $qry_sel_txt_cr[0]['txt_cr']!=''){
						?>
							<input type="submit" value="MODIFIER" class="bn_valider_candidat">
						<?php
						}else{
						?>
							<input type="submit" value="AJOUTER" class="bn_valider_candidat">
						<?php
						}
						?>
					  </td>
					  <td width="14"></td>
					</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
		</table>
		
		<?php
		}
		?>
		</form>
		<?php
		// Si un ou plusieurs compte-rendus sont disponibles pour les criteres sélectionnés..
		if(is_array($qry_liste_cr)){
			?>
			<br>
			<table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Candidats2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;Récapitulatif des lignes d'entretien</td>
				</tr>
			</table>
			<table width="700" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="center" valign="top" class="TX">
				  <table width="100%" align="left" cellpadding="5" cellspacing="1" bgcolor="#000000">
				  
				  <?php
				  foreach($qry_liste_cr as $elmt){
					echo '<tr>';
					echo '<td align="left" bgcolor="#FFFFFF" class="TX">'.nl2br($elmt['theme']).'</td>';
					echo '<td align="left" bgcolor="#FFFFFF" class="TX">'.nl2br($elmt['txt_cr']).'</td>';
					echo '<td align="left" bgcolor="#FFFFFF" width="50"><div><img style="display: inline;" onclick="fct_edit('.$elmt['num_ligne'].')" onmouseover="this.style.cursor=\'pointer\'" src="../images/comment_edit.png" title="Modifier" alt="Modifier">&nbsp;&nbsp;&nbsp;&nbsp;<img style="display: inline;" onclick="fct_delete('.$elmt['num_ligne'].')" onmouseover="this.style.cursor=\'pointer\'" src="../images/icon_supp2.gif" alt="Supprimer" title="Supprimer"></div></td>';
					echo '</tr>';
				  }
				  ?>
				  </table>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			</table>
			<?php
		}
		?>
	</body>
	</html>
	<?php
	}else{
	include('no_acces.php');
}
?>
