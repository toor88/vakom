<?php
session_start();
include ("../config/lib/connex.php");
include ("../config/lib/db.oracle.php");
$db = new db($conn);

header('Content-type: text/html; charset=UTF-8'); 

if ($_GET['certid']!='' && $_GET['certifid']!=''){
		
	// On vérifie si l'entrée n'existe pas déjà
	$sql_verif			= "SELECT * FROM CERT_A_CERTIF WHERE CERTIF_CERT_ID= '".txt_db(intval($_GET['certid']))."' AND CERTIF_CODE_ID='".txt_db(intval($_GET['certifid']))."'";
	$qry_verif			= $db->query($sql_verif);

	// Si l'entrée n'existe pas, on enregistre les données
	if (!is_array($qry_verif)){
		// On insert la nouvelle ligne dans la base de données
		$sql_insert			= "INSERT INTO CERT_A_CERTIF VALUES(SEQ_ID.NEXTVAL, '".txt_db(intval($_GET['certid']))."', '".txt_db(intval($_GET['certifid']))."', '', '', '', '')";
		$qry_insert			= $db->query($sql_insert);
	}

	// Chargement de la liste des certifications
	$query_clients 		= "SELECT CERT_A_CERTIF.*, CODE.*, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_PERFECTIONNEMENT, 'DD') JJ, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_PERFECTIONNEMENT, 'MM') MM, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_PERFECTIONNEMENT, 'YYYY') AAAA, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_CERTIFICATION, 'DD') JJ2, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_CERTIFICATION, 'MM') MM2, 
		TO_CHAR(CERT_A_CERTIF.CERTIF_CERTIFICATION, 'YYYY') AAAA2 
		FROM CERT_A_CERTIF,CODE WHERE CODE.CODE_TABLE='CERTIFICATION' AND CERT_A_CERTIF.CERTIF_CODE_ID=CODE.CODE_ID AND CERT_A_CERTIF.CERTIF_CERT_ID='".txt_db(intval($_GET['certid']))."' ORDER BY CODE.CODE_LIBELLE ASC";
	$rows_clients 		= $db->query($query_clients);
	
	$v='<table width="600" border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000">
		<tr> 
		  <td class="TX_bold">&nbsp;'.$t_fiche_certif_list.'</td>
		  <td class="TX_bold" align="center">'.$t_date_perfection.'</td>
		  <td class="TX_bold" align="center">'.$t_date_certif.'</td>
		  <td class="TX_bold" align="center">'.$t_en_formation.'</td>
		  <td class="TX_bold" align="center">'.$t_suspendu.'</td>
		</tr>';
	if (is_array($rows_clients)){
		foreach ($rows_clients as $clients) // Tant que la requete renvoie un résultat
			{
			$o .= '<tr bgcolor="F1F1F1"> 
			  <td><input type="hidden" name="liste_certif[]" value="'. $clients['certif_id'] .'">'. $clients['code_libelle'] .'</td>
			  <td align="center"> 
				<input type="text" onkeyup="pass(1, '. $clients['certif_id'].')" id="'. $clients['certif_id'] .'_JJ" name="'. $clients['certif_id'] .'_JJ" id="'. $clients['certif_id'] .'_JJ" size="2" maxlength="2" class="form_ediht" value="'.$clients['jj'].'" >
				/ 
				<input type="text" onkeyup="pass(2, '. $clients['certif_id'].')" id="'. $clients['certif_id'] .'_MM" name="'. $clients['certif_id'] .'_MM" id="'. $clients['certif_id'] .'_MM" size="2" maxlength="2" class="form_ediht" value="'.$clients['mm'].'" >
				/ 
				<input type="text" onkeyup="pass(3, '. $clients['certif_id'].')" id="'. $clients['certif_id'] .'_AAAA" name="'. $clients['certif_id'] .'_AAAA" id="'. $clients['certif_id'] .'_AAAA" size="4" maxlength="4" class="form_ediht" value="'.$clients['aaaa'].'" >
			  </td>
			  <td align="center"> 
				<input type="text" onkeyup="pass(4, '. $clients['certif_id'].')" id="'. $clients['certif_id'] .'_JJ2" name="'. $clients['certif_id'] .'_JJ2" id="'. $clients['certif_id'] .'_JJ2" size="2" maxlength="2" class="form_ediht" value="'.$clients['jj2'].'" >
				/ 
				<input type="text" onkeyup="pass(5, '. $clients['certif_id'].')" id="'. $clients['certif_id'] .'_MM2" name="'. $clients['certif_id'] .'_MM2" id="'. $clients['certif_id'] .'_MM2" size="2" maxlength="2" class="form_ediht" value="'.$clients['mm2'].'" >
				/ 
				<input type="text" id="'. $clients['certif_id'] .'_AAAA2" name="'. $clients['certif_id'] .'_AAAA2" id="'. $clients['certif_id'] .'_AAAA2" size="4" maxlength="4" class="form_ediht" value="'.$clients['aaaa2'].'" >
			  </td>
			  <td align="center"> 
				<input type="checkbox" name="'. $clients['certif_id'] .'_formation" value="1" ';
				
				if ($clients['certif_formation'] == '1'){
					$o.= ' checked="checked"';
				}
				
				$o.= '>
			  </td>
			  <td align="center"> 
				<input type="checkbox" name="'. $clients['certif_id'] .'_suspendu" value="1" ';
				if ($clients['certif_suspendu'] == '1'){
					$o.= ' checked="checked"';
				}
				$o.= '>
			  </td>
			</tr>';
			}
	}else{
		$o .= '<tr bgcolor="F1F1F1"><td colspan="5" align="center" class="TX">'.$t_aucune_cert_cert.'</td></tr>';
	}
	if ($o!=''){
		//<option value="-1">Libre</option>
		$v	.= $o.'</table>';
	}else{
	$v	= '';
	}
	echo $v.":";
}
?>