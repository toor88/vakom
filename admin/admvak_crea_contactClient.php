<?php
session_start();
	// Si l'utilisateur est un super admin
if ($_SESSION['droit']>5){
	if ($_GET['partid']!=''){
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);

		if ($_POST['posted']){
		
		if ($_POST['inactif']=='1'){
			$actif='0';
		}else{
			$actif='1';
		}	
		
		// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
		$sql_seq_num 	= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
		$qry_seq_num 	= $db->query($sql_seq_num);
		$seq_num		= intval($qry_seq_num[0]['seq_num']);
		
		/* Si le pays selectionné n'est pas la france */
		if ($_POST['select_pays']!='5'){
			/* Le CP a pour valeur le champ cp_etr et la ville a pour valeur le champ ville_etr*/
			$cp 	= $_POST['cp_etr'];
			$ville 	= $_POST['ville_etr'];
		}else{
		/* Si le pays selectionné est la france, le CP porte la valeur du champ cp */
			$cp 	= $_POST['cp'];
			/* Si la ville n'existe pas dans la base, la ville porte la valeur du champ nom_new_ville */
			if ($_POST['new_ville']=='1'){
				$ville 	= $_POST['nom_new_ville'];
			}else{
				/* Si elle existe, la valeur de la ville est celle de la liste de sélection */
				$tab_v 	=  explode("_",$_POST['ville']);
				$ville 	= $tab_v[1];
			}
		}
		
		$sql_insert_cert = "INSERT INTO CERTIFIE VALUES('".$seq_num."',
			'".txt_db($_GET['partid'])."', 
			'".txt_db($_POST['nom'])."', 
			'".txt_db($_POST['prenom'])."', ";			
		if (strlen($_POST['JJ_DNS'])==2 && strlen($_POST['MM_DNS'])==2 && strlen($_POST['AAAA_DNS'])==4 && $_POST['JJ_DNS']>00 && $_POST['JJ_DNS']<32 && $_POST['MM_DNS']>00 && $_POST['MM_DNS']<13 && $_POST['AAAA_DNS']>1900 && $_POST['AAAA_DNS']<date('Y')){
			$sql_insert_cert .= "TO_DATE('".txt_db($_POST['JJ_DNS'])."/".txt_db($_POST['MM_DNS'])."/".txt_db($_POST['AAAA_DNS'])."', 'DD/MM/YYYY'), ";
		}else{
			$sql_insert_cert .= "'', ";
		}			
		$sql_insert_cert .= "'".txt_db($_POST['droit_admin'])."', 
			'".txt_db($_POST['droit_certif'])."', 
			'".txt_db($_POST['ca'])."', 
			'".txt_db($_POST['mp'])."', 
			'".txt_db($_POST['tel'])."', 
			'".txt_db($_POST['mail'])."', 
			'".txt_db($_POST['adr'])."', 
			'".txt_db($_POST['adr2'])."', 
			'".txt_db($cp)."', 
			'".txt_db(strtoupper($ville))."', 
			'".txt_db($_POST['select_pays'])."', 
			'".txt_db($_POST['fax'])."', 
			'".txt_db($_POST['tel_p'])."', 
			'', 
			'".txt_db($_POST['comm1'])."', 
			'".txt_db($_POST['comm2'])."', 
			'".txt_db($_SESSION['vak_id'])."', 
			SYSDATE, 
			'',
			'',
			'',
			'',
			'".$actif."',
			'".txt_db($_POST['autre_nom_part'])."')";

			$qry_insert_cert	= $db->query($sql_insert_cert);
			//echo $sql_insert_cert;
			
			$sql_insert_certif = "INSERT INTO CERT_A_CERTIF (CERTIF_ID,CERTIF_CERT_ID,CERTIF_CODE_ID) (select seq_id.nextval,'".$seq_num."',code_id from code where code_table='CERTIFICATION' and code_lang_id=1)";
			$qry_insert_certif = $db->query($sql_insert_certif);
			if ($_POST['visu_soc']=='1'){
				$sql_insert_certif = "insert into client_a_cert (select distinct cli_id,".$seq_num." from certifie,client_a_cert where certifie.cert_id=client_a_cert.cert_id and cert_part_id=".txt_db($_GET['partid']).")";
				$qry_insert_certif = $db->query($sql_insert_certif);
			}
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				document.location.href='admvak_edit_contactClient.php?certid=<?php echo $seq_num ?>&idnc=012';
			</script>
			<?php
		}
		
		// Chargement de la liste des pats
		$sql_pays_list 		= "SELECT * FROM CODE WHERE CODE_TABLE='PAYS' ORDER BY CODE_ORDRE";
		$qry_pays_list 		= $db->query($sql_pays_list);

		// Chargement du nom du partenaire
		$sql_part = "SELECT PART_NOM FROM PARTENAIRE WHERE PART_ID= '".txt_db($_GET['partid'])."'";
		$qry_part = $db->query($sql_part);
		
			?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--
			function hid_ville(id_pays){
				if (id_pays != "5"){
					document.getElementById('cp1').style.display='none';
					document.getElementById('cp2').style.display='inline';
					document.getElementById('ville2').style.display='none';
					document.getElementById('ville3').style.display='inline';
				}else{
					document.getElementById('cp1').style.display='inline';
					document.getElementById('cp2').style.display='none';
					document.getElementById('ville2').style.display='inline';
					document.getElementById('ville3').style.display='none';
				}
			}
			
			function charge_code(code){
				var tmp_code = code.split("_");
				document.getElementById('cp').value = tmp_code[0];
			}

			function TestVille(){
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_ville.php"; // La page qui réceptionne les données
				var cp = document.getElementById('cp').value; 
				var data     = null; 
				
				if 	(cp.length>1){ //Si le code postal tapé possède au moins 2 caractères
					document.getElementById("ville2").innerHTML='<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" />';
					var xhr_object = null; 
						 
						if(window.XMLHttpRequest) // Firefox 
						   xhr_object = new XMLHttpRequest(); 
						else if(window.ActiveXObject) // Internet Explorer 
						   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
						else { // XMLHttpRequest non supporté par le navigateur 
						   alert("<?php echo $t_browser_support_error_1 ?>");
						   return; 
						} 
						 
						
						 
						if(cp != ""){
							data = "cp="+cp;
						}
						if(method == "GET" && data != null) {
						   filename += "?"+data+"&class=cert";
						   data      = null;
						}
						 
						xhr_object.open(method, filename, true);

						xhr_object.onreadystatechange = function() {
						   if(xhr_object.readyState == 4) {
							  var tmp = xhr_object.responseText.split(":"); 
							  if(typeof(tmp[0]) != "undefined") { 
								 document.getElementById("ville2").innerHTML = '';
								 if (tmp[0]!=''){
									 document.getElementById('saisie_ville_m').style.display='inline';
									 document.getElementById('saisie_ville_m_lab').style.display='inline';
									 document.getElementById('saisie_ville_m').checked = false;
									 document.getElementById("ville2").innerHTML = tmp[0];
								 }else{
									 document.getElementById('saisie_ville_m').style.display='none';
									 document.getElementById('saisie_ville_m_lab').style.display='none';
									 document.getElementById("ville2").innerHTML = '<input type="hidden" name="new_ville" value="1"><input type="text" size="46" name="nom_new_ville" maxlength="255" class="form_ediht_Certifies" style="text-transform:uppercase" />';
								 }
							  }
						   } 
						} 

						xhr_object.send(data); //On envoie les données
				}
			}

			function doSaisieLibre(){
				var saisie_ville_m = document.getElementById('saisie_ville_m');
				if (saisie_ville_m.checked){
					document.getElementById('saisie_ville_m').style.display='none';
					document.getElementById('saisie_ville_m_lab').style.display='none';
					document.getElementById("ville2").innerHTML = '<input type="hidden" name="new_ville" value="1"><input type="text" size="46" name="nom_new_ville" maxlength="255" class="form_ediht_Certifies" style="text-transform:uppercase" />';
				}
			}

			
			function pasdate1(){
				var obj=document.form.JJ_DNS.value.length;
				if(navigator.appName=='Microsoft Internet Explorer'){
					if (obj==2){
						document.form.MM_DNS.value='';
						document.form.MM_DNS.focus();
					}
				}
				else{
					if(navigator.appName=='Netscape'){
						if (obj==1){//alert('je passe');
							document.form.MM_DNS.value='';
							document.form.MM_DNS.focus();
						}
					}
				}
			}
			function pasdate2(){
				var obj=document.form.MM_DNS.value.length;
				if(navigator.appName=='Microsoft Internet Explorer'){
					if (obj==2){
						document.form.AAAA_DNS.value='';
						document.form.AAAA_DNS.focus();
					}
				}
				else{
					if(navigator.appName=='Netscape'){
						if (obj==1){//alert('je passe');
							document.form.AAAA_DNS.value='';
							document.form.AAAA_DNS.focus();
						}
					}
				}
			}			
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}

			function verif(){
				error = '';
				error2 = '';
					
				if (document.form.nom.value<1){
					error += "<?php echo $t_nom_oblig ?>\n";
				}				
				if (document.form.prenom.value<1){
					error += "<?php echo $t_prenom_oblig ?>\n";
				}
				if (document.form.ca.value<1){
					error += "<?php echo $t_login_oblig ?>\n";
				}
				if (document.form.cp.value == '' && document.form.cp_etr.value == ''){
							error += "<?php echo $t_cp_oblig ?>\n";
						}
				if ((document.form.ville != undefined && document.form.ville.value == '' && document.form.ville_etr.value == '') || (document.form.ville == undefined && document.form.nom_new_ville.value == '')){				
					error += "<?php echo $t_ville_oblig ?>\n";
				}
				if (document.form.mp.value<1){
					error += "<?php echo $t_mdp_oblig ?>\n";
				}
				if (document.form.tel.value<1){
					error += "<?php echo $t_tel_oblig ?>\n";
				}
				if (document.form.mail.value<1){
					error += "<?php echo $t_adresse_oblig ?>\n";
				}
				if (document.form.droit_admin.checked!="1" && document.form.droit_certif.checked!="1"){
					error += "<?php echo $t_droits_oblig ?>\n";
				}
				
				if (document.form.JJ_DNS.value!='' || document.form.MM_DNS.value!='' || document.form.AAAA_DNS.value!=''){
					// Verif du format date de fin
					if (document.form.JJ_DNS.value<1 || document.form.JJ_DNS.value>31 || document.form.JJ_DNS.value.length<2){
						error2 = true;
					}
					if (document.form.MM_DNS.value<1 || document.form.MM_DNS.value>12 || document.form.MM_DNS.value.length<1){
						error2 = true;
					}
					if (document.form.AAAA_DNS.value<1910 || document.form.AAAA_DNS.value.length<4){
						error2 = true;
					}
					if (error2==true){
						error +="<?php echo $t_mauvaise_date_naiss ?>\n";
					}
				}
				
				if (error!=''){
					alert(error);
				}else{
					document.form.submit();
				}
			}
			
			
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}

			//-->
			</script>
			</head>

			<body bgcolor="#FFFFFF" text="#000000">
			<form method="post" action="#" name="form">
			  <table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Certifies"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo strtoupper($qry_part[0]['part_nom'])?> > <?php echo $t_fiche_new_contact ?></td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td><table border="0" cellspacing="0" cellpadding="0"  width="100%" align="center">
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  </tr>
				<tr> 
				  <td width="150" class="fond_tablo_certifies"><?php echo $t_fiche_cert ?></td>
				  <td align="left" class="fond_tablo_certifies2"><?php echo $t_date_creation ?> : le <?php echo date('d/m/Y H:i') ?></td>
			    </tr>
				<tr> 
				  <td align="center" colspan="2" valign="top" class="TX"> 
					
				  </td>
			    </tr>
				
				<!--
				
				<tr> 
				  <td width="14"></td>
				  <td align="center" valign="top" class="TX">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" valign="top" class="TX">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" valign="top" class="TX"> 
					<table border="0" cellspacing="0" cellpadding="0">
					  <tr> 
						<td class="TX" height="40" valign="top">Niveau de certification :&nbsp;&nbsp;</td>
						<td> 
						  <table width="600" border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000">
							<tr> 
							  <td class="TX_bold">&nbsp;Liste des Certifications</td>
							  <td class="TX_bold" align="center">Date de <br>
								perfectionnement</td>
							  <td class="TX_bold" align="center">Date de <br>
								certification</td>
							  <td class="TX_bold" align="center">En formation</td>
							  <td class="TX_bold" align="center">Suspendu</td>
							</tr>
							<tr bgcolor="F1F1F1"> 
							  <td> Certification 1</td>
							  <td align="center"> 
								<input type="text" name="JJ222" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="MM222" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="AA53" size="2" class="form_ediht_Certifies" >
							  </td>
							  <td align="center"> 
								<input type="text" name="JJ5" size="2" class="form_ediht_Certifies" value="10" >
								/ 
								<input type="text" name="MM5" size="2" class="form_ediht_Certifies" value="06" >
								/ 
								<input type="text" name="AA5" size="2" class="form_ediht_Certifies" value="09" >
							  </td>
							  <td align="center"> 
								<input type="checkbox" name="checkbox62" value="1">
							  </td>
							  <td align="center"> 
								<input type="checkbox" name="checkbox6" value="1">
							  </td>
							</tr>
							<tr bgcolor="F1F1F1"> 
							  <td>Certification 2</td>
							  <td align="center"> 
								<input type="text" name="JJ2222" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="MM2222" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="AA537" size="2" class="form_ediht_Certifies" >
							  </td>
							  <td align="center"> 
								<input type="text" name="JJ22" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="MM22" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="AA535" size="2" class="form_ediht_Certifies" >
							  </td>
							  <td align="center"> 
								<input type="checkbox" name="checkbox63" value="1" checked>
							  </td>
							  <td align="center"> 
								<input type="checkbox" name="checkbox22" value="2">
							  </td>
							</tr>
							<tr bgcolor="F1F1F1"> 
							  <td>Certification 3</td>
							  <td align="center"> 
								<input type="text" name="JJ2223" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="MM2223" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="AA538" size="2" class="form_ediht_Certifies" >
							  </td>
							  <td align="center"> 
								<input type="text" name="JJ52" size="2" class="form_ediht_Certifies" value="10" >
								/ 
								<input type="text" name="MM52" size="2" class="form_ediht_Certifies" value="06" >
								/ 
								<input type="text" name="AA52" size="2" class="form_ediht_Certifies" value="09" >
							  </td>
							  <td align="center"> 
								<input type="checkbox" name="checkbox64" value="1">
							  </td>
							  <td align="center"> 
								<input type="checkbox" name="checkbox32" value="3" checked>
							  </td>
							</tr>
							<tr bgcolor="F1F1F1"> 
							  <td>Certification PRO</td>
							  <td align="center"> 
								<input type="text" name="JJ2224" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="MM2224" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="AA539" size="2" class="form_ediht_Certifies" >
							  </td>
							  <td align="center"> 
								<input type="text" name="JJ42" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="MM42" size="2" class="form_ediht_Certifies" >
								/ 
								<input type="text" name="AA536" size="2" class="form_ediht_Certifies" >
							  </td>
							  <td align="center"> 
								<input type="checkbox" name="checkbox65" value="1">
							  </td>
							  <td align="center"> 
								<input type="checkbox" name="checkbox42" value="4">
							  </td>
							</tr>
						  </table>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" valign="top">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  <tr> 
						<td class="TX" valign="top">Transf&eacute;rer le certifi&eacute; vers 
						  :&nbsp;&nbsp;&nbsp; </td>
						<td class="TX"> 
						  <select name="select" class="form_ediht_Certifies">
							<option>nom Partenaire</option>
						  </select>
						</td>
					  </tr>
					</table>
				  </td>
				  <td width="14"></td>
				</tr>
				
				-->
			  </table></td>
			    </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td ><table border="0" cellspacing="0" cellpadding="0" width="100%">
					  <tr> 
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
						<td class="champsoblig" valign="middle" align="right"><?php echo $t_champs_oblig ?> 
						  * </td>
					  </tr>
					  <tr> 
						<td class="TX" ><?php echo $t_fiche_cert_inactif ?>* :</td>
						<td class="TX"> 
						  <input type="checkbox" name="inactif" value="1">
						</td>
						<td class="TX">
						</td>
						<td class="TX"> 						  
						</td>
					  </tr>
					  <tr> 
						<td colspan="4" class="TX" ><?php echo $t_autre_nom_part ?>&nbsp;: 
						  <input type="text" name="autre_nom_part" style="width:50%" maxlength="80" class="form_ediht_Certifies">                        </td>
					  </tr>
					  <tr> 
						<td class="TX" ><?php echo $t_nom ?>* :</td>
						<td class="TX"> 
						  <input type="text" name="nom" size="40" maxlength="40" class="form_ediht_Certifies">
						</td>
						<td class="TX">&nbsp;<?php echo $t_prenom ?>* :</td>
						<td class="TX"> 
						  <input type="text" name="prenom" size="40" maxlength="40" class="form_ediht_Certifies">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" ><?php echo $t_date_naissance ?>&nbsp;: </td>
						<td class="TX"> 
						  <input type="text" name="JJ_DNS" onKeyPress="pasdate1()" size="2" class="form_ediht_Certifies" maxlength="2">
						  / 
						  <input type="text" name="MM_DNS" onKeyPress="pasdate2()" size="2" class="form_ediht_Certifies" maxlength="2">
						  / 
						  <input type="text" name="AAAA_DNS" size="4" class="form_ediht_Certifies" maxlength="4">
						</td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  <tr>
						<td class="TX" ><?php echo $t_droits ?>*&nbsp;:</td>
						<td class="TX">
						  <input type="checkbox" name="droit_admin" value="1">
						  <?php echo $t_admin ?>&nbsp;&nbsp;&nbsp; 
						  <input type="checkbox" name="droit_certif" value="1">
						 <?php echo $t_cert ?></td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp;</td>
					  </tr>
					  <tr> 
						<td class="TX" ><?php echo $t_login.' '.$t_extranet ?>* :</td>
						<td class="TX"> 
						  <input type="text" name="ca" size="30" class="form_ediht_Certifies" maxlength="30">
						</td>
						<td class="TX"><?php echo $t_mdp_extranet ?>*&nbsp;:&nbsp;</td>
						<td class="TX"> 
						  <input type="text" name="mp" size="40" class="form_ediht_Certifies" maxlength="40">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" ><?php echo $t_tel_fix ?>*&nbsp;:</td>
						<td class="TX"> 
						  <input type="text" name="tel" size="40" class="form_ediht_Certifies" maxlength="25" value="<?php echo htmlentities($qry_cert[0]['cert_tel']) ?>">
						</td>
						<td class="TX"> </td>
						<td class="TX"> </td>
					  </tr>
					  <tr> 
						<td class="TX" ><?php echo $t_email ?>*&nbsp;:</td>
						<td colspan="3" class="TX"> 
						  <input type="text" name="mail" style="width:98%" class="form_ediht_Certifies" maxlength="100" value="<?php echo htmlentities($qry_cert[0]['cert_email']) ?>">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" ><?php echo $t_adresse ?>&nbsp;:</td>
						<td colspan="3" class="TX"> 
						  <input type="text" name="adr" style="width:98%" class="form_ediht_Certifies" maxlength="100"></td>
					  </tr>
					  <tr>
					    <td class="TX" >&nbsp;</td>
					    <td colspan="3" class="TX"><input type="text" name="adr2" style="width:98%" class="form_ediht_Certifies" maxlength="100"></td>
				    </tr>
					  <tr> 
						<td class="TX" ><?php echo $t_pays ?>*&nbsp;:</td>
						<td class="TX"> 
						  <select name="select_pays" id="pays" class="form_ediht_Certifies" onChange="hid_ville(document.getElementById('pays').options[document.getElementById('pays').selectedIndex].value)">
						<?php
						  if (is_array($qry_pays_list)){
							foreach($qry_pays_list as $pays){
								echo '<option value="'.$pays['code_id'].'">'.$pays['code_libelle'].'</option>';
							}
						  }
						?>
						  </select>
						</td>
						<td class="TX" colspan="2">&nbsp;</td>
					  </tr>
					  <tr> 
						<td class="TX" ><?php echo $t_cp ?>* :</td>
						<td class="TX"> 
						  <div id="cp1" width="1%" style="display: inline;">
						  <input type="text" name="cp" id="cp" size="5" maxlength="5" class="form_ediht_Certifies" onkeyUp="TestVille()" onBlur="charge_code(document.getElementById('ville').options[document.getElementById('ville').selectedIndex].value)">
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						  </div>
						  <div id="cp2" width="1%" style="display: none;">
						  <input type="text" name="cp_etr" id="cp_etr" size="5" maxlength="5" class="form_ediht_Certifies">
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						  </div>
					    </td>
						  <td class="TX"><?php echo $t_ville ?>*&nbsp;:&nbsp;</td>
						  <td class="TX">
						 <div id="ville2" width="1%" style="float: left;display: inline;"><input type="hidden" id="ville" name="ville" value=""></div>
							  <div  >
								  <input type="checkbox" name="saisie_ville_m" id="saisie_ville_m"  onclick="doSaisieLibre()" style="display: none;float: left;    margin-left: 25px;margin-top: 5px;">
								  <span id="saisie_ville_m_lab" style="display: none;line-height: 20px;" >&nbsp;&nbsp;Saisie libre</span>

							  </div>
						<div id="ville3" style="display: none;">
							<input type="text" name="ville_etr" maxlength="255" class="form_ediht_Certifies">
						</div>
						</td>
					  </tr>
					  <tr> 
						<td class="TX" >&nbsp;<?php echo $t_fax ?> : </td>
						<td class="TX"> 
						  <input type="text" name="fax" size="40" class="form_ediht_Certifies" maxlength="25">
						</td>
						<td class="TX"><?php echo $t_tel_port ?> :</td>
						<td class="TX"> 
						  <input type="text" name="tel_p" size="40" class="form_ediht_Certifies" maxlength="25">
						</td>
					  </tr>
					  <!--
					  <tr> 
						<td class="TX" height="40"><a href="#">Profil OPR</a> :</td>
						<td class="TX"> 2.1 du questionnaire 10/02/2009</td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp; </td>
					  </tr>
					  <tr> 
						<td class="TX" height="40">Candidats : </td>
						<td class="TX"> 
						  <select name="select_candid" class="form_ediht_Certifies">
							<option>Nom l Pr&eacute;nom l Date questionnaire </option>
						  </select>
						  <input type="button" name="Submit2" value="Parcourir ..." class="bn_ajouter">
						</td>
						<td class="TX">&nbsp;</td>
						<td class="TX">&nbsp; </td>
					  </tr>
					  -->
					  <tr> 
						<td class="TX" ><?php echo $t_info1 ?> :</td>
						<td class="TX"> 
						  <input type="text" name="comm1" size="40" class="form_ediht_Certifies" maxlength="40">
						</td>
						<td class="TX"><?php echo $t_info2 ?> :</td>
						<td class="TX"> 
						  <input type="text" name="comm2" size="40" class="form_ediht_Certifies" maxlength="40">
						</td>
					  </tr>
					  <tr> 
						<td class="TX" >Voir toutes les sociétés :</td>
						<td class="TX"> 
						  <input type="checkbox" name="visu_soc" value="1">
						</td>
						<td class="TX">
						</td>
						<td class="TX"> 						  
						</td>
					  </tr>					  
				  </table></td>
			    </tr>
			  </table>
			  
			  <br>
			  <p style="text-align:center">
					<input type="hidden" name="posted" value="1">
					<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_certifie" onClick="verif();">
				  </p>
			</form>
			</body>
			</html>
		<?php
	}
}else{
	include('no_acces.php');
}
?>
