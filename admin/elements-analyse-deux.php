<!DOCTYPE html>
<html lang="fr-FR" moznomarginboxes mozdisallowselectionprint> 
<?php
    include ("../config/lib/connex.php");
    include ("../config/lib/db.oracle.php");
    $db = new db($conn);
	$opeid = $_GET['opeid'];
	$manger_nom ="";
	$mangerplusun_nom ="";
	$manger_cand_id = null;
	$nb_personnes_mg=-2;

if (isset($opeid)){
	$sql_select_question = "SELECT distinct CAND_ID FROM CAND_A_QUEST WHERE OPE_ID = ".txt_db(intval($opeid));
    $condidats   	 = $db->query($sql_select_question);
	    
    foreach($condidats as $condidat){
		$sql_infos_cand = "SELECT CANDIDAT.*,CAND_A_OPE.* FROM CANDIDAT, CAND_A_OPE WHERE CAND_A_OPE.CAND_ID = CANDIDAT.CAND_ID AND CANDIDAT.CAND_ACTIF='1' AND CANDIDAT.CAND_ID='".txt_db(intval($condidat['cand_id']))."'  AND CAND_A_OPE.OPE_ID=".txt_db(intval($opeid))."";
		$infos_candidat = $db->query($sql_infos_cand);            
        if ($infos_candidat[0]['niveau'] == 1){
			$manger_cand_id = $condidat['cand_id'];
        	$manger_nom = $infos_candidat[0]['cand_prenom'] . ' '. $infos_candidat[0]['cand_nom'];
			$cand_fonction = $infos_candidat[0]['cand_fonction'];
        }
        if ($infos_candidat[0]['niveau'] == 2){
        	$mangerplusun_nom = $infos_candidat[0]['cand_prenom'] . ' '. $infos_candidat[0]['cand_nom'];        	
        }        
		$nb_personnes_mg++;
		
		/* BPM */
		$sql_questionnaire = "SELECT distinct questionnaire.quest_id,quest_type_question_code_id FROM recup_quest,questionnaire,cand_ope 
		WHERE recup_quest.txt_img_quest_id=questionnaire.quest_id and cand_ope.prod_id=recup_quest.prod_id and cand_id='".txt_db(intval($condidat['cand_id']))."' AND ope_id='".txt_db(intval($opeid))."'";
		$qry_questionnaire = $db->query($sql_questionnaire);				
		$quest_id = $qry_questionnaire[0]['quest_id'];
    }

    $sql_questions  = "SELECT * FROM QUEST_A_LANG,QUESTIONNAIRE, CHOIX, QUEST_A_VAL WHERE QUEST_A_VAL.VAL_QUEST_ID=QUESTIONNAIRE.QUEST_ID
		AND QUEST_A_VAL.VAL_CHOIX_ID=CHOIX.CHOIX_ID AND QUESTIONNAIRE.QUEST_ID=".txt_db(intval($quest_id))."
		AND QUEST_A_LANG.QUEST_ID(+)=QUESTIONNAIRE.QUEST_ID AND QUEST_A_VAL.VAL_DATE_SUPPRESSION IS NULL
		ORDER BY QUEST_A_VAL.VAL_ORDRE, QUEST_A_VAL.VAL_CHOIX_ID ASC, CHOIX.CODE_ID DESC";
    	$questions 		= $db->query($sql_questions);
    	
	$themes = array();		
    foreach($questions as $question){
		$sql_bpm_quest = "SELECT * FROM BPM WHERE ITHEME_ID='".txt_db($question['code_id'])."'";
		$qry_bpm_quest = $db->query($sql_bpm_quest);			

		$theme_id = $qry_bpm_quest[0]['theme_id'];
		$stheme_id = $qry_bpm_quest[0]['stheme_id'];
		$itheme_id = $qry_bpm_quest[0]['itheme_id'];	
		
		$sql_theme_quest = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='BPM_THEME' AND CODE_LANG_ID=1 and CODE_ACTIF=1 AND CODE_ID='".$theme_id."' ";
		$qry_theme_quest = $db->query($sql_theme_quest);
											
		$sql_sthemes = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='BPM_STHEME' AND CODE_LANG_ID=1 and CODE_ACTIF=1 AND CODE_ID='".$stheme_id."' ";
		$qry_sthemes = $db->query($sql_sthemes);
											
		$sql_ithemes = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='BPM_ITHEME' AND CODE_LANG_ID=1 and CODE_ACTIF=1 AND CODE_ID='".$itheme_id."' ";
		$qry_ithemes = $db->query($sql_ithemes);
											
		$code_libelle_theme = $qry_theme_quest[0]['code_libelle'];
		$code_libelle_stheme = $qry_sthemes[0]['code_libelle'];
		$code_libelle_itheme = $qry_ithemes[0]['code_libelle'];	

		if (empty($themes[$theme_id]))
		{
			$choix = array();			
		 	$theme = new stdClass;
		 	$stheme = new stdClass;
		 	$itheme = new stdClass;
		 			 	
		 	$theme->max = 1;		 	
		 	$stheme->max = 1;
		 	$itheme->max = 1;

		 	$theme->code_libelle = $code_libelle_theme;		 	
		 	$stheme->code_libelle = $code_libelle_stheme;
		 	$itheme->code_libelle = $code_libelle_itheme;
			
		 	$theme->stheme[$stheme_id]=$stheme;
		 	
			$sql_auto_eval = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=1 and choix_id=".$question['choix_id'];
			$qry_auto_eval = $db->query($sql_auto_eval);
			$sql_nplusun = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=2 and choix_id=".$question['choix_id'];
			$qry_nplusun = $db->query($sql_nplusun);
			$sql_coll = "select round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0 and choix_id=".$question['choix_id'];
			$qry_coll = $db->query($sql_coll);
			
			$quest = new stdClass;
			$quest->question = $question['choix_1'];
			$quest->auto_eval = $qry_auto_eval[0]['txt_libre'];
			$quest->nplusun = $qry_nplusun[0]['txt_libre'];
			$quest->coll = $qry_coll[0]['note'];
															
			$itheme->questions[] = $quest;
										 	
		 	$theme->stheme[$stheme_id]->itheme[$itheme_id]=$itheme;
		 		 	
			$themes[$theme_id]=$theme;
		}
		else 
		{
			$themeObj = $themes[$theme_id];
			$themeObj->max += 1;
			
			if (empty($themeObj->stheme[$stheme_id]))
			{
				$sthemes = array();				
				$theme = $themes[$theme_id];
				
				$stheme = new stdClass;		 					
		 		$stheme->max = 1;	
						 
		 		$stheme->code_libelle = $code_libelle_stheme;
		 					
				$themeObj->stheme[$stheme_id] = $stheme;
				
				$itheme = new stdClass;
		 		$itheme->max = 1;
		 		$itheme->code_libelle = $code_libelle_itheme;
					 			 			
				$sql_auto_eval = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=1 and choix_id=".$question['choix_id'];
				$qry_auto_eval = $db->query($sql_auto_eval);
				$sql_nplusun = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=2 and choix_id=".$question['choix_id'];
				$qry_nplusun = $db->query($sql_nplusun);
				$sql_coll = "select round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0 and choix_id=".$question['choix_id'];
				$qry_coll = $db->query($sql_coll);
					
				$quest = new stdClass;
				$quest->question = $question['choix_1'];
				$quest->auto_eval = $qry_auto_eval[0]['txt_libre'];
				$quest->nplusun = $qry_nplusun[0]['txt_libre'];
				$quest->coll = $qry_coll[0]['note'];
															
				$itheme->questions[] = $quest;
									 			 			
				$themeObj->stheme[$stheme_id]->itheme[$itheme_id] = $itheme;
					
			} else {
				$stheme = $themeObj->stheme[$stheme_id];
				$stheme->max += 1;
				$themeObj->stheme[$stheme_id] = $stheme;					
				
				if (empty($themeObj->stheme[$stheme_id]->itheme[$itheme_id]))
				{
					$itheme = new stdClass;
			 		$itheme->max = 1;
		 			$itheme->code_libelle = $code_libelle_itheme;			 											
					$sql_auto_eval = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=1 and choix_id=".$question['choix_id'];
					$qry_auto_eval = $db->query($sql_auto_eval);
					$sql_nplusun = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=2 and choix_id=".$question['choix_id'];
					$qry_nplusun = $db->query($sql_nplusun);
					$sql_coll = "select round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0 and choix_id=".$question['choix_id'];
					$qry_coll = $db->query($sql_coll);
					
					$quest = new stdClass;
					$quest->question = $question['choix_1'];
					$quest->auto_eval = $qry_auto_eval[0]['txt_libre'];
					$quest->nplusun = $qry_nplusun[0]['txt_libre'];
					$quest->coll = $qry_coll[0]['note'];
															
					$itheme->questions[] = $quest;
										
					$themeObj->stheme[$stheme_id]->itheme[$itheme_id] = $itheme;			 		
				} else {
					$itheme = $stheme->itheme[$itheme_id];
					$itheme->max += 1;

					//Auto evaluation : 
					$sql_auto_eval = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=1 and choix_id=".$question['choix_id'];
					$qry_auto_eval = $db->query($sql_auto_eval);
					//N+1 : 
					$sql_nplusun = "select txt_libre from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=2 and choix_id=".$question['choix_id'];
					$qry_nplusun = $db->query($sql_nplusun);
					//Coll : 
					$sql_coll = "select round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0 and choix_id=".$question['choix_id'];
					$qry_coll = $db->query($sql_coll);
					
					$quest = new stdClass;					
					$quest->question = $question['choix_1'];
					$quest->auto_eval = $qry_auto_eval[0]['txt_libre'];
					$quest->nplusun = $qry_nplusun[0]['txt_libre'];
					$quest->coll = $qry_coll[0]['note'];
															
					$itheme->questions[] = $quest;
										
					$themeObj->stheme[$stheme_id]->itheme[$itheme_id] = $itheme;
				}
			}
			$themes[$theme_id] = $themeObj;			
		}
    }
}
?>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title>VAKOM | Barometre des postures manag&eacute;riales</title>
	<link rel="icon" type="image/jpg" href="/favicon.jpg" />
	
	<meta name="robots" content="noindex,follow" />
	<link href="style.css" rel="stylesheet" type="text/css" />    	
</head>
<body>
	<button id="cmd">Imprimer</button>	
	<div id="pageTop" class="hfeed site top">
		<div class="logo-vakom">				
			<img src="../images/logo-bpm.png" style="height: 75px;" alt="logo"/>				
		</div>
		<div id="secondary" class="sidebar-container sidebar-header">
			<div class="widget-area">
			<div id="nav_menu-2" class="widget widget_nav_menu">
				<div class="menu-menu-top-container">
        	    	<ul id="menu-menu-top" class="menu">
						<li  class="menu-item "><a href="#">
								<img src="../images/vakom-sens-de-humain.png" style="height: 105px;margin-top: -15px;" alt="logo client"></a>								
						</li>
					</ul>
				</div>
			</div>		
		</div>
		</div>
	</div>	
	<div id="page" class="site">	
		<div id="main" class="site-main">
			<div id="content">
				<div class="entry-contentAdmin">	
					<p class="TitreTheme">&eacute;l&eacute;ments d'analyse</p>
					<table class="tableAnalyse">
			<tbody>
			    <tr>
      <td style="width: 30%"  >Nom et pr&eacute;nom de la personne &eacute;valu&eacute;e</td>
      <td><?php echo$manger_nom?></td>
      </tr>
			    <tr>
      <td  >Fonction</td>
      <td  ><?php echo$cand_fonction?></td>
      </tr>
			    <tr>
      <td  >Nombre de collaborateurs inscrits</td>
      <td ><?php echo$nb_personnes_mg?></td>
      </tr>
			    <tr>
      <td  >Nom du manager de la personne &eacute;valu&eacute;e</td>
      <td ><?php echo$mangerplusun_nom?></td>
      </tr>
    		</tbody>
		</table>	
					<div class="CadreMoyenne">	
	<?php
		$sql_coll = "select cand_id,round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0 group by cand_id";
		$qry_coll = $db->query($sql_coll);	
	?>

	<table class="tableMoyenne">
  		<tbody>
	<?php 
		$index = 1;
		foreach($qry_coll as $coll) { ?>
    	<tr>
	      <th>Moyenne collaborateur <?php echo$index?></th>
    	  <td><?php echo $coll['note'];?></td>
	    </tr>		
	<?php $index++;} 
			$sql_coll = "select round((sum(to_number(txt_libre))/count(choix_id)),2) note from reponse_bpm where ope_id='".txt_db(intval($opeid))."' and niveau=0";
			$qry_coll = $db->query($sql_coll);
	?>
    <tr>
      <th >Moyenne collaborateur G&eacute;n&eacute;rale</th>
      <td ><?php echo $qry_coll[0]['note'];?></td>
    </tr>
    </tbody>
	</table>
</div>
					<div class="CadreCentre">
						<div class="cadreCentre">
	<?php 
	$table ='<table class="tableResultAnalyse"><tbody>';
	$table .='<tr><th >Th&egrave;me</th><th >Sous-th&egrave;me</th><th >Items</th><th >Affirmations</th>';
	$table .='<th style="background-color: #0b2e43; width: 8%" >Auto<br>&eacute;valuation</th>';
	$table .='<th style="background-color: #3785b3; width: 8%" >N+1</th>';
	$table .='<th style="background-color: #46b0ef; width: 8%">Moyenne<br>Coll</th></tr>';
    $style = 0;    
    foreach($themes as $theme){
    	$index_theme = 0;    
	    foreach($theme->stheme as $stheme){
    		$index_stheme = 0; 
		    foreach($stheme->itheme as $itheme){  
    			$index_itheme = 0;
    			
		    	foreach($itheme->questions as $quest)
		    	//for($max = 0; $max<$itheme->max;$max++ )
		    	{
	    			$table .="<tr>";
		    		if ($index_theme == 0)
		    		{
						$table .="<td rowspan=$theme->max style='width: 4%;text-align: center;'>".$theme->code_libelle."</td>";
		    		}
					if ($index_stheme == 0)
		    		{		    		
		    			$table .="<td rowspan=$stheme->max style='width: 4%;text-align: center;'>".$stheme->code_libelle."</td>";
					}		    		
		    		if ($index_itheme == 0)
		    		{
						if ($style%2 == 0){
			    			$table .="<td class='color' rowspan=$itheme->max style='width: 4%;text-align: center;'>".$itheme->code_libelle."</td>";						
						} else {
			    			$table .="<td rowspan=$itheme->max>".$itheme->code_libelle."</td>";						
						}
		    		}
		    		if ( $style%2 == 0 ){
		    			$table .="<td class='color'>".$quest->question."</td>";
		    		}else {
		    			$table .="<td style='width: 46%;'>".$quest->question."</td>";		    		
		    		}
		    		$table .="<td class='center'>".$quest->auto_eval."</td>";
		    		$table .="<td class='center'>".$quest->nplusun."</td>";
		    		$table .="<td class='center'>".$quest->coll."</td>";		    		
	    			$table .="</tr>";
	    			$index_itheme++;		    		    			
					$index_stheme++;	    			
					$index_theme++;					
		    	}
				$style++;
			}
		}			    			
    }
	$table .="</tbody></table>";	
	?>
	<?php echo$table?>
<div class="cadrePagination"> </div>	

</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="html2canvas.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
<script>
	$('#cmd').click(function () {
		$('#cmd').hide();
	    html2canvas(window.document.body[0], 
    	{
			onrendered: function(canvas) {
        		var myImage = canvas.toDataURL("image/png");
    		    $(window.document.body[0])
        		    .html("<img id='Image' src=" + myImage + " style='width:100%;'></img>")
            		.ready(function() {
                		window.focus();
	                	window.print();
						$('#cmd').show();
    	        });
    		}        	
	    });
	});
</script>
</html>