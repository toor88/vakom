<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['cert_id']!='' && $_GET['cliid']!=''){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	$db = new db($conn);

	if ($_POST['posted']){
		/* Modification de la table client pour le client en question */
		$sql_update_clt = "UPDATE CLIENT SET CLI_NOM='".txt_db($_POST['nom'])."',
		CLI_AD1 = '".txt_db($_POST['adr1'])."',
		CLI_AD2 = '".txt_db($_POST['adr2'])."',
		CLI_CP = '".txt_db($_POST['cp'])."',
		CLI_VILLE = '".txt_db($_POST['ville'])."',
		CLI_PAYS_CODE_ID = '".txt_db($_POST['select_pays'])."',
		CLI_TEL = '".txt_db($_POST['tel'])."',
		CLI_FAX = '".txt_db($_POST['fax'])."',
		CLI_EMAIL = '".txt_db($_POST['email'])."',
		CLI_CONTACT = '".txt_db($_POST['contact'])."',
		CLI_INFO1 = '".txt_db($_POST['comm1'])."',
		CLI_INFO2 = '".txt_db($_POST['comm2'])."',
		CLI_ACTIF = '".txt_db($_POST['actif'])."',
		CLI_USER_MODIFICATION_ID = '".txt_db($_SESSION['cert_id'])."',
		CLI_DATE_MODIFICATION = SYSDATE WHERE CLI_ID='".txt_db(intval($_GET['cliid']))."'";
		//echo $sql_update_clt;
		$qry_update_clt = $db->query($sql_update_clt);
		
		if (is_array($_POST['partage'])){
		
			$sql_delete_clt_a_cert 	= "DELETE FROM CLIENT_A_CERT WHERE CLI_ID='".txt_db(intval($_GET['cliid']))."'";
			$qry_delete_clt_a_cert 	= $db->query($sql_delete_clt_a_cert);
		
			foreach($_POST['partage'] as $cert_id){
				$sql_select_ins_clt_a_cert 	= "SELECT * FROM CLIENT_A_CERT WHERE CLI_ID='".txt_db(intval($_GET['cliid']))."' AND CERT_ID='".txt_db(intval($cert_id))."'";
				$qry_select_ins_clt_a_cert	= $db->query($sql_select_ins_clt_a_cert);
				if (!is_array($qry_select_ins_clt_a_cert)){
					$sql_ins_clt_a_cert 		= "INSERT INTO CLIENT_A_CERT VALUES('".txt_db(intval($_GET['cliid']))."', '".txt_db(intval($cert_id))."')";
					$qry_ins_clt_a_cert			= $db->query($sql_ins_clt_a_cert);
				}
			}
		}
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>		
			<?php
	}
	
		/* Sélection des informations du client */
		$sql_info_clt	= "SELECT * FROM CLIENT, CLIENT_A_CERT WHERE CLIENT.CLI_ID='".txt_db(intval($_GET['cliid']))."' AND CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND CLIENT_A_CERT.CERT_ID='".txt_db($_SESSION['cert_id'])."'";
		$qry_info_clt	= $db->query($sql_info_clt);	
		//echo $qry_info_clt;

		$sql_info_cert = "SELECT * FROM CERTIFIE WHERE CERT_ID='".txt_db($qry_info_clt[0]['cert_id'])."'";
		$qry_info_cert = $db->query($sql_info_cert);

		$sql_pays_list = "SELECT * FROM CODE WHERE CODE_TABLE='PAYS'";
		$qry_pays_list = $db->query($sql_pays_list);

	if (is_array($qry_info_clt)){
		
		/* On sélectionne tous les certifiés du partenaire */
		$sql_list_certifie = "SELECT CERT_NOM, CERT_PRENOM, CERT_ID FROM CERTIFIE, PARTENAIRE WHERE PARTENAIRE.PART_ID=CERTIFIE.CERT_PART_ID AND PARTENAIRE.PART_ID='".$qry_info_cert[0]['cert_part_id']."' AND CERT_DATE_SUPPRESSION IS NULL";
		//echo $sql_list_certifie;
		$qry_list_certifie = $db->query($sql_list_certifie);
		?>
		<html>
			<head>
				<title>Vakom</title>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<link rel="stylesheet" href="../css/nvo.css" type="text/css">
				<link rel="stylesheet" href="../css/general.css" type="text/css">
				<script language="JavaScript">
					<!--
					function che_all(field){
						if (document.form.select_all.checked==true){
							field.checked=true;
							for (i=0;i<field.length;i++)
							{
							field[i].checked = true;
							}
						}
						if (document.form.select_all.checked==false){
							field.checked=false;
							for (i=0;i<field.length;i++)
							{
							field[i].checked = false;
							}
						}
					}
					function verif(){
						error = '';
						if (document.form.nom.value == ''){
							error += "<?php echo $t_nom_oblig ?>\n";
						}
						if (error!=''){
							alert(error);
						}else{
							document.form.submit();
						}
					}
					//-->
				</script>
			</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#" name="form">
		  <table border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td align="left" class="TX"> 
					<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					  <td width="20">&nbsp;</td>
					  <td class="Titre_Candidats"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php
						echo strtoupper($qry_info_cert[0]['cert_nom']).' '.ucfirst(strtolower($qry_info_cert[0]['cert_prenom'])).' > '.htmlentities($qry_info_clt[0]['cli_nom']);
					?></td>
					</tr>
				   </table>
				<table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="800">
				  <tr> 
					<td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				  </tr>
				  <tr> 
					<td width="14"></td>
					<td align="center" valign="top" class="TX"> 
					  <table width="750" border="0" cellspacing="0" cellpadding="0">
					  <tr> 
						<td class="fond_tablo_candidats" height="40" align="left"><?php echo $t_fiche_clt ?></td>
						<td class="fond_tablo_candidats2" valign="middle" align="left"><?php echo $t_date_creation ?>
						  : le <?php echo $qry_info_clt[0]['cli_date_creation'] ?><br>
						  <?php
						  if ($qry_info_clt[0]['cli_date_modification']!=''){
							  ?>
							  <?php echo $t_date_modif ?> : le <?php echo $qry_info_clt[0]['cli_date_modification'] ?>
							  <?php
						  }
						  ?>
						  </td>
					  </tr>
						<tr> 
						  <td class="TX">&nbsp;</td>
						  <td class="champsoblig" valign="middle" align="right"><?php echo $t_champs_oblig	?> * </td>
						</tr>
						<tr> 
						  <td class="TX">&nbsp;</td>
						  <td class="TX" valign="middle" align="right">&nbsp;</td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_fiche_ent_nom ?>* :&nbsp;</td>
						  <td> 
							<input type="text" name="nom" size="70" maxlength="70" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_nom']) ?>">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_adresse ?> :</td>
						  <td> 
							<input type="text" name="adr1" size="70" maxlength="70" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_ad1']) ?>">
						  </td>
						</tr>
						<tr> 
						  <td class="TX">&nbsp;</td>
						  <td class="TX"> 
							<input type="text" name="adr2" size="70" maxlength="70" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_ad2']) ?>">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_cp ?> :</td>
						  <td class="TX"> 
							<input type="text" name="cp" size="5" maxlength="10" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_cp']) ?>">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $t_ville ?> 
							<input type="text" name="ville" size="50" maxlength="50" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_ville']) ?>">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_pays ?>* :</td>
						  <td> 
							  <select name="select_pays" class="form_ediht_Candidats">
								<?php
								  if (is_array($qry_pays_list)){
									foreach($qry_pays_list as $pays){
										unset($selected_pays);
										if ($qry_info_clt[0]['cli_pays_code_id']==$pays['code_id']){
											$selected_pays = ' selected="selected"';
										}
										echo '<option value="'.$pays['code_id'].'"'.$selected_pays.'>'.$pays['code_libelle'].'</option>';
									}
								  }
								?>
							  </select>
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_tel ?> :</td>
						  <td class="TX"> 
							<input type="text" name="tel" size="25" maxlength="25" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_tel']) ?>">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $t_fax ?> 
							: 
							<input type="text" name="fax" size="25" maxlength="25" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_fax']) ?>">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_email ?>&nbsp;&nbsp;&nbsp;: </td>
						  <td> 
							<input type="text" name="email" size="25" maxlength="70" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_email']) ?>">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_contact ?> :</td>
						  <td> 
							<input type="text" name="contact" size="70" maxlength="70" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_contact']) ?>">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40" valign="top"><?php echo $t_comment ?> :</td>
						  <td> 
							<input type="text" name="comm1" size="70" maxlength="70" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_info1']) ?>">
							<br>
							<input type="text" name="comm2" size="70" maxlength="70" class="form_ediht_Candidats" value="<?php echo htmlentities($qry_info_clt[0]['cli_info2']) ?>">
						  </td>
						</tr>
						<tr> 
						  <td class="TX" height="40"><?php echo $t_fiche_clt_actif ?> :</td>
						  <td class="TX"> 
							<input type="checkbox" name="actif" value="1" <?php if ($qry_info_clt[0]['cli_actif']=='1') echo 'checked="checked"'; ?>>
						  </td>
						</tr>
						<tr> 
						  <td class="TX">&nbsp;</td>
						  <td class="TX">&nbsp;</td>
						</tr>
						
						<tr> 
						  <td class="TX" height="40" valign="top"><?php echo $t_fiche_soc_partage ?> 
							:&nbsp;&nbsp; </td>
						  <td class="TX"> 
							<table width="100%" border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000">
							  <tr> 
								<td class="TX_bold"><?php echo $t_certs1 ?></td>
								<td class="TX_bold" align="center"><?php echo $t_selection ?></td>
							  </tr>
							  <tr bgcolor="F1F1F1"> 
								<td>&nbsp;</td>
								<td align="center"> 
								  <input type="checkbox" name="select_all" id="select_all" onclick="che_all(document.form.partage)" value="1">
								  <?php echo $t_tous ?></td>
							  </tr>
							  <?php
							  if (is_array($qry_list_certifie)){
								  foreach($qry_list_certifie as $list_certifie){
									?>
									  <tr bgcolor="F1F1F1"> 
										<td><?php echo ucfirst($list_certifie['cert_prenom']).' '.strtoupper($list_certifie['cert_nom'])?></td>
										<td align="center"> 
										<?php 		/* On sélectionne tous les certifiés du partenaire */
										unset ($checked);
										$sql_list_clt_a_certifie = "SELECT * FROM CLIENT_A_CERT WHERE CLI_ID='".txt_db(intval($_GET['cliid']))."' AND CERT_ID='".$list_certifie['cert_id']."'";
										$qry_list_clt_a_certifie = $db->query($sql_list_clt_a_certifie);
										if (is_array($qry_list_clt_a_certifie)){
											$checked = ' checked="checked"'; 
										}										
										?>
										  <input type="checkbox" name="partage[]" id="partage" value="<?php echo $list_certifie['cert_id'] ?>"<?php echo $checked ?>>
										</td>
									  </tr>
									<?php
								  }
							  }
							  ?>
							</table>
						  </td>
						</tr>
						
					  </table>
					  <p>&nbsp;</p>
					</td>
					<td width="14"></td>
				  </tr>
				  <tr> 
					<td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
					<td height="14"></td>
					<td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				  </tr>
				</table>
			  </td>
			</tr>
			<tr>
			  <td align="center">&nbsp;</td>
			</tr>
			<tr> 
			  <td align="center"> 
				<input type="hidden" name="posted" value="1">
				<input type="button" name="Submit" value="<?php echo $t_btn_valider ?>" class="bn_valider_candidat" onclick="verif();">
			  </td>
			</tr>
			<tr> 
			  <td align="right" width="180">&nbsp; </td>
			</tr>
		  </table>
		</form>
		</body>
		</html>
	<?php
	}else{
	echo ' ';
		//include('no_acces.php');
	}
}else{
	include('no_acces.php');
}
?>