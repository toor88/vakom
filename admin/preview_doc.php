<?php
session_start();
if ($_SESSION['droit']!=''){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	if ($_GET['candid'] && $_GET['opeid'] && $_GET['dossid']){
	
		/* Si le candidat est sélectionné */
		$sql_info_candidat = "select CAND_NOM,CAND_PRENOM,CAND_SEXE,CLI_NOM,CAND_LANG_ID
								from candidat,client
								where candidat.cand_cli_id=client.cli_id(+)
								and cand_id='".txt_db(intval($_GET['candid']))."'";
		$qry_info_candidat = $db->query($sql_info_candidat);
	
		/* On sélectionne les informations sur le dossier pour le candidat et l'opération sélectionnés */	
		$sql_infos_doss = "SELECT DOSSIER_ID,CAND_ID,OPE_ID,PROD_NOM,TITRE,DOC_ID,DOC_NOM,TYPE_GABARIT, TRI FROM CAND_DOC 
			WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' 
				AND OPE_ID='".txt_db(intval($_GET['opeid']))."' 
				AND DOSSIER_ID='".txt_db(intval($_GET['dossid']))."' 
			GROUP BY DOSSIER_ID,CAND_ID,OPE_ID,PROD_NOM,TITRE,DOC_ID,DOC_NOM,TYPE_GABARIT, TRI ORDER BY DOSSIER_ID,TRI";
		$qry_infos_doss = $db->query($sql_infos_doss);
//echo $sql_infos_doss.'<br>';		
		
		echo '<!DOCTYPE html>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">';
		?>
		<script src="../js/jquery-latest.min.js"></script>
		<strong><script src="https://cdn.tiny.cloud/1/n15lggt937iydpcbh026tvnj0a11vtiwcjd2tdoxiaf4y4da/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script></strong>
		<script language="JavaScript">
		<!--
		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		//-->
		
		tinymce.init({
			selector: 'textarea',
			height: 300,
			width:600,
			plugins: [
			  'advlist autolink link image lists charmap print preview hr anchor pagebreak',
			  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			  'table emoticons template paste help'
			],
			toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
			  'bullist numlist outdent indent | link image | print preview media fullpage | ' +
			  'forecolor backcolor emoticons | help',
			menu: {
			  favs: {title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons'}
			},
			menubar: 'favs file edit view insert format tools table help',
			content_css: 'css/content.css'
		  });
		function uploadFile(inp, editor) {
			var input = inp.get(0);
			var data = new FormData();
			data.append('image[file]', input.files[0]);

			$.ajax({
				url: 'uploads/post.php',
				type: 'POST',
				data: data,
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				success: function(data, textStatus, jqXHR) {
					data = jQuery.parseJSON(data);
					editor.insertContent('<img class="content-img" src="uploads/images/' + data.location + '"/>');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if(jqXHR.responseText) {
						errors = JSON.parse(jqXHR.responseText).errors
						alert('Error uploading image: ' + errors.join(", ") + '. Make sure the file is an image and has extension jpg/jpeg/png.');
					}
				}
			});
		}
		</script>
		</head>
		<body bgcolor="#FFFFFF" text="#000000" width="100%">
			<table align="center" width="100%"><tr><td align="center">
		<?php
		echo '<center><img src="../images/logo.jpg" alt=""></center><br><br>';
		echo '<center><h1>'.strtoupper($qry_infos_doss[0]['titre']).'</h1></center><br><hr><br>';
		echo '<center><h2>'.$qry_info_candidat[0]['cand_prenom'].' '.$qry_info_candidat[0]['cand_nom'].'</h2></center>';
		echo '<center><h2>'.$qry_info_candidat[0]['cli_nom'].'</h2></center><br>';
		echo '<center><h3><i>'.date('d/m/Y').'</i></h3></center><br><br>';
		/* Pour chaque document du dossier */
		foreach($qry_infos_doss as $infos_document){
		echo '<table align="center"><tr><td align="left">';
			echo '<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$infos_document['doc_nom'].'</h3>';
			/* On regarde le type de gabarit pour ce document, on définit dans quelle(s) zone(s) on doit chercher */
			switch ($infos_document['type_gabarit']){
				case 1:
				default:
					$nb_zones = 1;
				break;
				case 2:
					$nb_zones = 2;
				break;
				case 3:
				case 4:
					$nb_zones = 3;
				break;
				case 5:
					$nb_zones = 4;
				break;
			}
			
			/* On recherche dans les zones du document */
			for ($zone_id=1;$zone_id<=$nb_zones;$zone_id++){
				$sql_infos_zones = "SELECT CAND_DOC.* FROM CAND_DOC 
				WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' 
					AND OPE_ID='".txt_db(intval($_GET['opeid']))."' 
					AND DOSSIER_ID='".txt_db(intval($_GET['dossid']))."' 
					AND DOC_ID='".txt_db(intval($infos_document['doc_id']))."' 
					AND NUM_ZONE='".$zone_id."' 
				ORDER BY DOSSIER_ID,TRI";

				$infos_zones = $db->query($sql_infos_zones);
				switch($infos_zones[0]['type_zone'.$zone_id]){
					case 1:
						$sql_select_graphe = "SELECT * FROM GRAPHE_A_QUEST WHERE GRAPHE_ID='".txt_db(intval($infos_zones[0]['doc_graphe_id']))."'";
						//echo $sql_select_graphe.'<br>';
						$qry_select_graphe = $db->query($sql_select_graphe);	
						if ($infos_document['type_gabarit'] == 1){
							if ($qry_select_graphe[0]['graphe_id'] == "10258") {
								$contenu[$zone_id] = '<iframe align="center" src="http://crystal.ornis.com/edition_cr/bin/vakom_pnpi.asp?cand_id='.txt_db(intval($_GET['candid'])).'&ope_id='.txt_db(intval($_GET['opeid'])).'&quest_id='.$qry_select_graphe[0]['quest_id'].'" align=left scrolling=no width="100%" height="1200">GRAPHE</iframe>';
							}
							elseif ($qry_select_graphe[0]['type_profil_code_id'] == "29") {
								$contenu[$zone_id] = '<iframe align="center" src="http://crystal.ornis.com/edition_cr/bin/vakom_pi.asp?type='.$qry_select_graphe[0]['type_profil_code_id'].'&cand_id='.txt_db(intval($_GET['candid'])).'&ope_id='.txt_db(intval($_GET['opeid'])).'&quest_id='.$qry_select_graphe[0]['quest_id'].'" align=left scrolling=no width="100%" height="1200">GRAPHE</iframe>';
							}
							elseif ($qry_select_graphe[0]['type_profil_code_id'] == "30") {
								$contenu[$zone_id] = '<iframe align="center" src="http://crystal.ornis.com/edition_cr/bin/vakom_pn.asp?type='.$qry_select_graphe[0]['type_profil_code_id'].'&cand_id='.txt_db(intval($_GET['candid'])).'&ope_id='.txt_db(intval($_GET['opeid'])).'&quest_id='.$qry_select_graphe[0]['quest_id'].'" align=left scrolling=no width="100%" height="1200">GRAPHE</iframe>';
							}
							elseif ($qry_select_graphe[0]['type_profil_code_id'] == "32") {
								$contenu[$zone_id] = '<iframe align="center" src="http://crystal.ornis.com/edition_cr/bin/vakom_pr.asp?type='.$qry_select_graphe[0]['type_profil_code_id'].'&cand_id='.txt_db(intval($_GET['candid'])).'&ope_id='.txt_db(intval($_GET['opeid'])).'&quest_id='.$qry_select_graphe[0]['quest_id'].'" align=left scrolling=no width="100%" height="1200">GRAPHE</iframe>';
							}
							else{
								$contenu[$zone_id] = '<iframe align="center" src="http://crystal.ornis.com/edition_cr/bin/vakom_p.asp?type='.$qry_select_graphe[0]['type_profil_code_id'].'&cand_id='.txt_db(intval($_GET['candid'])).'&ope_id='.txt_db(intval($_GET['opeid'])).'&quest_id='.$qry_select_graphe[0]['quest_id'].'" align=left scrolling=no width="100%" height="1200">GRAPHE</iframe>';							
							}
						}
						else
						{
							if ($qry_select_graphe[0]['type_profil_code_id'] == "29") {
								$contenu[$zone_id] = '<iframe align="center" src="http://crystal.ornis.com/edition_cr/bin/vakom_pi3.asp?test=1&type='.$qry_select_graphe[0]['type_profil_code_id'].'&cand_id='.txt_db(intval($_GET['candid'])).'&ope_id='.txt_db(intval($_GET['opeid'])).'&quest_id='.$qry_select_graphe[0]['quest_id'].'" align=left scrolling=no width="100%" height="1200">GRAPHE</iframe>';
							}
							elseif ($qry_select_graphe[0]['type_profil_code_id'] == "30") {
								$contenu[$zone_id] = '<iframe align="center" src="http://crystal.ornis.com/edition_cr/bin/vakom_pn3.asp?test=1&type='.$qry_select_graphe[0]['type_profil_code_id'].'&cand_id='.txt_db(intval($_GET['candid'])).'&ope_id='.txt_db(intval($_GET['opeid'])).'&quest_id='.$qry_select_graphe[0]['quest_id'].'" align=left scrolling=no width="100%" height="1200">GRAPHE</iframe>';
							}
							elseif ($qry_select_graphe[0]['type_profil_code_id'] == "32") {
								$contenu[$zone_id] = '<iframe align="center" src="http://crystal.ornis.com/edition_cr/bin/vakom_pr3.asp?test=1&type='.$qry_select_graphe[0]['type_profil_code_id'].'&cand_id='.txt_db(intval($_GET['candid'])).'&ope_id='.txt_db(intval($_GET['opeid'])).'&quest_id='.$qry_select_graphe[0]['quest_id'].'" align=left scrolling=no width="100%" height="1200">GRAPHE</iframe>';
							}
						}
						break;
					case 2:
						$contenu[$zone_id] = $infos_zones[0]['doc_txt_img_id'];
					break;
					case 3:
						$sql_select_txt_img = "SELECT * FROM TEXTE_IMAGE WHERE TXT_IMG_ID='".txt_db(intval($infos_zones[0]['doc_txt_img_id']))."'";
						//echo $sql_select_txt_img.'<br>';
						$qry_select_txt_img = $db->query($sql_select_txt_img);
						
						$tab_lettres = array(
							1=>'E',
							2=>'C',
							3=>'P',
							4=>'A');
						$tab_profils = array(
							29=>'PI',
							30=>'PN',
							32=>'PR');
						unset($combi);
						foreach($tab_lettres as $key => $value){
							/* boucle pour chaque lettre */
							switch($qry_select_txt_img[0]['txt_img_precision_code_id']){
								case 34:
									/* Par segment */
									$vars = 'COR_REGLE_SEGMENT';
								break;
								case 35:
									/* Par bloc */
									$vars = 'COR_REGLE_BLOC';
								break;
							}
							
							$sql_combi = "SELECT ".$vars." VAR_".$value." FROM CAND_".$tab_profils[$qry_select_txt_img[0]['txt_img_type_profil_code_id']]." WHERE CAND_ID='".txt_db(intval($_GET['candid']))."' AND OPE_ID='".txt_db(intval($_GET['opeid']))."' AND REPONSE_".$tab_profils[$qry_select_txt_img[0]['txt_img_type_profil_code_id']]." = '".$value."'";
							//echo $sql_combi.'<br>';
							$qry_combi = $db->query($sql_combi);
							$combi[$key] = $qry_combi[0]['var_'.strtolower($value)];
							$valeur_combi .= $combi[$key];
							
							//echo $sql.'<br>';							
						}
						
						$sql_lib_textes = "select * from cand_comb,texte_image
						where cand_comb.txt_img_id=texte_image.txt_img_id
						and cand_comb.txt_img_quest_id=texte_image.txt_img_quest_id
						and cand_comb.TXT_IMG_TYPE_PROFIL_CODE_ID=texte_image.TXT_IMG_TYPE_PROFIL_CODE_ID
						and cand_comb.TXT_IMG_PRECISION_CODE_ID=texte_image.TXT_IMG_PRECISION_CODE_ID
						and texte_image.txt_img_id='".txt_db(intval($infos_zones[0]['doc_txt_img_id']))."'
						and COMBI_E_INF='".$combi[1]."'
						and COMBI_C_INF='".$combi[2]."'
						and COMBI_P_INF='".$combi[3]."'
						and COMBI_A_INF='".$combi[4]."'";
						$sql_lib_textes = $db->query($sql_lib_textes);
						
						$contenu[$zone_id] = $sql_lib_textes[0]['txt_homme'];
					break;
					case 6:
						$contenu[$zone_id] = '<br><center><textarea name="CR_'.$infos_document['doc_id'].'_'.$zone_id.'" id="CR_'.$infos_document['doc_id'].'_'.$zone_id.'">
						<table align="center" border="1" width="450">
							<tr><th bgcolor="#EBEBEB">COLONNE 1</th><th bgcolor="#EBEBEB">COLONNE 2</th></tr>
							<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
							<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
							<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
						</table>
						</textarea></center></br>';
					break;
				}
			}
			
			switch($infos_document['type_gabarit']){
				case 1:
					echo '
					<table width="900" align="center" cellpadding="5" cellspacing="0" style="border: 1px solid black;" height="100%">
						<tr><td bgcolor="#ACACAC" width="900" align="left">'.$contenu[1].'</td></tr>
					</table>';
				break;
				case 2:
					echo '
					<table width="900" align="center" cellpadding="5" cellspacing="0" style="border: 1px solid black;" height="100%">
						<tr><td bgcolor="#ACACAC" width="900" align="left">'.$contenu[1].'</td></tr>
						<tr><td bgcolor="#ACACAC" width="900" align="left">'.$contenu[2].'</td></tr>
					</table>';
				break;
				case 3:
					echo '
					<table width="900" align="center" cellpadding="5" cellspacing="0" style="border: 1px solid black;" height="100%">
						<tr><td bgcolor="#ACACAC" width="450" align="left">'.$contenu[1].'</td>
							<td bgcolor="#ACACAC" width="450" align="left">'.$contenu[2].'</td>
						<tr><td bgcolor="#ACACAC" colspan="2" width="900" align="left">'.$contenu[3].'</td></tr>
					</table>';
				break;
				case 4:
					echo '
					<table width="900" align="center" cellpadding="5" cellspacing="0" style="border: 1px solid black;" height="100%">
						<tr><td bgcolor="#ACACAC" width="900" align="left">'.$contenu[1].'</td></tr>
						<tr><td bgcolor="#ACACAC" width="900" align="left">'.$contenu[2].'</td></tr>
						<tr><td bgcolor="#ACACAC" width="900" align="left">'.$contenu[3].'</td></tr>
					</table>';
				break;
				case 5:
					echo '
					<table width="900" align="center" cellpadding="5" cellspacing="0" style="border: 1px solid black;" height="100%">
						<tr><td bgcolor="#ACACAC" width="300" align="left">'.$contenu[1].'</td>
							<td bgcolor="#ACACAC" width="300" align="left">'.$contenu[2].'</td>
							<td bgcolor="#ACACAC" width="300" align="left">'.$contenu[3].'</td></tr>
						<tr><td bgcolor="#ACACAC" width="900" colspan="3" align="left">'.$contenu[4].'</td></tr>
					</table>';
				break;
			}
			##############################################################
			#############   FIN DE LA BOUCLE DOCUMENT   ##################
			##############################################################
			echo '<br><hr><br></td></tr></table>';
		}
		
/*
  TITRE (DOSSIER)

            DOC_NOM (DOC_ID)

                        TYPE GABARIT (1 à 5)

                                SI =1 => select * from cand_doc where cand_id=1643 and ope_id=8666 and DOSSIER_ID=9 and doc_id=1 and num_zone=1 order by DOSSIER_ID,TRI

                                                SI TYPE_ZONE1=1 =>GRAPHE => DOC_GRAPHE_ID => SELECT TYPE_PROFIL_CODE_ID FROM GRAPHE_A_QUEST WHERE GRAPHE_A_QUEST_ID=DOC_GRAPHE_ID (PN, PR, PI)

                                                SI TYPE_ZONE1=2 =>AFFICHE DOC_TEXTE_FIXE

                                                SI TYPE_ZONE1=3 => RECUPERE DOC_TXT_IMG_ID

                                                                          SELECT * FROM TEXTE_IMAGE WHERE TXT_IMG_ID = DOC_TXT_IMG_ID => TXT_IMG_TYPE_PROFIL_CODE_ID (PN, PR, PI) et TXT_IMG_PRECISION_CODE_ID (Segment - Bloc)

                                                                                       SI BLOC : select COR_REGLE_BLOC VAR_E from (cand_pn ou cand_pr ou cand_pi) where cand_id=1643 and ope_id=8666 and REPONSE_PN='E'

                                                                                                       select COR_REGLE_BLOC VAR_C from (cand_pn ou cand_pr ou cand_pi) where cand_id=1643 and ope_id=8666 and REPONSE_PN='C'

                                                                                                       select COR_REGLE_BLOC VAR_P from (cand_pn ou cand_pr ou cand_pi) where cand_id=1643 and ope_id=8666 and REPONSE_PN='P'

                                                                                                       select COR_REGLE_BLOC VAR_A from (cand_pn ou cand_pr ou cand_pi) where cand_id=1643 and ope_id=8666 and REPONSE_PN='A'

                                                                                       SI SEGMENT: select COR_REGLE_SEGMENT VAR_E from (cand_pn ou cand_pr ou cand_pi) where cand_id=1643 and ope_id=8666 and REPONSE_PN='E'

                                                                                                       select COR_REGLE_SEGMENT VAR_C from (cand_pn ou cand_pr ou cand_pi) where cand_id=1643 and ope_id=8666 and REPONSE_PN='C'

                                                                                                       select COR_REGLE_SEGMENT VAR_P from (cand_pn ou cand_pr ou cand_pi) where cand_id=1643 and ope_id=8666 and REPONSE_PN='P'

                                                                                                       select COR_REGLE_SEGMENT VAR_A from (cand_pn ou cand_pr ou cand_pi) where cand_id=1643 and ope_id=8666 and REPONSE_PN='A'

                                                                            select * from cand_comb,texte_image

                                                                            where 

                                                                            cand_comb.txt_img_id=texte_image.txt_img_id

                                                                            and cand_comb.txt_img_quest_id=texte_image.txt_img_quest_id

                                                                             and cand_comb.TXT_IMG_TYPE_PROFIL_CODE_ID=texte_image.TXT_IMG_TYPE_PROFIL_CODE_ID

                                                                            and cand_comb.TXT_IMG_PRECISION_CODE_ID=texte_image.TXT_IMG_PRECISION_CODE_ID

                                                                            and texte_image.txt_img_id=DOC_TXT_IMG_ID

                                                                            and COMBI_E_INF=VAR_E

                                                                            and COMBI_C_INF=VAR_C

                                                                            and COMBI_P_INF=VAR_P

                                                                            and COMBI_A_INF=VAR_A

                                                                                =>>>>>>>>>>>> TXT_HOMME       TXT_FEMME

                                                            SI TYPE_ZONE1=6 => AFFICHE UNE ZONE SAISIE MULTI LIGNE WISYWYG AVEC LES DEUX COLLONES

                                SI =2 => select * from cand_doc where cand_id=1643 and ope_id=8666 and DOSSIER_ID=9 and doc_id=1 and num_zone=1 order by DOSSIER_ID,TRI

                                                                                                XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                                             select * from cand_doc where cand_id=1643 and ope_id=8666 and DOSSIER_ID=9 and doc_id=1 and num_zone=2 order by DOSSIER_ID,TRI

                                                                                                XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                                SI =3,4 => select * from cand_doc where cand_id=1643 and ope_id=8666 and DOSSIER_ID=9 and doc_id=1 and num_zone=1 order by DOSSIER_ID,TRI

                                                                                                XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                                             select * from cand_doc where cand_id=1643 and ope_id=8666 and DOSSIER_ID=9 and doc_id=1 and num_zone=2 order by DOSSIER_ID,TRI

                                                                                                XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                                            select * from cand_doc where cand_id=1643 and ope_id=8666 and DOSSIER_ID=9 and doc_id=1 and num_zone=3 order by DOSSIER_ID,TRI 

                                                                                                XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                                SI =5 => select * from cand_doc where cand_id=1643 and ope_id=8666 and DOSSIER_ID=9 and doc_id=1 and num_zone=1 order by DOSSIER_ID,TRI

                                                                                                XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                                             select * from cand_doc where cand_id=1643 and ope_id=8666 and DOSSIER_ID=9 and doc_id=1 and num_zone=2 order by DOSSIER_ID,TRI

                                                                                                XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                                            select * from cand_doc where cand_id=1643 and ope_id=8666 and DOSSIER_ID=9 and doc_id=1 and num_zone=3 order by DOSSIER_ID,TRI

                                                                                                XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                                            select * from cand_doc where cand_id=1643 and ope_id=8666 and DOSSIER_ID=9 and doc_id=1 and num_zone=4 order by DOSSIER_ID,TRI

                                                                                                XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		
		
		
		
		*/
		
		
	}
	?>
				</td>
			</tr>
		</table>
		</body>
	</html>
	<?php
}else{
	include('no_acces.php');
}
?>