<?php
session_start();

include ("../config/lib/connex.php");
include ("../config/lib/db.oracle.php");
$db = new db($conn);

header('Content-type: text/html; charset=UTF-8'); 

if ($_GET['cliid']){
	$query_codes	= "SELECT count(*) nb FROM PRODUIT,PRODUIT_A_DOC,DOC_A_INFO WHERE PRODUIT.PROD_ID = PRODUIT_A_DOC.PROD_ID 
	AND PRODUIT_A_DOC.DOC_ID = DOC_A_INFO.DOC_ID 
	AND (DOC_A_INFO.DOC_GRAPHE_MULTI=1 OR PRODUIT_A_DOC.EQUIPE=1) 
	AND PRODUIT.PROD_ID=".txt_db(intval($_GET['prodid']))."";
	$rows_codes		= $db->query($query_codes);
	$w = 0;
	if($rows_codes[0]['nb']>0){
		$w = 1;
	}

	if ($_GET['cliid']=='libre' && $_GET['certid']>0){
		// Quels sont les candidat qui appartiennent à ce client ?
		if ($w == 1){
		$sql_more_candidat = "SELECT * FROM (SELECT DISTINCT CANDIDAT.CAND_ID, CANDIDAT.CAND_EMAIL, CLIENT.CLI_NOM, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM, CANDIDAT.CAND_ACTIF, CANDIDAT.CAND_CP, CANDIDAT.CAND_VILLE 
		FROM CANDIDAT,CAND_A_OPE,OPERATION,CLIENT_A_CERT,CLIENT 
		WHERE CANDIDAT.CAND_CLI_ID=CLIENT_A_CERT.CLI_ID AND CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND CANDIDAT.CAND_ID=CAND_A_OPE.CAND_ID AND CAND_A_OPE.OPE_ID=OPERATION.OPE_ID AND CAND_A_OPE.DATE_FIN IS NOT NULL 
		AND prod_id in (select distinct prod_id from recup_quest where txt_img_quest_id in (select distinct txt_img_quest_id from recup_quest where prod_id=".txt_db(intval($_GET['prodid']))."))
		AND CANDIDAT.CAND_NOM LIKE upper('".txt_db($_GET['rechcand'])."%') AND CLIENT_A_CERT.CERT_ID=".txt_db(intval($_GET['certid']))." 
		UNION
		SELECT DISTINCT CANDIDAT.CAND_ID, CANDIDAT.CAND_EMAIL, 'Libre' CLI_NOM, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM, CANDIDAT.CAND_ACTIF, CANDIDAT.CAND_CP, CANDIDAT.CAND_VILLE 
		FROM CANDIDAT,CAND_A_OPE,OPERATION
		WHERE CANDIDAT.CAND_ID=CAND_A_OPE.CAND_ID AND CAND_A_OPE.OPE_ID=OPERATION.OPE_ID AND CAND_A_OPE.DATE_FIN IS NOT NULL 
		AND prod_id in (select distinct prod_id from recup_quest where txt_img_quest_id in (select distinct txt_img_quest_id from recup_quest where prod_id=".txt_db(intval($_GET['prodid']))."))
		AND CANDIDAT.CAND_CLI_ID=-1 AND CANDIDAT.CAND_NOM LIKE upper('".txt_db($_GET['rechcand'])."%') AND CANDIDAT.CAND_CERT_ID IN (select cert_id from certifie where cert_part_id in (select cert_part_id from certifie where cert_id=".txt_db(intval($_GET['certid']))."))
		) ORDER BY CAND_NOM,CAND_PRENOM";
		}
		else
		{
		$sql_more_candidat = "SELECT * FROM (SELECT DISTINCT CANDIDAT.CAND_ID, CANDIDAT.CAND_EMAIL, CLIENT.CLI_NOM, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM, CANDIDAT.CAND_ACTIF, CANDIDAT.CAND_CP, CANDIDAT.CAND_VILLE 
		FROM CANDIDAT,CLIENT_A_CERT,CLIENT  
		WHERE CANDIDAT.CAND_CLI_ID=CLIENT_A_CERT.CLI_ID AND CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND CANDIDAT.CAND_NOM LIKE upper('".txt_db($_GET['rechcand'])."%') AND CLIENT_A_CERT.CERT_ID=".txt_db(intval($_GET['certid']))."
		UNION
		SELECT DISTINCT CANDIDAT.CAND_ID, CANDIDAT.CAND_EMAIL, 'Libre' CLI_NOM, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM, CANDIDAT.CAND_ACTIF, CANDIDAT.CAND_CP, CANDIDAT.CAND_VILLE 
		FROM CANDIDAT 
		WHERE CANDIDAT.CAND_CLI_ID=-1 AND CANDIDAT.CAND_NOM LIKE upper('".txt_db($_GET['rechcand'])."%') AND CANDIDAT.CAND_CERT_ID IN (select cert_id from certifie where cert_part_id in (select cert_part_id from certifie where cert_id=".txt_db(intval($_GET['certid']))."))
		) ORDER BY CAND_NOM,CAND_PRENOM";
		}
		//echo $sql_more_candidat;
	}elseif($_GET['cliid']>0){
		// Quels sont les candidat qui appartiennent à ce client ?
		if ($w == 1){
		$sql_more_candidat = "SELECT DISTINCT CANDIDAT.CAND_ID, CANDIDAT.CAND_EMAIL, CLIENT.CLI_NOM, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM, CANDIDAT.CAND_ACTIF, CANDIDAT.CAND_CP, CANDIDAT.CAND_VILLE 
		FROM CANDIDAT,CAND_A_OPE,OPERATION,CLIENT  
		WHERE CANDIDAT.CAND_ID=CAND_A_OPE.CAND_ID AND CANDIDAT.CAND_CLI_ID=CLIENT.CLI_ID AND CAND_A_OPE.OPE_ID=OPERATION.OPE_ID 
		AND prod_id in (select distinct prod_id from recup_quest where txt_img_quest_id in (select distinct txt_img_quest_id from recup_quest where prod_id=".txt_db(intval($_GET['prodid']))."))
		AND CAND_A_OPE.DATE_FIN IS NOT NULL AND CANDIDAT.CAND_CLI_ID='".txt_db(intval($_GET['cliid']))."' ORDER BY CAND_NOM,CAND_PRENOM";
		}
		else
		{
		$sql_more_candidat = "SELECT DISTINCT CANDIDAT.CAND_ID, CANDIDAT.CAND_EMAIL, CLIENT.CLI_NOM, CANDIDAT.CAND_NOM, CANDIDAT.CAND_PRENOM, CANDIDAT.CAND_ACTIF, CANDIDAT.CAND_CP, CANDIDAT.CAND_VILLE 
		FROM CANDIDAT,CLIENT 
		WHERE CANDIDAT.CAND_CLI_ID=CLIENT.CLI_ID AND CANDIDAT.CAND_CLI_ID='".txt_db(intval($_GET['cliid']))."' ORDER BY CAND_NOM,CAND_PRENOM";
		}
		//echo $sql_more_candidat;
	}
	$qry_more_candidat = $db->query($sql_more_candidat);

	echo '<table align="right" width="98%" border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000">
	  <tr> 
		<td class="TX_bold">'.$t_nom.'</td>
		<td class="TX_bold" align="center">'.$t_select.'</td>
	  </tr>';
	  if (is_array($qry_more_candidat)){
		foreach($qry_more_candidat as $more_candidat){
		  echo '<tr bgcolor="F1F1F1"> 
			<td>
			<a href="./admvak_certifie_edit_Candidat.php?candid='.$more_candidat['cand_id'].'" target="_blank">'.$more_candidat['cand_nom'].' '.$more_candidat['cand_prenom'].'</a> - '. $more_candidat['cand_cp'] .' '. $more_candidat['cand_ville'] .' - '. $more_candidat['cli_nom'] .'</td>
			<td align="center">';
			  if($more_candidat['cand_actif']=='1'){
			   if($more_candidat['cand_email']!=''){
			    echo '<input type="checkbox" name="ajout_cand[]" value="'. $more_candidat['cand_id'] .'">';
				}else{
					if ($w == 1){
						echo '<input type="checkbox" name="ajout_cand[]" value="'. $more_candidat['cand_id'] .'">';
					}
					else
					{
						echo '<?php echo $t_candidat_sans_email ?>';
					}
				}
			  }else{
				echo '<?php echo $t_candidat_inactif ?>';
			  }
			echo '</td>
		  </tr>';
		}
	  }else{
		echo '<tr bgcolor="F1F1F1"><td colspan="2" align="center">'.$t_aucun_candidat_r.'</td></tr>';
	  }
	echo '</table>:';
}