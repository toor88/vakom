<?php
session_start();
	// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	/* Si le formulaire est posté */
	if ($_POST['PI_1']>0){
	
		/* On insert les données dans la base */
		for ($a=1; $a<5; $a++){
			$sql_up_PI = "UPDATE PA SET VAR = '".intval($_POST['PI_'.$a])."' WHERE NUM_COL = '".$a."'";
			$qry_up_PI = $db->query($sql_up_PI);
		}
		for ($b=1; $b<5; $b++){
			$sql_up_PA = "UPDATE PA SET VAR_2 = '".intval($_POST['PA_'.$b])."' WHERE NUM_COL = '".$b."'";
			$qry_up_PA = $db->query($sql_up_PA);
		}
		?>
		<script style="text/javascript">
			window.open('http://crystal.ornis.com/edition_cr/bin/vakom.asp', 'graphe','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=800');
		</script>
		<?php
	}
?>
<html>
<head>
<title>Vakom</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/nvo.css" type="text/css">
<link rel="stylesheet" href="../css/general.css" type="text/css">
<link rel="stylesheet" href="../css/style.css" type="text/css">	
<script language="JavaScript">
<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
	<?php
		include("menu_top_new.php");
	?>
<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>	
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="center" class="menu_Gris">&nbsp;</td>
  </tr>
  <tr> 
    <td align="right"> 
      <table width="961" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="180" valign="top">&nbsp;</td>
          <td align="left" valign="top"> 
            <table width="754" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1">
              <tr> 
                <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
              </tr>
              <tr> 
                <td width="14"></td>
                <td align="center" class="TX"> 
                  <form action="#" method="post" name="form">
					  <table border="0" cellspacing="0" cellpadding="0" align="center" width="500">
						<tr>
							<td class="TX" align="center" colspan="4"><b>Profil Imposé</b></td>
						</tr>
						<tr>
							<td class="TX" align="center">Point 1</td>
							<td class="TX" align="center">Point 2</td>
							<td class="TX" align="center">Point 3</td>
							<td class="TX" align="center">Point 4</td>
						</tr>
						<tr>
							<?php
							for ($c=1; $c<5; $c++){ // Boucle 4x
							?>
								<td class="TX" align="center">
									<select name="PI_<?php echo $c ?>" class="form-ediht">
									<?php
									
										$sql_sel_points_PI = "SELECT VAR FROM PA WHERE NUM_COL='".$c."'";
										$qry_sel_points_PI = $db->query($sql_sel_points_PI);
										for ($i=1; $i<28;$i++){
											unset($selected);
											if ($qry_sel_points_PI[0]['var']==$i){
												$selected=' selected="selected"';
											}
											echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
										}
									?>
									</select>
								</td>
							<?php
							}
							?>
						</tr>
						<tr> 
						  <td class="TX" align="center" colspan="4">
							&nbsp;
						  </td>
						</tr>
						<tr>
							<td class="TX" align="center" colspan="4"><b>Profil Accepté</b></td>
						</tr>
						<tr>
							<td class="TX" align="center">Point 1</td>
							<td class="TX" align="center">Point 2</td>
							<td class="TX" align="center">Point 3</td>
							<td class="TX" align="center">Point 4</td>
						</tr>
						<tr>
						<tr>
							<?php
							for ($d=1; $d<5; $d++){ // Boucle 4x
							?>
								<td class="TX" align="center">
									<select name="PA_<?php echo $d ?>" class="form-ediht">
									<?php
										$sql_sel_points_PA = "SELECT VAR_2 FROM PA WHERE NUM_COL='".$d."'";
										$qry_sel_points_PA = $db->query($sql_sel_points_PA);
										for ($j=1; $j<28;$j++){
											unset($selected);
											if ($qry_sel_points_PA[0]['var_2']==$j){
												$selected=' selected="selected"';
											}
											echo '<option value="'.$j.'"'.$selected.'>'.$j.'</option>';
										}
									?>
									</select>
								</td>
							<?php
							}
							?>
						</tr>
						</tr>
						<tr> 
						  <td class="TX" align="center" colspan="4">
							&nbsp;
						  </td>
						</tr>
						<tr> 
						  <td class="TX" align="center" colspan="4">
							<input type="submit" name="ok2" value="Générer le Graphe" class="bn_ajouter"></td>
						</tr>
					  </table>
				  </form>
                </td>
                <td width="14"></td>
              </tr>
              <tr> 
                <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
                <td height="14"></td>
                <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td width="180" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td align="right" width="180">&nbsp; </td>
  </tr>
</table>
		</p></div>	</article></div>	</div>	</div>	</div>					
</body>
</html>
<?php
}else{
	include('no_acces.php');
}
?>