<?php
session_start();
if($_SESSION['vak_id'] != 1){
	header('location:./index.php');
	exit;
}

$result = '';

if(!empty($_GET)){
	if(($_GET['old'])&&($_GET['new'])){
		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);
		$sqlArray = array(	'UPDATE cand_a_ope SET cand_id='.$_GET['new'].' WHERE cand_id='.$_GET['old'],
							'UPDATE cand_a_cr SET cand_id='.$_GET['new'].' WHERE cand_id='.$_GET['old'],
							'UPDATE cand_a_quest SET cand_id='.$_GET['new'].' WHERE cand_id='.$_GET['old'],
							'UPDATE certifie SET cert_cand_id='.$_GET['new'].' WHERE cert_cand_id='.$_GET['old'],
							'DELETE FROM candidat WHERE cand_id='.$_GET['old']);
		foreach($sqlArray as $request){
			$db->query($request);
		}
	}
}

?>
<html>
	<head>
	</head>
	<body>
		<div style="width: 400px;text-align: center;margin: auto;padding: 10px;border: 1px solid #e0e0e0;">
			<p>Basculer un candidat</p>
			<form action="" method="GET">
				<p>ID initial : <input type="text" size="30" name="old"/></p>
				<p>ID final : <input type="text" size="30" style="margin-left: 4px;" name="new"/></p>
				<p><input type="submit" value="Changer l'id"/></p>
			</form>
		</div>
		<?php if($result){ ?>
		<div style="width: 400px;text-align: center;margin: auto;padding: 0px 10px;border: 1px solid #e0e0e0;margin-top: 10px;">
			<p>Le changement a été effectué.</p>
		</div>
		<?php } ?>
	</body>
</html>