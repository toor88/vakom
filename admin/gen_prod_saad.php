<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']>1){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	$db = new db($conn);
	
	switch($_SESSION['droit']){
	// Certifié utilisateur
	case 2:
		$str_cert_id		= $_SESSION['cert_id'];
	break;		
	// Autres
	case ($_SESSION['droit']>2):
		$str_cert_id		= $_GET['certid'];
	break;	
}
		// On sélectionne les infos du certifié
		$sql_cert_info = "SELECT CERT_ID, CERT_NOM, CERT_PRENOM, PART_NOM, PART_ID, CERT_AUTRE_NOM_PART FROM CERTIFIE, PARTENAIRE WHERE CERT_PART_ID=PART_ID AND CERT_ID= '".txt_db(intval($str_cert_id))."'";
		$qry_cert_info = $db->query($sql_cert_info);
		
		// Chargement de la liste des produits
		$sql_prod_list 			= "SELECT distinct produit.prod_id,prod_nom
		FROM PRODUIT,PRODUIT_A_CERTIF,CERT_A_CERTIF";
		
		if($_SESSION['droit']<6){
			$sql_prod_list .= ", PRODUIT_A_PRIX";
		}
		
		$sql_prod_list .= " WHERE PRODUIT.PROD_ID = PRODUIT_A_CERTIF.PROD_ID AND PRODUIT_A_CERTIF.CODE_ID = CERT_A_CERTIF.CERTIF_CODE_ID
		AND CERT_A_CERTIF.CERTIF_CERT_ID='".txt_db($str_cert_id)."' 
		AND 
		(
		(CERT_A_CERTIF.CERTIF_CERTIFICATION Is Not Null And CERT_A_CERTIF.CERTIF_CERTIFICATION<=SYSDATE) OR (CERTIF_FORMATION=1)
		)
		AND (CERT_A_CERTIF.CERTIF_SUSPENDU=0 OR CERT_A_CERTIF.CERTIF_SUSPENDU IS NULL) 
		AND PRODUIT.PROD_ID IN (SELECT DISTINCT PROD_ID FROM RECUP_QUEST) ";
		if($_SESSION['droit']<6){
			$sql_prod_list 			.= "AND PRODUIT_A_PRIX.PROD_ID=PRODUIT.PROD_ID AND PRODUIT_A_PRIX.NATURE_CODE_ID<>46 ";
		}
		$sql_prod_list 			.= "ORDER BY PROD_NOM";
		// echo $sql_prod_list;
		$qry_prod_list 			= $db->query($sql_prod_list);
		
		// Ajout d'un candidat
		if (@is_array($_POST['ajout_cand'])){
			if (is_array($_SESSION['checked_cand'])){
				foreach($_POST['ajout_cand'] as $cand_ajout){
					if (!in_array($cand_ajout, $_SESSION['checked_cand'])) {
						array_push($_SESSION['checked_cand'], $cand_ajout);
					}				
				}
			}else{
				$_SESSION['checked_cand'] = $_POST['ajout_cand'];
			}
			header('location:gen_prod.php?certid='.$str_cert_id.'&prodid='.$_GET['prodid'].'&typeopeid='.$_GET['typeopeid'].'&opeid='.$_GET['opeid'].'&objet_mail='.stripslashes($_GET['objet_mail']).'&corps_mail='.stripslashes($_GET['corps_mail']).'&select='.$_GET['select'].'&idnc='.($_GET['idnc']+1));
		}elseif(@$_POST['hid_more_candidat'] == 1){
			header('location:gen_prod.php?certid='.$str_cert_id.'&prodid='.$_GET['prodid'].'&typeopeid='.$_GET['typeopeid'].'&opeid='.$_GET['opeid'].'&objet_mail='.stripslashes($_GET['objet_mail']).'&corps_mail='.stripslashes($_GET['corps_mail']).'&select='.$_GET['select'].'&idnc='.($_GET['idnc']+1));
		}
		
		// Suppression d'un candidat
		if (isset($_GET['del_cand']) && $_GET['del_cand']>=0){
			$del_cand = intval($_GET['del_cand']);
			unset($_SESSION['checked_cand'][$del_cand]);
			header('location:gen_prod.php?certid='.$str_cert_id.'&prodid='.$_GET['prodid'].'&typeopeid='.$_GET['typeopeid'].'&opeid='.$_GET['opeid'].'&objet_mail='.stripslashes($_GET['objet_mail']).'&corps_mail='.stripslashes($_GET['corps_mail']).'&select='.$_GET['select']);
		}

		if (@$_POST['crea_ope']){
			if (trim($_POST['txt_crea_ope']) != ''){
				/* On sélectionne le nouvel id à insérer */
				$sql_num_ope = "SELECT SEQ_ID.NEXTVAL NB FROM DUAL";
				$qry_num_ope = $db->query($sql_num_ope);
				$num_ope = $qry_num_ope[0]['nb'];
				
				/* On insère la nouvelle opération dans la base */
				$sql_insert_ope = "INSERT INTO TYPE_OPERATION VALUES ('".txt_db(intval($num_ope))."', '".txt_db(intval($str_cert_id))."', '".txt_db(intval($_POST['select_type_op']))."', '".txt_db($_POST['txt_crea_ope'])."')";
				$qry_insert_ope = $db->query($sql_insert_ope);
				
				/* On redirige le client */
				header('location:gen_prod.php?certid='.$str_cert_id.'&prodid='.$_GET['prodid'].'&typeopeid='.$_POST['select_type_op'].'&opeid='.$num_ope.'&objet_mail='.stripslashes($_GET['objet_mail']).'&corps_mail='.stripslashes($_GET['corps_mail']).'&select=1');
			}
		}
		
		// On a réussi à valider le formulaire d'affectation
		if (@$_POST['valid_final']){
			// On commence par récupérer l'id à insérer.
			$sql_new_id = "SELECT SEQ_ID.NEXTVAL NB FROM DUAL";
			$qry_new_id = $db->query($sql_new_id);
			$num = $qry_new_id[0]['nb'];
			
			// Si on a sélectionné une opération, On insère l'opération dans la table opération.
			$query_codes_tmp	= "SELECT count(*) nb FROM PRODUIT,PRODUIT_A_DOC,DOC_A_INFO WHERE PRODUIT.PROD_ID = PRODUIT_A_DOC.PROD_ID 
			AND PRODUIT_A_DOC.DOC_ID = DOC_A_INFO.DOC_ID 
			AND (DOC_A_INFO.DOC_GRAPHE_MULTI=1 OR PRODUIT_A_DOC.EQUIPE=1) 
			AND PRODUIT.PROD_ID='".intval($_POST['hid_prodid'])."'";
			$rows_codes_tmp		= $db->query($query_codes_tmp);
			if($rows_codes_tmp[0]['nb']>0){
				$type_ope_id = txt_db(intval($_POST['hid_opeid']));
			}
			else
			{
				if ($_POST['hid_select']=='1'){
					$type_ope_id = txt_db(intval($_POST['hid_opeid']));
				}else{
					$type_ope_id = '';
				}
			}
			/* On insère l'opération */
			$sql_insert_ope = "INSERT INTO OPERATION VALUES ('".$num."', '".txt_db(intval($_POST['hid_prodid']))."', '".$type_ope_id."', '".txt_db($_POST['hid_objet'])."', SYSDATE, '".$_SESSION['vak_id']."', '', '')";
			$qry_insert_ope = $db->query($sql_insert_ope);
			
			/* On sélectionne le nom du produit */
			$sql_sel_prod_nom = "SELECT PROD_NOM FROM PRODUIT WHERE PROD_ID='".intval($_POST['hid_prodid'])."'";
			$qry_sel_prod_nom = $db->query($sql_sel_prod_nom);
			
			/* On insère également les candidats de la liste */
			$w = 0;
			$Indic_Ligne_Tmp = 0;
			foreach($_SESSION['checked_cand'] as $candid){
				$Indic_Ligne_Tmp = $Indic_Ligne_Tmp+1;
				$sql_calcul_jet_cert = "select JETON_CORRESP_ID , JET_TARIF, affecte-utilise reste from v_jeton_synthese where 
				to_char(JET_DEB_VALIDITE,'YYYYMMDD')<=to_char(SYSDATE,'YYYYMMDD') and TO_char(JET_FIN_VALIDITE,'YYYYMMDD')>=to_char(SYSDATE,'YYYYMMDD')
				and jet_part_id=".txt_db(intval($qry_cert_info[0]['part_id']))." and JET_PROD_ID='".txt_db(intval($_POST['hid_prodid']))."'
				and CERT_ID in (-1,".txt_db(intval($str_cert_id)).") and affecte-utilise>0 order by code_ordre asc,CERT_ID desc,JET_DEB_VALIDITE,JET_FIN_VALIDITE";
				$qry_calcul_jet_cert = $db->query($sql_calcul_jet_cert);										
				//echo $sql_calcul_jet_cert;
				$query_codes_tmp	= "SELECT count(*) nb FROM PRODUIT,PRODUIT_A_DOC,DOC_A_INFO WHERE PRODUIT.PROD_ID = PRODUIT_A_DOC.PROD_ID 
				AND PRODUIT_A_DOC.DOC_ID = DOC_A_INFO.DOC_ID 
				AND (DOC_A_INFO.DOC_GRAPHE_MULTI=1 OR PRODUIT_A_DOC.EQUIPE=1) 
				AND PRODUIT.PROD_ID='".intval($_POST['hid_prodid'])."'";
				$rows_codes_tmp		= $db->query($query_codes_tmp);
				
				if($rows_codes_tmp[0]['nb']>0){
					$w = $w+1;
				}
				else
				{
					$w = 1;
				}
				if($w==1){
					$recup_JETON_CORRESP_ID = $qry_calcul_jet_cert[0]['jeton_corresp_id'];
					$recup_JETON_TARIF = $qry_calcul_jet_cert[0]['jet_tarif'];
					$sql_insert_jet 	= "UPDATE JETON_CORRESP_CERT SET UTILISE = UTILISE + 1 WHERE JETON_CORRESP_ID=".$qry_calcul_jet_cert[0]['jeton_corresp_id'];
					$qry_insert_jet		= $db->query($sql_insert_jet);	
				}
				//echo $sql_insert_jet;
				$sql_sel_info_candidat = "SELECT * FROM CANDIDAT WHERE CAND_ID='".txt_db(intval($candid))."' AND CAND_ACTIF='1'";
				$infos_candidat = $db->query($sql_sel_info_candidat);
				//echo $sql_sel_info_candidat;
				unset($mdp_random);
				$mdp_random = substr(md5(uniqid(rand(), true)),0,8);
				
				if($infos_candidat[0]['cand_actif'] == 1){
					$sql_sel_nom_quest	= "select distinct txt_img_quest_id from recup_quest where prod_id=".txt_db(intval($_POST['hid_prodid']))."";
					//echo $sql_sel_nom_quest;
					$qry_sel_nom_quest 	= $db->query($sql_sel_nom_quest);
					$entre_envoi_mail=0;
					foreach ($qry_sel_nom_quest as $sel_nom_quest){ // Tant que la requete renvoie un résultat
						$qestidtmp = $candid.$sel_nom_quest['txt_img_quest_id'];
						//echo $qestidtmp.'<br>';
						if ($_POST["chk_quest_$qestidtmp"] == '1'){
							$sql_infos_cert = "SELECT CERT_EMAIL, CERT_PRENOM, CERT_NOM, CERT_TEL, PART_NOM, PART_AD1, PART_AD2, PART_CP, PART_VILLE, PART_TEL, PART_FAX,CERT_AUTRE_NOM_PART FROM PARTENAIRE,CERTIFIE WHERE PARTENAIRE.PART_ID=CERTIFIE.CERT_PART_ID AND CERT_ID='".txt_db(intval($str_cert_id))."'";
							$qry_infos_cert	= $db->query($sql_infos_cert);
							$date = "''";
							//if ($infos_candidat[0]['cand_email']){
								if ($_POST['hid_corps']==''){
									$hid_corps_tmp='';
								}
								else{
									$hid_corps_tmp='<br>'.stripslashes($_POST['hid_corps']);
								}
								$mailto = $infos_candidat[0]['cand_email'];
								//$mailto = 'frederiquelys@free.fr, flys@vakom.fr, julien.joye@risc-group.com, alecauchois@vakom.fr, david.naze@deliberata.com, gilles.bensimon@risc-group.com';
								
								$sujet_mail = stripslashes($_POST['hid_objet']);
								
								$name_s    		 = $qry_infos_cert[0]['cert_prenom']." ".$qry_infos_cert[0]['cert_nom'];
								$email_s   		 = $qry_infos_cert[0]['cert_email'];
								//$email_s 		 = "Diffusion_Developpement-ITS@risc-group.biz";

								$boundary = "-----=" . md5( uniqid ( rand() ) );
//								$headers = "Reply-to: \"lesensdelhumain.com\" <questionnaire@lesensdelhumain.com>\n"; 
//								$headers .= "From: \"questionnaire@lesensdelhumain.com\"<questionnaire@lesensdelhumain.com>\n";
								$headers    	 = 'From: Questionnaire OPR <questionnaire@lesensdelhumain.com>'."\r\n";
								$headers        .= 'Cc: '. $email_s . "\r\n";							
								$headers        .= 'Bcc: fdegremont@vakom.fr,sgueguen@vakom.fr,mcousin@vakom.fr'."\r\n";							
								//$headers        .= 'Disposition-Notification-To: '. $email_s . "\r\n";		
//								$headers .= "MIME-Version: 1.0\n";
//								$headers .= "Content-Type: multipart/alternative; boundary=\"$boundary\"";
								$headers   		.= 'Content-Type: text/html; charset="ISO-8859-1"'."\r\n";
								$headers  		.= 'Content-Transfer-Encoding: 8bit'."\r\n";
	/*						
								$message_mail	 = "<html>

	<body>

	<p><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\">Bonjour </font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	font-weight:700\" color=\"#FF6600\">".ucfirst($infos_candidat[0]['cand_prenom'])." ".strtoupper($infos_candidat[0]['cand_nom'])."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\">,<br>
	<br>
	Merci de votre confiance. La démarche que vous allez entreprendre va vous être très enrichissante.<br>
	Nous souhaitons quelle vous apporte entière satisfaction.<br>
	<br>
	Pour remplir le questionnaire </font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	font-weight:700\" color=\"#FF6600\">".$qry_sel_prod_nom[0]['prod_nom']."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\"> sur notre site internet, merci de bien vouloir 
	cliquer sur le lien ci-dessous :<br>
	<br>
	</font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	text-decoration:underline\" color=\"#0000FF\"><a href=\"http://www.lesensdelhumain.com/public/passerelle.php?case=2&acces=".$mdp_random."\">Renseigner le questionnaire</a></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\"><br>
	<br>
	Si ce lien ne fonctionne pas, vous pouvez accéder au questionnaire sur le site 
	</font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	text-decoration:underline\" color=\"#0000FF\"><a href=\"http://www.vakom.fr\">www.vakom.fr</a></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\">, cliquer sur le bouton OPR EN LIGNE puis saisir votre code daccès : </font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	font-weight:700\" color=\"#FF6600\">N° ".$mdp_random."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\"><br>
	<br>
	Pour en savoir plus, cliquez sur le lien ci-dessous,<br>
	<br>
	</font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	text-decoration:underline\" color=\"#0000FF\"><a href=\"http://www.vakom.fr/page-methode-opr-vakom-11.html\">En savoir plus sur la méthode OPR</a></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\">
	<br>".$hid_corps_tmp."<br>Bien cordialement,<br>
	</font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	font-weight:700\" color=\"#FF6600\">".$qry_infos_cert[0]['cert_prenom']." ".$qry_infos_cert[0]['cert_nom']."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\"><br><br>
	</font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;color:#415564\">
	<img border=\"0\" src=\"http://www.extranet.lesensdelhumain.com/images/logo-miniopr.jpg\">
	<br><br><b>".$qry_infos_cert[0]['part_nom']."</b> <br>
	".$qry_infos_cert[0]['part_ad1']." ".$qry_infos_cert[0]['part_ad2']." <br>
	".$qry_infos_cert[0]['part_cp']." ".$qry_infos_cert[0]['part_ville']."<br>
	Tel : ".$qry_infos_cert[0]['part_tel']."<br>
	<br>
	</font>
	</p>
	</body>
	</html>";
	*/
	if ($qry_infos_cert[0]['cert_autre_nom_part'] != '')
		$nom_part_affiche = $qry_infos_cert[0]['cert_autre_nom_part'];
	else
		$nom_part_affiche = $qry_infos_cert[0]['part_nom'];
	
	$message_mail	 = "<html>\n";
	$message_mail	 .=	"<body>\n";
	$message_mail	 .=	"<p><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\">".$t_gen_prod_1." </font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	font-weight:700\" color=\"#FF6600\">".ucfirst($infos_candidat[0]['cand_prenom'])." ".strtoupper($infos_candidat[0]['cand_nom'])."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\">,<br>
	<br>
	".$t_gen_prod_2."<br>
	".stripslashes($t_gen_prod_3)."<br>
	<br>
	".$t_gen_prod_4." </font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	font-weight:700\" color=\"#FF6600\">".$qry_sel_prod_nom[0]['prod_nom']."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\"> ".$t_gen_prod_5." 
	".$t_gen_prod_6." :<br>
	".$t_gen_prod_7."
	<br>
	</font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	text-decoration:underline\" color=\"#0000FF\"><a href = \"http://www.lesensdelhumain.com/public/passerelle.php?case=2&acces=".$mdp_random."\">".$t_gen_prod_8."</a></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\"><br>
	<br>
	</font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;color:#415564\">
	".$t_gen_prod_12."<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;	font-weight:700\" color=\"#FF6600\">".$mdp_random."</font><br><br>".$t_gen_prod_9."<br>
	<br>
	</font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	text-decoration:underline\" color=\"#0000FF\"><a href = \"http://www.vakom.fr/page-methode-opr-vakom-11.html\">".$t_gen_prod_10."</a></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\">
	<br><b>".$hid_corps_tmp."</b><br><br>".$t_gen_prod_11."<br>
	</font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	font-weight:700\" color=\"#FF6600\">".$qry_infos_cert[0]['cert_prenom']." ".$qry_infos_cert[0]['cert_nom']."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
	color:#415564\"><br><br>
	</font>
	<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;color:#415564\">
	<img border=\"0\" src=\"http://www.extranet.lesensdelhumain.com/images/logo-miniopr.jpg\">
	<br><br><b>".$nom_part_affiche."</b> <br>
	".$qry_infos_cert[0]['part_ad1']." ".$qry_infos_cert[0]['part_ad2']." <br>
	".$qry_infos_cert[0]['part_cp']." ".$qry_infos_cert[0]['part_ville']."<br>
	Tel : ".$qry_infos_cert[0]['part_tel']."<br>
	<br>
	</font>
	</p>";
	$message_mail	 .=	"</body>\n";
	$message_mail	 .=	"</html>\n";
								if ($entre_envoi_mail == 0)
								{
									$entre_envoi_mail=1;
//									$message_email = "--" . $boundary . "\n";
//									$message_email .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
//									$message_email .= "Content-Transfer-Encoding: quoted-printable\n\n";
//									$message_email .= $message_mail;
//									$message_email .= "\n\n";
//									$message_email .= "--" . $boundary . "--\n";
									
									mail($mailto, $sujet_mail, $message_mail, $headers);
								}
							//}
						}else{
							$date = "SYSDATE";
						}
						if ($_POST["chk_quest_$qestidtmp"]==1) {
							$sql_insert_cand 		= "INSERT INTO CAND_A_OPE (CAND_ID,
												OPE_ID,
												QUEST_A_SAISIR,
												CODE_ACCES,
												DATE_QUEST,
												CERT_ID) VALUES ('".intval($candid)."', '".intval($num)."', '1', '".$mdp_random."', SYSDATE, '".txt_db(intval($str_cert_id))."')";
							$qry_insert_cand 		= $db->query($sql_insert_cand);
							$sql_insert_cand = "UPDATE CAND_A_OPE SET LISTE_QUEST_ID=concat(LISTE_QUEST_ID,'".$sel_nom_quest['txt_img_quest_id'].";') WHERE CAND_ID=".intval($candid)." and OPE_ID=".intval($num);
							$qry_insert_cand = $db->query($sql_insert_cand);
						}
						else
						{
							//echo $sel_nom_quest['txt_img_quest_id'].' =2<br>';
							// SÉLECTION DE LA DERNIERE OPÉRATION POUR LE CANDIDAT ET LE PRODUIT SÉLECTIONNÉS
//							$sql_sel_max_ope 	= "select max(ope_id) opeid from cand_ope where date_fin is not null and cand_id=".intval($candid)." and prod_id in 
//							(select distinct prod_id from recup_quest where txt_img_quest_id in (select distinct txt_img_quest_id from recup_quest where prod_id=".intval($_POST['hid_prodid'])."))";
							$sql_sel_max_ope 	= "select max(ope_id) opeid from cand_ope where date_fin is not null and ope_id <> ".intval($num) ." and cand_id=".intval($candid)." and prod_id in 
							(select distinct prod_id from recup_quest where txt_img_quest_id=".$sel_nom_quest['txt_img_quest_id'].")";
							//echo $sql_sel_max_ope.'<br>';
							$info_cand_max_ope 	= $db->query($sql_sel_max_ope);
							// SÉLECTION DES DATES DE SAISIE POUR LA DERNIERE OPERATION RECENSEE
//							$sql_sel_max_dateQ	= "select to_char(date_quest,'DD/MM/YYYY') date_quest, to_char(date_deb,'DD/MM/YYYY') date_deb, to_char(date_fin,'DD/MM/YYYY') date_fin from cand_a_ope where cand_id=".intval($candid)." and ope_id=".intval($info_cand_max_ope[0]['opeid'])."";
//							$qry_sel_max_dateQ 	= $db->query($sql_sel_max_dateQ);
							$sql_sel_max_dateQ	= "select to_char(date_quest,'DD/MM/YYYY') date_quest, to_char(date_deb,'DD/MM/YYYY') date_deb, to_char(date_fin,'DD/MM/YYYY') date_fin from cand_a_ope where date_fin is not null and cand_id=".intval($candid)." and ope_id=".intval($info_cand_max_ope[0]['opeid'])."";
							$qry_sel_max_dateQ 	= $db->query($sql_sel_max_dateQ);
							//echo $sql_sel_max_dateQ.'<br>';
							
							/*
							$sql_info_cand_max_ope 	= "select ope_id,to_char(max(cand_ope.date_creation),'DD/MM/YYYY') DO from cand_ope where cand_id='".intval($candid)."' group by ope_id";
							$info_cand_max_ope 		= $db->query($sql_info_cand_max_ope);
							*/
							
							$sql_insert_cand 		= "INSERT INTO CAND_A_OPE (CAND_ID,
												OPE_ID,
												QUEST_A_SAISIR,
												DATE_DEB,
												DATE_FIN,
												DATE_QUEST,
												CERT_ID) VALUES ('".intval($candid)."', '".intval($num)."', 0, to_date('".$qry_sel_max_dateQ[0]['date_deb']."','DD/MM/YYYY'), to_date('".$qry_sel_max_dateQ[0]['date_fin']."','DD/MM/YYYY'), to_date('".$qry_sel_max_dateQ[0]['date_quest']."','DD/MM/YYYY'),'".txt_db(intval($str_cert_id))."')";
							//echo $sql_insert_cand.'<br>';					
							$qry_insert_cand 		= $db->query($sql_insert_cand);
							
							$sql_insert_cand 		= "INSERT INTO CAND_A_QUEST (SELECT ".intval($candid).",".intval($num).",QUEST_ID,CHOIX_ID,REPONSE_PI,REPONSE_PN,TXT_LIBRE FROM CAND_A_QUEST
							WHERE CAND_ID=".intval($candid)." AND OPE_ID=".$info_cand_max_ope[0]['opeid']." AND QUEST_ID=".$sel_nom_quest['txt_img_quest_id'].")";
							//echo $sql_insert_cand.'<br>';					
							$qry_insert_cand 		= $db->query($sql_insert_cand);
						}
						$sql_insert_cand = "UPDATE CAND_A_OPE SET JETON_CORRESP_ID=".$recup_JETON_CORRESP_ID.",JETON_TARIF=".$recup_JETON_TARIF." WHERE JETON_CORRESP_ID IS NULL AND CAND_ID=".intval($candid)." and OPE_ID=".intval($num);
						$qry_insert_cand = $db->query($sql_insert_cand);
						
	//					$sql_quest_list = "select dossier_id from produit_a_dossier where prod_id=".txt_db(intval($_POST['hid_prodid']))."";
	//					$qry_quest_list = $db->query($sql_quest_list);
	//					foreach($qry_quest_list as $quest_list){
	//						system("(cd /var/www/html/vakom.fr/admin; /usr/bin/php /var/www/html/vakom.fr/admin/gen_doc_batch.php 0 ".intval($candid)." ".intval($num)." ".$quest_list['dossier_id']." & ) > /dev/null 2>&1");
	//					}
					}
				}
			}
			unset($_SESSION['checked_cand']);
			header('location:gen_prod.php?certid='.$str_cert_id);
		}
		
		if ($qry_cert_info[0]['part_id']>0 && @$_GET['prodid']>0 && $str_cert_id>0){
			$sql_jeton_partenaire ="select nvl(sum(affecte-utilise),0) reste from v_jeton_synthese where jet_part_id='".txt_db(intval($qry_cert_info[0]['part_id']))."'
			and jet_prod_id='".txt_db(intval($_GET['prodid']))."'
			and TO_char(JET_DEB_VALIDITE,'YYYYMMDD')<=to_char(SYSDATE,'YYYYMMDD') and TO_char(JET_FIN_VALIDITE,'YYYYMMDD')>=to_char(SYSDATE,'YYYYMMDD') 
			and cert_id in (-1,".txt_db(intval($str_cert_id)).")";	
//				echo $sql_jeton_partenaire;
			$qry_jeton_partenaire = $db->query($sql_jeton_partenaire);

			$jetons_dispo = $qry_jeton_partenaire[0]['reste'];			
		}
		
		if ($str_cert_id){
			// Requete pour récupérer la 1ere opération.
			$query_clients	 = "SELECT TYPE_OPERATION.TYPE_OPE_ID, TYPE_OPERATION.TYPE_OPE_LIBELLE FROM TYPE_OPERATION, CODE WHERE CODE.CODE_ID=TYPE_OPERATION.TYPE_OPE_CODE_ID AND CODE.CODE_TABLE='TYPE_OPERATION' AND CERT_ID='".txt_db(intval($str_cert_id))."'";
			$first_operation = $db->query($query_clients);
		}
		
		// Sélection des types d'opérations 
		$sql_type_ope	 = "SELECT CODE_LIBELLE, CODE_ID FROM CODE WHERE CODE.CODE_TABLE='TYPE_OPERATION' ORDER BY CODE_LIBELLE ASC";
		$qry_type_ope	 = $db->query($sql_type_ope);
		
		$opetype = '';
		
		if(isset($_GET['typeopeid']))
			$opetype = 'AND TYPE_OPERATION.TYPE_OPE_CODE_ID = '.$_GET['typeopeid'];

		// On sélectionne les opérations pour le certifié
		$sql_operations	 = "SELECT TYPE_OPERATION.TYPE_OPE_ID, TYPE_OPERATION.TYPE_OPE_LIBELLE FROM TYPE_OPERATION, CODE WHERE CODE.CODE_ID=TYPE_OPERATION.TYPE_OPE_CODE_ID ".$opetype." AND CODE.CODE_TABLE='TYPE_OPERATION' AND CERT_ID='".txt_db(intval($str_cert_id))."' ORDER BY TYPE_OPERATION.TYPE_OPE_LIBELLE ASC";
		$qry_operations	 = $db->query($sql_operations);
		
		// On sélectionne les opérations pour le partenaire et ainsi bloquer a création de doublon
		$sql_operations_part	 = "SELECT TYPE_OPERATION.TYPE_OPE_ID, TYPE_OPERATION.TYPE_OPE_LIBELLE FROM TYPE_OPERATION, CODE, CERTIFIE, PARTENAIRE WHERE CODE.CODE_ID=TYPE_OPERATION.TYPE_OPE_CODE_ID AND CODE.CODE_TABLE='TYPE_OPERATION' AND TYPE_OPERATION.CERT_ID=CERTIFIE.CERT_ID AND CERTIFIE.CERT_PART_ID=PARTENAIRE.PART_ID AND PART_ID IN (SELECT CERT_PART_ID FROM CERTIFIE WHERE CERT_ID=".$str_cert_id.") ORDER BY TYPE_OPERATION.TYPE_OPE_LIBELLE ASC";
		$qry_operations_part	 = $db->query($sql_operations_part);

		//var_dump($_SESSION['checked_cand']);
		
		
		
		if(@is_array($_SESSION['checked_cand'])){
			$str_candidats = 'VOUS AVEZ SELECTIONNE : ';
			/* On insère également les candidats de la liste */
			foreach($_SESSION['checked_cand'] as $candid){							
				//echo $sql_insert_jet;
				$sql_sel_info_candidat = "SELECT * FROM CANDIDAT WHERE CAND_ID=".intval($candid)." AND CAND_ACTIF='1'";
				$infos_candidat = $db->query($sql_sel_info_candidat);
				
				if ($infos_candidat[0]['cand_cli_id']>0){
					$sql_info_soc 	= "SELECT CLI_NOM FROM CLIENT WHERE CLI_ID='".txt_db(intval($infos_candidat[0]['cand_cli_id']))."' ORDER BY CLI_NOM";
					$info_soc 		= $db->query($sql_info_soc);
					$societe 		= $info_soc[0]['cli_nom'];
				}else{
					$societe		= 'Candidat&nbsp;libre';
				}
				$str_candidats .= '<span style="font-weight: normal;">'.$infos_candidat[0]['cand_prenom'].'</span>&nbsp;<b>'.$infos_candidat[0]['cand_nom'].'</b> <span style="font-weight: normal;">('.$societe.')</span>, ';
			}
			$str_candidats = substr($str_candidats, 0, (strlen($str_candidats)-2));
		}
	?>
	
	<html>
	<head>
	<title>Vakom Gestion de produits</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../css/nvo.css" type="text/css">
	<link rel="stylesheet" href="../css/general.css" type="text/css">
	<link rel="stylesheet" href="../css/style.css" type="text/css">	
	<script language="JavaScript">
	<!--
	
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}

	function change_action(check_ed){
		if (check_ed=='0'){
			//alert("ici");
			document.getElementById("step2-1").style.display='none';
		}
		if (check_ed=='1'){
			//alert("ici");
			document.getElementById("step2-1").style.display='block';
		}
		document.getElementById("step3").style.display='block';
		document.more_candidat.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
		document.form_crea_ope.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
		if(document.getElementById('sope_1')){
			if(document.getElementById('sope_1').checked==true || document.getElementById('sope_2').checked==true){
				if(document.getElementById("fin_de_form")){
					document.getElementById("fin_de_form").style.display='block';
				}
			}else{
				if(document.getElementById("fin_de_form")){
					document.getElementById("fin_de_form").style.display='hidden';
				}
			}
		}
	}
	
	function change_ope(opeid){
		document.getElementById("hid_opeid").value = opeid;
		document.more_candidat.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
		document.form_crea_ope.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
		document.form_crea_ope.submit();
	}
		
	
	function check_jetons(){
		<?php
		if(@$_GET['select']==1){
		
			?>
			//alert('test');
			document.getElementById('sope_1').checked=true;
			document.getElementById('sope_2').checked=false
			document.getElementById("step2").style.display='block';
			document.getElementById("step2-1").style.display='block';
			
			<?php
		}elseif((isset($_GET['select']))&&($_GET['select']==0)){
			?>
			//alert('test');
			document.getElementById('sope_1').checked=false;
			document.getElementById('sope_2').checked=true
			document.getElementById("step2").style.display='block';
			//alert("ici");
			document.getElementById("step2-1").style.display='none';
			<?php
		}
		?>
		if (document.getElementById('jetons_dispo').value>0){
		
			document.getElementById("step2").style.display='block';
			if(document.getElementById('sope_1')){
				if(document.getElementById('sope_1').checked==true || document.getElementById('sope_2').checked==true){
					if(document.getElementById('sope_1').checked==true){
						document.getElementById("step2-1").style.display='block';
					}
					if(document.getElementById("fin_de_form")){
						document.getElementById("fin_de_form").style.display='block';
					}
				}else{
					if(document.getElementById("fin_de_form")){
						document.getElementById("fin_de_form").style.display='none';
					}
				}
			}
			document.getElementById("div_jetons").innerHTML= '';
		}
/*		else
		{
			document.getElementById("div_jetons").innerHTML= 'Vous n\'avez pas de jeton disponible pour ce produit';
		}*/
	}
	var team = 0;
	function check_dispo(partid, prodid, type){
		if(type == 1)
			window.location = 'gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+prodid;
		document.getElementById("fin_de_form").style.display='none';

		document.getElementById('hid_prodid').value=prodid;
		document.more_candidat.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
		document.form_crea_ope.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
		
		if (prodid>0 && partid>0){
			document.getElementById("div_jetons").innerHTML= '<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" >';
			/* On lance la fonction ajax  qui va chercher les types de jetons disponibles pour le produit en question */					
			var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
			var filename = "ajax_jetons_dispo_all.php"; // La page qui réceptionne les données
			var data     = null; 
			
			var xhr_object = null; 
				 
			if(window.XMLHttpRequest) // Firefox 
			   xhr_object = new XMLHttpRequest(); 
			else if(window.ActiveXObject) // Internet Explorer 
			   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
			else { // XMLHttpRequest non supporté par le navigateur 
			   alert("<?php echo $t_browser_support_error_1 ?>");
			   return; 
			} 
			 
			
			 
			if(prodid != ""){
				data = "prodid="+prodid+"&partid="+partid+"&certid=<?php echo intval($str_cert_id)?>&typeopeid=<?php echo intval(@$_GET['typeopeid'])?>&opeid=<?php echo intval(@$_GET['opeid'])?>&objet_mail=<?php echo intval(@$_GET['objet_mail'])?>";
			}
			if(method == "GET" && data != null) {
			   filename += "?"+data;
			   data      = null;
			}
			 
			xhr_object.open(method, filename, true);

			xhr_object.onreadystatechange = function() {
			   if(xhr_object.readyState == 4) {
				  var tmp = xhr_object.responseText.split(":"); 
				  if(typeof(tmp[0]) != "undefined") {
					 if (tmp[0]!=''){
					 //alert(tmp[0]);
						if (tmp[0]>0){
					
					<?php

					if(@$_GET['select']==1){
						?>
						document.getElementById('sope_1').checked=true;
						document.getElementById('sope_2').checked=false
						document.getElementById("step2").style.display='block';
						document.getElementById("step2-1").style.display='block';
						
						<?php
					}elseif(@$_GET['select']==0){
						?>	
						document.getElementById('sope_1').checked=false;
						document.getElementById('sope_2').checked=true
						document.getElementById("step2").style.display='block';
						document.getElementById("step2-1").style.display='none';
						<?php
					}
					?>
							//document.getElementById("step2").style.display='block';
							if(document.getElementById('sope_1')){
								if(document.getElementById('sope_1').checked==true || document.getElementById('sope_2').checked==true){
									if(document.getElementById('sope_1').checked==true){
										document.getElementById("step2-1").style.display='block';
									}
									if(document.getElementById("fin_de_form")){
										document.getElementById("fin_de_form").style.display='block';
									}
								}else{
									if(document.getElementById("fin_de_form")){
										document.getElementById("fin_de_form").style.display='none';
									}
								}
							}
							document.getElementById("jetons_dispo").value = tmp[0];
							document.getElementById("div_jetons").innerHTML= '';
							if(type == 2)
								window.location = 'gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+prodid;
						}else{
							document.getElementById("div_jetons").innerHTML= "<?php echo $t_pas_de_jeton_dispo ?>";
							document.getElementById("step2").style.display='none';
							if(document.getElementById("fin_de_form")){
								document.getElementById("fin_de_form").style.display='none';
							}
						}
						
						if(typeof(tmp[1]) != "undefined"){
							if (tmp[1]!=''){
								if (tmp[1]>0){ // Profil d'équipe
									document.getElementsByName("select_ope")[0].checked='checked';
									document.getElementsByName("select_ope")[1].disabled=true;
									document.getElementById("step2-1").style.display='block';
									document.getElementById("step3").style.display='block';
									document.form_attribution_finale.hid_select.value =1 ;
									team = 1;
								}else{
									document.getElementsByName("select_ope")[1].disabled=false;
									document.getElementById("step2-1").style.display='none';
									team = 0;
									//document.getElementById("step2-1").style.display='block';
									//document.getElementById("step3").style.display='none';
								}
							}else{
								document.getElementsByName("select_ope")[1].disabled=false;
								<?php
								if(@$_GET['select']==1){
									?>
									document.getElementById("step2-1").style.display='block';
									<?php
								}
								?>
								//document.getElementById("step2-1").style.display='block';
								//document.getElementById("step3").style.display='none';
							}
						}
					 }
				  }
			   } 
			}
			xhr_object.send(data); //On envoie les données
/* 			if(type == 1)
				user_to_product(partid, prodid); */
		}else{
			document.getElementById("div_jetons").innerHTML='';
			document.getElementById("step2").style.display='none';
			document.getElementById("fin_de_form").style.display='none';
		}
		
	}
	
	function choisis_client(cliid){
		
			document.getElementById("div_more_candidat").innerHTML = '<?php echo $t_patientez ?>...<img src="../images/wait.gif" alt="" >';
			
			/* On lance la fonction ajax  qui va chercher les types de jetons disponibles pour le produit en question */					
			var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
			var filename = "ajax_client_a_candidat.php"; // La page qui réceptionne les données
			var data     = null; 
			
			var xhr_object = null; 
				 
			if(window.XMLHttpRequest) // Firefox 
			   xhr_object = new XMLHttpRequest(); 
			else if(window.ActiveXObject) // Internet Explorer 
			   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
			else { // XMLHttpRequest non supporté par le navigateur 
			   alert("<?php echo $t_browser_support_error_1 ?>");
			   return; 
			} 
			 
			if (document.getElementById('libre0').checked == true){
				if(cliid != ""){
					data = "cliid="+cliid+"&prodid="+document.getElementById('liste_prdt').options[document.getElementById('liste_prdt').selectedIndex].value;
				}
			}else{
				data = "cliid=libre&certid=<?php echo $str_cert_id ?>&prodid="+document.getElementById('liste_prdt').options[document.getElementById('liste_prdt').selectedIndex].value+"&rechcand="+document.getElementById('rechcand').value;
			}
			
			if(method == "GET" && data != null) {
			   filename += "?"+data;
			   data      = null;
			}
			 
			xhr_object.open(method, filename, true);

			xhr_object.onreadystatechange = function() {
			   if(xhr_object.readyState == 4) {
				  var tmp = xhr_object.responseText.split(":"); 
				  if(typeof(tmp[0]) != "undefined") {
					 if (tmp[0]!=''){
						document.getElementById("div_more_candidat").innerHTML = tmp[0];
					 }
				  }
			   } 
			}
			xhr_object.send(data); //On envoie les données
		
	}
	
	function charge_ope(typeopeid){
		if(typeopeid == 0){
			document.getElementById("hid_opeid").value = 0;
			document.getElementById('newOpe').disabled = "disabled";
			document.getElementById('verif_crea_ope_button').disabled = "disabled";
			document.getElementById('hid_typeopeid').value=typeopeid;
			document.getElementById("ope_liste").innerHTML='<?php echo $t_produit_ope_exist ?> : <select name="select_operation" disabled="disabled" id="select_operation" class="form_ediht_Tarifs"><option>Choisissez un type </option></select>';
			document.more_candidat.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
			document.form_crea_ope.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
			return;
		}
		
		document.getElementById('newOpe').disabled = "";
		document.getElementById('verif_crea_ope_button').disabled = "";
			
		var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
		var filename = "ajax_ope_liste.php"; // La page qui réceptionne les données
		var data     = null; 
		
		document.getElementById('hid_typeopeid').value=typeopeid;
		document.more_candidat.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
		document.form_crea_ope.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
						
		if 	(typeopeid.length>0){ //Si le code postal tapé possède au moins 2 caractères
			document.getElementById("ope_liste").innerHTML='<?php echo $t_patientez ?>...';
			var xhr_object = null; 
				 
				if(window.XMLHttpRequest) // Firefox 
				   xhr_object = new XMLHttpRequest(); 
				else if(window.ActiveXObject) // Internet Explorer 
				   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
				else { // XMLHttpRequest non supporté par le navigateur 
				   alert("<?php echo $t_browser_support_error_1 ?>");
				   return; 
				}
				 
				if(typeopeid != ""){
					data = "certid=<?php echo $str_cert_id?>&type_ope="+typeopeid;
				}
				if(method == "GET" && data != null) {
				   filename += "?"+data;
				   data      = null;
				}
				 
				xhr_object.open(method, filename, true);

				xhr_object.onreadystatechange = function() {
				   if(xhr_object.readyState == 4) {
					  var tmp = xhr_object.responseText.split(":"); 
					  if(typeof(tmp[0]) != "undefined") { 
						 document.getElementById("ope_liste").innerHTML = '';
						 if (tmp[0]!=''){
							document.getElementById("ope_liste").innerHTML = "<?php echo $t_produit_ope_exist ?> : "+tmp[0];
							if (tmp[1]!=''){
								document.getElementById("hid_opeid").value = tmp[1];
								document.more_candidat.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
								document.form_crea_ope.action='gen_prod.php?certid=<?php echo $str_cert_id ?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
							}
						 }else{
							document.getElementById("ope_liste").innerHTML="<?php echo $t_produit_ope_exist ?> : <select name=\"select_operation\" disabled=\"disabled\" id=\"select_operation\" class=\"form_ediht_Tarifs\"><option><?php echo $t_aucune_operation_cert ?> </option></select>";
							document.getElementById("hid_opeid").value = 0;
						 }
					  }
				   } 
				} 

				xhr_object.send(data); //On envoie les données
		}
	}

	
	function verif(nb_error){
		if(nb_error==0){
			error = '';
			
			if(team == 1){
				if(document.form_attribution_finale.jetons_dispo.value < 1)
					error += "<?php echo $t_pas_assez_jet ?>\n";
				if(document.form_attribution_finale.hid_select.value==1){
					if(document.form_attribution_finale.hid_opeid.value<1){
						error += "<?php echo $t_pas_sel_operation ?>\n";
					}
				}
			}
			else{
				var nbre_affect = <?php echo count(@$_SESSION['checked_cand']) ?>;
				if (parseInt(document.form_attribution_finale.jetons_dispo.value*10)<10 || nbre_affect > parseInt(document.form_attribution_finale.jetons_dispo.value)){
					error += "<?php echo $t_pas_assez_jet ?>\n";
				}
				if(document.form_attribution_finale.hid_select.value==1){
					if(document.form_attribution_finale.hid_opeid.value<1){
					error += "<?php echo $t_pas_sel_operation ?>\n";
					}
				}
			}
			if (error!=''){
				alert(error);
			}else{
				if (confirm("<?php echo $t_sur_de_gen_un_pdt ?>")){
					document.getElementById('fin_de_form').style.display='none';
					document.form_attribution_finale.submit();		
				}
			}
		}else{
			alert('<?php echo $t_cand_pas_quest ?>');
//			document.getElementById('fin_de_form').style.display='none';
//			document.form_attribution_finale.submit();		
		}
	}
	
	function supprime_candidat(candid){
		if (confirm("<?php echo $t_supp_cand_list ?>")){
			document.location.href='gen_prod.php?certid=<?php echo $str_cert_id?>&prodid='+document.getElementById('hid_prodid').value+'&typeopeid='+document.getElementById('hid_typeopeid').value+'&opeid='+document.getElementById('hid_opeid').value+'&del_cand='+candid+'&objet_mail='+document.getElementById('hid_objet').value+'&corps_mail='+document.getElementById('hid_corps').value+'&select='+document.getElementById('hid_select').value;
		}
	}
	
	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	
	function verif_crea_ope(type_ope_id){
		if(type_ope_id=='all'){
			alert('Vous devez choisir un type d\'opération.');
		}
		else if(document.getElementById('newOpe').value == ""){
			alert('Vous devez indiquer un libellé pour cette opération.');
		}else{
			if(document.getElementById('select_operation')){
			<?php

			if($qry_operations_part){
				$opeTableName = array();
				echo 'var opeNameArray = Array(';
				
				foreach($qry_operations_part as $existingOpeName){
						$opeTableName[] = '"'.htmlspecialchars($existingOpeName['type_ope_libelle']).'"';
						}
				$opeTableName = implode(",", $opeTableName);
				echo $opeTableName.');'."\r\n";
			?>
				for (var i = 0; i < opeNameArray.length; i++)
					if (opeNameArray[ i ] == document.getElementById('newOpe').value){
						alert('Une opération portant un nom identique existe déjà.');
						return;
					}
			<?php
			}
			?>
			}
			document.getElementById('fin_de_form').style.display='none';
			document.form_crea_ope.submit();
		}
	}
		//-->
	</script>
	</head>
	<body onLoad="check_jetons();<?php if(@$_GET['prodid']>0){ ?>check_dispo(<?php echo $qry_cert_info[0]['part_id'] ?>, <?php echo $_GET['prodid'] ?>);<?php } ?>" bgcolor="#FFFFFF" text="#000000">
		<?php
			include("menu_top_new.php");
		?>
		<br>
<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>			
		<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td width="20">&nbsp;</td>
		  <td class="Titre_Tarifs"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo $t_btn_generer_produit ?></td>
		</tr>
	   </table>
	  <table border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" width="961" align="center">
		<tr> 
		  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td bgcolor="#666666" height="1"></td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14"></td>
		  <td align="left" class="TX"> <br>
			<table border="0" cellspacing="0" cellpadding="2" >
			  <?php
			  if (@$str_candidats!='') echo ' <tr><td colspan="6" class="TX" style="text-align: left;">'.$str_candidats.'</td></tr><tr><td colspan="6" class="TX">&nbsp;</td></tr>';
			  ?>
			  <tr> 
				<td class="TX">1&nbsp;</td>
				<td class="TX_Gras" colspan="3" ><?php echo $t_produit_a_gen ?> 
				  :&nbsp;</td>
				<td class="TX" valign="top" align="left"> 
				  <select name="liste_prdt" class="form_ediht_Tarifs" id="liste_prdt" onchange="check_dispo(<?php echo $qry_cert_info[0]['part_id'] ?>, document.getElementById('liste_prdt').options[document.getElementById('liste_prdt').selectedIndex].value<?php if(@is_array($_SESSION['checked_cand'])) echo ',1';else echo ', 2'; ?>);">
				  <option value="0">---</option>
				  <?php
				 // print_r($_SESSION);
				  if (is_array($qry_prod_list)){
					foreach($qry_prod_list as $produit){
						unset($selected_produit);
						if (@$_GET['prodid'] == $produit['prod_id']){
							$selected_produit = ' selected="selected"';
						}
						echo '<option value="'.$produit['prod_id'].'"'.@$selected_produit.'>'.$produit['prod_nom'].'</option>';
					}
				  }
				  ?>
				  </select>
				  <div id="div_jetons" style="display: inline;"></div>
				</td>
			  </tr>
			  </table>
			  <br>
			  <div id="step2" style="display: none;">
			  <table border="0" cellspacing="0" cellpadding="2" width="100%">
			  <tr> 
				<td class="TX">2&nbsp;<?php echo $t_produit_ope_part ?> :&nbsp;
				  <input type="radio" id="sope_1" name="select_ope" onclick="document.getElementById('hid_select').value='1'; change_action(1);" value="1" <?php if (isset($_GET['select']) && $_GET['select']=='1'){ echo ' checked="checked"';}?>>
				  <?php echo $t_oui ?>&nbsp;&nbsp; 
				  <input type="radio" id="sope_2" name="select_ope" onclick="document.getElementById('hid_select').value='0'; change_action(0);" value="0" <?php if (isset($_GET['select']) && $_GET['select']=='0'){ echo ' checked="checked"';}?>>
				<?php echo $t_non ?></td>
				</tr>
				</table>
				<form action="#" method="post" name="form_crea_ope" id="form_crea_ope">
				<div id="step2-1" <?php if (@$_GET['select']!=1){echo 'style="display: none;"'; }?>>
				<table border="0" cellspacing="0" cellpadding="2" width="100%">
				<tr><td class="TX">
				
					<b><?php echo $t_type ?> : 				
				  <select name="select_type_op" id="select_type_op" class="form_ediht_Tarifs" onchange="charge_ope(document.getElementById('select_type_op').options[document.getElementById('select_type_op').selectedIndex].value);">
					<?php
					if (is_array($qry_type_ope)){
						echo '<option value="0">--------------</option>';
						foreach($qry_type_ope as $type_ope){
							unset($selected_type_ope);
							if (@$_GET['typeopeid'] == $type_ope['code_id']){
								$selected_type_ope = ' selected="selected"';
							}
							echo '<option value="'.$type_ope['code_id'].'"'.@$selected_type_ope.'>'.$type_ope['code_libelle'].'</option>';
						}
					?>
				  </select>&nbsp;&nbsp;&nbsp;&nbsp;
					  <div style="display: inline;" id="ope_liste" width="200">
						<?php echo $t_produit_ope_exist ?> : 
						<?php				
						//if (is_array($qry_operations)){
						if((!isset($_GET['opeid']))||($_GET['typeopeid']==0)){
							echo '<select name="select_operation" disabled="disabled" id="select_operation" class="form_ediht_Tarifs">';
							echo '<option>Choisissez un type </option>';
							echo '</select>';
						}
						else{
							echo '<select name="select_operation" id="select_operation" class="form_ediht_Tarifs" onchange="change_ope(document.getElementById(\'select_operation\').options[document.getElementById(\'select_operation\').selectedIndex].value);">';
 							foreach ($qry_operations as $operations){ // Tant que la requete renvoie un résultat
								unset($selected_opeid);
								if ($_GET['opeid'] == $operations['type_ope_id']){
									$selected_opeid = ' selected="selected"';
								}								
								echo '<option value="'. $operations['type_ope_id'].'"'.$selected_opeid.'>'. $operations['type_ope_libelle'] .'</option>';
							}
							echo '</select>';								
						}
						/*else
						{
							echo '<select name="select_operation" id="select_operation" class="form_ediht_Tarifs">';
							echo '</select>';								
						} */
						?>
					  </div>
				  <?php
				  }else{
					echo $t_aucune_operation_cert;
				  }
				  ?>
				  </b>
				  </td>
			  </tr>
					  <tr> 
						<td class="TX" height="30">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $t_produit_ope_nouvel ?> &nbsp;&nbsp;&nbsp;&nbsp;
						  :&nbsp;
							<input type="text" id="newOpe" <?php if((!isset($_GET['opeid']))||($_GET['typeopeid']==0)) echo 'disabled="disabled"' ?> name="txt_crea_ope" size="30" class="form_ediht_Tarifs" maxlength="100">
						  &nbsp; 
						  <input type="hidden" name="crea_ope" value="1">
						  <input type="button" id="verif_crea_ope_button" <?php if((!isset($_GET['opeid']))||($_GET['typeopeid']==0)) echo 'disabled="disabled"' ?> onclick="verif_crea_ope(document.getElementById('select_type_op').options[document.getElementById('select_type_op').selectedIndex].value);" value="<?php echo $t_btn_creer ?>" class="bn_ajouter">
						</td>
					  </tr>
				</form>
			</table>
			</div>
			<div id="step3" <?php if (@$_GET['select']!=1 && @$_GET['select']!=0){echo 'style="display: none;"'; }?>>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			  <tr> 
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top">&nbsp;</td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top">3&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top">
				<?php
				if (@!is_array($_SESSION['checked_cand'])){
					echo $t_produit_cand_a_sel;
				}else{
					echo $t_produit_cand_a_sel2;
				}
				?>
				</td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top"> 
				  <input type="radio" id="libre1" name="libres" value="1">
				  <?php echo $t_cand_libres ?>&nbsp;<input type="text" onclick="document.getElementById('libre1').checked=true;" id="rechcand" name="rechcand" size="20" class="form_ediht_Candidats"></td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top"> 
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr> 
					  <td class="TX" valign="top" width="50%"><input type="radio" id="libre0" name="libres" value="0" checked="checked">
					  <?php echo $t_cand_soc ?> : <b><br> 
						<?php
						$sql_list_clients = "SELECT DISTINCT CLIENT.CLI_ID, concat(concat(CLIENT.CLI_NOM,' - '),CLI_VILLE) CLI_NOM FROM CLIENT, CLIENT_A_CERT WHERE CLIENT_A_CERT.CLI_ID=CLIENT.CLI_ID AND (CLIENT.CLI_ACTIF IS NULL OR CLIENT.CLI_ACTIF=1) AND CLIENT_A_CERT.CERT_ID='".txt_db(intval($str_cert_id))."' ORDER BY CLI_NOM";
						$qry_list_clients = $db->query($sql_list_clients);
						?>
						<select id="liste_clients" onclick="document.getElementById('libre0').checked=true;" name="liste_clients" class="form_ediht_Tarifs">
						<?php
						if (is_array($qry_list_clients)){
							$entre_cli_tmp = 1;
							foreach($qry_list_clients as $list_clients){
								echo '<option value="'.$list_clients['cli_id'].'">'.$list_clients['cli_nom'].'</option>';
							}
						}
						else
						{
							$entre_cli_tmp = 0;
						}
						  ?>
						</select>
						<br><br>
						<?php
						if ($entre_cli_tmp == 1){
						?>
						<input type="button" name="choix_client" value="<?php echo $t_btn_rechercher ?>" class="bn_ajouter" onclick="choisis_client(document.getElementById('liste_clients').options[document.getElementById('liste_clients').selectedIndex].value)">
						<?php
						}
						else
						{
						?>
						<input type="button" name="choix_client" value="<?php echo $t_btn_rechercher ?>" class="bn_ajouter" onclick="choisis_client()">
						<?php
						}
						?>
						</b>&nbsp;<input class="bn_ajouter" type="button" value="<?php echo $t_btn_creer_candidat ?>" onClick="document.more_candidat.submit(); MM_openBrWindow('admvak_certifie_nvoCandidat.php?certid=<?php echo $str_cert_id ?>&origine=gen_prod','crea_cand','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')"><br>
					  </td>
					  <td class="TX" valign="top"> 
						<form action="#" method="post" name="more_candidat">
						<input type="hidden" name="hid_more_candidat" value="1">
						<div style="text-align: right;" id="div_more_candidat">
						</div>
					  </td>
					</tr>
					<tr> 
					  <td class="TX" valign="top">&nbsp;</td>
					  <td class="TX" valign="top">&nbsp;</td>
					</tr>
					<tr> 
					  <td class="TX" valign="top">&nbsp;</td>
					  <td class="TX" align="right"> 
						<input type="submit" name="bn_ajoutCL" value="<?php echo $t_btn_ajouter ?> " class="bn_ajouter"></td>
					</tr>
				  </form>
				  </table>
				</td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top">&nbsp;</td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top">4&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top"><?php echo $t_produit_recap_cand ?></td>
			  </tr>
			  <tr>
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top">
				  <table width="100%" border="0" cellspacing="1" cellpadding="2" class="TX" bgcolor="#000000">
					<tr> 
					  <td align="center" class="TX_bold"><?php echo $t_produit_quest_a_sais ?></td>
					  <td class="TX_bold"><?php echo $t_nom.' '.$t_prenom.' - '.$t_societe ?></td>
					  <td class="TX_bold"><?php echo $t_produit_quest_type ?></td>
					  <td class="TX_bold"><?php echo $t_date_last_quest ?></td>
					  <td align="center" class="TX_bold"><?php echo $t_supprimer ?></td>
					</tr>
					<form method="post" action="#" name="form_attribution_finale">
					<?php	
/*					if(($_GET['opeid'])&&($_GET['prodid'])){
						$get_old_cand = "SELECT CAND_ID, MAX(CLI_NOM) CLI_NOM, MAX(CLI_ID) CLI_ID, MAX(CLI_VILLE) CLI_VILLE, MAX(CAND_NOM) CAND_NOM,
						MAX(CAND_PRENOM) CAND_PRENOM, MAX(CAND_SEXE) CAND_SEXE, MAX(CAND_DNS) CAND_DNS, MAX(CAND_FONCTION) CAND_FONCTION, MAX(CAND_ACTIF) CAND_ACTIF ,
						MAX(AGE) AGE, MAX(DATE_DEB) DATE_DEB, MAX(LAST_OPR) LAST_OPR, MAX(DATE_FIN) DATE_FIN, MAX(CAND_OPE.OPE_ID) OPE_ID FROM CAND_OPE,
						OPERATION WHERE CAND_OPE.OPE_ID=OPERATION.OPE_ID AND OPERATION.TYPE_OPE_ID='".$_GET['opeid']."' AND CERT_ID = '".$_GET['certid']."' 
						AND CAND_ACTIF='1' AND operation.prod_id = '".$_GET['prodid']."'
						GROUP BY CAND_ID ORDER BY CLI_NOM, CAND_NOM, CAND_PRENOM ";
						//echo $get_old_cand;
						$get_old_cand = $db->query($get_old_cand);
						if (@is_array($get_old_cand)){
							foreach($get_old_cand as $cle => $checked_candidat){
								if (is_array($_SESSION['checked_cand'])){
									if (!in_array($checked_candidat['cand_id'], $_SESSION['checked_cand'])) {
										array_push($_SESSION['checked_cand'], $checked_candidat['cand_id']);
									}				
								}else{
									$_SESSION['checked_cand'] = $checked_candidat['cand_id'];
								}
							}
						}
					}
					*/
					$query_codes	= "SELECT count(*) nb FROM PRODUIT,PRODUIT_A_DOC,DOC_A_INFO WHERE PRODUIT.PROD_ID = PRODUIT_A_DOC.PROD_ID 
					AND PRODUIT_A_DOC.DOC_ID = DOC_A_INFO.DOC_ID 
					AND (DOC_A_INFO.DOC_GRAPHE_MULTI=1 OR PRODUIT_A_DOC.EQUIPE=1) 
					AND PRODUIT.PROD_ID=".txt_db(@intval($_GET['prodid']))."";
					//echo $query_codes;
					$rows_codes		= $db->query($query_codes);

					if (@is_array($_SESSION['checked_cand'])){
						$Indic_Ligne_Tmp=0;
						foreach($_SESSION['checked_cand'] as $cle => $checked_candidat){
							$Indic_Ligne_Tmp = $Indic_Ligne_Tmp+1;
							$sql_info_cand = "SELECT * FROM CANDIDAT WHERE CAND_ID='".txt_db(intval($checked_candidat))."'";
							$info_cand = $db->query($sql_info_cand);
							/*
							$sql_info_cand_max_ope = "select to_char(max(cand_ope.date_creation),'DD/MM/YYYY') DO from cand_ope where cand_id='".txt_db(intval($checked_candidat))."'";
							$info_cand_max_ope = $db->query($sql_info_cand_max_ope);
							*/
							// SÉLECTION DE LA DERNIERE OPÉRATION POUR LE CANDIDAT ET LE PRODUIT SÉLECTIONNÉS
//							$sql_sel_max_ope 	= "select max(ope_id) opeid from cand_ope where date_fin is not null and cand_id=".intval($checked_candidat)." and prod_id in 
//							(select distinct prod_id from recup_quest where txt_img_quest_id in (select distinct txt_img_quest_id from recup_quest where prod_id=".intval($_GET['prodid'])."))";
//							$info_cand_max_ope 	= $db->query($sql_sel_max_ope);
							//echo $sql_sel_max_ope;
							//echo 'GBE'.intval($_GET['prodid']).'GBE';
							// SÉLECTION DES DATES DE SAISIE POUR LA DERNIERE OPERATION RECENSEE
//							$sql_sel_max_dateQ	= "select to_char(date_quest,'DD/MM/YYYY') date_quest from cand_a_ope where date_fin is not null and cand_id=".intval($checked_candidat)." and ope_id=".intval($info_cand_max_ope[0]['opeid'])."";
//							$qry_sel_max_dateQ 	= $db->query($sql_sel_max_dateQ);
							
							// NOM DU QUESTIONNAIRE
							$sql_sel_nom_quest_cpt	= "select count(distinct txt_img_quest_id) cpt from recup_quest	where prod_id=".txt_db(intval($_GET['prodid']))."";
							$qry_sel_nom_quest_cpt 	= $db->query($sql_sel_nom_quest_cpt);
							$sql_sel_nom_quest	= "select distinct txt_img_quest_id,quest_nom from recup_quest,questionnaire
							where recup_quest.txt_img_quest_id=questionnaire.quest_id
							and recup_quest.prod_id=".txt_db(intval($_GET['prodid']))."";
							//echo $sql_sel_nom_quest;
							$qry_sel_nom_quest 	= $db->query($sql_sel_nom_quest);
							foreach ($qry_sel_nom_quest as $sel_nom_quest){ // Tant que la requete renvoie un résultat
								// SÉLECTION DE LA DERNIERE OPÉRATION POUR LE CANDIDAT ET LE PRODUIT SÉLECTIONNÉS
								$sql_sel_max_ope 	= "select max(ope_id) opeid from cand_ope where date_fin is not null and cand_id=".intval($checked_candidat)." and prod_id in 
								(select distinct prod_id from recup_quest where txt_img_quest_id=".$sel_nom_quest['txt_img_quest_id'].")";
								$info_cand_max_ope 	= $db->query($sql_sel_max_ope);
								//echo $sql_sel_max_ope;
								//echo 'GBE'.intval($_GET['prodid']).'GBE';
								// SÉLECTION DES DATES DE SAISIE POUR LA DERNIERE OPERATION RECENSEE
								$sql_sel_max_dateQ	= "select to_char(date_quest,'DD/MM/YYYY') date_quest from cand_a_ope where date_fin is not null and cand_id=".intval($checked_candidat)." and ope_id=".intval($info_cand_max_ope[0]['opeid'])."";
								$qry_sel_max_dateQ 	= $db->query($sql_sel_max_dateQ);
							?>
							<tr> 
							  <td align="center" bgcolor="F1F1F1">
							  <?php
							  if ($info_cand[0]['cand_actif']==1){
								unset($checked);
								$checked = ' checked="checked"';
								if ($qry_sel_max_dateQ[0]['date_quest']==''){
									$checked = ' checked="checked" onclick="return false;"';
								}
								if ($rows_codes[0]['nb']>0){
									if ($qry_sel_max_dateQ[0]['date_quest']==''){
										$var_error++;
									}								
									$checked = ' onclick="return false;"';
								}							  
								if ($qry_sel_nom_quest_cpt[0]['cpt']>1){ // MULTI QUEST
									if ($sel_nom_quest['txt_img_quest_id'] == 241){ // OPR 48
										if ($qry_sel_max_dateQ[0]['date_quest']==''){
											$var_error++;
										}
										$checked = ' onclick="return false;"';
									}
								}							  
							  ?>
								<input type="checkbox" name="chk_quest_<?php echo$info_cand[0]['cand_id'].$sel_nom_quest['txt_img_quest_id']?>" value="1" <?php echo $checked ?>>
							  <?php
							  }
							  ?>
							  </td>
							  <td bgcolor="F1F1F1">
								<?php 
									echo '<a onclick="document.getElementById(\'hid_corps\').value=document.getElementById(\'txt_corps\').value; change_action();" href="./admvak_certifie_edit_Candidat.php?candid='.$info_cand[0]['cand_id'].'" target="_blank">'.$info_cand[0]['cand_nom'].' '.$info_cand[0]['cand_prenom'].'</a> - ';
									if ($info_cand[0]['cand_cli_id']>0){
										$sql_info_soc 	= "SELECT CLI_NOM FROM CLIENT WHERE CLI_ID='".txt_db(intval($info_cand[0]['cand_cli_id']))."'";
										$info_soc 		= $db->query($sql_info_soc);
										$societe 		= $info_soc[0]['cli_nom'];
									}
									else
										$societe 		= 'Libre';
									echo $societe; 
								  ?>
							  </td>
							  <td bgcolor="F1F1F1"><?php echo $sel_nom_quest['quest_nom'] ?></td>
							  <td bgcolor="F1F1F1"><?php echo$qry_sel_max_dateQ[0]['date_quest']?></td>
							  <td align="center" bgcolor="F1F1F1"><img onclick="supprime_candidat(<?php echo $cle ?>)" onmouseover="this.style.cursor='pointer'" src="../images/icon_supp2.gif" width="11" height="12"></td>
							</tr>
							<?php
							}
							?>
							<?php
						}
					}else{
						echo '<tr><td align="center" colspan="5" bgcolor="F1F1F1">'.$t_produit_ajout_cand.'</td></tr>';
					}
					?>
				  </table>
				</td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top">&nbsp;</td>
			  </tr>
				<?php	
					$query_codes	= "SELECT count(*) nb FROM PRODUIT,PRODUIT_A_DOC,DOC_A_INFO WHERE PRODUIT.PROD_ID = PRODUIT_A_DOC.PROD_ID 
					AND PRODUIT_A_DOC.DOC_ID = DOC_A_INFO.DOC_ID 
					AND (DOC_A_INFO.DOC_GRAPHE_MULTI=1 OR PRODUIT_A_DOC.EQUIPE=1) 
					AND PRODUIT.PROD_ID=".txt_db(@intval($_GET['prodid']))."";
					//echo $query_codes;
					$rows_codes		= $db->query($query_codes);
					if ($rows_codes[0]['nb']==0){
				?>
			  <tr> 
				<td class="TX" valign="top">5&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top"><?php echo $t_produit_objet_mail ?>&nbsp;: </td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top"><?php echo $t_produit_mess_perso_1 ?>&nbsp; </td>
			  </tr>			  
			  <tr> 
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX" colspan="3" valign="top"><b> 
				  <input type="text" name="objet" size="150" class="form_ediht_Tarifs" value="<?php if (@$_GET['objet_mail']!=''){ echo stripslashes(htmlentities($_GET['objet_mail']));} else { echo 'Invitation à remplir votre questionnaire';} ?>" onblur="document.getElementById('hid_objet').value=this.value;change_action(); " onkeypress="document.getElementById('hid_objet').value=this.value;change_action(); " maxlength="140">
				  </b></td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX_Gras" colspan="3" valign="top"><?php echo $t_produit_mess_perso ?>&nbsp; </td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX" colspan="3" valign="top">
				<!--<input type="text" id="txt_corps" name="corps" size="150" class="form_ediht_Tarifs" onblur="document.getElementById('hid_corps').value=this.value; change_action();" onkeypress="document.getElementById('hid_corps').value=this.value;change_action(); " maxlength="140" value="<?php //if ($_GET['corps_mail']!=''){ echo stripslashes(htmlentities($_GET['corps_mail']));} else { echo '';} ?>"></td>-->
				<textarea id="txt_corps" name="corps" COLS="107" class="form_ediht_Tarifs" style="height: 60px;" onblur="document.getElementById('hid_corps').value=this.value; change_action();" onkeypress="document.getElementById('hid_corps').value=this.value;change_action(); " maxlength="512" value="<?php if (@$_GET['corps_mail']!=''){ echo stripslashes(htmlentities($_GET['corps_mail']));} else { echo '';} ?>"></textarea></td>
			  </tr>
			  <tr> 
				<td class="TX" valign="top">&nbsp;</td>
				<td class="TX" colspan="3" valign="top">&nbsp;</td>
			  </tr>
			  <?php
			  }
			  ?>
			</table>
			
		  </td>
		  <td width="14"></td>
		</tr>
		<tr> 
		  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
		  <td height="14"></td>
		  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
		</tr>
	  </table>
		</p></div>	</article></div>	</div>	</div>	</div>						  
	  </div>
	  <br>
	  <table border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td align="center">
		  	<div style="display: none;" id="fin_de_form">
				<input type="hidden" id="hid_select" name="hid_select" value="<?php if (@$_GET['select']!=''){ echo stripslashes(htmlentities($_GET['select']));} else { echo '0';} ?>">
				<input type="hidden" id="hid_objet" name="hid_objet" value="<?php if (@$_GET['objet_mail']!=''){ echo stripslashes(htmlentities($_GET['objet_mail']));} else { echo 'Invitation à remplir votre questionnaire';} ?>">
				<input type="hidden" id="hid_opeid" name="hid_opeid" value="<?php if (@$_GET['opeid']) { echo intval($_GET['opeid']); }else{ echo 0 ;}//$first_operation[0]['type_ope_id'] ; } ?>">
				<input type="hidden" id="hid_corps" name="hid_corps" value="<?php if (@$_GET['corps_mail']) { echo stripslashes(htmlentities($_GET['corps_mail'])); } else { echo '';} ?>">
		  		<input type="hidden" id="hid_typeopeid" name="hid_typeopeid" value="<?php echo intval(@$_GET['typeopeid']) ?>">
				<input type="hidden" id="hid_prodid" name="hid_prodid" value="<?php echo intval(@$_GET['prodid']) ?>">
				<input type="hidden" name="jetons_dispo" id="jetons_dispo" value="<?php echo intval(@$jetons_dispo) ?>">
				<input type="hidden" name="valid_final" value="1">
				<?php
				if(@count($_SESSION['checked_cand'])>0){
					echo '<input type="button" name="submit_final" value="'.$t_btn_generer.'" class="BN" onclick="verif('.intval(@$var_error).');">';
					
				}
				?>
			</form>
		  </div>
		  </td>
		</tr>
	  </table>
		</p></div>	</article></div>	</div>	</div>	</div>					
	</body>
	</html>
<?php
}else{
	include('no_acces.php');
}
?>