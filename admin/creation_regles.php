<?php
session_start();
// Si l'utilisateur est un super admin
if ($_SESSION['droit']=='9'){
		
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");
	$db = new db($conn);
	
	if ($_GET['questid'] > 0){
		/* On fait la requete pour trouver le questionnaire voulu */
		$sql_quest = "SELECT QUEST_ID, QUEST_NOM FROM QUESTIONNAIRE WHERE QUEST_ID='".txt_db($_GET['questid'])."'";
		$qry_quest = $db->query($sql_quest);
		
		/* Sélection des différents profils */
		$sql_list_type_profil = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='TYPE_PROFIL' ORDER BY CODE_LIBELLE ASC";
		$qry_list_type_profil = $db->query($sql_list_type_profil);
	
		if ($_POST['segments']>=0  && $_POST['blocs']>=0  && $_POST['points_sup'] >= $_POST['points_inf'] && $_POST['select_profil']>0){
			$sql_insert_cor_regle = "INSERT INTO QUEST_A_CORRESP_REGLE VALUES(SEQ_ID.NEXTVAL, '".txt_db($_GET['questid'])."', '".txt_db($_POST['select_profil'])."', ".$_POST['points_inf'].", ".$_POST['points_sup'].", '".txt_db(intval($_POST['segments']))."', '".txt_db(intval($_POST['blocs']))."', '".$_SESSION['vak_id']."', SYSDATE, '','','','')";
			//echo $sql_insert_cor_regle;
			$qry_insert_cor_regle = $db->query($sql_insert_cor_regle);
			
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				window.close();
			</script>
			<?php
		}
	}
		?>
	<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<script language="JavaScript">
		<!--
		function verif(){
			error = '';
			
			if (document.form.points_sup.value.length<1 || document.form.points_inf.value.length<1){
				error += '<?php echo $t_valeur_points_obg ?>\n';
			}
			if ((document.form.points_sup.value)*10 < (document.form.points_inf.value)*10){
				error += '<?php echo $t_balise_egal_error ?>\n';
			}
			if (document.form.segments.value.length<1){
				error += '<?php echo $t_segments_obg ?>\n';
			}
			if (document.form.blocs.value.length<1){
				error += '<?php echo $t_blocks_obg ?>\n';
			}
			
			if (error !=''){
				alert(error);
			}else{
				document.form.submit();
			}
		}
					
		function MM_goToURL() { //v3.0
		  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		//-->
		</script>
		</head>

		<body bgcolor="#FFFFFF" text="#000000">
		<form method="post" action="#" name="form">
			<table width="830" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;CORRESPONDANCES&nbsp;>&nbsp;<?php echo ucfirst($qry_quest[0]['quest_nom']) ?></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><table border="0" cellspacing="0" cellpadding="0"  width="100%">
				  <tr> 
					<td height="14"></td>
					<td height="14"></td>
					<td height="14"></td>
					<td height="14"></td>
					<td height="14"></td>
				  </tr>
			<tr> 
			  <td class="TX_Tarifs" colspan="5">Ajout&nbsp;d'une&nbsp;correspondance&nbsp;points/segements/blocs&nbsp;</td>
			  </tr>
			<tr> 
			  <td bgcolor="#666666" height="1"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td bgcolor="#666666" height="1"></td>
			  <td bgcolor="#666666" height="1"></td>
			  </tr>
			<tr>
			  <td class="TX" align="center">&nbsp;</td>
			  <td class="TX" align="center">&nbsp;</td>
			  <td class="TX" align="center">&nbsp;</td>
			  <td class="TX" align="center">&nbsp;</td>
			  <td class="TX" align="center">&nbsp;</td>
			  </tr>
					<?php
					if (is_array($qry_quest) && is_array($qry_list_type_profil)){
					?>
					  <tr> 
						<td align="left" class="TX">Type de profil :</td>
						<td align="center" class="TX"></td>
						<td align="left"> 
						  <select name="select_profil" class="form_ediht_Tarifs">
							  <?php
							  foreach($qry_list_type_profil as $type_profil){
								echo '<option value="'.$type_profil['code_id'].'">'.htmlentities($type_profil['code_libelle']).'</option>';
							  }
							  ?>
						  </select>
						</td>
						<td align="center" class="TX">&nbsp;</td>
						<td align="center" class="TX">&nbsp;</td>
					  </tr>
					  <tr> 
						<td align="left" class="TX">&nbsp;</td>
						<td align="center" class="TX">&nbsp;</td>
						<td align="left">&nbsp;</td>
						<td align="center" class="TX">&nbsp;</td>
						<td align="center" class="TX">&nbsp;</td>
					  </tr>
					  <tr> 
						<td align="left" class="TX" height="40">Valeur&nbsp;des&nbsp;points&nbsp;:&nbsp;</td>
						<td align="center" height="40" class="TX">Supérieur&nbsp;ou&nbsp;égal&nbsp;à&nbsp;</td>
						<td align="left" height="40"> 
						  <input type="text" class="form_ediht_Tarifs" name="points_inf" maxlength="5" size="5">
						</td>
						<td align="center" height="40" class="TX">Strictement&nbsp;inférieur&nbsp;à&nbsp;</td>
						<td align="center" height="40" class="TX">
						 <input type="text" class="form_ediht_Tarifs" name="points_sup" maxlength="5" size="5">
						</td>
					  </tr>
					  <tr> 
						<td align="left" class="TX" height="40">Valeur&nbsp;des&nbsp;segments&nbsp;:&nbsp;</td>
						<td align="center" height="40" class="TX">&nbsp;</td>
						<td align="left" height="40"> 
						  <input type="text" class="form_ediht_Tarifs" name="segments" maxlength="5" size="5">
						</td>
						<td align="center" height="40" class="TX">&nbsp;</td>
						<td align="center" height="40" class="TX">&nbsp;</td>
					  </tr>
					  <tr> 
						<td align="left" class="TX" height="40">Valeur&nbsp;des&nbsp;blocs&nbsp;:&nbsp;</td>
						<td align="center" height="40" class="TX">&nbsp;</td>
						<td align="left" height="40"> 
						  <input type="text" class="form_ediht_Tarifs" name="blocs" maxlength="5" size="5">
						</td>
						<td align="center" height="40" class="TX">&nbsp;</td>
						<td align="center" height="40" class="TX">&nbsp;</td>
					  </tr>
					<?php
					}else{
					?>
						<tr> 
						  <td colspan="3" align="center" height="40" class="TX">Un problème est survenu</td>
						  <td width="14" height="40"></td>
						</tr>
					<?php
					}
					?>
			    </table></td>
			  </tr>
			</table>
		  <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
			<tr> 
			  <td align="left" class="TX"> 
				
			  </td>
			</tr>

			  </table>
			  <br>
			  
			  <p style="text-align:center">
				<?php
				if (is_array($qry_quest)){
					?>
					<input type="button" name="Submit" value="Valider" class="BN" onClick="verif();">&nbsp;
					<?php
				}
				?>
				 </p>
		</form>
		</body>
	</html>
<?php
}else{
	include('no_acces.php');
}
?>