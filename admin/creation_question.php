<?php
session_start();
// Si l'utilisateur est un admin vakom ou un super admin
if ($_SESSION['droit']==9 && $_GET['questid']!=''){
	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	$db = new db($conn);
	
	if ($_POST['posted'] && trim($_POST['question'])){
		// On récupère l'id à insérer. (Il nous servira pour faire la redirection) 
		$sql_seq_num 	= "SELECT SEQ_ID.NEXTVAL SEQ_NUM FROM DUAL";
		$qry_seq_num 	= $db->query($sql_seq_num);
		$seq_num		= intval($qry_seq_num[0]['seq_num']);

		if(isset($_POST['quest_type_question_code_id']) && intval($_POST['quest_type_question_code_id']) == 12000)
		{
					$sql_insert_question = "INSERT INTO CHOIX (CHOIX_ID,CHOIX_LANG_ID,CHOIX_QUESTION,CHOIX_QUESTION_COMM,CHOIX_1,CHOIX_2,CHOIX_3,
			CHOIX_4, CODE_ID) VALUES('".$seq_num."',
			'1',
			'',
			'',
			'".txt_db($_POST['question'])."',
			'".txt_db($_POST['question2'])."',
			'".txt_db($_POST['question3'])."',
			'',
			'".txt_db($_POST['item_selected'])."')";
		} else {
			$sql_insert_question = "INSERT INTO CHOIX (CHOIX_ID,CHOIX_LANG_ID,CHOIX_QUESTION,CHOIX_QUESTION_COMM,CHOIX_1,CHOIX_2,CHOIX_3,
			CHOIX_4) VALUES('".$seq_num."',
			'1',
			'".txt_db($_POST['question'])."',
			'".txt_db($_POST['question_comm'])."',
			'".txt_db($_POST['choix1'])."',
			'".txt_db($_POST['choix2'])."',
			'".txt_db($_POST['choix3'])."',
			'".txt_db($_POST['choix4'])."')";
		}
		//echo $sql_insert_question;
		$qry_insert_question = $db->query($sql_insert_question);
		
		if (trim($_POST['question2'])!='' && trim($_POST['choix12'])!=''){
			$sql_insert_question2 = "INSERT INTO CHOIX (CHOIX_ID,CHOIX_LANG_ID,CHOIX_QUESTION,CHOIX_QUESTION_COMM,CHOIX_1,CHOIX_2,CHOIX_3,
			CHOIX_4) VALUES('".$seq_num."',
			'".txt_db($_POST['lang'])."',
			'".txt_db($_POST['question2'])."',
			'".txt_db($_POST['question2_comm'])."',
			'".txt_db($_POST['choix12'])."',
			'".txt_db($_POST['choix22'])."',
			'".txt_db($_POST['choix32'])."',
			'".txt_db($_POST['choix42'])."')";
			
			//echo $sql_insert_question2;
			$qry_insert_question2 = $db->query($sql_insert_question2);
		}
		$sql_insert_val = "INSERT INTO QUEST_A_VAL VALUES('".txt_db($_GET['questid'])."',
		'".$seq_num."',
		'1',
		'".txt_db($_POST['pi1'])."',
		'".txt_db($_POST['pi2'])."',
		'".txt_db($_POST['pi3'])."',
		'".txt_db($_POST['pi4'])."',
		'".txt_db($_POST['pn1'])."',
		'".txt_db($_POST['pn2'])."',
		'".txt_db($_POST['pn3'])."',
		'".txt_db($_POST['pn4'])."',
		'".txt_db($_SESSION['vak_id'])."',
		SYSDATE,
		'',
		'',
		'',
		'')";
		
		//echo $sql_insert_val;
		$qry_insert_val = $db->query($sql_insert_val);
		
			?>
			<script type="text/javascript">
				window.opener.location.reload(true);
				//document.location.href='edit_question.php?questionid='+<?php echo $seq_num ?>+'&questid='+<?php echo $_GET['questid'] ?>;
				window.close();
			</script>		
			<?php
	}


		$sql_info_quest = "SELECT * FROM QUESTIONNAIRE WHERE QUEST_ID='".txt_db($_GET['questid'])."'";
		$qry_info_quest = $db->query($sql_info_quest);

		$sql_langue_list = "SELECT LANGUE.LANG_ID, LANGUE.LANG_LIBELLE FROM LANGUE, QUEST_A_LANG WHERE QUEST_A_LANG.LANG_ID=LANGUE.LANG_ID AND QUEST_A_LANG.QUEST_ID='".txt_db($_GET['questid'])."' AND LANGUE.LANG_ID NOT IN ('1')";
		$qry_langue_list = $db->query($sql_langue_list);

		$sql_theme_quest = "SELECT CODE_ID, CODE_LIBELLE FROM CODE WHERE CODE_TABLE='BPM_THEME' AND CODE_LANG_ID=1 and CODE_ACTIF=1 ORDER BY CODE_ORDRE";
		$qry_theme_quest = $db->query($sql_theme_quest);
		
		if (is_array($qry_info_quest)){
			?>
			<html>
			<head>
			<title>Vakom</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<link rel="stylesheet" href="../css/nvo.css" type="text/css">
			<link rel="stylesheet" href="../css/general.css" type="text/css">
			<script language="JavaScript">
			<!--
			function tester_question(){
				var question = document.getElementById('question').value;
				var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
				var filename = "ajax_test_question.php"; // La page qui réceptionne les données
				var data     = null; 

				var xhr_object = null; 
					 
				if(window.XMLHttpRequest) // Firefox 
				   xhr_object = new XMLHttpRequest(); 
				else if(window.ActiveXObject) // Internet Explorer 
				   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
				else { // XMLHttpRequest non supporté par le navigateur 
				   alert("<?php echo $t_browser_support_error_1 ?>");
				   return; 
				} 
				 

				data = "question="+question+"&questid=<?php echo $_GET['questid'] ?>&questionid=0";
				
				if(method == "GET" && data != null) {
				   filename += "?"+data;
				   data      = null;
				}
				 
				xhr_object.open(method, filename, true);

				xhr_object.onreadystatechange = function() {
				   if(xhr_object.readyState == 4) {
					  var tmp = xhr_object.responseText.split(":"); 
					  if(typeof(tmp[0]) != "undefined") { 
						//if(tmp[0]>0){
						//	alert('Question déjà existante.');
						//}else{
							document.form.submit();
						//}
					  }
				   }
				} 

				xhr_object.send(data); //On envoie les données
			}
			function MM_goToURL() { //v3.0
			  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			//-->
			</script>
			</head>
			<body bgcolor="#FFFFFF" text="#000000">
			<?php 
			if (is_array($qry_info_quest) && $qry_info_quest[0]['quest_type_question_code_id'] != 12000) { ?>
			<form name="form" method="post" action="#">
				<table width="830" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;QUESTIONNAIRE <?php echo $qry_info_quest[0]['quest_nom'] ?></td>
				</tr>
				</table>
			  <table border="0" cellspacing="0" cellpadding="0"  width="830" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td class="TX_Tarifs" colspan="5">Ajout&nbsp;d'une&nbsp;question</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td width="14"></td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">Langue : </td>
				  <td align="left" class="TX">Français</td>
				  <td align="left" class="TX"> 
					<?php
					if (is_array($qry_langue_list)){
						?>
						<select name="lang" class="form_ediht_Tarifs">
							<?php
							foreach ($qry_langue_list as $langue){
								echo '<option value="'.$langue['lang_id'].'">'.$langue['lang_libelle'].'</option>';
							}
							?>
						</select>
						<?php
					}
					?>
				  </td>
				  <td align="center">&nbsp; </td>
				  <td align="left">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">Question :</td>
				  <td align="left" class="TX"> 
					<textarea name="question" id="question" class="form_ediht_Tarifs" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="left"> 
					<textarea name="question2" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="left">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">Commentaire :</td>
				  <td align="left" class="TX"> 
					<textarea name="question_comm" id="question_comm" class="form_ediht_Tarifs" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="left"> 
					<textarea name="question2_comm" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="left">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" valign="top" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td align="center">+</td>
				  <td align="center">-</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="40"></td>
				  <td align="left" class="TX" height="40">Choix 1 :</td>
				  <td align="left"  class="TX"> 
					<textarea name="choix1" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="left" height="40"> 
					<textarea name="choix12" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="center" height="40"> 
					<select name="pi1" class="form_ediht_Tarifs">
					  <option value="E">E</option>
					  <option value="P">P</option>
					  <option value="C">C</option>
					  <option value="A">A</option>
					  <option value="N">N</option>
					</select>
				  </td>
				  <td align="center" height="40"> 
					<select name="pn1" class="form_ediht_Tarifs">
					  <option value="E">E</option>
					  <option value="P">P</option>
					  <option value="C">C</option>
					  <option value="A">A</option>
					  <option value="N">N</option>
					</select>
				  </td>
				  <td width="14" height="40"></td>
				</tr>
				<tr> 
				  <td width="14" height="40"></td>
				  <td align="left" class="TX" height="40">Choix 2 :</td>
				  <td align="left" class="TX"> 
					<textarea name="choix2" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="left" height="40"> 
					<textarea name="choix22" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="center" height="40"> 
					<select name="pi2" class="form_ediht_Tarifs">
					  <option value="E">E</option>
					  <option value="P">P</option>
					  <option value="C">C</option>
					  <option value="A">A</option>
					  <option value="N">N</option>
					</select>
				  </td>
				  <td align="center" height="40"> 
					<select name="pn2" class="form_ediht_Tarifs">
					  <option value="E">E</option>
					  <option value="P">P</option>
					  <option value="C">C</option>
					  <option value="A">A</option>
					  <option value="N">N</option>
					</select>
				  </td>
				  <td width="14" height="40"></td>
				</tr>
				<tr> 
				  <td width="14" height="40"></td>
				  <td align="left" class="TX" height="40">Choix 3 :</td>
				  <td align="left" class="TX"> 
					<textarea name="choix3" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="left" height="40"> 
					<textarea name="choix32" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="center" height="40"> 
					<select name="pi3" class="form_ediht_Tarifs">
					  <option value="E">E</option>
					  <option value="P">P</option>
					  <option value="C">C</option>
					  <option value="A">A</option>
					  <option value="N">N</option>
					</select>
				  </td>
				  <td align="center" height="40"> 
					<select name="pn3" class="form_ediht_Tarifs">
					  <option value="E">E</option>
					  <option value="P">P</option>
					  <option value="C">C</option>
					  <option value="A">A</option>
					  <option value="N">N</option>
					</select>
				  </td>
				  <td width="14" height="40"></td>
				</tr>
				<tr> 
				  <td width="14" height="40"></td>
				  <td align="left" class="TX" height="40">Choix 4 :</td>
				  <td align="left" height="40"> 
					<textarea name="choix4" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="left" height="40"> 
					<textarea name="choix42" class="form_ediht_Tarifs" maxlength="255" cols="30" style="height: 50px;"></textarea>
				  </td>
				  <td align="center" height="40"> 
					<select name="pi4" class="form_ediht_Tarifs">
					  <option value="E">E</option>
					  <option value="P">P</option>
					  <option value="C">C</option>
					  <option value="A">A</option>
					  <option value="N">N</option>
					</select>
				  </td>
				  <td align="center" height="40"> 
					<select name="pn4" class="form_ediht_Tarifs">
					  <option value="E">E</option>
					  <option value="P">P</option>
					  <option value="C">C</option>
					  <option value="A">A</option>
					  <option value="N">N</option>
					</select>
				  </td>
				  <td width="14" height="40"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
			  <br>
			  <p style="text-align:center">
					<input type="hidden" name="posted" value="1">
					<input type="button" onClick="tester_question();" name="Submit" value="Valider" class="BN">
				  </p>
                  
			</form>
			<?php } else { ?>
			<form name="form" method="post" action="#">
				<input type="hidden" name="quest_type_question_code_id" value="<?php echo $qry_info_quest[0]['quest_type_question_code_id']; ?>">
				<input type="hidden" name="item_selected" id="item_selected" value="">				
				<table width="830" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				  <td width="20">&nbsp;</td>
				  <td class="Titre_Tarifs2"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;QUESTIONNAIRE BPM</td>
				</tr>
				</table>
				
				
			  <table border="0" cellspacing="0" cellpadding="0"  width="830" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td class="TX_Tarifs" colspan="3">Ajout&nbsp;d'une&nbsp;question</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td bgcolor="#666666" height="1"></td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td width="14"></td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td class="TX" align="center">&nbsp;</td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">Langue : </td>
				  <td colspan="2" align="left"><select name="langue" id="langue" class="form_ediht_Tarifs">
					  <option value="">Français</option>
					  <option value="">Anglais</option>
					  <option value="">Allemand</option>
					  <option value="">Italien</option>
					</select>			      </td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">Thème :</td>
				  <td align="left"><span class="TX">
				    <select name="theme" id="theme" class="form_ediht_Tarifs">
				    <?php
						foreach ($qry_theme_quest as $qry_theme){
							echo '<option value="'.$qry_theme['code_id'].'">'.$qry_theme['code_libelle'].'</option>';
						}
					?>
			      </select>
				  </span></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">Sous-thème : </td>
				  <td align="left"><span class="TX">
				    <select name="stheme" id="stheme" class="form_ediht_Tarifs"></select>
				  </span></td>
				  <td align="left" class="TX">Item : 
				    <select name="item" id="item" class="form_ediht_Tarifs">
	              </select></td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">Question : </td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">Manager</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr> 
				  <td width="14"></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left" class="TX"> 
					<textarea name="question" id="question" class="form_ediht_Tarifs" style="height: 50px; width: 90%"></textarea>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">Collaborateur (trice) pour un manager Homme</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left" class="TX"><textarea name="question2" class="form_ediht_Tarifs" style="height: 50px; width: 90%"></textarea></td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td align="left" class="TX">Collaborateur (trice) pour un manager Femme</td>
				  <td align="left">&nbsp;</td>
				  <td></td>
			    </tr>
				<tr>
				  <td></td>
				  <td align="left" class="TX">&nbsp;</td>
				  <td colspan="2" align="left" class="TX"><textarea name="question3" class="form_ediht_Tarifs" style="height: 50px; width: 90%"></textarea></td>
				  <td></td>
			    </tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			  </table>
			  <br>
			  <p style="text-align:center">
					<input type="hidden" name="posted" value="1">
					<input type="button" onClick="tester_question();" name="Submit" value="Valider" class="BN">
				  </p>
			</form>
			<?php } ?>
			</body>
			
			</html>
	<?php
	}
}else{
	include('no_acces.php');
}
?>
<script type="text/javascript" src="../js/jquery-latest.min.js"></script>
<script>
	loadTheme($('#theme').val(), 'stheme');
	
	$('#theme').change(function() {
		loadTheme($(this).val(), 'stheme');
	});
	$('#stheme').change(function() {
		var id = $("#stheme option:selected").attr("id");
		loadTheme(id, 'item');
	});	
	$('#item').change(function() {
		var id = $("#item option:selected").attr("id");
	  	$("#item_selected").val(id);
	});		
	function loadTheme(themeid, themetype) {
		$.ajax({
		  	url: "ajax-BPM.php",
  			cache: false,
  			data: {themeid : themeid, themetype : themetype},
  			dataType: "html",
  			success: function(html){
	  			if (themetype == 'stheme'){
  					$("#stheme").empty();
					$("#stheme").append(html);
					var stheme = $("#stheme option:selected").attr("id");
					loadTheme(stheme, 'item');
	  			} else {
  					$("#item").empty();
					$("#item").append(html);  					
	  			}
	  			$("#item_selected").val($("#item option:selected").attr("id"));
			}
		});
	}


</script>
