<?php
session_start();

if ($_SESSION['droit']>1){
	if ($_SESSION['part_id']>0 && $_SESSION['cert_id']>0){

		include ("../config/lib/connex.php");
		include ("../config/lib/db.oracle.php");
		$db = new db($conn);
		
		$sql_contact = "SELECT * FROM CERTIFIE, PARTENAIRE WHERE PARTENAIRE.PART_ID='".txt_db($_SESSION['part_id'])."' AND CERTIFIE.CERT_ID='".txt_db($_SESSION['cert_id'])."' AND CERTIFIE.CERT_PART_ID=PARTENAIRE.PART_ID";
		$contact = $db->query($sql_contact);
		if (is_array($contact)){
		?>
		<html>
		<head>
		<title>Vakom</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
		<link rel="stylesheet" href="../css/style.css" type="text/css">		
		<script language="JavaScript">
		<!--

		function MM_openBrWindow(theURL,winName,features) { //v2.0
		  window.open(theURL,winName,features);
		}

		function charge_ope(typeopeid){
			var method   = "GET"; //On définit la methode (ici je passe le code postal par l'url)
			var filename = "ajax_ope_liste.php"; // La page qui réceptionne les données
			var data     = null; 
			
			if 	(typeopeid.length>0){ //Si le code postal tapé possède au moins 2 caractères
				document.getElementById("ope_liste").innerHTML='<?php echo $t_patientez ?>...';
				var xhr_object = null; 
					 
					if(window.XMLHttpRequest) // Firefox 
					   xhr_object = new XMLHttpRequest(); 
					else if(window.ActiveXObject) // Internet Explorer 
					   xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
					else { // XMLHttpRequest non supporté par le navigateur 
					   alert("<?php echo $t_browser_support_error_1 ?>");
					   return; 
					} 
					 
					
					 
					if(typeopeid != ""){
						data = "certid=<?php echo $_GET['certid']?>&type_ope="+typeopeid;
					}
					if(method == "GET" && data != null) {
					   filename += "?"+data;
					   data      = null;
					}
					 
					xhr_object.open(method, filename, true);

					xhr_object.onreadystatechange = function() {
					   if(xhr_object.readyState == 4) {
						  var tmp = xhr_object.responseText.split(":"); 
						  if(typeof(tmp[0]) != "undefined") { 
							 document.getElementById("ope_liste").innerHTML = '';
							 if (tmp[0]!=''){
								document.getElementById("ope_liste").innerHTML = tmp[0];
							 }else{
								document.getElementById("ope_liste").innerHTML = "<?php echo $t_aucune_operation_type ?>";
							 }
						  }
					   } 
					} 

					xhr_object.send(data); //On envoie les données
			}
		}
		//-->
		</script>
		</head>
		<body bgcolor="#FFFFFF" text="#000000">
			<?php
				include("menu_top_new.php");
			?>
<div id="page" class="hfeed site">	
		<div id="main" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">                
				<article id="post-5" class="post-5 page type-page status-publish hentryAdmin">					
					<div class="entry-contentAdmin">
						<p class="Ident">Bienvenue&nbsp;<?php echo ucfirst($_SESSION['prenom']).'&nbsp;'.strtoupper(htmlentities($_SESSION['nom'])) ?></p>
						<p>				
			<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td width="20">&nbsp;</td>
			  <td class="Titre_Certifies"><img src="../images/fleche_grise.jpg" width="28" height="28" align="absmiddle">&nbsp;<?php echo htmlentities($contact[0]['part_nom']); ?>
				</td>
			</tr>
			</table>
			<table width="961" border="0" cellspacing="0" cellpadding="0" bgcolor="F1F1F1" align="center">
				<tr> 
				  <td width="14" height="14"><img src="../images/grishg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grishd.gif" width="14" height="14"></td>
				</tr>
				<tr> 
				  <td width="14"></td>
				  <td align="center" class="TX"> 
					<table width="900" border="0" cellspacing="0" cellpadding="2" class="TX">
					  <tr align="left"> 
						<td colspan="8" class="TX_Certifies"><?php echo $t_contacts ?></td>
					  </tr>
					  <tr align="left">
						<td colspan="8" height="1" bgcolor="#666666"> </td>
					  </tr>
					  <tr align="left"> 
						<td colspan="8" class="TX_GD"> 
						&nbsp;
						</td>
					  </tr>
							  <tr> 
								<td class="TX_bold"><?php echo $t_nom ?></td>
								<td class="TX_bold"><?php echo $t_prenom ?></td>
								<td align="center" class="TX_bold"><?php echo $t_droits ?></td>
								<td align="left" class="TX_bold"><?php echo $t_profil_OPR ?></td>
								<td  class="TX_bold"><?php echo $t_niveau_certif ?></td>
								<td class="TX_bold"><?php echo $t_date_certif ?></td>
								<td class="TX_bold" align="center"><?php echo $t_etat ?></td>
								<td class="TX_bold" align="center"><?php echo $t_jetons ?></td>
							  </tr>
							  <tr> 
								<td colspan="8" bgcolor="#CCCCCC" height="1" valign="top"></td>
							  </tr>
								<?php
								foreach($contact as $cert){
									if ($cert['cert_droit_certifie']=='1'){
										$sql_certif = "SELECT * FROM CERT_A_CERTIF, CODE WHERE CERT_A_CERTIF.CERTIF_CODE_ID=CODE.CODE_ID AND (CERTIF_CERTIFICATION IS NOT NULL OR CERTIF_FORMATION = 1 OR CERTIF_SUSPENDU= 1 ) AND CODE.CODE_TABLE='CERTIFICATION' AND CERT_A_CERTIF.CERTIF_CERT_ID='".txt_db($cert['cert_id'])."' ORDER BY CODE.CODE_LIBELLE ASC";
										//echo $sql_certif;
										$qry_certif = $db->query($sql_certif);
									}
									else
									{
										$qry_certif ='';
									}
								?>
								  <tr> 
									<td class="TX"><a href="#" onClick="MM_openBrWindow('admcertif_edit_contactClient.php','edit','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=940,height=500')"><?php echo strtoupper($cert['cert_nom']) ?></a></td>
									<td class="TX"><?php echo ucfirst($cert['cert_prenom']) ?></td>
									<td align="center" class="TX">
									<?php
									if ($cert['cert_droit_admin']=='1'){
										echo $t_admin;
										if ($cert['cert_droit_certifie']=='1'){
										echo '/<br>';
										}
									}
									if ($cert['cert_droit_certifie']=='1'){
										echo $t_cert;
									}
									?>
									</td>
									<td align="left" class="TX">
									<?php
									if($cert['cert_cand_id']!=''){
										$sql_opr = "SELECT LAST_OPR FROM CANDIDAT WHERE CAND_ID='".txt_db($cert['cert_cand_id'])."'";
										$qry_opr = $db->query($sql_opr);
										echo $qry_opr[0]['last_opr'];
									}
									?>
									  </td>
									<td class="TX">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												echo $certif['code_libelle'].'<br>';
											}
										}
										?>
									</td>
									<td class="TX">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												echo $certif['certif_certification'].'<br>';
											}
										}
										?>
									</td>
									<td class="TX" align="center">
										<?php
										if (is_array($qry_certif)){
											foreach($qry_certif as $certif){
												if ($certif['certif_formation']=='1'){
													echo $t_en_formation;
												}elseif($certif['certif_suspendu']=='1'){
													echo $t_suspendu;
												}
												echo '<br>';												
											}
										}
										?>
									</td>
									<td class="TX" align="center"><a href="#" onClick="MM_openBrWindow('admcertif_produits_contactClient.php','','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes')"><?php echo $t_consultez ?></a></td>
								  </tr>
								  <tr> 
									<td colspan="8" bgcolor="#CCCCCC" height="1" valign="top"></td>
								  </tr>
								<?php
								}
						?>
					</table>
				  </td>
				  <td width="14"></td>
				</tr>
				<tr> 
				  <td width="14" height="14"><img src="../images/grisbg.gif" width="14" height="14"></td>
				  <td height="14"></td>
				  <td width="14" height="14"><img src="../images/grisbd.gif" width="14" height="14"></td>
				</tr>
			</table>
</p></div>	</article></div>	</div>	</div>	</div>			
		</body>
		</html>
		<?php
		}
	}
}else{
	include('no_acces.php');
}

?>
