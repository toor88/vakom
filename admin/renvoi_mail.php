<?php
session_start();

include ("../config/lib/connex.php");
include ("../config/lib/db.oracle.php");
$db = new db($conn);
	
	/* Si le formulaire a été soumis */
	if($_POST['valid']==1){
		
		$sql_infos_cand 		= "SELECT CERT_ID, CERT_NOM, CERT_PRENOM, CERT_LOGIN, CERT_PWD, CERT_EMAIL FROM CERTIFIE WHERE LOWER(CERT_EMAIL) ='".txt_db(strtolower($_POST['email']))."'";
		//echo $sql_infos_cand;
		$qry_infos_cand 		= $db->query($sql_infos_cand);

	
	/* Définition des variables simplifiées */
	$cert_nom	 			= $qry_infos_cand[0]['cert_nom'];
	$cert_prenom 			= $qry_infos_cand[0]['cert_prenom'];
	$login 					= $qry_infos_cand[0]['cert_login'];
	$mot_de_passe 			= $qry_infos_cand[0]['cert_pwd'];
	$dest					= str_replace(';',',',trim($qry_infos_cand[0]['cert_email']));
	
	/* Definition des entêtes du mail*/
	
	/* Libellés des textes du mail */
	$t_objet_renvoi_mail 	= "Renvoi de vos identifiants";
	$t_trouvez_mdp			= "Veuillez trouver ci-dessous les identifiants associ&eacute;s &agrave; votre adresse email.";
	$t_quest_bonjour		= "Bonjour";
	
	$t_quest_merci			= "Merci de votre confiance";
	$t_quest_cord			= "Bien cordialement";
	$t_quest_equipe			= "L'Equipe";
	$t_quest_envir			= "Avant d'imprimer cet email, r&eacute;fl&eacute;chissez &agrave; l'impact sur l'environnement";
	
	$objet					= $t_objet_renvoi_mail;

$sujet_mail = '[VAKOM] Vos codes d\'acc&egrave;s';

$name_s    		 = "VAKOM";
$email_s 		 = "accueil@vakom.fr";
$headers    	 = "From: ". $name_s . " <" . $email_s . ">\r\n";
$headers   		.= 'Content-Type: text/html; charset="ISO-8859-1"'."\r\n";
$headers  		.= 'Content-Transfer-Encoding: 8bit'."\r\n";

$message		 ="<html>
<body>
<p><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">Bonjour </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$cert_prenom."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">,<br>
<br>
Dans le cadre de notre partenariat vous b&eacute;n&eacute;ficiez d'un acc&egrave;s priv&eacute; &agrave; notre site 
: <br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
text-decoration:underline\" color=\"#0000FF\"><a href=\"http://www.vakom.fr\">Mon espace partenaire</a></font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<br>
Afin de pouvoir acc&eacute;der aux diff&eacute;rentes rubriques de l'Espace partenaire, il est 
n&eacute;cessaire de vous authentifier.<br>
<br>
<u>Vos identifiants sont les suivants :</u><br>
Login : </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$login."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
Mot de passe : </font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\">".$mot_de_passe."</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\"><br>
<br>
Ces informations ne peuvent en aucun cas &ecirc;tre communiqu&eacute;es par téléphone, nous vous invitons donc &agrave; les conserver.<br>
<br>
Bien cordialement,<br>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;font-weight:700\" color=\"#FF6600\">L'Equipe VAKOM<br></font>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
font-weight:700\" color=\"#FF6600\"><br>
</font><font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">
<img border=\"0\" src=\"http://www.extranet.lesensdelhumain.com/images/logo-miniopr.jpg\"><br>
</font>
<font style=\"font-size:11.0pt;font-family:&quot;Franklin Gothic Book&quot;;
color:#415564\">VAKOM<br>
38 rue bouquet<br>
76108 ROUEN<br>
Tel : 0232105920:<br>
<br>
</font>
</p>

</body>

</html>";

	/* Renvoi de l'email */
	if (is_array($qry_infos_cand) && mail($dest,$sujet_mail,$message,$headers)){
		$error_color = "#00FF00";
		$error_mail = "L'email a bien été envoyé.";		
		?>
		<script type="text/javascript">
			// On ferme la fenetre
			window.close();
		</script>
		<?php
	}else{
		$error_color = "#FF0000";
		$error_mail = "L'email n'a pas été envoyé (Email inconnu).";		
	}
}
?>
<html>
<head>
	<title>Vakom - Renvoi de vos identifiants</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/nvo.css" type="text/css">
		<link rel="stylesheet" href="../css/general.css" type="text/css">
</head>
<body text="#000000" bgcolor="#ffffff" topmargin="0">
	<form name="form" action="#" method="post">
		<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" background="images/fond_pages.png" valign="top">
			<tr>
				<td width="100" height="50"> </td><td colspan="2" class="texte" align="left" style="color:<?php echo $error_color ?>;"><b><?php echo $error_mail ?></b></td>
			</tr>
			<tr>
				<td width="100">&nbsp;</td>
				<td style="text-align: left; font-family:Trebuchet, Trebuchet MS, Arial, Helevetica, sans-serif; font-size: 11pt;" valign="top" colspan="2">Vous avez oublié votre mot de passe, merci d'entrer votre adresse électronique de connexion.<br>
					Vous recevrez, dans les plus brefs délais, votre mot de passe par mail.<br>
					Si vous souhaitez changer de mot de passe, merci de contacter Vakom.<br>
				</td>
			</tr>
			<tr>
				<td height="30" width="100" >&nbsp;</td>
				<td height="30" width="1" style="line-height:30px; text-align: left; font-family:Trebuchet, Trebuchet MS, Arial, Helevetica, sans-serif; font-size: 11pt;"><strong>Adresse&nbsp;Email&nbsp;:&nbsp;</strong></td>
				<td height="30" style="line-height:30px; text-align: left;" width="390"><input type="text" name="email" value="<?php echo htmlentities(stripslashes($_GET['mail'])) ?>" size="50" maxlength="100" class="form_ediht" style="margin: 0px; padding: 0px;"/>&nbsp;<input type="hidden" name="valid" value="1"></td>
			</tr>
			<tr>
				<td height="30" colspan="3" valign="middle" style="line-height:30px; text-align: center; font-family:Trebuchet, Trebuchet MS, Arial, Helevetica, sans-serif; font-size: 11px;"><input type="submit" value="OK" class="BN"></td>
			</tr>
			<tr><td colspan="3" height="1%">&nbsp;</td></tr>
		</table>
	</form>
</body>
</html>