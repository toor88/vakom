<?php
session_start();

/* Si la personne est loguée */
if($_SESSION['vak_id']>0){

	include ("../config/lib/connex.php");
	include ("../config/lib/db.oracle.php");

	$db = new db($conn);
	
	header('Content-type: text/html; charset=UTF-8'); 

	/* Si toutes les conditions sont réunies pour effectuer la requête */
	if (isset($_GET['txtimgid']) && $_GET['langid']!='' && $_GET['regroupid']!='' && ($_GET['sexe']=='h' || $_GET['sexe']=='f')){
		
		/* On sélectionne le champ approprié */
		switch($_GET['sexe']){
			case 'h':
				$champ='img_homme';
			break;
			case 'f':
				$champ='img_femme';
			break;
		}
		
		/* On sélectionne l'image correspondant aux parametres */
		$sql_img = "SELECT ".strtoupper($champ)." FROM TEXTE_IMAGE_A_IMG WHERE TXT_IMG_ID='".txt_db($_GET['txtimgid'])."' AND TXT_IMG_LANG_ID='".intval($_GET['langid'])."' AND CODE_REGROUP_ID='".txt_db($_GET['regroupid'])."'";
		$qry_img = $db->query($sql_img);

		/* Si on a la réponse */
		if(is_array($qry_img)){
			/* Si le fichier existe  */
			if(file_exists('./images_txtimg/'.$qry_img[0][$champ]) && $qry_img[0][$champ]!=''){
				/* On le supprime */
				unlink('./images_txtimg/'.$qry_img[0][$champ]);
			}
			/* On met a jour la table */
			$sql_up_img = "UPDATE TEXTE_IMAGE_A_IMG SET ".strtoupper($champ)."='' WHERE TXT_IMG_ID='".txt_db($_GET['txtimgid'])."' AND TXT_IMG_LANG_ID='".intval($_GET['langid'])."' AND CODE_REGROUP_ID='".txt_db($_GET['regroupid'])."'";
			$qry_up_img = $db->query($sql_up_img);
		}
	}
}

/* On envoie la réponse ajx */
echo ":";
?>